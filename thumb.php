<?php
	function imageResize($srcfn,$dstfn,$w,$h) { 
		/*	
		$imgstr = exif_thumbnail($srcfn,$w2,$h2,$type);
		if($imgstr != false) {
			file_put_contents($dstfn,$imgstr);
			return true;	
		} */
	
		$tp = exif_imagetype($srcfn);
		switch($tp) {
			case 1: $src = imagecreatefromgif($srcfn); break;
			case 2: $src = imagecreatefromjpeg($srcfn); break;
			case 3: $src = imagecreatefrompng($srcfn); break;
			default: $src = imagecreatefromjpeg("error.jpg"); $srcfn = "error.jpg";
		}
	
		// get the source image's widht and hight
		$src_w = imagesx($src);
		$src_h = imagesy($src);
		
		//檢查原圖是否小於縮圖
		if ( ($src_w <= $w) and ($src_h <= $h)) {
			copy($srcfn,$dstfn);
			imagedestroy($src);
			return true;
		} 
		
		// assign thumbnail's widht and hight
		if($src_w > $src_h){
			 $thumb_w = $w;
			 $thumb_h = intval($src_h / $src_w * $w);
		}else{
			 $thumb_h = $h;
			 $thumb_w = intval($src_w / $src_h * $h);
		}
		
		// if you are using GD 1.6.x, please use imagecreate()
		$thumb = imagecreatetruecolor($thumb_w, $thumb_h);
		
		// start resize
		//imagecopyresized($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h); 品質不佳
		imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);

		// save thumbnail
		imagejpeg($thumb, $dstfn,100);
		imagedestroy($thumb);
		return true;
	}
?>