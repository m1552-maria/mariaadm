<?	//設定維護系統全域變數
	// putenv("TZ=Asia/Taipei"); 
	date_default_timezone_set("Asia/Taipei");
	define('DB_SOURCE',"mysql:host=localhost;dbname=mariaadm");
	define('DB_ACCOUNT',"root");
	define('DB_PASSWORD',"maRia2371");

	$CompanyName = "瑪利老爹網";
	$root = $_SERVER['DOCUMENT_ROOT'];
	$AESKEY = 'maRia2371';
	
	//::for首頁輪撥圖片
	$newspic_Path = 'data/news/';

	$DB_Type = 0; // 0=MySQL, 1=MSSQL, 2=Access, 3=ODBC
  $VenderDB = 'mariaadm';
	$conn = mysql_connect("localhost","root","maRia2371") or die("資料庫連接失敗!");		
  mysql_select_db($VenderDB,$conn) or die("資料庫選擇失敗!");
	mysql_query("SET NAMES 'utf8'");
	if(isset($_REQUEST['cfginfo'])) showInfo();
	
	function getRulPwd($id, $pwd) {
		return "maria".$id.$pwd.$id."maria";
	}

	//---- Utilities Function	--------------------------------------------------------------//
	function showInfo() {
		global $CompanyName,$VenderDB,$root,$DB_Type,$conn;
		echo "CompanyName = $CompanyName<br>";
		echo "VenderDB = $VenderDB<br>";
		echo "root = $root<br>";
		echo "DB_Type = $DB_Type<br>";
		echo "conn = $conn<br>";
	}
	
	function getClassPath($dbtable, $id, $idField, $pidField, $titleField) {
		$rzt = '';
		$sql = "select $pidField,$titleField from $dbtable where $idField='$id'";
		$rs = db_query($sql);
		if(!db_eof($rs)) {
			$r = db_fetch_array($rs);
			$rzt = $r[$titleField].':'.$id;
			if($r[0]=='0') return $rzt;
			return $rzt.','.getClassPath($dbtable,$r[0],$idField, $pidField, $titleField);
		} else return $rzt;
	}

  function ckEmpIDValid($empID,$bLeader=0) {  //檢查empID有效性, $bLeader=1則判斷是否副主任以上主管
    $LeaderAry = array('G001','G002','G003','G004','G005','G006');
		$sql = "select * from emplyee where empID='$empID'";
		$rs = db_query($sql);
    if(!db_eof($rs)) {
      $r = db_fetch_array($rs);
      if($r['isOnduty']!=1) return false;
      if($bLeader && !in_array($r['jobID'],$LeaderAry)) return false; 
      return true;  
    } else return false;
  }
	
	//---- Accounting Function	--------------------------------------------------------------//
	/* DEMO:
		lock table bullets write;
		unlock tables
		Notice: 當LOCK Table/Record時 會永久Block其它Session 直到Unlock，若發生Dead LOCK,就很嚴重
		Maybe 設定連線時間 wait_timeout=180 (default is 28800) 會好一點 
	*/
	function checkTableLock($tblname) {	//檢查 Table 是否已經LOCK
		global $VenderDB;
		$sql = "show OPEN Tables where in_use>0 and `Database`='$VenderDB' and `table`='$tblname'";
		$rs = db_query($sql);
		return !db_eof($rs);
	}
	
	function checkAcctClose($idTitle, $condi, $user) {	//關帳檢查-後台
		$bClose = 0;	//未設定
		//$sql = "select isClose,allClose,cMan from acctClose where title='$idTitle' and conditions='$condi' and (allClose=1 or cMan='$user')";
		$sql = "select isClose,allClose,cMan from acctClose where title='$idTitle' and conditions='$condi'";
		$rs = db_query($sql);
		$aa =array();
		while( $r=db_fetch_array($rs) ) {
			if($r[1]) { $bClose=1; return $bClose; }	//關帳
			else if($r[0]) $aa[] = $r[2];		//鎖定的帳號Array
		}
		if(count($aa)>0) $bClose=join(',',$aa);
		return $bClose;
	}
	
	function checkAcctF($idTitle, $condi) {	//關帳檢查-前台
		$bClose = 0;	//未設定
		$sql = "select isClose,allClose,cMan from acctClose where title='$idTitle' and conditions='$condi' and allClose=1";
		$rs = db_query($sql);
		$bClose = !db_eof($rs);
		return $bClose;
	}	
	
	function setAcctClose($idTitle, $condi, $user, $value=1, $allClose=0) {	//關帳設定
		//檢查是否已有設定
		$sql = "select id from acctClose where title='$idTitle' and conditions='$condi' and cMan='$user'";
		$rs = db_query($sql);
		if(db_eof($rs)) {	//新增
			if($allClose) { //全帳號關閉
				$sql = "insert into acctClose(title,conditions,cMan,allClose) values('$idTitle','$condi','$user',1)";
			} else {	//關閉特定帳號
				$sql = "insert into acctClose(title,conditions,cMan,isClose) values('$idTitle','$condi','$user',$value)";
			}	
		} else {	//修改
			$r = db_fetch_array($rs);
			$id = $r[0];
			if($allClose) { //全帳號關閉
				$sql = "update acctClose set allClose=$allClose where id=$id";
			} else {	//關閉特定帳號
				$sql = "update acctClose set isClose=$value where id=$id";
			}	
		}
		return db_query($sql);
	}
	
	//---	Database Functions	--------------------------------------------------------------//
	function db_data_seek($rs,$pos) {
		global $DB_Type;
		switch($DB_Type) {	
			case 0: return mysql_data_seek($rs,$pos);
			case 1: return mssql_data_seek($rs,$pos);
			case 3: return false;	//odbc 不支援 data_seek
		}		
		return false;
	}
	
	function db_num_rows($rs) {
		global $DB_Type;
		switch($DB_Type) {
			case 0: return mysql_num_rows($rs);
			case 1: return mssql_num_rows($rs); 
			case 3: return odbc_num_rows($rs);		
		}	
		return 0;
	}
	
	function db_field_name($rs,$idx) {
		global $DB_Type;
		switch($DB_Type) {
			case 0: return mysql_field_name($rs,$idx); break;
			case 1: return mssql_field_name($rs,$idx); break; 
			case 3: return odbc_field_name($rs,$idx);
		}		
	}
	
	function db_num_fields($rs) {
		global $DB_Type;
		switch($DB_Type) {
			case 0: return mysql_num_fields($rs); break;
			case 1: return mssql_num_fields($rs); break; 
			case 3: return odbc_num_fields($rs);
		}	
	}
	
	function db_close($conn) {
		global $DB_Type;
		switch($DB_Type) {
			case 0: mysql_close($conn); break;
			case 1: mssql_close($conn); break; 
			case 3: odbc_close($conn);
		}
	}
	
	function db_query($sql,$con=NULL) {
		global $DB_Type;
		global $conn;
		if(!$con) $con=$conn;
		switch($DB_Type) {
			case 0: return mysql_query($sql,$con);
			case 1: return mssql_query($sql,$con); 
			case 3: return odbc_exec($con,$sql);
		}
		return false;
	}
		
	function db_eof($rs) {
		global $DB_Type;
		switch($DB_Type) {
			case 0: return mysql_num_rows($rs)==0;
			case 1: return mssql_num_rows($rs)==0; 
			case 3: return odbc_num_rows($rs)==0;
		}
		return false;		
	}
	
	function db_fetch_array($rs) {
		global $DB_Type;
		switch($DB_Type) {
			case 0: return mysql_fetch_array($rs);
			case 1: return mssql_fetch_array($rs); 
			case 3: return odbc_fetch_array($rs);
		}
		return false;	
	}
	
	function db_free_result($rs) {
		global $DB_Type;
		switch($DB_Type) {	
			case 1: return mysql_free_result($rs);
			case 3: return mssql_free_result($rs);
			case 3: return odbc_free_result($rs);
		}
		return false;
	}		
?>