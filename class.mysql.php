<?php 
//** Pre-Condition: setup Connect for Database **//
class mysql {
  var $result;						//recordset resouce
  var $table;							//資料表
	var $row = array();     // fields in this table field=value
	
	//*** 建構子 */
	function __construct() {  // (table name)
		$arg  = func_get_args();
		if(isset($arg[0])) $this->table =	$arg[0];
		mysql_query("SET NAMES 'utf8'");
	}
	
	//*** 解構子 */
	function __desteuct() {
		if($this->result)	mysql_free_result($this->result);
		mysql_close();
	}
	
	/*** SQL QUERY */
  function query($str) {
    $this->result = mysql_query($str) or die(mysql_error());
		return $this->result;
  }	
	
	/*** 新增ㄧ筆資料 : 使用前須完成 row 的內容 ***/
	function insert() {
		if(count($this->row)==0) return false;
		$fa = array_keys($this->row);
		$va = array_values($this->row);
		$fs = join(',',$fa);
		$vs = join(',',$va);
		$sql =  sprintf("INSERT INTO %s (%s) VALUES (%s)",$this->table, $fs, $vs);
		//echo $sql; exit;
		$this->result = mysql_query($sql) or die(mysql_error());
		return $this->result;
	}
	
	function update($condition) {
		if(count($this->row)==0) return false;
		$updateA = array();
		foreach($this->row as $f=>$v) $updateA[] = "$f=$v";
		$updateStr = join(',',$updateA);
		$sql = sprintf("UPDATE %s SET %s %s", $this->table, $updateStr, $condition);
		//echo $sql; exit;
		$this->result = mysql_query($sql) or die(mysql_error());
		return $this->result;
	}
}
?>