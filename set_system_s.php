<?php
	header("Content-type: text/html; charset=utf-8");
	$sSystemVar = "";
	foreach ($_POST as $key => $value) {
		if ($value === "") {
			echo "<script>alert('請輸入完整系統參數!!');history.back();</script>"; exit;
		}
		$sSystemVar .= '$'.$key.' = "'.$value.'";'."\r\n";
	}
	$sSystemVar = '<?'."\r\n".$sSystemVar.'?>';
	if (!empty($sSystemVar)) {
		if (!$handle = fopen("inc_sys.php", 'w')) {
			echo "<script>alert('無法設定系統參數!!');history.back();</script>"; exit;
    }

    // Write $somecontent to our opened file.
    if (fwrite($handle, $sSystemVar) === FALSE) {
			echo "<script>alert('系統參數設定失敗!!');history.back();</script>"; exit;
    }
    fclose($handle);
		echo "<script>alert('系統參數已儲存!!');location.href='set_system.php';</script>";
	}
?>
