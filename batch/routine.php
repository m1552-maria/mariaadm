<?php
	/* 執行 todo_list 內的內容 */
	include_once "../config.php";
	include_once '../domail.php';
	include_once($root.'/maintain/emplyee_annual/sendMail.php');
	//include '../getEmplyeeInfo.php';	
		
	//選取時間內應執行的動作	
	$sql = "select * from todo_list where ((bDate < Now() and eDate > Now()) or (bDate is null and eDate is null))  and status=0";
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)) {
		$id = $r['id'];
		$actAry = split(':',$r['action']);
		switch($actAry[0]) {
			case '活動投保通知' :
				$paramAry = split(',',$actAry[1]);
				$sID = $paramAry[0];					//schedule.ID
				$announcer = $paramAry[1];	//員編
				$title = $paramAry[2];			//schedule.title
				//取的員工資料
				$sqlcmd = "select * from emplyee where empID=$announcer";
				$rsX = db_query($sqlcmd);
				$rX = db_fetch_array($rsX);
				$empName = $rX['empName'];
				$email = $rX['email'];
				
				//::寄信通知	
				$fn='../mails/insurence_notify.html'; 
				$content=file_get_contents($fn);
				$content=str_replace("{Name}",$empName,$content);
				$content=str_replace("{title}",$title,$content);

				$subtx = $CompanyName.'-活動投保通知';
				$rzt   = send_mail($email, $subtx, $content); 
				//echo $rzt;
				//::write back result
				$sql = "update todo_list set status=2,results='$rzt' where id=$id";
				db_query($sql);
				break;	
			case '當年度特休提醒' :
				$annualMai = setUpAnnualMail();
				break;
			case '每日特休提醒' :
				$annualMailOne = sendAnnualMailOne();
				break;
			/*case '未休完特休提醒' :
				$dateList = explode(',', $actAry[1]);
				$sendDate = explode('=', $dateList[0]);
				$cDate = explode('=', $dateList[1]);

				$noRestMail = annualNoRestMail($sendDate[1], $cDate[1]);
				$sql = "update todo_list set status=2 where id=$id";
				db_query($sql);
				break;*/
			case '每日員工基本表備份' :
				include_once($root.'/maintain/emplyee/backupEmplyee.php');
				break;
		}
	}
?>
