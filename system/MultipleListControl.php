<?php
	class MultipleListControl extends BaseListControl{
		function __construct($data=array(), $fields=array(), $option=array(),$pgInfo=null)	{
			parent::__construct($data, $fields, $option, $pgInfo);
			$this->defaultOption['isCooked'] = (!empty($option['isCooked'])) ? true : false;
			$this->defaultOption['cookName'] = (!empty($option['cookName'])) ? $option['cookName'] : 'checkMinorId';

			if($this->defaultOption['isCooked']) echo '<input type="hidden" data-cook-name="'.$this->defaultOption['cookName'].'" value="'.$this->defaultOption['cookName'].'">';
			if(get_class($this)=='MultipleListControl') echo "</table></form></div>";
		}

		protected function genMinorHeader($fields) {
			echo "<tr class='columnRow'>";
			foreach ($fields as $key => $value) {
				$text = $value[0];
				echo "<th class='columnHead' width='$value[1]'>$text</th>";
			}
			echo "</tr>";
		}

		protected function genRow($data, $fields = array(), $level = '') {
			global $ulpath;
			if(count($fields) == 0 ) $fields = $this->mFields;
			$count = 1;
			//$mod = ($this->mPageInf->curRecord) % $this->mPageInf->pageSize; $mod++;
			foreach ($data as $item) {
				$this->curRow = $item;	//for outside use 
				$color = '';
				if($this->defaultOption['alterColor']) {
					if($count%2==1)	$color="columnDataEven"; else $color="columnDataOdd";
				}
				//if($mod == $count) $color="columnCurrent";
				
				$key = array_keys($fields);
				$value = '';
				$levelVal = '';
				for($idx=0; $idx<count($fields); $idx++) {
					echo "<td class='columnData $color'>";					
					$ii = $key[$idx];
					$value = $this->defaultOption['dataKey']?$item[$ii]:$item[$idx];
					$fType = $fields[$ii][3][0];

					$minorCheckAll = '';
					if($idx==0 && !empty($item['minorData'])) {
						echo "<span class='foldOpen' onClick='foldAction(this);'> + </span>";
						$minorCheckAll = "data-minor-All='1'";
					}

					if($idx==0 && $this->defaultOption['checkbox']) {
						$levelVal = ($level != '') ? $level.'_'.$value : $value;
						echo "<input ".$minorCheckAll." type='checkbox' value='$value' name='ID[]' data-level-val='".$levelVal."'>";
					}
					switch ($fType) {
						case 'Id':
							$fmt=$fields[$ii][3][1];
							$recIdx = ($this->mPageInf->curPage-1)*$this->mPageInf->pageSize+$count;
							$onclick = sprintf($fmt,$recIdx);  
							echo "<a href='#' onClick='$onclick'>$value</a>";	break;	
						case 'Link': $link=$fields[$ii][3][1];	echo "<a href='$link$value'>$value</a>"; break;
						case 'Date': echo $value&&$value!='0000-00-00'?date('Y-m-d',strtotime($value)):'&nbsp;'; break;
						case 'DateTime': echo $value&&$value!='0000-00-00'?date('Y-m-d H:i:s',strtotime($value)):'&nbsp;'; break;
						case 'Image': if($value) {$w=$fields[$ii][3][1]; echo "<img src='$ulpath$value' width='$w'>";} break;
						case 'Select': $aa=$fields[$ii][3][1];   echo $aa[$value];	break;
						case 'Define': $func=$fields[$ii][3][1]; echo $func($value,$this); break;
						default: echo $value;	break;
					}
					echo "</td>";					
				}
				
				echo "</tr>";
				if(!empty($item['minorData'])) {
					echo "<tr class='none'><td></td>";
					echo "<td colspan='".(count($fields)-1)."'><table class='minorTable'>";
					$this->genMinorHeader($fields[key($fields)][4]);
					$this->genRow($item['minorData'], $fields[key($fields)][4], $levelVal);
					echo "</table></td></tr>";
				}
				$count++;
			}
		}
	}
?>