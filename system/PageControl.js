// JavaScript Document
$(function () {
	//$('#PageControl .firstBtn').bind('click',function(){alert('PageControl.firstBtn');});
	$('#PageControl button').bind('click',function(evt){
		func = window[evt.target.dataset.handler];
		if(isFunc(func)) func(evt);
	});
	
	$('#PageControl .numEdit').bind('keypress',function(evt){
		if(evt.keyCode==13) $('#PageControl .goBtn').click();
	});
	
});