$(function(){
	var cookName = '';
	if($('[data-cook-name]').length != 0) {
		cookName = ($('[data-cook-name]').val()) ? $('[data-cook-name]').val() : 'checkMinorId';
		multipleInit(cookName);
	}
	
	$('[data-level-val]').click(function(){
		if($(this).attr('data-minor-All') == 1) minorCheckAll(this);
		if(cookName != '') {
			var cookItem = [];
			if($.cookie(cookName) != null && $.cookie(cookName) != '') {
				cookItem = $.cookie(cookName).split(',');
			}
			
			$('[data-level-val]').each(function(){
				var thisVal = $(this).attr('data-level-val');
				if($(this).prop('checked') && $.inArray(thisVal, cookItem) < 0) {
					cookItem.push($(this).attr('data-level-val'));
				} else if(!$(this).prop('checked') && $.inArray(thisVal, cookItem) >= 0) {
					cookItem.splice($.inArray(thisVal, cookItem), 1);
				}
			});
			$.cookie(cookName, cookItem.join(','));
		}
	});
})

function multipleInit(cookName) {
	if($.cookie(cookName) != null && $.cookie(cookName) != '') {
		var minorIdList =  $.cookie(cookName).split(',');
		for(var i in minorIdList) {
			var checkID = $('[data-level-val="'+minorIdList[i]+'"]');
			checkID.parent('span').addClass('checked');
			checkID.prop('checked', true);
			checkID.parents('table').parents('tr').show();
			var levelVal =  minorIdList[i].split('_');
			var pid = '';
			//判別是否還有父層 (大於1 代表有)
			if(levelVal.length > 1) {
				var levelId = [];
				for (var j=0; j<(levelVal.length-1); j++) {
					levelId.push(levelVal[j]);
				}
				if(pid != levelId.join('_')) {
					pid = levelId.join('_');
					var foldObj = $('[data-level-val="'+pid+'"]').parents('td').children('.foldOpen');
					$(foldObj).attr('class','foldClose');
					$(foldObj).html(' - ');
				}
			}
		}

	}
}

function foldAction(obj) {
	if($(obj).attr('class') == 'foldOpen') {
		$(obj).attr('class','foldClose');
		$(obj).html(' - ');
		$(obj).parents('tr').next('tr').show();
	} else {
		$(obj).attr('class','foldOpen');
		$(obj).html(' + ');
		$(obj).parents('tr').next('tr').hide();
	}
}

function minorCheckAll(obj) {
	var minorTr = $(obj).parents('tr').next('tr');
	if($(obj).prop('checked') == true) {
		var foldObj = $(obj).parents('td').children('.foldOpen');
		$(foldObj).attr('class','foldClose');
		$(foldObj).html(' - ');
		minorTr.show();
		minorTr.find('tr').show();
		minorTr.find('[type="checkbox"]').parent('span').addClass('checked');
		minorTr.find('[type="checkbox"]').prop('checked', true);
    } else {
        minorTr.find('[type="checkbox"]').parent('span').removeClass('checked');
        minorTr.find('[type="checkbox"]').prop('checked', false);
    }

}
