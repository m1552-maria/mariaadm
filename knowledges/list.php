<div class="BannerDiv">知識平台</div>
<?
	$icons = array('doc'=>'icon_doc.gif','docx'=>'icon_doc.gif','pdf'=>'icon_pdf.gif','ppt'=>'icon_ppt.gif','pptx'=>'icon_ppt.gif',
	  'txt'=>'icon_txt.gif','xls'=>'icon_xls.gif','xlsx'=>'icon_xls.gif','zip'=>'icon_zip.gif','rar'=>'icon_rar.gif');
  include_once '../config.php';
	$ClassID = $_REQUEST[cid];
	//$ClassName = $_REQUEST[cnm];
	$rsT = db_query("select * from kwgclasses where ID like '$ClassID%'");
	while( $rT=db_fetch_array($rsT) ) $classTitle[$rT[ID]]=$rT[Title];
	
	$ID  = $_SESSION[empID];
	
	//建立上層查詢命令
	$dID = $_SESSION[depID];	
	$dIDA= explode('-',$dID);
	$n = count($dIDA);
  $whDIP = "instr(B.depPM,'".$dIDA[0].":')";	
	if(strlen($dIDA[0])>1) { $tid = substr($dIDA[0],0,1).':'; $whDIP .= " or instr(B.depPM,'$tid')"; }	
	if($n>1) { $tid = $dIDA[0].'-'.$dIDA[1].':';	$whDIP .= " or instr(B.depPM,'$tid')"; }
	if($n>2) { $tid = $dIDA[0].'-'.$dIDA[1].'-'.$dIDA[2].':'; $whDIP .= " or instr(B.depPM,'$tid')"; }
		
	$jID = $_SESSION[jobID];
	$sql = "select A.* from knowledges A left join permits B on A.PID=B.id where (PID=0 or instr(B.empPM,'$ID:') or $whDIP or instr(B.jobPM,'$jID:')) and ClassID like '$ClassID%' and beginDate<Now() and (unValidDate>Now() or unValidDate=0) and IsReady=1"; 
	if($_REQUEST[Submit] && $_REQUEST[keyword]) {
		$key = $_REQUEST[keyword];
		$sql .= " and (A.title like '%".$key."%' or SimpleText like '%".$key."%')"; 
	} 
	$sql .= " order by ClassID,Seque";
	//echo $sql;
  $rs = db_query($sql);
?>
<script>
function switchContent(elm) {
	var target = $(elm).parent().parent().next();
	target.toggle();
	if(target.css('display')=='none') $(elm).text('▼'); else $(elm).text('▲')
}
</script>
<div align="right" style="padding-right: 12px">
	<form style="margin:0">
  	<input type="hidden" name="incfn" value="<?=$_REQUEST[incfn]?>" />
    <input type="hidden" name="cid" value="<?=$_REQUEST[cid]?>" />
  	<input type="text" name="keyword" /><input type="image" src="/images/search.jpg" name="Submit" value="搜尋" align="absmiddle" />搜尋
  </form>
</div>
<div class="mainList">
  <p class="subpageTitle">
  	<span style="padding-left:10px; float:left">
		<? 
			$path = array_reverse( explode(',',getClassPath('kwgclasses',$ClassID,'ID','PID','Title')) );
			$counts = count($path);
			for($i=0; $i<$counts; $i++) {
				$itm = explode(':',$path[$i]);
				if($i==$counts-1) echo "$itm[0]";	else echo "<a href='index.php?incfn=list.php&cid=$itm[1]'>$itm[0]</a> &gt; ";
			}
		?> 
  	</span> 
  	<span style="font-size:14px; float:right; padding-right:8px; color:#cfc;">字級： 
    	<span style="font-size:13px; color:#ffc; cursor:pointer" onclick="$('.mainTable').css('font-size','13px')">小</span> 
    	<span style="font-size:15px; color:#ffc; cursor:pointer" onclick="$('.mainTable').css('font-size','15px')">中</span> 
    	<span style="font-size:17px; color:#ffc; cursor:pointer" onclick="$('.mainTable').css('font-size','17px')">大</span>  
    </span>
  </p>
  <div class="mainDiv"><table width="100%" cellpadding="4" cellspacing="1" class="mainTable">
  <tr class="maintable_Head">
    <th colspan="2">名稱</th>
    <th>內容</th><th width="50"></th><th width="90">公告日期</th><th>分類</th></tr>
  <? $curClassID = ''; $curBG = false;
	  while($r=db_fetch_array($rs)) {  
		 if(!$curClassID || $curClassID!=$r[ClassID]) {
			 $curClassID=$r[ClassID];
			 $cTitle = $classTitle[$r[ClassID]];
			 $curBG = !$curBG;
			 $bgcolor = $curBG?'#ffffff':'#f0f0f0';
		 } else $cTitle='';
	?>
     <tr bgcolor="<?=$bgcolor?>">
      <td><? if($r[Content]) { ?><span style="cursor:pointer" onclick="switchContent(this)">▼</span><? } ?></td>
      <td>
      <? if($r[filepath]) { ?>
      	<a href="/data/knowledges/<?=$r[ClassID]?>/<?=$r[filepath]?>" target="_new"><?=$r[title]?></a>
     <? } else echo $r[title] ?>   
     </td>
      <td><?=$r[SimpleText]?></td>
      <td align="center">
      <? if($r[filepath]) { ?>
				<? $ext = pathinfo($r[filepath],PATHINFO_EXTENSION); 
					 $fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; 
					 echo "<img src='/images/$fn' align='absmiddle'/>";
				?><a href="/data/knowledges/<?=$r[ClassID]?>/<?=$r[filepath]?>" target="_new">下載</a>
      <? } ?>  
      </td>
      <td align="center"><?=date('Y-m-d',strtotime($r[beginDate]))?></td>
      <td align="left"><?=$cTitle?></td>
     </tr>
     <? if($r[Content]) { ?>
     	<tr valign="top" style="display:none; background-color:#F0F0F0">
      	<td bgcolor="#FFFFFF"></td>
      	<td colspan="5"><?=$r[Content]?></td>
      </tr>
     <? } ?>
  <? } ?>
  </table></div>
</div>
<p>&nbsp;</p>