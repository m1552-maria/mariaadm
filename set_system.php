<?php
	@session_start();
	if ($_SESSION['privilege']<255) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	
	include 'inc_sys.php';
?>

<html>

<head>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>系統参數設定</title>
</head>

<body bgcolor="ffffff" leftmargin="0" topmargin="0">
	<form action="set_system_s.php" method="post" name="form1">
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="14%">&nbsp;</td>
          <td width="86%" align="right"><table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="right"><a href="news.php"></a><a href="news_add.php"></a></td>
              </tr>
          </table></td>
        </tr>
      </table>
    <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td width="18%" height="25" align="right" valign="middle"><font color="#666666" size="2">系統名稱：</font></td>
          <td width="82%"><input name="gSys_Title" type="text" value="<?=$gSys_Title?>" size="100"></td>
        </tr>
        <tr>
          <td width="18%" height="25" align="right" valign="middle"><font color="#666666" size="2">主選單：</font></td>
          <td width="82%"><input name="gSys_Menu" type="text" value="<?=$gSys_Menu?>" size="100"></td>
        </tr>
        <tr>
          <td width="18%" height="25" align="right" valign="middle"><font color="#666666" size="2">主選單連結：</font></td>
          <td width="82%"><input name="gSys_MenuLink" type="text" value="<?=$gSys_MenuLink?>" size="100"></td>
        </tr>
        
        <tr>
          <td width="18%" height="25" align="right" valign="middle"><font color="#666666" size="2">網站Title：</font></td>
          <td width="82%"><input name="gHTML_Title" type="text" value="<?=$gHTML_Title?>" size="100"></td>
        </tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">服務信箱：</font></td>
          <td><input name="gServiceMail" type="text" value="<?=$gServiceMail?>" size="50"></td>
        </tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">服務名稱：</font></td>
          <td><input name="gServiceMailName" type="text" value="<?=$gServiceMailName?>"></td>
        </tr>
        
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">服務電話：</font></td>
          <td><input name="gTEL" type="text" value="<?=$gTEL?>"></td>
        </tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">傳真：</font></td>
          <td><input name="gFax" type="text" value="<?=$gFax?>"></td>
        </tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">地址：</font></td>
          <td><input name="gAdress" type="text" value="<?=$gAdress?>" size="50"></td>
      	</tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">郵政劃撥帳號：</font></td>
          <td><input name="gPostAct" type="text" value="<?=$gPostAct?>"></td>
        </tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">戶名：</font></td>
          <td><input name="gPostName" type="text" value="<?=$gPostName?>" size="50"></td>
      	</tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">最新公告則數：</font></td>
          <td><input name="gBulletCt" type="text" value="<?=$gBulletCt?>"></td>
      	</tr>
        <tr>
          <td height="25" align="right" valign="middle"><font color="#666666" size="2">最新消息則數：</font></td>
          <td><input name="gNewsCt" type="text" value="<?=$gNewsCt?>"></td>
      	</tr>                
        
        <tr><td>&nbsp;</td><td><input type="submit" value="儲存"></td></tr>
    </table>
  </form>

</body>
</html>