// JavaScript Document
CKEDITOR.addTemplates( 'default',
{
	// The name of sub folder which hold the shortcut preview images of the templates.
	imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates :
		[
			{
				title: '樣式 1',
				image: 'template1.gif',
				description: 'Description of my template 1.',
				html:
					'<h2>Template 1</h2>' +
					'<p><img src="/logo.png" style="float:left" />Type the text here.</p>'
			},
			{
				title: '樣式 2',
				html:
					'<h3>Template 2</h3>' +
					'<p>Type the text here.</p>'
			}
		]
});
