<?php
 if (!function_exists('file_put_contents')) {
    defined('FILE_APPEND') || define('FILE_APPEND','FILE_APPEND');
    defined('FILE_USE_INCLUDE_PATH') || define('FILE_USE_INCLUDE_PATH','FILE_USE_INCLUDE_PATH');
    function file_put_contents($filename, $data, $flag = '') {
        $flag = explode('|', $flag);
        $f_flag = in_array(FILE_APPEND, $flag) ? 'ab+' : 'wb+';
        $f_uip = in_array(FILE_USE_INCLUDE_PATH, $flag) ? true : false;

        if ($handle = fopen($filename, $f_flag, $f_uip))  {
             if (in_array(LOCK_EX, $flag)) {
                flock($handle,LOCK_EX);
             }
             if (is_resource($data)) {
                $contents = '';
                 while (!feof($data)) {
                    $contents .= fread($data, 1024);
                 }
                $data = $contents;
             } elseif (is_array($data)) {
                $data = implode('',$data);
             }
            fwrite($handle,$data);
            fclose($handle);
            return true;
         } else {
            return false;
         }
     }
 }

 $fn = $_REQUEST[fn];
 if($_POST[editor1]) {
	 //echo str_replace("\\","",$_POST[editor1]); 
	 header("Content-type: text/html; charset=utf-8"); 
	 file_put_contents($fn,$_POST[editor1]);
 	 echo $_POST[editor1];  exit;
 }
 $ct = file_get_contents($fn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>網頁編輯</title>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	var editor;
	window.onload = function() {	if(!editor)editor = CKEDITOR.replace('editor1',{customConfig :'/ckeditor/config.js'}); }
	CKEDITOR.on('instanceReady', function( evt ) {  var editor = evt.editor;  editor.execCommand('maximize');  });
</script>
</head>

<body>
<form id="form1" method="post" action="" enctype="application/x-www-form-urlencoded">
  <textarea cols="80" id="editor1" name="editor1" rows="10"><?=$ct?></textarea>
</form> 
</body>
</html>