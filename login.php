﻿<?php
	@session_start();
	date_default_timezone_set('Asia/Taipei');
  include("config_pdo.php");
	
  if ($_REQUEST["Submit"]>"") {
		$act = $_REQUEST['empID'];
		$pwd = $_REQUEST['password'];
    $pwd = getRulPwd($act, $pwd);
    $sql = "SELECT empName,A.depID,jobID,jobLevel,canProject,canSchedule,B.depName,canEditEmp FROM emplyee A,department B where A.depID=B.depID and empID=? and password=SHA2(?, 256) and isOnduty=1";
    $rs = db_query($sql,array($act,$pwd));
		if (!db_eof($rs)) {
			/* login Log
		  $dstr = date('Y-m-d H:i:s');
		  db_query("insert into logs(actor,action,rdate) values('$act','login','$dstr')",$conn); */
		  $r = db_fetch_array($rs);
			if ($r) {
				session_unset();										//防止由後台直接過來
				$_SESSION["empName"] = $r[0];
	      $_SESSION["empID"] = nf_to_wf(trim($act));//全形轉半形
	      $_SESSION["depID"] = $r[1];
				$_SESSION["depTL"] = $r['depName'];
				$_SESSION["jobID"] = $r[2];
				$_SESSION["loginTm"] = date("H:i:s");
				$_SESSION["canProject"] = $r[canProject];
				$_SESSION["canSchedule"] = $r[canSchedule];
				$_SESSION["canEditEmp"] = $r[canEditEmp];
				//if($r[jobLevel]=='主管') {
					$_SESSION['domain_Host']=$r[1];		//主管
					$_SESSION['privilege']=10;				//主管後臺權限
				//} else $_SESSION['privilege']=1;		//員工後臺權限
				//::取的權限設定
				$str = file_get_contents("maintainUser/previege/".$_SESSION["empID"].".txt");
				if($str) $priviege = json_decode($str,true); else $priviege=array(); 
				$str = file_get_contents('maintainUser/previege/'.$_SESSION["empID"].'_class.txt');
				if($str) $classdef = json_decode($str); else $classdef=array();
				$_SESSION['user_priviege'] = $priviege;
				$_SESSION['user_classdef'] = $classdef;
	
				header("Location: index.php"); exit;
			}	else $ErrMsg = "Your Account is not open yet !";
		} else $ErrMsg = "帳號 或 密碼錯誤, 請確認後重新輸入！";
	}

  function nf_to_wf($strs, $types){  //全形半形轉換
      $nft = array(
          "(", ")", "[", "]", "{", "}", ".", ",", ";", ":",
          "-", "?", "!", "@", "#", "$", "%", "&", "|", "\\",
          "/", "+", "=", "*", "~", "`", "'", "\"", "<", ">",
          "^", "_",
          "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
          "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
          "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
          "u", "v", "w", "x", "y", "z",
          "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
          "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
          "U", "V", "W", "X", "Y", "Z",
          " "
      );
      $wft = array(
          "（", "）", "〔", "〕", "｛", "｝", "﹒", "，", "；", "：",
          "－", "？", "！", "＠", "＃", "＄", "％", "＆", "｜", "＼",
          "／", "＋", "＝", "＊", "～", "、", "、", "＂", "＜", "＞",
          "︿", "＿",
          "０", "１", "２", "３", "４", "５", "６", "７", "８", "９",
          "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ",
          "ｋ", "ｌ", "ｍ", "ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ",
          "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ",
          "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ",
          "Ｋ", "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ",
          "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ",
          "　"
      );
      if ($types == '1'){ // 轉全形
        $strtmp = str_replace($nft, $wft, $strs);
      }else{ // 轉半形
        $strtmp = str_replace($wft, $nft, $strs);
      }
      return $strtmp;
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登入系統</title>
<link href="css/index.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { background-color: #f3cac4 }
.hint { font-size:11px; color:#AAA }
.loginfm {
	font-family: "微軟正黑體", Verdana;
	font-size: 16px;
	background-color: #f3cac4;
}
.loginbtn {
	font-family: "微軟正黑體", Verdana;
	font-size: 16px;
}
-->
</style>
<script>
function formvalid(tfm) {
	if(!tfm.password.value) {
		alert('請輸入登入密碼!');
		tfm.password.focus();
		return false;
	}
	
	return true;
}
window.onload = function() {<? if($ErrMsg) echo "alert('$ErrMsg');" ?>}
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td width="1000" class="loginfm" >
    	<!-- 頁面本體 ↓-->
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="150" height="100">&nbsp;</td>
          <td height="100">&nbsp;</td>
          <td width="150" height="100">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td background="pan/loginfm/pan_01.gif" width="29" height="27"></td>
              <td background="pan/loginfm/pan_02.gif"></td>
              <td background="pan/loginfm/pan_03.gif" width="46"></td>
            </tr>
            <tr>
              <td background="pan/loginfm/pan_04.gif"></td>
              <td bgcolor="#f9e6e2">
              <form method="post" id="fmLogin" onsubmit="return formvalid(this)">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td rowspan="5"><img src="pan/loginfm/pic1.jpg" width="313" height="159" /></td>
                  <td align="right">員工編號：</td>
                  <td><input type="text" name="empID" id="empID" /></td>
                </tr>
                <tr><td>&nbsp;</td><td><span class="hint">請輸入5位數字的員工編號，如:00015</span></td></tr>
                <tr>
                  <td align="right">登入密碼：</td>
                  <td><input type="password" name="password" id="password" /></td>
                </tr>
                <tr><td>&nbsp;</td><td><span class="hint">預設為個人身分證字號，若欲變更密碼請至<br />[個人資訊]專區</span></td></tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                  	<input name="Submit" type="submit" class="loginbtn" id="Submit" value="送出" />
                    <input name="button2" type="reset" class="loginbtn" id="button2" value="重設" />
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
              </table>
              </form>
              </td>
              <td background="pan/loginfm/pan_06.gif"></td>
            </tr>
            <tr>
              <td background="pan/loginfm/pan_07.gif" height="39"></td>
              <td background="pan/loginfm/pan_08.gif"></td>
              <td background="pan/loginfm/pan_09.jpg"></td>
            </tr>
          </table></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td height="150">&nbsp;</td>
          <td height="150" align="center"><p>建議使用 Chrome 瀏覽器，以便使用最佳效果</p></td>
          <td height="150">&nbsp;</td>
        </tr>
      </table>
      <!-- 頁面本體 ↑-->
    </td>  
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>