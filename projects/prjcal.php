<?
	$sql = "select * from schedule_project where ClassID='$_SESSION[prjID]'";	$rs = db_query($sql);
	include 'menu2.php';
?>
<script>
	Accordion1.openPanel(4);
	
	$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {left:'prev,next today', center:'title', right:'month,agendaWeek,agendaDay'},
			editable: false,
			buttonText : { today: '今日',month: '月',agendaWeek: '周',	agendaDay: '日'	},
			monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			dayNames : ['周日','週一','週二','週三','週四','週五','週六'],
			dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],
			//eventClick: function(e){ alert(e); },
			//dayClick:function(dayDate, allDay, jsEvent, view){      //点击单元格事件   
      //  alert(dayDate.getFullYear()+"-"+dayDate.getMonth()+"-"+dayDate.getDate()+"-星期:"+dayDate.getDay()+ "视图:"+view.name);   
        //$('#calendar').fullCalendar("incrementDate", -1, 0, 0); //移动日历 后三个参数为 年 月 日   
        //$('#calendar').fullCalendar("prev"); // 跳转到前一月/周/天, 根据当前的view决定   
        //$('#calendar').fullCalendar("next"); // 跳转到下一月/周/天, 根据当前的view决定   
        //$('#calendar').fullCalendar("today"); // 跳转今天   
        //$('#calendar').fullCalendar("gotoDate", year[, month[, date]]); //跳转到指定的日期   
        //$('#calendar').fullCalendar("changeView","viewName"); //切换日历的view, viewName必须是允许的views   
    	//}, //	eventColor: '#336699', //變更預設顏色
			events: [ <? 
			$evtlst = array();
			while($r=db_fetch_array($rs)) {
				 $bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				 $eD = date('Y/m/d H:i',strtotime($r[endDate]));
				 $evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false}";
			} 
			echo join(',',$evtlst);
			?>
			]
		});
	});	
</script>
<link rel='stylesheet' type='text/css' href='/scripts/fullcalendar/fullcalendar.css' />
<script type='text/javascript' src='/scripts/jquery-ui-1.7.2.custom.min.js'></script>
<script type='text/javascript' src='/scripts/fullcalendar/fullcalendar.min.js'></script>

<div class="pageTitle">專案行事曆</div>	
<div id='calendar'></div>

