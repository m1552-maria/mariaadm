<?php
	include '../getEmplyeeInfo.php';
	include '../maintain/inc_vars.php';
	$prjID = $_SESSION['prjID'];
	$sql = "select * from projects where id=$prjID";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
	include 'menu2.php';
?>
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
function addItem(tbtn,fld) {
	var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6'>"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	if(!tfm.title.value) { alert('專案名稱不能為空白！'); tfm.title.focus(); return false; }
	if(!tfm.bDate.value) { alert('起始日期不能為空白！'); tfm.bDate.focus(); return false; }

	return true;
}
function set_type(elm) {
	if(elm.selectedIndex>0) $('#tyName').val(elm.options[elm.selectedIndex].text);
}
function set_status(elm) {
	if(elm.selectedIndex>0) $('#tyState').val(elm.options[elm.selectedIndex].text);
}
</script>
<form id="prjform" action="editprojectDo.php" method="post" onsubmit="return formCheck(this)">
<table width="100%" border="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td width="100" align="right">專案名稱：</td>
    <td colspan="3"><input name="title" type="text" id="title" size="60" value="<?=$r[title]?>"/></td>
  </tr>
  <tr>
    <td width="100" align="right">專案類別：</td>
    <td><input type="text" name="tyName" id="tyName" value="<?=$r[tyName]?>"/>
    	<select name="seltype" id="seltype" onchange="set_type(this)">
      <option>-- 選擇 --</option><? foreach($projClass as $v) echo "<option>$v</option>"; ?>
      </select>
    </td>
    <td width="140" align="right">計畫年度：</td>
    <td><input name="actYear" type="text" id="actYear" value="<?=$r[actYear]?>" size="10" maxlength="10"/></td>
  </tr>
  <tr>
    <td width="100" align="right">起始日期：</td>
    <td><input type="text" name="bDate" id="bDate" class="queryDate" value="<?=$r[bDate]?>"/></td>
    <td width="140" align="right">結束日期：</td>
    <td><input type="text" name="eDate" id="eDate" class="queryDate" value="<?=$r[eDate]?>"/></td>
  </tr>
  <tr>
    <td width="100" align="right">專案成員：</td>
    <td colspan="3">
	  <? $aa = explode(',',$r[PID]) ?>
  	<input name="empID[]" type="text" class="queryID" id="empID" size="6" value="<?=$aa[0]?>" title="<?=$emplyeeinfo[$aa[0]]?>"/> <span class="btn" onClick="addItem(this,'empID')"> +新增</span>
    <? for($i=1; $i<count($aa); $i++) {
			 echo "<div><input name='empID[]' id='empID' type='text' size='6' value='$aa[$i]'>"
			 . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$emplyeeinfo[$aa[$i]]."</span>"
			 . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		} ?>          
    </td>
  </tr>
	<tr>
    <td width="100" align="right">內容說明：</td>
    <td colspan="3"><textarea name="description" id="description" cols="60" rows="5" class='ckeditor'><?=$r[description]?></textarea></td>
  </tr>
	<tr>
    <td width="100" align="right">方案性質：</td>
    <td colspan="3"><textarea name="prjText" id="prjText" cols="60" rows="5" class='ckeditor'><?=$r[prjText]?></textarea></td>
  </tr>  
  <tr>
    <td width="100" align="right">主辦單位：</td>
    <td><input type="text" name="mainUnit" id="mainUnit" value="<?=$r[mainUnit]?>"/></td>
    <td width="140" align="right">協(承)辦單位：</td>
    <td><input type="text" name="subUnit" id="subUnit" value="<?=$r[subUnit]?>"/></td>
  </tr>
  <tr>
    <td width="100" align="right">辦理地點：</td>
    <td><input type="text" name="Place" id="Place" value="<?=$r[Place]?>"/></td>
    <td width="140" align="right">參加對象：</td>
    <td><input type="text" name="prjObject" id="prjObject" value="<?=$r[prjObject]?>"/></td>
  </tr>
  <tr>
    <td width="100" align="right">使用經費：</td>
    <td><input type="text" name="fund" id="fund" value="<?=$r[fund]?>"/></td>
    <td width="140" align="right">經費來源：</td>
    <td><input type="text" name="fundSource" id="fundSource" value="<?=$r[fundSource]?>"/></td>
  </tr>
  <tr>
    <td width="100" align="right">補助金額：</td>
    <td><input type="text" name="allowance" id="allowance" value="<?=$r[allowance]?>"/></td>
    <td width="140" align="right">預算金額：</td>
    <td><input type="text" name="budget" id="budget" value="<?=$r[budget]?>"/></td>
  </tr>
  <tr>
    <td width="100" align="right">預算計劃編號：</td>
    <td><input type="text" name="prjPlanID" id="prjPlanID" value="<?=$r[prjPlanID]?>"/></td>
    <td width="140" align="right">受益人數(次)及效益：</td>
    <td><input type="text" name="benefit" id="benefit" value="<?=$r[benefit]?>"/></td>
  </tr>
  <tr>
    <td width="100" align="right">是否逾期核銷：</td>
    <td><input name="isCheckDelay" type="checkbox" id="isCheckDelay" value="1" <?=$r[isCheckDelay]?'checked':''?>/></td>
    <td width="140" align="right">核銷金額核銷日期：</td>
    <td><input type="text" name="checkDate" id="checkDate" value="<?=$r[checkDate]?>" class="queryDate"/></td>
  </tr>  
  <tr>
    <td width="100" align="right">專案狀態：</td>
    <td colspan="3"><input type="text" name="tyState" id="tyState" value="<?=$r[status]?>"/>
    	<select name="selstatus" id="selstatus" onchange="set_status(this)">
      <option>-- 選擇 --</option><? foreach($projState as $v) echo "<option>$v</option>"; ?>
      </select>
    </td>
  </tr>  
  
  <tr>
    <td>&nbsp;</td>
    <td colspan="3">
    	<input type="hidden" name="id" value="<?=$r[id]?>" />
    	<input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" />
    </td>
  </tr>
</table>
</form>