<style>
/*  主選單 */
.menuBar { background-color:#5C0A10;	font: 19px '微軟正黑體',Verdana;	color:#FFFFE0 }
#mainmenu a { text-decoration:none }
#mainmenu a:link { color:#FFFFE0 }
#mainmenu a:visited { color:#FFFFE0 }
#mainmenu a:hover { color:#FFFFE0 }
#mainmenu a:active { color:#FFFFE0 }
.foot {	font:12px "微軟正黑體",Verdana;	color: #CCC; background-color: #7B0C13;	padding: 5px; }

/*  次選單 */
.subpageTitle {
	font-family: "微軟正黑體", Verdana;
	font-size: 16px;
	height: 21px;
	font-weight: bold;
	background-color:#8A4822;
	color:#FFFFFF;
	padding: 4px 0 4px 0;
	margin-bottom: 0;
}
.subpageTitle a { text-decoration:none }
.subpageTitle a:link { color:#FFF }
.subpageTitle a:visited { color:#FFF }
.subpageTitle a:hover { color:#FF0 }
.subpageTitle a:active { color:#FFF }

.mainDiv a {text-decoration:none }
.BannerDiv {
	font:bold 30px "微軟正黑體";
	color:#76161B;
}

.mainList { 
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px; 
	border-radius: 20px;
	border:dashed 1px #910048;
	width:733px;
}
.mainTable { font:13px "微軟正黑體"; 	}
.mainDiv { 
	padding: 0px 6px 6px 6px; 
}

.maintable_Head {
	font-size:110%;
	background-color:#f5d0be;
}
</style>
<script type="text/javascript" src="../Scripts/jquery.corner.js"></script>
