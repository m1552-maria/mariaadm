<?
	$self = $_SERVER['PHP_SELF']."?incfn=mymsg.php";
	$prjID = $_SESSION['prjID'];
	include 'menu2.php';
?>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
function formCheck(tfm) {
	if(!tfm.title.value) { alert('訊息主旨 不能為空白！'); tfm.title.focus(); return false; }
	if(!tfm.unValidDate.value) { alert('失效時間 不能為空白！'); tfm.unValidDate.focus(); return false; }
	if(!tfm.Content.value) { alert('訊息內容 不能為空白！'); tfm.Content.focus(); return false; }
	return true;
}
</script>

<div class="pageTitle">發佈新的訊息</div>	
<form action="newmsgdo.php" method="post" enctype="multipart/form-data" onsubmit="return formCheck(this)">
<table class="sTable" width="100%" cellpadding="4" cellspacing="1">
	<tr><td width="90" class="hdCell">訊息主旨：</td><td><input type="text" name="title" size="60" /></td></tr>
  <tr><td class="hdCell">失效時間：</td><td><input type="text" name="unValidDate" class="queryDate" /></td></tr>
	<tr><td class="hdCell">訊息內容：</td><td><textarea name="Content" cols="60" rows="5"></textarea></td></tr>
  <tr><td class="hdCell">訊息附件：</td><td><input type="file" name="filepath" /></td></tr>
  <tr><td class="hdCell">通知方式：</td><td>
    <label>
      <input name="notifies[]" type="checkbox" id="notifies[]" value="1" checked="checked"/>
      系統訊息</label>
    <br />
    <label>
      <input type="checkbox" name="notifies[]" value="2" id="notifies[]" />
      Email通知</label>
    <br />
    <!--label><input type="checkbox" name="CheckboxGroup1" value="核取方塊" id="CheckboxGroup1_2" />手機簡訊</label-->
  </td></tr>
  <tr><td class="hdCell">&nbsp;</td><td>
  	<input type="hidden" name="ClassID" value="<?=$prjID?>" />
    <input type="hidden" name="announcer" value="<?=$_SESSION[empID]?>" />
  	<input type="submit" name="Submit" value="確定發佈" /> 
    <input type="reset" value="清除重設" />
  </td></tr>
</table>
</form>