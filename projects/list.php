<link rel="stylesheet" type="text/css" href="/Scripts/themes/flora/flora.resizable.css">
<style>
#newDoc {
	background-color:#F0F0F0;
}
</style>
<script type="text/javascript" src="/Scripts/ui/ui.core.js"></script>
<script type="text/javascript" src="/Scripts/ui/ui.dialog.js"></script>
<script type="text/javascript" src="/Scripts/ui/ui.draggable.js"></script>
<script type="text/javascript" src="/Scripts/ui/ui.resizable.js"></script>  
<script>
$(function(){ 
	var dragop = { 
	  autoOpen: false,
    modal: true,
    width: 480,
    height: 260,
    overlay: { backgroundColor: "#000", opacity: 0.5 }
	};
	
	$('#newDoc').dialog(dragop);
});
	
function newDocFormCheck(tfm) {
	if(!tfm.title.value) { alert('名稱不能為空白！'); tfm.title.focus(); return false; }
	return true;
}
</script>
<?
	$icons = array('doc'=>'icon_doc.gif','docx'=>'icon_doc.gif','pdf'=>'icon_pdf.gif','ppt'=>'icon_ppt.gif','pptx'=>'icon_ppt.gif',
	  'txt'=>'icon_txt.gif','xls'=>'icon_xls.gif','xlsx'=>'icon_xls.gif','zip'=>'icon_zip.gif','rar'=>'icon_rar.gif');	
  include_once '../config.php';
	include '../maintain/inc_vars.php';		
	$ClassID = $_SESSION['prjID'];
	$sql = "select * from documents_prj where ClassID ='$ClassID' order by docClass"; 
  $rs = db_query($sql);
	include 'menu2.php';
?>
<br />
<div class="mainList">
	<p class="subpageTitle">
  	<span style="padding-left:10px; float:left">專案文件</span> 
    <span onclick="$('#newDoc').dialog('open');" style="float:right; color:#69F; font:bold 15px '微軟正黑體'; padding-right:12px"><img src="upload.png" align="absmiddle" /> 上傳文件</span>
  </p>
  <div class="mainDiv"><table width="100%" cellpadding="4" cellspacing="1" class="mainTable">
  <tr class="maintable_Head"><th width="100">發佈日期</th><th width="50">分類</th><th width="150">標題</th><th>説明</th><th width="50"></th></tr>
  <? while($r=db_fetch_array($rs)) {  ?>
     <tr>
      <td align="center"><?=date('Y-m-d',strtotime($r[R_Date]))?></td>
      <td><?=$r[docClass]?></td>
      <td><a href="/data/projects/<?=$r[ClassID]?>/<?=$r[filepath]?>" target="_new"><?=$r[title]?></a></td>
      <td><?=$r[SimpleText]?></td>
      <td align="center">
				<? $ext = pathinfo($r[filepath],PATHINFO_EXTENSION); 
					 $fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; 
					 echo "<img src='/images/$fn' align='absmiddle'/>";
				?>
        <a href="/data/projects/<?=$r[ClassID]?>/<?=$r[filepath]?>" target="_new">下載</a> &nbsp;
        <a href="delfn.php?ID=<?=$r[ID]?>&fn=<?=$r[ClassID]?>/<?=$r[filepath]?>"><img src="/images/delete.png" border="0" align="absmiddle" /></a>
      </td>
     </tr>
  <? } ?>
  </table></div>
</div>

<div id="newDoc" title="上傳文件">
<form id="newDocForm" action="newdocDo.php" method="post" enctype="multipart/form-data" onsubmit="return newDocFormCheck(this)">
	<table style="font-size:13px">
  	<tr><td align="right">名稱:</td><td><input name="title" type="text" id="title" size="60" /></td></tr>
    <tr><td align="right">分類:</td><td><select name="docClass"><? foreach($projDocClass as $v) echo "<option>$v</option>"; ?></select></td></tr>
    <tr><td align="right">簡述:</td><td><textarea name="SimpleText" id="SimpleText" cols="45" rows="5"></textarea></td></tr>
    <tr><td>上傳檔案:</td><td><input name="filepath" type="file" id="filepath" size="50" /></td></tr>
    <tr><td></td><td><input name="Submit" type="submit" id="Submit" value="確定上傳" /></td></tr>
  </table>
</form>
</div>