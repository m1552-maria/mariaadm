<?
	include_once("../config.php");
	include_once('../miclib.php');	
	$extfiles = 'schedule/';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../data/jobs/';
	$imgpath = '../data/jobs/';
	
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
 <script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 新增</title>
</Head>

<body class="page">
<form action="schedule/doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>

	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><? switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  		case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "datetime": echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; 
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datetimepicker();})</script>";
				break;	
			case "select"  : echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $v) echo "<option>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>
	
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
    	<input type="hidden" value="<?=$_SESSION['prjID']?>" name="ClassID">
			<input type="submit" value="確定新增" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
      <input type="button" value="取消新增" class="btn" onClick="history.back()">
		</td>
	</tr>
</table>
</form>
</body>
</html>
