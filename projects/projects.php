<?php
	//:: 專案概況
	include '../getEmplyeeInfo.php';
	
	if($_REQUEST['pjid']) {
		$prjID = $_REQUEST['pjid'];
		$_SESSION['prjID'] = 	$prjID;
	} else if($_SESSION['prjID']) {
		$prjID = $_SESSION['prjID'];
	} else $prjID = 0;
	
	$sql = "select * from projects where id=$prjID";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
	$_SESSION['prjTitle'] = $r[title];
	$_SESSION['prjState'] = $r[status];
	include 'menu2.php';
?>
<style>
#prjtbl { 
	font-family:"微軟正黑體", Verdana;
	font-size:14px; 
}
.prjtblHead { background-color: #f0f0f0;
	
}
</style>
<table id="prjtbl" border="0" width="100%" cellpadding="4" cellspacing="1">
  <tr>
    <td width="100" align="right" class="prjtblHead">專案名稱：</td>
    <td align="left"><?=$r[title]?></td>
    <td align="right" class="prjtblHead">計畫年度：</td>
    <td align="left"><?=$r[actYear]?></td>
  </tr>
  <tr>
    <td width="100" align="right" class="prjtblHead">專案類別：</td>
    <td align="left"><?=$r[tyName]?></td>
    <td width="140" align="right" class="prjtblHead">負責人：</td>
    <td align="left"><?=$emplyeeinfo[$r[leadman]]?></td>
  </tr>
  <tr>
    <td width="100" align="right" class="prjtblHead">起始日期：</td>
    <td align="left"><?=$r[bDate]?></td>
    <td width="140" align="right" class="prjtblHead">結束日期：</td>
    <td align="left"><?=$r[eDate]?></td>
  </tr>
  <tr>
    <td width="100" align="right" class="prjtblHead">專案成員：</td>
    <td colspan="3" align="left"><? $aa=explode(',',$r[PID]); foreach($aa as $v) echo $emplyeeinfo[$v].' '; ?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">內容說明：</td>
    <td colspan="3" align="left"><?=$r[description]?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">方案性質：</td>
    <td colspan="3" align="left"><?=$r[prjText]?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">主辦單位：</td>
    <td align="left"><?=$r[mainUnit]?></td>
    <td width="140" align="right" class="prjtblHead">協(承)辦單位：</td>
    <td align="left"><?=$r[subUnit]?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">辦理地點：</td>
    <td align="left"><?=$r[Place]?></td>
    <td width="140" align="right" class="prjtblHead">參加對象：</td>
    <td align="left"><?=$r[prjObject]?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">使用經費：</td>
    <td align="left"><?=$r[fund]?></td>
    <td width="140" align="right" class="prjtblHead">經費來源：</td>
    <td align="left"><?=$r[fundSource]?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">補助金額：</td>
    <td align="left"><?=$r[allowance]?></td>
    <td width="140" align="right" class="prjtblHead">預算金額：</td>
    <td align="left"><?=$r[budget]?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">預算計劃編號：</td>
    <td align="left"><?=$r[prjPlanID]?></td>
    <td width="140" align="right" class="prjtblHead">受益人數(次)及效益：</td>
    <td align="left"><?=$r[benefit]?></td>
  </tr>
  <tr>
    <td width="100" align="right" valign="top" class="prjtblHead">是否逾期核銷：</td>
    <td align="left"><img src="<?=$r[isCheckDelay]?'checked.gif':'uncheck.gif'?>" /></td>
    <td width="140" align="right" class="prjtblHead">核銷金額核銷日期：</td>
    <td align="left"><?=$r[checkDate]?></td>
  </tr>
</table>
