<?	
	// for Access Permission Control
	session_start();

	include("../../config.php");
	include '../../getEmplyeeInfo.php';
				
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "授權資料維護";
	$tableName = "permits_emplyee";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/jobs/';
	$delField = 'ahead';
	$delFlag = true;
	$imgpath = '../../data/jobs/';
	
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "crman";
	$searchField2 = "title";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,empPM,depPM,jobPM,rdate,crman,upman");
	$ftAry = explode(',',"編號,授權名稱,員工授權,部門授權,職掌授權,登錄時間,建立者,異動者");
	$flAry = explode(',',"60,150,,,,80,60,60");
	$ftyAy = explode(',',"ID,text,empPM,depPM,jobPM,date,text,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"title,crman");
	$newftA = explode(',',"授權名稱,建立者");
	$newflA = explode(',',"60,");
	$newfhA = explode(',',",");
	$newfvA = array('',"$_SESSION[Account]");
	$newetA = explode(',',"text,hidden");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title");
	$editftA = explode(',',"編號,授權名稱");
	$editflA = explode(',',",60");
	$editfhA = explode(',',",");
	$editetA = explode(',',"text,text");
?>