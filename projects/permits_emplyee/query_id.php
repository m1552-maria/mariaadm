<?php	
	include("../../config.php");
	include "../../getEmplyeeInfo.php";
	$tableName = "permits_emplyee";		
  $rs = db_query("select * from $tableName",$conn);
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font:13px "微軟正黑體",Verdana;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
  </style>
  <script type="text/javascript" src="/Scripts/jquery.js"></script>    
  <script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		$(document).ready(function(){
			selid = dialogArguments;
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
			
	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				window.returnValue = [id,title]; 
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}
		function switchTab() {
			if($('#newPerMit').css('display')=='none') {
				$('#newPerMit').css('display','block');
				$('#browser').css('display','none');
				$('#lbbtn1').text('-返回');
			} else {
				$('#newPerMit').css('display','none');
				$('#browser').css('display','block');
				$('#lbbtn1').text('+新增');
			}
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table width="100%" border="0" bgcolor="#666666" cellpadding="4" cellspacing="0" style="color:#FFF; font-size:14px">
  <tr>
  	<th width="80">[ <span id="lbbtn1" onClick="switchTab()">+新增</span> ]</th>
  	<th>::: 授權代碼表 :::</th>
  </tr>
</table>
<table id="browser" border="0" cellpadding="4" cellspacing="1" bgcolor="#333333" width="100%" class="whitebg">
  <tr bgcolor="#999999"><th colspan="2">授權名稱</th><th>員工授權</th><th>部門授權</th><th>職掌授權</th></tr>
  <? while($r=db_fetch_array($rs)) { ?>
  	<tr>
    	<td><img src='/images/key.png'></td>
      <td><a onClick=setID('<?=$r[id]?>','<?=$r[title]?>')><?=$r[title]?></a></td>
      <td><? $lstA=array();	$AA=explode(';',$r[empPM]); foreach($AA as $v){$AAA=explode(':',$v); $lstA[]=$emplyeeinfo[$AAA[0]];} echo join(',',$lstA); ?></td>
      <td><? $lstA=array();	$AA=explode(';',$r[depPM]); foreach($AA as $v){$AAA=explode(':',$v); $lstA[]=$departmentinfo[$AAA[0]];} echo join(',',$lstA); ?></td>
      <td><? $lstA=array();	$AA=explode(';',$r[jobPM]); foreach($AA as $v){$AAA=explode(':',$v); $lstA[]=$jobinfo[$AAA[0]];} echo join(',',$lstA); ?></td>
    </tr>
  <? } ?> 
</table>
<iframe id="newPerMit" src="qrappend.php" width="100%" height="422" frameborder="0" style="display:none"></iframe>
</body>
</html>