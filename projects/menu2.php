<!-- Toolbar for this Project -->
<script>
$(document).ready(function(e) {
  $(window).bind('resize',resizeHandler);
	setTimeout(resizeHandler,250);
});

function resizeHandler() {
	var pos = $('#btnMain').offset();
	pos.top += 23;
	$('#dropdown1')[0].style.left = pos.left+'px';
	$('#dropdown1')[0].style.top = pos.top+'px';
	
	var pos = $('#btnMain2').offset();
	pos.top += 23;
	$('#dropdown2')[0].style.left = pos.left+'px';
	$('#dropdown2')[0].style.top = pos.top+'px';
}
function swapPulldown(tMenu) { $('#dropdown1').toggle(); }
function swapPulldown2(tMenu) { $('#dropdown2').toggle(); }
</script>
<style>
#dropdown1 { 
	position:absolute;
	display:none;
	font:normal 15px "微軟正黑體";
	background-color:#DDD; 
	border: groove #AAA 1px;
	padding: 4px 8px 4px 8px;
	width: 140px;
	z-index: 999;
}
#dropdown2 { 
	position:absolute;
	display:none;
	font:normal 15px "微軟正黑體";
	background-color:#DDD; 
	border: groove #AAA 1px;
	padding: 4px 8px 4px 8px;
	z-index: 999;
}
#dropdown1 ul,#dropdown2 ul {
	margin: 0;
	padding: 0 16px 0 16px;
	font:normal 15px "微軟正黑體";
	line-height: 1.5em;
}
#dropdown1 ul a,#dropdown2 ul a {	text-decoration:none; }
#menuTbl { 
	font:bold 16px "微軟正黑體";
	background-color:#F5D0BE;
}
.menuTbl_hd {
	font:bold 18px "微軟正黑體";
	background-color:#8A4822;
	border:solid 1 #69381B;
	color: #FFFFFF;
}
#menuTbl a { text-decoration:none; }
</style>
<table width="100%" id="menuTbl" cellpadding="0" cellspacing="0">
<tr><td colspan="9" class="menuTbl_hd"><img src="../images/<?=$_SESSION['prjState']?>.png" align="absmiddle"/> <?=$_SESSION['prjTitle'].' ('.$_SESSION['prjState'].')' ?></td></tr>
<tr>
	<th><a href='index.php?incfn=projects.php'>專案計畫</a></th>
  <td bgcolor="#ffffff" width="2"></td>
  <th><a href='index.php?incfn=list.php'>專案文件</a></th>
  <td bgcolor="#ffffff" width="1"></td>
  <th id="btnMain2" onclick="swapPulldown2(this)">專案資訊分享 ▼</th>
  <td bgcolor="#ffffff" width="1"></td>
  <th><a href='index.php?incfn=prjcal.php'>專案行事曆</a></th>
  <td bgcolor="#ffffff" width="1"></td>
  <th id="btnMain" onclick="swapPulldown(this)">系統維護 ▼</th>
</tr>
</table>
<div style="width:100%; height:8px"></div>
<div id="dropdown1">
<ul>
  <li><a href="index.php?incfn=editproject.php">維護專案內容</a></li>
	<li><a href="index.php?incfn=schedule/list.php">維護行事歷</a></li>
</ul>
</div>
<div id="dropdown2">
<ul>
  <li><a href="index.php?incfn=unreadmsg.php">尚未讀取的訊息</a></li>
  <li><a href="index.php?incfn=readedmsg.php">已經讀取的訊息</a></li>
  <li><a href="index.php?incfn=mymsg.php">我發佈的訊息</a></li>
  <li><a href="index.php?incfn=newmsg.php">發佈新的訊息</a></li>  
</ul>
</div>