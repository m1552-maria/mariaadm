<?php
	@session_start();
	if(!$_SESSION[empID]) header("Location: /login.php");
	include '../inc_sys.php';
	$incfn = $_REQUEST[incfn];
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Keywords" content="網站關鍵字" />
	<title>個人資訊</title>
	<link href="/css/index.css" rel="stylesheet" type="text/css" />
  <link href="../Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
	<style>
  .menuBar { background-color:#7B0C13; font: 1.2em '微軟正黑體',Verdana; color:#FFFFE0 }
  #mainmenu a { text-decoration:none }
  #mainmenu a:link { color:#FFFFE0 }
  #mainmenu a:visited { color:#FFFFE0 }
  #mainmenu a:hover { color:#FFFFE0 }
  #mainmenu a:active { color:#FFFFE0 }
	.foot {	font:12px "微軟正黑體",Verdana;	color: #CCC; background-color: #7B0C13;	padding: 8px; }
  </style>  
	<script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mainTable">
  <tr>
    <td class="sideBar">&nbsp;</td>
    <td width="1000" class="main" >
    	<!-- 頁面本體 ↓-->
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td class="headBar"><? include('../inc_head.php')?></td></tr>
        <tr><td class="menu"><? include('../inc_menu2.php') ?></td></tr>        
        <tr><td class="main">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr height="8"><td width="20"></td><td width="200"></td><td colspan="2"></td><td width="10"></td></tr>
            <!-- 目錄頁本體 ↓-->
            <tr valign="top">
              <td>&nbsp;</td>
              <td><? include 'menu.php' ?></td>
              <td width="24"></td>
              <td valign="top"><? include $incfn ?></td>
              <td>&nbsp;</td>
            </tr>
            <!-- 目錄頁本體 ↓-->
            <tr><td>&nbsp;</td><td colspan="3">&nbsp;</td><td>&nbsp;</td></tr>
          </table>
        </td></tr>
        <tr><td class="foot" align="center"><? include('../inc_foot.php')?></td></tr>
      </table>
      <!-- 頁面本體 ↑-->
    </td>  
    <td class="sideBar">&nbsp;</td>
  </tr>
</table>
</body>
<? include 'style.php' ?>
</html>
