<? include '../maintain/inc_vars.php'; ?>
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
function addItem(tbtn,fld) {
	var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6'>"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	if(!tfm.title.value) { alert('專案名稱不能為空白！'); tfm.title.focus(); return false; }
	if(!tfm.bDate.value) { alert('起始日期不能為空白！'); tfm.bDate.focus(); return false; }
	return true;
}
function set_type(elm) {
	if(elm.selectedIndex>0) $('#tyName').val(elm.options[elm.selectedIndex].text);
}
</script>
<form id="prjform" action="newprojectDo.php" method="post" onsubmit="return formCheck(this)">
<table width="100%" border="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td align="right">專案名稱：</td>
    <td colspan="3"><input name="title" type="text" id="title" size="90" /></td>
  </tr>
  <tr>
    <td align="right">專案類別：</td>
    <td><input type="text" name="tyName" id="tyName" />
     	<select name="seltype" id="seltype" onchange="set_type(this)">
      <option>-- 選擇 --</option><? foreach($projClass as $v) echo "<option>$v</option>"; ?>
      </select>
    </td>  
    <td align="right">計畫年度：</td>
    <td><input name="actYear" type="text" id="actYear" size="8" maxlength="8" /></td>
  </tr>
  <tr>
    <td align="right">起始日期：</td>
    <td><input type="text" name="bDate" id="bDate" class="queryDate"/></td>
    <td align="right">結束日期：</td>
    <td><input type="text" name="eDate" id="eDate" class="queryDate"/></td>
  </tr>
	<tr>
    <td align="right">專案成員：</td>
    <td colspan="3">
	    <input name="empID[]" type="text" class="queryID" id="empID" size="6" /> <span class="btn" onClick="addItem(this,'empID')"> +新增</span>
    </td>
  </tr>  
  <tr>
    <td align="right">內容說明：</td>
    <td colspan="3"><textarea name="description" id="description" cols="60" rows="3" class='ckeditor'></textarea></td>
  </tr>
  <tr>
    <td align="right">方案性質：</td>
    <td colspan="3"><textarea name="prjText" id="prjText" cols="60" rows="3" class='ckeditor'></textarea></td>
  </tr>
	<tr>
    <td align="right">主辦單位：</td>
    <td><input type="text" name="mainUnit" id="mainUnit" /></td>
    <td align="right">協(承)辦單位</td>
    <td><input type="text" name="subUnit" id="subUnit" /></td>
  </tr>
	<tr>
	  <td align="right">辦理地點：</td>
	  <td><input type="text" name="Place" id="Place" /></td>
	  <td align="right">參加對象：</td>
	  <td><input type="text" name="prjObject" id="prjObject" /></td>
	  </tr>
	<tr>
	  <td align="right">使用經費：</td>
	  <td><input type="text" name="fund" id="fund" /></td>
	  <td align="right">經費來源：</td>
	  <td><input type="text" name="fundSource" id="fundSource" /></td>
	  </tr>
	<tr>
	  <td align="right">補助金額：</td>
	  <td><input type="text" name="allowance" id="allowance" /></td>
	  <td align="right">預算金額：</td>
	  <td><input type="text" name="budget" id="budget" /></td>
	  </tr>
	<tr>
	  <td align="right">預算計劃編號：</td>
	  <td><input type="text" name="prjPlanID" id="prjPlanID" /></td>
	  <td align="right">受益人數(次)及效益：</td>
	  <td><input type="text" name="benefit" id="benefit" /></td>
	  </tr>
	<tr>
	  <td align="right">是否逾期核銷：</td>
	  <td><input name="isCheckDelay" type="checkbox" id="isCheckDelay" value="1" /></td>
	  <td align="right">核銷金額核銷日期：</td>
	  <td><input type="text" name="checkDate" id="checkDate" class="queryDate"/></td>
	  </tr>  
  <tr>
    <td>&nbsp;</td>
    <td colspan="3"><input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
</form>