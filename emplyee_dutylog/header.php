<?php
    include_once("../config.php");
    include_once('../miclib.php');
    include_once('common.php');

    $dep = '';
    //當前IP是否有權限
    $punch_card_privage = 0;
    //用網段去判斷
    /*$ip = explode('.', $_SERVER['REMOTE_ADDR']);
    $ip = $ip[0].".".$ip[1].".".$ip[2];
    $sql ="select * from `punch_card_setting` where SUBSTRING_INDEX(`ip`,'.',3)='".$ip."' and status='1'";//找到是否有符合的網段/
    $rs = db_query($sql);
    if(!db_eof($rs)){
        $r=db_fetch_array($rs);
        $dep = $r['place'];
    }*/
    $data = array('type'=>'ip');
    $punchList = get_punch_card_setting($data);
    if(count($punchList) > 0) {
        $dep = $punchList[key($punchList)]['place'];
        $punch_card_privage = 1;
    }
?>

<div class="row">
	<div class="col-md-12 top-bg">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-1">
                     <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button> <a class="navbar-brand" href="/emplyee_dutylog"><img src="../images/logo.jpg" class='logo'/></a>
            </div>
            

            <!-- 對應的單位名稱 -->
            <div class="header" id="head" value="<?=$dep;?>"><?=$dep;?></div>
            <div class="collapse navbar-collapse" id="navbar-1">
               <form class="navbar-form navbar-right" >
                    <!-- <a href="https://whatismyipaddress.com/" target="_blank" class="btn btn-default btn-white menager" style="padding: 10px;color:#cc0000;">查看IP</a> -->
                    <button type="button" class="btn btn-default btn-white menager" data-toggle="modal" data-target="#valid">管理功能</button>
                </form> 
            </div>
		</nav>
	</div>
</div>