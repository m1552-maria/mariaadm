<?php
  include_once("../config.php");
  include_once('../miclib.php');
  include_once('common.php');

  date_default_timezone_set("Asia/Taipei");//設成台灣時間

  switch ($_POST['type']) {
    case 'manager':            manager();            break;
    case 'manager_check':      manager_check();      break;
    case 'punch_card':         punch_card();         break;
    case 'log_fullscreen':     log_fullscreen();     break;
    case 'time_check':         time_check();         break;
    case 'repunch':            repunch();            break;
    case 'nowip':              nowip();              break;
    case 'ip_list':            ip_list();            break;
    case 'ip_setting':         ip_setting();         break;
    case 'punchcard_version':  punchcard_version();  break;
    case 'gps_list':           gps_list();           break;
    case 'gps_setting':        gps_setting();        break;
    case 'gps_check_setting':  gps_check_setting();  break;
    case 'ip_check_setting':   ip_check_setting();   break;
  }

  function nowip(){
    echo $_SERVER['REMOTE_ADDR'];//真正的IP位置
  }

///*20170920****///
  function gps_list(){  //列出符合manager中可修改的gps清單
      $manager = '';
      $sql2 = "select empID,empName,cardNo from emplyee where cardNo ='".$_POST['managerid']."' and isOnduty=1";//先用卡號去判別是否為員工
      $rs = db_query($sql2);
      if(!db_eof($rs)){
        $r = db_fetch_array($rs);
        $manager = $r['empID'];
      }
      if($manager == '') {
        echo '';
        exit;
      }

      $sql = "select * from  punch_card_setting where manager like '%".$manager."%' and status='1'"; //再利用員工編號去找是否為管理員
      if(!db_eof($rs)){
        $rs = db_query($sql);
        $gps_list = array();
        while ($r = db_fetch_array($rs)){
          array_push($gps_list,array($r['place'],$r['id'],$r['lat'],$r['lon'])); //地點，編號，緯度，經度
        }
        echo json_encode($gps_list);
        exit;
      }
      echo '';
  }

  //設定gps位置
  function gps_setting(){  //更新gps綁定位置
    $id = 0;
    $sql = 'select * from `punch_card_setting` where id="'.$_POST['id'].'" and status=1';
    $rs = db_query($sql);
    if(!db_eof($rs)){
      $r=db_fetch_array($rs);
      $id = $r['id'];
    }

    if($id == 0) exit;

    $lokation = explode(',',$_POST['place']); //print_r($lokation);//lokation[0] =>經度, lokation[1] =>緯度
    $sql = "update punch_card_setting set lat='".$lokation[1]."',lon='".$lokation[0]."' where id='".$id."' and status='1'";
    $rs = db_query($sql);

    //抓emplyee
    $sql = 'select empID,empName,cardNo from emplyee where cardNo="'.$_POST['managerid'].'" and `isOnduty`=1';
    $rs = db_query($sql);
    $r=db_fetch_array($rs);

    //新增
    $sql = 'insert into `punch_card_log` (`fid`,`operactor`,`status`,`notes`) values ('.$id.',"'.$r['empID'].'","gps_setting","綁定GPS")';
    $rs = db_query($sql);

  }

  
  //抓管理人員 
  /*20170926 sean edit*/
  function manager(){
    $manager = array();
    
    //找管理人員有哪些
    //先看ip有沒有被設定
    $punchList = array();
    $data = array('type'=>'ip');
    $punchList = get_punch_card_setting($data);

    //找GPS
    if($_POST['location'] != '' && count($punchList) == 0) {
      $data = array('type'=>'gps', 'val'=>$_POST['location']);
      $punchList = get_punch_card_setting($data);
    }
    //沒設定位置時抓全部
    if(count($punchList) == 0) {
      $data = array('type'=>'true');
      $punchList = get_punch_card_setting($data);
    }

    foreach ($punchList as $key => $value) {
      array_push($manager, $value['manager']);
    }

    $emp_list = array();
    $sql = 'select empID,empName,cardNo from emplyee where empID in ('.implode(',', array_filter($manager)).') and isOnduty=1';//必須在職員工
    $rs = db_query($sql);
    while ($r=db_fetch_array($rs)){
      if($r['cardNo']!=''){
        $emp_list[$r['empID']] = $r['cardNo'];
      }
    }
    echo json_encode($emp_list);  
   
  }


  function manager_check(){
    $tag = 0;
    $manager = array();

    $punchList = array();
    $data = array('type'=>'ip');
    $punchList = get_punch_card_setting($data);

    if($_POST['location'] != '' && count($punchList) == 0) {
      $data = array('type'=>'gps', 'val'=>$_POST['location']);
      $punchList = get_punch_card_setting($data);
    }

    if(count($punchList) == 0) {
      $data = array('type'=>'true');
      $punchList = get_punch_card_setting($data);
    }

    foreach ($punchList as $key => $value) {
      array_push($manager, $value['manager']);
    }

    $emp_list = array();
    $sql = 'select empID,empName,cardNo from emplyee where empID in ('.implode(',', array_filter($manager)).') and isOnduty=1';//必須在職員工
    $rs = db_query($sql);
    while ($r=db_fetch_array($rs)){
      if($r['cardNo']!=''){
        if($r['cardNo']==$_POST['cardno']){
          $tag = 1;
        }
      }
    }
    echo $tag;
  }


  //抓打卡機權限
  function punch_card() {
    $result = array('punch_card'=>0, 'msg'=>'查無打卡點');
    //用網段去判斷
    $data = array('type'=>'ip');
    $punchList = get_punch_card_setting($data);

    if(count($punchList) > 0) {
      $result['punch_card'] = 1;
      $result['msg'] = '';
    } elseif(count($punchList) == 0 && $_POST['location'] != '') {
      $data = array('type'=>'gps', 'val'=>$_POST['location']);
      $punchList = get_punch_card_setting($data);
      if(count($punchList) > 0) {
        $result['punch_card'] = 1;
        $result['msg'] = '';
      }
    }

    $tCheck = time_check(0);
    if($tCheck['type'] == 2) {
      $result['punch_card'] = 0;
      $result['msg'] = '本機時間異常';
    }

    echo json_encode($result);
  }


  //異動管理log
  function log_fullscreen(){
    //抓ip的id
    $id = 0;
    $data = array('type'=>'ip');
    $punchList = get_punch_card_setting($data);

    if(count($punchList) > 0) $id = key($punchList);
    elseif(count($punchList) == 0 && $_POST['location'] != '') {
      $data = array('type'=>'gps', 'val'=>$_POST['location']);
      $punchList = get_punch_card_setting($data);
      if(count($punchList) > 0) $id = key($punchList);
    }

    //抓emplyee
    $sql = 'select empID,empName,cardNo from emplyee where cardNo="'.$_POST['managerid'].'" and `isOnduty`=1';
    $rs = db_query($sql);
    $r=db_fetch_array($rs);
    $empID = $r['empID'];

    //新增
    $sql = 'insert into `punch_card_log` (`fid`,`operactor`,`status`) values ('.$id.',"'.$r['empID'].'","'.$_POST['status'].'")';
    $rs = db_query($sql);
  }
  

  //校正時間 20170922 sean edit*/
  function time_check($isAjax = 1){
    $result = array('type'=>'', 'severTime'=>'', 'gap'=>'');
    $user_time = date("Y/m/d H:i:s",strtotime($_POST['now_time']));
    $result['user_time'] = $user_time;
    $server_time = date("Y/m/d H:i:s");
    //$server_time = date("Y/m/d H:i:s" , strtotime('+2 min'));

    $result['severTime'] = $server_time;
    $result['gap'] = strtotime($server_time) - strtotime($user_time);
    $total = abs($result['gap']);

    if($total>60 && $total<=180){//是否大於60秒-180秒
      $result['type'] = 0;
    }elseif($total>180){//是否大於3分鐘
      $result['type'] = 2;//大於3分鐘
    }else{
      $result['type'] = 1;//正常
    }
    if($isAjax == 1) echo json_encode($result);
    else return $result;
  }

  //補傳打卡資料
  function repunch() {
    $punch_list = array(); if(isset($_POST['data']) && $_POST['data']!='') $punch_list = json_decode($_POST['data']);
    $punch_list1 = array(); if(isset($_POST['data1']) && $_POST['data1']!='') $punch_list1 = json_decode($_POST['data1']);
    $punch_list2 = array(); if(isset($_POST['data2']) && $_POST['data2']!='') $punch_list2 = json_decode($_POST['data2']);
    $punch_list3 = array(); if(isset($_POST['data3']) && $_POST['data3']!='') $punch_list3 = json_decode($_POST['data3']);
    $punch_list4 = array(); if(isset($_POST['data4']) && $_POST['data4']!='') $punch_list4 = json_decode($_POST['data4']);
    $punch_list5 = array(); if(isset($_POST['data5']) && $_POST['data5']!='') $punch_list5 = json_decode($_POST['data5']);
    $punch_list6 = array(); if(isset($_POST['data6']) && $_POST['data6']!='') $punch_list6 = json_decode($_POST['data6']);
    $punch_list7 = array(); if(isset($_POST['now_day']) && $_POST['now_day']!='') $punch_list7 = json_decode($_POST['now_day']);
    
    //筆數
    $fail_num = 0;//不成功筆數
    $success_num = 0;//成功筆數
    $date_list = array();
    $total_list = array($punch_list,$punch_list1,$punch_list2,$punch_list3,$punch_list4,$punch_list5,$punch_list6,$punch_list7);
    $error_list = array();
    foreach($total_list as $k=>$v){
      foreach($v as $k1=>$v1){
        if($v1->is_fail == 1){//失敗才要補傳
          //記錄有匯的日期
          $date_list[$v1->day] = $v1->day;
          $now_array = (array) $v[$k1];
          //$result = punch_send($v1->cardno,$v1->date,$v1->dType,$v1->ip,$v1->gps,$v1->ipCheck,$now_array);
          $result = punch_send($v1,$now_array);
          if($result==0){ //不成功
            $fail_num ++;
            array_push($error_list,$v1);
          }else{ //成功
            $success_num ++;
          }
        }
      }
    }

    //重匯打卡資料
    foreach($date_list as $k=>$v){
      $start_day = date('Y-m-d H:i:s',strtotime($v));
      $end_day = date('Y-m-d H:i:s',mktime(23, 59, 59, date('m',strtotime($v)), date('d',strtotime($v)),date('Y',strtotime($v))));
   
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['SERVER_NAME'].'/maintain/punch/api.php');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//不要有回傳值而且可以執行
      curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( array( "start_day"=>$start_day, "end_day"=>$end_day) )); 
      curl_exec($ch); 
      curl_close($ch);
    }
    echo json_encode($error_list);//回傳不成功的值值給ajax
  
    //抓ip的id
    $id = 0;
    $punchList = array();
    $data = array('type'=>'ip');
    $punchList = get_punch_card_setting($data);

    if(count($punchList) > 0) $id = $punchList[key($punchList)]['id'];
    elseif($_POST['location'] != '' && count($punchList) == 0) {
      $data = array('type'=>'gps', 'val'=>$_POST['location']);
      $punchList = get_punch_card_setting($data);
      if(count($punchList) > 0) {
        $id = $punchList[key($punchList)]['id'];
      }
    }

    //抓emplyee
    $sql = 'select empID,empName,cardNo from emplyee where cardNo="'.$_POST['managerid'].'" and `isOnduty`=1';
    $rs = db_query($sql);
    $r=db_fetch_array($rs);
    $empID = $r['empID'];

    //新增
    if($fail_num==0 && $success_num==0){
      $notes = '無資料匯入';
    }else{
      $notes = '成功:'.$success_num.'筆,'.'失敗:'.$fail_num.'筆';
    }
    $sql = 'insert into `punch_card_log` (`fid`,`operactor`,`status`,`notes`) values ('.$id.',"'.$empID.'","repunch","'.$notes.'")';
    $rs = db_query($sql);
    exit;
  }

  function punch_send($dataObj=array(),$now_array=array()) {
    //抓打卡機的位置
    $place = '';
    $isIP = 1;
    if(!isset($dataObj->ipCheck) || (isset($dataObj->ipCheck) && $dataObj->ipCheck == 1)) {
      $data = array('type'=>'ip', 'val'=>$dataObj->ip);
      $punchList = get_punch_card_setting($data);
      if(count($punchList) > 0) { $place = $punchList[key($punchList)]['place'];  }
    } elseif(isset($dataObj->ipCheck) && $dataObj->ipCheck == 0 && $_POST['location'] != '') {
      $data = array('type'=>'gps', 'val'=>$_POST['location']);
      $punchList = get_punch_card_setting($data);
      if(count($punchList) > 0) { $isIP = 0;  $place = $punchList[key($punchList)]['place'];  }
    }

    //判斷卡號有沒有存在
    $sql = "select empID,empName,cardNo,isOnduty from emplyee where cardNo='".$dataObj->cardno."' AND `isOnduty`=1";
    $rs = db_query($sql);
    /*----20171026 sean 將志工打卡部分匯進來 start----*/
    $selectRows = db_num_rows($rs);
    $userType = 'emplyee';
    if($selectRows == 0) {
      $userType = 'volunteers';
      include_once("../config_volunteers.php");
      include_once("../system/db.php");
      $op = array(
        'dbSource' => $DB_SOURCE,
        'dbAccount' => $DB_ACCOUNT,
        'dbPassword' => $DB_PASSWORD,
      );
      $db = new db($op);
      $sql = "select id,VID,name,cardNo from volunteers where cardNo=? and status = 1";
      $rs = $db->query($sql, array($dataObj->cardno));
      $selectRows = $db->num_rows($rs);
    }

    if($selectRows > 0){
      if($userType == 'emplyee') {
        $r = db_fetch_array($rs);
        $userNum = $r['empID'];
      } elseif($userType == 'volunteers') {
        $r = $db->fetch_array($rs);  
        $userNum = $r['VID'];
        $userID = $r['id'];
      }

      $now_date = date("Y-m-d H:i:s",strtotime($dataObj->date));

      $dType = '';
      switch ($dataObj->dType) {
        case '上班': $dType = 'duty_in'; break;
        case '下班': $dType = 'duty_out'; break;
        case '外出': $dType = 'outoffice_out'; break;
        case '返回': $dType = 'outoffice_back'; break;
      }

      if($userType == 'emplyee') {
        if($isIP == 1) {
          $sql = ' insert into `emplyee_dutylog` (`empID`,`dType`,`ip`,`place`,`d_datetime`,`cardNo`) values("'.$userNum.'","'.$dType.'","'.$dataObj->ip.'","'.$place.'","'.$now_date.'","'.$dataObj->cardno.'")';
        } else {
          $sql = ' insert into `emplyee_dutylog` (`empID`,`dType`,`ip`,`gps`,`place`,`d_datetime`,`cardNo`) values("'.$userNum.'","'.$dType.'","'.$dataObj->ip.'","'.$_POST['location'].'","'.$place.'","'.$now_date.'","'.$dataObj->cardno.'")';
        }
        
        db_query($sql);
        $new_id=mysql_insert_id();//新增的id
        if($new_id>0){//成功
          return 1;
        }else{//失敗
          return 0;
        }
      } elseif($userType == 'volunteers') {
        $db = new db('volunteers_dutylog');
        $db->row['VID'] = "'".$userNum."'";
        $db->row['dType'] = "'".$dType."'";
        $db->row['ip'] = "'".$dataObj->ip."'";
        $db->row['place'] = "'".$place."'";
        $db->row['d_datetime'] = "'".$now_date."'";
        $db->row['cardNo'] = "'".$dataObj->cardno."'";
        if($isIP == 0) $db->row['gps'] = "'".$_POST['location']."'";
        $db->insert();

        include_once('volunteers_duty.php');
        $vD = new vDuty();
        $data = array(
          'id'=>$userID,
          'dType'=>$dType,
          'place'=>$place,
          'dateTime'=>$now_date
        );
        $vD->insertDuty($data);
        return 1;
      }
    }else{
      return 1; //可能是因為沒有這個卡號//所以不用回傳失敗
    }
  }

  function ip_list(){
    //判斷卡號有沒有存在
    $empID = '';
    $sql = "select empID,empName,cardNo,isOnduty from emplyee where cardNo='".$_POST['cardno']."' AND `isOnduty`=1";
    $rs = db_query($sql);
    if(!db_eof($rs)){
      $r = db_fetch_array($rs);  
      $empID = $r['empID'];
    }

    if($empID!=''){
      $place = array();
      $sql = 'select * from punch_card_setting where manager like '."'%".$empID."%' and status='1'";
      $rs = db_query($sql);
      if(!db_eof($rs)){
        while($r=db_fetch_array($rs)){
          array_push($place, $r['place'].','.$r['id'].','.$r['ip']);
        }
        echo implode(':', $place);
      }
    }
  }


  function ip_setting(){
    //判斷此ip有沒有被綁定過
    // $id = 0;
    // $sql = 'select * from punch_card_setting where ip='."'".$_POST['ip']."'";
    // echo $sql.'</br>';
    // $rs = db_query($sql);
    // if(!db_eof($rs)){
    //   $r=db_fetch_array($rs);
    //   $id = $r['id'];
    //   echo $id.'</br>';
    // }

    // if($id!=0){
    //   //將原本有的刪掉
    //   $sql = 'update punch_card_setting set ip='."''".' where id='.$id;
    //   echo $sql.'</br>';
    //   db_query($sql);
    // }

    //更新
    $sql = 'update punch_card_setting set ip='."'".$_SERVER['REMOTE_ADDR']."'".' where id='.$_POST['id'];
    db_query($sql);


    //記log
    //抓ip的id//可能會抓錯//log的資料有可能是錯的//因為ip有可能一樣的會有很多個
    $id = 0;
    $sql = 'select * from `punch_card_setting` where `ip`="'.$_SERVER['REMOTE_ADDR'].'" and status=1';
    $rs = db_query($sql);
    if(!db_eof($rs)){
      $r=db_fetch_array($rs);
      $id = $r['id'];
    }

    //抓emplyee
    $sql = 'select empID,empName,cardNo from emplyee where cardNo="'.$_POST['managerid'].'" and `isOnduty`=1';
    $rs = db_query($sql);
    $r=db_fetch_array($rs);
    $empID = $r['empID'];
    //新增
    $sql = 'insert into `punch_card_log` (`fid`,`operactor`,`status`) values ('.$id.',"'.$r['empID'].'","'.$_POST['status'].'")';
    $rs = db_query($sql);
  }

  function punchcard_version(){
    include('../maintain/inc_vars.php');
    echo $punchcard_version[0];
  }

  function ip_check_setting(){
    $ip = explode('.', $_SERVER['REMOTE_ADDR']);
    $ip = $ip[0].".".$ip[1].".".$ip[2];
    $sql ="select * from `punch_card_setting` where SUBSTRING_INDEX(`ip`,'.',3)='".$ip."' and status='1'";//找到是否有符合的網段/
    $rs = db_query($sql); 
    if(!db_eof($rs)){ 
      echo $text1 = '<div class="jumbotron bg-empID"><form class="form-horizontal" role="form">
      <div class="form-group"><label class="col-sm-3 control-label lab-empID">識別卡卡號：</label>
      <div class="col-sm-9"><input type="password" class="form-control txt-empID" id="empID" onkeypress="return punch(event);" autocomplete="off" />
      </div></div></form></div>';
    }
    
  }

function gps_check_setting(){
    $result = array('html'=>'', 'place'=>'', 'isGps'=>0);
    $data = array(
      'type' => 'gps',
      'val' => $_POST['location']
    );
    $punchList = get_punch_card_setting($data);
    if(count($punchList) > 0) {
      $result['isGps'] = 1;
      $result['html'] = '<div class="jumbotron bg-empID"><form class="form-horizontal" role="form">
    <div class="form-group"><label class="col-sm-3 control-label lab-empID">識別卡卡號：</label>
    <div class="col-sm-9"><input type="password" class="form-control txt-empID" id="empID" onkeypress="return punch(event);" autocomplete="off" />
    </div></div></form></div>';
      foreach ($punchList as $key => $value) {
        $result['place'] = $value['place'];
        break;
      }
    }
    echo json_encode($result);
}

?>