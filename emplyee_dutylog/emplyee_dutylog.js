var nowip = '';     //現在ip
var manager = '';   //現在管理者
var punch_card = 0; //是否能打卡
var time_check = 0; //是否能打卡
var managerid = ''; //管理者id
var punchdate, now_hour, now_day, before_day, delete_day, one_day, two_day, three_day, four_day, five_day, six_day; //時間
var browser = detectBrowser();//判斷當前瀏覽器是什麼
var now_manager_cardno='';
var version = '';//版本
//打卡紀錄
var punch_list = [];
var gps_location = ''; //gps位置
var time_gap = 0; //秒差
var NowDate = 0; //顯示時間
var error_msg = '';
//var punch_location =''; //打卡位置

window.onbeforeunload = bunload;
function bunload() {
    if (true) {
        mess = "確定離開?";
        return mess;
    }
}
document.ondragstart = function () { $('#empID').focus();return false; }
document.oncontextmenu = function () { $('#empID').focus();return false; }
document.onselectstart = function () { $('#empID').focus();return false; }

$(function() {
    $.when(timeCheck()).then(function(){
       ShowTime();
    });
    //永遠要focus在input裡面
    $('#empID').focus(); //一開始要做的事情
    
    //設定GPS
    $('#gps_setting').on('click', function() {
        $('#manager').modal('hide'); //關閉dialog
        $('#gps_setting_div').modal('show'); 

        if(gps_location == '') {
            $('.gps_list').html('<div class="jumbotron bg-dialog"><h1>請使用IE10以上的瀏覽器</h1></div>');
            return false;
        }

        $.ajax({
            method: "POST",
            url: "api.php",
            data: { type: 'gps_list' , location: gps_location , managerid: managerid}
        }).done(function(data) {
            $('#gps_setting_div .modal-msg p').html('目前gps:'+gps_location);
            if(data == ''){
                $('.gps_list').html('<div class="jumbotron bg-dialog"><h1>沒有能綁定的單位</h1></div>');
            }else{
                content = "";
                data=JSON.parse(data);
                $.each(data,function(k,v){
                    content += '<div class="jumbotron bg-dialog"><h1 onclick="gps_setting('+v[1]+')">'+v[0]+'</h1>緯度:'+v[3]+'經度:'+v[2]+'</div>';
                }) 
                $('.gps_list').html(content);
            }
        });
    })
    
    
    //全螢幕
    $('#fullscreen').on('click', function() {
        $('#manager').modal('hide'); //關閉dialog
        fullScreen(document.documentElement);
        $('#manager').on('hidden.bs.modal', function() {
            $('#empID').focus();
        })
        //切換文字
        if ($(this).children('h1').html() == '鎖定螢幕') {
            manager_log('lock');
            $(this).children('h1').html('解開螢幕');
        } else if ($(this).children('h1').html() == '解開螢幕') {
            manager_log('unlock');
            $(this).children('h1').html('鎖定螢幕');
        }
    })

    //抓ip的列表
    $('#ip_setting').on('click',function(){
      $('#manager').modal('hide'); //關閉dialog
      $('#ip_setting_div').modal('show');
      $('#ip_setting_div').on('shown.bs.modal', function() {
        $.ajax({
          method: "POST",
          url: "api.php",
          data: { ip: nowip, type: 'ip_list',cardno:managerid}
        }).done(function(data) {
          $('#ip_setting_div .modal-msg p').html('目前IP:'+nowip);
             if(data == ''){
            $('.gps_list').html('<div class="jumbotron bg-dialog"><h1>沒有能綁定的單位</h1></div>');
            }else{
            content = '';
            data = data.split(':');
            $.each(data,function(k,v){
              list = v.split(',');
              id = list[1];
              title = list[0]; 
              depip = list[2];
              content += '<div class="jumbotron bg-dialog"><h1 onclick="ip_setting('+id+')">'+title+'</h1>IP:'+depip+'</div>';
            })
            $('.ip_list').html(content);
          }
        });
        
      });
    })



    //監聽滑鼠點擊事件
    $(document).click(function(e) {
        $('#manager').on('hidden.bs.modal', function() {
            $("#empID").focus();
        })
        $('#valid').on('hidden.bs.modal', function() {
            $("#empID").focus();
        })
        $('#empID').focus();
    });



    $("#valid").click(function(e) {
        $("#empID2").focus();
        e.stopPropagation();
    });
    $('#ipblock').click(function(e) {
        e.stopPropagation();
    });
    $('#block').click(function(e) {
        e.stopPropagation();
    });
    $('#ip_setting_div').click(function(e) {
        e.stopPropagation();
    });
    $('#gps_setting_div').click(function(e) {
        e.stopPropagation();
    });


    $(".menager").click(function(e) { //如果是點擊管理功能的話
        $('#valid').on('shown.bs.modal', function() {
            $("#empID2").focus();
        });
        //先判斷是不是全螢幕//因為全螢幕不能再點擊全螢幕的時候才判斷文字
        if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
            $('#fullscreen').children('h1').html('鎖定螢幕');
        } else {
            $('#fullscreen').children('h1').html('解開螢幕');
        }
    });

    //處理上班下班
    $('.panel-footer .btn').on('click', function() {
        var status = $(this).html();
        $('.s_title').html(status);
        $('.s_title2').html(status + '-');
        if (status == '上班') {
            $('.datetime').attr('class', 'jumbotron bg-success datetime');
        } else if (status == '下班') {
            $('.datetime').attr('class', 'jumbotron bg-danger datetime');
        } else if (status == '外出') {
            $('.datetime').attr('class', 'jumbotron bg-warning datetime');
        } else if (status == '返回') {
            $('.datetime').attr('class', 'jumbotron bg-info datetime');
        }
    })

    //判斷經緯度是否有服合範圍的打卡點
    //alert(browser+' '+$('#punchUI').attr('data-ip-check'));
    if(browser == 'IE') getLocation();
    start();
});



function now_time() { //處理時間
    //當前時間
    punchdate = new Date();
    now_hour = punchdate.getHours();
    now_day = punchdate.toLocaleDateString();
    //前天
    before_day = new Date();
    before_day.setDate(before_day.getDate() - 1);
    before_day = before_day.toLocaleDateString(); //轉成可以用的日期
    //前7天的時間都要找出來
    delete_day = new Date();
    delete_day.setDate(delete_day.getDate() - 8); //要刪掉的webstorge
    delete_day = delete_day.toLocaleDateString(); //轉成可以用的日期
    one_day = new Date();
    one_day.setDate(one_day.getDate() - 7);
    one_day = one_day.toLocaleDateString();
    two_day = new Date();
    two_day.setDate(two_day.getDate() - 6);
    two_day = two_day.toLocaleDateString();
    three_day = new Date();
    three_day.setDate(three_day.getDate() - 5);
    three_day = three_day.toLocaleDateString();
    four_day = new Date();
    four_day.setDate(four_day.getDate() - 4);
    four_day = four_day.toLocaleDateString();
    five_day = new Date();
    five_day.setDate(five_day.getDate() - 3);
    five_day = five_day.toLocaleDateString();
    six_day = new Date();
    six_day.setDate(six_day.getDate() - 2);
    six_day = six_day.toLocaleDateString();
}


function manager_log(status) {
    $.ajax({
        method: "POST",
        url: "api.php",
        data: { ip: nowip, type: 'log_fullscreen', managerid: managerid, status: status, location:gps_location }
    }).done(function(data) {});
}


function start() {//一開始//當天第一次打卡
    $.ajax({
        method: "POST",
        url: "api.php",
        data: { type: 'nowip'}
    }).done(function(data) {
        now_time();
        nowip = data;
        //目前能不能使用刷卡功能//要有ip才能做
        $.ajax({
            method: "POST",
            url: "api.php",
            data: { ip: nowip, type: 'punch_card' , location: gps_location, now_time: punchdate.toISOString() }
        }).done(function(data) {
            var d = JSON.parse(data);
            punch_card = d['punch_card'];
            error_msg = d['msg'];
        });

        //目前版本
        if(version==''){//第一次進來
            $.ajax({
                method: "POST",
                url: "api.php",
                data: { ip: nowip, type: 'punchcard_version' }
            }).done(function(data) {
                version = data;
            });
        }else if(version!=''){
            $.ajax({
                method: "POST",
                url: "api.php",
                data: { ip: nowip, type: 'punchcard_version' }
            }).done(function(data) {
                if(version!=data){
                    //隱藏刷卡欄位
                    $('.bg-empID').hide();
                    $('.panel-footer').hide();
                    $('.versionerror').show();
                    //重新整理
                    alert('版本錯誤請重新整理頁面');
                    location.reload();
                }
            });
        }

        //抓全部有權限的管理者//不用ip也能做//無網路時需要用到的manager資料
        $.ajax({
            method: "POST",
            url: "api.php",
            data: { type: 'manager',ip: nowip, location: gps_location}
        }).done(function(data) {
            manager = data;
        });
    });

    
    timeCheck();
    //當前時間
    /*now_time();

    //校正時間
    $.ajax({
        method: "POST",
        url: "api.php",
        data: { now_time: punchdate.toISOString(), type: 'time_check' }
    }).done(function(data) {
        var d = JSON.parse(data);
        if (d['type'] == 0) {
            $('.error').show();//跳錯誤訊息但可以打卡
        }else if(d['2'] == 2){//超過3分鐘
            $('#block').modal({ backdrop: 'static', keyboard: false });
            $('#empID').blur();
            $('.error').show();
        }
        time_check = data;
    })*/
}

//校正時間
/*20170922 sean add*/
function timeCheck() {
    var dfd = $.Deferred();
    //當前時間
    now_time();
    //校正時間
    $.ajax({
        method: "POST",
        url: "api.php",
        data: { now_time: punchdate.toISOString(), type: 'time_check' }
    }).done(function(data) {
        //console.log(punchdate.toISOString());
        //console.log(data);
        var d = JSON.parse(data);
        if (d['type'] == 0) {
            //$('.error').show();//跳錯誤訊息但可以打卡
        } else if(d['type'] == 2){//超過3分鐘
            $('#block').modal({ backdrop: 'static', keyboard: false });
            $('#empID').blur();
            $('.error').show();
        } 
        time_gap = parseInt(d['gap']);
        dfd.resolve();
    })
    return dfd.promise();
    
}

//打卡
function punch(e) {
    if (punch_card == 0) {
        if (e.keyCode == 13) {
            $('.now_date').hide();
            $(".punch_msg").html('不能刷卡-'+error_msg+'!!').show();
           
            setTimeout(function() {
                $('.now_date').show();
                $(".punch_msg").hide();
            }, 1500);
            $('#empID').val('');
            $('#empID').focus();
            e.preventDefault(); //ie只能用這個
            return false;
        }
    }else {
        if (e.keyCode == 13) { //input 的enter事件
            now_time(); //當前時間
            //判斷有沒有今天的打卡資料
            if (window.localStorage.getItem(now_day)) {
                punch_list = JSON.parse(window.localStorage.getItem(now_day));
            } else { 
                //第一筆//立即補傳會讓今天第一筆消失
                start();
                //刪除7天前資料
                if (window.localStorage.getItem(delete_day)) { //判斷有沒有資料 有就刪掉
                    window.localStorage.removeItem(delete_day); //移除
                }
                //補傳資料
                //抓前一天資料
                if (window.localStorage.getItem(before_day)) {
                    data1 = window.localStorage.getItem(one_day); //第七天
                    data2 = window.localStorage.getItem(two_day);
                    data3 = window.localStorage.getItem(three_day);
                    data4 = window.localStorage.getItem(four_day);
                    data5 = window.localStorage.getItem(five_day);
                    data6 = window.localStorage.getItem(six_day);
                    data = window.localStorage.getItem(before_day);
                    $.ajax({
                        method: "POST",
                        url: "api.php",
                        data: { ip: nowip, type: 'repunch', location:gps_location, data: data, data1: data1, data2: data2, data3: data3, data4: data4, data5: data5, data6: data6 }
                    }).done(function(data) {
                        //成功就移除全部資料
                        window.localStorage.removeItem(one_day); //移除7
                        window.localStorage.removeItem(two_day); //移除6
                        window.localStorage.removeItem(three_day); //移除5
                        window.localStorage.removeItem(four_day); //移除4
                        window.localStorage.removeItem(five_day); //移除3
                        window.localStorage.removeItem(six_day); //移除2
                        window.localStorage.removeItem(before_day); //移除1
                        punch_list = [];//也要清空
                    });
                }
            }

            if (punch_card == 0) {//重新抓
                    $('.now_date').hide();
                    $(".punch_msg").html('不能刷卡-'+error_msg+'!!').show();
                    setTimeout(function() {
                        $('.now_date').show();
                        $(".punch_msg").hide();
                    }, 1500);
                    $('#empID').val('');
                    $('#empID').focus();
                    e.preventDefault(); //ie只能用這個
                    return false;
            }else{
                //打卡
                $.ajax({
                    method: "POST",
                    url: "punch.php",
                    data: { 
                        carNo: $('#empID').val(), 
                        dType: $('.s_title').html(), 
                        d_datetime: NowDate.toISOString(),
                        clientTime: punchdate.toISOString(),
                        ip: nowip , 
                        location: gps_location, 
                        status:punch_card
                    }
                }).done(function(data) {
                    $('.error').hide();
                    var d = JSON.parse(data);
                    if (d['type'] == 0) {
                        //$('.error').show();//跳錯誤訊息但可以打卡
                    } else if(d['type'] == 2){//超過3分鐘
                        punch_card = 0;
                        error_msg = '本機時間異常';
                        $('#block').modal({ backdrop: 'static', keyboard: false });
                        $('#empID').blur();
                        $('.error').show();
                    } 
                    time_gap = parseInt(d['gap']);
                    punch_result(d['msg'], 0);
                }).fail(function() {
                    punch_result('刷卡成功!!', 1);
                });
                if(browser=='IE7'||browser=='IE'){
                  e.preventDefault(); //ie只能用這個
                }else{
                  return false;  
                }    
            }
           
        }
    }
}


function punch_result(msg, result) {
    //打卡資料
    punch_list.push({ 
        'date': punchdate.toISOString(), 
        'cardno': $('#empID').val(), 
        'dType': $('.s_title').html(), 
        'day': now_day, 
        'ip': nowip, 
        'is_fail': result, 
        'gps': gps_location, 
        'ipCheck': $('#punchUI').attr('data-ip-check')
    });
    //記到webstorage裡面
    window.localStorage.setItem(now_day, JSON.stringify(punch_list));
    $('.now_date').hide();
    $(".punch_msg").html(msg).show();
    setTimeout(function() {
        $('.now_date').show();
        $(".punch_msg").hide();
    }, 1500);
    //7-9上班
    $tt=$('.s_title').html(); //排除月絣打卡
    if (($tt!='月餅到班' && $tt!='月餅離班') && now_hour>=7 && now_hour<=9) {
      setTimeout(function() { $('.s_title').html('上班'); $('.datetime').attr('class', 'jumbotron bg-success datetime');}, 1500);
    }
    $('#empID').val('');
    $('#empID').focus();
}


//立即補傳
function repunch() {
    now_time();
    //補傳資料
    //抓前一天資料
    data1 = window.localStorage.getItem(one_day); //第七天
    data2 = window.localStorage.getItem(two_day);
    data3 = window.localStorage.getItem(three_day);
    data4 = window.localStorage.getItem(four_day);
    data5 = window.localStorage.getItem(five_day);
    data6 = window.localStorage.getItem(six_day);
    data7 = window.localStorage.getItem(now_day);
    data = window.localStorage.getItem(before_day);
    $.ajax({
        method: "POST",
        url: "api.php",
        data: { ip: nowip, managerid: managerid, type: 'repunch', location:gps_location, data: data, data1: data1, data2: data2, data3: data3, data4: data4, data5: data5, data6: data6, now_day: data7 }
    }).done(function(data) {
        $('#manager').modal('hide'); //關掉
        $('#manager').on('hidden.bs.modal', function() {
            $("#empID").focus();
        })
        //成功就移除全部資料
        window.localStorage.removeItem(one_day); //移除7
        window.localStorage.removeItem(two_day); //移除6
        window.localStorage.removeItem(three_day); //移除5
        window.localStorage.removeItem(four_day); //移除4
        window.localStorage.removeItem(five_day); //移除3
        window.localStorage.removeItem(six_day); //移除2
        window.localStorage.removeItem(before_day); //移除1
        window.localStorage.removeItem(now_day); //移除1
        punch_list = [];//也要清空
        if(data && data!=''){//有未成功的資料
            var now_array = $.parseJSON(data);
            $.each(now_array,function(k,v){//記進去//不管日期
                punch_list.push(v);
            })
            //打卡資料
            //記到webstorage裡面
            window.localStorage.setItem(now_day, JSON.stringify(punch_list));
        }
        alert('補傳成功!!');

    }).fail(function() {
        $('#manager').modal('hide'); //關掉
        $('#manager').on('hidden.bs.modal', function() {
            $("#empID").focus();
        })
        alert('補傳失敗!!');
    });

}


function manager_test(e) { //判斷是不是管理者
    if (e.keyCode == 13) { //input 的enter事件

        //先用ajax抓有沒有權限
        //抓全部有權限的管理者//不用ip也能做
        $.ajax({
            method: "POST",
            url: "api.php",
            data: { type: 'manager_check',cardno: $('#empID2').val().toUpperCase(),ip: nowip, location:gps_location}
        }).done(function(data) {
            //console.log(data); return;

            if(data == 0){
                alert('沒有權限!');
                $('#empID2').val(''); //清空
                $('#valid').modal('hide'); //關掉
                // 偵測如果被關掉
                $('#valid').on('hidden.bs.modal', function() {
                    $('#empID').focus(); //回到empID
                })
            }else{
                managerid = $('#empID2').val();
                now_manager_cardno = $('#empID2').val().toUpperCase();//當前的管理者卡號//好像重複了
                $('#valid').modal('hide'); //關掉
                $('#manager').modal('show');
                $('#empID2').val(''); //清空
            }
        }).fail(function() {
            //無網路權限下
            obj = $.parseJSON(manager);
            var tag = 0;
            $.each(obj, function(i, v) {
                //如果有權限
                if (v.toUpperCase() == $('#empID2').val().toUpperCase()) {
                    managerid = $('#empID2').val();
                    $('#valid').modal('hide'); //關掉
                    $('#manager').modal('show');
                    $('#empID2').val(''); //清空
                    now_manager_cardno = v.toUpperCase();//當前的管理者卡號
                    tag = 1;
                }
            });

            if (tag == 0) {
                alert('沒有權限!');
                $('#empID2').val(''); //清空
                $('#valid').modal('hide'); //關掉
                // 偵測如果被關掉
                $('#valid').on('hidden.bs.modal', function() {
                    $('#empID').focus(); //回到empID
                })
            }
            if(browser=='IE7'||browser=='IE'){
              e.preventDefault(); //ie只能用這個
            }else{
              return false;  
            }
        });

        if(browser=='IE7'||browser=='IE'){
          e.preventDefault(); //ie只能用這個
        }else{
          return false;  
        }


        //無網路權限下
        // obj = $.parseJSON(manager);
        // var tag = 0;
        // $.each(obj, function(i, v) {
        //     //如果有權限
        //     if (v.toUpperCase() == $('#empID2').val().toUpperCase()) {
        //         managerid = $('#empID2').val();
        //         $('#valid').modal('hide'); //關掉
        //         $('#manager').modal('show');
        //         $('#empID2').val(''); //清空
        //         now_manager_cardno = v.toUpperCase();//當前的管理者卡號
        //         tag = 1;
        //     }
        // });

        // if (tag == 0) {
        //     alert('沒有權限!');
        //     $('#empID2').val(''); //清空
        //     $('#valid').modal('hide'); //關掉
        //     // 偵測如果被關掉
        //     $('#valid').on('hidden.bs.modal', function() {
        //         $('#empID').focus(); //回到empID
        //     })
        // }
        // if(browser=='IE7'||browser=='IE'){
        //   e.preventDefault(); //ie只能用這個
        // }else{
        //   return false;  
        // }
    }
}

//設定ip
function ip_setting(id){
  if(confirm('確定要綁定到此單位?')){
    $.ajax({
        method: "POST",
        url: "api.php",
        data: { id:id,ip: nowip,type:'ip_setting',managerid:managerid,status:'ip_setting'}
    }).done(function(data) {
      alert('設定成功!');
      $('#ip_setting_div').modal('hide'); //關閉dialog
      $('#ip_setting_div').on('hidden.bs.modal', function() {
        $('#empID').focus();
      })
      location.reload();
    }).fail(function() {
      alert('設定失敗!');
    });  
  }
}


function ShowTime() {
    NowDate = new Date();
    var NowDate_s = NowDate.getTime()+1000*time_gap;
    NowDate.setTime(NowDate_s);
    $('.now_date').html(NowDate.toLocaleString());
    setTimeout('ShowTime()', 1000);
}


function fullScreen(elem) { //全螢幕
    if (!document.fullscreenElement && !document.mozFullScreenElement &&
        !document.webkitFullscreenElement && !document.msFullscreenElement) {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
}


//判斷瀏覽器
function detectBrowser(){
    var Sys = {};
    var ua = navigator.userAgent.toLowerCase();
    var s;
    (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = s[1] :
    (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
    (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
    (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
    (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
    (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;


    browser = '';
    var isIE = navigator.userAgent.search("MSIE") > -1;
    var isIE7 = navigator.userAgent.search("MSIE 7") > -1;
    var isFirefox = navigator.userAgent.search("Firefox") > -1;
    var isOpera = navigator.userAgent.search("Opera") > -1;
    var isSafari = navigator.userAgent.search("Safari") > -1;//Google瀏覽器是用這核心
    
    if (isIE7) browser = 'IE7';
    if (isIE)  browser = 'IE';
    if (isFirefox) browser = 'Firefox';
    if (isOpera) browser = 'Opera';
    if (isSafari) browser = 'Safari/Chrome';
    if(browser == '' && Sys.ie) browser = 'IE';
    return browser;
}

//設定gps
function gps_setting(id){
    if(confirm('確定要綁定到此單位?')){
      $.ajax({
          method: "POST",
          url: "api.php",
          data: { type:'gps_setting',id:id,place:gps_location,managerid:managerid}
      }).done(function(data) {
        alert('設定成功!');
        $('#gps_setting_div').modal('hide'); //關閉dialog
        $('#gps_setting_div').on('hidden.bs.modal', function() {
          $('#empID').focus();
        })
        location.reload();
      }).fail(function() {
        alert('設定失敗!');
      });  
    }
  }
  
//GPS 位置
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition( function showPosition(position) {
            /*lat = Math.round(position.coords.latitude*1000)/1000; //換算小數點第3位
            lon = Math.round(position.coords.longitude*1000)/1000;
            gps_location = lon+","+lat;*/
            gps_location = position.coords.latitude+","+position.coords.longitude;
            if($('#punchUI').attr('data-ip-check') == 0) {
                $.ajax({
                     method: "POST",
                     url: "api.php",
                     data: { type: 'gps_check_setting' , location: gps_location}
                }).done(function(data) {
                    //console.log(data);
                    d = JSON.parse(data); 
                    $('#punchUI').html(d['html']);
                    $('#head').html(d['place']); 
                    start();
                });
            }

        }, function error(e){
            gps_location = '';
        });
    } else { 
      gps_location = '';
    }
}
