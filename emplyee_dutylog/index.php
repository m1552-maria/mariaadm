<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1, user-scalable=no" />
    <meta name="apple-touch-fullscreen" content="YES"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <title>打卡系統</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <script src="../Scripts/jquery-1.12.3.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../cookies.js"></script>
    <script src="emplyee_dutylog.js?<?php echo date('YmdHis');?>"></script>
  </head>
  <body>

	<div class="container-fluid">
		<?	include "header.php";		//頁首		?>
		<?	include "main.php";	    //主要區域	?>
		<?	include "footer.php"; 	//頁尾		?>
	</div>
    <?  include "valid.html";   //管理身份驗證  ?>
	  <?	include "manager.html"; //管理功能	?>
    <?  include "block.html";   //block  ?>
  </body>
</html>