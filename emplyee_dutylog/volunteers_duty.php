<?php
	class vDuty {
		private $db;

		function __construct() {
			$this->db = new db('volunteers_duty');
		}

		public function insertDuty($data = array()) {
			/*data = array(
				'id'=>'',      // 有ID 不需要 cardno
				'dType'=>'',
				'place'=>'',
				'dateTime'=>'',
			);*/
			$selectDate = date('Y-m-d',strtotime($data['dateTime']));
			$sql = "select * from ".$this->db->table." where vid = '".$data['id']."' and (startTime like '".$selectDate."%' or startTime2 like '".$selectDate."%' or endTime2 like '".$selectDate."%')";
			/*print_r($sql);
			exit;*/
			$rs = $this->db->query($sql);
			$rCount = $this->db->num_rows($rs);
			if($rCount > 0) {
				$r=$this->db->fetch_array($rs);
				if($data['dType'] == 'duty_in') {
					if($r['startTime2'] == '' || strtotime($r['startTime2']) > strtotime($data['dateTime'])) {
						$this->db->row['startTime2'] = "'".$data['dateTime']."'";
						$this->db->row['startLocation'] = "'".$data['place']."'";
					}
				} else {
					if($r['endTime2'] == '' || strtotime($r['endTime2']) < strtotime($data['dateTime'])) {
						$this->db->row['endTime2'] = "'".$data['dateTime']."'";
						$this->db->row['endLocation'] = "'".$data['place']."'";
					}
				}
				$whereStr = "id='".$r['id']."'";
				$this->db->update($whereStr);

			} else {
				$dateTime = ($data['dType'] == 'duty_in') ? 'startTime2' : 'endTime2';
				$place = ($data['dType'] == 'duty_in') ? 'startLocation' : 'endLocation';
				$this->db->row[$dateTime] = "'".$data['dateTime']."'";
				$this->db->row[$place] = "'".$data['place']."'";
				$this->db->row['vid'] = "'".$data['id']."'";
				$this->db->row['is_leadman'] = "'0'";
				$this->db->row['isOk'] = "'0'";
				$this->db->insert();

			}

		}
	}
?>