<?php
  switch($_REQUEST['cake']) {
    case 1: $dtStr='月餅到班'; break;
    case 2: $dtStr='月餅離班'; break;
    default: $dtStr='上班'; break;
  }
?>
<div class="row main">
	<!--	分類選單	-->
	<div class="col-md-12">
		<div class="panel panel-default">
			<span class='s_title'><?=$dtStr?></span>
			<div class="panel-body">
				<div class="jumbotron bg-success datetime" style="position: relative;">
					<div class="error">系統時間有誤，請校正系統時間為以下顯示時間</div>
					<span class='s_title2' ><?=$dtStr?> - </span>
					<span class="now_date"></span>
					<!-- 刷卡要出現的訊息 -->
					<span class="punch_msg">刷卡成功</span>
				</div>
				<div id='punchUI' data-ip-check="<?php echo $punch_card_privage;?>">
    <? if($punch_card_privage == 1) { ?>
					<div class="jumbotron bg-empID">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label class="col-sm-3 control-label lab-empID">識別卡卡號：</label>
								<div class="col-sm-9">
									<input type="password" class="form-control txt-empID" id="empID" onkeypress="return punch(event);" autocomplete="off"/>
								</div>
								<!--
								<div class="col-sm-5">
									<div class="alert alert-info" >
									    <strong class='alert-msg'>刷卡成功</strong>
									</div>
								</div>
								-->
							</div>
						</form>
					</div>
      <? } ?>
				</div>
				<div class="panel-footer">
          <? if(!$_REQUEST['cake']) { ?>
          <div class="btn-group">
            <button class="btn btn-success " type="button">上班</button>
            <button class="btn btn-danger" type="button">下班</button> 
          </div>
          <div class="btn-group">
            <button class="btn btn-warning " type="button">外出</button> 
            <button class="btn btn-info" type="button">返回</button>
          </div>
          <? } ?>
        </div>
		</div>
	</div>
</div>