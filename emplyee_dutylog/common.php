<?php
	/*20170922 sean add*/
function get_punch_card_setting($data = array()) {
    $result = array();
    $sql = "select * from `punch_card_setting` where status = '1'";
    switch ($data['type']) {
      case 'ip':
        if(empty($data['val'])) $data['val'] = $_SERVER['REMOTE_ADDR'];
        $ip3 = explode('.', $data['val']);
        $ip3 = $ip3[0].".".$ip3[1].".".$ip3[2];
        $sql .= " and SUBSTRING_INDEX(`ip`,'.',3) = '".$ip3."'";
        break;
      case 'gps':
        $data['val'] = explode(',', $data['val']);
        break;
      case 'true':
        break;
    }

    $rs = db_query($sql);
    if(!db_eof($rs)){
      while ($r=db_fetch_array($rs)) {
        switch ($data['type']) {
          case 'ip':
            $result[$r['id']] = $r;
            //判斷是否有完全符合的IP
            if($data['val'] == $r['ip']) {
              $result = array();
              $result[$r['id']] = $r;
              break 2;
            } 
            break;
          case 'gps':
            $distance = getDistance($r['lon'], $r['lat'], $data['val'][0], $data['val'][1]);//方圓100
            if($distance <= 100) $result[$r['id']] = $r;
            break;
          default:
            $result[$r['id']] = $r;
            break;
        }
      }
    }
    return $result;
}

  ///距離換算
function getDistance($lon1,$lat1,$lon2,$lat2){
    $radLat1 = deg2rad($lat1); //deg2rad() 函數將角度轉換為弧度
    $radLat2 = deg2rad($lat2);
    $radLon1 = deg2rad($lon1);
    $radLon2 = deg2rad($lon2);
    $a = $radLat1-$radLat2;
    $b = $radLon1-$radLon2;
    $s = 2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378137.0; //結果 公式為Google公開的距離計算公式
    $miter = round($s * 10000) / 10000;   //換算公尺
    return $miter;
}  
?>