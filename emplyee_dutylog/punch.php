<?php
  include_once("../config.php");
  include_once('../miclib.php');
  include_once('common.php');

  date_default_timezone_set("Asia/Taipei");//設成台灣時間

  $result = array('severTime'=>date("Y/m/d H:i:s"), 'gap'=>0, 'msg'=>'刷卡失敗.!!', 'type'=>1);
  $dutyTime = date("Y/m/d H:i:s",strtotime($_POST['d_datetime']));
  $clientTime = date("Y/m/d H:i:s",strtotime($_POST['clientTime']));
  $result['gap'] = strtotime($result['severTime']) - strtotime($clientTime);
  $gap = abs($result['gap']);
  if($gap>60 && $gap<=180){//是否大於60秒-180秒
    $result['type'] = 0;
  }elseif($gap>180){//是否大於3分鐘
    $result['type'] = 2;//大於3分鐘
  }

  $sql = "select empID,empName,cardNo,isOnduty,noSignIn from emplyee where cardNo='".$_POST['carNo']."'";
  $rs = db_query($sql);

/*----20171026 sean 將志工打卡部分匯進來 start----*/
  $selectRows = db_num_rows($rs);
  $userType = 'emplyee';
  if($selectRows == 0) {
    $userType = 'volunteers';
    include_once("../config_volunteers.php");
    include_once("../system/db.php");
    $op = array(
      'dbSource' => $DB_SOURCE,
      'dbAccount' => $DB_ACCOUNT,
      'dbPassword' => $DB_PASSWORD,
    );
    $db = new db($op);
    $sql = "select id,VID,name,cardNo from volunteers where cardNo=? and status = 1";
    $rs = $db->query($sql, array($_POST['carNo']));
    $selectRows = $db->num_rows($rs);
  }

  if($selectRows == 0){
    $result['msg'] = '刷卡失敗!!';
    echo json_encode($result);
    exit;
  }

  if($userType == 'emplyee') {
    $r = db_fetch_array($rs);
    if($r['isOnduty']<1){
      $result['msg'] = '已經離職!!';
      echo json_encode($result);
      exit;
    }elseif( $r['noSignIn']==1 && !($_POST['dType']=='月餅到班' || $_POST['dType']=='月餅離班') ){
      $result['msg'] = '不須打卡!!';
      echo json_encode($result);
      exit;
    }
    $userName = $r['empName'];
    $userNum = $r['empID'];
    $userID = $r['empID'];
  } elseif($userType == 'volunteers') {
    $r = $db->fetch_array($rs);
    $userName = $r['name'];
    $userNum = $r['VID'];
    $userID = $r['id'];
  }

  /*----20171026 sean 將志工打卡部分匯進來 end----*/

  //因為打卡只要有成功都是用系統時間下去記
  //$now_date = date("Y-m-d H:i:s");//用server時間紀錄

  $dType = '';
  switch ($_POST['dType']) {
    case '上班': $dType = 'duty_in'; break;
    case '下班': $dType = 'duty_out'; break;
    case '外出': $dType = 'outoffice_out'; break;
    case '返回': $dType = 'outoffice_back'; break;
    //::for 月餅排班  
    case '月餅到班': $dType = 'cake_in'; break;
    case '月餅離班': $dType = 'cake_out'; break;  
  }
  
  //抓打卡機的位置
  $settingData = array();
  $isGps = 0;
  $data = array('type'=>'ip');
  $punchList = get_punch_card_setting($data);

  if(count($punchList) > 0) {
    $settingData = $punchList[key($punchList)];
  } elseif(count($punchList) == 0 && $_POST['location'] != '') {
    $isGps = 1;
    $data = array('type'=>'gps', 'val'=>$_POST['location']);
    $punchList = get_punch_card_setting($data);
    if(count($punchList) > 0) {
      $settingData = $punchList[key($punchList)];
    }
  }

  /*20171027 sean edit*/
  if(!isset($settingData['id'])) {
    $result['msg'] = '刷卡失敗(無刷卡點)!!';
    echo json_encode($result);
    exit;
  }

  $total = abs(strtotime($result['severTime']) - strtotime($dutyTime));

  /*20171027 seab edit 加入志工部分*/
  if($userType == 'emplyee') {
    if($isGps == 0) { 
      $sql = ' insert into `emplyee_dutylog` (`empID`,`dType`,`ip`,`place`,`d_datetime`,`cardNo`) values("'.$userNum.'","'.$dType.'","'.$_SERVER['REMOTE_ADDR'].'","'.$settingData['place'].'","'.$dutyTime.'","'.$_POST['carNo'].'")';
    } else {
      $sql = ' insert into `emplyee_dutylog` (`empID`,`dType`,`ip`,`gps`,`place`,`d_datetime`,`cardNo`) values("'.$userNum.'","'.$dType.'","'.$_SERVER['REMOTE_ADDR'].'","'.$_POST['location'].'","'.$settingData['place'].'","'.$dutyTime.'","'.$_POST['carNo'].'")';
    }
    db_query($sql);
    $new_id = mysql_insert_id();
    $result['msg'] = $userName.' 刷卡成功!!';
    //$result['msg'] = $sql;
    if($total > 180) {
      $notes = '打卡異常 id:'.$new_id.', 系統時間:'.$result['severTime'].', 打卡時間:'.$dutyTime;
      $sql = 'insert into `punch_card_log` (`fid`,`operactor`,`status`,`notes`) values ('.$settingData['id'].',"'.$userNum.'","overpunch","'.$notes.'")';
      $rs = db_query($sql);
    }
  } elseif($userType == 'volunteers') {
    $db = new db('volunteers_dutylog');
    $db->row['VID'] = "'".$userNum."'";
    $db->row['dType'] = "'".$dType."'";
    $db->row['ip'] = "'".$_SERVER['REMOTE_ADDR']."'";
    $db->row['place'] = "'".$settingData['place']."'";
    $db->row['d_datetime'] = "'".$dutyTime."'";
    $db->row['cardNo'] = "'".$_POST['carNo']."'";
    if($isGps == 1) $db->row['gps'] = "'".$_POST['location']."'";
    $db->insert();
    $result['msg'] = $userName.'(志工) 刷卡成功!!';
    include_once('volunteers_duty.php');
    $vD = new vDuty();
    $data = array(
      'id'=>$userID,
      'dType'=>$dType,
      'place'=>$settingData['place'],
      'dateTime'=>$dutyTime
    );
    $vD->insertDuty($data);
  }
  echo json_encode($result); 
?>