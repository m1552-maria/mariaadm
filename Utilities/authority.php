<?php
	// include '../config.php';
	// session_start();
	// $ck=ckAuthority($_SESSION['user_classdef'][2],'','D3-001,B');
	// if($ck){
	// 	echo 'in';
	// }else{
	// 	echo 'out';
	// }

	function ckAuthority($permit,$depID,$empID){
		$permit_depIDs=$permit[0];
		$permit_empIDs=$permit[1];
		$depAry=explode(",",$depID);
		$dep_rs = array_intersect($permit_depIDs, $depAry);
		if(count($dep_rs)>0)	return true;

		$empAry=explode(",",$empID);
		$emp_rs = array_intersect($permit_empIDs, $empAry);
		if(count($emp_rs)>0)	return true;
		
		return false;

	}
	function getPermit($user_classdef){//取得登入者的權限視野
		$ary=array();
		foreach($user_classdef as $v){
			if($v)	array_push($ary," like '".$v."%' ");
		} 
		$sql="select * from emplyee ";
		$sql.="where depID ".join(" or depID ",$ary);
		$rs=db_query($sql);
		$permit_empIDs=array();
		$permit_depIDs=array();
		if(!db_eof($rs)){
			while($r=db_fetch_array($rs)){
				array_push($permit_empIDs,$r[empID]);
				array_push($permit_depIDs,$r[depID]);
			}
		}
		return array($permit_depIDs,$permit_empIDs);

	}
?>