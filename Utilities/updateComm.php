<?
	include '../config_psi.php';
	include "$root/system/db.php";
	include '../maintain/inc_vars.php';
	$db = new db();
	$sql = "select m.ME_NO,m.ME_ADDR,m.ME_TEL,m.ME_CONTAAD,m.ME_CONTATE,m.ME_SPONSOR,m.ME_RELAT,m.ME_RADDR,m.ME_RTEL from m_employ m join emplyee e on(e.empID=m.ME_NO)";
	$rs = $db->query($sql);
	
	while ($r = $db->fetch_array($rs)) {
		$addr = address($r['ME_ADDR']);
		$db->table = 'emplyee';
		$db->row = array('county'=>"'".$addr['county']."'",'city'=>"'".$addr['city']."'",'address'=>"'".$addr['address']."'");
		$db->update("empID='".$r['ME_NO']."'");

		$db->table = 'community';
		unset($db->row);
		$addr = address($r['ME_CONTAAD']);
		$db->row = array('empID'=>"'".$r['ME_NO']."'",'title'=>"'現居通訊處'",'county'=>"'".$addr['county']."'",'city'=>"'".$addr['city']."'",'address'=>"'".$addr['address']."'",'tel'=>"'".$r['ME_CONTATE']."'");
		$db->insert();

		unset($db->row);

		if(empty($r['ME_SPONSOR']) || empty($r['ME_RELAT'])  || empty($r['ME_RADDR'])) continue;
		$db->table = 'community';
		$addr = address($r['ME_RADDR']);
		$db->row = array('empID'=>"'".$r['ME_NO']."'",'title'=>"'".$r['ME_RELAT']."－".$r['ME_SPONSOR']."'",'county'=>"'".$addr['county']."'",'city'=>"'".$addr['city']."'",'address'=>"'".$addr['address']."'",'tel'=>"'".$r['ME_RTEL']."'");
		$db->insert();
		unset($db->row);
	}


	function address($add){
		$pos = new db();
		global $county;
		$county['BB'] = "台中縣";
		$county['BBA'] = "臺中縣";
		$county['BBB'] = "臺中市";
		$county['HH'] = "桃園縣";
		$county['DD'] = "台南縣";
		$county['AA'] = "臺北市";
		$data = array('county'=>'','city'=>'','address'=>'');
		foreach ($county as $key => $value) {
			if (preg_match("/$value/i", $add)){
				$data['county'] = $key;
				$add = trim(str_replace($value,'',$add));
			}
		}

		$sql = "select Name from ptycd3";
		$posRs = $pos->query($sql);
		$posR = $pos->fetch_all($posRs);
		foreach ($posR as $key => $value){
			if (preg_match("/{$value['Name']}/i", $add)){
				$data['city'] = $value['Name'];
				$add = trim(str_replace($value['Name'],'',$add));
			}
		}

		$data['address'] = $add;

		switch ($data['county']) {
			case 'AA':
				$data['county'] = 'A'; break;
			case 'BB':
			case 'BBA':
			case 'BBB':
				$data['county'] = 'B'; break;
			case 'HH':
				$data['county'] = 'H'; break;
			case 'DD':
				$data['county'] = 'D'; break;
		}

		return $data;
	}
?>