<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}

	function clickEdit(aid) {	
		location.href='edit.php?wkYear='+aid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}	

	function doSubmit() {
		form1.submit();
	}

	
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body bgcolor="#EEEEEE">
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <input type="button" name="edit" value="－查閱" class="sBtn" onClick="DoEdit()">

     年度:<select name="year" id="year" onChange="doSubmit()">
     	<? foreach ($yearList as $i => $value) { 
     		$sel = ($_REQUEST['year'] == $i) ? 'selected': '';
     		echo "<option $sel value='$i'>$value</option>"; 
     	} ?>
     		
     	</select>

  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr bgcolor="#336699" height="25" valign="bottom" align="center" bordercolordark="#336699">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
if(!isset($sortDirection)) $sortDirection = 'desc';
	//過濾條件
	if(isset($filter)) $sql="select e.*, (e.wkYear-1911) as year from $tableName  where ($filter)"; else $sql="select * from $tableName where 1";
  // 搜尋處理
  if ($keyword!="") $sql.=" and ($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')";
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize"; 
  //echo $sql;
  $rs = db_query($sql,$conn);
  if($rs){
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?> >
			<a href="javascript: clickEdit('<?=$id+1911?>')"><?=$id?></a>
    </td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td  <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "select": 	$tm=$jobStatusChange; echo $tm[$fldValue]; break;
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo $fldValue&&$fldValue!='0000-00-00'?date('Y/m/d',strtotime($fldValue)):'&nbsp;'; break;
						case "custom_date" : echo date('Y/m/d',strtotime($r['bdate'])).'-'.date('Y/m/d',strtotime($r['edate'])); break;
						default : 
							echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
  </tr>    
<? } 
  }?>   
 </table>
</form>
</body>
</Html>