<?php
	header("Content-type: text/html; charset=utf-8");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=".date("m/d").".xlsx");
	header("Pragma:no-cache");
	header("Expires:0");
	
	//include class
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../inc_vars.php';
	require_once '../Classes/PHPExcel.php';
	require_once '../Classes/PHPExcel/IOFactory.php';

	//設定變數
	$objPHPExcel = new PHPExcel();
	$sqlExcel = str_replace('DESC', 'ASC', $_POST['sql']);
	//選擇的工作表Title
	if(!empty($_POST['checkTitle'])) $title = explode(",", $_POST['checkTitle']); 
	//選擇的基本資料
	if(!empty($_POST['basicInfo'])) $basicInfo = explode(",", $_POST['basicInfo']);
	if(!empty($_POST['comInfo'])) $comInfo = explode(",", $_POST['comInfo']);
	if(!empty($_POST['expInfo']) || $_POST['expInfo'] == '0') $expInfo = explode(",", $_POST['expInfo']);
	if(!empty($_POST['eduInfo']) || $_POST['expInfo'] == '0') $eduInfo = explode(",", $_POST['eduInfo']);
	if(!empty($_POST['licInfo'])) $licInfo = explode(",", $_POST['licInfo']);

	$impGrade = array('0'=>'','1'=>'輕度','2'=>'中度','3'=>'重度','4'=>'極重度');
	$gradeHigh = array('0'=>'一般','1'=>'最高','2'=>'次高');//學歷
	$expHigh = array('0'=>'一般','1'=>'主要','2'=>'次要');//經歷
	$source = array('0'=>'本會自籌','1'=>'自費','2'=>'政府/民間全額補助','3'=>'政府/民間部分補助','4'=>'免費');//經費來源
	$sqlOrder = 'order by empID';

//基本資料---------------------------------------------------------------------------

	//部門
	$sql = 'select depID,depTitle,depName from department';
	$rs = db_query($sql);
	$depId = array();
	while($r = db_fetch_array($rs)){
		$depId[$r['depID']] = $r['depTitle'];
		$depClass[$r['depID']] = $r['depName'];
	}

	//職銜
	/*$sql = 'select jobID,jobTitle from jobs';
	$rs = db_query($sql);
	$jobId = array();
	while ($r = db_fetch_array($rs)) $jobId[$r['jobID']] = $r['jobTitle'];*/
	//BASIC's TITLE
	$basic = array(
		'empID'=>array('val'=>'編號','width'=>8),
		'empName'=>array('val'=>'中文姓名','width'=>10),
		'empEn'=>array('val'=>'英文姓名','width'=>20),
		'sex'=>array('val'=>'性別','width'=>8),
		'depClass'=>array('val'=>'管理單位','width'=>20),
		'depID'=>array('val'=>'管理組別','width'=>40),
		'jobID'=>array('val'=>'職銜','width'=>18),
		'perID'=>array('val'=>'身分字號','width'=>12),
		'birthday'=>array('val'=>'生日','width'=>11),
		'jobType'=>array('val'=>'身分別','width'=>12),
		'jobLevel'=>array('val'=>'職等','width'=>8),
		'salaryAct'=>array('val'=>'薪資帳號','width'=>15),
		'impaired'=>array('val'=>'障別','width'=>15),
		'blood'=>array('val'=>'血型','width'=>8),
		'jobClass'=>array('val'=>'類型','width'=>17),
		'hireDate'=>array('val'=>'到職日','width'=>11),
		'leaveDate'=>array('val'=>'離職日','width'=>11),
		'isOnduty'=>array('val'=>'職況','width'=>8),
		'tel'=>array('val'=>'電話','width'=>21),
		'mobile'=>array('val'=>'行動電話','width'=>17),
		'email'=>array('val'=>'E-Mail','width'=>33),
		'contactMan'=>array('val'=>'連絡人','width'=>14),
		'relation'=>array('val'=>'連絡人關係','width'=>12),
		'county'=>array('val'=>'員工戶籍','width'=>80),
		'Marriage'=>array('val'=>'婚姻','width'=>8),
		'spHolidays'=>array('val'=>'特休時數','width'=>10),
		'grants'=>array('val'=>'補助類別','width'=>10),
		'projID'=>array('val'=>'計畫編號','width'=>10),
		'noSignIn'=>array('val'=>'是否打卡','width'=>10),
		'donate'=>array('val'=>'固定捐款','width'=>10),
		'tax'=>array('val'=>'所得稅金額','width'=>12));

	//抓資料
	if(strpos($sqlExcel,'emplyee_education')){
		foreach($basicInfo as $key => $value) if(!empty($value)) $bif[$key] = 'emplyee.'.$value;
	}else $bif = $basicInfo;

	$filter = implode(',', array_filter($bif));
	if(strpos($filter,'depClass') != false){
		$filter = str_replace(',depClass','',$filter);
	}
	$sqlBasic = str_replace('*', $filter, $sqlExcel);
	$rsBasic = db_query($sqlBasic);
	$rowBasic = array();
	while ($rBasic = db_fetch_array($rsBasic)){
		$rowBasic[$rBasic['empID']] = $rBasic;
		if(!empty($rBasic['depID'])){
			$rowBasic[$rBasic['empID']]['depClass'] = $depClass[substr($rBasic['depID'],0,1)];
		}
	}

	//創建基本資料工作表
	$objPHPExcel->setActiveSheetIndex(0);
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->setTitle('基本資料');

	//基本資料內容
	$objPHPExcel->setActiveSheetIndex(0);
	$a = 'A'; // excel $a欄位  => A欄位 => A1
	foreach ($basicInfo as $key => $value) {
		if($value == 'address' || $value == 'city' || $value == 'impGrade') continue; //重複戶籍跳過迴圈
		$i = 1;  // 列編號  $a.$i = A1 ;
		$sheet->setCellValue($a.$i, $basic[$value]['val']);
		$sheet->getColumnDimension($a)->setWidth($basic[$value]['width']);
		foreach ($rowBasic as $k => $v) {
			$i++;
			$showVal = '';
			switch ($value) {
				case 'empID':
				case 'salaryAct':
				case 'tel':
				case 'mobile':
				case 'projID':
					$sheet->setCellValueExplicit($a.$i, $v[$value]); 
					//將數值欄位轉型字串
					break;
				case 'depID':
					$showVal = (isset($depClass[$v[$value]])) ? $depClass[$v[$value]] : '';
					$sheet->setCellValue($a.$i, $showVal);
					break;
				case 'jobID':
					$showVal = (isset($jobinfo[$v[$value]])) ? $jobinfo[$v[$value]] : '';
					$sheet->setCellValue($a.$i, $showVal);
					break;
				case 'jobType':
					//print_r($value.'<br>'.$v[$value].'<br>');
					$showVal = (isset($jobType[$v[$value]])) ? $jobType[$v[$value]] : '';
					$sheet->setCellValue($a.$i, $showVal);
					break;
				case 'impaired':
					$impStr = $impaired[$v[$value]].$impGrade[$v['impGrade']];
					$sheet->setCellValue($a.$i, $impStr);
					break;
				case 'isOnduty':
					$sheet->setCellValue($a.$i, $jobStatus[$v[$value]]);
					break;
				case 'county':
					$addStr = $county[$v[$value]].$v['city'].$v['address'];
					$sheet->setCellValue($a.$i, $addStr);
					break;
				case 'noSignIn':
					$v[$value] ==1 ? $sheet->setCellValue($a.$i, '不需打卡') : $sheet->setCellValue($a.$i, '需打卡');
					break;
				case 'leaveDate':
					$showDate = (empty($v[$value]) || $v[$value] == '0000-00-00') ? '' : $v[$value];
					$sheet->setCellValue($a.$i, $showDate);
					break;
				default:
					$sheet->setCellValue($a.$i, $v[$value]);
					break;
			}
		}
		$a++;
	}

//其他資料工作表---------------------------------------------------------------------

	$shetI=1; //sheet Index ;
	foreach ($title as $key => $value){
		if($value =='基本資料') continue;
		//創建工作表
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex($shetI);
		$sheet = $objPHPExcel->getActiveSheet($shetI);
		$shetI++;
		$sheet->setTitle($value); //設定sheet名稱
		$titleArr = array();
		$tabFilter = array();
		switch ($value) {
			case '通訊資料':
				if(empty($comInfo)) break;
				$titleArr = array(
					'empID'=>array('val'=>'編號','width'=>8),
					'empName'=>array('val'=>'中文姓名','width'=>10),
					'title'=>array('val'=>'名稱','width'=>13),
					'email'=>array('val'=>'E-Mail','width'=>30),
					'tel'=>array('val'=>'電話','width'=>12),
					'mobile'=>array('val'=>'手機','width'=>12),
					'county'=>array('val'=>'縣市','width'=>8),
					'city'=>array('val'=>'區域','width'=>8),
					'address'=>array('val'=>'地址','width'=>43));
				$tabName = 'community';
				$sqlOrder = 'order by empID,rDate';
				foreach ($comInfo as $key => $value) $tabFilter[$value] = "title = '$value'";
				break;
			case '經歷':
				if(empty($expInfo)) break;
				$titleArr = array(
					'empID'=>array('val'=>'編號','width'=>8),
					'empName'=>array('val'=>'中文姓名','width'=>10),
					'title'=>array('val'=>'公司名稱','width'=>39),
					'job'=>array('val'=>'職稱','width'=>22),
					'peoples'=>array('val'=>'人數','width'=>8),
					'isHigh'=>array('val'=>'經歷','width'=>8),
					'content'=>array('val'=>'工作描述','width'=>36),
					'bdate'=>array('val'=>'期間(起)','width'=>11),
					'edate'=>array('val'=>'期間(迄)','width'=>11),
					'response'=>array('val'=>'管理責任','width'=>10),
					'notes'=>array('val'=>'備註','width'=>58));
				$tabName = 'emplyee_experience';
				foreach ($expInfo as $key => $value) $tabFilter[$value] = "isHigh = '$value'";
				break;
			case '學歷':
				if(empty($eduInfo)) break;
				$titleArr = array(
					'empID'=>array('val'=>'編號','width'=>8),
					'empName'=>array('val'=>'中文姓名','width'=>10),
					'title'=>array('val'=>'學校名稱','width'=>48),
					'grade'=>array('val'=>'學歷程度','width'=>10),
					'subject'=>array('val'=>'科系','width'=>31),
					'bdate'=>array('val'=>'期間(起)','width'=>11),
					'edate'=>array('val'=>'期間(迄)','width'=>11),
					'isHigh'=>array('val'=>'學歷','width'=>8),
					'states'=>array('val'=>'狀態','width'=>8),
					'notes'=>array('val'=>'備註','width'=>42));
				$tabName = 'emplyee_education';
				foreach ($eduInfo as $key => $value) $tabFilter[$value] = "isHigh = '$value'";
				break;
			case '證照':
				if(empty($licInfo)) break;
				$titleArr = array(
					'empID'=>array('val'=>'編號','width'=>8),
					'empName'=>array('val'=>'中文姓名','width'=>10),
					'title'=>array('val'=>'名稱','width'=>39),
					'adate'=>array('val'=>'取得日期','width'=>11),
					'vdate'=>array('val'=>'有效日期','width'=>11),
					'type'=>array('val'=>'形式','width'=>8),
					'notes'=>array('val'=>'備註','width'=>40));
				$tabName = 'emplyee_license';
				foreach ($licInfo as $key => $value) $tabFilter[$value] = "type = '$value'";
				break;
			case '教育訓練':
				$titleArr = array(
					'empID'=>array('val'=>'編號','width'=>8),
					'empName'=>array('val'=>'中文姓名','width'=>10),
					'title'=>array('val'=>'課程名稱','width'=>81),
					'trainType'=>array('val'=>'內/外訓','width'=>8),
					'trainClass'=>array('val'=>'課程分類','width'=>10),
					'bdate'=>array('val'=>'訓練起日','width'=>11),
					'edate'=>array('val'=>'訓練迄日','width'=>11),
					'hours'=>array('val'=>'時數','width'=>8),
					'trainUnit'=>array('val'=>'辦訓單位','width'=>75),
					'source'=>array('val'=>'經費來源','width'=>20),
					'fee'=>array('val'=>'費用','width'=>8));
				$tabName = 'emplyee_training';
				$tabFilter = '';
				break;
			case '薪資':
				$titleArr = array(
					'empID'=>array('val'=>'編號','width'=>8),
					'empName'=>array('val'=>'中文姓名','width'=>10),
					'changeDate'=>array('val'=>'變更起日','width'=>11),
					'salaryType'=>array('val'=>'給薪類別','width'=>10),
					'salary'=>array('val'=>'薪資','width'=>10),
					'hourfee'=>array('val'=>'時薪','width'=>10),
					'ratio'=>array('val'=>'工時比例','width'=>10),
					'capacity_ratio'=>array('val'=>'產能敘薪比例','width'=>15),
					// 'post'=>array('val'=>'職務加點','width'=>10),
					// 'food'=>array('val'=>'伙食加點','width'=>10),
					// 'manager'=>array('val'=>'主管加點','width'=>10),
					// 'edu'=>array('val'=>'學歷加點','width'=>10),
					// 'license'=>array('val'=>'證照加點','width'=>10),
					// 'seniority'=>array('val'=>'年資加點','width'=>10),
					// 'response'=>array('val'=>'責任加點','width'=>10),
					// 'allowance'=>array('val'=>'相關津貼','width'=>10),
					// 'special'=>array('val'=>'特別加點','width'=>10),
					// 'transform'=>array('val'=>'轉換點數','width'=>10),
					// 'sums'=>array('val'=>'合計點數','width'=>10),
					'notes'=>array('val'=>'異動說明','width'=>50));
				$tabName = 'emplyee_salary';
				$tabFilter = array("date(changeDate)<='".date('Y-m-d')."'");
				$sqlOrder = 'order by empID asc,changeDate desc Limit 1';
				break;
		}

		//sql 語法
		if(strpos($sqlExcel,'emplyee_education')){
			$sql = str_replace('*', 'emplyee.empID,emplyee.empName', $sqlExcel);
		}else $sql = str_replace('*', 'empID,empName', $sqlExcel);
		$row = array();
		$rs = db_query($sql);

		while ($r = db_fetch_array($rs)){
			if(!empty($tabFilter)){
				$sqlAll = "select * from $tabName where empID = '{$r['empID']}' and (".implode(' or ', $tabFilter).") $sqlOrder";
			}else $sqlAll = "select * from $tabName where empID = '{$r['empID']}' $sqlOrder";
			$rsAll = db_query($sqlAll);
			$name[$r['empID']] = $r['empName'];
			while ($rAll = db_fetch_array($rsAll)) $row[$rAll['empID']][$rAll['id']] = $rAll;
		}

		$a = 'A'; // excel $a欄位  => A欄位 => A1
		foreach ($titleArr as $k => $v) {
			$i = 1;
			$sheet->setCellValue($a.$i, $titleArr[$k]['val']);
			$sheet->getColumnDimension($a)->setWidth($titleArr[$k]['width']);
			foreach ($row as $k1 => $v1) {
				foreach ($v1 as $k2 => $v2) {
					$i++;
					$showVal = '';
					switch ($k){
						case 'adate':
						case 'vdate':
						case 'bdate':
						case 'edate':
							if($row[$k1][$k2][$k] == '0000-00-00' || $row[$k1][$k2][$k] == '' || $row[$k1][$k2][$k] == null){
								$sheet->setCellValue($a.$i,'');
							}else{
								$sheet->setCellValue($a.$i,$row[$k1][$k2][$k]);
							}
							break;
						case 'empID':
						case 'tel':
						case 'mobile':
							$sheet->setCellValueExplicit($a.$i, $row[$k1][$k2][$k]);
							break;
						case 'peoples':
							$showVal = (isset($peoples[$row[$k1][$k2][$k]])) ? $peoples[$row[$k1][$k2][$k]] : '';
							$sheet->setCellValue($a.$i, $showVal);
							break;
						case 'county':
							$showVal = (isset($county[$row[$k1][$k2][$k]])) ? $county[$row[$k1][$k2][$k]] : '';
							$sheet->setCellValue($a.$i, $showVal);
							break;
						case 'grade':
							$showVal = (isset($grade[$row[$k1][$k2][$k]])) ? $grade[$row[$k1][$k2][$k]] : '';
							$sheet->setCellValue($a.$i, $showVal);
							break;
						case 'isHigh':
							if($tabName == 'emplyee_education'){
								$showVal = (isset($gradeHigh[$row[$k1][$k2][$k]])) ? $gradeHigh[$row[$k1][$k2][$k]] : '';
							}else if($tabName == 'emplyee_experience'){
								$showVal = (isset($expHigh[$row[$k1][$k2][$k]])) ? $expHigh[$row[$k1][$k2][$k]] : '';								
							}
							$sheet->setCellValue($a.$i, $showVal);
							break;
						case 'trainType':
							$showVal = (isset($trainType[$row[$k1][$k2][$k]])) ? $trainType[$row[$k1][$k2][$k]] : '';
							$sheet->setCellValue($a.$i, $showVal);
							break;
						case 'trainClass':
							$showVal = (isset($trainClass[$row[$k1][$k2][$k]])) ? $trainClass[$row[$k1][$k2][$k]] : '';
							$sheet->setCellValue($a.$i, $showVal);							
							break;
						case 'source':
							$showVal = (isset($source[$row[$k1][$k2][$k]])) ? $source[$row[$k1][$k2][$k]] : '';
							$sheet->setCellValue($a.$i, $showVal);	
							break;
						case 'empName':
							$showVal = (isset($name[$k1])) ? $name[$k1] : '';
							$sheet->setCellValue($a.$i, $showVal);	
							break;
						case 'salaryType':
							$showVal = (isset($salaryType[$row[$k1][$k2][$k]])) ? $salaryType[$row[$k1][$k2][$k]] : '';
							$sheet->setCellValue($a.$i, $showVal);	
							break;
						case 'salary':
							if($row[$k1][$k2]['salaryType'] != '2'){
								$sheet->setCellValueExplicit($a.$i, round($row[$k1][$k2][$k]*$row[$k1][$k2]['ratio']));
							}else{
								$sheet->setCellValueExplicit($a.$i, round($row[$k1][$k2][$k]*$row[$k1][$k2]['ratio']*$row[$k1][$k2]['capacity_ratio']));
							}
							break;
						case 'capacity_ratio':
							if($row[$k1][$k2]['salaryType'] == '2') $sheet->setCellValueExplicit($a.$i, $row[$k1][$k2][$k]); 
							else  $sheet->setCellValueExplicit($a.$i, 0); 
							break;
						default:
							$sheet->setCellValue($a.$i, $row[$k1][$k2][$k]);
							break;
					}
				}
			}
			$a++;
		}	
	}
	$objPHPExcel->setActiveSheetIndex(0);
	
	//Save Excel 2007 file 保存
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');
?>