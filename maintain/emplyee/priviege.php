<?php
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../inc_vars.php';
	$rsT = db_query("select * from docclasses");
	while( $rT=db_fetch_array($rsT) ) $docTitle[$rT['ID']]=$rT['Title'];
	$rsT = db_query("select * from kwgclasses");
	while( $rT=db_fetch_array($rsT) ) $kwgTitle[$rT['ID']]=$rT['Title'];
	$rsT = db_query("select * from infoclasses");
	while( $rT=db_fetch_array($rsT) ) $infoTitle[$rT['ID']]=$rT['Title'];	

	$funcs = array(
	 	'A' => '基本資料維護','A1'=>'部門 資料維護','A2'=>'職掌 資料維護','A3'=>'員工 資料查詢','A5'=>'年資/特休','A6'=>'特休信件',
		'B' => '制度規章維護','B1'=>'制度規章 分類表','B2'=>'發佈 制度規章',
		'C' => '知識平台維護','C1'=>'知識平台 分類表','C2'=>'發佈 知識平台',
		'F' => '最新消息','J' => '保險系統',
		'K' => '請假系統','K1' => '綜合申請單','K2' => '外出單',
		'L' => '資訊查閱維護','L1'=>'資訊查閱 分類表','L2'=>'發佈 資訊查閱',
		'M' => '資源管理維護','M1'=>'資源管理 分類表','M2'=>'發佈 資源內容',
		'N' => '影像事件管理','N1'=>'影像事件分類表','N2'=>'發佈影像事件',
		'O' => '員工排班作業',
		'P' => '雜項費用',
		'Q' => '薪資作業',
		'R' => '打卡系統','R1'=>'打卡資料','R2'=>'員工識別證清單','R3'=>'匯入打卡資料','R4'=>'出勤資料',
		'S' => '出納作業',
		'Z' => '預算管理系統',	'Z1'=> '公文系統','Z2'=> '財管系統'
	);
	
	$eid = $_REQUEST['eid'];
	$pvfn = '../../maintainUser/previege/'.$eid.'.txt';
	$csfn = '../../maintainUser/previege/'.$eid.'_class.txt';
	
	//:: Database handles
	if(isset($_REQUEST['DoSubmit'])) {
		foreach($_REQUEST as $k=>$v) {
			$varAR = explode('~',$k);
			if($varAR[1]) $prvilige[] = $varAR[1];
			if($k=='NewFunc') {
				$varAR2 = explode('~',$v);
				if($varAR2[1]) $prvilige[] = $varAR2[1];
			}
			if($k=='docID') $classdef[]=$_REQUEST[$k];
			if($k=='kwgID') $classdef[]=$_REQUEST[$k];
			if($k=='depID') $classdef[]=$_REQUEST[$k];
			if($k=='insTbl') {
				if(count($_REQUEST[$k]) > 1) {
					unset($_REQUEST[$k][0]);
					$classdef[]= join(',',$_REQUEST[$k]);
				} else {
					$classdef[] = '';
				}
				
			}
			if($k=='infoID') $classdef[]=$_REQUEST[$k];
			if($k=='depCenter') $classdef[]=$_REQUEST[$k];
			if($k=='bank') {
				$bank = array();
				foreach ($v as $key => $value) {
					if($value != '' && !in_array($value, $bank)) $bank[] = $value;
				}
				if(count($bank) > 0) $classdef[] = $bank;
				else $classdef[] = '';
			}
			if($k=='projID') {
				$projID = array();
				foreach ($v as $key => $value) {
					if($value != '' && !in_array($value, $projID)) $projID[] = $value;
				}
				if(count($projID) > 0) $classdef[] = $projID;
				else $classdef[] = '';
			}
		}
		if($prvilige)	$JSstr=json_encode($prvilige); else $JSstr='[]';
		file_put_contents($pvfn,$JSstr);
		if($classdef){$JSstr=json_encode($classdef); file_put_contents($csfn,$JSstr);}
	}
	
	if(!file_exists($pvfn)) file_put_contents($pvfn,'');
	$str = file_get_contents($pvfn);
	if($str) {
		$priviege = json_decode($str);
		sort($priviege);
	} else $priviege=array();
	$unList = array_diff(array_keys($funcs),$priviege); 
	
	if(!file_exists($csfn)) file_put_contents($csfn,'');
	$str = file_get_contents($csfn); 
	if($str) $classdef = json_decode($str); else $classdef=array(); 
	/*echo "<pre>";
	print_r($classdef);*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>等級功能設定</title>
	<link rel="stylesheet" href="list.css" />
	<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>   
	<script src="/Scripts/form.js" type="text/javascript"></script>  
	<script>
	function addNewFunc(btn,level) {
		var fc = $(btn).prev().val(); 
		$('#NewFunc').val('fc~'+fc);
		$('#DoSubmit').click();
	}
	function addItem(tbtn,fld) {
		var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='3'>"
			+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode;
		$(elm).remove();
	}

	function addBank(obj) {
		if($(obj).html() == '+新增') {
			var html = $('[data-bank]').eq(0).html();
			html = html.replace('+新增', '-移除');
			html = html.replace('selected', '');
			html = '<div data-bank="" style="margin-bottom: 5px;">'+html+'</div>';
			$('#tdBank').append(html);
		} else {
			$(obj).parent('div').remove();
		}
		
	}

	function addProj(obj) {
		if($(obj).html() == '+新增') {
			var html = $('[data-proj]').eq(0).html();
			html = html.replace('+新增', '-移除');
			html = '<div data-proj="" style="margin-bottom: 5px;">'+html+'</div>';
			$('#tdProjID').append(html);
			$('[name="projID[]"]:last').val('');
		} else {
			$(obj).parent('div').remove();
		}
		
	}

	</script>
<style type="text/css">
body { margin:0 }
.colLabel { background-color:#e0e0e0; }
</style>
</head> 
<body bgcolor="#F0F0F0">
<form id="funcFM" name="funcFM" method="post" action="priviege.php" onsubmit="return true">
<table width="100%">
<tr valign="top">

  <td width="300"><table class="sText" width="100%">
    <tr><td class="colLabel">制度規章維護分類</td></tr>
    <tr><td><input name="docID" type="text" class="queryID" id="docID" size="3" value="<?php echo isset($classdef[0])? $classdef[0] : ''?>" title="<?php echo (isset($classdef[0]) && isset($docTitle[$classdef[0]]))?$docTitle[$classdef[0]]:''?>"/></td></tr>
    <tr><td class="colLabel">知識平台維護分類</td></tr>
    <tr><td><input name="kwgID" type="text" class="queryID" id="kwgID" size="3" value="<?php echo  isset($classdef[1])?$classdef[1]:''?>" title="<?php echo (isset($classdef[1]) && isset($kwgTitle[$classdef[1]]))?$kwgTitle[$classdef[1]]:''?>"/></td></tr>
    <tr valign="top"><td class="colLabel">可維護人員之部門</td></tr>
    <tr><td>
      <?php 
      	$aa = isset($classdef[2]) ? $classdef[2] : array() ; 
      ?>
      <input name="depID[]" type="text" class="queryID" id="depID" size="3" value="<?php echo isset($aa[0]) ? $aa[0] : '';?>" title="<?php echo isset($aa[0]) ? $departmentinfo[$aa[0]] : '';?>"/> <span class="btn" onClick="addItem(this,'depID')"> +新增</span>
      <?php for($i=1; $i<count($aa); $i++) {
         echo "<div><input name='depID[]' id='depID' type='text' size='3' value='$aa[$i]'>"
         . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$departmentinfo[$aa[$i]]."</span>"
         . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
      } ?>
    </td></tr>    
    <tr><td class="colLabel">可維護人員的保險</td></tr>
    <tr><td>
    		<input type="checkbox" name="insTbl[]" value="0" checked style="display:none;">	
			<?php
			$classdef[3] = isset($classdef[3]) ? $classdef[3] : '';
			 foreach($insTbl as $k=>$v) echo "<input type='checkbox' name='insTbl[]' value='$k'".( strpos($classdef[3],$k)===false?'':' checked ')."/>$v" ?>
    </td></tr>
 		<tr><td class="colLabel">資訊查閱維護分類</td></tr>
    <tr><td><input name="infoID" type="text" class="queryID" id="infoID" size="3" value="<?php echo isset($classdef[4]) ? $classdef[4] : ''?>" title="<?php echo isset($classdef[4]) ? $infoTitle[$classdef[4]] : ''?>"/></td></tr> 
    <tr valign="top"><td class="colLabel">可維護之機構</td></tr>
    <tr><td>
    <select id="depCenter" name="depCenter"><option></option>
    <?php 
			$rsX = db_query("select * from permits where isCenter=1");
			while($rX = db_fetch_array($rsX)) echo "<option value='".$rX['id']."' ".($classdef[5]==$rX['id']?"selected":"").">".$rX['title']."</option>"; 
		?>
    </select>
    </td></tr>
    <tr><td class="colLabel">可維護銀行帳號</td></tr>
    <tr >
    	<td id="tdBank">

    		<div data-bank="" style="margin-bottom: 5px;">
	    		<select name="bank[]" >
	    			<option></option>
	<?php
		$sqlBank = 'select * from psi_banks b left join psi_company c on(b.fid=c.id)';
		$rsBank = db_query($sqlBank);
		while ($rBank=db_fetch_array($rsBank)) {
			$selected = (isset($classdef[6][0]) && $classdef[6][0] == $rBank['projID']) ? 'selected' : '';
			$optionBank[$rBank['projID']] = $rBank['actno'].' ('.$rBank['title'].')';
			echo '<option value="'.$rBank['projID'].'" '.$selected.'>'.$rBank['actno'].' ('.$rBank['title'].')'.'</option>';
		}
		
	?>
	    		</select>
	    		<span class="btn" onClick="addBank(this)">+新增</span>
	    	</div>
	<?php 
		$classdef[6] = (!empty($classdef[6])) ? $classdef[6] : array();
		foreach ($classdef[6] as $key => $value) {
			if($key == 0) continue;
	?>
			<div data-bank="" style="margin-bottom: 5px;">
	    		<select name="bank[]" >
	    			<option></option>
	<?php
			foreach ($optionBank as $kk => $rBank) {
				$selected = ($value == $kk) ? 'selected' : '';
				echo '<option value="'.$kk.'" '.$selected.'>'.$rBank.'</option>';
			}
	?>
				</select>
	    		<span class="btn" onClick="addBank(this)">-移除</span>
	    	</div>
	<?php
		}
	?>   	


    	</td>

    </tr>
     <tr><td class="colLabel">可維護計畫編號</td></tr>
     <tr>
     	<td id="tdProjID">
     		<div data-proj="" style="margin-bottom: 5px;">
     			<input type="text" name="projID[]" value="<?php echo isset($classdef[7][0]) ? $classdef[7][0] : '';?>">
     			<span class="btn" onClick="addProj(this)">+新增</span>
     		</div>
    <?php 
    	$classdef[7] = !empty($classdef[7]) ? $classdef[7] : array();
    	foreach($classdef[7] as $key => $value) {
    		if($key == 0) continue;
   	?>
    		<div data-proj="" style="margin-bottom: 5px;">
     			<input type="text" name="projID[]" value="<?php echo $value;?>">
     			<span class="btn" onClick="addProj(this)">-移除</span>
     		</div>
    <?php }?> 		
     	</td>
     </tr>
  </table></td>

	<td><table class="sText">
   <tr valign="middle">
    <td colspan="2">
    <?php
      $char = '';	//funtions list char 1
      foreach($priviege as $v) {
        if(!$char) {
          $char = substr($v,0,1);
          echo "<input type='checkbox' name='fc".'~'."$v' checked>$funcs[$v]"; 
        } else {
          if($char!=substr($v,0,1)) { echo '<br>'; $char=substr($v,0,1); }
          echo "<input type='checkbox' name='fc".'~'."$v' checked>$funcs[$v]"; 
        }
      }
    ?>
    </td>
   </tr>
  <?php if( count($unList) > 0 ) { ?>
  <tr>
    <td colspan="2"><select size="1"><?php foreach($unList as $vv) echo "<option value='$vv'>$funcs[$vv]</option>" ?></select> <span onclick="addNewFunc(this,'<?php echo $k?>')">[ 新增 ]</span></td>
  </tr>
  <?php } ?>
  </table><br />
  <div>
    <input type="hidden" name="eid" value="<?php echo $eid?>" />
    <input type="hidden" id="NewFunc" name="NewFunc" value="" />
    <input type="submit" id="DoSubmit" name="DoSubmit" value="確定" />
    <input type="reset" name="Reset" value="重設" />
  </div>  
	</td>
   
</tr></table>
</form>
</body>
</html>