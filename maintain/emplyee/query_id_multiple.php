<?php
	$host = '';
  if(isset($_GET['ishost'])) {
  	$host = explode('.', $_SERVER['HTTP_HOST']);
  	array_shift($host);
  	$host = implode('.', $host);
  }
?>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<title>員工代碼表</title>
	<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
	<link href="/system/TreeControl.css" rel="stylesheet">
	<style type="text/css">
		.selItem {background-color:#CCCCCC; color:#F90; }
		.gray{color:#999;}
		#TreeControl { border: none; } /* override */
	</style>
	<script src="/media/js/jquery-1.10.1.min.js"></script>
	<script src="/Scripts/jquery.treeview.js"></script>
	<script src="/system/TreeControl.js"></script>
	<script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = true;	//可複選
		var selObjs = new Array();
		
<?php if($host != '') { ?> document.domain="<?php echo $host;?>"; <?php } ?>
		//document.domain="cloudcode.com.tw";
		$(document).ready(function(){
			//selid = dialogArguments;
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
			
	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}
	  }
		function returnSeletions() {
			var data = [];
			$('input[name="empID[]"]:checked').each(function(e){
				data.push({'id':$(this).val(),'title':$(this).attr('title')});
			})

			window.opener.returnValueMultiple(data);
			window.opener.rzt = false;
			window.close();
		}		
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="0" cellspacing="0" bgcolor="#333333" width="100%">
<tr><th id="tblHead" bgcolor="#CCCCCC">::: 員工代碼表 :::</th></tr>
<tr><td bgcolor="#FFFFFF">
<?
	include "../../config.php";
	include "../../system/db.php";
	include "../../system/TreeControl.php";
	$dbop = array(
		'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
		'dbAccount'  => "root",
		'dbPassword' => "16657143"
	);
	$db = new db($dbop); 
	//:TreeControl ---------------------------------------------------------------------
	//$cWhereStr = $_REQUEST['lead']?"isOnduty=1 and jobID like 'G%'":"isOnduty=1";
	$whereStr = array();
	$cWhereStr = array("isOnduty = '1'");
	if(isset($_REQUEST['depID'])) {
		$depIDList = explode('-', $_REQUEST['depID']);
		$depID = array();
		foreach ($depIDList as $key => $value) {
			$depID[] = "'".$value."'";
		}
		$whereStr[] = "depID in (".implode(',', $depID).")";
		$cWhereStr[] = "depID in (".implode(',', $depID).")";
	}

	if(isset($_REQUEST['lead'])) {
		$cWhereStr[] = "jobID like 'G%' and jobID not in('G001', 'G002')";
	}

	if(isset($_REQUEST['jobID'])) {
		$jobIDList = explode('-', $_REQUEST['jobID']);
		$jobID = array();
		foreach ($jobIDList as $key => $value) {
			$jobID[] = "'".$value."'";
		}
		$cWhereStr[] = "jobID in (".implode(',', $jobID).")";
	}

	$op = array(
		'ID'		=> 'depID',
		'Title'	=> 'depTitle',
		'TableName' => 'department',
		'RootClass' => 0,
		'RootTitle' => $CompanyName,
		'DB' => $db,
		'Where'=> implode(' and ', $whereStr),
		'Modal' => false,
		'orderBy' => 'depID',
		//'URLFmt' => "javascript:setID(\"%s\",\"%s\",this)",
		'ChildTbl'=>'emplyee',
		'FKey'=>'depID',
		'cID'=>'empID',
		'cWhere'=> implode(' and ', $cWhereStr),
		'cIcon'=>'people',
		'cTitle'=>'empName' 
	);
	new CboxTreeControl($op);
?>	
</td></tr>
</table>
</body>
</html>