<?php	
	// for Access Permission Control
	session_start();

	$user_classdef = isset($_SESSION['user_classdef'][2]) ? json_encode($_SESSION['user_classdef'][2]) : '';
	

	// exit;
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");

	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "員工資料維護";
	$tableName = "emplyee";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee/';
	$delField = '';				//用ID存檔，沒有欄位
	$delFlag = true;
	$imgpath = '../../data/emplyee/';
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	$temp = array();
	if ($_SESSION['privilege'] > 10) {	//::check if from maintainUser 
		$bE = true;	//異動權限
		if(!empty($_REQUEST['depFilter'])) $temp[] = "depID like '".$_REQUEST['depFilter']."%'";
	}	else {
		if(!empty($_REQUEST['depFilter'])) $temp[] = "depID like '".$_REQUEST['depFilter']."%'";
		else{
			if(isset($_SESSION['user_classdef'][2])) {
				$aa = array();
				foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
				if(count($aa)) $temp[]="(".join(' or ',$aa).")"; else $temp[] = "depID=''";	
			} else $temp[] = "depID=''";	
		}
		
	}

	$depFilter = isset($_REQUEST['depFilter']) ? $_REQUEST['depFilter'] : '';
	$sttFilter = isset($_REQUEST['sttFilter']) ? $_REQUEST['sttFilter'] : '1';
	$jobFilter = isset($_REQUEST['jobFilter']) ? $_REQUEST['jobFilter'] : '';
	$typFilter = isset($_REQUEST['typFilter']) ? $_REQUEST['typFilter'] : '';


	if(!empty($_REQUEST['jobFilter'])) $temp[] = "jobID='".$_REQUEST['jobFilter']."'";	
	if(!empty($_REQUEST['typFilter'])) $temp[] = "jobType='".$_REQUEST['typFilter']."'";

	if(isset($_REQUEST['sttFilter']) && $_REQUEST['sttFilter']!='') $temp[] = "isOnduty=".$_REQUEST['sttFilter']; 
	else $temp[] = "isOnduty=1";
	
	if(count($temp)>0) $filter = join(' and ',$temp);
	//::for advenced search
	if(isset($_REQUEST['advFilter'])) {
		if ($_SESSION['privilege'] > 10) {
			// 把*取代成%
			$_REQUEST['advFilter'] = str_replace ('*','%',$_REQUEST['advFilter']);
			
			$filter = $_REQUEST['advFilter'];
		} else {
			// 把*取代成%
			$_REQUEST['advFilter'] = str_replace ('*','%',$_REQUEST['advFilter']);

			$filter = $_REQUEST['advFilter']." and (".join(' or ',$aa).")"; 
		}
	}

	/* $PID = 'depID';	$CLASS_PID = 'emplyee_depID';
	// Here for List.asp ======================================= //
	if($_SESSION['domain_Host']) $RootClass = $_SESSION['domain_Host']; else $RootClass = '';
	if(isset($_REQUEST[filter])) $_SESSION[$CLASS_PID]=$_REQUEST[filter]; 
	if(!$_SESSION[$CLASS_PID]) $_SESSION[$CLASS_PID]=$RootClass;
	if($RootClass) $filter = "$PID='$_SESSION[$CLASS_PID]'"; 
	*/
	//::	----------------------------------------------------------------------------------- <
		
	$defaultOrder = "empID";
	$searchField1 = "empID";
	$searchField2 = "empName";
	$searchField3 = "depID";
	$pageSize = 10;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"empID,empName,empEn,depID,jobID,sex,birthday,hireDate,leaveDate,rdate");
	$ftAry = explode(',',"編號,中文姓名,英文姓名,部門,職銜,性別,生日,到職日,離職日,登錄時間");
	$flAry = explode(',',",,,,,,,,,");
	$ftyAy = explode(',',"ID,text,text,deptitle,jobtitle,text,date,date,date,date");

?>