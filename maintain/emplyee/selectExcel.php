<?php 
	include '../inc_vars.php';
	$gradeHigh = array('0'=>'一般','1'=>'最高','2'=>'次高');
	$expHigh = array('0'=>'一般','1'=>'主要','2'=>'次要');
	$licForm = array('專業'=>'專業','政府'=>'政府','一般'=>'一般','期刊'=>'期刊','論文'=>'論文');
	$basic = array('empID'=>'編號','empName'=>'中文姓名','empEn'=>'英文姓名','sex'=>'性別','depClass,depID'=>'部門','jobID'=>'職銜','perID'=>'身分字號','birthday'=>'生日','jobType'=>'身分別','jobLevel'=>'職等','salaryAct'=>'薪資帳號','impaired,impGrade'=>'障別','blood'=>'血型','jobClass'=>'類型','hireDate'=>'到職日','leaveDate'=>'離職日','isOnduty'=>'職況','tel'=>'電話','mobile'=>'行動電話','email'=>'E-Mail','county,city,address'=>'員工戶籍','Marriage'=>'婚姻','contactMan'=>'連絡人','relation'=>'連絡人關係','spHolidays'=>'特休時數','grants'=>'補助類別','projID'=>'計畫編號','noSignIn'=>'是否打卡','donate'=>'固定捐款','tax'=>'所得稅金額');
?>
<link href="/css/bootstrap.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<div style="height:20px;"></div>
	<div class="container">
		<form name="form2" method="post" action="#">
			<label>
				<input id="bIfon" type="checkbox" name="checkTitle[]" value="基本資料">基本資料
			</label><br>
				<div id="basicItem" style="margin-left: 40px;display: none;">
					<div class="row">
						<label class="col-md-12">
							<input type="checkbox" onclick="checkAll(this,'basicInfo[]','basAllCheck',1)">
							<sapn id="basAllCheck">基本資料全選</sapn>
						</label>
					</div>
					<?php
						$i = 1;
						foreach ($basic as $key => $value) {
							if($i%4==1) echo '<div class="row">';
							echo '<label class="col-md-3">';
							if($key == 'empID' || $key == 'empName'){
								echo '<input type="checkbox" value="'.$key.'" disabled checked>';
							}else{
								echo '<input type="checkbox" name="basicInfo[]" value="'.$key.'">';
							}
							echo str_pad($value,12,"　");
							echo '</label>';
							if($i%4==0) echo '</div>';
							$i++;
						}
						if(count($basic)<4 || count($basic)%4!=0) echo '</div>';
					?>
				</div>
			<label>
				<input id="comIfon" type="checkbox" name="checkTitle[]" value="通訊資料">通訊資料
			</label><br>
				<div id="comItem" style="margin-left: 40px;display: none;">
					<div class="row">
						<label class="col-md-12">
							<input type="checkbox" onclick="checkAll(this,'comInfo[]','comAllCheck',2)">
							<sapn id="comAllCheck">通訊資料全選</sapn>
						</label>
					</div>
					<?php
						$i = 1;
						foreach ($comClass as $key => $value) {
							if($i%4==1) echo '<div class="row">';
							echo '<label class="col-md-3">';
							echo '<input type="checkbox" name="comInfo[]" value="'.$key.'">';
							echo $value;
							echo '</label>';
							if($i%4==0) echo '</div>';
							$i++;
						}
						if(count($comClass)<4 || count($comClass)%4!=0) echo '</div>';
					?>
				</div>
			<label>
				<input id="exper" type="checkbox" name="checkTitle[]" value="經歷">經歷
			</label><br>
				<div id="expItem" style="margin-left: 40px;display: none;">
					<div class="row">
						<label class="col-md-12">
							<input type="checkbox" onclick="checkAll(this,'expInfo[]','expAllCheck',3)">
							<sapn id="expAllCheck">經歷全選</sapn>
						</label>
					</div>
					<?php
						$i = 1;
						foreach ($expHigh as $key => $value) {
							if($i%4==1) echo '<div class="row">';
							echo '<label class="col-md-3">';
							echo '<input type="checkbox" name="expInfo[]" value="'.$key.'">';
							echo $value;
							echo '</label>';
							if($i%4==0) echo '</div>';
							$i++;
						}
						if(count($expHigh)<4 || count($expHigh)%4!=0) echo '</div>';
					?>
				</div>
			<label>
				<input id="educ" type="checkbox" name="checkTitle[]" value="學歷">學歷
			</label><br>
				<div id="eduItem" style="margin-left: 40px;display: none;">
					<div class="row">
						<label class="col-md-12">
							<input type="checkbox" onclick="checkAll(this,'eduInfo[]','eduAllCheck',4)">
							<sapn id="eduAllCheck">學歷全選</sapn>
						</label>
					</div>
					<?php
						$i = 1;
						foreach ($gradeHigh as $key => $value) {
							if($i%4==1) echo '<div class="row">';
							echo '<label class="col-md-3">';
							echo '<input type="checkbox" name="eduInfo[]" value="'.$key.'">';
							echo $value;
							echo '</label>';
							if($i%4==0) echo '</div>';
							$i++;
						}
						if(count($gradeHigh)<4 || count($gradeHigh)%4!=0) echo '</div>';
					?>
				</div>
			<label>
				<input id="lice" type="checkbox" name="checkTitle[]" value="證照">證照
			</label><br>
				<div id="licItem" style="margin-left: 40px;display: none;">
					<div class="row">
						<label class="col-md-12">
							<input type="checkbox" onclick="checkAll(this,'licInfo[]','licAllCheck',5)">
							<sapn id="licAllCheck">證照全選</sapn>
						</label>
					</div>
					<?php
						$i = 1;
						foreach ($licForm as $key => $value) {
							if($i%4==1) echo '<div class="row">';
							echo '<label class="col-md-3">';
							echo '<input type="checkbox" name="licInfo[]" value="'.$key.'">';
							echo $value;
							echo '</label>';
							if($i%4==0) echo '</div>';
							$i++;
						}
						if(count($licForm)<4 || count($licForm)%4!=0) echo '</div>';
					?>
				</div>
			<label>
				<input id="educTrain" type="checkbox" name="checkTitle[]" value="教育訓練">教育訓練
			</label><br>
			<label>
				<input id="salaryTrain" type="checkbox" name="checkTitle[]" value="薪資">
				薪資
			</label><br>
			<br>
			<button type="submit" onclick="returnPar()">匯出Excel</button>
			<button type="reset">清除</button>
		</form>
	</div>
<script src="../../Scripts/jquery-1.12.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#bIfon').prop('checked', true);
		$('#basicItem').css('display','block');

		var defArr = ['sex', 'depClass,depID', 'jobID', 'perID', 'birthday', 
					  'jobType', 'jobLevel', 'impaired,impGrade', 'jobClass', 'hireDate', 
					  'leaveDate', 'isOnduty', 'projID'];
		$('[name="basicInfo[]"]').each(function(){
			if($.inArray($(this).val(), defArr) > -1){
				$(this).prop('checked', true);
			}
		});
	});

	function checkAll(obj,cName,allId,k) {
		var box = document.getElementsByName(cName);
		var titleStr = ['','基本資料','通訊資料','經歷','學歷','證照'];

		$('input[name="'+cName+'"]').prop('checked',true).trigger('click');
		for(var i=0; i<box.length; i++){
			box[i].checked = obj.checked;
		}
		if(obj.checked){
			$('#'+allId).html(titleStr[k]+'取消全選');
		}else{
			$('#'+allId).html(titleStr[k]+'全選');
		}
	}

	$('#bIfon').change(function(){
		if(this.checked){
			$('#basicItem').css('display','block');
		}else{
			$('#basicItem').css('display','none')
		}
	});
	$('#comIfon').change(function(){
		if(this.checked){
			$('#comItem').css('display','block');
		}else{
			$('#comItem').css('display','none')
		}
	});
	$('#exper').change(function(){
		if(this.checked){
			$('#expItem').css('display','block');
		}else{
			$('#expItem').css('display','none')
		}
	});
	$('#educ').change(function(){
		if(this.checked){
			$('#eduItem').css('display','block');
		}else{
			$('#eduItem').css('display','none')
		}
	});
	$('#lice').change(function(){
		if(this.checked){
			$('#licItem').css('display','block');
		}else{
			$('#licItem').css('display','none')
		}
	});
	function returnPar(){
		var a = $('input:checkbox:checked[name="checkTitle[]"]').map(function(){return $(this).val();}).get();
		var b = ['empID','empName'];
			b.push($('input:checkbox:checked[name="basicInfo[]"]').map(function(){return $(this).val();}).get());
		var c = $('input:checkbox:checked[name="comInfo[]"]').map(function(){return $(this).val();}).get();
		var d = $('input:checkbox:checked[name="expInfo[]"]').map(function(){return $(this).val();}).get();
		var e = $('input:checkbox:checked[name="eduInfo[]"]').map(function(){return $(this).val();}).get();
		var f = $('input:checkbox:checked[name="licInfo[]"]').map(function(){return $(this).val();}).get();
		form2.submit();
		window.opener.doExcel(a,b,c,d,e,f);
		window.close();
	}
</script>