<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	include('../../getEmplyeeInfo.php');	
	$tabidx = $_SESSION['emplyee_tab']?$_SESSION['emplyee_tab']:0;
	$empID = $_REQUEST[empID];
	if($empID) { //修改
		$actfn = 'doedit.php?keyword='.$_REQUEST['keyword'].'&depFilter='.$_REQUEST['depFilter'].'&sttFilter='.$_REQUEST['sttFilter'].'&jobFilter='.$_REQUEST['jobFilter'].'&typFilter='.$_REQUEST['typFilter'];
		$rs = db_query("select * from emplyee where empID='$empID'");
		if($rs) $r=db_fetch_array($rs);
	} else { //新增
		$actfn = 'doappend.php?keyword='.$_REQUEST['keyword'].'&depFilter='.$_REQUEST['depFilter'].'&sttFilter='.$_REQUEST['sttFilter'].'&jobFilter='.$_REQUEST['jobFilter'].'&typFilter='.$_REQUEST['typFilter'];
	}

  //特休
  $emplyee_annual = 0;
  $sql1 = 'select * from `emplyee_annual` where `empID`='.$empID.' and `year`='.(date('Y'));
  $rs1 = db_query($sql1);
  if($rs) $r1=db_fetch_array($rs1);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta Http-Equiv="Cache-Control" Content="no-cache">
  <meta Http-Equiv="Pragma" Content="no-cache">
  <meta Http-Equiv="Expires" Content="0">
  <meta Http-Equiv="Pragma-directive: no-cache">
  <meta Http-Equiv="Cache-directive: no-cache"> 
  
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>
	function formvalid(tfm) {
		if(!tfm.empID.value) { alert('請輸入員工編號'); tfm.empID.focus(); return false; }
		if(!tfm.empName.value) { alert('請輸入員工姓名'); tfm.empName.focus(); return false; }
    if(!tfm.depID.value) { alert('請輸入員工所屬部門'); tfm.depID.focus(); return false; }
    if(!tfm.perID.value) { alert('請輸入員工身分字號'); tfm.perID.focus(); return false; }
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	
	function setnail(n) {
		$('.tabimg').attr('src','../images/unnail.gif');
		$('#tab'+n).attr('src','../images/nail.gif');
		$.get('setnail.php',{'idx':n});
	}
  function runScript(e) {
    if (e.keyCode == 13) { return false;  }
  }

  function isDel(empID,cardNo,type){
    if(confirm('確定要變更嗎?')){
      //版本問題只能用這種的
      $.ajax({
        method: "POST",
        url: "api.php",
        data: { 'empID': empID,'type':type,'cardNo':cardNo},
        success: function(data){
          if(data!=''){
            alert(data);
          } 
          location.reload();
        }
      })
    }
  }
	</script>
<style type="text/css">
	body { margin: 0; }
	.card td,th{ padding:3px; }
	.card th{ background-color: #cd9696;}
	.tabCorner { border-radius: 0 10px 0 0; }
</style>
</head>

<body>
  <iframe width="100%" height="508" frameborder="0" scrolling="auto" src="../emplyee_duty/list.php?eid=<?php echo $r[empID]?>"></iframe>
</body>

</html>