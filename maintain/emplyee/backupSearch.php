<?php
	@session_start();
	include '../../config.php';
  $basic = array('empID'=>'編號','empName'=>'中文姓名','empEn'=>'英文姓名','sex'=>'性別','depID'=>'部門','jobID'=>'職銜','perID'=>'身分字號','birthday'=>'生日','jobType'=>'身分別','jobLevel'=>'職等','salaryAct'=>'薪資帳號','impaired,impGrade'=>'障別','blood'=>'血型','jobClass'=>'類型','hireDate'=>'到職日','leaveDate'=>'離職日','isOnduty'=>'職況','tel'=>'電話','mobile'=>'行動電話','email'=>'E-Mail','county,city,address'=>'員工戶籍','Marriage'=>'婚姻','contactMan'=>'連絡人','relation'=>'連絡人關係','spHolidays'=>'特休時數','grants'=>'補助類別','projID'=>'計畫編號','noSignIn'=>'是否打卡','donate'=>'固定捐款','tax'=>'所得稅金額');
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>歷史檔匯出</title>
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
<script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script src="../../Scripts/form.js" type="text/javascript"></script>

</head>

<body>
<form name="form1" method="post" action="">
<table width="90%" border="0" align="center" cellpadding="4" cellspacing="0" style="padding-top:10px; font-family:'微軟正黑體',Verdana; font-size:13px">
  <tr>
    <td width="80" align="right">員工編號：</td>
    <td><input type="text" name="empID" id="empID"></td>
  </tr>
  
  <tr>
    <td align="right">搜尋日期：</td>
    <td>
      <input name="search_date" type="text" class="queryDate" id="search_date" size="7" maxlength="10" value="" /> 
    </td>
  </tr>
   
    <td colspan="2">
      <div id="basicItem" style="margin-left: 40px">
          <div class="row">
            <label class="col-md-12">
              <input type="checkbox" onclick="checkAll(this,'basicInfo[]','basAllCheck',1)">
              <sapn id="basAllCheck">基本資料全選</sapn>
            </label>
          </div>
          <?php
            $i = 1;
            foreach ($basic as $key => $value) {
              if($i%4==1) echo '<div class="row" style="padding-top:5px;">';
              echo '<label class="col-md-3" style="padding-right: 20px;">';
              if($key == 'empID' || $key == 'empName'){
                echo '<input type="checkbox" value="'.$key.'" disabled checked>';
              }else{
                echo '<input type="checkbox" name="basicInfo[]" value="'.$key.'">';
              }
              echo str_pad($value,12,"　");
              echo '</label>';
              if($i%4==0) echo '</div>';
              $i++;
            }
            if(count($basic)<4 || count($basic)%4!=0) echo '</div>';
          ?>
        </div>
    </td>
  <tr>
    <td align="right">&nbsp;</td>
    <td>
    	<input type="button" name="btnOK" id="btnOK" value="確定" onClick="doConfirm()">
      <input type="button" name="btnCancel" id="btnCancel" value="取消" onClick="window.close()"></td>
  </tr>
</table>
</form>
<script type="text/javascript">
  $(function(){
    $('#search_date').val("<?php echo date('Y/m/d', strtotime('-1 day'));?>");

    var defArr = ['sex', 'depClass,depID', 'jobID', 'perID', 'birthday', 
            'jobType', 'jobLevel', 'impaired,impGrade', 'jobClass', 'hireDate', 
            'leaveDate', 'isOnduty', 'projID'];
    $('[name="basicInfo[]"]').each(function(){
      if($.inArray($(this).val(), defArr) > -1){
        $(this).attr('checked', 'checked');
      }
    });
  })
  function doConfirm() {
    if($('#search_date').val() == '' || $('#empID').val() =='') {
      alert('請輸入完整的日期與員編');
      return;
    }
    var fields = ['empID','empName'];
    $('[name="basicInfo[]"]:checked').each(function(){
      fields.push($(this).val());
    });
    fields.push();
    var o = {
      'date' : $('#search_date').val(),
      'empID' : $('#empID').val(),
      'fields': fields.join(',')
    };
    window.opener.returnBackupValue(o);
    window.opener.rzt = false;
    //opener.source.postMessage('OK');
    window.close(); 
  }

  function checkAll(obj,cName,allId,k) {
    var box = document.getElementsByName(cName);
    var titleStr = ['','基本資料','通訊資料','經歷','學歷','證照'];

    $('input[name="'+cName+'"]').attr('checked','checked').trigger('click');
    for(var i=0; i<box.length; i++){
      box[i].checked = obj.checked;
    }
    if(obj.checked){
      $('#'+allId).html(titleStr[k]+'取消全選');
    }else{
      $('#'+allId).html(titleStr[k]+'全選');
    }
  }
</script>
</body>
</html>