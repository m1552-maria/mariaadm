<?php
	@session_start();
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../inc_vars.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>進階搜尋</title>
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
<script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script src="../../Scripts/form.js" type="text/javascript"></script>
<script>
function doConfirm() {
	var rzt='';
  //因為網址列不能用% 所以用*取代
	var v = form1.empID.value; if(v) { if(rzt) rzt += " AND empID like '*"+v+"*'"; else rzt += "empID like '*"+v+"*'"; }
	var v = form1.empName.value; if(v) { if(rzt) rzt += " AND empName like '*"+v+"*'"; else rzt += "empName like '*"+v+"*'"; }
  var v = form1.sex.value; if(v) { if(rzt) rzt += " AND sex='"+v+"'"; else rzt += "sex='"+v+"'"; }
  <?php if ($_SESSION['privilege'] > 10) {	?>  	
	var v = form1.depID.options[form1.depID.selectedIndex].value; if(v) { if(rzt) rzt += " AND depID='"+v+"'"; else rzt += "depID='"+v+"'"; }
	<?php } ?>

  var isOnduty = $('input:checkbox:checked[name="isOnduty[]"]').map(function(){
    return "isOnduty='"+$(this).val()+"'";
  }).get();
  var isOndutyNum = $('input:checkbox:checked[name="isOnduty[]"]').map(function(){
    return $(this).val();
  }).get();
  if(isOnduty.length > 0) {
    if(rzt) rzt += " AND ";
    rzt += '(';
    rzt += isOnduty.join(" or ");
    rzt += ')';
  }
  
	// var v = form1.isOnduty.options[form1.isOnduty.selectedIndex].value; if(v) { if(rzt) rzt += " AND (isOnduty='"+v+"')"; else rzt += "isOnduty='"+v+"'"; }
	var v = form1.jobID.options[form1.jobID.selectedIndex].value; if(v) { if(rzt) rzt += " AND jobID='"+v+"'"; else rzt += "jobID='"+v+"'"; }
  var v = form1.Marriage.value; if(v) { if(rzt) rzt += " AND Marriage='"+v+"'"; else rzt += "Marriage='"+v+"'"; }
  var v = form1.projID.value; if(v) { if(rzt) rzt += " AND projID like '*"+v+"*'"; else rzt += "projID like '*"+v+"*'"; }
  var v = form1.grants.value; if(v) { if(rzt) rzt += " AND grants='"+v+"'"; else rzt += "grants='"+v+"'"; }
	// var v = form1.birthday.value; if(v) { if(rzt) rzt += " AND birthday='"+v+"'"; else rzt += "birthday='"+v+"'"; }
	// var v = form1.hireDate.value; if(v) { if(rzt) rzt += " AND hireDate='"+v+"'"; else rzt += "hireDate='"+v+"'"; }
	// var v = form1.leaveDate.value; if(v) { if(rzt) rzt += " AND leaveDate='"+v+"'"; else rzt += "leaveDate='"+v+"'"; }

  var v = form1.birthday_start.value;
  var v1 = form1.birthday_end.value;
  //判斷都有沒有值
  if(v!='' && v1!=''){
    if(rzt) rzt += " AND birthday>='"+v+"' AND birthday<='"+v1+"'"; else rzt += "birthday>='"+v+"' AND birthday<='"+v1+"'";
  }else if(v=='' && v1==''){
  }else{
    alert('請輸入完整日期');
    return false;
  }
  
  var v = form1.hireDate_start.value; 
  var v1 = form1.hireDate_end.value; 
  //判斷都有沒有值
  if(v!='' && v1!=''){
    if(rzt) rzt += " AND hireDate>='"+v+"' AND hireDate<='"+v1+"'"; else rzt += "hireDate>='"+v+"' AND hireDate<='"+v1+"'";
  }else if(v=='' && v1==''){
  }else{
    alert('請輸入完整日期');
    return false;
  }
  
  var v = form1.leaveDate_start.value; 
  var v1 = form1.leaveDate_end.value; 
  //判斷都有沒有值
  if(v!='' && v1!=''){
    if(rzt) rzt += " AND leaveDate>='"+v+"' AND leaveDate<='"+v1+"'"; else rzt += "leaveDate>='"+v+"' AND leaveDate<='"+v1+"'";
  }else if(v=='' && v1==''){
  }else{
    alert('請輸入完整日期');
    return false;
  }
  
  


	var v = form1.jobType.options[form1.jobType.selectedIndex].value; if(v) { if(rzt) rzt += " AND jobType='"+v+"'"; else rzt += "jobType='"+v+"'"; }
	var v = form1.jobLevel.options[form1.jobLevel.selectedIndex].value; if(v) { if(rzt) rzt += " AND jobLevel='"+v+"'"; else rzt += "jobLevel='"+v+"'"; }
	var v = form1.impaired.options[form1.impaired.selectedIndex].value; if(v) { if(rzt) rzt += " AND impaired='"+v+"'"; else rzt += "impaired='"+v+"'"; }
	var v = form1.blood.options[form1.blood.selectedIndex].value; if(v) { if(rzt) rzt += " AND blood='"+v+"'"; else rzt += "blood='"+v+"'"; }
	var v = form1.jobClass.options[form1.jobClass.selectedIndex].value; if(v) { if(rzt) rzt += " AND jobClass='"+v+"'"; else rzt += "jobClass='"+v+"'"; }

  //join table 分隔
  var v = form1.grade.value; if(v) rzt += ",grade='"+v+"'";
  
	if(!rzt) { alert('您尚未設定任何條件!'); return false; }
	if (window.opener != undefined) { //for chrome
    var result_all = [rzt,form1.depID.options[form1.depID.selectedIndex].value,isOndutyNum,form1.jobID.options[form1.jobID.selectedIndex].value,form1.jobType.options[form1.jobType.selectedIndex].value];
    // window.opener.returnValue(rzt);//傳回父層的returnValue function裡面
    window.opener.returnValue(result_all);
	} else {
    window.returnValue(rzt); //傳回父層的returnValue function裡面
  }
	window.close(); //關掉
}
</script>
</head>

<body>
<form name="form1" method="post" action="">
<table width="90%" border="0" align="center" cellpadding="4" cellspacing="0" style="padding-top:10px; font-family:'微軟正黑體',Verdana; font-size:13px">
  <tr>
    <td width="80" align="right">員工編號：</td>
    <td><input type="text" name="empID" id="empID"></td>
  </tr>
  <tr>
    <td align="right">中文姓名：</td>
    <td><input type="text" name="empName" id="empName"></td>
  </tr>
  <tr>
    <td align="right">姓別：</td>
    <td><label><input type="radio" name="sex" id="sex" value="男">男</label><label><input type="radio" name="sex" id="sex" value="女">女</label></td>
  </tr>
  <?php if ($_SESSION['privilege'] > 10) {	?> 
  <tr>
    <td align="right">部門：</td>
    <td><select name="depID" id="depID"><option value=''>-全部-</option><?php foreach($departmentinfo as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <?php }else{ ?>
  <tr hidden>
    <td align="right">部門：</td>
    <td><select name="depID" id="depID"><option value=''>-全部-</option></select></td>
  </tr>
  <?php } ?>
  <tr>
    <td align="right">職況：</td>
    <td><?php foreach($jobStatus as $k=>$v) echo "<input type='checkbox' name='isOnduty[]' value='$k'>$v</option>" ?></td>
  </tr>
  <tr>
    <td align="right">職銜：</td>
    <td><select name="jobID" id="jobID"><option value=''>-全部-</option><?php foreach($jobinfo as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <tr>
    <td align="right">學歷：</td>
    <td><select name="grade" id="grade"><option value=''>-全部-</option><?php foreach($grade as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <tr>
    <td align="right">生日：</td>
    <td>
      起始日期:<input name="birthday_start" type="text" class="queryDate" id="birthday_start" size="7" maxlength="10" /> ~
      結束日期:<input name="birthday_end" type="text" class="queryDate" id="birthday_end" size="7" maxlength="10" />
    </td>
  </tr>
  <tr>
    <td align="right">到職日：</td>
    <td>
      起始日期:<input name="hireDate_start" type="text" class="queryDate" id="hireDate_start" size="7" maxlength="10" /> ~
      結束日期:<input name="hireDate_end" type="text" class="queryDate" id="hireDate_end" size="7" maxlength="10" />
    </td>
  </tr>
  <tr>
    <td align="right">離職日：</td>
    <td>
      起始日期:<input name="leaveDate_start" type="text" class="queryDate" id="leaveDate_start" size="7" maxlength="10" /> ~
      結束日期:<input name="leaveDate_end" type="text" class="queryDate" id="leaveDate_end" size="7" maxlength="10" />
    </td>
  </tr>
  <tr>
    <td align="right">身份別：</td>
    <td><select name="jobType" id="jobType"><option value=''>-全部-</option><?php foreach($jobType as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <tr>
    <td align="right">職等：</td>
    <td><select name="jobLevel" id="jobLevel"><option value=''>-全部-</option><?php foreach($jobLevel as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <tr>
    <td align="right">障別：</td>
    <td><select name="impaired" id="impaired"><option value=''>-全部-</option><?php foreach($impaired as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <tr>
    <td align="right">血型：</td>
    <td><select name="blood" id="blood"><option value=''>-全部-</option><option value="A">A</option><option value="B">B</option><option value="O">O</option><option  value="AB">AB</option></select></td>
  </tr>
  <tr>
    <td align="right">婚姻：</td>
    <td><select name="Marriage" id="Marriage"><option value=''>-全部-</option><?php foreach($Marriage as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <tr>
    <td align="right">類型：</td>
    <td><select name="jobClass" id="jobClass"><option value=''>-全部-</option><?php foreach($jobClass as $k=>$v) echo "<option value='$k'>$v</option>" ?></select></td>
  </tr>
  <tr>
  <td align="right">輔助類別：</td>
    <td><select name="grants" id="grants"><option value=''>-全部-</option><?php foreach($grants as $k=>$v) echo "<option value='$v'>$v</option>" ?></select></td>
  </tr>
  <tr>
    <td align="right">計畫編號：</td>
    <td><input type="text" name="projID" id="projID"></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>
    	<input type="button" name="btnOK" id="btnOK" value="確定" onClick="doConfirm()">
      <input type="button" name="btnCancel" id="btnCancel" value="取消" onClick="window.close()"></td>
  </tr>
</table>
</form>
</body>
</html>