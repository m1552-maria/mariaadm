<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	
	if(isset($_REQUEST['Submit'])) {
		$sql ="insert into emplyee_insurence(empID,job_bdate,job_edate,job_grades,health_bdate,health_edate,health_grades,health_rate,
				retire_grades,retire_rate,feed_nums,mRetire_bdate,mRetire_edate,job_accident,job_insurance,job_disaster,job_rate,retire_bdate,retire_edate,feed_nums_date)";
		$sql.=" values('"
				 . $_REQUEST['eid'] . "','" 
				 . $_REQUEST['jobBdate'] . "','"
				 . $_REQUEST['jobEdate'] . "','"
				 . $_REQUEST['jobGrades'] . "','"
				 . $_REQUEST['healthBdate'] . "','"
				 . $_REQUEST['healthEdate'] . "','"
				 . $_REQUEST['healthGrades'] . "','"
				 . ($_REQUEST['healthRate']?$_REQUEST['healthRate']:0)
				 ."','". $_REQUEST['retireGrades'] . "','"
				 . ($_REQUEST['retireRate']?$_REQUEST['retireRate']:0) . "','"
				 . $_REQUEST['feed_nums'] ."',"
				 . "'".$_REQUEST['mRetire_bdate'] ."',"
				 . "'".$_REQUEST['mRetire_edate'] ."','"
				 . $_REQUEST['job_accident'] ."','"
				 . $_REQUEST['job_insurance'] ."','"
				 . $_REQUEST['job_disaster'] ."','"
				 . $_REQUEST['job_rate'] ."',"
				 . "'".$_REQUEST['retire_bdate'] ."',"
				 . "'".$_REQUEST['retire_edate']."',"
				 . "'".$_REQUEST['feed_nums_date']."')";
		db_query($sql);
		db_close();
	}
	
	$rsX = db_query("select * from map_retire order by levels");
	while($rX=db_fetch_array($rsX)) $retireGrades[$rX['value']]=$rX['title'];
	$rsX = db_query("select * from map_job order by levels");
	while($rX=db_fetch_array($rsX)) $jobGrades[$rX['levels']]=$rX['title'];
	$rsX = db_query("select * from map_health order by levels");
	while($rX=db_fetch_array($rsX)) $healthGrades[$rX['levels']]=$rX['title'];
	$eid = $_REQUEST['eid'];
	$sql = "select * from emplyee_insurence where empID='$eid' order by id DESC limit 1";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>員工保險</title>
  <style type="text/css">
	body {
		margin-left: 0px;
		margin-top: 0px;
		margin-right: 0px;
		margin-bottom: 0px;
		background-color: #F0F0F0;
	}
	.ui-datepicker{ z-index:9999;}
	.opt{	width:15px; height:15px;vertical-align:middle;}
	.row-heavy{	background: #dedede;}
	table{	border-collapse:collapse; }
	table td{padding:2px 4px;}
	.formTx{width:800px;}
	</style>
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
  <script src="../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
	<script src="../../Scripts/form.js" type="text/javascript"></script>
	<script>
	
	function openhistory(eid) {
		window.open('../emplyee_insurence/list.php?eid='+eid,'history','width=900,height=600');
	}
	// function ninwindows(elm) {
	// 	if(parent.document.getElementById('ifins').height>90) {
	// 		elm.value = '▼隱藏清單';
	// 		parent.document.getElementById('ifins').height = 90;
	// 	} else { 
	// 		elm.value = '▲開啟清單';
	// 		parent.document.getElementById('ifins').height = 240;
	// 	}
	// }
	function formValid(){
		var obj=["accident","insurance","disaster"];
		$.each( obj, function( key, val ) {
		  	if($("[name='j_"+val+"']").attr("checked")=="checked")	{
				$("[name='job_"+val+"']").val("1");
			}else $("[name='job_"+val+"']").val("0");
		});
		return true;
	}
	</script>
</head>

<body>
<form onsubmit="return formValid()" scroll='auto'>
	<table class="formTx">
	<tr >
		<td >勞保加保日期</td>
		<td width='120'><input name="jobBdate" type="text" class="queryDate" id="jobBdate" size="7" value="<?php echo (!empty($r['job_bdate'])&&$r['job_bdate']!='0000-00-00')?date('Y/m/d',strtotime($r['job_bdate'])):''?>"/></td>
		<td >勞保退保日期</td>
		<td width='120'><input name="jobEdate" type="text" class="queryDate" id="jobEdate" size="7" value="<?php echo (!empty($r['job_edate'])&&$r['job_edate']!='0000-00-00')?date('Y/m/d',strtotime($r['job_edate'])):''?>"/></td>
		<td >勞保投保級別</td>
        <td width='110'><select name="jobGrades" id="jobGrades"><?php foreach($jobGrades as $k=>$v) echo "<option value='$v' ".($r['job_grades']==$v?'selected':'').">$v</option>" ?></select></td>
    </tr>
    <tr >
        <td >勞保補助比例</td>
        <td><input type="text" name="job_rate" value="<?php echo $r['job_rate']?>" size="1" />%</td>
        <td colspan="4">
		<?php 
		$ck1=$r['job_accident']=="1"?"checked":"";
		$ck2=$r['job_insurance']=="1"?"checked":"";
		$ck3=$r['job_disaster']=="1"?"checked":"";
		foreach($jobInsurence as $k=>$v){	
				if($k=="自付比例")	continue;	
				if($k=="普通事故費率"){	 ?><input type="checkbox" class='opt' name="j_accident" value="<?php echo $ck1==""?0:1?>" <?php echo $ck1?>/><?php echo $k.":".$jobInsurence["普通事故費率"]; }	?>
		<?php		if($k=="就業保險費率"){	?><input type="checkbox" class='opt' name="j_insurance" value="<?php echo $ck2==""?0:1?>" <?php echo $ck2?>/><?php echo $k.":".$jobInsurence["就業保險費率"];}	?>    
		<?php		if($k=="職災費率"){	?><input type="checkbox" class='opt' name="j_disaster" value="<?php echo $ck3==""?0:1?>" <?php echo $ck3?>/><?php echo $k.":".$jobInsurence["職災費率"];}	?>
        <?php    } ?>
        </td>
       
	</tr>
	<tr class='row-heavy'>
		<td>勞退提繳日期</td>
		<td><input name="retire_bdate" type="text" class="queryDate" id="retire_bdate" size="7" value="<?php echo (!empty($r['retire_bdate'])&&$r['retire_bdate']!='0000-00-00')?date('Y/m/d',strtotime($r['retire_bdate'])):''?>"/></td>
		<td>勞退停繳日期</td>
		<td><input name="retire_edate" type="text" class="queryDate" id="retire_edate" size="7" value="<?php echo (!empty($r['retire_edate'])&&$r['retire_edate']!='0000-00-00')?date('Y/m/d',strtotime($r['retire_edate'])):''?>"/></td>
		<td>勞退月提工資</td>
		<td><select name="retireGrades" id="jobType"><?php foreach($retireGrades as $k=>$v) echo "<option value='$k' ".($r['retire_grades']==$k?'selected':'').">$k</option>" ?></select></td>
	</tr>
	<tr class='row-heavy'>
		<td >員工自提比例</td>
        <td><input type="text" name="retireRate" value="<?php echo $r['retire_rate']?>" size="1" />%</td>
        <td >勞退員工自提</td>
		<td><input name="mRetire_bdate" type="text" class="queryDate" id="mRetire_bdate" size="7" value="<?php echo (!empty($r['mRetire_bdate'])&&$r['mRetire_bdate']!='0000-00-00')?date('Y/m/d',strtotime($r['mRetire_bdate'])):''?>"/></td>
		<td >勞退員工停繳</td>
		<td><input name="mRetire_edate" type="text" class="queryDate" id="mRsetire_edate" size="7" value="<?php echo (!empty($r['mRetire_edate'])&&$r['mRetire_edate']!='0000-00-00')?date('Y/m/d',strtotime($r['mRetire_edate'])):''?>"/></td>
	</tr>
	<tr >
		<td>健保加保日期</td>
		<td><input name="healthBdate" type="text" class="queryDate" id="healthBdate" size="7" value="<?php echo (!empty($r['health_bdate'])&&$r['health_bdate']!='0000-00-00')?date('Y/m/d',strtotime($r['health_bdate'])):''?>"/></td>
		<td>健保退保日期</td>
		<td><input name="healthEdate" type="text" class="queryDate" id="healthEdate" size="7" value="<?php echo (!empty($r['health_edate'])&&$r['health_edate']!='0000-00-00')?date('Y/m/d',strtotime($r['health_edate'])):''?>"/></td>
		<td>健保投保級別</td>
        <td><select name="healthGrades" id="healthGrades"><?php foreach($healthGrades as $k=>$v) echo "<option value='$v' ".($r['health_grades']==$v?'selected':'').">$v</option>" ?></select></td>
    </tr>
    <tr >
		<td >健保補助比例</td>
        <td><input type="text" name="healthRate" value="<?php echo $r['health_rate']?>" size="1" />%</td>
        <td>人數變更日期</td>
		<td><input name="feed_nums_date" type="text" class="queryDate" id="feed_nums_date" size="7" value="<?php echo (!empty($r['feed_nums_date'])&&$r['feed_nums_date']!='0000-00-00')?date('Y/m/d',strtotime($r['feed_nums_date'])):''?>"/></td>
        <td >健保眷屬人數</td>
        <td ><input type="text" name="feed_nums" value="<?php echo (!empty($r['feed_nums']))?$r['feed_nums']:0?>" size="1" /></td>

        <td>
            <input type="hidden" name="eid" value="<?php echo $eid?>" />
            <input type="hidden" name="job_accident"/>
            <input type="hidden" name="job_insurance"/>
            <input type="hidden" name="job_disaster"/>
            <input type="submit" name="Submit" value="確定" />
            <input type="button" value="異動紀錄" onclick="openhistory('<?php echo $eid?>')">
          <!-- <input type="button" name="btnUp" value="▼隱藏清單" onclick="ninwindows(this)" /> -->
        </td>
	</tr>

	</table>
</form>  
</body>
</html>  