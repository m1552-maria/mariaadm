<?php
	session_start();
  	include_once('../../config.php');
 	require_once('../Classes/PHPExcel.php');
  	require_once('../Classes/PHPExcel/IOFactory.php');
  	include '../../getEmplyeeInfo.php';
	include '../inc_vars.php';

  	$searchDate = $_REQUEST['date'];
  	$empID = $_REQUEST['empID'];
  	$ulpath = "../../data/emplyee/backup/";
  	$fName = $ulpath.date('Ymd',strtotime($searchDate)).'.txt';
  	if(!is_dir($ulpath) || !is_file($fName)) {
  		echo '查無此日期檔案';
  		exit;
  	}

  	$str = file_get_contents($fName);
  	$data = json_decode($str, true);
  	if(!isset($data[$empID])) {
  		echo '查無此員工檔案';
  		exit;
  	}
  	$showData = $data[$empID];
  	$filename = date("Y-m-d").'.xlsx';//ie中文檔名亂碼
   

    
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);
    $sheet = $objPHPExcel->getActiveSheet();

    $_fields = $_REQUEST['fields'];
    $_fields = explode(',', $_fields);
    $fields = array();
    foreach ($_fields as $key => $value) {
    	$fields[$value] = $value;
    }

   	$basic = array(
		'empID'=>array('val'=>'編號','width'=>8),
		'empName'=>array('val'=>'中文姓名','width'=>10),
		'empEn'=>array('val'=>'英文姓名','width'=>20),
		'sex'=>array('val'=>'性別','width'=>8),
		'depID'=>array('val'=>'部門','width'=>40),
		'jobID'=>array('val'=>'職銜','width'=>18),
		'perID'=>array('val'=>'身分字號','width'=>12),
		'birthday'=>array('val'=>'生日','width'=>11),
		'jobType'=>array('val'=>'身分別','width'=>12),
		'jobLevel'=>array('val'=>'職等','width'=>8),
		'salaryAct'=>array('val'=>'薪資帳號','width'=>15),
		'impaired'=>array('val'=>'障別','width'=>15),
		'blood'=>array('val'=>'血型','width'=>8),
		'jobClass'=>array('val'=>'類型','width'=>17),
		'hireDate'=>array('val'=>'到職日','width'=>11),
		'leaveDate'=>array('val'=>'離職日','width'=>11),
		'isOnduty'=>array('val'=>'職況','width'=>8),
		'tel'=>array('val'=>'電話','width'=>21),
		'mobile'=>array('val'=>'行動電話','width'=>17),
		'email'=>array('val'=>'E-Mail','width'=>33),
		'contactMan'=>array('val'=>'連絡人','width'=>14),
		'relation'=>array('val'=>'連絡人關係','width'=>12),
		'county'=>array('val'=>'員工戶籍','width'=>80),
		'Marriage'=>array('val'=>'婚姻','width'=>8),
		'spHolidays'=>array('val'=>'特休時數','width'=>10),
		'grants'=>array('val'=>'補助類別','width'=>10),
		'projID'=>array('val'=>'計畫編號','width'=>10),
		'noSignIn'=>array('val'=>'是否打卡','width'=>10),
		'donate'=>array('val'=>'固定捐款','width'=>10),
		'tax'=>array('val'=>'所得稅金額','width'=>12)
	);
   
   	$i = 1;
   	$j = 0;
    foreach ($basic as $key => $value) {
    	if(!isset($fields[$key])) continue;
    	$j++;
    	$num = 64+$j;
    	$num = ($num > 90) ? 'A'.chr($num-26) : chr(64+$j);
    	$sheet->setCellValue($num.$i, $value['val']);
    	$sheet->getColumnDimension($num)->setWidth($value['width']);
    	
    	$showVal = $showData[$key];
    	$kk = $num.($i+1);

    	switch ($key) {
			case 'empID':
			case 'salaryAct':
			case 'tel':
			case 'mobile':
			case 'projID':
				$sheet->setCellValueExplicit($kk, $showVal); 
				//將數值欄位轉型字串
				break;
			case 'depID':
				$sheet->setCellValue($kk, $departmentinfo[$showVal]);
				break;
			case 'jobID':
				$sheet->setCellValue($kk, $jobinfo[$showVal]);
				break;
			case 'jobType':
				$sheet->setCellValue($kk, $jobType[$showVal]);
				break;
			case 'impaired':
				$impStr = $impaired[$showVal].$impGrade[$showData['impGrade']];
				$sheet->setCellValue($kk, $impStr);
				break;
			case 'isOnduty':
				$sheet->setCellValue($kk, $jobStatus[$showVal]);
				break;
			case 'county':
				$addStr = $county[$showVal].$showData['city'].$showData['address'];
				$sheet->setCellValue($kk, $addStr);
				break;
			case 'noSignIn':
				$showVal ==1 ? $sheet->setCellValue($kk, '不需打卡') : $sheet->setCellValue($kk, '需打卡');
				break;
			default:
				$sheet->setCellValue($kk, $showVal);
				break;
		}
    }

 	header("Content-type:application/vnd.ms-excel");
    header("Content-Type:text/html; charset=utf-8");
    header('Content-Disposition:attachment;filename='.$filename);//中文檔名亂碼的問題
    header("Pragma:no-cache");
    header("Expires:0");
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->setPreCalculateFormulas(false);
    
    $objWriter->save('php://output');

?>