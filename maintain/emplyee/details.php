<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	include('../../getEmplyeeInfo.php');	
	$tabidx = isset($_SESSION['emplyee_tab']) ? $_SESSION['emplyee_tab'] : 0;
	$empID = isset($_REQUEST['empID']) ? $_REQUEST['empID'] : '';
  $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
  $depFilter = isset($_REQUEST['depFilter']) ? $_REQUEST['depFilter'] : '';
  $sttFilter = isset($_REQUEST['sttFilter']) ? $_REQUEST['sttFilter'] : '1';
  $jobFilter = isset($_REQUEST['jobFilter']) ? $_REQUEST['jobFilter'] : '';
  $typFilter = isset($_REQUEST['typFilter']) ? $_REQUEST['typFilter'] : '';
	if($empID) { //修改
		$actfn = 'doedit.php?keyword='.$keyword.'&depFilter='.$depFilter.'&sttFilter='.$sttFilter.'&jobFilter='.$jobFilter.'&typFilter='.$typFilter;
		$rs = db_query("select * from emplyee where empID='$empID'");
		if($rs) $r=db_fetch_array($rs);
	} else { //新增
		$actfn = 'doappend.php?keyword='.$keyword.'&depFilter='.$depFilter.'&sttFilter='.$sttFilter.'&jobFilter='.$jobFilter.'&typFilter='.$typFilter;
	}

  //特休
  $emplyee_annual = 0;
  $sql1 = 'select * from `emplyee_annual` where `empID`='.$empID.' and `year`='.(date('Y'));
  $rs1 = db_query($sql1);
  if(isset($rs)) $r1=db_fetch_array($rs1);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta Http-Equiv="Cache-Control" Content="no-cache">
  <meta Http-Equiv="Pragma" Content="no-cache">
  <meta Http-Equiv="Expires" Content="0">
  <meta Http-Equiv="Pragma-directive: no-cache">
  <meta Http-Equiv="Cache-directive: no-cache"> 
  
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>
	$(function(){
		var n = TabbedPanels1.getCurrentTabIndex();
		if(n==0||n==2||n==3) {
			document.getElementById('Submit').style.display=''
			document.getElementById('Reset').style.display=''
		}else {
			document.getElementById('Submit').style.display='none'
			document.getElementById('Reset').style.display='none'
		}
		
		$('.TabbedPanelsTab').click(function(){
			var n=this.getAttribute('tabindex');
			if(n==0||n==2||n==3) {
				document.getElementById('Submit').style.display=''
				document.getElementById('Reset').style.display=''
			}else {
				document.getElementById('Submit').style.display='none'
				document.getElementById('Reset').style.display='none'
			}
		});
		
		//擴展iframe到最大
		var iframeH = $(window).height()-47;
		$('.TabbedPanelsContent>iframe:not(.iframe2)').height(iframeH);
	});
	
	function formvalid(tfm) {
		if(!tfm.empID.value) { alert('請輸入員工編號'); tfm.empID.focus(); return false; }
		if(!tfm.empName.value) { alert('請輸入員工姓名'); tfm.empName.focus(); return false; }
    if(!tfm.depID.value) { alert('請輸入員工所屬部門'); tfm.depID.focus(); return false; }
    if(!tfm.perID.value) { alert('請輸入員工身分字號'); tfm.perID.focus(); return false; }
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	
	function setnail(n) {
		$('.tabimg').attr('src','../images/unnail.gif');
		$('#tab'+n).attr('src','../images/nail.gif');
		$.get('setnail.php',{'idx':n});
	}
  function runScript(e) {
    if (e.keyCode == 13) { return false;  }
  }

  function isDel(empID,cardNo,type){
    if(confirm('確定要變更嗎?')){
      //版本問題只能用這種的
      $.ajax({
        method: "POST",
        url: "api.php",
        data: { 'empID': empID,'type':type,'cardNo':cardNo},
        success: function(data){
          if(data!=''){
            alert(data);
          } 
          location.reload();
        }
      })
    }
  }
	</script>
<style type="text/css">
	body { margin: 0; }
	.card td,th{ padding:3px; }
	.card th{ background-color: #cd9696;}
	.tabCorner { border-radius: 0 10px 0 0; }
</style>
</head>

<body>
<form action="<?php echo $actfn?>" method="post" target="_parent" style="margin:0" onsubmit="return formvalid(this)">
<table width="100%" border="0">
  <tr>
    <td>
    <div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab tabCorner" tabindex="0">基本資料<img id="tab0" class="tabimg" src="../images/<?php echo  $tabidx==0?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(0)" /></li>
    <?php if($empID) { ?>
        <li class="TabbedPanelsTab tabCorner" tabindex="1">通訊資料<img id="tab1" class="tabimg" src="../images/<?php echo  $tabidx==1?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(1)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="2" style="display:<?php echo ($_SESSION['privilege']>10 || $_SESSION["canEditEmp"]?'':'none')?>">照片及密碼<img id="tab2" class="tabimg" src="../images/<?php echo  $tabidx==2?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(2)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="3" style="display:<?php echo ($_SESSION['privilege']>10?'':'none')?>">權限設定<img id="tab3" class="tabimg" src="../images/<?php echo  $tabidx==3?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(3)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="4">經歷<img id="tab4" class="tabimg" src="../images/<?php echo  $tabidx==4?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(4)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="5">學歷<img id="tab5" class="tabimg" src="../images/<?php echo  $tabidx==5?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(5)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="6">證照/著作<img id="tab6" class="tabimg" src="../images/<?php echo  $tabidx==6?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(6)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="7">教育訓練<img id="tab7" class="tabimg" src="../images/<?php echo  $tabidx==7?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(7)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="8">職況<img id="tab8" class="tabimg" src="../images/<?php echo  $tabidx==8?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(8)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="9">薪資異動<img id="tab9" class="tabimg" src="../images/<?php echo  $tabidx==9?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(9)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="10">勞健退<img id="tab10" class="tabimg" src="../images/<?php echo  $tabidx==10?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(10)"/></li>
        <li class="TabbedPanelsTab tabCorner" tabindex="10">出勤表<img id="tab11" class="tabimg" src="../images/<?php echo  $tabidx==11?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(11)"/></li>
    <?php } ?>
      </ul>
      
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent" title="---基本資料---">
          <table width="100%" border="0" cellpadding="5" cellspacing="0" class="formTx">
            <tr>
              <td width='60'>編號</td><td ><input name="empID" type="text" id="empID" value="<?php echo isset($r['empID']) ? $r['empID'] : ''?>" size="10" <?php echo  $empID?'readonly class="readonlyField"':'' ?>/></td>
              <td width='50'>中文姓名</td><td width='220'><input name="empName" type="text" id="empName" size="10" value="<?php echo isset($r['empName']) ? $r['empName'] : ''?>" /></td>
              <td width='50'>英文姓名</td><td><input name="empEn" type="text" id="empEn" value="<?php echo isset($r['empEn']) ? $r['empEn'] : ''?>" size="16" /></td>              
            </tr>
            <tr>
             <td width='30'>性別</td>
             <?php $sex = isset($r['sex']) ? $r['sex'] : '';?>
              <td><input type="radio" name="sex" id="sex" value="男" <?php echo $sex=='男'?'checked':'' ?>/>男
                <input type="radio" name="sex" id="sex" value="女" <?php echo $sex=='女'?'checked':'' ?>/>女</td>
    <?php if(empty($_REQUEST['empID'])){ ?>
              <td>部門</td><td><input name="depID" type="text" class="queryID" id="depID" size="5" value="<?php echo  isset($r['depID']) ? $r['depID'] : ''?>" title="<?php echo  isset($r['depID']) ? $departmentinfo[$r['depID']] : ''?>"/></td>
    <?php }else{?>
              <td>部門</td><td><input readonly type="text" class="readonlyField" size="5" value="<?php echo isset($r['depID']) ? $r['depID'] : ''?>" /><?php echo isset($r['depID']) ? $departmentinfo[$r['depID']] : ''?></td>
    <?php } ?>

              <td>職銜</td><td><input type="text" name="jobID" id="jobID" size="5" class="queryID" value="<?php echo isset($r['jobID']) ? $r['jobID'] : ''?>" title="<?php echo  !empty($r['jobID']) ? $jobinfo[$r['jobID']] : ''?>"/></td>
            </tr>
            <tr>
              <td>身分字號</td><td><input name="perID" type="text" id="perID" value="<?php echo isset($r['perID']) ? $r['perID'] : ''?>" size="16" maxlength="10" /></td>
              <td>生日</td><td><input name="birthday" type="text" class="queryDate" id="birthday" value="<?php echo isset($r['birthday']) ? date('Y/m/d',strtotime($r['birthday'])) : ''?>" size="7" maxlength="10" /></td>
              <?php $sJobType = isset($r['jobType']) ? $r['jobType'] : ''?>
              <td>身份別</td><td><select name="jobType" id="jobType"><?php foreach($jobType as $k=>$v) echo "<option value='$k' ".($sJobType==$k?'selected':'').">$v</option>" ?></select></td>         
            </tr>
            <tr>
              <td>職等</td>
               <?php $sJobLevel = isset($r['jobLevel']) ? $r['jobLevel'] : ''?>
              <td><select name="jobLevel" id="jobLevel"><?php foreach($jobLevel as $k=>$v) echo "<option value='$k' ".($sJobLevel==$k?'selected':'').">$v</option>" ?></select></td>
              <td>障別</td><td>
                  <?php $sImpaired = isset($r['impaired']) ? $r['impaired'] : ''?>
              	<select name="impaired" id="impaired"><?php foreach($impaired as $k=>$v) echo "<option value='$k' ".($sImpaired==$k?'selected':'').">$v</option>" ?></select>
                  <?php $sImpGrade = isset($r['impGrade']) ? $r['impGrade'] : ''?>
                <select name="impGrade" id="impGrade"><?php foreach($impGrade as $k=>$v) echo "<option value='$k' ".($sImpGrade==$k?'selected':'').">$v</option>" ?></select>
              </td>
              <td>薪資帳號</td>
              <td>
                <select name='salaryBank' id='salaryBank'>
                  <option value="">--無--</option>
                   <?php $salaryBank = isset($r['salaryBank']) ? $r['salaryBank'] : ''?>
                <?php foreach($bankID as $key => $value) echo "<option value='$key'".($salaryBank==$key?'selected':'').">$value</option>"; ?>
                </select>
                <input name="salaryAct" type="text" id="salaryAct" value="<?php echo isset($r['salaryAct']) ? $r['salaryAct'] : ''?>" size="16" />
              </td>
            </tr>
            <tr>
               <?php $blood = isset($r['blood']) ? $r['blood'] : ''?>
              <td>血型</td><td><select name="blood" id="blood">
                <option <?php echo $blood=='A'?'selected':''?>>A</option>
                <option <?php echo $blood=='B'?'selected':''?>>B</option>
                <option <?php echo $blood=='O'?'selected':''?>>O</option>
                <option <?php echo $blood=='AB'?'selected':''?>>AB</option>
              </select></td>
              <?php $sJobClass = isset($r['jobClass']) ? $r['jobClass'] : ''?>
              <td>類型</td><td><select name="jobClass" id="jobClass"><?php foreach($jobClass as $k=>$v) echo "<option value='$k' ".($sJobClass==$k?'selected':'').">$v</option>" ?></select></td>
              <td>到職日</td><td><input name="hireDate" type="text" class="queryDate" id="hireDate" size="10" value="<?php echo !empty($r['hireDate'])?date('Y/m/d',strtotime($r['hireDate'])):''?>"/></td>  
              </tr>
            <tr>
              <td>離職日</td>
              <td><input name="leaveDate" type="text" class="queryDate" id="leaveDate" size="10" value="<?php echo  (!empty($r['leaveDate'])&&$r['leaveDate']!='0000-00-00')?date('Y/m/d',strtotime($r['leaveDate'])):''?>"/></td>
              <td>職況</td>
              <?php $isOnduty = isset($r['isOnduty']) ? $r['isOnduty'] : ''?>
              <td><select name="isOnduty" id="isOnduty">
                <?php foreach($jobStatus as $k=>$v) echo "<option value='$k' ".($isOnduty==$k?'selected':(!$empID&&$k==1?'selected':'')).">$v</option>" ?>
              </select></td>
              <td>電話</td><td><input name="tel" type="text" id="tel" size="12" value="<?php echo isset($r['tel']) ? $r['tel'] : ''?>"/></td>
              </tr>
            <tr>
              <td>行動電話</td><td><input name="mobile" type="text" id="mobile" size="12" value="<?php echo isset($r['mobile']) ? $r['mobile'] : ''?>"/></td>
              <td>EMail</td><td ><input name="email" type="text" id="email" value="<?php echo isset($r['email']) ? $r['email'] : ''?>" size="28"/></td>
              <td>連絡人</td><td><input name="contactMan" type="text" id="contactMan" size="8" value="<?php echo isset($r['contactMan']) ? $r['contactMan'] : ''?>"/></td>
              </tr>
             <tr>
               <td>關係</td>
                <?php $sRelation = isset($r['relation']) ? $r['relation'] : ''?>
                <td><select name="relation" id="relation">
                  <?php foreach($relation as $k=>$v) echo "<option value='$k' ".($sRelation==$k?'selected':'').">$v</option>" ?>
                </select></td>
                <td>員工戶籍</td>
                <?php $sCounty = isset($r['county']) ? $r['county'] : ''?>
                <td ><select name="county">
                  <?php foreach($county as $k=>$v) { $ck=($sCounty==$k?'selected':''); echo "<option value='$k' $ck>$v</option>"; } ?>
                </select>
                  <select name="city" id="city">
                    <?php if(isset($r['city'])) echo '<option selected value="'.$r['city'].'">'.$r['city'].'</option>';?>
                  </select>
                  <input name="address" type="text" id="address" value="<?php echo isset($r['address']) ? $r['address'] : ''?>" size="28" /></td>
                <td>婚姻</td>
                 <?php $sMarriage = isset($r['Marriage']) ? $r['Marriage'] : ''?>
                <td><select name="Marriage" id="Marriage">
                  <?php foreach($Marriage as $k=>$v) echo "<option value='$k' ".($sMarriage==$k?'selected':'').">$v</option>" ?>
                </select></td>        
              </tr>
              <tr>
               
                <td>補助類別</td>
                 <?php $sGrants = isset($r['grants']) ? $r['grants'] : ''?>
                <td><select name="grants" id="grants"><?php foreach($grants as $v) echo "<option ".($sGrants==$v?'selected':'').">$v</option>" ?></select> &nbsp;</td>
                <td>計畫編號</td>
                <td>
            <?php if(empty($_REQUEST['empID'])){ ?>
                  <input type="text" name="projID" id="projID" value="<?php echo isset($r['projID']) ? $r['projID'] : ''?>" size='10'/>
            <?php } else { ?>
                  <input type="text" readonly value="<?php echo isset($r['projID']) ? $r['projID'] : ''?>" size='10' class="readonlyField"/>
                  <input type="button" onclick="openHistory('<?php echo $empID;?>')" value="異動資料" title="部門、計畫編號異動">
            <?php } ?>
                </td>
                <td>是否打卡</td>
                  <td><input name="noSignIn" type="checkbox" id="noSignIn" value="1" <?php echo !empty($r['noSignIn'])?'checked':''?>/>不需打卡&nbsp;&nbsp;</td>
              </tr>
          </table>
        </div>
 
        <div class="TabbedPanelsContent" title="---通訊資料---">
          <iframe width="100%" height="255"  frameborder="0" src="../community/list.php?eid=<?php echo $empID ?>"></iframe>
        </div>
        <div class="TabbedPanelsContent" title="---照片及密碼---">
        	<table class="formTx">
          	<tr valign="top">
            	<td><iframe width="260" height="220" frameborder="0" src="headpic.php?empID=<?php echo $empID?>"></iframe></td>
          		<td><table><!--<tr><td>登入密碼：</td><td><input name="password" type="text" id="password" value="<?php echo $r['password']?>"/></td></tr>-->

              <tr><td>密碼初始化：</td><td><label style="cursor:pointer;"><input type="checkbox" name="changePwd" id="changePwd" value="1"/>將密碼初始為員編</label></td></tr>

              <tr><td>識別卡卡號：</td><td>
              <?php if(isset($_SESSION['user_classdef'][2])){ //是不是從前台登入的 ?>
              <input name="cardNo" type="password" id="cardNo" value="<?php echo $r['cardNo']?>"/ onkeypress="return runScript(event);" disabled >
              <input name="cardNo_before" type="hidden" id="cardNo_before" value="<?php echo $r['cardNo']?>"/>
              <?php } else { ?>
              <input name="cardNo" type="tel" id="cardNo" value="<?php echo $r['cardNo']?>"/ onkeypress="return runScript(event);"  onfocus="this.select()" style="ime-mode:disabled;-webkit-ime-mode::disabled;-moz-ime-mode:disabled;-o-ime-mode:disabled;-ms-ime-mode:disabled;">
              <input name="cardNo_before" type="hidden" id="cardNo_before" value="<?php echo $r['cardNo']?>"/>
              <input type="submit" name="button" id="button" value="確認變更卡號" />
              <?php
                if(stripos($r['cardNo'],'--')){
                  echo '<input type="button" id="button" value="恢復使用"  onclick="isDel('.$r['empID'].','."'".$r['cardNo']."'".','."'".'reuse'."'".')" />';
                }else{
                  echo '<input type="button" id="button" value="暫停使用"  onclick="isDel('.$r['empID'].','."'".$r['cardNo']."'".','."'".'isDel'."'".')" />';
                }
               } ?>
              <tr>
                <td style="vertical-align: top;">換卡紀錄:</td>
                <td>
                <?php
                  echo '<div style="overflow-y:auto;height:160px;">';
                  $sql1 = 'select * from `emplyee_card_log` where `empID`="'.$empID.'" order by `rdate` desc';
                  $rs1 = db_query($sql1);
                  if(!db_eof($rs1)){
                    echo '<table width="100%" border="1" CELLPADDING="0" CELLSPACING="0" class="card"><tr><th>卡號</th><th>日期</th></tr>';
                    while ($r1=db_fetch_array($rs1)){
                      if(isset($_SESSION['user_classdef'][2])){//是不是從前台登入的
                        $r1['cardNo'] = substr_replace ( $r1['cardNo'], '****', 4);
                        echo '<tr><td>'.$r1['cardNo'].'</td> <td>'.$r1['rdate'].'</td></tr>';
                      }else{
                        echo '<tr><td>'.$r1['cardNo'].'</td> <td>'.$r1['rdate'].'</td></tr>';
                      }
                    }
                    echo '</table>';
                  }else{
                    echo 'No data';
                  }
                  echo '</div>';
                ?>
                </td>
              </tr>
              </table></td>  
        		</tr>
          </table>
        </div>
        <div class="TabbedPanelsContent" title="---權限設定---">
        	<table class="formTx">
            <tr valign="top">
            	<td width="120">
            		<input type="checkbox" name="canProject" value="1" <?php echo  $r['canProject']?'checked':''?>/>建立新專案權限<br />
            		<input type="checkbox" name="canSchedule" value="1" <?php echo  $r['canSchedule']?'checked':''?>/>部門管理權限<br />
                <input type="checkbox" name="canEditEmp" value="1" <?php echo  $r['canEditEmp']?'checked':''?>/>修改員工資料權限
              </td>
              <td><iframe width="780" height="360" frameborder="0" src="priviege.php?eid=<?php echo $r['empID']?>"></iframe></td>
            </tr>
          </table>
        </div>
         <div class="TabbedPanelsContent" title="---經歷---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_experience/list.php?eid=<?php echo $r['empID']?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---學歷---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_education/list.php?eid=<?php echo $r['empID']?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---證照/著作---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_license/list.php?eid=<?php echo $r['empID']?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---教育訓練---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_training/list.php?eid=<?php echo $r['empID']?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---職況---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_status/list.php?eid=<?php echo $r['empID']?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---薪資異動---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_salary/list.php?eid=<?php echo $r['empID']?>"></iframe>
        </div>
        <div class="TabbedPanelsContent" title="---勞健退---">
		    	<iframe width="100%" id="ifins" height="185" scrolling="no" frameborder="0" src="insurence.php?eid=<?php echo $r['empID']?>"  class='iframe2'></iframe>
          <iframe width="100%" height="180" scrolling="auto" frameborder="0" src="../emplyee_dependents/list.php?eid=<?php echo $r['empID']?>" class='iframe2'></iframe>
        </div>
        <div class="TabbedPanelsContent" title="---出勤表---">
          <iframe width="100%" height="508" frameborder="0" scrolling="auto" src="../emplyee_duty/list.php?eid=<?php echo $r['empID']?>"></iframe>
        </div>

      </div>
    </div>
    </td>
  </tr>
 <?php if ($_SESSION['privilege']>10 || $_SESSION["canEditEmp"]) {	?>
  <tr>
    <td>
    	<input type="hidden" name="params" id="params" value="" />
    	<input type="submit" name="button" id="Submit" value="<?php echo $empID?'確定修改':'確定新增'?>" />
      <input type="reset" name="button2" id="Reset" value="重設" />
    </td>
  </tr>
 <?php } ?> 
</table>
</form>
</body>
<script type="text/javascript">
  var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");TabbedPanels1.showPanel(<?php echo $tabidx?>);

  var historyOpen = false;
  function openHistory(eid) {
   // window.location.reload();
    if(historyOpen) historyOpen.close();
    historyOpen = window.open('../emplyee_history/list.php?eid='+eid,'','width=800,height=600');
  }

  var loop = setInterval(function() {
    if(historyOpen.closed) { 
      clearInterval(loop); 
      window.location.reload();
    } 
  }, 1000);

</script>
</html>