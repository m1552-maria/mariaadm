<?php 
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<10) {
	  	header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	if(($_SESSION["canEditEmp"] && $_SESSION['privilege']<=10) || $_SESSION['privilege']>10){
		$bE2=true;
	}else{
		$bE2=false;
	}
	//$bE = true;
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "薪資異動";
	$tableName = "emplyee_salary";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if($_REQUEST[eid]) {
		$eid = $_REQUEST['eid'];
		$_SESSION['EE_EID'] = $eid;
	} else $eid = $_SESSION['EE_EID'];
	$filter = "empID='$eid'";
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "grade";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,changeDate,salaryType,hourfee,ratio,notes,sums,salary,rdate");
	$ftAry = explode(',',"ID,變更起日,給薪類別,時薪,工時比例,異動說明,合計點數,核定薪資,登錄日期");
	$flAry = explode(',',"80,100,100,80,80,150,100,80,100");
	$ftyAy = explode(',',"id,date,select,text,text,text,text,text,date");
	
	$isHighAry=array("0"=>'一般','1'=>'最高','2'=>'次高');
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"empID,changeDate,salaryType,hourfee,ratio,capacity_ratio,notes,post,food,manager,edu,license,seniority,response,allowance,special,transform,sums,salary");
	$newftA = explode(',',"員工編號,變更起日,給薪類別,時薪,工時比例,產能敘薪比例,異動說明,職務加點,伙食加點,主管加點,學歷加點,證照加點,年資加點,責任加點,相關津貼,特別加點,轉換點數,合計點數,核定薪資");
	$newflA = explode(',',",,,,,,18,,,,,,,,,,,,");
	$newfhA = explode(',',",,,,,,3,,,,,,,,,,,,");
	$newfvA = array('','',$salaryType,0,1,0.7,'',0,0,0,0,0,0,0,0,0,0,0,0);
	$newetA = explode(',',"empID,date,select,text,text,text,textbox,text,text,text,text,text,text,text,text,text,text,label,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,changeDate,salaryType,hourfee,ratio,capacity_ratio,notes,post,food,manager,edu,license,seniority,response,allowance,special,transform,sums,salary");
	$editftA = explode(',',"ID,變更起日,給薪類別,時薪,工時比例,產能敘薪比例,異動說明,職務加點,伙食加點,主管加點,學歷加點,證照加點,年資加點,責任加點,相關津貼,特別加點,轉換點數,合計點數,核定薪資");
	$editflA = explode(',',",,,,,,18,,,,,,,,,,,");
	$editfhA = explode(',',",,,,,,3,,,,,,,,,,,");
	$editfvA = array('','',$salaryType,'','','','','','','','','','','','','','','','');
	$editetA = explode(',',"id,date,select,text,text,text,textbox,text,text,text,text,text,text,text,text,text,text,label,text");
?>