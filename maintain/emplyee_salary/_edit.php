<?php
	session_start();
	include("../../config.php");
	include("init.php");
	$ID = $_REQUEST['ID'];
  	$sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
	$icons = array('doc'=>'icon_doc.gif','docx'=>'icon_doc.gif','pdf'=>'icon_pdf.gif','ppt'=>'icon_ppt.gif','pptx'=>'icon_ppt.gif',
	  'txt'=>'icon_txt.gif','xls'=>'icon_xls.gif','xlsx'=>'icon_xls.gif','zip'=>'icon_zip.gif','rar'=>'icon_rar.gif');
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
.photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
.photoPreview .photoDesc { float:left; margin-left:5px; }
.photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
.photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
.photoPreview .photoItem .photoDel { margin:5px 0 0 5px; cursor:pointer; height:20px;}
.photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }

 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?php echo $extfiles?>edit.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <title><?php echo $pageTitle?> - 編輯</title>
 <script type="text/javascript">
  function chg(){
    var salaryType=$("[name='salaryType']").val();
	var objects;
	switch(salaryType){
		case "0"://月薪
			objects=['hourfee','salary','capacity_ratio'];
			showObj(objects,'N');
			objects=['post','food','manager','edu','license','seniority','response','allowance','special','transform','sums','ratio','notes'];
			showObj(objects,'Y');
			break;
		case "1"://時薪 
			objects=['hourfee','notes','ratio'];
			showObj(objects,'Y');
			objects=['post','food','manager','edu','license','seniority','response','allowance','special','transform','sums','salary','capacity_ratio'];
			showObj(objects,'N');
			break;
		case "2"://產能敘薪
			objects=['salary','notes','ratio','capacity_ratio'];
			showObj(objects,'Y');
			objects=['hourfee','post','food','manager','edu','license','seniority','response','allowance','special','transform','sums'];
			showObj(objects,'N');
			break;
		case "3"://固定薪
			objects=['salary','notes','ratio'];
			showObj(objects,'Y');
			objects=['hourfee','post','food','manager','edu','license','seniority','response','allowance','special','transform','sums','capacity_ratio'];
			showObj(objects,'N');
			break;
		
	}
 }
 function showObj(objs,type){
	for(var i in objs){
		if(type=='Y'){
			$("[name='tr_"+objs[i]+"']").show();
		}else{
			$("[name='tr_"+objs[i]+"']").hide();
		}
	 }
 }
 </script>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?php echo $pageTitle?> -【編輯作業】</td></tr>
	<tr>
 		<td align="right" class="colLabel"><?php echo $editftA[0]?></td>
 		<td class="colContent"><?php echo $ID?></td>
	</tr>

	<?php 
	for ($i=1; $i<count($editfnA); $i++) { 
		if($r['salaryType']=='0'){//月薪
			if($editfnA[$i]=="salary" || $editfnA[$i]=="hourfee" || $editfnA[$i]=='capacity_ratio')	$display='none';	else $display='';
		}else if($r['salaryType']=='1'){//時薪
			if($editfnA[$i]=="hourfee" || $editfnA[$i]=="notes" || $editfnA[$i]=="id" || $editfnA[$i]=="changeDate" || $editfnA[$i]=="salaryType" || $editfnA[$i]=="ratio")	$display='';	else $display='none';
		}else if($r['salaryType']=='2'){//產能敘薪
			if($editfnA[$i]=="notes" || $editfnA[$i]=="id" || $editfnA[$i]=="changeDate" || $editfnA[$i]=="salaryType" || $editfnA[$i]=="salary" || $editfnA[$i]=="ratio" || $editfnA[$i]=="capacity_ratio")	$display='';	else $display='none';
		}else if($r['salaryType']=='3'){//固定薪
			if($editfnA[$i]=="notes" || $editfnA[$i]=="id" || $editfnA[$i]=="changeDate" || $editfnA[$i]=="salaryType" || $editfnA[$i]=="salary" || $editfnA[$i]=="ratio")	$display='';	else $display='none';
		}
	?>
    <tr name="tr_<?php echo $editfnA[$i]?>" class="<?php echo  $editetA[$i]=='hidden'?'colLabel_Hidden':''?>" style='display:<?php echo $display?>'>
  	<td align="right" class="colLabel"><?php echo $editftA[$i]?></td>
  	<td><?php switch($editetA[$i]) { 
  	 		case "text" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."' onKeyUp='doSum()'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' id='".$editfnA[$i]."' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea> <input type='button' value='編輯' onClick='HTMLEdit($editfnA[$i])'>"; break;
  			case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' id='".$editfnA[$i]."' onClick='setHigh(\"".$editfnA[$i]."\")' class='optBtn'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>是"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "radio":echo "<input  type='radio' name='$editfnA[$i]' value='$editfvA[$i]'>是";	break;
			case "select"  : 
					echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input' onchange='chg()'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; 
				break;
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";				
				break;
			case "label":
				echo "<input type='hidden' name='$editfnA[$i]' value='".$r[$editfnA[$i]]."' id='$editfnA[$i]'>";
				echo "<div id='d_$editfnA[$i]'>".$r[$editfnA[$i]]."</div>";
				break;	
  	} ?></td>
  </tr><?php } ?>
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" id='selid' value="<?php echo $ID?>">
  	<input type="hidden" name="pages" value="<?php echo $_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?php echo $_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?php echo $_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?php echo $_REQUEST['sortDirection']?>">
    <input type='hidden' id='bdate' name='bdate' value="<?php echo $r['bdate']?>">
    <input type='hidden' id='edate' name='edate' value="<?php echo $r['edate']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
  	<input type="button" class="btn" onClick="history.back()" value="取消編輯">
  </td>
</tr>
</table>
</form>
</body>
</html>
