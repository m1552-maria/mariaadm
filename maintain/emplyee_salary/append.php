<?php 
	session_start();
	header("Content-Type:text/html; charset=utf-8");
	include("../../config.php");
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
	.ui-widget {font-size:12px; }
	.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?php echo $extfiles?>append.js" ></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script>
 <title><?php echo $pageTitle?> - 新增</title>
 <script type="text/javascript">
 var date = <?php echo json_encode($changAry["changeDate"])?>;
 $(function(){
	chg();
 });
 function chkDay(){	
	for( var i in date){
		if($('#changeDate').val()== date[i]){
			alert("變更起日與不可重覆!");
			$('#changeDate').val('')
		}
	}
 }
 
 function chg(){
 	var salaryType=$("[name='salaryType']").val();
	var objects;
	$('#salary').removeAttr('readonly');
	switch(salaryType){
		case "0"://月薪
			objects=['hourfee','capacity_ratio'];
			showObj(objects,'N');
			objects=['post','sums','ratio','notes','salary','extra','salaryCt'];	//'food','manager','edu','license','seniority','response','allowance','special','transform'
			showObj(objects,'Y');
			showObj2('N');
			$('#salary').attr('readonly','readonly');
			break;
		case "1"://時薪 
			objects=['hourfee','notes','ratio'];
			showObj(objects,'Y');
			objects=['post','sums','salary','capacity_ratio','extra','salaryCt'];
			showObj(objects,'N');
			break;
		case "2"://產能敘薪
			objects=['salary','notes','ratio','capacity_ratio','salaryCt'];
			showObj(objects,'Y');
			objects=['hourfee','post','sums','extra'];
			showObj(objects,'N');
			break;
		case "3"://固定薪
			objects=['salary','notes','ratio','post','food','extra'];
			showObj(objects,'Y');
			objects=['hourfee','sums','capacity_ratio'];
			showObj(objects,'N');
			showObj2('Y');
			$('#salary').attr('readonly','readonly');
			break;	
	}
	$('#hourfee').val(0);
	$('#sums').val(0);
	$('#extra').val(0);
	$('#salary').val(0);
	doSum();
 }
 
 function showObj(objs,type){
   for(var i in objs){
		 if(type=='Y') $("[name='tr_"+objs[i]+"']").show(); else $("[name='tr_"+objs[i]+"']").hide();
	 }
 }
 function showObj2(type){
	 var objs = ['manager','edu','license','response','allowance','special','transform'];
	 if(type=='Y') {
		 $("tr[name='tr_post']").children('td:first-child').text('加給(元)');
		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().hide();
		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().prev().hide();
	 } else {
		 $("tr[name='tr_post']").children('td:first-child').text('點數');
 		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().show();
		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().prev().show();
	 }
 }
 
 $(function(){ 
 	$("#prjform").keypress(checkFormEnter); 
	if($("[name='salaryType']").val()==3) showObj2('Y');
	doSum();
 });
 </script>
</Head>

<body class="page" style="overflow:auto">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?php echo $pageTitle?> -【新增作業】</td></tr>

	<?php for ($i=0; $i<count($newfnA); $i++) {
			if($newfnA[$i]=="hourfee" || $newfnA[$i]=="salary" || $newfnA[$i]=='capacity_ratio')	$display='none';	else	$display='';
	?>
    
 	<?php if($newetA[$i]!='texv') { ?>
	  <tr name="tr_<?php echo $newfnA[$i]?>" class="<?php echo  $newetA[$i]=='hidden'?'colLabel_Hidden':''?>" style='display:<?php echo $display?>'> 
   	<td align="right" class="colLabel"><?php echo $newftA[$i]?></td>
	<?php } elseif($newfnA[$i]!='post') { 
		   echo "<td>$newftA[$i]</td>";
		 } else echo "<tr name='tr_$newfnA[$i]' style='display:$display'><td align='right' class='colLabel'>點數</td><td><table style='font-size:12px'><tr><td>$newftA[$i]</td>"	;
	?>    
    
  	<td><?php  switch ($newetA[$i]) { 
			case "empID":	echo $_SESSION['EE_EID'];	break;
			case "readonly": echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "texv"    :  echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='".(float)$newfvA[$i]."' class='input' onKeyUp='doSum()'>"; break;
			case "text"    :  echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' onKeyUp='doSum()'>"; break;
			case "textbox" :  echo "<textarea id='$newfnA[$i]' name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea> <input type='button' value='編輯' onClick='HTMLEdit($newfnA[$i])'>"; break;
  		case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' id='$newfnA[$i]' value='$newfvA[$i]' class='optBtn'"; if ($newfvA[$i]=="true") echo " checked"; echo '>是'; break;
			case "radio":	echo "<input  type='radio' name='$newfnA[$i]' value='$newfvA[$i]'>是";	break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'  >";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true,onClose:chkDay});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input' onchange='chg()'>"; foreach($newfvA[$i] as $k=>$v){ $ans2=''; if($k==$changAry["salaryType"]) { $ans2 = 'selected';} echo "<option value='$k' $ans2>$v</option>";} echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
			case "multiPhoto":
				echo "<input type='file' name='".$newfnA[$i]."[]' size='$newflA[$i]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview' >";
				echo "<div class='photoItem'><img /><span id='fName'></span><a class='photoDel' href='#' >刪除</a><div class='clearfx'></div></div>";
				echo "</div>";
				break;	
			case "label":
				echo "<input type='hidden' name='$newfnA[$i]' value='0' id='$newfnA[$i]'>";
				echo "<div id='d_$newfnA[$i]'>".($changAry["sums"]==''?'0':$changAry["sums"])."</div>";
				break;
			
  	} ?>
    </td>
  <?php 
		if($editetA[$i]!='texv') echo "</tr>";
		if($editfnA[$i]=='transform') echo "</tr></table></td></tr>"; 
		?>  
  <?php } ?>
	
  <?php if(isset($oldData['salaryType']) && $oldData['salaryType']==1 ) $display='none' ?>
	<tr name="tr_salaryCt" style='display:<?php echo $display?>'> 
 		<td align="right" class="colLabel">薪資</td>
 		<td class="colContent"><label id="salaryCt" name="salaryCt" style="font-size:larger; font-weight:bolder"><?php echo $oldData['salary']*$oldData['ratio']?> 元</label></td>
	</tr>
   
  <tr>
  	<td colspan="2" align="center" class="rowSubmit">
    <input type='hidden' id='id' name='id' value="<?php echo $_SESSION['EE_EID']?>">
    <input type='hidden' id='bdate' name='bdate'>
    <input type='hidden' id='edate' name='edate'>
    <input type="submit" value="確定新增" class="btn">&nbsp;
  	<input type="reset" value="重新輸入" class="btn">&nbsp;
  	<input type="button" class="btn" onClick="history.back()" value="取消新增">
   	</td>
  </tr>
</table>
</form>
</body>
</html>
