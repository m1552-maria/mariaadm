<?php
	@session_start();
	include("../../config.php");
	include("init.php");
	$ID = $_REQUEST['ID'];
  $sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
	$icons = array(
		'doc'=>'icon_doc.gif','docx'=>'icon_doc.gif','pdf'=>'icon_pdf.gif','ppt'=>'icon_ppt.gif','pptx'=>'icon_ppt.gif',
	  'txt'=>'icon_txt.gif','xls'=>'icon_xls.gif','xlsx'=>'icon_xls.gif','zip'=>'icon_zip.gif','rar'=>'icon_rar.gif');
	$tmp_time= str_replace('-','/',$r['changeDate']);
	foreach($changAry["changeDate"] as $i=>$chV){
		if($changAry["changeDate"][$i]==$tmp_time){
			unset($changAry["changeDate"][$i]); //避免選到今日
		}	
	}
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
	.ui-widget {font-size:12px; }
	.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?php echo $extfiles?>edit.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <title><?php echo $pageTitle?> - 編輯</title>
 <script type="text/javascript">
 var date = <?php echo json_encode($changAry["changeDate"])?>;
 function chkDay(){
	for( var i in date){
		if($('#changeDate').val()== date[i]){
			alert("變更起日與不可重覆!");
			$('#changeDate').val('')
		}
	}
 }
 
 function chg(){
 	var salaryType=$("[name='salaryType']").val();
	var objects;
	$('#salary').removeAttr('readonly');
	switch(salaryType){
		case "0"://月薪
			objects=['hourfee','capacity_ratio'];
			showObj(objects,'N');
			objects=['post','sums','ratio','notes','salary','extra','salaryCt'];	//'food','manager','edu','license','seniority','response','allowance','special','transform'
			showObj(objects,'Y');
			showObj2('N');
			$('#salary').attr('readonly','readonly');
			break;
		case "1"://時薪 
			objects=['hourfee','notes','ratio'];
			showObj(objects,'Y');
			objects=['post','sums','salary','capacity_ratio','extra','salaryCt'];
			showObj(objects,'N');
			break;
		case "2"://產能敘薪
			objects=['salary','notes','ratio','capacity_ratio','salaryCt'];
			showObj(objects,'Y');
			objects=['hourfee','post','sums','extra'];
			showObj(objects,'N');
			break;
		case "3"://固定薪
			objects=['salary','notes','ratio','post','food','extra'];
			showObj(objects,'Y');
			objects=['hourfee','sums','capacity_ratio'];
			showObj(objects,'N');
			showObj2('Y');
			$('#salary').attr('readonly','readonly');
			break;	
	}
	$('#hourfee').val(0);
	$('#sums').val(0);
	$('#extra').val(0);
	$('#salary').val(0);
	doSum();
 }
 
 function showObj(objs,type){
   for(var i in objs){
		 if(type=='Y') $("[name='tr_"+objs[i]+"']").show(); else $("[name='tr_"+objs[i]+"']").hide();
	 }
 }
 function showObj2(type){
	 var objs = ['manager','edu','license','response','allowance','special','transform'];
	 if(type=='Y') {
		 $("tr[name='tr_post']").children('td:first-child').text('加給(元)');
		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().hide();
		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().prev().hide();
	 } else {
		 $("tr[name='tr_post']").children('td:first-child').text('點數');
 		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().show();
		 for(var i in objs) $("input[name='"+objs[i]+"']").parent().prev().show();
	 }
 }
 
 $(function(){ 
 	$("#prjform").keypress(checkFormEnter); 
	if($("[name='salaryType']").val()==3) showObj2('Y');
	doSum();
 });
 </script>
</Head>

<body class="page">
<form id="prjform" method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?php echo $pageTitle?> -【編輯作業】</td></tr>
	<tr>
 		<td align="right" class="colLabel"><?php echo $editftA[0]?></td>
 		<td class="colContent"><?php echo $ID?></td>
	</tr>

	<?php
	for ($i=1; $i<count($editfnA); $i++) { 
		$display=''; 
		if($r['salaryType']=='0'){//月薪
			if($editfnA[$i]=="hourfee" || $editfnA[$i]=='capacity_ratio')	$display='none';
		}else if($r['salaryType']=='1'){//時薪
			if($editfnA[$i]=="post" || $editfnA[$i]=="sums" || $editfnA[$i]=="salary" || $editfnA[$i]=="capacity_ratio" || $editfnA[$i]=="extra" || $editfnA[$i]=="salaryCt")	$display='none';
		}else if($r['salaryType']=='2'){//產能敘薪
			if($editfnA[$i]=="hourfee" || $editfnA[$i]=="post" || $editfnA[$i]=="sums" || $editfnA[$i]=="extra") $display='none';
		}else if($r['salaryType']=='3'){//固定薪
			if($editfnA[$i]=="hourfee" || $editfnA[$i]=="sums" || $editfnA[$i]=="capacity_ratio")	$display='none';
		}
				
	?>
 	<?php if($editetA[$i]!='texv') { ?>
	  <tr name="tr_<?php echo $editfnA[$i]?>" class="<?php echo  $editetA[$i]=='hidden'?'colLabel_Hidden':''?>" style='display:<?php echo $display?>'> 
   	<td align="right" class="colLabel"><?php echo $editftA[$i]?></td>
	<?php } elseif($editfnA[$i]!='post') { 
		   echo "<td>$editftA[$i]</td>";
		 } else echo "<tr name='tr_$editfnA[$i]' style='display:$display'><td align='right' class='colLabel'>點數</td><td><table style='font-size:12px'><tr><td>$editftA[$i]</td>"	;
	?>
  
  	<td>
		<?php switch($editetA[$i]) { 
  	 	case "texv" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='". (float)$r[$editfnA[$i]] ."' onKeyUp='doSum()'>"; break;
			case "text" :
				$readonly = ($editfnA[$i] == 'salary' && ($r['salaryType'] == 0 || $r['salaryType'] == 3)) ? 'readonly' : ''; 
			 echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."' onKeyUp='doSum()' $readonly>"; break;

			case "textbox" : echo "<textarea name='$editfnA[$i]' id='".$editfnA[$i]."' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea> <input type='button' value='編輯' onClick='HTMLEdit($editfnA[$i])'>"; break;
  		case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' id='".$editfnA[$i]."' onClick='setHigh(\"".$editfnA[$i]."\")' class='optBtn'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>是"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "radio":echo "<input  type='radio' name='$editfnA[$i]' value='$editfvA[$i]'>是";	break;
			case "select"  : 
					echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input' onchange='chg()'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; 
				break;
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input' >";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true,onClose:chkDay});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";				
				break;
			case "label":
				echo "<input type='hidden' name='$editfnA[$i]' value='".$r[$editfnA[$i]]."' id='$editfnA[$i]'>";
				echo "<div id='d_$editfnA[$i]'>".$r[$editfnA[$i]]."</div>";
				break;	
  	  } 
		?>
   	</td>
    <?php 
		if($editetA[$i]!='texv') echo "</tr>";
		if($editfnA[$i]=='transform') echo "</tr></table></td></tr>"; 
		?>
  <?php } ?>
	
  <?php if($r['salaryType']==1 ) $display='none' ?>
	<tr name="tr_salaryCt" style='display:<?php echo $display?>'> 
 		<td align="right" class="colLabel">薪資</td>
 		<td class="colContent"><label id="salaryCt" name="salaryCt" style="font-size:larger; font-weight:bolder"><?php echo $r['salary']*$r['ratio']?> 元</label></td>
	</tr>
  
  <tr>
    <td colspan="2" align="center" class="rowSubmit">
      <input type="hidden" name="selid" id='selid' value="<?php echo $ID?>">
      <input type="hidden" name="pages" value="<?php echo $_REQUEST['pages']?>">
      <input type="hidden" name="keyword" value="<?php echo $_REQUEST['keyword']?>">
      <input type="hidden" name="fieldname" value="<?php echo $_REQUEST['fieldname']?>">
      <input type="hidden" name="sortDirection" value="<?php echo $_REQUEST['sortDirection']?>">
      <input type='hidden' id='bdate' name='bdate' value="<?php echo $r['bdate']?>">
      <input type='hidden' id='edate' name='edate' value="<?php echo $r['edate']?>">
      <input type="submit" value="確定變更" class="btn">&nbsp;	
      <input type="reset" value="重新輸入" class="btn">&nbsp; 
      <input type="button" class="btn" onClick="history.back()" value="取消編輯">
    </td>
  </tr>
</table>
</form>
</body>
</html>
