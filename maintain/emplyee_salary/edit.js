﻿function HTMLEdit(thText) {
	var aobj = new Object();
	aobj.value = thText.value;
	var upath = "/htmledit/builded.php";
	ReturnStr = window.showModalDialog(upath,aobj,"dialogWidth=800px;dialogHeight=600px;scrollbars=no;help=no;status=no;location=no;edge=raised");	
	if((ReturnStr != null) && (ReturnStr != "")) {
		//alert(ReturnStr);
		thText.value = ReturnStr
	}
}

function OpenWin(adate) {
	var dateStr = adate.value;
	ReturnStr = window.showModalDialog("../selectTime.htm",dateStr,"dialogWidth=295px;dialogHeight=250px;scrollbars=no;help=no;status=no;edge=raised");
	if((ReturnStr != null) && (ReturnStr != "")) {
	  adate.value = ReturnStr;
  }
}

function formvalid(tfm) { 
	if($('#changeDate').val()==""){	alert("請選擇變更起日");	return false;	}
	var da = true;
	for (var ss in date) {
		if ($('#changeDate').val() == date[ss]) {
			alert("變更起日已經存在!");
			da = false;
			break;
		}
	}
	if (!da) { return false; }

	var field=["post","food","manager","edu","license","seniority","response","allowance","special","transform"];
	var ck=true;
	for(var i=0;i<field.length;i++){
		if(isNaN($("#"+field[i]).val())){
			alert("必須為數字");
			$("#"+field[i]).focus();
			ck=false;
			break;
		}
	}
	if(!ck){	return false;}
	
	return true;
}

function doSum(){
	if(event.target.id != 'salary') {
		if ($('select[name=salaryType]').val()=='3') {
			var sums=parseFloat($('#post').val())+parseFloat($('#food').val())+parseFloat($('#seniority').val());
	 		if(isNaN(sums)) return;
			$('#d_sums').val(''); $('#sums').val('');
		} else if($('select[name=salaryType]').val()=='0') {
			var sums=parseFloat($('#post').val())+parseFloat($('#food').val())+parseFloat($('#manager').val())+parseFloat($('#edu').val())+parseFloat($('#license').val())+parseFloat($('#seniority').val())+parseFloat($('#response').val())+parseFloat($('#allowance').val())+parseFloat($('#special').val())+parseFloat($('#transform').val());
	 		if(isNaN(sums)) return;
			$('#d_sums').html(sums); $('#sums').val(sums);	//點數合計
			sums=sums*30;			
		} else sums = parseFloat($('#salary').val());
		
		if(sums>=0) {
			if(parseInt($('#extra').val())) sums+= parseFloat($('#extra').val());
			sums=sums.toFixed(2); 
			$('#salary').val(sums);		//核定薪資
		}
	} else sums = $('#salary').val();
	
	if(sums>=0) {
		if(parseFloat($('#ratio').val())>=0) {
			//月薪的其他加給不需要乘工時比例
			if($('select[name=salaryType]').val()=='0' && $('#extra').val() > 0) {
				var extra = parseInt($('#extra').val());
				sums = parseInt(sums-extra)*parseFloat($('#ratio').val()) + extra;
			} else {
				sums = parseInt(sums)*parseFloat($('#ratio').val());
			}
			
		}
		if(parseFloat($('#capacity_ratio').val())>=0 && $('select[name=salaryType]').val()=='2') sums = sums*parseFloat($('#capacity_ratio').val());
		$('#salaryCt').text(sums+' 元');
	}
}