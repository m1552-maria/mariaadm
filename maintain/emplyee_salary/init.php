<?php 
	// for Access Permission Control
	if(!session_id()) session_start();
	if ($_SESSION['privilege']<10) {
	  	header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	if((isset($_SESSION["canEditEmp"]) && $_SESSION['privilege']<=10) || $_SESSION['privilege']>10){
		$bE2=true;
	}else{
		$bE2=false;
	}
	//$bE = true;
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "薪資異動";
	$tableName = "emplyee_salary";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['EE_EID'] = $eid;
	} else $eid = $_SESSION['EE_EID'];
	$sql= "select * from emplyee_salary where empID= '".$_SESSION["EE_EID"]."' order by id desc"; 
	$rs = db_query($sql);
	//echo $sql;
	$row=db_num_rows($rs);

	
	$filter = "empID='$eid'";
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "grade";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,changeDate,salaryType,hourfee,ratio,notes,sums,salary,rdate");
	$ftAry = explode(',',"ID,變更起日,給薪類別,時薪,工時比例,異動說明,合計點數,核定薪資(計算比例),登錄日期");
	$flAry = explode(',',"80,90,100,80,80,,80,120,90");
	$ftyAy = explode(',',"id,date,select,text,text,text,text,text,date");
	
	$isHighAry=array("0"=>'一般','1'=>'最高','2'=>'次高');
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"empID,changeDate,salaryType,hourfee,ratio,capacity_ratio,notes,post,food,manager,edu,license,seniority,response,allowance,special,transform,sums,extra,salary");
	$newftA = explode(',',"員工編號,變更起日,給薪類別,時薪,工時比例,產能敘薪比例,異動說明,職務,伙食,主管,學歷,證照,年資,責任,津貼,特別,轉換,合計點數,其他加給,核定薪資");
	$newflA = explode(',',",,,,,,27,3,3,3,3,3,3,3,3,3,3,,,");
	$newfhA = explode(',',",,,,,,3,,,,,,,,,,,,,");

	$changAry = array("changeDate"=>array(),"salaryType"=>'',"sums"=>'');
	$oldData = array();
	if(empty($row)){ //當沒有最新一筆的筆數時
		$newfvA = array('','',$salaryType,0,1,0.7,'',0,0,0,0,0,0,0,0,0,0,0,0,0);
	}else{
		$i=0;
		while($r=db_fetch_array($rs)){
			$oldData = $r;
			$i++;
			$rst[]=$r; //第一筆
			$chk_chdate[]=str_replace('-','/',$r['changeDate']);  //不能與之前的日子重複
		}
		$newfvA = array('','',$salaryType,$rst[0]['hourfee'],$rst[0]['ratio'],$rst[0]['capacity_ratio'],$rst[0]['notes'],$rst[0]['post'],$rst[0]['food'],$rst[0]['manager']
		,$rst[0]['edu'],$rst[0]['license'],$rst[0]['seniority'],$rst[0]['response'],$rst[0]['allowance'],$rst[0]['special'],$rst[0]['transform'],$rst[0]['sums'],$rst[0]['extra'],$rst[0]['salary']);
		$changAry = array("changeDate"=>$chk_chdate,"salaryType"=>$rst[0]['salaryType'],"sums"=>$rst[0]['sums']);
	}

	$newetA = explode(',',"empID,date,select,text,text,text,textbox,texv,texv,texv,texv,texv,texv,texv,texv,texv,texv,label,text,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,changeDate,salaryType,hourfee,ratio,capacity_ratio,notes,post,food,manager,edu,license,seniority,response,allowance,special,transform,sums,extra,salary");
	$editftA = explode(',',"ID,變更起日,給薪類別,時薪(元),工時比例,產能敘薪比例,異動說明,職務,伙食,主管,學歷,證照,年資,責任,津貼,特別,轉換,合計點數,其他加給(元),核定薪資(元)");
	$editflA = explode(',',",,,,,,27,3,3,3,3,3,3,3,3,3,3,,,");
	$editfhA = explode(',',",,,,,,3,,,,,,,,,,,,");
	$editfvA = array('','',$salaryType,'','','','','','','','','','','','','','','','','');
	$editetA = explode(',',"id,date,select,text,text,text,textbox,texv,texv,texv,texv,texv,texv,texv,texv,texv,texv,label,text,text");
?>