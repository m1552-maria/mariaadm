<?php 
	session_start();
	header("Content-Type:text/html; charset=utf-8");
	include("../../config.php");
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
.photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
.photoPreview .photoDesc { float:left; margin-left:5px; }
.photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
.photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
.photoPreview .photoItem .photoDel { margin:5px 0 0 5px; cursor:pointer; height:20px;}
.photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?php echo $extfiles?>append.js" ></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script>
 <script type="text/javascript">
 function chg(){
    var salaryType=$("[name='salaryType']").val();
	var objects;
	switch(salaryType){
		case "0"://月薪
			objects=['hourfee','salary','capacity_ratio'];
			showObj(objects,'N');
			objects=['post','food','manager','edu','license','seniority','response','allowance','special','transform','sums','ratio','notes'];
			showObj(objects,'Y');
			break;
		case "1"://時薪 
			objects=['hourfee','notes','ratio'];
			showObj(objects,'Y');
			objects=['post','food','manager','edu','license','seniority','response','allowance','special','transform','sums','salary','capacity_ratio'];
			showObj(objects,'N');
			break;
		case "2"://產能敘薪
			objects=['salary','notes','ratio','capacity_ratio'];
			showObj(objects,'Y');
			objects=['hourfee','post','food','manager','edu','license','seniority','response','allowance','special','transform','sums'];
			showObj(objects,'N');
			break;
		case "3"://固定薪
			objects=['salary','notes','ratio'];
			showObj(objects,'Y');
			objects=['hourfee','post','food','manager','edu','license','seniority','response','allowance','special','transform','sums','capacity_ratio'];
			showObj(objects,'N');
			break;
		
	}
 }
 function showObj(objs,type){
	for(var i in objs){
		if(type=='Y'){
			$("[name='tr_"+objs[i]+"']").show();
		}else{
			$("[name='tr_"+objs[i]+"']").hide();
		}
	 }
 }
 </script>
 <title><?php echo $pageTitle?> - 新增</title>
</Head>

<body class="page" style="overflow:auto">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?php echo $pageTitle?> -【新增作業】</td></tr>

	<?php for ($i=0; $i<count($newfnA); $i++) {
			if($newfnA[$i]=="hourfee" || $newfnA[$i]=="salary" || $newfnA[$i]=='capacity_ratio')	$display='none';	else	$display='';
	?>
    <tr name="tr_<?php echo $newfnA[$i]?>" class="<?php echo  $newetA[$i]=='hidden'?'colLabel_Hidden':''?>" style='display:<?php echo $display?>'>
		<td align="right" class="colLabel"><?php echo $newftA[$i]?></td>
  	<td><?php  switch ($newetA[$i]) { 
			case "empID":	echo $_SESSION['EE_EID'];	break;
			case "readonly": echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' onKeyUp='doSum()'>"; break;
			case "textbox" : echo "<textarea id='$newfnA[$i]' name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea> <input type='button' value='編輯' onClick='HTMLEdit($newfnA[$i])'>"; break;
  		case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' id='$newfnA[$i]' value='$newfvA[$i]' class='optBtn'"; if ($newfvA[$i]=="true") echo " checked"; echo '>是'; break;
			case "radio":	echo "<input  type='radio' name='$newfnA[$i]' value='$newfvA[$i]'>是";	break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input' onchange='chg()'>"; foreach($newfvA[$i] as $k=>$v) echo "<option value='$k'>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
			case "multiPhoto":
				echo "<input type='file' name='".$newfnA[$i]."[]' size='$newflA[$i]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview' >";
				echo "<div class='photoItem'><img /><span id='fName'></span><a class='photoDel' href='#' >刪除</a><div class='clearfx'></div></div>";
				echo "</div>";
				break;	
			case "label":
				echo "<input type='hidden' name='$newfnA[$i]' value='0' id='$newfnA[$i]'>";
				echo "<div id='d_$newfnA[$i]'>0</div>";
				break;
			
  	} ?></td>
  </tr><?php } ?>
  <tr>
  	<td colspan="2" align="center" class="rowSubmit">
    <input type='hidden' id='id' name='id' value="<?php echo $_SESSION['EE_EID']?>">
    <input type='hidden' id='bdate' name='bdate'>
    <input type='hidden' id='edate' name='edate'>
    <input type="submit" value="確定新增" class="btn">&nbsp;
  	<input type="reset" value="重新輸入" class="btn">&nbsp;
  	<input type="button" class="btn" onClick="history.back()" value="取消新增">
   	</td>
  </tr>
</table>
</form>
</body>
</html>
