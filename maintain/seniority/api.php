<?
	session_start();
	include("../../config.php");
	include('../../miclib.php');

	//處理時間//真正
	if(date('m-d')=='12-01' || date('m-d')=='01-01'){
		//才能排程
		if(date('m-d')=='12-01') {$now_date = date('Y').'-12-31';$now_year = date('Y');}
		elseif(date('m-d')=='01-01') {$now_date = (date('Y')-1).'-12-31';$now_year = (date('Y')-1);}

	}

	//處理時間//測試用
	if($_POST['date'] == '2017/1/1試算2017年度特休'){
		$now_date = (date('Y')-1).'-12-31';
		$now_year = (date('Y')-1);
	}elseif($_POST['date'] == '2016/12/1試算2017年度特休'){
		$now_date = date('Y').'-12-31';
		$now_year = date('Y');
	}


	//抓emplyee
	$emplyee_list = array();
	$sql = "select `empID`,`isOnduty`,`empName`,`hireDate` from `emplyee` where `isOnduty` IN (1,2)";
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		if($r['empID']!=''){
			$emplyee_list[$r['empID']] = $r;	
		}
	}
	
	//留職停薪//只算今年度
	$job_status = array();
	$sql = 'select * from `emplyee_status` where `jobStatus` IN (3,4) and (`bdate` BETWEEN '."'".$now_year.'-1-1'."'".' AND '."'".$now_year.'-12-31'."')";//留職停薪或是延長留停
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		$job_status[$r['empID']][$r['id']] = $r;
	}

	//原本就有的資料
	$annual_list = array();
	$sql = 'select * from `emplyee_annual` where `year`='.($now_year+1);//預算明年的特休
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		$annual_list[$r['empID']] = $r;
	}


	//開始計算特休 今年
	$insertdata = array();
	foreach($emplyee_list as $k=>$v){
		$now_hours = (strtotime($now_date) - strtotime($v['hireDate']))/3600;//相減是秒數 // 3600 算時數//86400 天數//年資
		if(isset($job_status[$k])){//代表有留職停薪
			//看有多少個留職停薪
			$leave = 0;
			foreach($job_status[$k] as $k1=>$v1){
				if($v1['edate']=='' || $v1['edate']=='0000-00-00'){
					$v1['edate'] = date('Y-m-d');//如果沒有日期預設結束日為今天日期
				}
				$leave = $leave+(strtotime($v1['edate']) - strtotime($v1['bdate']))/3600;//結束時間剪掉開始時間
			}
			//結果
			//算特休比例 (去年工作天數-去年留職停薪天數)/365
			$rate = (365-($leave/24))/365;
			$insertdata[$k]['hours'] = annual($now_hours,$rate);

		}else{//乖乖的算特休
			$insertdata[$k]['hours'] = annual($now_hours,1);
		}
	}


	//判斷年資
	function annual($now_hours,$leave){
		$hours = 0;
		//判斷時數要多少
		if($now_hours>=(365*24) && $now_hours<(365*24*3)){//大於一年在三年以下
			$hours = intval(7*8*$leave);
		}elseif($now_hours>=(365*24*3) && $now_hours<(365*24*5)){
			$hours = intval(10*8*$leave);
		}elseif($now_hours>=(365*24*5) && $now_hours<(365*24*10)){
			$hours = intval(14*8*$leave);
		}elseif($now_hours>=(365*24*10)){//超過10年每一年+1 最多不能超過30天 10年也一加一天
			//計算要加幾年上去
			$year = intval(($now_hours - (365*24*9))/24/365);//計算公式(14+年資-9)
			$total = 14+$year;
			if($total>30){
				$total = 30;
			}
			$hours = intval($total*8*$leave);
		}

		return $hours;
	}



	//寫資料到資料庫
	foreach($insertdata as $k=>$v){
		if(isset($annual_list[$k])){
			$sql = 'update `emplyee_annual` set `hours`='.$v['hours'].' where `id`='.$annual_list[$k]['id'];
			db_query($sql);
		}else{
			//預算明年的特休
			$sql = ' insert into `emplyee_annual` (`empID`,`year`,`hours`) values ('."'".$k."',".($now_year+1).','.$v['hours'].')';
			db_query($sql);
		}
	}

	echo '已經完成';
	exit;
	

?>