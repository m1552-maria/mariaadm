<?php
	session_start();
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
  <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script> 
 <title><?=$pageTitle?></title>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		url+="&<?=$paramsStr?>";
		url +='&year=<?=$curYr?>';
		url +='&month=<?=$curMo?>';
		// url+="&fid=<?=$_REQUEST[fid]?>";
		// url+="&type=<?=$_REQUEST[type]?>";
		// url+="&detail=<?=$_REQUEST[detail]?>";
		// url+="&depID=<?=$_REQUEST[depID]?>";
		//console.log(url);

		location.href = url;

	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
<?php if($_SESSION['privilege'] <= 10) { ?>
	<!--	<select name="projIDFilter" id="projIDFilter" onChange="filterSubmit(this)"><option value=''>-全部計畫-</option>
			<? 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo "<option value='$v'".($_REQUEST[projIDFilter]==$v?' selected ':'').">$v</option>";
		  	 
	    ?>
	    </select>-->
<?php }?>
    <select name="depFilter" id="depFilter" onChange="doSubmit()"><option value=''>-全部部門-</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
			foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>
    <select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
	    <select name="month" id="month" onChange="doSubmit()">
	    	<? 
	    		if($curYr<$nowYr){	$m=12;	}else{	$m=date('m');}
				
	    		for($i=1;$i<=$m;$i++) { 
	    			$sel=($curMo==str_pad($i,2,"0",STR_PAD_LEFT)?'selected':''); 
	    			echo $curMo.'=='.str_pad($i,2,"0",STR_PAD_LEFT);
	    			echo $sel;
	    			echo "<option $sel value='$i'>$i 月</option>"; 
	    		} 
	    	?>
	    </select>
	    <input type="button" name="toExcel" value="匯出Excel" onClick="$('#excelfm').submit()" >
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr bgcolor="#336699" height="25" valign="bottom" align="center" bordercolordark="#336699">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)) $sql="select *, r.id as fid from $tableName where ($filter)"; else $sql="select * from $tableName where 1";
  // 搜尋處理
  if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%'))";
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sqlExcel = $sql;
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	//echo $sql;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { 
		$id = $r[$fnAry[0]];
		$depNameList = getDepNameList($r['depID']);
		$r['depName'] = $depNameList[0];
		$r['depName2'] = $depNameList[1];
		$sql2 = "select * from emplyee_salary_items where fid='".$r['fid']."' and iType='insurence'";
		$rs2 = db_query($sql2,$conn);
		while ($r2=db_fetch_array($rs2)) {
			$r2['money'] = number_format($r2['money']);
			if($r2['isExpand'] == 0) {
				switch ($r2['title']) {
					case '勞保自負額':
						$r['labor'] = $r2['money'];
						break;
					case '健保自負額':
						$r['health'] = $r2['money'];
						break;
					case '勞退自提額':
						$r['retire'] = $r2['money'];
						break;
				}
			} else {
				switch ($r2['title']) {
					case '勞保自負額':
						$r['laborExpand'] = $r2['addsub'].$r2['money'];
						break;
					case '健保自負額':
						$r['healthExpand'] = $r2['addsub'].$r2['money'];
						break;
					case '勞退自提額':
						$r['retireExpand'] = $r2['addsub'].$r2['money'];
						break;
				}
			}
		}
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>


		<? for($i=0; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						default : echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>
<form name="excelfm" id="excelfm" action="excel.php" method="post" target="excel">
	<input type="hidden" name="sql" value="<?php echo $sqlExcel;?>">
	<input type="hidden" name="year" value="<?php echo $curYr;?>">
	<input type="hidden" name="month" value="<?php echo $curMo;?>">
</form>
<div align="right"><a href="#top">Top ↑</a></div>
<script type="text/javascript">
	function SearchKeyword() {
		rzlt = prompt("請輸入關鍵字",keyword);
	 	if (rzlt!=null) {
	 		//::過濾條件要跟
			var depflt = $('select#depFilter option:selected').val();
			var url='';
			if(depflt) url = url+'&depFilter='+depflt;
			url +='&year='+$("[name='year']").val();
			url +='&month='+$("[name='month']").val();
			location.href="list.php?keyword="+encodeURIComponent(rzlt)+url;
	 }	 
	}
	function doSubmit() {
		form1.submit();
	}
</script>
</body>
</Html>