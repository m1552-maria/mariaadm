<?
  session_start();

  $title = ((int)$_REQUEST['year']-1911).'年'.$_REQUEST['month'].'月薪資勞健退';
  $filename = $title.date("Y-m-d H:i:s",time()).'.xlsx';
  

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');

  $sql = $_REQUEST['sql'];

  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();

  $sheet->mergeCells('A1:L1');//合併
  $sheet->setCellValue('A1', $title);
  $sheet->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A1:L1')->getFont()->setSize(16);

  $header = array(
    'A'=>array('key'=>'empID','width'=>10,'val'=>array('員編')),
    'B'=>array('key'=>'empName','width'=>10,'val'=>array('姓名')),
    'C'=>array('key'=>'perID','width'=>12,'val'=>array('身分字號')),
    'D'=>array('key'=>'depName','width'=>15,'val'=>array('單位')),
    'E'=>array('key'=>'depName2','width'=>18,'val'=>array('組別')),
    'F'=>array('key'=>'projID','width'=>12,'val'=>array('計畫編號')),
    'G'=>array('key'=>'labor','width'=>15,'val'=>array('勞保費自負')),
    'H'=>array('key'=>'laborExpand','width'=>15,'val'=>array('勞保加減調整')),
    'I'=>array('key'=>'health','width'=>15,'val'=>array('健保費自負')),
    'J'=>array('key'=>'healthExpand','width'=>15,'val'=>array('健保加減調整')),
    'K'=>array('key'=>'retire','width'=>15,'val'=>array('勞退自提')),
    'L'=>array('key'=>'retireExpand','width'=>17,'val'=>array('勞退自提加減調整')),
  );

  foreach ($header as $k => $v) {
    $sheet->getColumnDimension($k)->setWidth($v['width']);  
    $sheet->setCellValue($k.'2', $v['val'][0]);
  }

  $rs = db_query($sql);


  $i = 3;
  while ($r=db_fetch_array($rs)) {
    $depNameList = getDepNameList($r['depID']);
    $r['depName'] = $depNameList[0];
    $r['depName2'] = $depNameList[1];
    $sql2 = "select * from emplyee_salary_items where fid='".$r['fid']."' and iType='insurence'";
    $rs2 = db_query($sql2,$conn);
    $r['empID'] = ' '.$r['empID'];
    while ($r2=db_fetch_array($rs2)) {
     // $r2['money'] = number_format($r2['money']);
      if($r2['isExpand'] == 0) {
        switch ($r2['title']) {
          case '勞保自負額':
            $r['labor'] = $r2['money'];
            break;
          case '健保自負額':
            $r['health'] = $r2['money'];
            break;
          case '勞退自提額':
            $r['retire'] = $r2['money'];
            break;
        }
      } else {
        switch ($r2['title']) {
          case '勞保自負額':
            $r['laborExpand'] = $r2['addsub'].$r2['money'];
            break;
          case '健保自負額':
            $r['healthExpand'] = $r2['addsub'].$r2['money'];
            break;
          case '勞退自提額':
            $r['retireExpand'] = $r2['addsub'].$r2['money'];
            break;
        }
      }
    }
    $sheet->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $sheet->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $sheet->getStyle('I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $sheet->getStyle('J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $sheet->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $sheet->getStyle('L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    foreach ($header as $k => $v) {
      switch ($v['key']) {
        case 'labor':
        case 'laborExpand':
        case 'health':
        case 'healthExpand':
        case 'retire':
        case 'retireExpand':
          $sheet->getStyle($k.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getCell($k.$i)->setValueExplicit($r[$v['key']], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          break;
        
        default:
          $sheet->setCellValue($k.$i, $r[$v['key']]);
          break;
      }
     
    }
    $i++;
  }

  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');



?>