<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
		
	$bE = $_SESSION['privilege']>10?true:false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "職銜資料維護";
	$tableName = "jobs";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/jobs/';
	$delField = 'ahead';
	$delFlag = true;
	$imgpath = '../../data/jobs/';
	
	// Here for List.asp ======================================= //
	$defaultOrder = "jobID";
	$searchField1 = "jobID";
	$searchField2 = "jobTitle";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"jobID,jobTitle,jobName,jobEn,description,rdate");
	$ftAry = explode(',',"編號,全稱,簡稱,英文名稱,職掌敘述,登錄時間");
	$flAry = explode(',',"80,150,,,,");
	$ftyAy = explode(',',"ID,text,text,text,text,date");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"jobID,jobTitle,jobName,jobEn,description");
	$newftA = explode(',',"職掌編號,職掌全稱,職掌簡稱,英文名稱,職掌敘述");
	$newflA = explode(',',",60,60,60,80");
	$newfhA = explode(',',",,,,6");
	$newfvA = array('','','','','');
	$newetA = explode(',',"text,text,text,text,textbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"jobID,jobTitle,jobName,jobEn,description");
	$editftA = explode(',',"職掌編號,職掌全稱,職掌簡稱,英文名稱,職掌敘述");
	$editflA = explode(',',",60,60,60,80");
	$editfhA = explode(',',",,,,6");
	$editetA = explode(',',"text,text,text,text,textbox");
?>