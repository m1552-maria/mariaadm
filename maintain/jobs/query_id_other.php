<?php
	include 'init.php';
  $rs = db_query("select * from $tableName");
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font:15px "微軟正黑體",Verdana;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	#browser li {
		list-style:none;
		list-style-position: outside;
		padding: 0 4px 0 4px; 
		cursor:pointer;
	}
	.spanIm{
		margin: 0 2px;
		background: url(../../images/job.png) 0 0 no-repeat;
	    padding: 1px 0 1px 16px;
	    width: 10px;
	    height: 10px;
	    background-size: contain;

	}
  </style>
  <script type="text/javascript" src="/Scripts/jquery.js"></script>    
  <script type="text/javascript">
		var selObjs = new Array();
		$(document).ready(function(){
		});
			
	 	function setID(obj) {
	 		selObjs = [];
	 		$('[type="checkbox"]:checked').each(function(){
	 			selObjs.push({'id':$(this).val(),'title':$(this).attr('data-name')});
	 		});
	  	}
		function returnSeletions() {
			window.opener.returnPluralVal(selObjs);
			window.opener.rzt = false;
			window.close(); 
			//console.log(selObjs);
		}		
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
	<tr>
   	<th id="tblHead" bgcolor="#CCCCCC">::: 職掌代碼表 :::<button style='margin-left:30px' onClick='returnSeletions()'>確定送出</button></th>    
  </tr>
	<tr><td bgcolor="#FFFFFF">
    <div id="markup" class="whitebg"><ul id="browser">
    <?php while($r=db_fetch_array($rs)) echo "<li><label><input onClick=setID(this) type='checkbox' name='job[]' value='".$r['jobID']."' data-name='".$r['jobName']."'><span class='spanIm'></span><a>".$r['jobName']."</a></label></li>"; ?> 
    </ul></div>
	</td></tr>
</table>
</body>
</html>