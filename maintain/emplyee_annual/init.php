<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	
	$bE = false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "年資/特休";
	$tableName = "emplyee_annual as a left join emplyee as b";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee_annual/';
	$delField = 'ahead';
	$delFlag = false;
	$imgpath = '../../data/emplyee_annual/';

	//預設年度
	$nowYr = date('Y');
	$curYr = $_REQUEST['year']?$_REQUEST['year']:$nowYr;
	
	$yearList = array($nowYr+1=>$nowYr+1);
	$sql = "select year from emplyee_annual group by year order by year desc";
	$rs = db_query($sql);
	while ($r = db_fetch_array($rs)) {
		$yearList[$r['year']] = $r['year'];
	}
	if(empty($yearList[date('Y')])) $yearList[date('Y')] = date('Y');

	if(!isset($yearList[$curYr])) $curYr = key($yearList);

	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "depID=''";
	}

	if($_REQUEST['year']!=''){
		$temp[] = "a.year='$_REQUEST[year]'";	
	}else{
		$temp[] = "a.year='$curYr'";
	}
	$temp[] = "(b.isOnduty=1 OR b.isOnduty=2)";//不要看到離職員工的資料
	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //

	//left join 條件
	$defWh = "b.empID=a.empID";


	$defaultOrder = "b.depID,b.empID";
	$searchField1 = "b.empID";
	$searchField2 = "b.empName";

	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	// $fnAry = explode(',',"empID,empName,depID,spHolidays,actDays,trueDays,action");
	// $ftAry = explode(',',"編號,姓名,部門,今年度特休,會計年資,實際年資,調整");
	// $flAry = explode(',',"50,,,,,,,,,");
	// $ftyAy = explode(',',"id,text,text,text,text,text,text,text,text");

	$fnAry = explode(',',"empID,empName,depID,spHolidays,lastHours,hours,isDelay,actDays,trueDays,hireDate");
	$ftAry = explode(',',"編號,姓名,部門,年度特休,去年剩餘時數,特休總時數,申請延休,會計年資,實際年資(算到今天),到職日");
	$flAry = explode(',',"50,,,,,,,,,,,,");
	$ftyAy = explode(',',"id,text,text,text,text,text,text,text,text,text,text,text");
?>