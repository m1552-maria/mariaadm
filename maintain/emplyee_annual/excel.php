<?php
	session_start();
  	include_once('../../config.php');
  	include_once('../../getEmplyeeInfo.php');  
 	require_once('../Classes/PHPExcel.php');
  	require_once('../Classes/PHPExcel/IOFactory.php');
    include_once('../absence/getYears.php');  

  	$filename = date("Y-m-d").'.xlsx';//ie中文檔名亂碼
    header("Content-type:application/vnd.ms-excel");
    header("Content-Type:text/html; charset=utf-8");
    /*20171218 sean 增加判別選擇的年度是否已經出現在試算表(emplyee_annual)*/
    $sql = "select 1 from emplyee_annual where year = '".$_REQUEST['year']."'";
    $rs = db_query($sql);
    if(db_num_rows($rs) == 0) {
      echo $_REQUEST['year'].'特休尚未試算, 請先到 年資/特休 進行試算';
      exit;
    }
    header('Content-Disposition:attachment;filename='.iconv('utf-8', 'big5', urlencode('員工特休時數管理').$filename));//中文檔名亂碼的問題
    header("Pragma:no-cache");
    header("Expires:0");

    
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);
    $sheet = $objPHPExcel->getActiveSheet();
    // 設定欄位背景色(方法1)
    $sheet->getStyle('A5:L5')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    //合併儲存隔 
    $sheet->mergeCells("A1:G1");
    $sheet->mergeCells("B2:G2");
    $sheet->mergeCells("B3:G3");
    $sheet->mergeCells("B4:G4");
    //對齊方式
    $sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet->getStyle('A5:J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    


    $sheet->setCellValue('A1','員工特休假時數管理');
    $sheet->getStyle('A1')->getFont()->setSize(18);

    $sheet->setCellValue('A2','製作日期');
    $sheet->setCellValue('A3','製表日期');
    $sheet->setCellValue('A4','部門名稱');
    $sheet->setCellValue('B2',date('Y/m/d'));
    $sheet->setCellValue('B3',$_REQUEST['year'].'全年合計表');
    if($_REQUEST['depID']!=''){
      $dep = $departmentinfo[$_REQUEST['depID']];
    }else{
      $dep = '全部部門';
    }
    $sheet->setCellValue('B4',$dep);



    //跑header 迴圈用
    $headerstart = 5;
    $header = array('A','B','C','D','E','F','G','H','I','J','K','L');
    $headervalue = array('流水號','員工編號','員工名稱','所屬部門','到職日','工時身分','總年資',$_REQUEST['year'].'特休假總時數','已申請特休時數','剩餘特休時數','未休特休工資','申請延休');
    foreach($header as $k=>$v){
      $sheet->setCellValue($v.$headerstart,$headervalue[$k]);   
    }
    $sheet->getStyle('A1:L5')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


    //處理請假
    $emp_abs = array();
    $sql = "select empID,SUM(hours) AS abhours from absence where isOK=1 and isDel=0 and aType='特休' and SUBSTR(bDate,1,4)='".$_REQUEST['year']."' group by empID order by empID";
    $rs = db_query($sql);
    while($rs && $r=db_fetch_array($rs)) {
        $emp_abs[$r['empID']] = $r['abhours'];

    }

    

    //處理特休
    $emplyee_annual = array();
    if($_REQUEST['depID']!=''){
        //抓emplyee的特休
        /*20171218 sean 加入取得特休試算的emplyee_annual*/ 
        $sql = "select e.empID,ea.hours as spHolidays,ea.isDelay,e.empName,e.depID,e.jobClass,e.hireDate  
                from emplyee as e inner join emplyee_annual as ea on e.empID = ea.empID
                where e.isOnduty in (1,2) and e.depID LIKE '".$_REQUEST['depID']."%' 
                and ea.year='".$_REQUEST['year']."'";
        /*$sql = "select empID,spHolidays,empName,depID,jobClass,hireDate  
                from emplyee
                where isOnduty in (1,2) and depID LIKE '".$_REQUEST['depID']."%' 
                order by depID,empID ";*/
    }else{
      //抓emplyee的特休
      /*20171218 sean 加入取得特休試算的emplyee_annual*/ 
      $sql = "select e.empID,ea.hours as spHolidays, ea.isDelay ,e.empName,e.depID,e.jobClass,e.hireDate from emplyee as e inner join emplyee_annual as ea on e.empID = ea.empID where (e.isOnduty=1 OR e.isOnduty=2) and ea.year='".$_REQUEST['year']."'";
    
      if($_SESSION['user_classdef'][2]) {//前台權限
        $aa = array();
        foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="e.depID like '$v%'";
        $deplist = "(".join(' or ',$aa).")";
        $sql.= " and $deplist ";
      } 
       /*if($_SESSION['user_classdef'][2]) {//前台權限
            $aa = array();
            foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
            $deplist = "(".join(' or ',$aa).")";
            $sql = "select empID,spHolidays,empName,depID,jobClass,hireDate from emplyee
                    where (isOnduty=1 OR isOnduty=2) and $deplist  order by depID,empID";  
          }else{
            $sql = "select empID,spHolidays,empName,depID,jobClass,hireDate from emplyee
                    where (isOnduty=1 OR isOnduty=2) order by depID,empID";  
          }*/
    }

    //判斷是否有搜尋條件
    if($_REQUEST['keyword'] != '') {
      $_sql = $_REQUEST['sql']." group by A.empID";
      $rs = db_query($_sql);
      $empIDList = array();
      while ($r=db_fetch_array($rs)) {
        $empIDList[] = "'".$r['empID']."'";
      }
      if(count($empIDList) > 0) {
        $sql .= " and e.empID in (".implode(',', $empIDList).")";
      }
    }

    $sql.= " order by e.depID,e.empID";

    $rs = db_query($sql);

    if(!db_eof($rs)){
        include_once('../emplyee_salary_report/api.php');
        while($r=db_fetch_array($rs)) {
          $emplyee_annual[$r['empID']] = $r;

          if(isset($emp_abs[$r['empID']])){
            $emplyee_annual[$r['empID']]['abhours'] = $emp_abs[$r['empID']];
          }else{
            $emplyee_annual[$r['empID']]['abhours'] = 0;
          }
          
          /*當年延修就試算特休薪資就規0*/
          if($r['isDelay'] == 1) {
            $emplyee_annual[$r['empID']]['wage'] = 0;
            continue;
          }

          /* 引用 /emplyee_salary_report/api.php 的basic來計算出時薪水多少
          $nowY~ $leaveD 為basic需要的變數
          需要用全薪去計算出時薪 所以basic 的 $hireD 統一用1/1日 $leaveD不需要給
           */
          $nowY = $_REQUEST['year'];
          $nowM = 12;
          $days_in_month = 31;
          $base_days = 30;
          $hireD = $_REQUEST['year'].'-01-01'; //
          $leaveD = '';

          //$wage=round(basic($r[empID]));
          $basicData = basic($hireD,$leaveD,$r[empID]);
          $wage = $basicData[3];
          
          $emplyee_annual[$r['empID']]['wage']=$wage;

        }
    }
    $years = getYears($emplyee_annual);

    $i = 6;
    $total = 1;
    foreach($emplyee_annual as $k=>$v){
      $sheet->setCellValue('A'.$i,$total);
      $sheet->setCellValueExplicit('B'.$i, $v['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
      $sheet->setCellValue('C'.$i,$v['empName']);
      $sheet->setCellValue('D'.$i,$departmentinfo[$v['depID']]);
      $sheet->setCellValue('E'.$i,$v['hireDate']);
      $sheet->setCellValue('F'.$i,$v['jobClass']);
      $sheet->setCellValue('G'.$i,$years[$v[empID]]);
      $sheet->setCellValue('H'.$i,$v['spHolidays']);
      $sheet->setCellValue('I'.$i,$v['abhours']);
      $sheet->setCellValue('J'.$i,$v['spHolidays']-$v['abhours']);
      $sheet->setCellValue('K'.$i,round(($v['spHolidays']-$v['abhours']) * $v['wage']));
      $sheet->setCellValue('L'.$i,($v['isDelay']) ? 'V' : '');


      $sheet->getStyle('A'.$i.':L'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
      $total++;

    }
   
   
    //設定欄寬
    $width = array(20,20,20,20,20,20,20,20,20,20,20,20);
    foreach($header as $k=>$v){
      $sheet->getColumnDimension($v)->setWidth($width[$k]);  
    }

    
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->setPreCalculateFormulas(false);
    
    $objWriter->save('php://output');

?>