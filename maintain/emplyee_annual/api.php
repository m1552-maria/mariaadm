<?
	//年資或是會計年資都要扣掉留職停薪
	//年資只算到去年年底//今年變動的留職停薪不會影響年資
	//這樣算年特休才不會有問題	
	
	//api 用 因為要馬上回傳時間所以要帶入時間
	
	include_once("../../config.php");
	include_once('../../miclib.php');

	
	$now_year = date('Y');
	//今年和明年會需要的東西的array
	$all_data[$now_year] = array(
			'now_year'=>date('Y'),
			'now_date'=>date('Y').'-12-31',
			'before_date'=>(date('Y')-1).'-12-31',
			'now_start_day'=>strtotime((date('Y')-1).'-01-01'),
			'now_end_day'=> strtotime((date('Y')-1).'-12-31')
		);
	$all_data[($now_year+1)] = array(
			'now_year'=>(date('Y')+1),
			'now_date'=>(date('Y')+1).'-12-31',
			'before_date'=>date('Y').'-12-31',
			'now_start_day'=>strtotime((date('Y')).'-01-01'),
			'now_end_day'=> strtotime((date('Y')).'-12-31')
		);




	//抓emplyee員工列表
	// $for_empID = '00731';

	//for_empID 為了產能敘薪那邊要帶入

	$emplyee_list = array();
	if(isset($_REQUEST['empID']) && $_REQUEST['empID']!='' && $_REQUEST['empID']!=0){
		$sql = 'select `empID`,`isOnduty`,`empName`,`hireDate` from `emplyee` where `isOnduty` IN (1,2) and `empID`='."'".$_REQUEST['empID']."'";
	}elseif(isset($for_empID) && $for_empID!=''){
		$sql = 'select `empID`,`isOnduty`,`empName`,`hireDate` from `emplyee` where `isOnduty` IN (1,2) and `empID`='."'".$for_empID."'";
	}else{
		$sql = "select `empID`,`isOnduty`,`empName`,`hireDate` from `emplyee` where `isOnduty` IN (1,2)";//在職 留職停薪 因為這兩種狀態都是在職	
	}
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		if($r['empID']!='' && $r['hireDate']!=''){
			$emplyee_list[$r['empID']] = $r;
		}
	}
	

	
	//留職停薪
	$job_status = array();
	if(isset($_REQUEST['empID']) && $_REQUEST['empID']!='' && $_REQUEST['empID']!=0){
		$sql = 'select * from `emplyee_status` where `jobStatus` IN (3,4) and `empID`='."'".$_REQUEST['empID']."'";//留職停薪或是延長留停
	}elseif(isset($for_empID) && $for_empID!=''){
		$sql = 'select * from `emplyee_status` where `jobStatus` IN (3,4) and `empID`='."'".$for_empID."'";//留職停薪或是延長留停
	}else{
		$sql = 'select * from `emplyee_status` where `jobStatus` IN (3,4)';//留職停薪或是延長留停
	}
	
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		$bdate = strtotime($r['bdate']);
		$edate = strtotime($r['edate']);


		if($edate>$bdate){
			//處理今年和明年的留職停薪
			//今年和明年//有4種可能//處理跨年分的留職停薪
			foreach($all_data as $k=>$v){
				if($bdate>=$v['now_start_day'] && $edate<=$v['now_end_day']){//一般正常結果
					$job_status[$v['now_year']][$r['empID']][$r['id']] = $r;
				}elseif($bdate >= $v['now_start_day'] && $edate>$v['now_end_day'] && $bdate<=$v['now_end_day']){
					$job_status[$v['now_year']][$r['empID']][$r['id']] = $r;
					$job_status[$v['now_year']][$r['empID']][$r['id']]['edate'] = ($v['now_year']-1).'-12-31';//因為不能影響到原本的
				}elseif($bdate<$v['now_start_day'] && $edate<=$v['now_end_day'] && $edate>=$v['now_start_day']){
					$job_status[$v['now_year']][$r['empID']][$r['id']] = $r;
					$job_status[$v['now_year']][$r['empID']][$r['id']]['bdate'] = ($v['now_year']-1).'-01-01';
				}elseif($bdate<$v['now_start_day'] && $edate>$v['now_end_day']){
					$job_status[$v['now_year']][$r['empID']][$r['id']] = $r;
					$job_status[$v['now_year']][$r['empID']][$r['id']]['edate'] = ($v['now_year']-1).'-12-31';
					$job_status[$v['now_year']][$r['empID']][$r['id']]['bdate'] = ($v['now_year']-1).'-01-01';
				}
			}

			$job_status['all_year'][$r['empID']][$r['id']] = $r;//每一年的留職停薪
		}
	}

	// echo '<pre>';
	// print_r($job_status);


	

	//薪資狀況emplyee_salary//如果是月薪和產能敘薪
	$emplyee_salary = array();
	if(isset($_REQUEST['empID']) && $_REQUEST['empID']!='' && $_REQUEST['empID']!=0){
		$sql = 'select `empID`,`salaryType`,`ratio`,`capacity_ratio` from `emplyee_salary` where `empID`='.$_REQUEST['empID'].' order by `changeDate`';//只有月薪和產能敘薪要列出來
	}elseif(isset($for_empID) && $for_empID!=''){
		$sql = 'select `empID`,`salaryType`,`ratio`,`capacity_ratio` from `emplyee_salary` where `empID`='.$for_empID.' order by `changeDate`';//只有月薪和產能敘薪要列出來
	}else{
		$sql = 'select `empID`,`salaryType`,`ratio`,`capacity_ratio` from `emplyee_salary` order by `changeDate`';//只有月薪和產能敘薪要列出來
	}
	
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		//判斷產能敘薪和工時比例有沒有超過1
		if($r['ratio']>1){//超過1都用1計算
			$r['ratio'] = 1;
		}
		if($r['capacity_ratio']>1){//超過1都用1計算
			$r['capacity_ratio'] = 1;
		}


		//計算比例
		$ratio=1;
		$capacity_ratio=1;
		//月薪和產能敘薪的工時比例//表示有薪資異動
		if($r['salaryType']==2){//產能敘薪
			$ratio = $r['ratio'];
			$capacity_ratio = $r['capacity_ratio'];
		}else{
			$ratio = $r['ratio'];//其他都要乘上工時比例
		}
		$ratio = $ratio*$capacity_ratio;
		$r['result_ratio'] = $ratio;

		$emplyee_salary[$r['empID']] = $r;//只會存最近的一筆
	}


// echo '<Pre>';
// print_r($emplyee_salary);
// exit;

	//開始計算特休
	$insertdata = array();
	foreach($emplyee_list as $k=>$v){
		foreach($all_data as $k1=>$v1){//今年和明年特休
			//宣告全部變數
			$now_year = $v1['now_year'];
			$now_date = $v1['now_date'];
			$before_date = $v1['before_date'];
			

			//計算年資
			$now_day = (strtotime($now_date) - strtotime($v['hireDate']))/86400+1;//天數
			//判斷留職停薪//去年之前的
			$leave = 0;//留職停薪日數
			if(isset($job_status['all_year'][$k])){
				foreach($job_status['all_year'][$k] as $k2=>$v2){
					//處理輸入錯誤的
					if($v2['bdate'] == '' || $v2['bdate']=='0000-00-00'){
						$v2['bdate'] = $before_date;//去年年底
					}
					if($v2['edate'] == '' || $v2['edate']=='0000-00-00'){
						$v2['edate'] = $before_date;//去年年底
					}


					//判斷要算到甚麼時候//計算到今年以前的年底//有符合才要扣掉
					$bdate = strtotime($v2['bdate']);
					$edate = strtotime($v2['edate']);

					if($edate>$bdate){//這樣才是有效數據
						if($bdate<strtotime($before_date)){//計算到去年的留職停薪總共有多少天//每一年的留職停薪//當年不能計算
							if($edate>strtotime($before_date)){//表示超過去年年底了
								$leave = $leave + ((strtotime($before_date)-$bdate))/86400+1;
							}else{//正常情況
								$leave = $leave + ($edate-$bdate)/86400+1;
							}
						}
					}
				}
			}

			//計算會計年資
			$actDays = (strtotime($before_date) - strtotime($v['hireDate']))/86400+1-$leave;//會計年資要扣掉留職停薪//天數
			if($actDays<0){
				$actDays = 0;//因為不滿一年
			}
			
			//計算產能敘薪比例
			$ratio = $emplyee_salary[$k]['result_ratio'];

			
			$now = intval($actDays/365);//有幾年
			// echo 'now_year:'.$now_year.'</br>';
			// echo 'now:'.$now.'</br>';
			// echo 'actDays:'.$actDays.'</br>';
			// echo 'leave:'.$leave.'</br>';

			//計算總共月數
			$total = $now*12 + intval(($actDays - (365*$now))/30);//總共月數//因為要處理誤差
			// echo 'total:'.$total.'</br>';


			// $total = intval($actDays/30);//總共月數//用會計年資判斷

			//處理去年留職停薪，為了處理去年溢請的特休
			$result = 0;
			foreach($job_status[$now_year][$k] as $k2=>$v2){
				$result = $result + intval(((strtotime($v2['edate']) - strtotime($v2['bdate']))/86400+1)/30);//去年留職停薪月數//加一是因為會有一天誤差
			}
			// echo 'result:'.$result.'</br>';
			
			
			//計算時數
			$hours = 0;
			$before_hours = 0;//前一段的時間
			$after_hours = 0;//後一段的時間
			$is_old = 0;//是否是資深員工
			if($actDays>0){
				//-----處理留職停薪造成的特休比例-----
				$after = $total - $now*12;//後面的月份
				$before = 12-($total - $now*12 );//前面的月份
				if($now<1){//只有在未滿一年才會出現
					if($before>6){
						$before = 6;
					}
				}
				//處理比例//因為留職停薪//處理去年溢請特休
				if($result>$before){
					$after = $after-($result-$before);
					$before = 0;
				}else{
					$before = $before-$result;
				}

				// if($result>$after){
				// 	$before = $before-($result-$after);
				// 	$after = 0;
				// }else{
				// 	$after = $after-$result;
				// }
				
				//-----處理留職停薪造成的特休比例---------end

				//計算特休--------------
				if($now<1){//可能會有一年和6個月的情況
					$hours = 7*8*$after/12;//一年佔的比例
					$after_hours = $hours;
					$hours = $hours + 3*8*$before/6;//6個月佔的比例
					$before_hours = $hours-$after_hours;
				}elseif($now>=1 && $now<2){
					$hours = 10*8*$after/12;//兩年佔的比例
					$after_hours = $hours;
					$hours = $hours + 7*8*$before/12;
					$before_hours = $hours-$after_hours;
				}elseif($now>=2 && $now<3){
					$hours = 14*8*$after/12;
					$after_hours = $hours;
					$hours = $hours + 10*8*$before/12;
					$before_hours = $hours-$after_hours;
				}elseif($now>=3 && $now<5){
					if($now<4){
						$hours = 14*8*$after/12;
					}else{
						$hours = 15*8*$after/12;
					}
					$after_hours = $hours;
					$hours = $hours + 14*8*$before/12;
					$before_hours = $hours-$after_hours;
				}elseif($now>=5 && $now<10){
					if($now<9){
						$hours = 15*8*$after/12;
					}else{
						$hours = 16*8*$after/12;
					}
					$after_hours = $hours;
					$hours = $hours + 15*8*$before/12;
					$before_hours = $hours-$after_hours;
				}elseif($now>=10){
					$hours = (16+($now-9))*8*$after/12;
					$after_hours = $hours;
					$hours = $hours + (16+($now-10))*8*$before/12;
					$before_hours = $hours-$after_hours;
					//不能超過30天//超過都以30天計算
					if($hours>(30*8)){
						$hours = 240;
						$is_old = 1;
					}
				}
			}else{
				//實際特休//今年剛到職的人
				$total = intval($now_day/30);//總共月數
				if(($total-$result)>=6){//表示有超過6個月//計算六個月比例有多少
					$hours = 3*8*($total-$result-6)/6;//6個月佔的比例
				}//如果是其他的就算有留職停薪也不算在內了
			}

			// echo 'hours:'.$hours.'</br>';
			$hours = $hours*$ratio;
			// echo 'ratio:'.$ratio.'</br>';

			$hours = ceil_dec($hours);//0.5為單位

			$insertdata[$now_year][$k]['actDays'] = $actDays;
			$insertdata[$now_year][$k]['hours'] = $hours;

			if(isset($_REQUEST['now_year']) && $_REQUEST['now_year']!=''){
				if($_REQUEST['now_year']==$now_year){
					//看要回傳什麼 callback是否今年有留職停薪(-1,1資深員工超過240hr,0),時數,會計年資,前面的比例,後面的比例,前面的時數,後面的時數,工時比例[工時比例*產能敘薪比例])
					if($result==0){
						if($is_old==0){
							echo '0,'.$hours.','.$actDays.','.$before.','.$after.','.$before_hours.','.$after_hours.','.$ratio;
						}else{
							echo '1,'.$hours.','.$actDays;
						}
					}else{
						echo '-1,'.$hours.','.$actDays;
					}
				}
			}
			
		}
	}

	function ceil_dec($num){//0.5進位
	    $wholeNumber = (int) $num;
	    $dec = $num - $wholeNumber;
	    if ($dec == 0){
	      $finalNumber = $wholeNumber;
	    }else if ($dec <= 0.5){
	      $finalNumber = $wholeNumber + 0.5;
	    }else if ($dec > 0.5){
	      $finalNumber = $wholeNumber + 1;
	    }
	    return $finalNumber ;
	}



	//看有沒有資料
	$emplyee_annual = array();
	$sql = 'select * from `emplyee_annual`';
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		$emplyee_annual[$r['year']][$r['empID']] = $r;
	}
	


	//寫資料到資料庫//今年和明年
	foreach($insertdata as $k=>$v){
		foreach($v as $k1=>$v1){
			if(isset($emplyee_annual[$k][$k1])){
				$sql = ' update `emplyee_annual` set `hours`='.$v1['hours'].',`actDays`='.$v1['actDays'].' where `empID`='."'".$k1."'".' AND `year`='.$k;
				db_query($sql);	
			}else{
				$sql = ' insert into `emplyee_annual` (`empID`,`year`,`hours`,`actDays`) values ('."'".$k1."',".$k.','.$v1['hours'].','.$v1['actDays'].')';
				db_query($sql);	
			}
		}
	}



	//不管怎樣都要把資料寫到emplyee_spholiday
	//要把資料匯到emplyee裡面的spholiday
	//先不要複寫
	// if(isset($emplyee_annual[$now_year])){
	// 	foreach($emplyee_annual[$now_year] as $k=>$v){
	// 		$sql = ' update `emplyee` set `spHolidays`='.$v['hours'].' where `empID`='."'".$k."'";
	// 		db_query($sql);
	// 	}
	// }


?>