<?php
	

	if(!isset($_POST['depID']) || !isset($_POST['wkYear'])) exit;
	$nowY = (int)date('Y');
	if((int)$_POST['wkYear'] < $nowY || (int)$_POST['wkYear'] > $nowY+1) exit;

	$eA = array();
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once("emplyeeAnnualApi.php");

	$eAData = array('wkYear'=>$_POST['wkYear'],'insertDB'=>1, 'noRecalculate'=>1);
	if($_POST['depID'] != '') $eAData['depID'] = $_POST['depID'];
	$eA = emplyeeAnnualApi($eAData);
	
	echo json_encode($eA);
?>