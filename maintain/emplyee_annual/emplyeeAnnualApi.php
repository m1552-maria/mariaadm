<?php
	/*
		特休時數(預給)計算公式

		= (特休天數 * (工時比例A所佔天數 /計算年前一年起始工作日,到計算年滿N年的工作日) * (8*工時比例A)) 
		+ (特休天數 * (工時比例B所佔天數 /計算年前一年起始工作日,到計算年滿N年的工作日) * (8*工時比例B)) 
		+ .....

		ex: 計算2018特休
		到職日 2017/01/10 工時比例0.5, 2017/06/01 工時比例調整為 0.8, 2018/01/01 工時比例調整為 1
		2018年 特休天數 :7 (2018/01/09會滿一年)
		計算年前一年,到計算年滿N年的工作天 = 2018/01/10 - 2017/01/10 = 365;

		特休時數 = 
		  (7 * ((2018/01/10 - 2018/01/01) / 365) * (8 * 1)) 
		+ (7 * ((2018/01/01 - 2017/06/01) / 365) * (8 * 0.8)) 
		+ (7 * ((2017/06/01 - 2017/01/10) / 365) * (8 * 0.5)) 
		= 38.5

	*/

	/*服務年資與特休對照*/
	$holidayList = array(
		50=>30, 49=>30, 48=>30,
		47=>30, 46=>30, 45=>30, 44=>30, 43=>30, 42=>30, 41=>30, 40=>30, 39=>30,
		38=>30, 37=>30, 36=>30, 35=>30, 34=>30, 33=>30, 32=>30, 31=>30, 30=>30,
		29=>30, 28=>30, 27=>30, 26=>30, 25=>30, 24=>30, 23=>29, 22=>28, 21=>27, 
		20=>26, 19=>25, 18=>24, 17=>23, 16=>22, 15=>21, 14=>20, 13=>19, 12=>18, 11=>17,
		10=>16, 9=>15, 8=>15, 7=>15, 6=>15, 5=>15, 4=>14, 3=>14, 2=>10, 1=>7,
		'0.5'=>3
	);

	//特休計算都已預給方式, 未滿六個月&一年者前台會在控管
	function emplyeeAnnualApi ($data = array()) {
		include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
		global $holidayList;
		$result = array();

		$wkYear = (!empty($data['wkYear'])) ? (int)$data['wkYear'] : (int)date('Y');
		//有要寫入DB才執行
		if(!empty($data['insertDB'])) {
		
			$sql = "select a.* from emplyee_annual a inner join emplyee e on a.empID=e.empID where a.year = '".$wkYear."'";
			if(!empty($data['empID'])) $sql .= " and a.empID = '".$data['empID']."'";
			if(!empty($data['depID'])) $sql .= " and e.depID like '".$data['depID']."%'";
			$rs = db_query($sql);
			$annualList = array();
			while($r=db_fetch_array($rs)){
				$annualList[$r['empID']] = $r;
			}

			/*print_r($annualList);
			exit;*/
		}

		/*取得 到去年年底的留停*/
		$stopData = array('wkDayE'=>($wkYear-1).'-12-31');
		if(!empty($data['empID'])) $stopData['empID'] = $data['empID'];
		if(!empty($data['depID'])) $stopData['depID'] = $data['depID'];
		$stopPreYear = calculateStopJop($stopData);

		/*取得 到傳入年的留停*/
		$stopData['wkDayE'] = $wkYear.'-12-31';
		$stopWkYear = calculateStopJop($stopData);

		/*取得 工時比例*/
		//$ratioData = array('wkDayB'=>($wkYear-1).'-01-01','wkDayE'=>($wkYear-1).'-12-31');
		$ratioData = array('wkYear'=>$wkYear);
		if(!empty($data['empID'])) $ratioData['empID'] = $data['empID'];
		if(!empty($data['depID'])) $ratioData['depID'] = $data['depID'];
		$ratioList = getRatio($ratioData);

		/*取得去年的特休狀況*/
		//$lYStatus = getLastYear(array('wkYear'=>$wkYear));

		/*取得去年剩餘時數*/
		$lastData = array('year'=>$wkYear-1);
		if(!empty($data['empID'])) $lastData['empID'] = $data['empID'];
		$lastList = getLastHours($lastData);

		$sql = "select * from `emplyee` where `isOnduty` IN (1,2) and hireDate is not null and hireDate <> '' ";
		if(!empty($data['empID'])) $sql .=" and empID='".$data['empID']."'";
		if(!empty($data['depID'])) $sql .=" and depID like '".$data['depID']."%'";

		$rs = db_query($sql);
		while($r=db_fetch_array($rs)){
			$result[$r['empID']] = array(
				'year' => $wkYear,
				'empID' => $r['empID'],
				'hireDate' => $r['hireDate'],
				'allDay'=>0,
				'stopDay'=>0,
				'actDays'=>0, //會計年資
				'allDayWk'=>0,
				'stopDayWk'=>0,
				'actDaysWk'=>0, //計算到傳入年年底的年資
				'ratio'=>0, //工時比例
				'yearPoint'=>0,
				'days' => 0,
				'hours'=> 0, //特休時數(總時數)
				'preDate' =>'', // 特休天數 是到哪一天滿N年
				'sameLastYear' => 0, //判斷是否跟計算年前一年的所屬年資一樣 如果一樣代表有留停造成計算年無特休
				'leaveDate'=>$r['leaveDate'],
				'leapCount'=>0, //到會計年資的閏年天數
				'leapCountWk'=>0, //計算到傳入年閏年天數
				'lastHours' => 0, //去年剩餘可延休時數
				'lastTotalHours' => 0,//去年剩餘全部時數
				'isUseLast' =>0, //是否要加總去年剩餘時數
				'nHours' => 0, //今年特休時數(未加去年延休)
			);
			//計算會計年資
			$bDate = strtotime($r['hireDate']);
			$eDate = strtotime($wkYear.'-01-01');
			if($bDate != '') {
				$result[$r['empID']]['actDays'] = ($eDate - $bDate) / (24*60*60); //bDate 為1/1 不用再加一天
				$result[$r['empID']]['allDay'] = $result[$r['empID']]['actDays'];
				//扣掉留停
				if(!empty($stopPreYear[$r['empID']]['stopDay'])) {
					$result[$r['empID']]['actDays'] -= $stopPreYear[$r['empID']]['stopDay'];
					$result[$r['empID']]['stopDay'] = $stopPreYear[$r['empID']]['stopDay'];
					$result[$r['empID']]['stopRange'] = $stopPreYear[$r['empID']]['stopRange'];	
				}

				//計算閏年(因為設定除365天計算年 所以要把閏年的天數扣掉)
				$result[$r['empID']]['leapCount'] = getLeapYearCount(date('Y', $bDate), ($wkYear-1));
				$result[$r['empID']]['actDays'] -= $result[$r['empID']]['leapCount'];
			}

			//計算到傳入年年底的年資 (預給特休需要)
			$eDate = (date('Y-m-d',strtotime($r['leaveDate'])) == '1970-01-01') ? strtotime($wkYear.'-12-31') : strtotime($r['leaveDate']);
			//$eDate = strtotime($wkYear.'-12-31');

			if($bDate != '' && $bDate < $eDate) {
				$result[$r['empID']]['actDaysWk'] = ($eDate - $bDate) / (24*60*60) + 1; 
				$result[$r['empID']]['allDayWk'] = $result[$r['empID']]['actDaysWk'];
				//扣掉留停
				if(!empty($stopWkYear[$r['empID']]['stopDay'])) {
					$result[$r['empID']]['actDaysWk'] -= $stopWkYear[$r['empID']]['stopDay'];
					$result[$r['empID']]['stopDayWk'] = $stopWkYear[$r['empID']]['stopDay'];
					$result[$r['empID']]['stopRangeWk'] = $stopWkYear[$r['empID']]['stopRange'];
				}

				//計算閏年(因為設定除365天計算年 所以要把閏年的天數扣掉)
				$result[$r['empID']]['leapCountWk'] = getLeapYearCount(date('Y', $bDate), date('Y', $eDate));
				$result[$r['empID']]['actDaysWk'] -= $result[$r['empID']]['leapCountWk'];
			}
			
			//塞入工時比例
			if(isset($ratioList[$r['empID']])) $result[$r['empID']]['ratio'] = $ratioList[$r['empID']];

			//計算特休
			$actYearWk = $result[$r['empID']]['actDaysWk']/365;
			foreach ($holidayList as $k => $v) {
				if($actYearWk > (float)$k) {
					//特休天數 是到哪一天滿N年 = 計算年的前一年底會計日 + (N年天數-計算年的前一年底會計年資天數)
					$result[$r['empID']]['preDate'] = ceil($k*365) - $result[$r['empID']]['actDays'];
					$result[$r['empID']]['preDate'] = date('Y-m-d',strtotime(($wkYear-1).'-12-31 +'.$result[$r['empID']]['preDate'].' day'));

					$result[$r['empID']]['yearPoint'] = $k;
					$result[$r['empID']]['days'] = $v;

					//當年度為1年的時候 要在判斷當年度是否有在滿 六個月
					if($k === 1 && $result[$r['empID']]['actDays'] < 180) {
						$result[$r['empID']]['yearPoint'] = '0.5&'.$k;
						$result[$r['empID']]['days'] += 3;
					}

					/*判斷是否跟前一年年資一樣 如果一樣代表有留停造成計算年特休要為0*/
					$actDays = $result[$r['empID']]['actDays']/365;
					if($actDays > (float)$k) {
						//特休0條件 小於25年 或者 等於25年後且到年底的年資為滿26年以上 
						if(($k == 25 && floor($actDays) == floor($actYearWk)) || $k < 25) {
							$result[$r['empID']]['sameLastYear'] = 1;
							break;
						} 
					}

					/*判斷是否跟前一年年資一樣 如果一樣代表有留停造成計算年特休要為0*/
					/*if(!empty($lYStatus[$r['empID']]) && $lYStatus[$r['empID']]['userYear'] == $result[$r['empID']]['yearPoint']) {
						break;
					}*/

					/*實際特休時數 = (特休天數 * (工時比工作天數 /(計算年的前一年總工作天數+到計算年滿N年天數)) * (8*工時比例))+.... */
					//$endDay 要拿來扣天數 一開始先加1天 才會剛好
					$endDay = date('Y-m-d',strtotime($result[$r['empID']]['preDate'].' +1 day'));
					//$startDay = (strtotime($r['hireDate']) > strtotime(($wkYear-1).'-01-01')) ? $r['hireDate'] : ($wkYear-1).'-01-01';
					$startDay = $r['hireDate'];

					//計算(計算年的前一年總工作天數+到計算年滿N年天數)
					$result[$r['empID']]['preDay'] = ((strtotime($endDay) - strtotime($startDay))/3600/24);

					//最多只需要取往前一年
					if($result[$r['empID']]['preDay'] > 365) {
						$result[$r['empID']]['preDay'] = 365;
						$startDay = date('Y-m-d',strtotime($endDay.' -365 day'));
					}


					/*echo "<pre>";
					print_r($startDay.'<br>');
					print_r($wkYear.'<br>');
					print_r($endDay.'<br>');
					print_r($result);*/

					foreach ($result[$r['empID']]['ratio'] as $k2 => $v2) {
						//工時比例日期 大於 滿N年的日期 不需要計算 continue
						if(strtotime($k2) > strtotime($endDay)) continue;

						//if($v2 > 1) $v2 = 1; //工時比例最高為1 (工時比例，原本設定最高為1，但因為霧峰夜班有10/8的工時，所以，要取消工時比例最高為1的設定。以實際工時比例核定特休假時)
						//$sDay = (strtotime($k2) < strtotime(($wkYear-1).'-01-01')) ? ($wkYear-1).'-01-01' : $k2;
						$sDay = (strtotime($k2) < strtotime($startDay)) ? $startDay : $k2;
						
						$wDay = ((strtotime($endDay) - strtotime($sDay))/3600/24);
						//print_r('endDay=> '.$endDay.'_sDay=>'.$sDay.' <br>');
						$result[$r['empID']]['hours'] += ($result[$r['empID']]['days'] * ($wDay/$result[$r['empID']]['preDay']) * (8*$v2));
						/*print_r('wDay=> '.$wDay.'<br>');
						print_r('hours=> '.$result[$r['empID']]['hours'].'<br>');*/

						if(strtotime($k2) <= strtotime($startDay)) { //各分段時數已經算完
							break;
						}
						$endDay = $k2;
	
					}

					/*echo "<pre>";
					print_r($wkYear.'<br>');
					print_r($endDay.'<br>');
					print_r($result);
					exit;*/

			
					$result[$r['empID']]['hours'] = round($result[$r['empID']]['hours'], 1);

					$hList = explode('.', $result[$r['empID']]['hours']);
					if(count($hList) == 2) {
						if((int)$hList[1] > 5) $result[$r['empID']]['hours'] = (int)$hList[0] + 1;
						else $result[$r['empID']]['hours'] = (int)$hList[0] + 0.5;
					}

					/*echo "<pre>";
					print_r($result);
					exit;*/

					break;
				}
			}

			//塞入今年計算完的時數
			$result[$r['empID']]['nHours'] = $result[$r['empID']]['hours'];


			//塞入去年剩餘時數
			if(isset($lastList[$r['empID']])) {
				$result[$r['empID']]['lastHours'] = $lastList[$r['empID']]['lastHours'];
				$result[$r['empID']]['lastTotalHours'] = $lastList[$r['empID']]['lastTotalHours'];
				$result[$r['empID']]['isUseLast'] = $lastList[$r['empID']]['isUseLast'];
			}

			//判斷是否使用去年剩餘時數
			if($result[$r['empID']]['isUseLast'] == 1) {
				$result[$r['empID']]['hours']+=$result[$r['empID']]['lastHours'];
			}

			//有要寫入DB才執行 寫入資料庫
			if(!empty($data['insertDB'])) {
				$fields = array(
					"actDays='".$result[$r['empID']]['actDays']."'",
					"cHours='".$result[$r['empID']]['hours']."'",
					"userYear='".$result[$r['empID']]['yearPoint']."'",
					"lastHours='".$result[$r['empID']]['lastHours']."'",
					"lastTotalHours='".$result[$r['empID']]['lastTotalHours']."'"
				);
				if(isset($annualList[$r['empID']]) && $annualList[$r['empID']]['isClose'] == 0) { //update
					if($annualList[$r['empID']]['isHand'] == 0 || empty($data['noRecalculate'])) {
						$fields[] = "isHand='0'";
						$fields[] = "hours='".$result[$r['empID']]['hours']."'";
						$fields[] = "nHours='".$result[$r['empID']]['nHours']."'";
					} 
					$sql = "update emplyee_annual set ".implode(',', $fields)." where empID='".$r['empID']."' and year = '".$wkYear."'";
				} else { //insert
					$fields[] = "isHand='0'";
					$fields[] = "hours='".$result[$r['empID']]['hours']."'";
					$fields[] = "nHours='".$result[$r['empID']]['nHours']."'";
					$fields[] = "year='".$wkYear."'";
					$fields[] = "empID='".$r['empID']."'";
					$sql = "insert into emplyee_annual set ".implode(',', $fields);
				}
				db_query($sql);
			}

		}
		return $result;
	}

	//計算留停時間
	function calculateStopJop($data = array()) {
		$result = array();
		
		$wkDayE = strtotime($data['wkDayE']);
		//留職停薪或是延長留停
		$sql = "select s.* from emplyee_status s inner join emplyee e on s.empID = e.empID where s.jobStatus IN (3,4) and s.bdate <= '".$data['wkDayE']."' and s.bdate >= e.hireDate and e.isOnduty IN (1,2) ";
		if(!empty($data['empID'])) $sql .=" and s.empID='".$data['empID']."' ";
		if(!empty($data['depID'])) $sql .=" and e.depID like '".$data['depID']."%' ";
		$sql.= "order by s.empID ASC, s.bdate ASC";
		$rs = db_query($sql);

		while($r=db_fetch_array($rs)){
			if(!isset($result[$r['empID']])) $result[$r['empID']]['stopDay'] = 0;
			$bDate = strtotime($r['bdate']);
			$eDate = strtotime($r['edate']);
			if($bDate != '' && $eDate != '' && $bDate <= $eDate) {
				if($bDate <= $wkDayE && $eDate <= $wkDayE){ //正常範圍
					$result[$r['empID']]['stopDay'] += ($eDate - $bDate) / (24*60*60) + 1;
					$result[$r['empID']]['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				} elseif($bDate <= $wkDayE && $eDate > $wkDayE) { //結束時間大於作業年最後一天
					$result[$r['empID']]['stopDay'] += ($wkDayE - $bDate) / (24*60*60) + 1;
					$result[$r['empID']]['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				} 
			}
			
		}
		
		return $result;
	}

	//取得工時比例
	function getRatio($data = array()) {
		$result = array();

		$sql = "select s.empID, s.changeDate, s.salaryType, s.ratio from emplyee_salary s inner join emplyee e on s.empID = e.empID where e.isOnduty IN (1,2) and s.changeDate >= e.hireDate and s.changeDate <='".$data['wkYear']."-12-31'";

		if(!empty($data['empID'])) $sql .=" and s.empID='".$data['empID']."'";
		if(!empty($data['depID'])) $sql .=" and e.depID like '".$data['depID']."%'";
		$sql.= " order by s.empID asc, s.changeDate desc";
		$rs = db_query($sql);
	
		$isStop = 0;
		while($r=db_fetch_array($rs)){
			if(!isset($result[$r['empID']])) $isStop = 0;
			if($isStop == 1) continue;
			if(strtotime($r['changeDate']) < strtotime(($data['wkYear']-1).'-01-01')) {
				$isStop = 1;
			}
			$result[$r['empID']][$r['changeDate']] = $r['ratio'];
		}

		return $result;
	}

	//取得去年的特休狀況
	function getLastYear($data = array()) {
		$result = array();
		$sql = "select * from emplyee_annual where year='".($data['wkYear']-1)."'";
		$rs = db_query($sql);
		while ($r = db2_fetch_array($rs)) {
			$result[$r['empID']] = $r;
		}
		return $result;
	}

	//取得閏年次數
	function getLeapYearCount($bYear, $eYear) {
		$result = 0;
		for($i=$bYear; $i<=$eYear; $i++) {
			if(checkdate(2, 29, $i)) $result++;
		}
		return $result;
	}

	//取得特休剩餘實數
	function getLastHours($data = array()) {
		$result = array();
		$sql = "select empID,hours, isDelay as isUseLast, lastHours, nHours from emplyee_annual where `year`=".$data['year'];
		if(!empty($data['empID'])) $sql .=" and empID='".$data['empID']."'";
		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {
			$result[$r['empID']] = $r;
			$result[$r['empID']]['lastTotalHours'] = $r['hours'];
			$result[$r['empID']]['lastHours'] = $r['nHours'];
		}

		$sql = "select empID,sum(hours) as leaveHours from absence where ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and aType='特休' and YEAR(bDate) = ".$data['year'];

		if(!empty($data['empID'])) $sql .=" and empID='".$data['empID']."'";
		$sql .= " group by empID ";

		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {
			$result[$r['empID']]['leaveHours'] = $r['leaveHours'];
			$result[$r['empID']]['lastTotalHours'] = $result[$r['empID']]['hours'] - $r['leaveHours'];
			//計算延休時數 延休時數不得跨兩年(含)以上
			$result[$r['empID']]['lastHours'] = ($result[$r['empID']]['lastTotalHours'] <= $result[$r['empID']]['nHours']) ? $result[$r['empID']]['lastTotalHours'] : $result[$r['empID']]['nHours'];
		}
		
		return $result;

	}
?>