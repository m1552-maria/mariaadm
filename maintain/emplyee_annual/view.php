<?
	/*20180105 sean add*/
	include_once("emplyeeAnnualApi.php");
	$data = array('wkDayE'=>date('Y-m-d'));
	$stopJopList =  calculateStopJop($data);
?>
<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?></title>
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link href="tuning.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script type="text/javascript" src="tuning.js" ></script> 
 <style type="text/css">
	.popupPane {
	    position: absolute;
	    left: 25%;
	    top: 5%;
	    background-color: #888;
	    padding: 8px;
	    display: none;
	}

	#d_setting {
	    display: none;
	    overflow: auto;
	    height: 150px;
	    width: 400px;
	    position: absolute;
	    top: 80px;
	    left: 120px;
	}

	.tip {
	    color: #900;
	}

	.divTitle {
	    color: #fff;
	    background-color: #900;
	    border: 0px;
	    padding: 8px;
	    font-family: '微軟正黑體';
	    font-size: 14px;
	    width: 364px;
	    margin-top: 10px;
	    margin-left: 10px;
	}

	.divBorder1 {
	    /* 內容區 第一層外框 */
	    background-color: #dedede;
	    border: 1px solid #666;
	    padding: 0px;
	    margin: 0px;
	    font-size: 16px;
	}

	.divBorder2 {
	    /* 內容區 第二層外框 */
	    padding: 8px;
	    background: #FFFFFF;
	    margin: 0px 10px 0px 10px;
	    height: 50px;
	    overflow-x: hidden;
	    overflow-y: auto;
	}

	#d_bottom {
	    padding-top: 8px;
	}

	table.grid {
	    margin: 0px;
	    border: 1px solid #ccc;
	    border-collapse: collapse;
	    width: 100%;
	}

	table.grid td {
	    border: 1px solid #999;
	    padding: 5px;
	    font-size: 12px;
	    color: #000;
	    background: #fff;
	    text-align: center;
	}
	.input_large{
		padding: 5px;
    	margin: 5px;
	}
	</style>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var typflt = $('select#aType option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var mtflt = $('select#month option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(typflt) url = url+'&aType='+typflt;
		if(yrflt) url = url+'&year='+yrflt;
		if(mtflt) url = url+'&month='+mtflt;
		location.href = url;		
	}

	$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<? if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[empID]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
		
		
	});	
	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}

	function detail(k){
		$('#d_setting').css({'display':'none'});
		$('#d_setting').css({'display':'block'});
		$('[name="empID"]').val(k);
	}


	
	function seniority(now){
		$.ajax({
		 // url: "../emplyee_annual/api.php",
		  url: "../emplyee_annual/apiAjax.php",
		  type: "POST",
		  dataType:"text",
		  data: {depID:$('#depFilter').val(),wkYear:$('#year').val()},
		  success: function(d){
		  	alert('已經完成');
			$('.msg').html('已經完成');
			goPage();
		  }
		});
	}
 

 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
			foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>

    
    <select name="year" id="year" onChange="doSubmit()"><? foreach ($yearList as $i => $value) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    <input type="button" <?php if($nowYr > $curYr) echo 'disabled';?> value="試算年度特休" onclick="seniority(this)">
    <!--<input type="button" name="toExcel" value="匯出特休時數" onclick="annual.submit()">-->
    
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?
			if($ftAry[$i]=='年度特休'){
				$ftAry[$i] = $curYr.$ftAry[$i];
			}elseif($ftAry[$i]=='會計年資'){
				$showyear = $curYr - 1;
				$ftAry[$i] =$ftAry[$i]."(算到$showyear/12/31)";
			}
		?>
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)) {
		$sql = "select a.year,a.hours,a.lastHours,a.nHours,a.isDelay,a.isHand,a.actDays,a.isClose,b.* from $tableName on $defWh where $filter";
	} else {
		$sql = "select a.year,a.hours,a.lastHours,a.nHours,a.isDelay,a.isHand,a.actDays,a.isClose,b.* from $tableName on $defWh where 1";
	}
	// 搜尋處理
	if ($keyword!="") {
		$sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%'))";
	}
	$outsql = $sql;
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	 //echo $sql; //exit;
  $rs = db_query($sql,$conn);

	while ($r=db_fetch_array($rs)) {$id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><?=$id?></td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:i',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						case "empID" : echo $emplyeeinfo[$fldValue]; break;
						default : 
							if ($fnAry[$i]=="actDays"){	
								if($fldValue!=''){
									echo stohis($fldValue*24*60*60);//會計年資	
								}
							}elseif($fnAry[$i]=="trueDays"){
								if($r['isOnduty'] != 1 && $r['isOnduty'] != 2) {
									echo "離職";
									break;
								}
								$now_date = date('Y-m-d');
								$s = (strtotime($now_date) - strtotime($r['hireDate'])+24*60*60);//會相差一天
								/*20180105 sean edit*/
								if(!empty($stopJopList[$r['empID']]['stopDay'])) {
									$s = $s - ($stopJopList[$r['empID']]['stopDay']*24*60*60);
								}
								echo stohis($s);

								/*if(isset($job_status[$r['empID']])){
									//20171212 sean 修正日期計算(換算成秒) 
									//$s = $s - $job_status[$r['empID']];
									$s = $s - ($job_status[$r['empID']]*24*60*60);
								}
								echo stohis($s);*/
							}elseif($fnAry[$i]=="spHolidays"){//今年度特休
								if($r['nHours']!='' && $r['year'] == date('Y') && $r['isClose'] == 0){

									echo '<input style="width: 50px;" name="changeHours['.$r['empID'].']" type="text" value="'.$r['nHours'].'">'.' /小時 <input style="cursor: pointer;" type="button" value="確定" onClick="changeHourd(this,'."'".$r['empID']."'".','."'".$r['empName']."'".')"> <span data-span="'.$r['empID'].'">' .(($r['isHand']) ? '(手動更新)':'').'</span>';
								} else echo $r['nHours'];
							}elseif ($fnAry[$i] == 'depID') {
								echo $departmentinfo[$fldValue];
							}else if($fnAry[$i]=="action"){
								echo '<input type=\'button\' value=\'調整此年度特休\' onclick=\'detail('.$r['empID'].')\'/>';
							}else if($fnAry[$i]=="hours"){
								if($r['hours'] > $r['nHours']) $fldValue.=" (加總去年延休)";
									echo $fldValue;
							}else if($fnAry[$i]=="isDelay"){
								/*echo "<pre>";
								print_r($r);
								exit;*/
								if($r['isClose'] == 0) {
									$checked = ($r['isDelay']) ? ' checked ' : '';
									//$disabled = ($r['year'] != date('Y')) ? ' disabled ' : '';
									echo '<input onChange="delay('."'".$r['empID']."'".',this)" type="checkbox" value="1" '.$checked.$disabled.'>';
								} else {
									if($fldValue == 1) echo "是";
									
								}		
							}else{	echo $fldValue;	}
							break;
			    } 
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?> 

<?
	function stohis($s){
		$year = intval($s/31536000);
		$month = intval(($s - ($year*31536000))/2592000);
		$day = intval(($s - ($year*31536000)-($month*2592000))/86400);
		return $year.'年'.$month.'月'.$day.'日';
	}
?> 
 </table>
</form>

<div id="d_setting" class='divBorder1'>
	<form id="form2" method="post" action="seniority.php" onSubmit="return Form1_Validator(this);" enctype="multipart/form-data" style="margin:0">
		<div class='divTitle'>設定此年度特休</div>
		<div class='divBorder2'>
			<div>新的特休時數:<input type="number" min="0" name="hours" class="input_large"></div>
			<input type="hidden" name="empID" value="">
			<input type="hidden" name="year" value="<?=$curYr?>">
		</div>
		<div id='d_bottom' style='text-align:center'><input type="button" value="送出" onclick="form2.submit();"></div>
	</form>
</div>

<form name="annual" action="excel.php" method="post" target="excel">
	<input type="hidden" name="fmTitle" value="員工特休時數管理">
	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
	<input type="hidden" name="year" value="<?=$_REQUEST['year']?>">
	<input type="hidden" name="keyword" value="<?=$keyword?>">
	<input type="hidden" name="sql" value="<?=$outsql?>">
</form>

<script type="text/javascript">
	function changeHourd(obj ,empID, empName) {
		if(!confirm('確定更改'+empName+'的特休時數?')) return false;
		var hCheck = /^[0-9]+(.[0-9]{1})?$/;
		var hours = $('[name="changeHours['+empID+']"]').val();
		if(!hCheck.test(hours)) {
			alert('請輸入正確數字');
			return false;
		}
		$.ajax({
			type:'POST',
			url:'changeHourd.php',
			dataType:'json',
			data:{year:$('#year').val(),empID:empID,hours:hours},
			success:function(){
				location.reload();
				//$('[data-span="'+empID+'"]').html('(手動更新)');
			}
		});
	}

	function delay(empID, obj) {
		if(!confirm(empID+'確定申請或者取消延休?')) {
			$(obj).prop('checked',!$(obj).prop('checked'));
			return false;
		}
		var isDelay = ($(obj).prop('checked')) ? 1 : 0;
		$.ajax({
			type:'POST',
			url:'delay.php',
			dataType:'json',
			data:{year:$('#year').val(),empID:empID,isDelay:isDelay},
			success:function(){
				//location.reload();
			}
		});
	}
</script>
</body>
</Html>