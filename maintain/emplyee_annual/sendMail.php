<?php

	//放上正式機前需要新增 todo_list資料setUpAnnualMail,sendAnnualMailOne兩個

	//session_start();
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($root."/emplyee/newmsgdo.php");
	include_once($root."/domail.php");
	include_once("emplyeeAnnualApi.php");

	$sendNoRestDate = array(
		array('sendDate'=>date('Y').'-7-10', 'cDate'=>date('Y').'-6-30'),
		array('sendDate'=>date('Y').'-11-10', 'cDate'=>date('Y').'-10-31')
	);

	if(isset($_REQUEST) && count($_REQUEST) > 0) {
		$result = array('result'=>0);
		switch ($_REQUEST['action']) {
			case 'setUpAnnualMail': //每年1/1產生特休寄信紀錄 病寄信
				$result = setUpAnnualMail();
				break;
			case 'setUpAnnualMail_sean': 
				$result = setUpAnnualMail_sean();
				break;
			case 'sendAnnualMailOne': //寄特休信
				$result = sendAnnualMailOne();
				break;
			case 'deleteAll': // 刪除特休信所有紀錄
				$result = deleteAll();
				break;
			case 'annualNoRestMail':
				$result = annualNoRestMail($_REQUEST['sendDate'], $_REQUEST['cDate']);
				break;
		}

		/*print_r($result);
		exit;*/
		return $result;
	}

	

	//建立要寄送特修的通知信紀錄
	function setUpAnnualMail($dataList = array()) {
		global $sendNoRestDate;
		$result = array('result'=>1);
		
		if(count($dataList) > 0) {
			foreach ($dataList as $key => $value) {
				$_REQUEST[$key] = $value;
			}
		}

		$ymd = (!empty($_REQUEST['ymd'])) ? $_REQUEST['ymd'] : date('Y-m-d');
		$y = date('Y', strtotime($ymd));
		$md = date('m-d', strtotime($ymd));
		//$ymd = date('Y-m-d');
		if($md != '01-01') {
			$result['msg'] = '日期錯誤';
			return $result;
		}

		/*if(empty($_REQUEST['isInsert'])) {
			$sql = "select 1 from schedule_send_mail where `type`='annual' and `sendDate`='".$ymd."'";

			$rs = db_query($sql);
			if(db_num_rows($rs) > 0) {
				$result['msg'] = '已經存在';
				return $result;
			}
		}*/


		$sqlD = "delete from schedule_send_mail where year='".$y."' and isSend <> '1'";

		/*重新試算特修*/
		$eAData = array('wkYear'=>$y, 'insertDB'=>1, 'noRecalculate'=>1);
		$sql = "select a.*, e.leaveDate, e.hireDate from emplyee_annual a inner join emplyee e on a.empID = e.empID where a.`hours` > 0 and a.`year`='".$y."' and (e.isOnduty=1 OR e.isOnduty=2) ";
		if(!empty($_REQUEST['empID'])) {
			$eAData['empID'] = $_REQUEST['empID'];
			$sql.= " and e.empID='". $_REQUEST['empID']."'";
			$sqlD.= " and empID='". $_REQUEST['empID']."'";
		}
		db_query($sqlD);

		if(empty($_REQUEST['isHand'])) $eA = emplyeeAnnualApi($eAData);

		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {

			//排除離職日為12月的
			if(strtotime($r['leaveDate']) >= strtotime(((int)$y - 1).'-12-01') && strtotime($r['leaveDate']) <= strtotime(((int)$y - 1).'-12-31')) continue;



			$fields = array(
				"`year`='".$y."'",
				"`type`='annual'",
				"`empID`='".$r['empID']."'",
				"`cDate`='".date('Y-m-d H:i:s')."'"
			);
			switch ($r['userYear']) {
				case '0.5':
				case '0.5&1':
				case '1':
					$data = array('wkDayE'=>$y.'-12-31','empID'=>$r['empID']);
					$stopJop =  calculateStopJop($data);
					$addDay = (!empty($stopJop[$r['empID']]['stopDay'])) ? $stopJop[$r['empID']]['stopDay'] : 0;
					$addDay += ($r['userYear'] == '1') ? 365 : 182;
					$fields[] = "`sendDate`='".date('Y-m-d', strtotime("+".$addDay." day",strtotime($r['hireDate'])))."'";
					if($r['userYear'] == '1') {
						$fields[] = "`sendType`='滿一年'";
					} else {
						$fields[] = "`sendType`='滿半年'";
					}
					//同時滿兩個條件 要有兩封信(滿半年寄一次 滿一年寄一次)
					if($r['userYear'] == '0.5&1') {

						$_fields = array(
							"`year`='".$y."'",
							"`type`='annual'",
							"`empID`='".$r['empID']."'",
							"`cDate`='".date('Y-m-d H:i:s')."'",
							"`sendDate`='".date('Y-m-d', strtotime("+".($addDay+183)." day",strtotime($r['hireDate'])))."'",
							"`freeField`='isSecondMail'",
							"`sendType`='滿一年'"
						);
						$sql = "insert into schedule_send_mail set ".implode(',', $_fields);
						db_query($sql);
					}
					break;
				default:
					$fields[] = (empty($_REQUEST['isTest'])) ? "`sendDate`='".$ymd."'" : "`sendDate`='".$y.'-01-01'."'";
					break;
			}
			$sql = "insert into schedule_send_mail set ".implode(',', $fields);
			db_query($sql);
		}

		//將未休完的提醒信 紀錄到todo_list(排程)
		/*foreach ($sendNoRestDate as $key => $value) {
			$fields = array(
				"`title`='".$value['sendDate']."未休完特休提醒'",
				"`action`='未休完特休提醒:sendDate=".$value['sendDate'].",cDate=".$value['cDate']."'",
				"`bDate`='".$value['sendDate']."'",
				"`eDate`='".date('Y-m-d', strtotime('+2 day', strtotime($value['sendDate'])))."'"
			);
			$sql = "insert into todo_list set ".implode(',', $fields);
			db_query($sql);
		}*/


		//$result = sendAnnualMailOne($ymd);


		return $result;
	}

	//寄發信件
	function sendAnnualMailOne($dataList = array()) {
		$result = array('result'=>1);
		global $holidayList;
		global $MailInfo;
		global $CustomHeader;

		if(count($dataList) > 0) {
			foreach ($dataList as $key => $value) {
				$_REQUEST[$key] = $value;
			}
		}

		$date = date('Y-m-d');
		if(!empty($_REQUEST['ymd'])) $date = $_REQUEST['ymd'];

		$y = date('Y',strtotime($date));
		$sD = $y.'-01-01';
		$eD = $date;
		$sql = "select e.email, e.empName, e.hireDate, s.*, a.userYear, a.hours, a.lastHours  from schedule_send_mail s inner join emplyee e on s.empID=e.empID inner join emplyee_annual a on s.empID = a.empID where s.sendDate >= '".$sD."' and s.sendDate <= '".$eD."' and s.isSend = '0' and s.`type`='annual' and a.year = '".$y."'";
		if(!empty($_REQUEST['empID'])) $sql .= " and e.empID='".$_REQUEST['empID']."'";



		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {
			$totalHours = $r['hours'];
			$send = '';
			if($r['email'] == '') continue;
			//如果是1月1日寄出 代表滿一年以上 正常寄送 
			$m = array_merge($MailInfo, array());
			$m["mailTo"][] = array("MailName"=>$r['email'],	"address"=>$r['email']); 
			$m["Subject"] = $y.'特休時數通知';
			$m["Body"] = "<p>親愛的".$r['empName']."您好:</p>";

			$canSend = 1;
			if($r['sendDate'] == $y.'-01-01') {
				$m["Body"] .= ((int)$y-1911)."年您的特休假總時數計".$totalHours."小時。<br> 若您在中途離職或有留職停薪等職務異動情況，將依您的實際年資重新核算您應有之特休假時數，溢休時數改以事假或病假核給，請假應扣薪資將於月薪給付時補扣或以現金收回。<br>";

			} else { //非1月1日 要在計算留停日
				$data = array('wkDayE'=>$y.'-12-31','empID'=>$r['empID']);
				$stopJop =  calculateStopJop($data);
				$addDay = (!empty($stopJop[$r['empID']]['stopDay'])) ? $stopJop[$r['empID']]['stopDay'] : 0;
				$addDay += ($r['userYear'] == '1') ? 365 : 182;
				if($r['freeField'] == 'isSecondMail') $addDay += 183; //isSecondMail => 0.5&1的第二封信(滿一年)

				$sendDate = date('Y-m-d', strtotime("+".$addDay." day",strtotime($r['hireDate'])));
				if($sendDate == $r['sendDate']) { //日期正確
					switch ($r['userYear']) {
						case '0.5':
							$m["Body"] .= '您的工作年資已滿六個月，<br>自'.date('Y/m/d', strtotime($sendDate)).'起特休假3日，時數計'.$totalHours.'小時。';
							break;
						case '1':
							$m["Body"] .= '您的工作年資已滿一年，<br>自'.date('Y/m/d', strtotime($sendDate)).'起特休假7日，時數計'.$totalHours.'小時。';
							break;
						case '0.5&1':
							$spHolidays = $totalHours;
							$sixM = $spHolidays/($holidayList['0.5']+$holidayList['1'])*$holidayList['0.5']; //滿半年可休時數

							if($r['freeField'] == 'isSecondMail') { //滿一年
								$spHolidays = $totalHours - floor($sixM); 
								$m["Body"] .= '您的工作年資已滿一年，<br>自'.date('Y/m/d', strtotime($sendDate)).'起特休假7日，時數計'.$spHolidays.'小時。';
							} else { //滿六個月
								$spHolidays = floor($sixM); 
								$m["Body"] .= '您的工作年資已滿六個月，<br>自'.date('Y/m/d', strtotime($sendDate)).'起特休假3日，時數計'.$spHolidays.'小時。';
							}

							break;
					}

				} else {
					$canSend = 0;
					if(date('Y',strtotime($sendDate)) == $y) { //更改寄信時間
						$sql = "update schedule_send_mail set cDate='".date('Y-m-d H:i:s')."', sendDate='".$sendDate."' where id='".$r['id']."'";
						db_query($sql);
					} else { //超過今年 直接刪掉
						$sql = "delete from schedule_send_mail where id='".$r['id']."'";
						db_query($sql);
					}
				}
			}


			if($canSend == 1) {
				$m["Body"].='<p>祝福您   平安 喜樂</p><br><div style="text-align:right;max-width:800px">財團法人瑪利亞社會福利基金會<br>行政管理室<br>'.date('Y年m月d日').'</div>';
				

				$sendData = array(
					'ClassID'=>0,
					'title'=>'('.$r['empName'].')'.$m["Subject"],
					'Content'=>$m["Body"],
					'unValidDate'=>date('Y-m-d', strtotime("+1 year",strtotime($r['sendDate']))),
					'announcer' => ($r['sender']) ? $r['sender'] : '00225',
					'empID' => $r['empID'],
					'notifies'=>array(1,2)
				);

				$resultSend = sendMessage($sendData);
				if($resultSend) {
					$sql = "update schedule_send_mail set cDate='".date('Y-m-d H:i:s')."', isSend='1' where id='".$r['id']."'";
					db_query($sql);
				}
				
				//$send = SMTP_Send_Mail($m, $CustomHeader);
			}

		
			/*if($send == 'Mail Send success') {
				$sql = "update schedule_send_mail set cDate='".date('Y-m-d H:i:s')."', isSend='1' where id='".$r['id']."'";
				db_query($sql);
			}*/


		}

		return $result;
	}

	/*未休完特休提醒*/
	function annualNoRestMail($sendDate = '', $cDate = '') {
		if($sendDate == '') $sendDate = date('Y-m-d');
		if($cDate == '') $cDate = date('Y-m-d');

		$result = array('result'=>0);
		global $MailInfo;
		global $CustomHeader;

		/*if($sendDate != date('Y-m-d')) {
			$result['msg'] = '日期錯誤';
			exit;
		}*/

		$y = date('Y');
		
		$preMonth = date('Y-m', strtotime('-1 month', strtotime($sendDate))).'-01';
		$lastDate = date('Y-m-t');

		$sql = "select a.*,e.email, e.empName, e.leaveDate from emplyee_annual a inner join emplyee e on a.empID = e.empID where a.`year`='".$y."' and a.`hours`+a.`lastHours` > 0";
		if($_REQUEST['empID']) $sql.=" and e.empID='".$_REQUEST['empID']."'";
		$rs = db_query($sql);

		while ($r = db_fetch_array($rs)) {
			//排除離職日為寄送日當月與前一個月的
			if(strtotime($r['leaveDate']) >= strtotime($preMonth) && strtotime($r['leaveDate']) <= strtotime($lastDate)) continue;

			$sql = "select ifnull(sum(hours), 0) as thours from `absence` where empID ='".$r['empID']."' and aType='特休' and ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and YEAR(bDate)=YEAR(NOW()) and bDate <= '".$cDate."'";
			$rs2 = db_query($sql);
			$annual = db_fetch_array($rs2);
			$lHours = $r['hours']+$r['lastHours']-$annual['thours'];
			if($lHours > 0) {
				$fields = array(
					"`year`='".$y."'",
					"`type`='annualNoRest'",
					"`empID`='".$r['empID']."'",
					"`cDate`='".date('Y-m-d H:i:s')."'",
					"`sendDate`='".$sendDate."'"
				);
				$sql = "insert into schedule_send_mail set ".implode(',', $fields);
				db_query($sql);
				$newID = mysql_insert_id();

				$m = array_merge($MailInfo, array());
				$m["mailTo"][] = array("MailName"=>$r['email'],	"address"=>$r['email']); 
				$m["Subject"] = $y.'未休完特休提醒';
				$m["Body"] = '<h3>感謝您在工作上的努力</h3>';
				$m["Body"] .= "<p>親愛的".$r['empName']."您好:</p>
				您今年度特休假時數共".($r['hours']+$r['lastHours'])."小時，結至".((int)$y-1911).'/'.date('m/d',strtotime($cDate))."止，已休時數".$annual['thours']."小時，尚有".$lHours."小時特休假未休，提醒您記得排休，也別忘了排休時與您的主管共同協商，讓您所屬單位的工作運作得以順利進行。感謝您在工作上的努力。<br>";
				$m["Body"].='<p>祝福您   平安 喜樂</p><br><div style="text-align:right;max-width:800px">財團法人瑪利亞社會福利基金會<br>行政管理室<br>'.date('Y年m月d日').'</div>';

				$send = SMTP_Send_Mail($m, $CustomHeader);

				if($send == 'Mail Send success') {
					$sql = "update schedule_send_mail set cDate='".date('Y-m-d H:i:s')."', isSend='1' where id='".$newID."'";
					db_query($sql);
				}
			}
		}

		$result['result'] = 1;
		return $result;

	}

	function deleteAll() {
		$sql = "delete from schedule_send_mail where `type` like 'annual%'";
		db_query($sql);
	}


	//2021判斷異動 建立信件
	function setUpAnnualMail_sean($dataList = array()) {
		global $sendNoRestDate;
		$result = array('result'=>1);
		
		if(count($dataList) > 0) {
			foreach ($dataList as $key => $value) {
				$_REQUEST[$key] = $value;
			}
		}

		$ymd = (!empty($_REQUEST['ymd'])) ? $_REQUEST['ymd'] : date('Y-m-d');
		$y = date('Y', strtotime($ymd));
		$md = date('m-d', strtotime($ymd));

		$sql = "select empID, hours from emplyee_annual where year='".$y."'";
		$rs = db_query($sql);
		$oldE = array();
		while ($r = db_fetch_array($rs)) {
			$oldE[$r['empID']] = $r['hours'];
		}

		//$sqlD = "delete from schedule_send_mail where year='".$y."' and isSend <> '1'";

		/*重新試算特修*/
		$eAData = array('wkYear'=>$y, 'insertDB'=>1, 'noRecalculate'=>1);
		$sql = "select a.*, e.leaveDate, e.hireDate from emplyee_annual a inner join emplyee e on a.empID = e.empID where a.`hours` > 0 and a.`year`='".$y."' and (e.isOnduty=1 OR e.isOnduty=2) ";
		if(!empty($_REQUEST['empID'])) {
			$eAData['empID'] = $_REQUEST['empID'];
			$sql.= " and e.empID='". $_REQUEST['empID']."'";
			//$sqlD.= " and empID='". $_REQUEST['empID']."'";
		}
		//db_query($sqlD);

		if(empty($_REQUEST['isHand'])) $eA = emplyeeAnnualApi($eAData);

		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {
			if($r['hours'] == $oldE[$r['empID']]) {
				continue;
			}

			//排除離職日為12月的
			if(strtotime($r['leaveDate']) >= strtotime(((int)$y - 1).'-12-01') && strtotime($r['leaveDate']) <= strtotime(((int)$y - 1).'-12-31')) continue;



			$fields = array(
				"`year`='".$y."'",
				"`type`='annual'",
				"`empID`='".$r['empID']."'",
				"`cDate`='".date('Y-m-d H:i:s')."'",
				"`sendType`='修正後信件'"
			);
			switch ($r['userYear']) {
				case '0.5':
				case '0.5&1':
				case '1':
					$data = array('wkDayE'=>$y.'-12-31','empID'=>$r['empID']);
					$stopJop =  calculateStopJop($data);
					$addDay = (!empty($stopJop[$r['empID']]['stopDay'])) ? $stopJop[$r['empID']]['stopDay'] : 0;
					$addDay += ($r['userYear'] == '1') ? 365 : 182;
					$fields[] = "`sendDate`='".date('Y-m-d', strtotime("+".$addDay." day",strtotime($r['hireDate'])))."'";
					if($r['userYear'] == '1') {
						$fields[4] = "`sendType`='修正後信件,滿一年'";
					} else {
						$fields[4] = "`sendType`='修正後信件,滿半年'";
					}
					//同時滿兩個條件 要有兩封信(滿半年寄一次 滿一年寄一次)
					if($r['userYear'] == '0.5&1') {

						$_fields = array(
							"`year`='".$y."'",
							"`type`='annual'",
							"`empID`='".$r['empID']."'",
							"`cDate`='".date('Y-m-d H:i:s')."'",
							"`sendDate`='".date('Y-m-d', strtotime("+".($addDay+183)." day",strtotime($r['hireDate'])))."'",
							"`freeField`='isSecondMail'",
							"`sendType`='修正後信件,滿一年'"
						);
						$sql = "insert into schedule_send_mail set ".implode(',', $_fields);
						db_query($sql);
					}
					break;
				default:
					$fields[] = (empty($_REQUEST['isTest'])) ? "`sendDate`='".$ymd."'" : "`sendDate`='".$y.'-01-01'."'";
					break;
			}
			print_r($r['empID'].'  new:'.$r['hours'].'  , old:'.$oldE[$r['empID']].'<br>');
			$sqlD = "delete from schedule_send_mail where year='".$y."' and isSend <> '1' and empID ='".$r['empID']."'";
			db_query($sqlD);
			$sql = "insert into schedule_send_mail set ".implode(',', $fields);
			db_query($sql);
		}



		return $result;
	}

?>
