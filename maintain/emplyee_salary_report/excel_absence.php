<?
  header("Content-Type:text/html; charset=utf-8");
   
  include "../../config.php";
  include "../../getEmplyeeInfo.php";
  include "func.php";
  
  $md5Name=$_REQUEST['md5Name'];
  $wkMonth=$_REQUEST['wkMonth'];
  $empID=$_REQUEST["empID"];
  $year=substr($wkMonth,0,4);
  $month=substr($wkMonth, 4,2);
  $depID=$_REQUEST[dep];
  $depName=$departmentinfo[$depID];
  $detail=$_REQUEST['detail'];

  $showZero = $_REQUEST['showZero'];

  $path = "../../data/reports";
  if(!is_dir($path))  mkdir($path);
  $path .= '/salary';
  if(!is_dir($path))  mkdir($path);
  $txtName=$path."/".$wkMonth.'-absence.txt';
  list($md5Name,$jo)=getMD5($depID,$empID,$wkMonth,$txtName);

  $cache_path="../../data/caches";
  if(!is_dir($cache_path)) mkdir($cache_path);
  $cache_file_name=$cache_path."/".$md5Name.".xlsx";
  //if(!file_exists($cache_file_name)){//判斷檔案內容是否有更新
    if($detail!="Y" && !$depID){//全表 & 選全部部門
      $jsonStr=file_get_contents($txtName);
      $jo=json_decode($jsonStr);
    }
    $filename = $wkMonth.'.xlsx';
    // header("Content-type:application/vnd.ms-excel");
    // header("Content-Disposition: attachment;filename=$filename");
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setSize(18);
    //設定欄位背景色(方法1)
    $objPHPExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cdcdcd')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:C1");
    $objPHPExcel->getActiveSheet()->mergeCells("D1:J1");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('D1:J1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:J2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
     $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $objPHPExcel->getActiveSheet()->setCellValue('A1','單位:'.($depName?$depName:'全部部門'));
    $objPHPExcel->getActiveSheet()->setCellValue('D1',$year."年".$month."月");
    $objPHPExcel->getActiveSheet()->getStyle("D1:J1")->applyFromArray($style);

    $reg = "((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))";
    preg_match($reg, $sql, $match);
    $aa = explode('/',$match[0]);
    
    $colAry=array("A2",'B2','C2',"D2","E2","F2","G2","H2","I2","J2");
    $titleAry=array("部門名稱",'員工編號','員工姓名',"月薪","時薪","事假時數","事假扣薪","病假時數","病假扣薪","扣薪合計");
    for($j=0;$j<count($colAry);$j++){
      $objPHPExcel->getActiveSheet()->setCellValue($colAry[$j],$titleAry[$j]);
    }

    $i = 3;
    $curDep = '';
    $curTyp = '';
    $tHours=0;
    $color = "dedede";
      
    $col2Ary=array("A",'B','C',"D","E","F","G","H","I","J");
    $wAry=array("15","10","15","15","10","20","15","15","15","20");
  
    getData($jo,$objPHPExcel,$col2Ary,$i,$curDep,$color);
    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    
    //設定欄寬
    for($j=0;$j<count($col2Ary);$j++){
      $objPHPExcel->getActiveSheet()->getColumnDimension($col2Ary[$j])->setWidth($wAry[$j]);
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $cache_path="../../data/caches";
    $objWriter->save($cache_path.'/'.$md5Name.'.xlsx'); 

  //} 

  echo $md5Name;
   
  function getData(&$obj,&$objPHPExcel,$col2Ary,&$i,&$curDep,&$color){
    global $emplyeeinfo,$departmentinfo;
    $kAry=array("empID","basic","depID","wage","personal","personal_hours","sick","sick_hours");
    foreach($obj as $k=>$val){
      if($k=='data'){
        foreach($val as $dk=>$dVal){
          proContent($dVal,$objPHPExcel,$col2Ary,$i,$curDep,$color);
        }
      }else if(in_array($k, $kAry)){
        proContent($obj,$objPHPExcel,$col2Ary,$i,$curDep,$color);
        break;
      } else{
        getData($val,$objPHPExcel,$col2Ary,$i,$curDep,$color);
      } 
    }
  }

function proContent($dVal,&$objPHPExcel,$col2Ary,&$i,&$curDep,&$color){
  global $emplyeeinfo,$departmentinfo, $showZero;
  if(!$showZero && ($dVal->personal+$dVal->sick) == 0) {
    return false;
  }
  $eid = $dVal->empID;
  $depID = $dVal->depID;
  if($curDep!=$depID) {

    $curDep = $depID;
    if($color=="ffffff"){
      $color="efefef";
    }else{
      $color="ffffff";
    }
    $objPHPExcel->getActiveSheet()->setBreak( 'A'.$i, PHPExcel_Worksheet::BREAK_ROW );
  }

  $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->applyFromArray(
      array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => $color)
          )
      )
  );
  $valAry=array();
  array_push($valAry,$departmentinfo[$depID]);    //部門名稱
  array_push($valAry,$eid);                       //員工編號
  array_push($valAry,$emplyeeinfo[$eid]);         //員工名稱
  array_push($valAry,$dVal->basic);               //月薪
  array_push($valAry,$dVal->wage);                //時薪
  array_push($valAry,$dVal->personal_hours);      //事假時數
  array_push($valAry,$dVal->personal);            //事假扣薪
  array_push($valAry,$dVal->sick_hours);          //病假時數
  array_push($valAry,$dVal->sick);                //病假扣薪
  array_push($valAry,round($dVal->personal+$dVal->sick));//扣薪合計
  for($j=0;$j<count($col2Ary);$j++){
    if($j==1){
      $objPHPExcel->getActiveSheet()->setCellValueExplicit($col2Ary[$j].$i,$valAry[$j],  PHPExcel_Cell_DataType::TYPE_STRING);
    }else{
      $objPHPExcel->getActiveSheet()->setCellValue($col2Ary[$j].$i,$valAry[$j]);
    }
  }
  $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
  $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => $color)
        )
    )
  );
  $i++;
}
?>