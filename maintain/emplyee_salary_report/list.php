<?
@session_start();
include "../../config.php";
include '../../getEmplyeeInfo.php';	
header("Content-Type:text/html; charset=utf-8");

//::秀出清單
$nowYr = date('Y');
$curYr = $_REQUEST['year']?$_REQUEST['year']:$nowYr;
if($_REQUEST['month']) $curMh = $_REQUEST['month']; else $curMh = date('m');
$wkMonth=$curYr.str_pad($curMh,2,"0",STR_PAD_LEFT);

//執行鎖定或關帳作業
$cMan = $_SESSION["Account"]?$_SESSION["Account"]:$_SESSION["empID"];
if($_POST['btn_abNormal']=='鎖定') {
	setAcctClose('salary',$wkMonth, $cMan);
} else if($_POST['btn_abNormal']=='解除鎖定') {
	setAcctClose('salary',$wkMonth, $cMan,0);
} else if($_POST['btn_abNormal']=='關帳') {
	setAcctClose('salary',$wkMonth, $cMan,1,1);
}
$fCheck = checkAcctClose('salary',$wkMonth,$cMan);

// 先取得紀錄筆數及算出分頁資訊 - for 分頁功能 //
$pageSize = 15;	//每頁的清單數量
if (!isset($_REQUEST['pages'])) $pages = 0; else $pages = $_REQUEST['pages'];
if (isset($_REQUEST["keyword"])) $keyword = $_REQUEST["keyword"]; else $keyword='';
if ($keyword=="") {
	$sql="select count(*) from emplyee_salary_report as r "
    ."left join emplyee as e on r.empID=e.empID "
    ."left join department as d on e.depID=d.depID "
    ."where r.wkMonth='".$wkMonth."' ";
}else {
	$sql="select count(*) from emplyee_salary_report as r ";
    $sql.="left join emplyee as e on r.empID=e.empID ";
    $sql.="left join department as d on e.depID=d.depID ";
    $sql.="where r.wkMonth='".$wkMonth."' ";
    $sql.="and ((e.empID like '%$keyword') or (e.empName like '%$keyword%')) ";
}
//是否離職專區
if($_REQUEST['fieldname']) {
	$fieldname=1;
	$rgBDate = strtotime("$curYr-$curMh-01");
	$rgEDate = strtotime('+1 month',$rgBDate);
	$sql .= "and e.leaveDate>='".date('Y-m-d',$rgBDate). "' and e.leaveDate<'".date('Y-m-d',$rgEDate)."' ";
}

//:: to 執行過濾功能 --------------------------------------------------------------------- >
if($_REQUEST[depFilter]){
	$temp[] = "d.depID like '$_REQUEST[depFilter]%'";
}else if ($_SESSION['privilege'] <= 10) {	
	if($_SESSION['user_classdef'][2]) {
		$aa = array();
		foreach($_SESSION['user_classdef'][2] as $v) { if($v)	$aa[]="d.depID like '$v%'";	}
		$temp[] = "(".join(' or ',$aa).")"; 	
	} else {
		$temp[] = "d.depID=''";
	}
}
if(count($temp)>0) $filter = join(' and ',$temp);
if($filter!="")	$sql.=" and $filter ";

$rs = db_query($sql,$conn);
$r = db_fetch_array($rs);
$recordCt = $r[0];
$pageCount=(int)($r[0] / $pageSize)-1; if($pageCount<0) $pageCount=0;
if(($r[0] % $pageSize) and ($recordCt>$pageSize)) $pageCount++; 

if (isset($_REQUEST['selid'])) $selid = $_REQUEST['selid']; else $selid=-1;

$sql="select *,r.notes as rNotes,e.empName,e.depID from emplyee_salary_report as r "
    ."left join emplyee as e on r.empID=e.empID "
    ."left join department as d on e.depID=d.depID "
    ."where r.wkMonth='".$wkMonth."' ";
if($filter!="")	$sql.="and $filter ";
if ($keyword!="") $sql.=" and ((e.empID like '%$keyword') or (e.empName like '%$keyword%'))";

//是否離職專區
if($_REQUEST['fieldname']) {
	$fieldname=1;
	$rgBDate = strtotime("$curYr-$curMh-01");
	$rgEDate = strtotime('+1 month',$rgBDate);
	$sql .= "and e.leaveDate>='".date('Y-m-d',$rgBDate). "' and e.leaveDate<'".date('Y-m-d',$rgEDate)."' ";
}

$sql .= " order by e.empID";

$rs=db_query($sql);
$outsql=$sql;
$sql .= " limit ".$pageSize*$pages.",$pageSize"; 
//echo $sql;
$rs=db_query($sql);
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="../list.css">
 <style type='text/css'>
	#d_hours{	
		position:absolute;
		left:35%;
		margin-left:-105px;
		overflow:auto;
		display:none;	
		top:8%;
	}
	.sBtn2{
		cursor:pointer;
		display:inline-block;
		text-decoration:none;
		font-size:12px;
		color:#fff;
		color:rgba(255,255,255,1);
		padding:3px 3px 1px 4px;
		border-style:solid;
		border-width:1px;
		border-radius:4px;
		box-shadow:0 1px 1px rgba(255,255,255,0.5) inset;
		
		background:#444444;
		border-color:#999;
		border-color:rgba(0,0,0,0.1);
		color:#fff; 
	}
 </style>
 <title></title>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script> 
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script> 
 <script type="text/javascript" src="/Scripts/jquery.blockUI.js"></script> 
 <script type="text/javascript">
  var selid = '<?=$selid?>';	
  var keyword = '<?=$keyword?>';
	var cancelFG = true;
	function Form1_Validator(tfm) {	return true; }
	
	function doSubmit() {	form1.submit();	}

	function filterSubmit(tSel) {	tSel.form.submit();	}
	
	//function DoEdit(id) {		window.parent.mainFrame.location = '../emplyee_salary_items/list.php?fid='+id;	}
	
	$(function() {	//點選清單第一項
		if( $('table.sTable').get(0).rows.length==1 ) return;				 
		if(document.all) $('table.sTable tr:eq(1) td:eq(0)')[0].click();	else {
			var evt = document.createEvent("MouseEvents");  
      		evt.initEvent("click", true, true);  
			var obj = $('table.sTable tr:eq(1) td:eq(0)')[0];
			obj.dispatchEvent(evt);
		}
		$('#d_hours').draggable();
		$('#d_setting').draggable();
	});

	$(document).ajaxStart(function () {
		$.blockUI({ 
		css: {border: 'none',padding: '15px',backgroundColor: '#000','-webkit-border-radius': '10px','-moz-border-radius': '10px',opacity: .6,color: '#fff'} ,
		message: '<h4>處理中，請稍候...</h4>'});
	}).ajaxStop($.unblockUI);

	function salary_calc(){
		if(!confirm("執行薪資試算前，請先執行「出缺勤異常作業」\r請問您有先執行「出缺勤異常作業」了嗎?"))	return;
		$.ajax({
		  url: "api.php",
		  method: "POST",
		  async: true,
		  dataType:"text",
		  data: { act: "salary_calc", dep: $("[name='depFilter']").val(), nowY:$("[name='year']").val(),nowM:$("[name='month']").val()}
		}).done(function(rs) {
			var url="list.php?depFilter="+$("[name='depFilter']").val()+"&year="+$("[name='year']").val()+"&month="+$("[name='month']").val();
			var params=rs.split(",");
			$.post("excel.php?wkMonth="+params[0]+"&depID="+params[1]+"&md5Name="+params[2], function(data) {	location.replace(url); })
			$.post("excel_absence.php?wkMonth="+params[0]+"&depID="+params[1]+"&md5Name="+params[3], function(data) {	})
		});
	}
	function setDutyHours(mCount,empName,id,empID){	
		$.ajax({
		  url: "api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act:'getHourfee',empID: empID}
		}).done(function(data) {
			$('#td_hourfee').html(data);
			$('#rId').val(id);
			$("#hours").val(mCount);
			$('#d_hours').show();
			$('#td_empName').html(empName);	
		});
	}
	function doHours(){
		$.ajax({
		  url: "api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act: "doHours", hours: $("#hours").val(), rId:$("#rId").val(),hourfee:$('#td_hourfee').html()}
		}).done(function() {
			location.reload();
		});
	}
	function cancel(){	$('#d_hours').hide();}
	
	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var year = $('select#year option:selected').val();
		var month = $('select#month option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(year) url = url+'&year='+year;
		if(month) url = url+'&month='+month;
		location.href = url;
	}
 </script> 
 <script type="text/javascript" src="list.js"></script> 
</Head>

<body>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this)" style="margin:0">
 <table width="100%">
 	<tr><td>
  	<? if($fieldname) echo "<input type='hidden' name='fieldname' value='1'>" ?>
		<input type="hidden" name="keyword" value="<?=$keyword?>">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
	  <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
	  <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? 
		  if ($_SESSION['privilege'] > 10) {
		    foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
		  } else {
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
		  }
			?>
	  </select>
	  <select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-2;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
	  <select name="month" id="month" onChange="doSubmit()">
	  <? 
	  	if($curYr<$nowYr){	$m=12;	}else{	$m=date('m');}
	   		for($i=1;$i<=$m;$i++) { 
	  		$sel=($curMh==$i?'selected':''); 
	  		echo "<option $sel value='$i'>$i 月</option>"; 
	  	} 
	  ?>
	  </select>
    <? if($fCheck==0 || ( $fCheck<>1 &&	strpos($fCheck,$cMan)===false) ) { ?>
      <input type='button' value="薪資試算" onclick='salary_calc()'/>
    <? } else { ?>
      <input type='button' value="薪資試算" disabled />    
    <? } ?>           
    <input type='button' value="薪資清冊-人事" onClick="getExcelV('exportExcel.php')"/>
    <input type='button' value="薪資清冊-會計" onClick="getExcelV('exportExcelaCC.php')"/>
    
    <? if ($_SESSION['privilege'] <= 10) {
      switch($fCheck) { 
        case 0:  echo '<input type="submit" name="btn_abNormal" value="鎖定">'; break; 
        case 1:  echo '<input type="button" value="已關帳" disabled>'; break;
				default: 
					if( strpos($fCheck,$cMan)===false ) {
						echo '<input type="submit" name="btn_abNormal" value="鎖定">'; $fCheck = 0;
					} else {
						echo '<input type="submit" name="btn_abNormal" value="解除鎖定">'; $fCheck = 1;
					}
					break; 
      }
    } else { 
      switch($fCheck) { 
        case 0: echo '<input type="submit" name="btn_abNormal" value="關帳">'; break;
        case 1: echo '<input type="button" value="已關帳" disabled>'; break;
        default: echo '<input type="submit" name="btn_abNormal" value="關帳">'." <span class='sText' style='color:#00F'>已設鎖的帳號：$fCheck</span>";
      }
    } ?>  
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).'/'.(int)($pageCount+1)." 總筆數：$recordCt"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
</form>
<table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<tr class="colHD" style="color:#FFF">
		<td width="60">序號</td><td width="60">月份</td><td>部門</td><td width="60">姓名</td><td width="60">給薪類別</td>
		<td>本薪</td><td>加項合計</td><td>減項合計</td><td>實付金額</td><td>退休金雇主提撥</td><td width="120">備註</td>
  	<? if($fieldname) echo "<td>離職日</td>" ?>
  </tr>
<?
$salaryType=array("0"=>"月薪","1"=>"時薪","2"=>"產能敘薪","3"=>"固定薪");
if(!db_eof($rs)){
	while($r=db_fetch_array($rs)){
		$id = $r[id];
		$tr="<tr valign='top' id='tr_$id' onMouseOver=\"SelRowIn('$id',this)\" onMouseOut=\"SelRowOut('$id',this)\" onClick='SelRow($id,this,\"".$r[empID]."\",\"".$r[depID]."\",".$r[wage].",$fieldname);' ".($id==$selid?'class="onSel"':'').">
		<td><img style='cursor: hand;' src='/images/download.png' onClick='salaryReportPersonal(".$r['id'].")'> ".$r[id]."</td>";
		$tr.="<td>".$r[wkMonth]."</td>";
		$tr.="<td>".$r[depName]."</td>";
		$tr.="<td>".$r[empName]."</td>";
		$tr.="<td>".$salaryType[$r[sType]]."</td>";
		if($r['sType']=='1'){//時薪
			if($r[mCount]==0)	$mb=0;	else $mb=$r[mBasic];
			$hourBtn='';
			if($fCheck==0 || ( $fCheck<>1 &&	strpos($fCheck,$cMan)===false) ) { 
		      $hourBtn = "<input type='button' value='填入實際工時' onClick='setDutyHours(".$r[mCount].",\"".$r[empName]."\",\"".$r[id]."\",\"".$r[empID]."\")'/>";
		    }   
			$tr.="<td>".$mb."&nbsp;".$hourBtn."</td>";
		}else{
			$tr.="<td>".($r[mBasic]?$r[mBasic]:0)."</td>";
		}
		$tr.="<td>".($r[addTotal]?$r[addTotal]:0)."</td>";
		$tr.="<td>".($r[subTotal]?$r[subTotal]:0)."</td>";
		$tr.="<td>".($r[payTotal]?$r[payTotal]:0)."</td>";
		$tr.="<td>".($r['retireCPay'])."</td>";
		$tr.="<td><input type='text' data-text='".$id."' value='".$r['rNotes']."'><button data-btn='".$id."'>確定</button></td>";
		if($fieldname) $tr.="<td>$r[leaveDate]</td>";
		$tr.="</tr>";
		echo $tr;
	}
}
?>
</table>
<div id='d_hours'> 
  <div id='dTitle' class='divTitle divTitle2'>填入實際工時</div>
    <div  class='divBorder2' >
       <table class='grid'>
        <input type='hidden' id='rId'/>
        <tr><td>員工姓名</td><td id='td_empName'></td></tr>
        <tr><td>時薪</td><td id='td_hourfee'></td></tr>
        <tr><td>實際工時</td><td><input type='text' id='hours'></td></tr>
    </table>
    <div class='dBottom'><input type="button" id='btnSave' class='sBtn2' onClick="doHours()" value="儲存">&nbsp;<input type="button" class='sBtn2' id='btnCancel' onClick="cancel()" value="取消"></div>
    </div>
</div>

<form name="excelfm" action="exportExcel.php" method="post" target="excel">
	<input type="hidden" name="fmTitle" value="員工薪資清冊">
	<input type="hidden" name="projID" value="">
	<input type="hidden" name="year" value="">
	<input type="hidden" name="month" value="">
	<input type="hidden" name="printType" value="">

	<input type="hidden" name="yearE" value="">
	<input type="hidden" name="monthE" value="">

	<input type="hidden" name="depID" value="">
	<input type="hidden" name="empID" value="">
</form>

<script type="text/javascript">
	var rztExcel = false;
	$(function(){
		$('[data-btn]').click(function(){
			var id = $(this).attr('data-btn');
			$.ajax({
				url: 'addNotes.php',
				type:"POST",
				//dataType:'JSON',
				data:{'id':id,'notes': $('[data-text="'+id+'"]').val()},
				success: function(data){alert('備註已經寫入');},
				error:function(xhr, ajaxOptions, thrownError){  }
			});

		})
		$('[name="btn_abNormal"]').click(function(){
			if(!confirm('確認要'+$(this).val()+'?')) return false;
		});
	});

	function getExcelV(action){
		var url = 'selectExcel.php?action='+action; 
		if(rztExcel){
			rztExcel.close();
		}
		url += '&year='+$('#year').val()+'&month='+$('#month').val();
		if(rztExcel == false){
			rztExcel = window.open(url, "web",'width=600,height=750');
		}else{
			rztExcel = window.open(url, "web",'width=600,height=750');
			rztExcel.focus();
		}
	}


	function salaryReportPersonal(id) {
		var url = '/maintain/salaryReportPersonal/edit.php?id='+id; 
		if(rztExcel){
			rztExcel.close();
		}
		url += '&year='+$('#year').val()+'&month='+$('#month').val();
		if(rztExcel == false){
			rztExcel = window.open(url, "web",'width=800,height=750');
		}else{
			rztExcel = window.open(url, "web",'width=800,height=750');
			rztExcel.focus();
		}
	}
</script>
</body>
</Html>