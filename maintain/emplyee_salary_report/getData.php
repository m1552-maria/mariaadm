<?php
	function getSalary($data = array()) {
		$result = array();
			//取每月薪資
	    $sql = "select r.*, r.id as rID, 
	    e.empName, e.jobClass, e.perID, e.birthday, e.depID, e.projID, e.hireDate
	    from emplyee_salary_report as r 
	    left join emplyee as e on r.empID=e.empID ";
	    $sql.= " where r.wkMonth >= '".$data['sWkMonth']."' and r.wkMonth <= '".$data['eWkMonth']."'";
	    if($data['empID'] != '') {
	    	$eList = explode(',', $data['empID']);
	    	$search = array();
	    	foreach ($eList as $key => $value) $search[] = "'".$value."'";
	   		$sql.= " and e.empID in (".implode(',', $search).")";
	    }

	    if($data['depID'] != '') {
	    	$dList = explode(',', $data['depID']);
	    	$search = array();
	    	foreach ($dList as $key => $value) $search[] = "'".$value."'";
	   		$sql.= " and e.depID in (".implode(',', $search).")";
	    }

	    $sql.= " order by e.projID,e.depID, r.wkMonth, e.empID";
	    $rs = db_query($sql);
	    $empIDList = array();
	    $dataList = array();
	    $rIDList = array();
	    while ($r=db_fetch_array($rs)) {
	      $rIDList[] = $r['rID'];
	      $empIDList[$r['empID']] = "'".$r['empID']."'";
	      $addsubList = array(
	          'allowance' =>$r['addTotal'], //津貼及加項 +
	          'overtime' =>0, //加班費 +
	          'absence' =>0, //請假調整 -
	          'taxPay' =>0, //所得稅 -
	          'jobPay' =>0, //勞保 -
	          'healthPay' =>0, //健保 -
	          'mRetirePay' =>0, //自提退休金 -
	          'otherPay' =>$r['subTotal'] //其他代扣 -
	      );
	      $r['source'] = 'dataList';
	      $dataList[$r['empID']][$r['wkMonth']] = array_merge($r,$addsubList);
	    }


	    //取得薪資其他加減項
	    $sql2 = "select * from emplyee_salary_items where fid in (".implode(',', $rIDList).")";
	    $rs2=db_query($sql2);
	    while ($r2=db_fetch_array($rs2)) {
	      $itemList[$r2['fid']][$r2['id']] = $r2;
	    }

	    foreach ($dataList as $empID => $second) {
	      foreach ($second as $wkMonth => $third) {
	        if(!isset($itemList[$third['rID']])) continue;
	        foreach ($itemList[$third['rID']] as $k2 => $r2) {
	            if($r2['addsub'] == '+') {
	                switch ($r2['iType']) {
	                  case 'absence':
	                    $dataList[$empID][$wkMonth]['absence'] -= $r2['money'];
	                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
	                    break;
	                  case 'overtime':
	                    $dataList[$empID][$wkMonth]['overtime'] += $r2['money'];
	                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
	                    break;  
	                  case 'otherfee': //雜項費用
	                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
	                    break;  
	                  default:
	                    switch ($r2['title']) {
	                      case '勞保自負額':
	                        $dataList[$empID][$wkMonth]['jobPay'] -= $r2['money'];
	                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
	                        break;
	                      case '健保自負額':
	                        $dataList[$empID][$wkMonth]['healthPay'] -= $r2['money'];
	                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
	                        break;
	                      case '勞退自提額':
	                        $dataList[$empID][$wkMonth]['mRetirePay'] -= $r2['money'];
	                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
	                        break;
	                      break;
	                    }
	                }
	                    
	            } else {
	                switch ($r2['iType']) {
	                  case 'absence':
	                    $dataList[$empID][$wkMonth]['absence'] += $r2['money'];
	                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                    break;
	                  case 'overtime':
	                    $dataList[$empID][$wkMonth]['overtime'] -= $r2['money'];
	                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                    break;
	                  case 'allowance':
	                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
	                    break;
	                  default:
	                    switch ($r2['title']) {
	                      case '所得稅':
	                          $dataList[$empID][$wkMonth]['taxPay'] += $r2['money'];
	                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                          break;
	                      case '勞保自負額':
	                          $dataList[$empID][$wkMonth]['jobPay'] += $r2['money'];
	                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                          break;
	                      case '健保自負額':
	                          $dataList[$empID][$wkMonth]['healthPay'] += $r2['money'];
	                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                          break;
	                      case '勞退自提額':
	                          $dataList[$empID][$wkMonth]['mRetirePay'] += $r2['money'];
	                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
	                          break;   
	                      break;
	                    }   
	                }
	            }
	        }
	      }
	    }

	    //取員工每月的計劃ID&部門ID
	    $empHistory = getEmpHistory($empIDList);
	    //塞入計劃ID&部門ID
	    $dataList = setProjIDAndDepID($dataList, $empHistory);


	    foreach ($dataList as $empID => $v1) {
	      foreach ($v1 as $wkMonth => $v2) {
	      	if($v2['projID'] == '') $v2['projID'] = 'no_proj';
	        $result[$v2['projID']][$v2['depID']]['sub_list'][$v2['id']] = $v2;
	      }
	    }


	    $projIDList = ($data['projID'] != '') ? explode(',', $data['projID']) : array();
	    $pCount = count($projIDList);
	    $depIDList = ($data['depID'] != '') ? explode(',', $data['depID']) : array();
	    $dCount = count($depIDList);

	    foreach ($result as $projID => $v1) {
	    	if($pCount > 0) {
	      		$i = $pCount;
	      		foreach ($projIDList as $k => $v) {
		          if(strpos((string)$projID, (string)$v) === 0) break; //有符合搜尋計畫編號條件
		          $i--;
		        }
		        if($i == 0) {
	          		unset($result[$projID]);
	          		continue;
		        }
	      	}
	    	foreach ($v1 as $depID => $v2) {
	    		if($dCount > 0) {
		      		$i = $dCount;
		      		foreach ($depIDList as $k => $v) {
			          if(strpos((string)$depID, (string)$v) === 0) break; //有符合搜尋計畫編號條件
			          $i--;
			        }
			        if($i == 0) {
		          		unset($result[$projID][$depID]);
		          		continue;
			        }
	      		}
	    	}
	    }


	    //排序
	    $sql = "select depID, projID from emplyee_history where 1 order by projID asc, depID asc";
	    $rs = db_query($sql);
	    $DPList = array('no_proj'=>'no_proj');
	    while ($r=db_fetch_array($rs)) {
	      $DPList[$r['projID']] = $r['projID'];
	    }

	    foreach ($DPList as $key => $value) {
	      if(isset($dataList[$key])) {
	        $result['dataList'][$key] = $dataList[$key];
	      }
	    }

	    return $result;

	}

	
	function getEmpHistory($empIDList = array()) {
	  	$result = array();
	  	$sql = "select *, date_format(rdate, '%Y%m') as wkMonth from emplyee_history where empID in (".implode(',', $empIDList).") ";

	    $sql .= " order by empID, rdate";
	    $rs = db_query($sql);

	    while ($r=db_fetch_array($rs)) {
	    	$result[] = $r;
	    }

	    return $result;

	}

	function setProjIDAndDepID($dataList = array(), $empHistory = array()) {
	    $firstData = array();
	    foreach($empHistory as $k => $r) {
	      if(!isset($dataList[$r['empID']])) continue;
	      if(!isset($firstData[$r['empID']])) $firstData[$r['empID']] = $r;
	      foreach ($dataList[$r['empID']] as $wkMonth => $value) {
	        //先都預設給第一筆的計畫ID跟部門ID
	        if(!isset($dataList[$r['empID']][$wkMonth]['projID'])) {
	          $dataList[$r['empID']][$wkMonth]['projID'] = $firstData[$r['empID']]['projID'];
	          $dataList[$r['empID']][$wkMonth]['depID'] = $firstData[$r['empID']]['depID'];
	        }

	        $_wkMonth = explode('_', $wkMonth);
	        $_wkMonth = $_wkMonth[0];
	        if((int)$_wkMonth >= (int)$r['wkMonth']) {
	          $dataList[$r['empID']][$wkMonth]['projID'] = $r['projID'];
	          $dataList[$r['empID']][$wkMonth]['depID'] = $r['depID'];
	        }

	        
	      }
	    }
	    return $dataList;

	}
?>