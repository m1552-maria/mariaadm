<?
//::處理加班費-------------------------------------------------------------
function db2items($depID){
	$sql="select * from department where depID like '$depID%' order by depID ";
	$rs=db_query($sql);
	$items=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$items[$r[depID]]=$r;
		}
	}
	return $items;
}
function items2treeN($items){
	$tree=array();
	//$reftab用來記錄item在tree裏位置的reference.
	$reftab=array();
	$rootID = null;
	foreach($items as $key => $row) {
	 	$pkey=$row['PID'];
		if(!isset($rootID)) $rootID = $pkey; 
		if($pkey==$rootID){ //Initial
			$tree[$key]=array();
			$reftab[$key]=&$tree[$key];
		}else {//一般節點
	    if(!isset($reftab[$pkey])){
	    	$reftab[$pkey]=array();
	    }
	    if(!isset($reftab[$key])){
	    	$reftab[$key]=array();
	    }
	    $reftab[$pkey][$key]=&$reftab[$key];
		}
	}
	return $tree;
}

// function tree2view(&$tree,$depID,$act,$emps){
// 	global $departmentinfo;
// 	$object=new Departments();
// 	$object->dept_id = $depID;
// 	$object->name = $departmentinfo[$depID]?$departmentinfo[$depID]:'所有部門';
// 	foreach($tree as $k=>$v) {	
// 		$object->sub_departments[]=tree2view($tree[$k],$k,$act); 
		
// 	}
// 	if($act=="emp"){
// 		$object->employess=$emps[$depID];
// 	}
// 	return $object;
// }

function db2emps2($depID){
	$sql="select * from emplyee ";
	if($depID!=""){
		$sql.="where depID like '$depID%' ";
		$sql.="order by depID ";
	}	
	$rs=db_query($sql);
	$items=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if(!is_array($items[$r[depID]]))	$items[$r[depID]]=array();
		}
	}
	return $items;
}

class Departments {
	
}
/*
 *	加班費
*/
class OvertimeFee{
	public $empID;
	public $basic;
	public $depID;
	public $wage;
	public $aType=array(0,1,2);
	
}
class OvertimeType{
	public $hourA=0;	//前2小時時數
	public $feeA=0;		//前2小時加班費
	public $hourB=0;	//第3-8小時時數
	public $feeB=0;		//第3-8小時加班費
	public $hourC=0;	//第9-12小時時數
	public $feeC=0;		//第9-12小時加班費
	public $sum;
	
	function doSum($type) {
		$this->sum = $this->aType[$type]->feeA + $this->aType[$type]->feeB + $this->aType[$type]->feeC ;
        return $this->sum;
    }
}

function tree2overtime(&$tree,$depID){
	global $departmentinfo,$emps;
	if($depID!="")	$object->data=$emps[$depID];
	foreach($tree as $k=>$v) {	
		$object->$k=tree2overtime($tree[$k],$k,$act);
	}
	return $object;
}
/*
 *	出缺勤
*/
class AbsenceFee{
	public $empID;
	public $basic;
	public $depID;
	public $wage;
	public $personal=0;		//事假
	public $personal_hours=0; //事假時數
	public $sick=0;			//病假
	public $sick_hours=0;		//病假時數
	
}

function tree2absence(&$tree,$depID){
	global $departmentinfo,$emps_absence;
	if($depID!="")	$object->data=$emps_absence[$depID];
	foreach($tree as $k=>$v) {	
		$object->$k=tree2absence($tree[$k],$k,$act);
	}
	return $object;
}

function getWage($eID, $aDate=NULL) {	//增加帶日期参數，以取得當時時薪 
	global $nowY,$nowM,$days_in_month,$base_days;
	if($aDate==NULL) $lastday = date('Y-m-d', strtotime($nowY."-".$nowM."-01 +1 month -1 day")); else $lastday = $aDate;
	$sql="select salary,salaryType,ratio,hourfee,capacity_ratio from emplyee_salary ";
	$sql.="where empID='$eID' and date(changeDate)<='".$lastday."' ";
	$sql.="order by changeDate DESC limit 0,1";
	$rs=db_query($sql);
	$rT=db_fetch_array($rs);
	$salaryType=$rT['salaryType'];
	$salary=$rT['salary'];
	$wage=0;		//時薪
	switch($salaryType){
		case "0"://::月薪	
			$wage=$salary*$rT['ratio']/30/(8*$rT['ratio']);	break;
		case "1"://::時薪
			$wage=$rT['hourfee'];	break;
		case "2"://::產能敘薪
			$wage=$mBasic/30/(8*$rT['ratio']); break;
		case "3"://::固定薪
			$wage=$salary/30/(8*$rT['ratio']); break;
	}
	return $wage;
}


function overtimeFee($aType,$wage,$hours){  //加班費計算
	$feeA=0; $feeB=0; $feeC=0;
	switch($aType){
		case "0":	//上班日	
		case "1":	//休息日	
			if($hours<=2){
				$feeA=round($wage*1.34*$hours);
			}else if($hours<=8){
				$feeA=round($wage*1.34*2);
				$feeB=round($wage*1.67*($hours-2));
			}else if($hours<=12){
				$feeA=round($wage*1.34*2);
				$feeB=round($wage*1.67*6);
				$feeC=round($wage*2.67*($hours-8));
			}	
			break;
		case "2":	//國定假日
			if($hours<=8){
				if($hours<=2){
					$feeA=round($wage*1*$hours);
				}else{
					$feeA=round($wage*1*2);
					$feeB=round($wage*1*($hours-2));
				}
			}else if($hours<=12){
				$feeA=round($wage*1*2);
				$feeB=round($wage*1*6);
				$feeC=round($wage*2*($hours-8));
			}
			break;
	}
	return $feeA+$feeB+$feeC;
}
?>