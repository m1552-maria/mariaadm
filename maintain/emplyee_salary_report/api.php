<?php
include_once "../../config.php";
include_once "../inc_vars.php";
include_once "../../getEmplyeeInfo.php";
include_once "Fee.php";


$act=$_GET['act'];
switch($act){
	case "salary_calc"://薪資試算
		$nowY=$_GET['nowY'];
		$nowM=$_GET['nowM'];
		$dep=$_GET['dep'];
		global $nowY,$nowM;
		salary_calc($dep,$jobInsurence,$healthInsurence,$retireInsurence);
		break;
	case "doHours"://填入時薪數，算薪水
		$rId=$_GET[rId];
		$hours=$_GET[hours];
		$hourfee=$_GET[hourfee];
		doHours($rId,$hours,$hourfee);
		break;
	case "getHourfee"://取得時薪
		$empID=$_GET[empID];
		getHourfee($empID);
		break;
}

function getHourfee($empID){
	$sql="select * from emplyee_salary where empID='".$empID."' and salaryType=1 order by rdate desc ";
	//echo $sql;
	$rs=db_query($sql);
	$hourfee=0;
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$hourfee=$r['hourfee'];
	}
	echo $hourfee;
}
function doHours($rId,$hours,$hourfee){
	$sql="select * from emplyee_salary_report where id=".$rId;
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$mBasic=$hourfee*$hours;
		$payTotal=$mBasic+$r['addTotal']-$r['subTotal']+$r['onhandleTotal'];
		$sql="update emplyee_salary_report set mCount=".$hours.",mBasic=$mBasic,payTotal=$payTotal where id=".$rId;
		db_query($sql);
	}
}
function salary_calc($dep,$jobInsurence,$healthInsurence,$retireInsurence) {
	global $nowY,$nowM,$days_in_month,$base_days,$wkMonth,$emps,$emps_absence,$departmentinfo,$emplyeeinfo,$addTotal,$subTotal, $oEmpList;

	$days_in_month = cal_days_in_month(CAL_GREGORIAN, $nowM, $nowY);
	/*取的該月 月中有留停或復職*/
	$mFirstDay = $nowY.'-'.str_pad($nowM,2,"0",STR_PAD_LEFT).'-01';
	$mLastDay = $nowY.'-'.str_pad($nowM,2,"0",STR_PAD_LEFT).'-'.$days_in_month;
	$oEmpList = array(); //取得當月 月中留停的empID
	$sEmpList = array(); // 需要特殊處理的empID (月中留停 & 月中復職)
	$sql = "select s.* from emplyee_status s inner join emplyee e on s.empID=e.empID where s.bdate > '".$mFirstDay."' and s.bdate <= '".$mLastDay."' and s.jobStatus in ('3', '5') ";
	if($dep) $sql .= " and e.depID like '$dep%'";
	$sql .= " order by e.empID";

	$rs = db_query($sql);
	while ($r = db_fetch_array($rs)) {
		if($r['jobStatus'] == '3') $oEmpList[$r['empID']] = "'".$r['empID']."'";
		$sEmpList[$r['empID']] = $r; 
	}

	//處理當月留停後直接離職狀況
	$stopEmpList = array(); // 若 當月留停後直接離職 當月整月為留停狀態 不撈取 計算薪資 (要先扣掉復職人員)
	$sql = "select e.empID from emplyee_status s inner join emplyee e on s.empID = e.empID and e.leaveDate = s.edate where s.edate >= '".$mFirstDay."' and s.edate <= '".$mLastDay."' and s.jobStatus in ('3', '4') ";
	$rs = db_query($sql);
	while ($r = db_fetch_array($rs)) {
		$stopEmpList[$r['empID']] = "'".$r['empID']."'";
	}


	//取得當月有復職的人
	$sql = "select s.* from emplyee_status s inner join emplyee e on s.empID=e.empID where s.bdate >= '".$mFirstDay."' and s.bdate <='".$mLastDay."' and s.jobStatus ='5' ";
	if($dep) $sql .= " and e.depID like '$dep%'";
	$sql .= " order by e.empID";
	$jobStatus5 = array(); //復職人員
	$rs = db_query($sql);
	while ($r = db_fetch_array($rs)) {
		$jobStatus5[$r['empID']] = "'".$r['empID']."'";
	}

	$sql = "select s.* from emplyee_status s inner join emplyee e on s.empID=e.empID where s.bdate <= '".$mFirstDay."' and (s.edate >= '".$mLastDay."' or s.edate = '0000-00-00') and s.jobStatus in ('3','4') ";
	//if(count($jobStatus5) > 0) $sql .= " and e.empID not in (".implode(',', $jobStatus5).")";
	if($dep) $sql .= " and e.depID like '$dep%'";
	$sql .= " order by e.empID";
	$rs = db_query($sql);
	while ($r = db_fetch_array($rs)) {
		if(isset($jobStatus5[$r['empID']])) continue; //扣掉復職人員
		$stopEmpList[$r['empID']] = "'".$r['empID']."'";
	}

	$items = db2items($dep);	//部門資料含下層
	$emps = db2emps2($dep);
	$emps_absence = db2emps2($dep);

	$sql="select *,e.empID as empID from emplyee as e ";
	$sql.="left join emplyee_insurence as i ";
	$sql.="on e.empID=i.empID ";

	//增加以離值日來判別是否撈取
	$_where = " or (e.isOnduty <> 1 and leaveDate >= '".$mFirstDay."' and leaveDate <= '".$mLastDay."') ";

	if(count($oEmpList) == 0) $sql.="where ( e.isOnduty=1 ".$_where.")";
	else $sql.="where (e.isOnduty=1 ".$_where." or e.empID in(".implode(',', $oEmpList).") )";

	if($dep) $sql .= " and e.depID like '$dep%'";
	if(count($stopEmpList) > 0) $sql .= " and e.empID not in(".implode(',', $stopEmpList).")";
	$sql.= " and e.hireDate <= '".$mLastDay."'";
	$sql.=" group by e.empID ";
	$sql.=" order by e.empID ";
	$rsX2 = db_query($sql);

  
	$base_days=30;	//大小月都以30天去計箅實際工作比例
	$wkMonth = $nowY.str_pad($nowM,2,"0",STR_PAD_LEFT);
	while($rX2 =db_fetch_array($rsX2)) {
		$eID=$rX2['empID'];
		$depID=$rX2['depID'];
		if(!$rX2['hireDate'])	continue;
		$isDonate=$rX2['isDonate'];
		$donate=$rX2['donate'];
		$tax=$rX2['tax'];
		//::本薪取得

		$hireDate = $rX2['hireDate'];
		$leaveDate = $rX2['leaveDate'];
		/*月中留停 or 月中復職 本薪計算點日期要更改*/
		if(isset($sEmpList[$eID])) {
			if($sEmpList[$eID]['jobStatus'] == '3') $leaveDate = date("Y-m-d",strtotime("-1 day", strtotime($sEmpList[$eID]['bdate']))); //月中留停
			else $hireDate = $sEmpList[$eID]['bdate']; //月中復職
		} 

		$ary= basic($hireDate,$leaveDate,$eID);	
		if(count($ary)==0) continue;
		$salaryType=$ary[0];
		$mBasic = $ary[1];	
		$mCount = $ary[2];
		$wage		= $ary[3];		//日薪換算後的時薪
		$mRatio	= $ary[4];		//工時比例		
		$food = $ary[5]; //伙食費
		// echo 'salaryType:'.$salaryType.",mBasic:".$mBasic."\n";
		
		//::薪資記錄表主檔
		$sql="select id, notes, mCount from emplyee_salary_report where empID='$eID' and wkMonth='$wkMonth' ";
		$rNotes = 'NULL';
		$rs=db_query($sql);
		if(!db_eof($rs)){
			$r=db_fetch_array($rs);
			$rId=$r[id];
			$rNotes = "'".$r['notes']."'";
			if($salaryType == 1 && $r['mCount'] > 0) $mCount = $r['mCount'];
		}
		$sql="delete from emplyee_salary_report where empID='$eID' and wkMonth='$wkMonth'"; db_query($sql);
		//::清除該員加減項
		$sql="delete from emplyee_salary_items where fid=$rId and isExpand=0 ";	db_query($sql);
		//::建立主檔資料
		$sql = "insert into emplyee_salary_report(empID,mBasic,wkMonth,sType,wage,food,ratio,notes) values('$eID',".$mBasic.",'$wkMonth',".$salaryType.",$wage,$food,$mRatio,$rNotes)"; db_query($sql);
		$mID = mysql_insert_id();
		
		//::更新該員手動增加的加減項，讓手動增加的項目不會因薪資重算而不見
		$sql="update emplyee_salary_items set fid=$mID where fid=$rId and isExpand=1 ";
		db_query($sql);
		//處理加減項(勞、健、退)
		list($job_mPay,$job_cPay,$health_mPay,$health_cPay,$retire_mPay,$retire_cPay)=addSub($mID,$eID,$jobInsurence,$healthInsurence,$retireInsurence);
		//處理請假
		$allMonthAbsence = absence($mID,$eID,$wage,$mRatio);//依據請假單加減項
		//處理加班費
		overtime($mID,$eID,$wage,$wkMonth,$depID);	//依據加班單(選取加班費者)計算
		allowance($mID,$eID,$wkMonth);							//津貼計算
		otherfee($mID,$eID,$wkMonth);								//雜項計算
		//捐款
		if($isDonate && $donate>0){
			$sql = "insert into emplyee_salary_items(fid,title,addsub,hours,money,iType,wkMonth) values($mID,'捐款','-',1,$donate,'otherfee','$wkMonth')";
			db_query($sql);
		}
		//所得稅
		if($tax>0){
			$sql = "insert into emplyee_salary_items(fid,title,addsub,hours,money,iType,wkMonth) values($mID,'所得稅','-',1,$tax,'otherfee','$wkMonth')";
			db_query($sql);
		}

		//增加餐費(sean)
		if($rX2['mealFee'] > 0) {
			$sql = "insert into emplyee_salary_items(fid,title,addsub,hours,money,iType,wkMonth) values($mID,'餐費','-',1,".$rX2['mealFee'].",'otherfee','$wkMonth')";
			db_query($sql);
		}

		//法院扣薪(sean)
		$_wkMonth = strtotime($nowY.'-'.str_pad($nowM,2,"0",STR_PAD_LEFT));
		if($rX2['court'] > 0) {
			if($rX2['courtEdate'] != '' && strtotime($rX2['courtSdate']) <= $_wkMonth && $_wkMonth <= strtotime($rX2['courtEdate'])) {
				$sql = "insert into emplyee_salary_items(fid,title,addsub,hours,money,iType,wkMonth) values($mID,'法院扣薪','-',1,".$rX2['court'].",'otherfee','$wkMonth')";
				db_query($sql);
			}elseif($rX2['courtEdate'] == '' && strtotime($rX2['courtSdate']) <= $_wkMonth) {
				$sql = "insert into emplyee_salary_items(fid,title,addsub,hours,money,iType,wkMonth) values($mID,'法院扣薪','-',1,".$rX2['court'].",'otherfee','$wkMonth')";
				db_query($sql);
			}
		}

		//::計算加減項、實付金額
		$sql="select * from emplyee_salary_items where fid=$mID";
		$rs=db_query($sql);
		$addTotal=0;
		$subTotal=0;
		if(!db_eof($rs)){
			//出缺勤
			$abObj=new AbsenceFee();
			$abObj->depID = $rX2[depID];
			$abObj->empID = $eID;
			$abObj->empName = $emplyeeinfo[$eID];
			$abObj->basic = $mBasic;
			$abObj->wage=$wage;
			//加班費
			$otObj=new OvertimeFee();
			$otObj->depID = $rX2[depID];
			$otObj->empID = $eID;
			$otObj->empName = $emplyeeinfo[$eID];
			$otObj->basic = $mBasic;
			$otObj->wage=$wage;
			$aType0=new OvertimeType();
			$aType1=new OvertimeType();
			$aType2=new OvertimeType();
			$aType3=new OvertimeType();
			while($r=db_fetch_array($rs)){
				if($r[iType]=="overtime"){//處理加班費
					if($r[title]=='加班-上班日'){
						$aType0=overtimeItem($aType0,$r[hours],$r[money],$r[subClass],$r[addsub]);
					}else if($r[title]=='加班-休息日'){
						$aType1=overtimeItem($aType1,$r[hours],$r[money],$r[subClass],$r[addsub]);
					}else if($r[title]=='加班-國定假日'){
						$aType2=overtimeItem($aType2,$r[hours],$r[money],$r[subClass],$r[addsub]);
					} else if($r[title]=='加班-天災、選舉日'){
						$aType3=overtimeItem($aType3,$r[hours],$r[money],$r[subClass],$r[addsub]);
					}
				}else if($r[iType]=="absence"){//處理出缺勤
					$abObj=absenceItem($abObj,$r[hours],$r[money],$r[addsub],$r[title]);
				}else{//處理其他加減項
					if($r[addsub]=="+"){
						$addTotal+=$r[money];
					}else{
						$subTotal+=$r[money];
					}
				}

			
			}
			$aType0->sum=$aType0->feeA + $aType0->feeB + $aType0->feeC;
			$aType1->sum=$aType1->feeA + $aType1->feeB + $aType1->feeC;
			$aType2->sum=$aType2->feeA + $aType2->feeB + $aType2->feeC;
			$aType3->sum=$aType3->feeA + $aType3->feeB + $aType3->feeC;
			$otObj->aType[0]=$aType0;
			$otObj->aType[1]=$aType1;
			$otObj->aType[2]=$aType2;
			$otObj->aType[3]=$aType3;
			$emps[$rX2[depID]][$eID]=$otObj;
			$emps_absence[$rX2[depID]][$eID]=$abObj;
		}
		
		if($allMonthAbsence > 0) { //該月整月請假
			$absenceTotal = $mBasic;
			$sql = "insert into emplyee_salary_items(fid,title,addsub,money,iType,hours,wkMonth) values('".$mID."','整月請假','-','".$absenceTotal."','absence','".$allMonthAbsence."','".$wkMonth."')";
			db_query($sql);
			$abObj=absenceItem($abObj, $allMonthAbsence, $absenceTotal, '-', '整月請假');
			$emps_absence[$rX2[depID]][$eID]=$abObj;

			//$subTotal += $absenceTotal;
		} 
		$payTotal = $mBasic+$addTotal-$subTotal;
		

		$sql="update emplyee_salary_report set mBasic=$mBasic,subTotal=$subTotal,addTotal=$addTotal,sType=$salaryType,mCount=$mCount,";
		$sql.="payTotal=$payTotal,cJob='$job_cPay',cHealth='$health_cPay',retireCPay='$retire_cPay',cRetire='$retire_cPay' ";
		$sql.="where id='$mID'";	
		db_query($sql);

		/*將領時薪 已經有輸入時數的金額補上去*/
		if($salaryType == 1 && $mCount > 0) {
			doHours($mID,$mCount,$wage);
		}
	}


	//排除舊有留停復職的資料
	if(count($stopEmpList) > 0) {
		$sql="select id from emplyee_salary_report where empID in (".implode(',', $stopEmpList).") and wkMonth='$wkMonth' ";
		$rs=db_query($sql);
		$delID = array();
		if(!db_eof($rs)){
			while ($r=db_fetch_array($rs)) {
				$delID[$r['id']] = "'".$r['id']."'";
			}
			$sql="delete from emplyee_salary_report where id in (".implode(',', $delID).")";
			db_query($sql);
			$sql="delete from emplyee_salary_items where fid in (".implode(',', $delID).")";
			db_query($sql);
		}
		
	}

	
	//::排除已離職人員
	$aDate = $nowY.'-'.str_pad($nowM,2,"0",STR_PAD_LEFT).'-01';
	$sql="select id,empID from emplyee_salary_report where wkMonth='$wkMonth' and empID in(SELECT empID FROM emplyee where isOnduty<>1 and leaveDate<'$aDate')";
	$rs=db_query($sql);
	while($r=db_fetch_array($rs)){
		$rId=$r['id']; $eID=$r['empID'];
		$sql="delete from emplyee_salary_report where empID='$eID' and wkMonth='$wkMonth'"; db_query($sql);
		$sql="delete from emplyee_salary_items where fid=$rId and isExpand=0 ";	db_query($sql);		
	}

	/*連動產生出納作業*/
	$transferData = array(
		'year'=>$nowY,
		'month'=>$nowM,
		'wkMonth'=>$wkMonth
	);
	insertTransfer($transferData);
	
	//::薪資試算產生報表用的文字檔 ----------------------------------------------------------------
	$tree = items2treeN($items); 
	/*echo "<pre>";
	print_r($dep);
	print_r($items);
	print_r($tree);*/

	if($dep=='%' || $dep=='') {
		$object = tree2overtime($tree,$dep);
		$object_absence = tree2absence($tree,$dep);
	} else {
		foreach($tree as $k=>$v) break;
		$object = tree2overtime($v,$dep);
		$object_absence = tree2absence($v,$dep);
	}
	//print_r($object);
	//print_r($object_absence);
	//exit;

	if($dep==""){
		$jsonStr=json_encode($object);
		$jsonStr_absence=json_encode($object_absence);
	}else{
		$jsonStr='{"'.$dep.'":'.json_encode($object).'}';
		$jsonStr_absence='{"'.$dep.'":'.json_encode($object_absence).'}';
	}	

	$path = "../../data/reports";
	if(!is_dir($path))	mkdir($path);
	$path .= '/salary';
	if(!is_dir($path))	mkdir($path);
	$filename=$path."/".$wkMonth.'-overtime.txt';
	$filename_absence=$path."/".$wkMonth.'-absence.txt';

	if(file_exists($filename)){
		$original=file_get_contents($filename);
		$original=json_decode($original);
	}
	if(file_exists($filename_absence)){
		$original_absence=file_get_contents($filename_absence);
		$original_absence=json_decode($original_absence);
	}

	
	// print_r(is_null($original->{"D"}->{"data"}));exit;
	if($dep==""){
		file_put_contents($filename, $jsonStr);
		file_put_contents($filename_absence, $jsonStr_absence);
	}else{
		if(strlen($dep)==1){
			$original->{$dep}=$object;
			$original_absence->{$dep}=$object_absence;
		}else if(strlen($dep)==2){
			$pid=substr($dep,0,1);
			$original->{$pid}->{$dep}=$object;
			$original_absence->{$pid}->{$dep}=$object_absence;
		}else{//ex.C4-001
			$pid1=substr($dep,0,1);
			$pid2=substr($dep,0,2);
			$original->{$pid1}->{$pid2}->{$dep}=$object;
			$original_absence->{$pid1}->{$pid2}->{$dep}=$object_absence;
		}
		$jsonStr=json_encode($original);
		file_put_contents($filename, $jsonStr);
		$jsonStr_absence=json_encode($original_absence);
		file_put_contents($filename_absence, $jsonStr_absence);
	}
	$md5Name=md5($jsonStr);
	$md5Name_absence=md5($jsonStr_absence);
	echo $wkMonth.",".$dep.','.$md5Name.",".$md5Name_absence;
}

function overtimeItem($aType,$hours,$money,$subClass,$addsub){
	global $addTotal,$subTotal;
	//A:前2小時，B:第3-8小時，C:第9-12小時
	if($addsub=="+"){//加項
		if($subClass=="A"){
			$aType->hourA+=$hours;
			$aType->feeA+=$money;
		}else if($subClass=="B"){
			$aType->hourB+=$hours;
			$aType->feeB+=$money;
		}else if($subClass=="C"){ 
			$aType->hourC+=$hours;
			$aType->feeC+=$money;
		}
		$addTotal+=$money;
	}else if($addsub=="-"){//減項
		if($subClass=="A"){
			$aType->hourA-=$hours;
			$aType->feeA-=$money;
		}else if($subClass=="B"){
			$aType->hourB-=$hours;
			$aType->feeB-=$money;
		}else if($subClass=="C"){
			$aType->hourC-=$hours;
			$aType->feeC-=$money;
		}

		$subTotal+=$money;
	}
	return $aType;
}
function absenceItem($abObj,$hours,$money,$addsub,$title){
	global $addTotal,$subTotal;
	$sickAry=array("普通傷病假","安胎假","生理假",'公傷病假','整月請假');
	$unpaidAry=array("事假","防疫照顧假(無薪)","防疫隔離假(無薪)","疫苗接種假(無薪)");
	if($addsub=="+"){//加項
		if(in_array($title, $sickAry)){
			$abObj->sick-=$money;
			$abObj->sick_hours-=$hours;
		}else if(in_array($title, $unpaidAry)){
			$abObj->personal-=$money;
			$abObj->personal_hours-=$hours;
		}
		$addTotal+=$money;
	}else if($addsub=="-"){//減項
		if(in_array($title, $sickAry)){
			$abObj->sick+=$money;
			$abObj->sick_hours+=$hours;
		}else if(in_array($title, $unpaidAry)){
			$abObj->personal+=$money;
			$abObj->personal_hours+=$hours;
		}
		$subTotal+=$money;
	}
	return $abObj;
}
function basic($hireD,$leaveD,$eID,$eData = array()){//::本薪取得
	if(count($eData) == 0) {
		global $nowY,$nowM,$days_in_month,$base_days;
		/*20171208 sean 更改當月條件 改成 當月最後一天*/
		$lastday = date('Y-m-d', strtotime($nowY."-".$nowM."-01 +1 month -1 day"));
		$sql="select salary,salaryType,ratio,hourfee,capacity_ratio,food,extra ";
		$sql.="from emplyee_salary ";
		//$sql.="where empID='$eID' and date(changeDate)<='".$nowY."-".$nowM."-01' ";
		$sql.="where empID='$eID' and date(changeDate)<='".$lastday."'";
		$sql.="order by changeDate DESC ";
		$sql.="limit 0,1 ";
		$rs=db_query($sql);
		if(!db_eof($rs))	$rT = db_fetch_array($rs);	else return array();
		$mBasic = $rT[0]?$rT[0]:0;
		$salary = $rT['salary'];
		$ratio = $rT['ratio'];
		$salaryType = $rT['salaryType'];
		$actual_ratio = doRatio($nowY,$nowM,$days_in_month,$base_days,$hireD,$leaveD,'byDay');
	} else {
		$mBasic = $eData['salary']?$eData['salary']:0;
		$salary = $eData['salary'];
		$ratio = $eData['ratio'];
		$salaryType = $eData['salaryType'];
		$actual_ratio = 1;
	}

	//最高限制為1800元
	//2021最高限制為2400元
	$food = 0; //20190424 sean 新增取得伙食加點
	$maxFood = 2400;	
	$wage=0;		//時薪
	$mCount=1;	//單位
	switch($salaryType){
		case "0"://::月薪	
			//其他加給不要乘工時比例
			$mBasic= (($salary-(int)$rT['extra'])*$ratio+(int)$rT['extra'])*$actual_ratio;
			//$mBasic=$salary*$ratio*$actual_ratio;

			$wage=$salary*$ratio/$base_days/(8*$ratio);
			$food = $rT['food']*30*$ratio*$actual_ratio;
			if($food > $maxFood) $food = $maxFood;
			break;
		case "1"://::時薪
			$mBasic=0;	//薪資試算前預設為0，試算完由人工去補填時數
			$wage=$rT['hourfee'];
			$mCount=0;
			break;
		case "2"://::產能敘薪
			$mBasic=$salary*$ratio*$rT['capacity_ratio']*$actual_ratio;
			$wage=$mBasic/$base_days/(8*$ratio);
			break;
		case "3"://::固定薪 
			$mBasic=$salary*$ratio*$actual_ratio;
			$wage=$salary*$ratio/$base_days/(8*$ratio);
			$food = $rT['food']*$ratio*$actual_ratio;
			if($food > $maxFood) $food = $maxFood;
			break;
	}
	return array($salaryType,$mBasic,$mCount,round($wage),$ratio,$food);
}
function addSub($mID,$eID,$jobInsurence,$healthInsurence,$retireInsurence){//::處理加減項(勞、健、退、津貼…)
	global $nowY,$nowM,$days_in_month,$base_days,$wkMonth, $oEmpList;
	$sql="select *,j.title as jTitle,h.title as hTitle,r.value as rVal ";
	$sql.="from emplyee_insurence as i ";
	$sql.="left join map_job as j on i.job_grades=j.title ";
	$sql.="left join map_health as h on i.health_grades=h.title ";
	$sql.="left join map_retire as r on i.retire_grades=r.value ";
	$sql.="where i.empID='$eID' order by i.id desc limit 1 ";
	$rsT = db_query($sql);//取最新的一筆
	if(!db_eof($rsT)) {
		$rT = db_fetch_array($rsT);
		/* 
		------------------勞保 新算法 要分段計算----------------
		勞保雇主負擔：(投保金額*普通事故9%*70%*工作比例) 
				    +(投保金額*就業保險1%*70%*工作比例) 
				    +(投保薪資*職業費率0.14%*工作比例)

		每個分段都要從小四點第四位 循環的四捨五入
		勞保個人負擔：(投保金額*普通事故9%*20%*工作比例) // 普通事故費
				    +(投保金額*就業保險1%*20%*工作比例) // 就業保險費
					= 一般費用
		如果有輔助 應計勞保費 = 一般費用 - ((普通事故費 * 輔助比例 + 就業保險費* 輔助比例))

		*/
		$max_base_days = ((int)$days_in_month > 30) ? 30 : $days_in_month;
		
		/*勞保除了2月用實際退保日期 其他退保日期最多只能到30號*/
		if(preg_match("/-31/i", $rT['job_edate'])) {
			$rT['job_edate'] = date('Y-m-d', strtotime($rT['job_edate'].' -1 day'));
		}

		$job_ratio=doRatio($nowY,$nowM,$days_in_month,$base_days,$rT['job_bdate'],$rT['job_edate'],'byDay', $max_base_days);
		$jTitle=explode(",",$rT['jTitle']);
		$jTitle=intval(join("",$jTitle));
		
		/*$job_rate=0;
		if($rT["job_accident"])	$job_rate+=floatval($jobInsurence["普通事故費率"])/100;
		if($rT["job_insurance"])	$job_rate+=floatval($jobInsurence["就業保險費率"])/100;
		if($rT["job_disaster"])	$job_disaster=floatval($jobInsurence["職災費率"])/100;
		$job_rate*=(100-floatval($rT['job_rate']))/100;
		// echo 'job_ratio:'.$job_ratio.',jTitle:'.$jTitle.',job_ratio:'.$job_ratio.',job_rate:'.$job_rate."\n";
		$job_mPay = $jTitle* $job_rate * 0.2 * $job_ratio;//勞保自負額
		$job_cPay = (($jTitle * $job_rate * 0.7)+ ($jTitle * $job_disaster)) *$job_ratio;		*/									  //勞保雇主負擔
		$job_mPay = 0;
		if($rT["job_accident"]) $job_mPay += roundEach($jTitle * (floatval($jobInsurence["普通事故費率"]) / 100) * 0.2 * $job_ratio, 4);
		if($rT["job_insurance"]) $job_mPay += roundEach($jTitle * (floatval($jobInsurence["就業保險費率"]) / 100) * 0.2 * $job_ratio, 4);//勞保自負額


		if($rT['job_rate'] > 0) {
			if($rT["job_accident"]) $job_mPay -= roundEach($jTitle * (floatval($jobInsurence["普通事故費率"]) / 100) * 0.2 * $job_ratio * ($rT['job_rate']/100) , 4);
			if($rT["job_insurance"]) $job_mPay -= roundEach($jTitle * (floatval($jobInsurence["就業保險費率"]) / 100) * 0.2 * $job_ratio * ($rT['job_rate']/100), 4);
		}

		$job_cPay = 0;
		if($rT["job_accident"]) $job_cPay += round($jTitle * (floatval($jobInsurence["普通事故費率"]) / 100) * 0.7 * $job_ratio);
		if($rT["job_insurance"]) $job_cPay += round($jTitle * (floatval($jobInsurence["就業保險費率"]) / 100) * 0.7 * $job_ratio);
		if($rT["job_disaster"]) $job_cPay += round($jTitle * (floatval($jobInsurence["職災費率"])/100) * $job_ratio);//勞保雇主負擔

		//echo 'job_mPay:'.$job_mPay.',jTitle:'.$jTitle.",job_rate:".$job_rate."\n";
		$sql = "insert into emplyee_salary_items(fid,title,addsub,money,iType,wkMonth) values($mID,'勞保自負額','-',$job_mPay,'insurence','$wkMonth')";
		// echo $sql."\n";
		db_query($sql);

		
		/* -------------- 健保 -------------
		健保雇主負擔：(投保金額*健保費率4.69%*本人及平均眷口數1.61)*60%
		健保個人負擔：(投保金額*健保費率4.69%)*30%

		每一段都要4捨5入
		*/
		$avg_feed_nums=1.61; //本人及平均眷口數
		$health_ratio=doRatio($nowY,$nowM,$days_in_month,$base_days,$rT['health_bdate'],$rT['health_edate'],'byMonth');
		// echo 'health_ratio:'.$health_ratio."\n";
		$hTitle=explode(",",$rT['hTitle']);
		$hTitle=intval(join("",$hTitle));
		$health_rate=$rT['health_rate'];

		$hMpay = $hTitle * floatval($healthInsurence["健保費率"]/100) * 0.3;
		$health_mPay = round($hMpay);
		$health_mPay -= ceil($hMpay * ($health_rate/100)); //健保自負額
		$health_mPay *= $health_ratio;
		$health_mPay = ($health_mPay < 0) ? 0 : $health_mPay; 

		$health_cPay = round($hTitle * floatval($healthInsurence["健保費率"]/100) * $avg_feed_nums * 0.6 * $health_ratio); //健保雇主負擔
		
		
		//$health_mPay = round($hTitle * floatval($healthInsurence["健保費率"]/100) * ((100-intval($health_rate))/100) * 0.3 * $health_ratio);	//健保自負額
		//$health_cPay = round($hTitle * floatval($healthInsurence["健保費率"]/100) * $avg_feed_nums * 0.6 * $health_ratio);      					//健保雇主負擔
		// echo 'hTitle:'.$hTitle.','.(floatval($healthInsurence["健保費率"]/100)).',health_mPay:'.$health_mPay.",health_cPay:".$health_cPay."\n";
		//加入眷屬健保費
		$bDate = $nowY.'-'.str_pad($nowM,2,"0",STR_PAD_LEFT).'-'.$days_in_month;
		$sql="select * from emplyee_dependents where empID='".$eID."' and bDate <= '".$bDate."' and edate='0000-00-00' ";
		 //echo $sql."\n";
		
		$rs=db_query($sql);
		///$h_cPay_count=0;
		if(!db_eof($rs)){
			while($r=db_fetch_array($rs)){
				if($r["health_rate"]>0){	
					$health_ratio=doRatio($nowY,$nowM,$days_in_month,$base_days,$r['bdate'],$r['edate']);
				}

				$health_rate=$r['health_rate'];					
				// echo 'feed mPay:'.($hTitle * floatval($healthInsurence["健保費率"]/100) * ((100-intval($health_rate))/100) * 0.3 * $health_ratio)."\n";	
				$_hMpay = $hTitle * floatval($healthInsurence["健保費率"]/100) * 0.3;
				$_mPay = round($_hMpay);
				$_mPay -= ceil($_hMpay * ($health_rate/100)); //健保自負額
				$_mPay *= $health_ratio;

				$_mPay = ($_mPay < 0) ? 0 : $_mPay; 

				$health_mPay += $_mPay;	//健保自負額." ,health_rate:".$health_rate."\n";
			}  		
		}

		$health_cPay += $health_cPay*$h_cPay_count;  //健保雇主負擔

		if(isset($oEmpList[$eID])) {
			$health_mPay = 0;
			$health_cPay = 0;
		}


		$sql = "insert into emplyee_salary_items(fid,title,addsub,money,iType,wkMonth) values($mID,'健保自負額','-',$health_mPay,'insurence','$wkMonth')";
		// echo $sql."\n";
		db_query($sql);
		
		/* -------------- 勞退 ------------- */
		/*勞退除了2月用實際退保日期 其他退保日期最多只能到30號*/
		if(preg_match("/-31/i", $rT['mRetire_edate'])) {
			$rT['mRetire_edate'] = date('Y-m-d', strtotime($rT['mRetire_edate'].' -1 day'));
		}
		if(preg_match("/-31/i", $rT['retire_edate'])) {
			$rT['retire_edate'] = date('Y-m-d', strtotime($rT['retire_edate'].' -1 day'));
		}
		//echo 'mRetire_bdate:'.$rT['mRetire_bdate'].",mRetire_edate:".$rT['mRetire_edate']."\n";

		//已過投保日
		if($rT['mRetire_edate'] != '' && $rT['mRetire_edate'] != '0000-00-00' && strtotime($rT['mRetire_edate']) < strtotime($nowY.'-'.$nowM.'-01')) {
			$retire_mPay = 0;
		} else {
			$mRetire_ratio=doRatio($nowY,$nowM,$days_in_month,$base_days,$rT['mRetire_bdate'],$rT['mRetire_edate'],'byDay', $max_base_days);
			$retire_mPay = round((intval($rT['rVal'])*intval($rT['retire_rate'])/100) * $mRetire_ratio);	//勞退自負額
		}

		$cRetire_ratio=doRatio($nowY,$nowM,$days_in_month,$base_days,$rT['retire_bdate'],$rT['retire_edate'],'byDay', $max_base_days);
		$retire_cPay =round(intval($rT['rVal'])* 0.06 * $cRetire_ratio);								//勞退雇主負擔



		/*$sql = "update emplyee_salary_report set retireCPay='".$retire_cPay."' where  id='".$mID."'";
		db_query($sql);*/
		// echo 'rVal:'.$rT['rVal'].',mRetire_ratio:'.$mRetire_ratio.",retire_mPay:".$retire_mPay.",cRetire_ratio:".$cRetire_ratio.",retire_cPay:".$retire_cPay."\n";
		$sql = "insert into emplyee_salary_items(fid,title,addsub,money,iType,wkMonth) values($mID,'勞退自提額','-',$retire_mPay,'insurence','$wkMonth')";
		db_query($sql);
	}
	return array($job_mPay,$job_cPay,$health_mPay,$health_cPay,$retire_mPay,$retire_cPay);
}
function absence($mID,$eID,$wage,$mRatio=1){//::掃請假單，根據不同的假種扣薪
	global $nowY,$nowM,$wkMonth;
	//::全年病假超過30天的部分必須扣全薪，所以先計算本月之前累積的天時
	$maxHours=30*8*$mRatio;  //工時比例
	$sql="select sum(hours) as sumHours from absence where empID='$eID' and YEAR(bDate)='$nowY' and MONTH(bDate)<'$nowM' and isOK=1 and isDel=0 and aType in ('普通傷病假','安胎假','生理假')";
	$rs=db_query($sql);
	// if($eID=="01488")	echo $sql."\n";
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$sumHours=$r['sumHours'];
	}

	//整月普通病假
	$sql="select sum(hours) as sumHours from absence where empID='$eID' and YEAR(bDate)='$nowY' and MONTH(bDate)='$nowM' and isOK=1 and isDel=0 and aType in ('普通傷病假','生理假')";
	$rs=db_query($sql);
	$sickTotal = 0;
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$sickTotal=$r['sumHours'];
	}

	$sql="select sum(hours) as sumHours from absence where empID='$eID' and YEAR(bDate)='$nowY' and MONTH(bDate)='$nowM' and isOK=1 and isDel=0 and aType in ('事假','防疫照顧假(無薪)','防疫隔離假(無薪)','疫苗接種假(無薪)','公傷病假','安胎假','天災未出勤')";
	$rs=db_query($sql);
	$leaveTotal = 0;
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$leaveTotal=$r['sumHours'];
	}

	//判斷是否為整月請假
	if(($leaveTotal+$sickTotal) >= $maxHours) {
		if(($sickTotal > 0 && $sumHours >= $maxHours) || ($sickTotal == 0)) {
			return ($leaveTotal+$sickTotal);
		}
	}

	$sql="select * from absence where empID='$eID' and YEAR(bDate)='$nowY' and MONTH(bDate)='$nowM' and isOK=1 and isDel=0 ";
	// if($eID=="01488")	echo $sql."\n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$hours=$r[hours];
			$days=$hours/24;
			$sub=0;
			switch($r['aType']){
				case "普通傷病假"://給假期間,工資折半。
					if($days>30){//逾三十天，給假期間,工資不給
						$sub=(30*8)*$wage/2; 
						$hours=$hours-(30*24);
						$d=floor($hours/24);//超過幾天
						$sub+=$d*8*$wage;	
						//$h=$hours%24;		//又超過幾小時
						$h=$hours-($d*24);

						$sub+=$h*$wage;	
					}else{
						if($sumHours>$maxHours){//既有的時數已超過30天
							$sub=$hours*$wage;	
						}else if(($sumHours+$hours)>=$maxHours){//部分超過30天
							$sub=($maxHours-$sumHours)*$wage/2;	  	
							// if($eID=="01488") echo $sub."\n";
							$sub+=($sumHours+$hours-$maxHours)*$wage;
							// if($eID=="01488") echo $sub."\n";
						}else{
							$sub=$hours*$wage/2;	  	
						}
					}
					$sumHours+=$hours;
					break;
				case "安胎假":
				//case "產檢假":
				case "生理假"://給假期間,工資折半。逾三十三天，給假期間,工資不給。
					if($days>33){
						$sub=(33*8)*$wage/2; 
						$hours=$hours-(33*24);
						$d=floor($hours/24);//超過幾天
						$sub+=$d*8*$wage;	
						//$h=$hours%24;		//又超過幾小時
						$h=$hours-($d*24);
						$sub+=$h*$wage;	
					}else{
						if($sumHours>$maxHours){//既有的時數已超過30天
							$sub=$hours*$wage;	
						}else if(($sumHours+$hours)>=$maxHours){//部分超過30天
							$sub=($maxHours-$sumHours)*$wage/2;	  	
							// if($eID=="01488") echo $sub."\n";
							$sub+=($sumHours+$hours-$maxHours)*$wage;
							// if($eID=="01488") echo $sub."\n";
						}else{
							$sub=$hours*$wage/2;	  	
						}
					}
					$sumHours+=$hours;
					break;
				/*case "公傷病假":
					$d=floor($hours/24);
					//$h=$hours%24;
					$h=$hours-($d*24);
					$sub=($d*8+$h)*$wage;//薪資不給，但給補償金
					if($days<=3){//前三日給日薪全額的補償
						$sub2=($wage*$d*8)+($wage*$h);
					}else if($days>3 && $days<=365){//第四日起，給30%日薪的補償金
						$sub2=$wage*3*8;
						$sub2+=(($wage*($d-3)*8)+($h*$wage))*0.3;
					}else{//第二年起，給50%日薪的補償金。
						$sub2=$wage*3*8;
						$sub2+=$wage*(365-3)*8*0.3;
						$sub2+=(($wage*8*($d-365))+($h*$wage))*0.5;
					}
					$sql = "insert into emplyee_salary_items(fid,title,addsub,money,iType,hours,wkMonth) values($mID,'".$r['aType']." - 補償金','+',$sub2,'absence',$hours,'$wkMonth')"; 
					db_query($sql);
					break;*/
				case "公傷病假" :
				case "天災未出勤" :					
				case "事假": //給天，給假期間,工資不給	
				case "防疫照顧假(無薪)":
				case "防疫隔離假(無薪)":
				case "疫苗接種假(無薪)":
					$sub=$hours*$wage;
					break;	
			}
			if($sub>0) {
				$sql = "insert into emplyee_salary_items(fid,title,addsub,money,iType,hours,wkMonth) values($mID,'".$r['aType']."','-',$sub,'absence',$hours,'$wkMonth')";
				db_query($sql);
			}
		}
	}
	return 0;
}

function overtime($mID,$eID,$wage,$wkMonth,$depID){//掃emplyee_abNormal：異常項目，要加減項
	global $nowY,$nowM;//,$otObj;
	$sql="delete from emplyee_salary_items where empID='$eID' and wkMonth='".$wkMonth."' and iType='overtime' ";//清除該員未處理的項目 ex.加班
	// echo $sql."\n";
	db_query($sql);
	$today=date('Y-m-d');
	$sql="select * from overtime ";
	$sql.="where YEAR(bDate)=".$nowY." and MONTH(bDate) = ".$nowM." and bDate<'".$today."' ";
	$sql.="and isOK=1 and empID='".$eID."' and isDel=0 and fmAct=1 ";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$subClass='';
		while($r=db_fetch_array($rs)){
			switch($r[aType]){
				case "0":	
					$abStatus='加班-上班日';	
					if($r['hours']<=2){
						$subClass='A';
						$feeA=round($wage*1.34*$r['hours']);
						addOvertime($mID,$abStatus,$feeA,$r[hours],$subClass,$wkMonth,"");
					}else if($r['hours']<=8){
						$subClass='B';
						$feeA=round($wage*1.34*2);
						$feeB=round($wage*1.67*($r[hours]-2));
						addOvertime($mID,$abStatus,$feeA,2,"A",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeB, ($r[hours]-2),"B",$wkMonth,"");

					}else if($r['hours']<=12){
						$subClass='C';
						$feeA=round($wage*1.34*2);
						$feeB=round($wage*1.67*6);
						$feeC=round($wage*2.67*($r['hours']-8));
						addOvertime($mID,$abStatus,$feeA,2,"A",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeB,6,"B",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeC, ($r[hours]-8),"C",$wkMonth,"");
					}	
					break;
				case "1":	
					$abStatus='加班-休息日';	
					if($r['hours']<=2){
						$subClass='A';
						$feeA=round($wage*1.34*$r['hours']);
						addOvertime($mID,$abStatus,$feeA,$r[hours],$subClass,$wkMonth,"");
					}else if($r['hours']<=8){
						$subClass='B';
						$feeA=round($wage*1.34*2);
						$feeB=round($wage*1.67*($r[hours]-2));
						addOvertime($mID,$abStatus,$feeA,2,"A",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeB, ($r[hours]-2),"B",$wkMonth,"");
					}else if($r['hours']<=12){
						$subClass='C';
						$feeA=round($wage*1.34*2);
						$feeB=round($wage*1.67*6);
						$feeC=round($wage*2.67*($r['hours']-8));
						addOvertime($mID,$abStatus,$feeA,2,"A",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeB,6,"B",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeC, ($r[hours]-8),"C",$wkMonth,"");
					}	
					break;
				case "2":	
					$abStatus='加班-國定假日';
					if($r['hours']<=8){
						if($r[hours]<=2){
							$feeA=round($wage*1*$r[hours]);
							$subClass='A';
							addOvertime($mID,$abStatus,$feeA,$r[hours],$subClass,$wkMonth,"");
						}else{
							$subClass='B';
							$feeA=round($wage*1*2);
							$feeB=round($wage*1*($r[hours]-2));
							addOvertime($mID,$abStatus,$feeA,2,"A",$wkMonth,"");
							addOvertime($mID,$abStatus,$feeB, ($r[hours]-2),"B",$wkMonth,"");
						}
					}else if($r['hours']<=12){
						$subClass='C';
						$feeA=round($wage*1*2);
						$feeB=round($wage*1*6);
						$feeC=round($wage*1.67*($r['hours']-8));
						addOvertime($mID,$abStatus,$feeA,2,"A",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeB,6,"B",$wkMonth,"");
						addOvertime($mID,$abStatus,$feeC, ($r[hours]-8),"C",$wkMonth,"");
					}
					break;
				case "3":
					$abStatus='加班-天災、選舉日';
					$feeA = $feeA=round($wage*1*$r[hours]);
					addOvertime($mID,$abStatus,$feeA,$r[hours],"A",$wkMonth,"");
					break;
				case "4":
					$abStatus='津貼-部分工時增加工時';
					$feeA = $feeA=round($wage*1*$r[hours]);
					$sql = "insert into emplyee_salary_items(fid,title,addsub,money,iType,hours,wkMonth) values(".$mID.",'".$abStatus."','+','".$feeA."','allowance','".$r[hours]."','".$wkMonth."')"; 
					// echo $sql."\n";
					db_query($sql);
					break;
			}
		}
	}
}
function addOvertime($mID,$abStatus,$fee,$abHours,$subClass,$wkMonth,$notes){
	$sql="insert into emplyee_salary_items (fid,title,addsub,money,iType,hours,subClass,wkMonth,notes) ";
	$sql.="values(".$mID.",'".$abStatus."','+',".$fee.",'overtime',$abHours,'$subClass','$wkMonth','$notes')";
	// echo $sql."\n";
	db_query($sql);
}
function allowance($mID,$eID,$wkMonth){//津貼
	$sql="select * from emplyee_allowance where empID='".$eID."' and wkMonth='".$wkMonth."'";
	// echo "@allowance \n";
	// echo $sql."\n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$add=$r['unitPrice']*$r['qty'];
			$sql = "insert into emplyee_salary_items(fid,title,addsub,hours,money,iType,wkMonth) values($mID,'".$r['awcTitle']."-".$r['qty'].$r['unitName']."','+',$r[qty],$add,'allowance','$wkMonth')"; 
			// echo $sql."\n";
			db_query($sql);
		}
	}
}
function otherfee($mID,$eID,$wkMonth){//雜項費用
	$sql="select * from emplyee_otherfee where empID='".$eID."' and wkMonth='".$wkMonth."'";
	// echo "@otherfee \n";
	// echo $sql."\n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$sub=$r['unitPrice']*$r['qty'];
			$sql = "insert into emplyee_salary_items(fid,title,addsub,hours,money,iType,wkMonth) values($mID,'".$r['ofcTitle']."-".$r['qty'].$r['unitName']."','-',$r[qty],$sub,'otherfee','$wkMonth')"; 
			// echo $sql."\n";
			db_query($sql);
		}
	}
}
function doRatio($nowY,$nowM,$days_in_month,$base_days,$startDate,$endDate,$type, $max_base_days=0){ //檢查到職、離職日，判斷滿月/未滿月
	$sTime=strtotime($startDate)?strtotime($startDate):0;
	$sDate=getdate(strtotime($startDate));

	if(date('Y-m',strtotime($endDate)) != date('Y-m',strtotime($nowY."-".$nowM))) { //排除非當月離職
		$endDate = null;
	}

	$eDate=getdate(strtotime($endDate));
	$s_mday=$sDate['mday'];
	$e_mday=$eDate['mday'];
	$ratio=1;

	if($max_base_days == 0) $max_base_days = $days_in_month;
	/*20200203 sean 設定 $max_base_days 最小為30*/
	if($max_base_days < 30) $max_base_days = 30;
	// echo "@doRatio startDate:".$startDate.",endDate:".$endDate.',type:'.$type."\n";
	if($type=="byDay"){//::按日算
		if(strtotime($endDate) && strtotime($endDate) > 0){//有離職日
			if(strtotime($nowY."-".$nowM."-01")<=$sTime /*&& strtotime($endDate)<=strtotime($nowY."-".$nowM."-".$days_in_month)*/){//未滿月、到職未滿月內離職
				if((int)$eDate['mon'] == 2 && $e_mday == $days_in_month) $e_mday = 30; //20200804如果是大月或是2月都用30天當工作天
				$ratio=($e_mday-$s_mday+1)/$base_days;
				$ratio>1?$ratio=1:$ratio=$ratio;
			}else if(strtotime($nowY."-".$nowM."-01")>$sTime && strtotime($endDate)<strtotime($nowY."-".$nowM."-".$days_in_month)){//離職當月未滿月
				$ratio=($e_mday)/$base_days;
			}
		}else{//沒有離職日
			if($sDate['year']==$nowY && $sDate['mon']==$nowM){//未滿月
				$ratio=($max_base_days-$s_mday+1)/$base_days;
				$ratio>1?$ratio=1:$ratio=$ratio;
			}else{
				$ratio=1;
			}
		}
	}else if($type=='byMonth'){//::按月算
		if(strtotime($endDate) && strtotime($endDate) > 0){//有離職日
			if(strtotime($endDate) < strtotime($nowY."-".$nowM."-".$days_in_month)) { //只要離職當月 退保(離職)日 不在月底最後一天 工作比例就為0
				$ratio=0;
			}
			/*if(strtotime($nowY."-".$nowM."-01")<=$sTime && strtotime($endDate)<=strtotime($nowY."-".$nowM."-".$days_in_month)){//到職未滿月內離職
				$ratio=0;
			}else if(strtotime($nowY."-".$nowM."-01")>$sTime && strtotime($endDate)<strtotime($nowY."-".$nowM."-".$days_in_month)){//離職當月未滿月
				$ratio=0;
			}*/
		} //未滿月且沒有離職日 $ratio=1; 不另外判斷
		/*else if(!strtotime($endDate) && $sDate['year']==$nowY && $sDate['mon']==$nowM){//未滿月且沒有離職日
			$ratio=1;
		}*/
	}
	return $ratio;
}


function insertTransfer($data = array()) {
	$sql = "select id from transfer where type='薪資' and pID='".$data['wkMonth']."'";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
	if(!empty($r['id'])) {
		$tID = $r['id'];
		$sql = "delete from emplyee_transfer where tID='".$tID."'";
		db_query($sql);
		$sql = "delete from transfer where id='".$tID."'";
		db_query($sql);
	}

	$insertData = array(
		"`type`='薪資'",
		"`pID`='".$data['wkMonth']."'",
		"`year`='".$data['year']."'",
		"`month`='".$data['month']."'",
		"`title`='".$data['wkMonth']."薪資'"
	);
	$sql = "insert into transfer set ".implode(',', $insertData);
	db_query($sql);

	$tID = mysql_insert_id();//新增的id

	$payType = array('1'=>'轉入帳戶','2'=>'領現','3'=>'匯款');

	$sql = "select r.*,e.salaryAct,e.payType from emplyee_salary_report r inner join emplyee e on r.empID=e.empID where r.wkMonth='".$data['wkMonth']."' and e.empID <>'99998' order by e.empID ";
	$rs = db_query($sql);



	while ($r = db_fetch_array($rs)) {
		$fields = array();
		$fields[] = "`empID`='".$r['empID']."'";
		$fields[] = "`tID`='".$tID."'";
		$fields[] = "`year`='".$data['year']."'";
		$fields[] = "`title`='".$data['wkMonth']."薪資'";
		$fields[] = "`identifier`='1'";
		$fields[] = "`class`='A2'";
		$fields[] = "`loan`='C'";
		$fields[] = "`inAccount`='".$r['salaryAct']."'";
		$fields[] = "`money`='".$r['payTotal']."'";
		$fields[] = "`payType`='".$payType[$r['payType']]."'";
		$sql = "insert into emplyee_transfer set ".implode(',', $fields);
		db_query($sql);
	}
}	

function roundEach($val, $num) {
	$num -= 1;
	$resutl = $val;
	for($i=$num; $i>=0; $i--) $resutl = round($resutl,$i);
	return $resutl;
}
?>