<?php
	@session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
  }
  
  include "../../config.php";
	include('../../getEmplyeeInfo.php');	

  if($_REQUEST['doFix']) {
    include '../../emplyee/absence/func.php'; 
    $id = $_REQUEST['id'];
    if($_REQUEST['force']){
      do_absence_overtime2($id);
    } else do_absence_overtime($id);
    echo "<script>alert('作業結束!')</script>";
  }

  $y = $_REQUEST['yy']?$_REQUEST['yy']:Date('Y');
  //補休
  $sql = "select * from absence where id not in (SELECT abId FROM `absence_overtime`) and aType='補休' and  isOK=1 and isReqD=0 and bDate between '$y-1-1' and '$y-12-31' order by empID";
  $rs = db_query($sql);
  $yy = intval(Date('Y'));
?>
<html>
<head>
<meta charset="utf-8">
<title>加班-補休 對應修正</title>
<style>
  .cap {
    background-color: #666;
    color: #fff;
    font-size: 18px;
    font-weight: bold;
    height: 24px;
    padding-top: 5px;
  }
  select {
    font-size: 18px;
  }
  button {
    margin-right: 5px;
  }
</style>
<script>
  function doFix(id) { window.location.href='fixAbsent.php?doFix=1&yy=<?=$y?>&id='+id; }
  function doFix2(id) { window.location.href='fixAbsent.php?doFix=1&force=1&yy=<?=$y?>&id='+id; }
</script>
</head>

<body>
<h2>
  <form name="form1">
  <select name="yy" onChange="this.form.submit()">
  <? for($i=$yy; $i>$yy-3; $i--) if($i==$y) echo "<option selected>$i</option>"; else echo "<option>$i</option>"; ?>
  </select> 異常補休單
  </form>  
</h2>  
<?php 
  $empID = ''; 
  while($r=db_fetch_array($rs)) {
    if($empID!=$r['empID']) {
      if($empID) {
        echo "</table>";
        echo "<table border='1' cellspacing='0' cellpadding='4'>";
        $sql = "select * from overtime where id not in (SELECT otId FROM `absence_overtime`) and fmAct=0 and  isOK=1 and isReqD=0 and bDate between '$y-1-1' and '$y-12-31' and empID='$empID'"; //加班單
        //var_dump($sql);
        $rs2 = db_query($sql);
        echo "<tr bgcolor='#ccccff'><td>加班單號</td><td>起始日期</td><td>結束日期</td><td>時數</td></tr>"; 
        while($r2=db_fetch_array($rs2)) echo "<tr bgcolor='#eeeeff'><td>$r2[id]</td><td>$r2[bDate]</td><td>$r2[eDate]</td><td>$r2[hours]</td></tr>"; 
        echo "</table></div><br/>";
      }
      $empID = $r['empID'];
      echo "<div>";
      echo "<table border='1' cellspacing='0' cellpadding='4'>";
      echo "<caption class='cap'>".$emplyeeinfo[$r['empID']]." ($empID)</caption>";
      echo "<tr bgcolor='#cccccc'><td>補休單號</td><td>起始日期</td><td>結束日期</td><td colspan='2'>時數</td></tr>"; 
      echo "<tr><td>$r[id]</td><td>$r[bDate]</td><td>$r[eDate]</td><td>$r[hours]</td><td><button onClick='doFix($r[id])'>合規修正</button><button onClick='doFix2($r[id])'>強制修正</button></td></tr>"; 
    } else {
      echo "<tr><td>$r[id]</td><td>$r[bDate]</td><td>$r[eDate]</td><td>$r[hours]</td><td><button onClick='doFix($r[id])'>合規修正</button><button onClick='doFix2($r[id])'>強制修正</button></td></tr>";
    }
  }
  
  if($empID) {
    echo "</table>";
    echo "<table border='1' cellspacing='0' cellpadding='4'>";
    $sql = "select * from overtime where id not in (SELECT otId FROM `absence_overtime`) and fmAct=0 and  isOK=1 and isReqD=0 and bDate between '$y-1-1' and '$y-12-31' and empID='$empID'"; //加班單
    $rs2 = db_query($sql);
    echo "<tr bgcolor='#ccccff'><td>加班單號</td><td>起始日期</td><td>結束日期</td><td>時數</td></tr>"; 
    while($r2=db_fetch_array($rs2)) echo "<tr bgcolor='#eeeeff'><td>$r2[id]</td><td>$r2[bDate]</td><td>$r2[eDate]</td><td>$r2[hours]</td></tr>"; 
    echo "</table></div><br/>";
  }
?>
</body>
</html>