<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	include('../../getEmplyeeInfo.php');	
	$tabidx = $_SESSION['emplyee_tab']?$_SESSION['emplyee_tab']:0;
	$empID = $_REQUEST[empID];
	if($empID) { //修改
		$actfn = 'doedit.php?keyword='.$_REQUEST['keyword'].'&depFilter='.$_REQUEST['depFilter'].'&sttFilter='.$_REQUEST['sttFilter'].'&jobFilter='.$_REQUEST['jobFilter'].'&typFilter='.$_REQUEST['typFilter'];
		$rs = db_query("select * from emplyee where empID='$empID'");
		if($rs) $r=db_fetch_array($rs);
	} else { //新增
		$actfn = 'doappend.php?keyword='.$_REQUEST['keyword'].'&depFilter='.$_REQUEST['depFilter'].'&sttFilter='.$_REQUEST['sttFilter'].'&jobFilter='.$_REQUEST['jobFilter'].'&typFilter='.$_REQUEST['typFilter'];
	}

  $ary=array();
  array_push($ary, "fid=".$_REQUEST['fid']);
  array_push($ary, "year=".$_REQUEST['year']);
  array_push($ary, "month=".$_REQUEST['month']);
  array_push($ary, "depID=".$_REQUEST['depID']);
  array_push($ary, "empID=".$_REQUEST['empID']);
  array_push($ary, "detail=Y");
  array_push($ary, "wage=".$_REQUEST['wage']);
  $params=join("&",$ary);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>

  
	function formvalid(tfm) {
    if(tfm.isDonate.checked){
        if(tfm.donate.value<=0) {
          alert('捐款金額必須大於0');
          return false;
        } 
    }
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	function setnail(n) {
		$('.tabimg').attr('src','../images/unnail.gif');
		$('#tab'+n).attr('src','../images/nail.gif');
		$.get('setnail.php',{'idx':n});
	}
  function runScript(e) {
    if (e.keyCode == 13) {
      return false;
    }
  }

  function isDel(empID,cardNo,type){
    if(confirm('確定要變更嗎?')){
      //版本問題只能用這種的
      $.ajax({
        method: "POST",
        url: "api.php",
        data: { 'empID': empID,'type':type,'cardNo':cardNo},
        success: function(){
          // console.log('jo');
          location.reload();
        }
      })
    }
    
  }
	</script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
.iframe{ z-index:-1;}
.iframe2{ z-index:9999;}
.card td,th{
  padding:3px;
}
.card th{
  background-color: #cd9696;
}
</style>
</head>

<body>
<form action="<?=$actfn?>" method="post" target="_parent" style="margin:0" onsubmit="return formvalid(this)">
<table width="100%" border="0">
  <tr>
    <td>
    <div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0">勞健退<img id="tab0" class="tabimg" src="../images/<?= $tabidx==0?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(0)" /></li>
        
        <li class="TabbedPanelsTab" tabindex="1">加班費<img id="tab1" class="tabimg" src="../images/<?= $tabidx==1?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(1)" /></li>
        <li class="TabbedPanelsTab" tabindex="2">出缺勤<img id="tab2" class="tabimg" src="../images/<?= $tabidx==2?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(2)" /></li>
        <li class="TabbedPanelsTab" tabindex="3">津貼<img id="tab3" class="tabimg" src="../images/<?= $tabidx==3?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(3)" /></li>
        <li class="TabbedPanelsTab" tabindex="4">雜項費用<img id="tab4" class="tabimg" src="../images/<?= $tabidx==4?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(4)" /></li>
       <!-- <li class="TabbedPanelsTab" tabindex="5">加減項<img id="tab5" class="tabimg" src="../images/<?= $tabidx==5?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(5)" /></li>-->
        <? if($_REQUEST['p2']) { ?>
        <li class="TabbedPanelsTab" tabindex="5">未補休加班單<img id="tab5" class="tabimg" src="../images/<?= $tabidx==5?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(5)" /></li>
        <li class="TabbedPanelsTab" tabindex="6">未休特休<img id="tab6" class="tabimg" src="../images/<?= $tabidx==6?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(6)" /></li>
        <? } ?>  
      </ul>
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="../emplyee_salary_items/list.php?type=insurence&<?=$params?>"></iframe>
        </div>
        <div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="../emplyee_salary_items/list.php?type=overtime&<?=$params?>"></iframe>
        </div>
         <div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="../emplyee_salary_items/list.php?type=absence&<?=$params?>"></iframe>
        </div> 
         <div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="../emplyee_salary_items/list.php?type=allowance&<?=$params?>"></iframe>
        </div> 
         <div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="../emplyee_salary_items/list.php?type=otherfee&<?=$params?>"></iframe>
        </div> 
        <!--div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="../emplyee_salary_items/list.php?type=addsub&<?=$params?>"></iframe>
        </div--> 
        <? if($_REQUEST['p2']) { ?>
        <div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="overdue2.php?<?=$params?>"></iframe>
        </div> 
        <div class="TabbedPanelsContent">
          <iframe width="100%" height="210" frameborder="0" src="../absence/annualList.php?<?=$params?>"></iframe>
        </div> 
        <? } ?>
      </div>
    </div>
    </td>
  </tr>
</table>
</form>
</body>
<script type="text/javascript">var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");TabbedPanels1.showPanel(<?=$tabidx?>);</script>
</html>