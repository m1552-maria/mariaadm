<?php
	@session_start();
	header("Content-type: text/html; charset=utf-8");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=".date("YmdHis").".xlsx");
	header("Pragma:no-cache");
	header("Expires:0");
	//include class
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../inc_vars.php';
	include 'Fee.php';
	require_once '../Classes/PHPExcel.php';
	require_once '../Classes/PHPExcel/IOFactory.php';

	$base_days=30;	//大小月都以30天去計箅實際工作比例
		
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);
	$sheet = $objPHPExcel->getActiveSheet();

	$head = array(
		'A'=>array('width'=>10,'val'=>'序號'),
		'B'=>array('width'=>10,'val'=>'加班單號'),
		'C'=>array('width'=>10,'val'=>'員工名稱'),
		'D'=>array('width'=>12,'val'=>'加班別'),
		'E'=>array('width'=>19,'val'=>'時間起'),
		'F'=>array('width'=>19,'val'=>'時間迄'),
		'G'=>array('width'=>12,'val'=>'加班時數'),
		'H'=>array('width'=>12,'val'=>'未補休時數'),
		'I'=>array('width'=>12,'val'=>'時薪'),
		'J'=>array('width'=>12,'val'=>'加班費'));
	$i = 1;
	foreach ($head as $key => $value) {
		$sheet->getColumnDimension($key)->setWidth($value['width']);
		$sheet->setCellValue($key.$i, $value['val']);
	}


	//$depID = $_POST['depSql'];
	/*20180323 sean 加入前後台顯示部門權限*/
	if($_POST['depSql'] != ''){
		$temp[] = "e.depID like '".$_POST['depSql']."%'";
	}else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v){
				if($v)	$aa[]="e.depID like '$v%'";	
			}
			$temp[] = "(".join(' or ',$aa).")"; 	
		} else {
			$temp[] = "e.depID=''";
		}
	}

	$filter = (count($temp)>0) ? implode(' and ',$temp).' and ' : '';
	$wkY = $_POST['yearSql']?$_POST['yearSql']:date('Y');
	$dateB = "$wkY-01-01";
	$dateE = "$wkY-12-31";	
	$sql="select o.*,e.depID from overtime o left join emplyee e on o.empID=e.empID "
		."where ".$filter." isOK=1 and isDel=0 and fmAct=0 and bDate between '$dateB' and '$dateE' order by empID,bDate";
	$rsX = db_query($sql);
	$aa = array();	//一維

	while($rX = db_fetch_array($rsX)){ //加班單迴圈
		$id = $rX['id'];
		$ba = array();
		$ba['empID'] = $rX['empID'];
		$ba['depID'] = $rX['depID'];
		$ba['bDate'] = $rX['bDate'];
		$ba['eDate'] = $rX['eDate'];
		$ba['aType'] = $rX['aType'];
		$ba['hours'] = $rX['hours'];	//加班時數
		$ba['hourfee'] = getWage($rX['empID'], date('Y-m-d',strtotime($rX['bDate'])) );
				
		
		$sql = "select * from absence_overtime where otId=$id";
		$rsX2=db_query($sql);
		if(!db_eof($rsX)){ //有沖銷，檢查沖銷餘額
			$v = 0;
			while($rX2=db_fetch_array($rsX2))	$v += $rX2['hours'];
			$ba['hours2'] = $rX['hours']-$v;		//未沖銷時數
		} else { //未沖銷
			$ba['hours2'] = $rX['hours'];
		}
		$aa[$id] = $ba;
	}
	$idNum = 1; //序號
	foreach ($aa as $key => $value) {
		if($value['hours2']==0) continue;
		$i++;
		$feeAll = overtimeFee($value['aType'],$value[hourfee],$value[hours]);
		$feeUse = overtimeFee($value['aType'],$value[hourfee],$value[hours]-$value[hours2]);
		$fee = $feeAll-$feeUse;
			
		$sheet->setCellValue('A'.$i, $idNum);
		$sheet->setCellValue('B'.$i, $key);
		$sheet->setCellValue('C'.$i, $emplyeeinfo[$value['empID']]);
		$sheet->setCellValue('D'.$i, $overtimeType[$value['aType']]);
		$sheet->setCellValue('E'.$i, $value['bDate']);
		$sheet->setCellValue('F'.$i, $value['eDate']);
		$sheet->setCellValue('G'.$i, $value['hours'].'小時');
		$sheet->setCellValue('H'.$i, $value['hours2'].'小時');
		$sheet->setCellValue('I'.$i, $value['hourfee']);
		$sheet->setCellValue('J'.$i, $fee);
		$idNum++;
	}

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');
?>