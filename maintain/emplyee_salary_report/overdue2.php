<?php
	@session_start();
	include "../../config.php";
	include('../../getEmplyeeInfo.php');	
	include '../inc_vars.php';
	include 'Fee.php';
	
	$empID =  $_REQUEST['empID'];
	$nowYr = date('Y');
	$curYr = $_REQUEST['year']?$_REQUEST['year']:$curYr=$nowYr;

	$key = "overdue_".$_REQUEST['year']."_".$_REQUEST['empID'];

	if($empID) {
		$base_days=30;	//大小月都以30天去計箅實際工作比例
		$dateB = "$curYr-01-01";
		$dateE = "$curYr-12-31";	
		$sql="select o.*,e.depID from overtime o left join emplyee e on o.empID=e.empID where o.empID='$empID' and isOK=1 and isDel=0 and fmAct=0 and bDate between '$dateB' and '$dateE' order by bDate";
		//echo $sql;
		$rsX=db_query($sql);		
		$tHours = 0;	//合計時數
		if(!db_eof($rsX)){
			$aa = array();	//一維
			while($rX=db_fetch_array($rsX)){ //加班單迴圈
				$id = $rX['id'];	//加班單號
				$ba = array();
				$ba['empID'] = $rX['empID'];
				$ba['depID'] = $rX['depID'];
				$ba['bDate'] = $rX['bDate'];
				$ba['eDate'] = $rX['eDate'];
				$ba['aType'] = $rX['aType'];
				$ba['hours'] = $rX['hours'];	//加班時數
				$ba['hourfee'] = getWage($rX['empID'], date('Y-m-d',strtotime($rX['bDate'])) );
				
				$sql = "select * from absence_overtime where otId=$id";
				$rsX2=db_query($sql);
				if(!db_eof($rsX)){ //有沖銷，檢查沖銷餘額
					$v = 0;
					while($rX2=db_fetch_array($rsX2))	$v += $rX2['hours'];
					$ba['hours2'] = $rX['hours']-$v;		//未沖銷時數
				} else { //未沖銷
					$otHours += $rX['hours'];
					$ba['hours2'] = $rX['hours'];
				}
				$aa[$id] = $ba;
				$otHours+=$rX['hours']?$rX['hours']:0;
				//$aa[$id]['fee'] 計算加班費金額
			}
		} 
	}
?>
<Html>
<Head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="<?=$extfiles?>list.css">
	<title>逾期未補休加班清單</title>
	<style>
		body {  margin: 0px; }
		#list { border-collapse:collapse; font-size: 10px; }
		#bgCover {
			position: absolute;
			display: none;
			left: 0; top: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(32, 32, 32, 0.75);
		}
		#formtbl {
			font-size: 12px;
			padding: 10px;
			border-radius: 10px;
		}
	</style>
	<script src="/Scripts/jquery.min.js"></script>
</head>

<body>

<table id="list" width="100%" border="1" cellpadding="2" cellspacing="0">
	<tr bgcolor="#C0C0C0"><th>加班單號</th><th>員工名稱</th><th>加班別</th><th>時間起</th><th>時間迄</th><th>加班時數</th><th>未補休時數</th><th>時薪</th><th>加班費</th></tr>
  <? 
	$empID = '';
	foreach($aa as $k=>$v) {
		if($v['hours2']==0) continue;
		$feeAll = overtimeFee($v['aType'],$v['hourfee'],$v['hours']);
		$feeUse = overtimeFee($v['aType'],$v['hourfee'],$v['hours']-$v['hours2']);
		$fee = $feeAll-$feeUse;
		echo "<tr><td>$k</td><td>"
		.$emplyeeinfo[$v['empID']]."</td><td>"
		.$overtimeType[$v['aType']]."</td><td>"
		.date('Y-m-d H:i',strtotime($v['bDate']))."</td><td>"
		.date('Y-m-d H:i',strtotime($v['eDate']))."</td><td>$v[hours] 小時</td><td>$v[hours2] 小時</td>"
		."<td>".round($v[hourfee])."</td>"
		."<td>$fee</td></tr>";
	} ?>
</table>
<br/>
<?
	$rsX = db_query("select * from pay_record where id='$key'");
?>
<table id="list" width="100%" align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="#C0C0C0">
	<th colspan="10" style="font-size: larger; "><div style="padding-top: 4px;">付款紀錄</div></th>
</tr>
<? if(!db_eof($rsX)) while($rX=db_fetch_array($rsX)) { ?>
<tr>
	<td bgcolor="#ddd" width="50">付款編號</td><td width="150"><?=$rX['id']?></td>
	<td bgcolor="#ddd" width="50">付款日期</td><td width="150"><?=$rX['rDate']?></td>
	<td bgcolor="#ddd" width="50">付款金額</td><td><?=$rX['money']?></td>
	<td bgcolor="#ddd" width="50">付款人員</td><td><?=$rX['createMan']?></td>
	<td bgcolor="#ddd" width="50">備註</td><td><?=$rX['notes']?></td>	
<tr>
<? } else { ?>
	<th colspan="10" style="font-size: larger; "><button style="float: left;" onclick="$('#bgCover').fadeIn(500)">新增</button></th>
<? } ?>
</table>

<div id="bgCover">
	<br/>
	<form id="form1" action="../pay_record/doappend.php" method="POST">
	<table id="formtbl" width="50%" bgcolor="#f0f0f0" align="center" cellpadding="4" cellspacing="0">
		<tr><th>付款金額</th><td><input type="text" name="money" value="0"></td></tr>
		<tr><th>備　　註</th><td><textarea name="notes" rows="3" cols="50"></textarea></td></tr>
		<tr><td>&nbsp;</td><td>
			<input type="hidden" name="key" value="<?=$key?>">
			<input type="hidden" name="empID" value="<?=$_REQUEST['empID']?>">
			<input type="hidden" name="year" value="<?=$_REQUEST['year']?>">
			<input type="hidden" name="createMan" value="<?=$_SESSION['Account']?>">
			<input type="hidden" name="backPath" value="../emplyee_salary_report/overdue2.php">
			<input type="submit" name="Submit" value="確定">
			<button type="button" onclick="$('#bgCover').fadeOut(500)">取消</button>
		</td></tr>
	</table>
	</form>
</div>
</body>
</Html>