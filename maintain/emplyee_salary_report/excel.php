<?
  header("Content-Type:text/html; charset=utf-8");
   
  include "../../config.php";
  include "../../getEmplyeeInfo.php";
  include '../inc_vars.php';
  include "func.php";
  
  $md5Name=$_REQUEST['md5Name'];
  $wkMonth=$_REQUEST['wkMonth'];
  $depID=$_REQUEST[dep];
  $depName=$departmentinfo[$depID];
  $empID=$_REQUEST["empID"];
  $detail=$_REQUEST['detail'];

  $showZero = $_REQUEST['showZero'];

  // $wkMonth='201706';
  // $md5Name='aaa';

  $path = "../../data/reports";
  if(!is_dir($path))  mkdir($path);
  $path .= '/salary';
  if(!is_dir($path))  mkdir($path);
  $txtName=$path."/".$wkMonth.'-overtime.txt';
  list($md5Name,$jo)=getMD5($depID,$empID,$wkMonth,$txtName);

  $cache_path="../../data/caches";
  if(!is_dir($cache_path)) mkdir($cache_path);
  $cache_file_name=$cache_path."/".$md5Name.".xlsx";
  //if(!file_exists($cache_file_name)){//判斷檔案內容是否有更新
    if($detail!="Y" && !$depID){//全表 & 選全部部門
      $jsonStr=file_get_contents($txtName);
      $jo=json_decode($jsonStr);
    }
    $filename = $wkMonth.'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition: attachment;filename=$filename");
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    $fmActAry=array("0"=>"補休","1"=>"加班費");   
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    //設定欄位背景色(方法1)
    $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cdcdcd')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:M1");
    $objPHPExcel->getActiveSheet()->mergeCells("A2:B2");
    $objPHPExcel->getActiveSheet()->mergeCells("A3:B3");
    $objPHPExcel->getActiveSheet()->mergeCells("E2:M2");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    
    $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
    $objPHPExcel->getActiveSheet()->setCellValue('A1',($depName?$depName:'全部部門'));
    $objPHPExcel->getActiveSheet()->setCellValue('A2','單位:'.($depName?$depName:'全部部門'));
    $objPHPExcel->getActiveSheet()->setCellValue('B2',$wkMonth);
    $objPHPExcel->getActiveSheet()->setCellValue('A3','報表日期：'.$wkMonth);

    $reg = "((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))";
    preg_match($reg, $sql, $match);
    $aa = explode('/',$match[0]);
    
    $colAry=array("A4",'B4','C4',"D4","E4","F4","G4","H4","I4","J4","K4","L4","M4");
    $titleAry=array("部門名稱",'員工編號','員工名稱',"月薪","時薪","加班別","前二小時時數","加班費A","第3-8小時時數","加班費B","第9-12小時時數","加班費C","合計加班費");
    for($j=0;$j<count($colAry);$j++){
      $objPHPExcel->getActiveSheet()->setCellValue($colAry[$j],$titleAry[$j]);
    }

    $i = 5;
    $curDep = '';
    $curTyp = '';
    $tHours=0;
    $color = "dedede";
      
    $col2Ary=array("A",'B','C',"D","E","F","G","H","I","J","K","L","M");
    $wAry=array("20","10","15","15","10","20","15","15","15","15","15","15","20");
  
    getData($jo,$objPHPExcel,$col2Ary,$i,$curDep,$color);
    //設定欄寬(自動欄寬)
    $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    
    //設定欄寬
    for($j=0;$j<count($col2Ary);$j++){
      $objPHPExcel->getActiveSheet()->getColumnDimension($col2Ary[$j])->setWidth($wAry[$j]);
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $cache_path="../../data/caches";
    $objWriter->save($cache_path.'/'.$md5Name.'.xlsx'); 

  //} 

  echo $md5Name;


  

  function getData(&$obj,&$objPHPExcel,$col2Ary,&$i,&$curDep,&$color){
    global $emplyeeinfo,$departmentinfo, $overtimeType;
    $kAry=array("empID","basic","depID","wage","aType","empName");
    //$aType=array(0=>"上班日加班",1=>"休息日加班",2=>"國定假日加班", 3=>'test');
    $aType = $overtimeType;
    foreach($obj as $k=>$val){
      if($k=='data'){
        foreach($val as $dk=>$dVal){
          proContent($dVal,$objPHPExcel,$col2Ary,$i,$curDep,$color,$aType);
        }
      }else if(in_array($k, $kAry)){
        proContent($obj,$objPHPExcel,$col2Ary,$i,$curDep,$color,$aType);
        break;
      } else{
        getData($val,$objPHPExcel,$col2Ary,$i,$curDep,$color);
      } 
    }
  }

function proContent($dVal,&$objPHPExcel,$col2Ary,&$i,&$curDep,&$color,$aType){
  global $showZero;
  if(!$showZero) {
    $total = 0;
    foreach ($aType as $k1 => $value) {
      $feeA=$dVal->aType[$k1]->feeA?$dVal->aType[$k1]->feeA:0;
      $feeB=$dVal->aType[$k1]->feeB?$dVal->aType[$k1]->feeB:0;
      $feeC=$dVal->aType[$k1]->feeC?$dVal->aType[$k1]->feeC:0;
      $total+=round($feeA+$feeB+$feeC);
    }

    if($total == 0) {
      return false;
    }

  }

 
  global $emplyeeinfo,$departmentinfo;
  $eid = $dVal->empID;
  $depID = $dVal->depID;
  if($curDep!=$depID) {
    $curDep = $depID;
    if($color=="ffffff"){
      $color="efefef";
    }else{
      $color="ffffff";
    }
  }

  $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->applyFromArray(
      array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => $color)
          )
      )
  );
  $total=0;
  //for($k1=0;$k1<=3;$k1++){
  foreach ($aType as $k1 => $value) {
  
    $valAry=array();
    $feeA=$dVal->aType[$k1]->feeA?$dVal->aType[$k1]->feeA:0;
    $feeB=$dVal->aType[$k1]->feeB?$dVal->aType[$k1]->feeB:0;
    $feeC=$dVal->aType[$k1]->feeC?$dVal->aType[$k1]->feeC:0;
    if(!$showZero && $feeA+$feeB+$feeC == 0) continue;
    array_push($valAry,$departmentinfo[$depID]);  //部門名稱
    array_push($valAry,$eid);                     //員工編號
    array_push($valAry,$emplyeeinfo[$eid]);       //員工名稱
    array_push($valAry,$dVal->basic);             //月薪
    array_push($valAry,$dVal->wage);              //時薪
    array_push($valAry,$aType[$k1]);            //加班別
    array_push($valAry,$dVal->aType[$k1]->hourA?$dVal->aType[$k1]->hourA:0);  //前二小時時數
    array_push($valAry,$feeA);                    //加班費A
    array_push($valAry,$dVal->aType[$k1]->hourB?$dVal->aType[$k1]->hourB:0);   //第3-8小時時數
    array_push($valAry,$feeB);                    //加班費B
    array_push($valAry,$dVal->aType[$k1]->hourC?$dVal->aType[$k1]->hourC:0);   //第9-12小時時數
    array_push($valAry,$feeC);                    //加班費C
    array_push($valAry,round($feeA+$feeB+$feeC));                       //合計加班費
    $total+=round($feeA+$feeB+$feeC);
    for($j=0;$j<count($col2Ary);$j++){
      if($j==1){
        $objPHPExcel->getActiveSheet()->setCellValueExplicit($col2Ary[$j].$i,$valAry[$j],  PHPExcel_Cell_DataType::TYPE_STRING);
      }else{
        $objPHPExcel->getActiveSheet()->setCellValue($col2Ary[$j].$i,$valAry[$j]);
      }
    }
    // '員工編號','員工名稱',"","","加班別","前二小時時數","加班費A","第3-8小時時數","加班費B","第9-12小時時數","加班費C","合計加班費"
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->applyFromArray(
      array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => $color)
          )
      )
    );
    $i++;
  }

  $objPHPExcel->getActiveSheet()->mergeCells("A".$i.":L".$i);
  $objPHPExcel->getActiveSheet()->setCellValue("M".$i,$total);
  $objPHPExcel->getActiveSheet()->getStyle("A".$i.":M".$i)->getFont()->setBold(true);
  $objPHPExcel->getActiveSheet()->getStyle("A".$i.":M".$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
  $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
  $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->applyFromArray(
      array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => $color)
          ),
           'font'  => array(
              'bold'  => true,
              'color' => array('rgb' => 'cc0000'),
              'name'  => 'Verdana'
          )
      )
    );
  $objPHPExcel->getActiveSheet()->setCellValue("A".$i,"總計");
  $i++;
}
?>