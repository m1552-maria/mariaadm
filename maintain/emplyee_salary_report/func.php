<?php
function getMD5($deps,$empID,$wkMonth,$filename){
	if(file_exists($filename)){
	  $original=file_get_contents($filename);
	  $original=json_decode($original);
	  $depAry=explode(",",$deps);
	  for($i=0;$i<count($depAry);$i++){
	    $dep=$depAry[$i];
	    if(strlen($dep)==1){//ex.C
	      if($empID){
	        $jo->{$dep}->{'data'}->{$empID}=$original->{$dep}->{'data'}->{$empID};
	      }else{
	        $jo->{$dep}=$original->{$dep};
	      }
	    }else if(strlen($dep)==2){//ex.C4
	      $pid=substr($dep,0,1);
	      if($empID){
	        $jo->{$pid}->{$dep}->{'data'}->{$empID} = $original->{$pid}->{$dep}->{'data'}->{$empID};
	      }else{
	        $jo->{$pid}->{$dep} = $original->{$pid}->{$dep};
	      }
	    }else if(strlen($dep)>2){//ex.C4-001
	      $pid1=substr($dep,0,1);
	      $pid2=substr($dep,0,2);
	      if($empID){
	        $jo->{$pid1}->{$pid2}->{$dep}->{'data'}->{$empID} = $original->{$pid1}->{$pid2}->{$dep}->{'data'}->{$empID};
	      }else{
	        $jo->{$pid1}->{$pid2}->{$dep} = $original->{$pid1}->{$pid2}->{$dep};
	      }
	    }else{
	      $jo=$original;
	    }
	  }
	  $jsonStr=json_encode($jo);
	  $md5Name=md5($jsonStr);
	}else{
	  $md5Name=-1;
	}
	$ary=array();
	$ary[0]=$md5Name;
	$ary[1]=$jo;
	return $ary;
}

?>