<?php
	@session_start();
	include "../../config.php";
	include('../../getEmplyeeInfo.php');	
	include '../inc_vars.php';
	include 'Fee.php';
		
	if(isset($_REQUEST['Submit'])) {
		//$nowY = $_REQUEST['yearFilter']?$_REQUEST['yearFilter']:date('Y');
		//$nowM=date('m');
		//$days_in_month = cal_days_in_month(CAL_GREGORIAN, $nowM, $nowY);
		$base_days=30;	//大小月都以30天去計箅實際工作比例
		//$depID = $_REQUEST[depFilter];
		/*20180323 sean 加入前後台顯示部門權限*/
		if(!empty($_REQUEST['depFilter'])){
			$temp[] = "e.depID like '".$_REQUEST['depFilter']."%'";
		}else if ($_SESSION['privilege'] <= 10) {	
			if($_SESSION['user_classdef'][2]) {
				$aa = array();
				foreach($_SESSION['user_classdef'][2] as $v){	if($v)	$aa[]="e.depID like '$v%'";	}
				$temp[] = "(".join(' or ',$aa).")"; 	
			} else { $temp[] = "e.depID=''";	}
		}
		$filter = (count($temp)>0) ? implode(' and ',$temp).' and ' : '';
		$wkY = $_REQUEST['yearFilter']?$_REQUEST['yearFilter']:date('Y');
		$dateB = "$wkY-01-01";
		$dateE = "$wkY-12-31";	
		$sql="select o.*,e.depID from overtime o left join emplyee e on o.empID=e.empID where ".$filter." isOK=1 and isDel=0 and fmAct=0 and bDate between '".$dateB."' and '".$dateE."' ";
		if($_REQUEST['searchData'] != '') {
			$sql.= " and (e.empID like '%".$_REQUEST['searchData']."%' or e.empName like '%".$_REQUEST['searchData']."%') ";
		}
		$sql.= " order by empID,bDate";
		//echo $sql;
		$rsX=db_query($sql);		
		$tHours = 0;	//合計時數
		if(!db_eof($rsX)){
			$aa = array();	//一維
			while($rX=db_fetch_array($rsX)){ //加班單迴圈
				$id = $rX['id'];	//加班單號
				$ba = array();
				$ba['empID'] = $rX['empID'];
				$ba['depID'] = $rX['depID'];
				$ba['bDate'] = $rX['bDate'];
				$ba['eDate'] = $rX['eDate'];
				$ba['aType'] = $rX['aType'];
				$ba['hours'] = $rX['hours'];	//加班時數
				$ba['hourfee'] = getWage($rX['empID'], date('Y-m-d',strtotime($rX['bDate'])) );
				
				$sql = "select * from absence_overtime where otId=$id";
				$rsX2=db_query($sql);
				if(!db_eof($rsX)){ //有沖銷，檢查沖銷餘額
					$v = 0;
					while($rX2=db_fetch_array($rsX2))	$v += $rX2['hours'];
					$ba['hours2'] = $rX['hours']-$v;		//未沖銷時數
				} else { //未沖銷
					$otHours += $rX['hours'];
					$ba['hours2'] = $rX['hours'];
				}
				$aa[$id] = $ba;
				$otHours+=$rX['hours']?$rX['hours']:0;
				//$aa[$id]['fee'] 計算加班費金額
			}
		} else {
			echo '<br>no data!';
			return false;	//無資料
		}	
	}
?>
<Html>
<Head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="<?=$extfiles?>list.css">
	<title>逾期未補休加班清單</title>
	<style>
		#list { border-collapse:collapse; }
	</style>
	<script type="text/javascript">
		function excelSqlForm(){
			document.getElementById('depSql').value = document.getElementById("depFilter").value;
			document.getElementById('yearSql').value = document.getElementById("yearFilter").value;
		}
	</script>
</head>

<body onLoad="excelSqlForm()"> <!-- 載入先把select的值帶入#depSql的input -->
<table width="96%" align="center" border="0" cellpadding="2" cellspacing="0">
	<tr><td>
		<form>
		<? if(!$_REQUEST['detail']){ $y = intval(date('Y')); $yp= $y-1; ?>
    <select name="yearFilter" id="yearFilter" onChange="excelSqlForm()">
    <option value="<?=$y?>" <?=($wkY==$y?'selected':'')?>><?=$y?>年</option><option value="<?=$yp?>" <?=($wkY==$yp?'selected':'')?>><?=$yp?>年</option>
    </select>

    
    <select name="depFilter" id="depFilter" onChange="excelSqlForm()"><option value=''>-全部部門-</option>
    <? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST['depFilter']==$k?' selected ':'').">$v</option>";
      } else {
	      foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST['depFilter']==$v?' selected ':'').">".$departmentinfo[$v]."</option>";
      }
    ?>
    </select>
    <?	} ?>
    <input type="text" name="searchData" id="searchData" value="<?php echo isset($_REQUEST['searchData']) ? $_REQUEST['searchData'] : '';?>" placeholder="請輸入姓名或員編">
    <input type="submit" name="Submit" value="確定"/>
    <input type="button" name="excel" value="匯出Excel" onClick="document.getElementById('excelForm').submit()">
    </form>
    <form action="overdueExcel.php" method="post" id="excelForm" target="excel">
    	<input type="hidden" id="depSql" name="depSql"> <!--紀錄選取的dep-->
      <input type="hidden" id="yearSql" name="yearSql"> <!--紀錄選取的dep-->
    </form>
  </td></tr>
</table>
 
<table id="list" width="96%" align="center" border="1" cellpadding="2" cellspacing="0">
	<tr bgcolor="#C0C0C0"><th>加班單號</th><th>員工名稱</th><th>加班別</th><th>時間起</th><th>時間迄</th><th>加班時數</th><th>未補休時數</th><th>時薪</th><th>加班費</th></tr>
  <?php 
	$empID = '';
	foreach($aa as $k=>$v) {
		if($v['hours2']==0) continue;
		$feeAll = overtimeFee($v['aType'],$v['hourfee'],$v['hours']);
		$feeUse = overtimeFee($v['aType'],$v['hourfee'],$v['hours']-$v['hours2']);
		$fee = $feeAll-$feeUse;
		
		if(	$empID != $v['empID']) {
			$empID = $v['empID'];
			echo "<tr bgcolor='#F0F0F0'><th colspan='9'>".$emplyeeinfo[$empID]." (".$departmentinfo[$v['depID']].")</th></tr>";
		}
		echo "<tr><td>$k</td><td>"
		.$emplyeeinfo[$v['empID']]."</td><td>"
		.$overtimeType[$v['aType']]."</td><td>"
		.date('Y-m-d H:i',strtotime($v['bDate']))."</td><td>"
		.date('Y-m-d H:i',strtotime($v['eDate']))."</td><td>".$v['hours']." 小時</td><td>".$v['hours2']." 小時</td>"
		."<td>".$v['hourfee']."</td>"
		."<td>$fee</td></tr>";
	} ?>
</table>
</body>
</Html>