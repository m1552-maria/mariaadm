<?php
	session_start();
	include("../../config.php");

	$nowYr = date('Y');
	$curYr = $_REQUEST['year']?$_REQUEST['year']:$nowYr;
	if($_REQUEST['month']) $curMh = $_REQUEST['month']; else $curMh = date('m');
	if($_SESSION['privilege'] <= 10 && count($_SESSION['user_classdef'][7]) == 0) {
		echo "請設定管理的計畫編號";
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<form name="excelfm2" action="exportExcel.php" method="post" target="excel">
<div>
<?php if($_GET['action'] == 'exportExcel.php') { ?>
	清冊類別:
	<select name="printType" id="printType">
		<option value="excel"> EXCEL </option>
		<option value="pdf"> PDF </option>
	</select>
<?php } ?>
</div>

<br>
 <select name="year" id="year"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
 <select name="month" id="month">
	  <? 
	   		for($i=1;$i<=12;$i++) { 
	  		$sel=($curMh==$i?'selected':''); 
	  		echo "<option $sel value='$i'>$i 月</option>"; 
	  	} 
	  ?>
</select>
~
<select name="yearE" id="yearE"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select> 
 <select name="monthE" id="monthE">
	  <? 
	   		for($i=1;$i<=12;$i++) { 
	  		$sel=($curMh==$i?'selected':''); 
	  		echo "<option $sel value='$i'>$i 月</option>"; 
	  	} 
	  ?>
</select>

<br><br>
計畫編號:
<?php if($_SESSION['privilege'] > 10) { ?>
	<input type="text" name="projIDFilter" id="projIDFilter" value="">
<?php }else{?>
	<div>
		<label><input type="checkbox"  value="全選" onclick="allCheck(this)">全選</label>
		<?php 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo '<label><input type="checkbox" name="projIDFilter" value="'.$v.'">'.$v.'</label>';
	    ?>
	</div>


	<!--<select name="projIDFilter" id="projIDFilter" ><option value=''>-全部計畫-</option>
		<? 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo "<option value='$v'".($_REQUEST[projIDFilter]==$v?' selected ':'').">$v</option>"; 
	    ?>
	    </select>-->
<?php } ?>
<br><br>
<div>選取部門及員工:</div>

<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr valign="top">
  			<td>
  				<ul class='dep-emp-ul'>
					<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
					<li class='dep-emp-li'>
						<div class='query-title'>部門</div>
						<div class="depID_list">NO DATA</div>
					</li>
					<li class='dep-emp-li'>
						<div class='query-title'>員工</div>
						<div class="empID_list">NO DATA</div>
					</li>
				</ul>
  			</td>
  		</tr>
</table>


<input type="button" onclick="excelfm2Smi()" value="確定">
<script src="../../Scripts/jquery-1.12.3.min.js" type="text/javascript"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script type="text/javascript">
	function excelfm2Smi() {
<?php if($_SESSION['privilege'] > 10) { ?>
		window.opener.excelfm.projID.value=$('#projIDFilter').val();
<?php } else { ?> 
		var projID = [];
		$('[name="projIDFilter"]').each(function(){
			if(this.checked) projID.push($(this).val());
		});
		window.opener.excelfm.projID.value=projID.join(',');
<?php } ?>

		window.opener.excelfm.action = '<?php echo $_GET['action']?>';
		window.opener.excelfm.year.value=$('#year').val();
		window.opener.excelfm.month.value=$('#month').val();
		window.opener.excelfm.printType.value=$('#printType').val();

		window.opener.excelfm.yearE.value=$('#yearE').val();
		window.opener.excelfm.monthE.value=$('#monthE').val();

		var depID = [];
		var empID = [];
		$('[name="depID[]"]').each(function(){
			depID.push($(this).val());
		});
		depID = (depID.length>0) ? depID.join(',') : '';

		$('[name="empID[]"]').each(function(){
			empID.push($(this).val());
		});
		empID = (empID.length>0) ? empID.join(',') : '';

		window.opener.excelfm.depID.value = depID;
		window.opener.excelfm.empID.value = empID;


		window.opener.excelfm.submit();
		window.close();
	}

	function allCheck(obj) {
		var pList = document.getElementsByName('projIDFilter');
		for(var i=0;i<pList.length;i++) {
			pList[i].checked = obj.checked;
		} 
	}
</script>
</form>
</body>
</html>