<?
  
    session_start();
    include("../../config.php");
    include('../inc_vars.php');  
    include('../../getEmplyeeInfo.php');
    include('getData.php');


    $data = array(
      'sWkMonth' => $_REQUEST['year'].str_pad($_REQUEST['month'],2,"0",STR_PAD_LEFT),
      'eWkMonth' => $_REQUEST['yearE'].str_pad($_REQUEST['monthE'],2,"0",STR_PAD_LEFT),
      'projID'=> $_REQUEST['projID'],
      'depID'=> '',
      'empID'=> $_REQUEST['empID']
    );

    if($_REQUEST['projID']) {
      $data['projID'] = $_REQUEST['projID'];
    } elseif ($_SESSION['privilege'] <= 10) {
      if($_SESSION['user_classdef'][7]) {
        $aa = array();
        foreach($_SESSION['user_classdef'][7] as $v){
          if($v)  $aa[]="$v";  
        }
        $data['projID'] = implode(',', $aa);
      } else {
        $data['projID'] = '-1';
      }
    }

    $list = getSalary($data);

  $rangeDate = $_REQUEST['year'].str_pad($_REQUEST['month'],2,'0',STR_PAD_LEFT).'~'.$_REQUEST['yearE'].str_pad($_REQUEST['monthE'],2,'0',STR_PAD_LEFT);

  $now_date_time = date('Y-m-d H:i:s');
  $lastYear=intval(date('Y')-1);

  $filename = date("Y-m-d H:i:s",time()).'.xlsx';
  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');
  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();

  //跑header 迴圈用
  $header = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P');
  $headervalue = array('序號','姓名','員編','薪資','津貼及加項','請假/調整','小計','伙食津貼','加班費','應領薪資','代扣所得稅','代扣勞保費','代扣健保費','退休金自提','其他代(補)扣','實領金額');
  $noFormat = array('A','B','C','D');
  
  $i=1;
  $number = 0;
  $pHeader = '';

  $totalList = array();
  for($j=69;$j<=80;$j++) {
    $totalList[chr($j)]['total'] = 0;
    $totalList[chr($j)]['allTotal'] = 0;
  }
  $totalList['P']['negativeTotal'] = 0;
  $totalList['C']['total'] = 0;
  $totalList['C']['allTotal'] = 0;
  foreach($list as $k=>$v){
    if(substr($k,0,2) != $pHeader) {
      if($pHeader != '') {
        $i++;
        $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);
        $sheet->setCellValue('B'.$i,'合計');
        $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        foreach ($totalList as $key => $value) {
          if(!in_array($key, $noFormat)) $value['allTotal'] = number_format($value['allTotal']);
          if($key == 'C') $value['allTotal'].= '人';
          $sheet->setCellValue($key.$i, $value['allTotal']);
          $totalList[$key]['allTotal'] = 0;
        }

        $i = $i+3;
        $sheet->setCellValue('B'.$i,'經辦:');
        $sheet->setCellValue('H'.$i,'主任/園長:');
        $sheet->setCellValue('M'.$i,'執行長：');
        $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);
        $i = $i+4;
      }
      $number = 0;

        //頭
      $pHeader = substr($k,0,2);
      $sheet->mergeCells('A'.$i.':P'.$i);//合併
      $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
      $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(16);
     
      $i++;
      $sheet->mergeCells('A'.$i.':P'.$i);
      $sheet->setCellValue('A'.$i,'員工薪資清冊');
      $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(14);
      
      $i++;
      $sheet->mergeCells('A'.$i.':P'.$i);
      $sheet->setCellValue('A'.$i, $rangeDate);
      $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(12);
      
    }

    foreach($v as $k1=>$v1){
      $totalList['C']['total'] = count($v1['sub_list']);
      $i++;
      $sheet->mergeCells('A'.$i.':C'.$i);//合併
      $sheet->setCellValue('A'.$i,'部門:'.$departmentinfo[$k1]);

      $sheet->mergeCells('E'.$i.':F'.$i);//合併
      if($k == 'no_proj') $sheet->setCellValue('E'.$i,'計畫編號:無');
      else $sheet->setCellValue('E'.$i,'計畫編號:'.$k);
  
      $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(12);

      $i++;
      //表頭
      $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);
      foreach($header as $k3=>$v3){
        $sheet->getStyle($v3.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue($v3.$i,$headervalue[$k3]);
      }

      $i++;
      $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);

      for($j=69;$j<=79;$j++) {
        $sheet->getStyle(chr($j).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        if($j<=73) $sheet->setCellValue(chr($j).$i,'(+)');
        elseif($j>74) $sheet->setCellValue(chr($j).$i,'(-)');
      }

      $sheet->getStyle('A'.$i.':P'.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);

      //員工列表
      foreach($v1['sub_list'] as $k2=>$v2){
        $i++;
        $number++;

        $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);
        $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('A'.$i,$number);
        $sheet->setCellValue('B'.$i,$v2['empName']);
        $sheet->setCellValueExplicit('C'.$i, $v2['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
        
        $sheet->getStyle('D'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('E'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('H'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('I'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('J'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('K'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('L'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('M'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('N'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('O'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('P'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle('D'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        //$sheet->setCellValue('D'.$i,number_format($v2['mBasic'] - $v2['food']));
        $sheet->getCell('D'.$i)->setValueExplicit(($v2['mBasic'] - $v2['food']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['D']['total'] += $v2['mBasic'] - $v2['food'];

        //$sheet->setCellValue('E'.$i,number_format($v2['allowance']));
        $sheet->getCell('E'.$i)->setValueExplicit($v2['allowance'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['E']['total'] += $v2['allowance'];

       //$sheet->setCellValue('F'.$i,number_format($v2['absence']));
        $sheet->getCell('F'.$i)->setValueExplicit($v2['absence'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['F']['total'] += $v2['absence'];

        $con = $v2['mBasic'] - $v2['food'] + $v2['allowance'] - $v2['absence'];
       //$sheet->setCellValue('G'.$i,number_format($con));
        $sheet->getCell('G'.$i)->setValueExplicit($con, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['G']['total'] += $con;

        //$sheet->setCellValue('H'.$i,number_format($v2['food']));
        $sheet->getCell('H'.$i)->setValueExplicit($v2['food'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['H']['total'] += $v2['food'];

        //$sheet->setCellValue('I'.$i,number_format($v2['overtime']));
        $sheet->getCell('I'.$i)->setValueExplicit($v2['overtime'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['I']['total'] += $v2['overtime'];

        //$jTotal = $v2['mBasic']+$v2['addTotal']-$v2['absence'];
        $jTotal = $v2['mBasic']+$v2['allowance']+$v2['overtime']-$v2['absence'];
        //$sheet->setCellValue('J'.$i,number_format($jTotal));
        $sheet->getCell('J'.$i)->setValueExplicit($jTotal, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['J']['total'] += $jTotal;

        //$sheet->setCellValue('K'.$i,number_format($v2['taxPay']));
        $sheet->getCell('K'.$i)->setValueExplicit($v2['taxPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['K']['total'] += $v2['taxPay'];

        //$sheet->setCellValue('L'.$i,number_format($v2['jobPay']));
        $sheet->getCell('L'.$i)->setValueExplicit($v2['jobPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['L']['total'] += $v2['jobPay'];

        //$sheet->setCellValue('M'.$i,number_format($v2['healthPay']));
        $sheet->getCell('M'.$i)->setValueExplicit($v2['healthPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['M']['total'] += $v2['healthPay'];

        //$sheet->setCellValue('N'.$i,number_format($v2['mRetirePay']));
        $sheet->getCell('N'.$i)->setValueExplicit($v2['mRetirePay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['N']['total'] += $v2['mRetirePay'];

        //$sheet->setCellValue('O'.$i,number_format($v2['otherPay']));
        $sheet->getCell('O'.$i)->setValueExplicit($v2['otherPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['O']['total'] += $v2['otherPay'];

        //$sheet->setCellValue('P'.$i,number_format($v2['payTotal']));
        $sheet->getCell('P'.$i)->setValueExplicit($v2['payTotal'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $totalList['P']['total'] += $v2['payTotal'];
        if($v2['payTotal'] < 0) $totalList['P']['negativeTotal'] += $v2['payTotal'];

      }
      $sheet->getStyle('A'.$i.':P'.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
      $i++;
      $sheet->setCellValue('B'.$i,'小計');
      $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);

      foreach ($totalList as $key => $value) {
        $sheet->getStyle($key.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $totalVal = ($key == 'C') ? $value['total'].'人' : $value['total'];
        if(!in_array($key, $noFormat)) {
          $sheet->getStyle($key.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getCell($key.$i)->setValueExplicit($totalVal, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        } else {
          $sheet->setCellValue($key.$i,$totalVal);
        }
        $sheet->getStyle($key.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $totalList[$key]['allTotal'] += $value['total'];
        $totalList[$key]['total'] = 0;
      }

      $i++;
    }

  }


  $i++;
  $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);
  $sheet->setCellValue('B'.$i,'合計');
  $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  foreach ($totalList as $key => $value) {
    if($key == 'C') $value['allTotal'].= '人';
    if(!in_array($key, $noFormat)) {
      $sheet->getStyle($key.$i)->getNumberFormat()->setFormatCode('#,##0');
      $sheet->getCell($key.$i)->setValueExplicit($value['allTotal'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
    } else {
      $sheet->setCellValue($key.$i, $value['allTotal']);
    }
     $sheet->getStyle($key.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

   // $totalList[$key]['allTotal'] = 0;
  }

  if($totalList['P']['negativeTotal'] != 0) {
      $i++;
      $sheet->setCellValue('O'.$i,'正數合計:');
      $sheet->setCellValue('P'.$i, number_format($totalList['P']['allTotal']-$totalList['P']['negativeTotal']));
      $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      $sheet->getStyle('O'.$i.':P'.$i)->getFont()->setSize(9);
      $i++;
      $sheet->setCellValue('O'.$i,'負數合計:');
      $sheet->setCellValue('P'.$i, number_format($totalList['P']['negativeTotal']));
      $sheet->getStyle('A'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      $sheet->getStyle('O'.$i.':P'.$i)->getFont()->setSize(9);
    }

  $i = $i+3;
  $sheet->setCellValue('B'.$i,'經辦:');
  $sheet->setCellValue('H'.$i,'主任/園長:');
  $sheet->setCellValue('M'.$i,'執行長：');
  $sheet->getStyle('A'.$i.':P'.$i)->getFont()->setSize(9);

  //設定欄寬
  //$width = array(5,10,10,10,15,15,15,15,15,15,15,15,15,15,15,15);
  $width = array(4,8,5,8,9,8,8,8,8,8,9,9,9,9,9,9);
  foreach($header as $k=>$v){
    $sheet->getColumnDimension($v)->setWidth($width[$k]);  
  }
  
  $sheet->getHeaderFooter()->setOddFooter('&P / &N');
  $sheet->getHeaderFooter()->setEvenFooter('&P / &N');
  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');

    


?>