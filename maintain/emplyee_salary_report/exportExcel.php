<?
  
    session_start();
    include("../../config.php");
    include('../inc_vars.php');  
    include('../../getEmplyeeInfo.php');
    include('getData.php');

  
    $data = array(
      'sWkMonth' => $_REQUEST['year'].str_pad($_REQUEST['month'],2,"0",STR_PAD_LEFT),
      'eWkMonth' => $_REQUEST['yearE'].str_pad($_REQUEST['monthE'],2,"0",STR_PAD_LEFT),
      'projID'=> $_REQUEST['projID'],
      'depID'=> $_REQUEST['depID'],
      'empID'=> $_REQUEST['empID']
    );


    if($_REQUEST['projID']) {
      $data['projID'] = $_REQUEST['projID'];
    } elseif ($_SESSION['privilege'] <= 10) {
      if($_SESSION['user_classdef'][7]) {
        $aa = array();
        foreach($_SESSION['user_classdef'][7] as $v){
          if($v)  $aa[]="$v";  
        }
        $data['projID'] = implode(',', $aa);
      } else {
        $data['projID'] = '-1';
      }
    }


    $list = getSalary($data);




  $now_date_time = date('Y-m-d H:i:s');
  $lastYear=intval(date('Y')-1);


  $rangeDate = $_REQUEST['year'].str_pad($_REQUEST['month'],2,'0',STR_PAD_LEFT).'~'.$_REQUEST['yearE'].str_pad($_REQUEST['monthE'],2,'0',STR_PAD_LEFT);

  if($_REQUEST['printType'] == 'excel') { 

    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);
    $sheet = $objPHPExcel->getActiveSheet();

    //跑header 迴圈用
    $header = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q');
    $headervalue = array('序號','姓名','員編','類型','薪資','伙食津貼','津貼及加項','加班費','請假/調整','應領薪資','代扣所得稅','代扣勞保費','代扣健保費','退休金自提','其他代(補)扣','實領金額','月份');
    $noFormat = array('A','B','C','D');
    $i=1;
    $number = 0;
    $pHeader = '';

    $totalList = array();
    for($j=69;$j<=80;$j++) {
      $totalList[chr($j)]['total'] = 0;
      $totalList[chr($j)]['allTotal'] = 0;
    }
    $totalList['P']['negativeTotal'] = 0;

    $totalList['C']['total'] = 0;
    $totalList['C']['allTotal'] = 0;
    foreach($list as $k=>$v){
      if(substr($k,0,2) != $pHeader) {
        if($pHeader != '') {
          $i++;
          $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(12);
          $sheet->setCellValue('B'.$i,'合計');
          $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          foreach ($totalList as $key => $value) {
            if($key == 'C') $value['allTotal'].= '人';
            if(!in_array($key, $noFormat)) $value['allTotal'] = number_format($value['allTotal']);
            $sheet->setCellValue($key.$i, $value['allTotal']);
            $totalList[$key]['allTotal'] = 0;
          }

          $i = $i+3;
          $sheet->setCellValue('B'.$i,'經辦:');
          $sheet->setCellValue('H'.$i,'主任/園長:');
          $sheet->setCellValue('O'.$i,'執行長：');
          $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
          $i = $i+4;
        }
        $number = 0;

          //頭
        $pHeader = substr($k,0,2);
        $sheet->mergeCells('A'.$i.':Q'.$i);//合併
        $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
        $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(20);
       
        $i++;
        $sheet->mergeCells('A'.$i.':Q'.$i);
        $sheet->setCellValue('A'.$i,'員工薪資清冊');
        $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(16);
        
        $i++;
        $sheet->mergeCells('A'.$i.':Q'.$i);
        $sheet->setCellValue('A'.$i, $rangeDate);
        $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(14);
        
      }

      foreach($v as $k1=>$v1){
        $totalList['C']['total'] = count($v1['sub_list']);
        $i++;
        $sheet->mergeCells('A'.$i.':C'.$i);//合併
        if($k == 'no_proj') $sheet->setCellValue('A'.$i,'計畫編號:無');
        else $sheet->setCellValue('A'.$i,'計畫編號:'.$k);

        $sheet->mergeCells('E'.$i.':F'.$i);//合併
        $sheet->setCellValue('E'.$i,'名稱:'.$departmentinfo[$k1]);
    
        $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(14);

        $i++;
        //表頭
        $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(12);
        foreach($header as $k3=>$v3){
          $sheet->getStyle($v3.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->setCellValue($v3.$i,$headervalue[$k3]);
        }

        $i++;
        $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(12);

        for($j=69;$j<=79;$j++) {
          $sheet->getStyle(chr($j).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          if($j<=72) $sheet->setCellValue(chr($j).$i,'(+)');
          elseif($j==73 || $j>74) $sheet->setCellValue(chr($j).$i,'(-)');
        }

        $sheet->getStyle('A'.$i.':Q'.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);

        //員工列表
        foreach($v1['sub_list'] as $k2=>$v2){
          $i++;
          $number++;

          $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(12);
          $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->setCellValue('A'.$i,$number);
          $sheet->setCellValue('B'.$i,$v2['empName']);
          $sheet->setCellValueExplicit('C'.$i, $v2['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
          $sheet->setCellValue('D'.$i,$v2['jobClass']);
          $sheet->getStyle('E'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('G'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('H'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('I'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('J'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('K'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('L'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('M'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('N'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('O'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('P'.$i)->getNumberFormat()->setFormatCode('#,##0');
          $sheet->getStyle('E'.$i.':P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

          //$sheet->setCellValue('E'.$i,number_format($v2['mBasic'] - $v2['food']));
          $sheet->getCell('E'.$i)->setValueExplicit(($v2['mBasic'] - $v2['food']), PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['E']['total'] += $v2['mBasic'] - $v2['food'];

          //$sheet->setCellValue('F'.$i,number_format($v2['food']));*/
          $sheet->getCell('F'.$i)->setValueExplicit($v2['food'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          //$sheet->setCellValueExplicit('A1', '20130829071002210', PHPExcel_Cell_DataType::TYPE_STRING);
          $totalList['F']['total'] += $v2['food'];

          //$sheet->setCellValue('G'.$i,number_format($v2['allowance']));
          $sheet->getCell('G'.$i)->setValueExplicit($v2['allowance'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['G']['total'] += $v2['allowance'];

          //$sheet->setCellValue('H'.$i,number_format($v2['overtime']));
          $sheet->getCell('H'.$i)->setValueExplicit($v2['overtime'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['H']['total'] += $v2['overtime'];

          //$sheet->setCellValue('I'.$i,number_format($v2['absence']));
          $sheet->getCell('I'.$i)->setValueExplicit($v2['absence'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['I']['total'] += $v2['absence'];

          //$jTotal = $v2['mBasic']+$v2['addTotal']-$v2['absence'];
          $jTotal = $v2['mBasic']+$v2['allowance']+$v2['overtime']-$v2['absence'];
          //$sheet->setCellValue('J'.$i,number_format($jTotal));
          $sheet->getCell('J'.$i)->setValueExplicit($jTotal, PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['J']['total'] += $jTotal;

          //$sheet->setCellValue('K'.$i,number_format($v2['taxPay']));
          $sheet->getCell('K'.$i)->setValueExplicit($v2['taxPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['K']['total'] += $v2['taxPay'];

          //$sheet->setCellValue('L'.$i,number_format($v2['jobPay']));
          $sheet->getCell('L'.$i)->setValueExplicit($v2['jobPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['L']['total'] += $v2['jobPay'];

          //$sheet->setCellValue('M'.$i,number_format($v2['healthPay']));
          $sheet->getCell('M'.$i)->setValueExplicit($v2['healthPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['M']['total'] += $v2['healthPay'];

          //$sheet->setCellValue('N'.$i,number_format($v2['mRetirePay']));
          $sheet->getCell('N'.$i)->setValueExplicit($v2['mRetirePay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['N']['total'] += $v2['mRetirePay'];

          //$sheet->setCellValue('O'.$i,number_format($v2['otherPay']));
          $sheet->getCell('O'.$i)->setValueExplicit($v2['otherPay'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['O']['total'] += $v2['otherPay'];

          //$sheet->setCellValue('P'.$i,number_format($v2['payTotal']));
          $sheet->getCell('P'.$i)->setValueExplicit($v2['payTotal'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
          $totalList['P']['total'] += $v2['payTotal'];
          if($v2['payTotal'] < 0) $totalList['P']['negativeTotal'] += $v2['payTotal'];

          $_wkMonth = (substr($v2['wkMonth'],0,4) - 1911).substr($v2['wkMonth'],4,2);
          $sheet->setCellValue('Q'.$i,$_wkMonth);

        }
        $sheet->getStyle('A'.$i.':Q'.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
        $i++;
        $sheet->setCellValue('B'.$i,'小計');
        $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(12);

        foreach ($totalList as $key => $value) {
          $sheet->getStyle($key.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $totalVal = ($key == 'C') ? $value['total'].'人' : $value['total'];
          if(!in_array($key, $noFormat)) {
            $sheet->getStyle($key.$i)->getNumberFormat()->setFormatCode('#,##0');
            $sheet->getCell($key.$i)->setValueExplicit($totalVal, PHPExcel_Cell_DataType::TYPE_NUMERIC);
          } else {
            $sheet->setCellValue($key.$i,$totalVal);
          }
           $sheet->getStyle($key.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

          $totalList[$key]['allTotal'] += $value['total'];
          $totalList[$key]['total'] = 0;
        }

        $i++;
      }

    }


    $i++;
    $sheet->getStyle('A'.$i.':Q'.$i)->getFont()->setSize(12);
    $sheet->setCellValue('B'.$i,'合計');
    $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    foreach ($totalList as $key => $value) {
      if($key == 'C') $value['allTotal'].= '人';
      if(!in_array($key, $noFormat)) {
        $sheet->getStyle($key.$i)->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getCell($key.$i)->setValueExplicit($value['allTotal'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
      } else {
        $sheet->setCellValue($key.$i, $value['allTotal']);
      }
      $sheet->getStyle($key.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      //$totalList[$key]['allTotal'] = 0;
    }

    if($totalList['P']['negativeTotal'] != 0) {
      $i++;
      $sheet->setCellValue('O'.$i,'正數合計:');
      $sheet->setCellValue('P'.$i, number_format($totalList['P']['allTotal']-$totalList['P']['negativeTotal']));
      $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
       $sheet->getStyle('P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      $i++;
      $sheet->setCellValue('O'.$i,'負數合計:');
      $sheet->setCellValue('P'.$i, number_format($totalList['P']['negativeTotal']));
      $sheet->getStyle('A'.$i.':Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    }

    $i = $i+3;
    $sheet->setCellValue('B'.$i,'經辦:');
    $sheet->setCellValue('H'.$i,'主任/園長:');
    $sheet->setCellValue('O'.$i,'執行長：');
    $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);

    //設定欄寬
    $width = array(5,10,10,10,15,15,15,15,15,15,15,15,15,15,15,15,15);
    foreach($header as $k=>$v){
      $sheet->getColumnDimension($v)->setWidth($width[$k]);  
    }
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->setPreCalculateFormulas(false);
    $objWriter->save('php://output');

  } elseif($_REQUEST['printType'] == 'pdf') {
    include_once '../../mpdf/mpdf.php';
    $mpdf = new mPDF('UTF-8','A4-L','','DFKai-sb',10,10,25,20);
    $mpdf->useAdobeCJK = true;
    $mpdf->SetAutoFont(AUTOFONT_ALL);
    $mpdf->SetDisplayMode('fullpage');
    $html = file_get_contents('pdfHtml.html');
    $_html = array();

    $number = 0;
    $pHeader = '';

    $header = array('NO'=>'序<br>號','empName'=>'姓名','empID'=>'員編','jobClass'=>'類型','mBasic'=>'薪資','food'=>'伙食<br>津貼','allowance'=>'津貼<br>及加項','overtime'=>'加班費','absence'=>'請假<br>/調整','jTotal'=>'應領薪資','taxPay'=>'代扣<br>所得稅','jobPay'=>'代扣<br>勞保費','healthPay'=>'代扣<br>健保費','mRetirePay'=>'退休金<br>自提','otherPay'=>'其他<br>代(補)扣','payTotal'=>'實領金額','wkMonth'=>'月份');
    $headerW = array('jobClass'=>'70px','mBasic'=>'100px','jTotal'=>'100px', 'payTotal'=>'100px','wkMonth'=>'50px');
    $noFormat = array('NO','empName','empID','jobClass','wkMonth');

    $totalList = array();
    $br = '<tr class="trBr"><td class="cLabel" colspan="'.count($header).'"><br></td></tr>';
    foreach ($header as $key => $value) {
      $totalList[$key]['total'] = 0;
      $totalList[$key]['allTotal'] = 0;
    }
    $totalList['payTotal']['negativeTotal'] = 0;
    foreach($list as $k=>$v){
      if(substr($k,0,2) != $pHeader) {
        if($pHeader != '') {
          $_html[] = $br;
          $_html[] = '<tr>';
          $totalList['NO']['allTotal'] = '';
          $totalList['wkMonth']['allTotal'] = '';
          $totalList['empName']['allTotal'] = '合計';
          $totalList['jobClass']['allTotal'] = '';
          foreach ($totalList as $key => $value) {
            if(!in_array($key, $noFormat)) $value['allTotal'] = number_format($value['allTotal']);
            $_html[] = '<td>'.$value['allTotal'].'</td>';
            $totalList[$key]['allTotal'] = 0;
          }
          $_html[] = '</tr>';
          $_html[] = $br.$br;
          $_html[] = '<tr class="lastTr">';
          $_html[] = '<td></td><td>經辦:</td><td colspan="4"></td>';
          $_html[] = '<td>主任/園長:</td><td colspan="4"></td>';
          $_html[] = '<td>執行長：</td><td colspan="5"></td>';
          $_html[] = '<tr></table>';
          $_html[] = '<h2 style="text-align: center;">財團法人瑪利亞社會福利基金會</h2>';
        }
        $number = 0;

          //頭
        $pHeader = substr($k,0,2);

        $_html[] = '<div class="title2">員工薪資清冊</div>';
        $_html[] = '<div class="title3">'.$rangeDate.'</div>';

        $_html[] = '<table class="tableData" width="1250px;" border="1">';
      }

      foreach($v as $k1=>$v1){
        $totalList['empID']['total'] = count($v1['sub_list']);
        $projTitle = ($k == 'no_proj') ? '計畫編號:無' : '計畫編號:'.$k;
        $_html[] = '<tr><td colspan="4">'.$projTitle.'</td><td colspan="13">名稱:'.$departmentinfo[$k1].'</td></tr>';
    
        $_html[] = '<tr>';
        foreach($header as $k3=>$v3){
          $width = ($headerW[$k3]) ? 'style="width:'.$headerW[$k3].'"' : '';
          $_html[] = '<td class="cLabel" '.$width.'>'.$v3.'</td>';
        }
        $_html[] = '</tr>';

        $_html[] = '<tr>';
        foreach($header as $k3=>$v3) {
          switch ($k3) {
            case 'mBasic':
            case 'food':
            case 'allowance':
            case 'overtime':
              $_html[] = '<td class="cLabel">(+)</td>';
              break;
            case 'absence':
            case 'taxPay':
            case 'jobPay':
            case 'healthPay':
            case 'mRetirePay':
            case 'otherPay':
              $_html[] = '<td class="cLabel">(-)</td>';
              break;
            default:
              $_html[] = '<td class="cLabel"></td>';
              break;

          }
        }
        $_html[] = '</tr>';
        //員工列表
        foreach($v1['sub_list'] as $k2=>$v2){
          $_html[] = '<tr>';
          $number++;
          foreach($header as $k3=>$v3){
            $showVal = '';
            switch ($k3) {
              case 'NO':
                $showVal = $number;
                break;
              case 'mBasic':
                $showVal = $v2['mBasic'] - $v2['food'];
                break;
              case 'jTotal':
                $showVal = $v2['mBasic']+$v2['allowance']+$v2['overtime']-$v2['absence'];
                break;
              case 'payTotal':
                $showVal = $v2[$k3];
                if($showVal < 0) $totalList['payTotal']['negativeTotal'] += $showVal;
                break;
              case 'wkMonth':
                $showVal = (substr($v2['wkMonth'],0,4) - 1911).substr($v2['wkMonth'],4,2);
                break;
              default:
                $showVal = $v2[$k3];
                break;
            }
            if($k3 != 'empID')$totalList[$k3]['total'] += $showVal;
            if(!in_array($k3, $noFormat)) {
              $showVal = number_format($showVal);
              $_html[] = '<td class="cLabel right">'.$showVal.'</td>';
            } else {
              $_html[] = '<td class="cLabel">'.$showVal.'</td>';
            }
            
          }
          $_html[] = '</tr>';
        }
        $_html[] = '<tr>';      

        foreach ($totalList as $key => $value) {
          $showVal = '';
          switch ($key) {
            case 'NO':
            case 'jobClass':
            case 'wkMonth':
              break;
            case 'empName':
              $showVal = '小計';
              break;
            case 'empID':
              $showVal = $value['total'].'人';
              break;   
            default:
              $showVal = $value['total'];
              break;
          }
          if(!in_array($key, $noFormat)) {
            $showVal = number_format($showVal);
            $_html[] = '<td class="cLabel right">'.$showVal.'</td>';
          } else {
            $_html[] = '<td class="cLabel">'.$showVal.'</td>';
          }
          $totalList[$key]['allTotal'] += $value['total'];
          $totalList[$key]['total'] = 0;
        }
        $_html[] = '</tr>';
        $_html[] = $br;
      }

    }

    $_html[] = '<tr>';   
    $totalList['NO']['allTotal'] = '';
    $totalList['wkMonth']['allTotal'] = '';
    $totalList['empName']['allTotal'] = '合計';
    $totalList['jobClass']['allTotal'] = '';
    foreach ($totalList as $key => $value) {
      if(!in_array($key, $noFormat)) {
        $value['allTotal'] = number_format($value['allTotal']);
        $_html[] = '<td class="cLabel right">'.$value['allTotal'].'</td>';
      } else {
        $_html[] = '<td class="cLabel">'.$value['allTotal'].'</td>';
      }
     // $totalList[$key]['allTotal'] = 0;
    }
   
    $_html[] = '</tr>';
    if($totalList['payTotal']['negativeTotal'] != 0) {
      $_html[] = '<tr>';
      foreach ($header as $key => $value) {
        $val = '';
        if($key == 'otherPay') $val = '正數合計:';
        if($key == 'payTotal') $val = number_format($totalList['payTotal']['allTotal']-$totalList['payTotal']['negativeTotal']);
        $_html[] = '<td class="cLabel right">'.$val.'</td>';
      }
      $_html[] = '</tr>';

      $_html[] = '<tr>';
      foreach ($header as $key => $value) {
        $val = '';
        if($key == 'otherPay') $val = '負數合計:';
        if($key == 'payTotal') $val = number_format($totalList['payTotal']['negativeTotal']);
        $_html[] = '<td class="cLabel right">'.$val.'</td>';
      }
      $_html[] = '</tr>';
    }

    $_html[] = $br;
    $_html[] = '<tr class="lastTr">';
    $_html[] = '<td></td><td>經辦:</td><td colspan="4"></td>';
    $_html[] = '<td>主任/園長:</td><td colspan="4"></td>';
    $_html[] = '<td>執行長：</td><td colspan="5"></td>';
    $_html[] = '</tr></table>';
    $_html = implode('', $_html);
    $html = str_replace('{#html#}', $_html, $html);

    //print_r($html);

    $mpdf->SetTitle($rangeDate);
    $mpdf->SetHTMLHeader('<h2 style="text-align: center;">財團法人瑪利亞社會福利基金會</h2>');
    $mpdf->SetWatermarkText('MARIA');
    $mpdf->showWatermarkText = true;
    $mpdf->setFooter('{PAGENO}');
  //  $mpdf->SetHTMLFooter('<p>test</p>');
    $mpdf->WriteHTML($html);
    $mpdf->Output();


  }
    


?>