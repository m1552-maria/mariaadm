<?php	
	// for Access Permission Control
	if (session_status() !==PHP_SESSION_ACTIVE) {
        session_start();
    }
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	//$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	$bE = true;
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "通訊資料設定";
	$tableName = "community";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['community_eid'] = $eid;
	} else $eid = $_SESSION['community_eid'];
	$filter = "empID='$eid'";
	
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "tel";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,relat,relatName,tel,mobile,email");
	$ftAry = explode(',',"ID,通訊類別,關係,聯絡人,電話,手機,EMail");
	$flAry = explode(',',",90,,,,,");
	$ftyAy = explode(',',"id,text,text,text,text,text,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"title,relat,relatName,tel,mobile,email");
	$newftA = explode(',',"通訊類別,關係,聯絡人,電話,手機,EMail");
	$newflA = explode(',',",,,,,60");
	$newfhA = explode(',',",,,,,");
	$newfvA = array($comClass,'','','','','');
	$newetA = explode(',',"select,text,text,text,text,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,relat,relatName,tel,mobile,email");
	$editftA = explode(',',"ID,通訊類別,關係,聯絡人,電話,手機,EMail");
	$editflA = explode(',',",,,,,,60");
	$editfhA = explode(',',",,,,,,");
	$editfvA = array('',$comClass,'','','','','');	
	$editetA = explode(',',"id,select,text,text,text,text,text");
?>