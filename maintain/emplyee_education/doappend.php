<?php
	session_start();
	include("../../config.php");
	include("init.php");	

	//處理上傳的附件
	$attach=array();
	if($_FILES['attach']){//::多檔上傳
		if(!is_dir($ulpath."/".$_SESSION['EE_EID'])){	mkdir($ulpath."/".$_SESSION['EE_EID']);	}
		if(!is_dir($ulpath."/".$_SESSION['EE_EID']."/education")){	mkdir($ulpath."/".$_SESSION['EE_EID']."/education");	}
		foreach($_FILES['attach']["name"] as $k=>$v){
			$finf = pathinfo($v); 
			/*20180424 sean 加入若是檔案名稱有逗號 用底線取代*/
			$_FILES['attach']["name"][$k] = str_replace(',','_',$_FILES['attach']["name"][$k]);
			$dstfn = md5($_FILES['attach']["name"][$k]).'.'.$finf['extension'];
			if($_FILES['attach']["tmp_name"][$k]=="")	continue;
			copy($_FILES['attach']["tmp_name"][$k],$ulpath."/".$_SESSION['EE_EID']."/education/".$dstfn);
			array_push($attach,$_FILES['attach']["name"][$k]);
			
		}
	}else{
		foreach($_FILES as $k=>$v) {
			$fn = $_FILES[$k]['name'];
			if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
			if ($$k != "") {
				if (!copy($$k,"$ulpath$fn2")) { //上傳失敗
					echo '附件上傳作業失敗 !'; exit;
				}	
			}
		}
	}
	//將其他最高學歷拿掉
	$sql="select * from $tableName where isHigh='$_REQUEST[isHigh]' and empID='$_SESSION[EE_EID]'";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$eduId=array();
		while($r=db_fetch_array($rs)){	array_push($eduId,$r['id']);	}
		$sql="update $tableName set isHigh='0' where empID='".$_SESSION['EE_EID']."' and id in (".join(",",$eduId).")";
		db_query($sql);
	}
	$n = count($newfnA);
	$fields=array();
	$valAry=array();
	$isHighC=0;
	for ($i=0; $i<$n; $i++) {
		if($newfnA[$i]=='isHigh' && $isHighC>0)	continue;
		array_push($fields,$newfnA[$i]);
  		switch($newetA[$i]) {
			case "hidden" :
			case "select" :
			case "readonly" :
			case "textbox":
			case "id":
			case "text" :	$fldv = "'".addslashes($_REQUEST[$newfnA[$i]])."'"; break;
			case "textarea" :	$fldv = "'".trim($_REQUEST[$newfnA[$i]])."'"; break;
			case "checkbox" : if(isset($_REQUEST[$newfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = ($_REQUEST[$newfnA[$i]] == '' ? 'NULL' : "'".$_REQUEST[$newfnA[$i]]."'") ; break;
			case "file" : $fldv = "'$fn'"; break;
			case "empID":	$fldv="'".$_SESSION['EE_EID']."'";	break;	
			case "radio" : 	$fldv="'".$_REQUEST[$newfnA[$i]]."'"; 	break;
			case "multiPhoto":	
				$fldv="'".join(",",$attach)."'";	
				break;
	  	}
		array_push($valAry,$fldv);
		if($newfnA[$i]=='isHigh')	$isHighC++;
	}	
	$sql = "insert into $tableName ";
	$sql .= '('.join(",",$fields).") values(".join(",",$valAry).')';
	db_query($sql);
	db_close();
	//echo $sql;exit;
	header("Location: list.php");
?>
