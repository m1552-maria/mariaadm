<?php 
	session_start();
	header("Content-Type:text/html; charset=utf-8");
	include("../../config.php");
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
.photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
.photoPreview .photoDesc { float:left; margin-left:5px; }
.photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
.photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
.photoPreview .photoItem .photoDel { margin:5px 0 0 5px; cursor:pointer; height:20px;}
.photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?php echo $extfiles?>append.js" ></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script>
 <title><?php echo $pageTitle?> - 新增</title>
 <script type="text/javascript">
 $(function() {
	 var icons=[];
	  icons['doc']='icon_doc.gif';
	  icons['docx']='icon_doc.gif';
	  icons['pdf']='icon_pdf.gif';
	  icons['ppt']='icon_ppt.gif';
	  icons['pptx']='icon_ppt.gif';
	  icons['txt']='icon_txt.gif';
	  icons['xls']='icon_xls.gif';
	  icons['xlsx']='icon_xls.gif';
	  icons['zip']='icon_zip.gif';
	  icons['rar']='icon_rar.gif';
 	$(".multiPhoto").change(function(){
		var that = $(this);
		var oTD = that.parents("td").eq(0);
		var fName=$(this.files)[0]['name'];
		var ary=fName.split(".");
		$(this.files).each(function(){
			var fReader = new FileReader();
			fReader.readAsDataURL(this);
			fReader.onloadend = function(event){
				var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);
				oItem.after(oItem.clone(true)).show();
				var img = oItem.find("img").get(0);
				img.style.padding='1';
				if(icons[ary[1]]){
					img.height=16;
					img.src='/images/'+icons[ary[1]];
				}else if(ary[1].toLowerCase()=="jpg" || ary[1].toLowerCase()=="png" || ary[1].toLowerCase()=="gif"){	
					img.height='80';
					img.src=event.target.result;
				}else{
					img.height=16;
					img.src='/images/icon_txt.gif';
				}
				var span=oItem.find("span").get(0);
				$(span).html(fName);
				oItem.get(0).input = that;
				var oNewFile = that.clone(true);
				oNewFile.val('');
				that.hide().after(oNewFile);

			}
		});
	});
	
	$(".photoDel").click(function(){
		var oItem = $(this).parents(".photoItem").eq(0);
		if (oItem.get(0).input) oItem.get(0).input.remove();
		oItem.remove();
	});
  });
 </script>
</Head>

<body class="page" style="overflow:auto">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?php echo $pageTitle?> -【新增作業】</td></tr>

	<?php for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?php echo  $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?php echo $newftA[$i]?></td>
  	<td><?php  switch ($newetA[$i]) { 
			case "empID":	echo $_SESSION['EE_EID'];	break;
			case "readonly": echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea id='$newfnA[$i]' name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea> <input type='button' value='編輯' onClick='HTMLEdit($newfnA[$i])'>"; break;
  			case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' id='$newfnA[$i]' value='$newfvA[$i]' class='optBtn'"; if ($newfvA[$i]=="true") echo " checked"; echo '>是'; break;
			case "radio":	echo "<input  type='radio' name='$newfnA[$i]' value='$newfvA[$i]'>是";	break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : 
				if($newfnA[$i]=='grade' || $newfnA[$i]=='states'){
					$data=array();
					echo "<select id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' class='input'>";
						if($newfnA[$i]=='states'){	
							$data=array('0'=>"畢業","1"=>"肄業","2"=>"就學中");	
							foreach($data as $k=>$v) echo "<option value='$v'>$v</option>"; 
						}else{
							$data=$grade;
							foreach($data as $k=>$v) echo "<option value='$k'>$v</option>";
						}
					echo "</select>"; 
				}else if($newfnA[$i]=="bdate" || $newfnA[$i]=="edate"){
					$Y=date("Y");
					//$initY=$Y-40;	
					$initY=1967;
					echo "<select id='".$newfnA[$i]."_sel_year' size='$newflA[$i]' class='input'>";
						for($j=$initY;$j<=$Y;$j++){
							if($j==$Y){ $sel='selected';}
							echo "<option value='$j' $sel>$j</option>"; 	
						}
					echo "</select>&nbsp;年"; 
					echo "<select id='".$newfnA[$i]."_sel_month' size='$newflA[$i]' class='input'>";
						for($j=1;$j<=12;$j++){	echo "<option value='".str_pad($j, 2,"0",STR_PAD_LEFT)."'>".str_pad($j, 2,"0",STR_PAD_LEFT)."</option>"; 	}
					echo "</select>&nbsp;月"; 
				}else{
					echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=>$v) echo "<option value='$k'>$v</option>"; echo "</select>"; 
				}
				break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
			case "multiPhoto":
				echo "<input type='file' name='".$newfnA[$i]."[]' size='$newflA[$i]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview' >";
				echo "<div class='photoItem'><img /><span id='fName'></span><a class='photoDel' href='#' >刪除</a><div class='clearfx'></div></div>";
				echo "</div>";
				break;	
			
  	} ?></td>
  </tr><?php } ?>
  <tr>
  	<td colspan="2" align="center" class="rowSubmit">
    <input type='hidden' id='id' name='id' value="<?php echo $_SESSION['EE_EID']?>">
    <input type='hidden' id='bdate' name='bdate'>
    <input type='hidden' id='edate' name='edate'>
    <input type="submit" value="確定新增" class="btn">&nbsp;
  	<input type="reset" value="重新輸入" class="btn">&nbsp;
  	<input type="button" class="btn" onClick="history.back()" value="取消新增">
   	</td>
  </tr>
</table>
</form>
</body>
</html>
