<?php 
	// for Access Permission Control
	if(!session_id()) session_start();
	if ($_SESSION['privilege']<10) {
	  	header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	if((isset($_SESSION["canEditEmp"]) && $_SESSION['privilege']<=10) || $_SESSION['privilege']>10){
		$bE2=true;
	}else{
		$bE2=false;
	}
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "學歷";
	$tableName = "emplyee_education";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['EE_EID'] = $eid;
	} else $eid = $_SESSION['EE_EID'];
	$filter = "empID='$eid'";
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "grade";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,grade,subject,bdate,edate,isHigh,states,notes,rdate");
	$ftAry = explode(',',"ID,學校名稱,學歷,科系,期間(起),期間(迄),學歷,狀態,備註,登錄日期");
	$flAry = explode(',',"80,100,80,100,,,,,100,");
	$ftyAy = explode(',',"id,text,text,text,date,date,bool,text,text,text");
	
	$isHighAry=array("0"=>'一般','1'=>'最高','2'=>'次高');
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"empID,title,grade,subjectClass,subject,bdate,edate,isHigh,states,notes,attach");
	$newftA = explode(',',"員工編號,學校名稱,學歷,科系類別,科系,期間(起),期間(迄),學歷,狀態,備註,相關文件上傳");
	$newflA = explode(',',",,,,,,,,,30,");
	$newfhA = explode(',',",,,,,5,,,,3,");
	$newfvA = array('','','',$subjectClass,'','','',$isHighAry,'','','');
	$newetA = explode(',',"empID,text,select,select,text,date,date,select,select,textbox,multiPhoto");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,grade,subjectClass,subject,bdate,edate,isHigh,states,notes,attach");
	$editftA = explode(',',"ID,學校名稱,學歷,科系類別,科系,期間(起),期間(迄),學歷,狀態,備註,相關文件上傳");
	$editflA = explode(',',",,,,,,,,,30,");
	$editfhA = explode(',',",,,,,5,,,,3,");
	$editfvA = array('','','',$subjectClass,'','','',$isHighAry,'','','');
	$editetA = explode(',',"id,text,select,select,text,date,date,select,select,textbox,multiPhoto");
?>