<?php 
	// for Access Permission Control
	if(!session_id()) session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "員工眷屬清單";
	$tableName = "emplyee_dependents";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['emplyee_dependents_eid'] = $eid;
	} else $eid = $_SESSION['emplyee_dependents_eid'];
	$filter = "empID='$eid'";
	
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "name";
	$searchField2 = "title";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,name,title,birthday,grants,health_rate,bdate,edate");
	$ftAry = explode(',',"ID,姓名,稱謂,生日,補助類別,健保補助比例,加保日,退保日");
	$flAry = explode(',',",90,,,,,,");
	$ftyAy = explode(',',"id,text,text,date,text,text,date,date");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"name,title,birthday,perID,grants,health_rate,bdate,edate");
	$newftA = explode(',',"姓名,稱謂,生日,身分字號,補助類別,健保補助比例,加保日,退保日");
	$newflA = explode(',',",,,,,,,");
	$newfhA = explode(',',",,,,,,,");
	$newfvA = array('','','','','','','','');
	$newetA = explode(',',"text,text,date,text,text,text,date,date");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,name,title,birthday,perID,grants,health_rate,bdate,edate");
	$editftA = explode(',',"ID,姓名,稱謂,生日,身分字號,補助類別,健保補助比例,加保日,退保日");
	$editflA = explode(',',",,,,,,,,");
	$editfhA = explode(',',",,,,,,,,");
	$editfvA = array('','','','','','','','','');	
	$editetA = explode(',',"id,text,text,date,text,text,text,date,date");
?>