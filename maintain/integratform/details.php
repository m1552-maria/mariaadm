<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	include('../../getEmplyeeInfo.php');	

	$id = $_REQUEST[id];
	$rs = db_query("select * from integratform where id=$id");
	if($rs) $r=db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>
	function formvalid(tfm) {
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	</script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	background-color:#f8f8f8;
}
</style>
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="4" cellspacing="1" class="formTx">
        <tr>
          <td align="right" bgcolor="#F0F0F0">申辦人員：</td>
          <td><?=$emplyeeinfo[$r[empID]]?></td>
          <td align="right" bgcolor="#F0F0F0">類別：</td>
          <td><?=$r[aType]?></td>
          <td align="right" bgcolor="#F0F0F0">申請單位：</td>
          <td><?=$departmentinfo[$r["depID"]]?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#F0F0F0">統編：</td>
          <td><?=$r[invNo]?></td>
          <td align="right" bgcolor="#F0F0F0">專案編號：</td>
          <td><?=$r[prjNo]?></td>
          <td align="right" bgcolor="#F0F0F0">申請時間：</td>
          <td><?=$r[rDate]?></td>
        </tr>
                  
        <tr>
          <td align="right" bgcolor="#F0F0F0">需求期間：</td>
          <td colspan="3"><?=$r[bDate]?> ~ <?=$r[eDate]?></td>
          <td align="right" bgcolor="#F0F0F0">辦理狀態：</td>
          <td><?=$r[rspState]?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#F0F0F0">需求內容：</td>
          <td colspan="5"><?= nl2br($r[content])?></td>
          </tr>
        <tr>
          <td align="right" bgcolor="#F0F0F0">需求說明：</td>
          <td colspan="5"><?= nl2br($r[description])?></td>
        </tr>

        <tr valign="top">
          <td align="right" bgcolor="#F0F0F0">承辦單位：</td>
          <td><?=$departmentinfo[$r[rspDep]]?></td>
          <td align="right" bgcolor="#F0F0F0">承辦人：</td>
          <td><?=$emplyeeinfo[$r[rspMan]]?> <img src="<?=$r[is_rspMan]?'checked.gif':'uncheck.gif'?>" align="absmiddle"></td>
          <td align="right" bgcolor="#F0F0F0">承辦單位主管：</td>
          <td><?=$emplyeeinfo[$r[rspLead]]?>
            <img src="<?=$r[rspLead]?'checked.gif':'uncheck.gif'?>" align="absmiddle" />
          <? if($r[is_rspLead]) echo date('Y/m/d H:i',strtotime($r['is_rspLead_date'])) ?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#F0F0F0">辦理情形：</td>
          <td colspan="5"><?=$r[rspContent]?></td>
        </tr>            
      </table>
    </td>
  </tr>
  
</table>
</body>
</html>