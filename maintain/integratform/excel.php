<?php
if($_REQUEST[sqltx]) {
	include("../../config.php");
	include('../../getEmplyeeInfo.php');	

	$sql = $_REQUEST[sqltx];
	//echo $sql; exit;
	$sql .= " order by empID,aType";
	$rs = db_query($sql);
	/*
	$filename = date("Y-m-d H:i:s",time());
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename.xls"); */
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <table width="100%" border="1" cellpadding="2" cellspacing="0" bordercolordark="#FFFFFF" style="font-size:12px">
  	<tr><td colspan="13" height="36" style="font-size:24px" align="center">員工請假 月報表</td><tr>
    <tr><td colspan="2">製作日期：</td><td colspan="11"><?=date('Y-m-d')?></td><tr>
    <tr><td colspan="2">月報表期間：</td><td colspan="11">
			<? $reg = "((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))";
				 preg_match($reg, $sql, $match);
				 $aa = explode('/',$match[0]);
				 echo "$aa[0] 年 $aa[1] 月"; 
			?>
    </td><tr>
    <tr bgcolor="#C0C0C0">
      <td>流水號</td>
      <td>員工編號</td>
      <td>員工名稱</td>
      <td width="120">請假時間</td>
      <td width="120">起始日期</td>
      <td width="120">結束日期</td>
      <td>請假時數</td>    
      <td>假別</td>
      <td>是由</td>
      <td>辦理情形</td>
      <td>已准假</td>
      <td>已申請銷假</td>
      <td>已銷假</td>
    </tr>
 <? $count  = 1; $curEmp = ''; $curTyp = ''; $color = false; $tHours=0;
		while($rs && $r=db_fetch_array($rs)) {
			$eid = $r[empID];
			if($curEmp!=$eid) {
				$curEmp = $eid;
				$color = !$color;
				$curTyp = ''; 
			}
		
			if($curTyp!=$r[aType]) {
				$curTyp = $r[aType];
				if($count!=1) echo "<tr bgcolor='$trColor'><td colspan='6' align='right'>$curTyp 時數合計：</td><td>$tHours</td><td colspan='6'>&nbsp;</td></tr>";
				$tHours = 0;		
			} 
			$tHours += intval($r[hours]);
			$trColor = $color?'#F8F8F8':'#E0E0E0';
 ?>
    <tr bgcolor="<?=$trColor?>">
      <td><?=$count?></td>
      <td>&nbsp;<?=$eid?></td>
      <td><?=$emplyeeinfo[$eid]?></td>
      <td><?=$r[rdate]?></td>
      <td><?=$r[bDate]?></td>
      <td><?=$r[eDate]?></td>
      <td><?=$r[hours]?></td>        
      <td><?=$r[aType]?></td>
      <td><?=$r[content]?></td>
      <td><?=$r[rspContent]?></td>
      <td><?=$r[isOK]?'v':''?></td>
      <td><?=$r[isReqD]?'v':''?></td>
      <td><?=$r[isDel]?'v':''?></td>
    </tr>  
  <? $count++; } ?>  
  </table>
<? } ?>