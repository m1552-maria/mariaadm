<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
		
	$bE = false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "綜合申請單";
	$tableName = "integratform";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/integratform/';
	$delField = 'ahead';
	$delFlag = false;
	$imgpath = '../../data/integratform/';
	
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	$temp = array();
	if ($_SESSION['privilege'] > 10) {	//::check if from maintainUser 
		if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	}	else {
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "depID=''";
	}
	
	if($_REQUEST['aType']) $temp[] = "aType='$_REQUEST[aType]'";
	
	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "empID";
	$pageSize = 10;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,fmNo,empID,aType,rDate,bDate,eDate,content,rspDep,rspMan,rspLead,rspState,rspContent");
	$ftAry = explode(',',"單號,流水單號,申辦員工,類別,請假日期,起始日期,結束日期,事由,承辦單位,承辦人,承辦主管,辦理狀態,辦理情形");
	$flAry = explode(',',"70,,80,,,,,,,,,");
	$ftyAy = explode(',',"ID,text,empID,text,date,datetime,datetime,text,rspDep,empID,empID,text,text");
?>