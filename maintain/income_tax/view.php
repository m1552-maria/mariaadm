<?
  include("getData.php");
   // $search_list = array("e.empID='00225'");
  $result = array();
  switch ($type) {
    case '':
      $result = getAllData($year, $search_list, $uniformNumbers);
      break;
    case 'isMerge':
      $result = getAllData($year, $search_list, $uniformNumbers, 1);
      break;
    case 'salary':
      $result = getSalary($year, $search_list, $uniformNumbers);
      break;
    case 'bonus':
      $result = getBonus($year, $search_list, $uniformNumbers);
      break;
    case 'excel':
      $result = getIncomeTaxDB($year, $search_list, $uniformNumbers, 'excel');
      break;
  }
  
  $dataList = $result['data'];
  $con = 0;
  foreach($dataList as $kkk => $second){ 
    foreach ($second as $empID => $third) {
      foreach ($third as $k => $fourth) {
        foreach ($fourth as $kk => $data) {
          $con++;
        }
      }
    }
  }


?> 
<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?></title>
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link href="tuning.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script type="text/javascript" src="tuning.js" ></script> 
 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  /*function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(yrflt) url = url+'&year='+yrflt;

		location.href = url;		
	}*/

	/*$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<? if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[empID]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
		
		
	});	*/
	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}


 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
 <style type="text/css">
  .popupPane {
    position:absolute; 
    left: 25%; top:5%;
    background-color:#888;
    padding: 8px;
    display:none;
  }
 </style>
</Head>

<body>
  <div id="popup" class="popupPane">
<form id="fm" action="importExcel.php" method="post" enctype="multipart/form-data" style="margin:0">
  <input type="hidden" name="year" value="<?=$year?>">
  <input type="hidden" name="type" value="<?=$type?>">
  <input type="hidden" name="uniformNumbers" value="<?=$uniformNumbers?>">
  <input type="hidden" name="keyword" value="<?=$keyword?>">
  <table bgcolor="#F0F0F0" style="font-size:12px">
  <tr><td>選擇要匯入的Excel檔：<br/><span style="color:#666666; font-size:12px">請選擇格式為 xls 的 excel 檔案，內容必須遵守範本的排法。</span><br/><a href="templet.xls" target="new">下載範本</a></td></tr>
  <tr><td><input type="file" name="file1" size="30" /></td></tr>
  <tr height="40" valign="bottom"><td>
    <input type="submit" name="Submit" value="匯入">
    <button type="button" onClick="document.getElementById('popup').style.display='none'">取消</button>
  </td></tr>
  </table>
</form></div>

<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<!--<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">-->
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <input type="submit" name="action" value="Ｘ刪除" class="sBtn" onClick="MsgBox()">
    年度：
    <select name="year" id="year" onChange="doSubmit()">
    	<? foreach ($yearList as $key => $value) {
    		$sel = ($year == $key) ? 'selected':'';
    	 	 echo "<option $sel value='".$key."'>".$value." 年</option>"; 
    	} ?>
    	
    </select>

    統編：
    <select name="uniformNumbers" id="uniformNumbers" onChange="doSubmit()">
      <option value="">-全部-</option>
      <? foreach ($UniformNumbersProjID as $key => $value) {
        $sel = ($uniformNumbers == $key) ? 'selected':'';
         echo "<option $sel value='".$key."'>".$key."</option>"; 
      } ?>
    </select>

    類別：
    <select name="type" id="type" onChange="doSubmit()">
      <option value="">全部(各別顯示)</option>
      <option value="isMerge" <?php if($type=='isMerge') echo 'selected';?>>全部(合併所得格式)</option>
      <option value="salary" <?php if($type=='salary') echo 'selected';?>>固定薪資</option>
      <option value="bonus" <?php if($type=='bonus') echo 'selected';?>>三節/其他獎金</option>
      <option value="excel" <?php if($type=='excel') echo 'selected';?>>EXCEL匯入</option>
    </select>
     <input type="button" name="ExcelIn" value="Excel匯入" onClick="document.getElementById('popup').style.display='block'">
    <input type="button" value="匯出媒體檔" onclick="excelfm2.submit()">
    
  	</td>
  	<td align="right">總筆數：<?=$con?><?php if ($keyword!="") echo " / 搜尋：$keyword "; ?></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
    <td><label><input type="checkbox" id="allCheck" value="1"></label></td>
<?php 
  foreach($fields as $key => $val){ 
    echo '<td>'.$val.'</td>';
  }
?>


  </tr>
<?php 
  foreach($dataList as $uNum => $second){ 
    echo '<tr><td colspan="'.(count($fields)+1).'" style="font-weight:bold;font-size: 13px;">統編：'.$uNum.'</td></tr>';
    foreach ($second as $empID => $third) {
      foreach ($third as $k => $fourth) {
        foreach ($fourth as $kk => $data) {
          echo '<tr valign="top">';
          echo '<td>';
          if($data['isClose'] == 0 && $type != 'isMerge') echo '<input type="checkbox" name="ID[]" value="'.$data['id'].'">';
          echo '</td>';
          foreach ($fields as $key => $value) {
            echo '<td>'.$data[$key].'</td>';
          }
          echo '</tr>';
        }
      }
    }
  }
?>


 </table>
</form>


<form name="excelfm2" action="mediaFiles.php" method="post" target="excel">
	<input type="hidden" name="year" value="<?=$year;?>">
  <input type="hidden" name="uNum" value="<?=$uniformNumbers;?>">
</form>

<script type="text/javascript">
	var rztExcel = false;
	function getExcelV(){
		var url = 'selectExcel.php?'; 
        if(rztExcel){
        	rztExcel.close();
        }
        if(rztExcel == false){
        	rztExcel = window.open(url, "web",'width=600,height=750');
        }else{
        	rztExcel = window.open(url, "web",'width=600,height=750');
        	rztExcel.focus();
        }
	}
  $(function(){
    $('#allCheck').click(function(){
      var checked = $(this).prop('checked');
      $('[name^="ID"]').each(function(){
        $(this).prop('checked', checked);
      });
    });
  })
</script>


</body>
</Html>