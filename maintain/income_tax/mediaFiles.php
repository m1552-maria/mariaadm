<?php
	@session_start();
	header("Content-Type:text/html; charset=utf-8");
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../inc_vars.php';	
	include 'bankClass.php';
	include("getData.php");
	$year = $_POST['year'];
	$uNum = $_POST['uNum'];
	$result = getAllData($year, array(), $uNum, 1);

	$dataList = $result['data'];

	$mediaList = array();
	foreach ($dataList as $uniformNumbers => $first) {
		$i = 0;
		$sNum = $year;
		foreach ($first as $perID => $second) {
			foreach ($second as $incomeType => $third) {
				foreach ($third as $key => $data) {
					$i++;
					$mediaList[$uniformNumbers.'.'.$data['payYear'].'.U8'][$i] = array(
				/*1*/	'checkAgeCode' => 'B48', //稽徵機關代號
				/*2*/	'sNum' => $year.str_pad($i,4,'0',STR_PAD_LEFT), //流水號
				/*3*/	'uNum' => $uniformNumbers, //扣繳單位統一編號
				/*4*/	'incomeNotes' => '', //所得註記
				/*5*/	'incomeType' => $incomeType, //所得格式
				/*6*/	'perID' => $perID, //所得人統一編號
				/*7*/	'cNum' => $data['cNum'], //證號別
				/*8*/	'payTotal' => $data['payTotal'], //給付總額
				/*9*/	'payTax' => $data['payTax'], //扣繳稅額
				/*10*/	'payMoney' => $data['payMoney'], //給付淨額
				/*11*/	'incomeNum' => $data['incomeNum'], // 所得人代號、租任房屋稅籍編號等等
				/*12*/	'isBlank1' => '', // 固定為空值
				/*13*/	'isBlank2' => '', // 固定為空值
				/*14*/	'software' => 'A', //軟體註記
				/*15*/	'errorNotes' => '', //錯誤註記
				/*16*/	'year' => $data['payYear'], //給付年度
				/*17*/	'name' => $data['name'], //姓名
				/*18*/	'address' => $data['address'], //地址
				/*19*/	'dateRange' => $data['payYear'].'01'.$data['payYear'].'12', //所得所屬期間
				/*20*/	'mRetirePay' => $data['mRetirePay'], //
				/*21*/	'isBlank3' => '', //固定為空值
				/*22*/	'isBlank4' => '', //固定為空值
				/*23*/	'isBlank5' => '', //固定為空值
				/*24*/	'isBlank6' => '', //固定為空值
				/*25*/	'isBlank7' => '', //扣繳稅額註記 固定為空值
				/*26*/	'sendType' => (($data['cNum'] == 1) ? '3' : '1'), //憑單發放方式 1:免填發、2:電子憑單、3:紙本憑單
				/*27*/	'is183' => '', //是否住滿183天
				/*28*/	'place' => '', //居住地代碼
				/*29*/	'isBlank8' => '', // 居住稅協定碼
				/*30*/	'isBlank9' => '', //固定為空值
				/*31*/	'rDate' => str_pad(date('m'),2,'0',STR_PAD_LEFT).str_pad(date('d'),2,'0',STR_PAD_LEFT), //檔案製作日期
				/*32*/	'TIN' => '', //稅務識別碼
					);
					/*switch ($incomeType) {
						case '50':
							break;
						case '51':
							break;
						case '53':
							break;
						case '91':
							break;
						case '92':
							break;
						case '9A':
							break;
						case '9B':
							break;
					}*/

				}
			}
		}
	}

	/*echo "<pre>";
	print_r($mediaList);
	exit;*/

	$filename = array();
	foreach ($mediaList as $key => $value) {
		$fp = fopen($key, 'a');
		foreach ($value as $k => $v) {
			fwrite($fp, implode('|', $v)."\r\n");
		}
		fclose($fp);
		$filename[] = $key;
	}

	
	$zipname = date("Ymd").'.zip';
	$zip = new ZipArchive;
	$zip->open($zipname, ZipArchive::CREATE);
	foreach ($filename as $key => $value) $zip->addFile($value);
	$zip->close();

	$dw = new download($zipname); //下載文檔
	unlink($zipname); //下載完成後要進行刪除
	foreach ($filename as $key => $value) unlink($value);


?>