<?php
	@session_start();
	header("Content-type: text/html; charset=utf-8");
	if($_REQUEST['Submit']) {
		if(empty($_POST['year'])) {
			echo '請輸入匯入年份'; exit;
		}
		include 'init.php';
		$year = (int)$_POST['year'];
		$sql = "select count(*) as con from ".$tableName." where year='".$year."' and isClose=1";
		$rs=db_query($sql);
   		$r=db_fetch_array($rs);
   		if($r['con'] > 0) {
   			echo $_POST['year'].'綜合所得稅 已經申報結束'; exit;
   		}


		//處理上傳的檔案
		if ($_FILES[file1][tmp_name] != "") {
			$fsrc = $ulpath.'import.xlsx';

			if (!copy($_FILES[file1][tmp_name],$fsrc)) { //上傳失敗
				echo '附件上傳作業失敗 !'; exit;
			}
		}		
		
		require_once '../Excel/reader.php';

		$data = new Spreadsheet_Excel_Reader();  
		//設定輸出編碼，指的是從excel讀取後再進行編碼
		$data->setOutputEncoding('UTF-8');  
		//載入要讀取的檔案
		$data->read($fsrc);  

		$heard = array(
			'uNum' => '機構統編',
            'name' => '所得人',
           	'cNum' => '證號別',
            'perID' => '統一編號',
            'address' => '所得人住址',
            'year' => '所得所屬年度',
            'payTotal' => '給付總額',
            'payTax' => '扣繳稅額',
            'payMoney' => '給付淨額',
            'incomeType' => '所得格式',
            'noInsert1' => '傳票號碼',
            'title' => '科目名稱[中文]',
            'notes1' => '備註',
            'noInsert2' => '會科編號',
            'noInsert3' => '所得類別',
            'noInsert4' => '給付日期',
            'incomeNum' => '分類代號',
            'noInsert6' => '補充保費',
            'wkDate' => '日期(格式化)',
            'noInsert7' => 'Prn_YM',
            'notes2' => '備註[依用戶設定]',
		);
		$i = 0;
		$importHeard = $data->sheets[0]['cells'][1];
		foreach ($heard as $k => $v) {
			$i++;
			if($importHeard[$i] != $v) {
				print_r('Excel 格式錯誤');
				exit;
			}
			
		}

		$inserData = array();
		$errorData = array();
		$perIDList = array();
		$typeList = array('50','53','91');
		foreach ($data->sheets[0]['cells'] as $key => $value) {
			if($key == '1') continue;
			$dateList = explode('/', $value[19]);
			$inYear = (int)$dateList[0]+1911;
			if($inYear > $year || ($inYear < $year && $dateList[1] != '12')){
				$errorData[] = '第'.$key.'列資料日期不在此年度當中';
				continue;
			}

			$i = 0;
			$fields = array();
			foreach ($heard as $k2 => $v2) {
				$i++;
				if(strpos((string)$k2, 'noInsert') === 0) continue;
				$insertVal = '';
				switch ($k2) {
					case 'year':
						$insertVal = $year;
						break;
					case 'incomeNum': 
						$insertVal = trim($value[$i]);
						//如果是這些所得格式 incomeNum 就為"所得人統編"
						if(in_array(trim($value[10]), $typeList)) $insertVal = trim($value[4]);
						break;
					default:
						$insertVal = trim($value[$i]);
						break;
				}


				$fields[] = "`".$k2."`='".$insertVal."'";
			}
			$fields[] = "`type`='excel'";
			$source = (trim($value[13]) != '') ? trim($value[13]) : trim($value[21]);
			$fields[] = "`source`='".$source."'";
			$perIDList[$value[4]] = "'".$value[4]."'";
			//$inserData[$value[4]] = $fields;
			$inserData[] = $fields;
		}

		if(count($errorData) > 0) {
			foreach ($errorData as $key => $value) {
				echo $value.'<br>';
			}
			exit;
		}

		$sql = "select perID,empID from emplyee where perID in(".implode(',', $perIDList).") order by empID desc";
		$rs=db_query($sql);
	
		/*$empList = array();
   		while($r=db_fetch_array($rs)) {
   			$empList[$r['perID']] = $r['empID'];
		}*/

		foreach ($inserData as $perID => $value) {
			$data = $value;
			//if(isset($empList[$perID])) $data[] = "`incomeNum`='".$empList[$perID]."'";
			$sql = "insert into ".$tableName." set ".implode(',', $data);
			db_query($sql);
		}

		$url = array();
		foreach ($_POST as $key => $value) {
			if($value != '') $url[] = $key.'='.$value;
		}
		//if($_POST['type'] != '') $url .= '&type='.$_POST['type'];
		header("Location: list.php?".implode('&', $url));


	}

?>