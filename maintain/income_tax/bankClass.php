<?php
	/* 下載ZIP */
	class download{
		protected $_filename;
		protected $_filepath;
		protected $_filesize;//文檔大小
		public function __construct($filename){
			$this->_filename = $filename;
			$this->_filepath = dirname(__FILE__).'/'.$filename;
			$this->getfiles();
		}
		public function getfilepath(){
			return $this->_filepath;
		}
		public function getfiles(){ //下載文檔的功能
			if (file_exists($this->_filepath)){ //檢查文檔是否存在
				$file = fopen($this->_filepath,"r");//打開文檔
				Header("Content-type: application/octet-stream");
				Header("Accept-Ranges: bytes");
				Header("Accept-Length: ".filesize($this->_filepath));
				Header("Content-Disposition: attachment; filename=".$this->_filename);
				echo fread($file, filesize($this->_filepath));//修改之前，一次性將數據傳輸給客户端
				$buffer=1024;//修改之後，一次只傳輸1024個字節的數據給客户端 向客户端回送數據
				//判斷文檔是否讀完
				while (!feof($file)) {
					$file_data=fread($file,$buffer);//將文檔讀入內存
					echo $file_data;//每次向客户端回送1024個字節的數據
				}
				fclose($file);
			}
		}
	}

?>