<?	
	session_start();
  	header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	include ('../inc_vars.php');


	$bE = false;	//異動權限	
	$pageTitle = "媒體作業";
	$tableName = "income_tax";

	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/income_tax/';
	$delField = '';
	$delFlag = false;


	$year = date('Y');
	$yearList = array();
	for ($i=$year; $i >= ($year-3); $i--) { 
		$yearList[$i] = $i;
	}

	



	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	/*if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="b.depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "b.depID=''";
	}*/
	$projIDFilter = '';
	if($_REQUEST['projIDFilter']) {
		$pList = explode(',', $_REQUEST[projIDFilter]);
		$aa = array();
		foreach ($pList as $v) {
			if(trim($v) != '') $aa[]="e.projID like '$v%'";	
		}
		$temp[] = "(".join(' or ',$aa).")"; 	
		$projIDFilter = $_REQUEST[projIDFilter];
	} elseif ($_SESSION['privilege'] <= 10) {
		if($_SESSION['user_classdef'][7]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][7] as $v){
				if($v)	$aa[]="e.projID like '$v%'";	
			}
			$temp[] = "(".join(' or ',$aa).")"; 	
		} else {
			$temp[] = "e.projID='-1'";
		}
	}

	if(!empty($_REQUEST['year'])){
		//$temp[] = "r.wkYear='$_REQUEST[year]'";	
		$year = $_REQUEST['year'];
	} else {
		//$temp[] = "r.wkYear='".$year."'";	
	}

	$uniformNumbers = (!empty($_REQUEST['uniformNumbers'])) ? $_REQUEST['uniformNumbers'] : '';
	$type = (!empty($_REQUEST['type'])) ? $_REQUEST['type'] : '';

	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //

	$searchField1 = "e.perID";//勿改
	$searchField2 = "e.empName";//勿改
	$sortDirection = 'ASC';

	$pageSize = 20;	//每頁的清單數量

	$fields = array(
		'uNum' => '統編',
		'name' =>'姓名',
		'title' => '科目',
		'incomeNum' => '所得人代號',
		'perID' => '身分證號',
		'cNum' => '證號別',
		//'incomeNotes' => '所得註記',
		'incomeType' => '所得格式',
        'payTotal' => '給付總額(扣自提)',
        'payTax' => '稅額',
        'payMoney' => '給付淨額',
        'mRetirePay' => '勞退自提',
        'payYear' => '給付年度',
        'address' => '地址',
        'projID' => '計劃',
        'source' => '來源',
	);

	// 注意 primary key shpuld be first one
	// $fnAry = explode(',',"empID,empName,depID,spHolidays,actDays,trueDays,action");
	// $ftAry = explode(',',"編號,姓名,部門,今年度特休,會計年資,實際年資,調整");
	// $flAry = explode(',',"50,,,,,,,,,");
	// $ftyAy = explode(',',"id,text,text,text,text,text,text,text,text");
?>