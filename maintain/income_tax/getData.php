<?php 
  function getAllData($year, $search_list = array(), $UniformNumbers = '', $isMerge = 0) {
    $result = array();
    $salaryData = getSalary($year, $search_list, $UniformNumbers);
    $result['data'] = $salaryData['data'];
    $bonusData = getBonus($year, $search_list, $UniformNumbers);
    foreach ($bonusData['data'] as $uniformNumbers => $second) {
      foreach ($second as $perID => $third) {
        foreach ($third as $incomeType => $value) {
          if($isMerge == 1) {
            if(!isset($result['data'][$uniformNumbers][$perID][$incomeType])) {
              $result['data'][$uniformNumbers][$perID][$incomeType][] = $value[0];
            } else {
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['title'].= '/'.$value[0]['title'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['payTotal']+= $value[0]['payTotal'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['payTax']+= $value[0]['payTax'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['payMoney']+= $value[0]['payMoney'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['source'].= $value[0]['source'];
            }

          } else {
             $result['data'][$uniformNumbers][$perID][$incomeType][] = $value[0];
          }

                      
        }
      }
    }

    $excelData = getIncomeTaxDB($year, $search_list, $UniformNumbers, 'excel');
    foreach ($excelData['data'] as $uniformNumbers => $second) {
      foreach ($second as $perID => $third) {
        foreach ($third as $incomeType => $value) {
          if($isMerge == 1) {
            $continueKey = -1;
            if(!isset($result['data'][$uniformNumbers][$perID][$incomeType])) {
              $result['data'][$uniformNumbers][$perID][$incomeType][0] = $value[0];
              $continueKey = 0;
            } 
            foreach ($value as $k => $v) {
              if($k == $continueKey) continue;
              if(!preg_match("/".$v['title']."/i", $result['data'][$uniformNumbers][$perID][$incomeType][0]['title'])) $result['data'][$uniformNumbers][$perID][$incomeType][0]['title'].= '/'.$v['title'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['payTotal']+= $v['payTotal'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['payTax']+= $v['payTax'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['payMoney']+= $v['payMoney'];
              $result['data'][$uniformNumbers][$perID][$incomeType][0]['source'].= '<br>'.$v['source'];
            }

          } else {
            if(!isset($result['data'][$uniformNumbers][$perID][$incomeType])) {
              $result['data'][$uniformNumbers][$perID][$incomeType] = $value;
            } else {
              $result['data'][$uniformNumbers][$perID][$incomeType] = array_merge($result['data'][$uniformNumbers][$perID][$incomeType],$value);
            }

          }
          
        }
      }
    }
    return $result;

  }

  function getSalary($year, $search_list = array(), $UniformNumbers = '') {
    global $UniformNumbersProjID, $county;
    $result = array('data'=>array());

    $result = getIncomeTaxDB($year, $search_list, $UniformNumbers, 'salary');
    if(count($result['data'])) {
      return $result;
    }


    //取每月薪資
    $sql = "select r.*, r.id as rID, e.empName as name, e.perID, e.perID as incomeNum, e.county, e.city, e.address 
    from emplyee_salary_report as r left join emplyee as e on r.empID=e.empID ";
    $sql.= " where r.wkMonth >= '".($year-1)."12' and r.wkMonth <= '".$year."11' ";
    if(count($search_list)>0) $sql.= " and ".implode(' and ', $search_list);
    $sql.= " order by e.empID, r.wkMonth";
    $rs = db_query($sql);
    $empIDList = array();
    $dataList = array();
    $rIDList = array();
    while ($r=db_fetch_array($rs)) {
      $rIDList[] = $r['rID'];
      $r['source'] = substr($r['wkMonth'], -2).'月';
      $r['address'] = trim($r['address']);
      if(!preg_match("/".$county[$r['county']]."/i", $r['address']) && !preg_match("/".$r['city']."/i", $r['address'])) {
        $r['address'] = $county[$r['county']].$r['city'].$r['address'];
      }
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $addsubList = array(
          'allowance' =>$r['addTotal'], //津貼及加項 +
          'overtime' =>0, //加班費 +
          'absence' =>0, //請假調整 -
          'taxPay' =>0, //所得稅 -
          'jobPay' =>0, //勞保 -
          'healthPay' =>0, //健保 -
          'mRetirePay' =>0, //自提退休金 -
          'otherPay' =>$r['subTotal'] //其他代扣 -
      );

       $dataList[$r['empID']][$r['wkMonth']] = array_merge($r,$addsubList);
    }


    //取得薪資其他加減項
    $sql2 = "select * from emplyee_salary_items where fid in (".implode(',', $rIDList).")";
    $rs2=db_query($sql2);
    while ($r2=db_fetch_array($rs2)) {
      $itemList[$r2['fid']][$r2['id']] = $r2;
    }

    foreach ($dataList as $empID => $second) {
      foreach ($second as $wkMonth => $third) {
        if(!isset($itemList[$third['rID']])) continue;
        foreach ($itemList[$third['rID']] as $k2 => $r2) {
            if($r2['addsub'] == '+') {
                switch ($r2['iType']) {
                  case 'absence':
                    $dataList[$empID][$wkMonth]['absence'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;
                  case 'overtime':
                    $dataList[$empID][$wkMonth]['overtime'] += $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;  
                  case 'otherfee': //雜項費用
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;  
                  default:
                    switch ($r2['title']) {
                      case '勞保自負額':
                        $dataList[$empID][$wkMonth]['jobPay'] -= $r2['money'];
                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                        break;
                      case '健保自負額':
                        $dataList[$empID][$wkMonth]['healthPay'] -= $r2['money'];
                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                        break;
                      case '勞退自提額':
                        $dataList[$empID][$wkMonth]['mRetirePay'] -= $r2['money'];
                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                        break;
                      break;
                    }
                }
                    
            } else {
                switch ($r2['iType']) {
                  case 'absence':
                    $dataList[$empID][$wkMonth]['absence'] += $r2['money'];
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    break;
                  case 'overtime':
                    $dataList[$empID][$wkMonth]['overtime'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    break;
                  case 'allowance':
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;
                  default:
                    switch ($r2['title']) {
                      case '所得稅':
                          $dataList[$empID][$wkMonth]['taxPay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;
                      case '勞保自負額':
                          $dataList[$empID][$wkMonth]['jobPay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;
                      case '健保自負額':
                          $dataList[$empID][$wkMonth]['healthPay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;
                      case '勞退自提額':
                          $dataList[$empID][$wkMonth]['mRetirePay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;   
                      break;
                    }   
                }
            }
        }
      }
    }

    //取員工每月的計劃ID&部門ID
    $empHistory = getEmpHistory($empIDList, $UniformNumbers);
    //塞入計劃ID&部門ID
    $dataList = setProjIDAndDepID($dataList, $empHistory);

    $perIDUniformNumbers = array();
    foreach ($UniformNumbersProjID as $key => $value) {
      if($UniformNumbers != '' && $key != $UniformNumbers) continue;
      $p = explode('/', $value);
      foreach ($p as $k => $v) {
        $perIDUniformNumbers[$v] = $key;
      }
    }
    //塞入統編&依照統編排序
    foreach ($dataList as $empID => $second) {
      foreach ($second as $wkMonth => $third) {
      	if(!isset($third['projID'])) continue;
        foreach ($perIDUniformNumbers as $projID => $uniformNumbers) {
          if(strpos((string)$third['projID'], (string)$projID) === 0) {
            $perID = $third['perID'];
            if(!isset($result['data'][$uniformNumbers][$perID][50])) {
              $result['data'][$uniformNumbers][$perID][50][0] = array(
                'uNum' => $uniformNumbers,
                'name' => $third['name'],
                'title' => '固定薪資',
                'incomeNum' => $third['incomeNum'],
                'perID' => $third['perID'],
                'cNum' => '0',
                'incomeNotes' => '',
                'incomeType' => '50',
                'payTotal' => 0,
                'payTax' => 0,
                'payMoney' => 0,
                'projID' => $projID,
                'payYear' => (int)$year - 1911,
                'address' => $third['address'],
                'mRetirePay' => 0,
                'source' => '', 
                'isClose' => 1
              );

            }
            $result['data'][$uniformNumbers][$perID][50][0]['payTotal'] += ($third['mBasic'] - $third['food'] + $third['allowance'] - $third['absence']-$third['mRetirePay']);
            $result['data'][$uniformNumbers][$perID][50][0]['payTax'] += $third['taxPay'];
            $result['data'][$uniformNumbers][$perID][50][0]['payMoney'] += (($third['mBasic'] - $third['food'] + $third['allowance'] - $third['absence']) - $third['taxPay']-$third['mRetirePay']);
            $result['data'][$uniformNumbers][$perID][50][0]['mRetirePay'] += $third['mRetirePay'];
            $result['data'][$uniformNumbers][$perID][50][0]['source'] .= $third['source'].',';
            continue;
          }
         
        }
    

      }
    }
   	
    return $result;

  }

  function getBonus($year, $search_list = array(), $UniformNumbers = '') {
  	global $UniformNumbersProjID,$county;
    $result = array();

    $result = getIncomeTaxDB($year, $search_list, $UniformNumbers, 'bonus');
    if(count($result['data'])) {
      return $result;
    }

    //年終獎金
  	$sql = "select y.bonus, y.tax, y.empID, e.empName as name, e.perID, e.perID as incomeNum, e.county, e.city, e.address  from emplyee_bonus_year_end y inner join bonus_festival b on y.wkYear = b.wkYear inner join emplyee e on y.empID = e.empID where b.wkYear='".($year-1)."' and b.type='year_end' and bonus > 0 ";
  	if(count($search_list)>0) $sql.= " and ".implode(' and ', $search_list);
  	$sql .= " order by e.empID ";
    $rs = db_query($sql);
    $dataList = array();
    $empIDList = array();
    while ($r=db_fetch_array($rs)) {
       //年終獎金要算在當年12月時所在的單位
      $r['wkMonth'] = ($year-1).'12';
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $r['source'] = '年終獎金';
      $dataList[$r['empID']][$r['wkMonth']] = $r;
    }

    //三節獎金
    $sql = "select f.money as bonus, 0 as tax, f.empID, f.title as source, date_format(b.giveDate, '%Y%m') as wkMonth, e.empName as name, e.perID, e.perID as incomeNum, e.county, e.city, e.address 
    from emplyee_bonus_festival f inner join bonus_festival b on f.fid = b.id inner join emplyee e on f.empID = e.empID where f.wkYear='".$year."' and (f.title like '%端午%' or f.title like '%中秋%') ";
    if(count($search_list)>0) $sql.= " and ".implode(' and ', $search_list);
    $sql .= " order by e.empID, f.title ";
    $rs = db_query($sql);
    while ($r=db_fetch_array($rs)) {
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $dataList[$r['empID']][$r['wkMonth']] = $r;
    }

     //取員工每月的計劃ID&部門ID
    $empHistory = getEmpHistory($empIDList, $UniformNumbers);
    //塞入計劃ID&部門ID
    $dataList = setProjIDAndDepID($dataList, $empHistory);
    /*echo "<pre>";
    print_r($dataList);
    exit;*/

    $perIDUniformNumbers = array();
    foreach ($UniformNumbersProjID as $key => $value) {
      if($UniformNumbers != '' && $key != $UniformNumbers) continue;
      $p = explode('/', $value);
      foreach ($p as $k => $v) {
        $perIDUniformNumbers[$v] = $key;
      }
    }

    //塞入統編&依照統編排序
    foreach ($dataList as $empID => $second) {
      foreach ($second as $wkMonth => $third) {
        if(!isset($third['projID'])) continue;
        foreach ($perIDUniformNumbers as $projID => $uniformNumbers) {
          if(strpos((string)$third['projID'], (string)$projID) === 0) {
            $perID = $third['perID'];
            if(!isset($result['data'][$uniformNumbers][$perID][50])) {
              $result['data'][$uniformNumbers][$perID][50][0] = array(
                'uNum' => $uniformNumbers,
                'name' => $third['name'],
                'title' => '三節/其他獎金',
                'incomeNum' => $third['incomeNum'],
                'perID' => $third['perID'],
                'cNum' => '0',
                'incomeNotes' => '',
                'incomeType' => '50',
                'payTotal' => 0,
                'payTax' => 0,
                'payMoney' => 0,
                'projID' => $projID,
                'payYear' => (int)$year - 1911,
                'address' => $third['address'],
                'mRetirePay' => 0,
                'source' => '', 
                'isClose' => 1
              );

            }
            $result['data'][$uniformNumbers][$perID][50][0]['payTotal'] += $third['bonus'];
            $result['data'][$uniformNumbers][$perID][50][0]['payTax'] += $third['tax'];
            $result['data'][$uniformNumbers][$perID][50][0]['payMoney'] += ($third['bonus']-$third['tax']);
            $result['data'][$uniformNumbers][$perID][50][0]['source'] .= $third['source'].',';
            continue;
          }
         
        }
    

      }
    }


    //其他獎金
    $sql = "select b.id, b.money as bonus, b.taxMoney as tax, b.empID, b.bcTitle as source, b.wkMonth, c.type as incomeType, c.invNo as checkInvNo, e.empName as name, e.perID, e.perID as incomeNum, e.county, e.city, e.address 
    from emplyee_bonus b inner join bonus_class c on b.bcID = c.bcID inner join emplyee as e on b.empID = e.empID where b.wkMonth >= '".($year-1)."12' and b.wkMonth <= '".$year."11' ";
    if(count($search_list)>0) $sql.= " and ".implode(' and ', $search_list);
    $sql .= " order by e.empID, b.bcTitle ";

    $rs = db_query($sql);
    $otherList = array();
    $empIDList = array();
    while ($r=db_fetch_array($rs)) {
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $otherList[$r['incomeType']][$r['empID']][$r['wkMonth'].'_'.$r['id']] = $r;
    }

    //取員工每月的計劃ID&部門ID
    $empHistory = getEmpHistory($empIDList, $UniformNumbers);
    //塞入計劃ID&部門ID
    $otherData = array();
    foreach ($otherList as $type => $data) {
      $otherData[$type] = setProjIDAndDepID($data, $empHistory);
    }

    $otherList = array();
    foreach ($otherData as $type => $dataList) {
      foreach ($dataList as $empID => $second) {
        foreach ($second as $wkMonth => $third) {
          if(!isset($third['projID'])) continue;
          foreach ($perIDUniformNumbers as $projID => $uniformNumbers) {
            if(strpos((string)$third['projID'], (string)$projID) === 0) {
              $perID = $third['perID'];
              if(!isset($result['data'][$uniformNumbers][$perID][$type])) {
                $result['data'][$uniformNumbers][$perID][$type][0] = array(
                  'uNum' => $uniformNumbers,
                  'name' => $third['name'],
                  'title' => '三節/其他獎金',
                  'incomeNum' => $third['incomeNum'],
                  'perID' => $third['perID'],
                  'cNum' => '0',
                  'incomeNotes' => '',
                  'incomeType' => $type,
                  'payTotal' => 0,
                  'payTax' => 0,
                  'payMoney' => 0,
                  'projID' => $projID,
                  'payYear' => (int)$year - 1911,
                  'address' => $third['address'],
                  'mRetirePay' => 0,
                  'source' => '', 
                  'isClose' => 1
                );

              }
              $result['data'][$uniformNumbers][$perID][$type][0]['payTotal'] += $third['bonus'];
              $result['data'][$uniformNumbers][$perID][$type][0]['payTax'] += $third['tax'];
              $result['data'][$uniformNumbers][$perID][$type][0]['payMoney'] += ($third['bonus']-$third['tax']);
              $result['data'][$uniformNumbers][$perID][$type][0]['source'] .= $third['source'].',';
              continue;
            }
           
          }
      

        }
      }
    }

    return $result;

  }

  function getIncomeTaxDB($year, $search_list = array(), $UniformNumbers = '', $type='') {
    $result = array('data'=>array());

    $sql = "select * from income_tax where type='".$type."' and year='".$year."'";
    if(count($search_list)>0) {
      $search = implode(' and ', $search_list);
      $search = str_replace('e.perID','incomeNum', $search);
      $search = str_replace('e.empName','name', $search);
      $sql.= " and ".$search;
    }
    if($UniformNumbers != '') $sql.= " and uNum='".$UniformNumbers."'";
    $sql .= " order by id ";
   // print_r($sql);
    $rs = db_query($sql);
    while ($r=db_fetch_array($rs)) {
      $r['payYear'] = (int)$r['year'] - 1911;
      $result['data'][$r['uNum']][$r['perID']][$r['incomeType']][] = $r;
    }

    return $result;
  }

  function getEmpHistory($empIDList = array(), $UniformNumbers = '') {
  	global $UniformNumbersProjID;
  	$result = array();
  	$sql = "select *, date_format(rdate, '%Y%m') as wkMonth from emplyee_history where empID in (".implode(',', $empIDList).") ";
    $sql .= " order by empID, rdate";
    $rs = db_query($sql);

    while ($r=db_fetch_array($rs)) {
    	$result[] = $r;
    }

    return $result;

  }

  function setProjIDAndDepID($dataList = array(), $empHistory = array()) {
    global $UniformNumbersProjID;
    $firstData = array();
    foreach($empHistory as $k => $r) {
      if(!isset($dataList[$r['empID']])) continue;
      if(!isset($firstData[$r['empID']])) $firstData[$r['empID']] = $r;
      foreach ($dataList[$r['empID']] as $wkMonth => $value) {
        //先都預設給第一筆的計畫ID跟部門ID
        if(!isset($dataList[$r['empID']][$wkMonth]['projID'])) {
          $dataList[$r['empID']][$wkMonth]['projID'] = $firstData[$r['empID']]['projID'];
          $dataList[$r['empID']][$wkMonth]['depID'] = $firstData[$r['empID']]['depID'];
        }
        $_wkMonth = explode('_', $wkMonth);
        $_wkMonth = $_wkMonth[0];
        if((int)$_wkMonth >= (int)$r['wkMonth']) {
          $dataList[$r['empID']][$wkMonth]['projID'] = $r['projID'];
          $dataList[$r['empID']][$wkMonth]['depID'] = $r['depID'];
        }

        /*特殊狀況 其他獎金有統編者 直接歸類在該統編的部門*/
        if(!empty($value['checkInvNo'])) {
          $uList = explode('/', $UniformNumbersProjID[$value['checkInvNo']]);
          if(!in_array(substr($dataList[$r['empID']][$wkMonth]['projID'],0,2), $uList)) {
            $dataList[$r['empID']][$wkMonth]['projID'] = $uList[0];
          }
        }

        /*特殊狀況 2019年12月(含12月)前的所有報稅金額 都要全算在統編05557110 計畫編號算30*/
        if((int)$wkMonth <= 201912) {
          $uList = explode('/', $UniformNumbersProjID['05557110']);
          if(!in_array(substr($dataList[$r['empID']][$wkMonth]['projID'],0,2), $uList)) {
            $dataList[$r['empID']][$wkMonth]['projID'] = $uList[0];
          }
  
        }

      }

    }

    return $dataList;

  }

?>