<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	$bE = true;	//異動權限
	$pageTitle = "最新消息";
	$tableName = "bullets";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$filter = 'ClassID=0';
	if($_SESSION['privilege']<255) $filter .= " and announcer='$_SESSION[empID]'";
	$defaultOrder = "ID";
	$searchField1 = "ID";
	$searchField2 = "SimpleText";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,SimpleText,filepath,R_Date,unValidDate,announcer");
	$ftAry = explode(',',"編號,內容,附件,發佈日期,失效日期,公佈者");
	$flAry = explode(',',"50,,,,,");
	$ftyAy = explode(',',"ID,text,text,date,date,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"SimpleText,filepath,Content,R_Date,unValidDate,announcer");
	$newftA = explode(',',"內容,附件,全文,發佈日期,失效日期,公佈者");
	$newflA = explode(',',"60,60,0,0,");
	$newfhA = explode(',',"3,,6,,,");
	$newfvA = array('','','','','',$_SESSION["empID"]);
	$newetA = explode(',',"textbox,file,textarea,date,date,hidden");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,SimpleText,filepath,Content,R_Date,unValidDate");
	$editftA = explode(',',"編號,內容,附件,全文,發佈日期,失效日期");
	$editflA = explode(',',",60,,60,,");
	$editfhA = explode(',',",3,,6,,");
	$editetA = explode(',',"text,textbox,file,textarea,date,date");
?>