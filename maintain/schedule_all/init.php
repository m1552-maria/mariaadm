<?	
	include("../../config.php");
	include('../../miclib.php');
	
	$bE = true;	//異動權限
	$pageTitle = "全員行事曆維護";
	$tableName = "schedule_all";
	$extfiles = '';

	// for Upload files and Delete Record use
	$ulpath = '../../data/schedule/';
	$delField = 'ahead';
	$delFlag = true;
	$imgpath = '../../data/schedule/';
		
	// Here for List.asp ======================================= //
	$defaultOrder = "ID";
	$searchField1 = "ID";
	$searchField2 = "title";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,title,Content,beginDate,endDate");
	$ftAry = explode(',',"編號,簡述,內容,時間起,時間迄");
	$flAry = explode(',',"80,150,,,");
	$ftyAy = explode(',',"ID,text,text,datetime,datetime");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"title,Content,beginDate,endDate,announcer");
	$newftA = explode(',',"簡述,內容,時間起,時間企,發布者");
	$newflA = explode(',',"60,60,,,");
	$newfhA = explode(',',",6,,,");
	$newfvA = array('','','','',"$_SESSION[empID]");
	$newetA = explode(',',"text,textarea,datetime,datetime,hidden");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,title,Content,beginDate,endDate");
	$editftA = explode(',',"編號,簡述,內容,時間起,時間企");
	$editflA = explode(',',",60,60,,");
	$editfhA = explode(',',",,6,,");
	$editetA = explode(',',"text,text,textarea,datetime,datetime");
?>