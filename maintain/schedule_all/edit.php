<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
  $sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />  
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script> 
 <script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script>  
 <script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script type="text/javascript" src="/ckeditor/ckeditor.js"></script> 
 <title><?=$pageTitle?> - 編輯</title>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data">
<table align="center" class="sTable" width="96%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<? for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? switch($editetA[$i]) { 
  	  case "text" : echo "<input type='text' name='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  		case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $v) { $vv=$r[$editfnA[$i]]==$v?"<option selected>$v</option>":"<option>$v</option>"; echo $vv;} echo "</select>"; break;				 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "datetime": echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d H:i:s',strtotime($r[$editfnA[$i]]))."' class='input'>"; 
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datetimepicker();})</script>";
				break;			
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";				
  	} ?></td>
  </tr><? } ?>

<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <input type="button" value="取消編輯" class="btn" onClick="history.back()">
  </td>
</tr>
</table>
</form>
</body>
</html>
