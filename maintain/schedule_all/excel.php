<?
  if($_REQUEST['sqltx']) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");

    include '../../config.php';
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    
    
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
    // $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray(
    //     array(
    //         'fill' => array(
    //             'type' => PHPExcel_Style_Fill::FILL_SOLID,
    //             'color' => array('rgb' => '969696')
    //         ),
    //         'font'   => array('bold' => true,
    //             'size' => '12',
    //             'color' => array('argb' => 'FFFFFF')
    //         )
    //     )
    // );

    $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
    $objPHPExcel->getActiveSheet()->mergeCells("B2:E2");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    /*
    VERTICAL_CENTER 垂直置中
    VERTICAL_TOP
    HORIZONTAL_CENTER
    HORIZONTAL_RIGHT
    HORIZONTAL_LEFT
    HORIZONTAL_JUSTIFY
    */
    $objPHPExcel->getActiveSheet()->setCellValue('A1','全員行事曆');
    $objPHPExcel->getActiveSheet()->setCellValue('A2','製作日期');
    $objPHPExcel->getActiveSheet()->setCellValue('B2',date("Y/m/d"));
    $objPHPExcel->getActiveSheet()->setCellValue('A3','流水號'); 
    $objPHPExcel->getActiveSheet()->setCellValue('B3','簡述'); 
    $objPHPExcel->getActiveSheet()->setCellValue('C3','內容'); 
    $objPHPExcel->getActiveSheet()->setCellValue('D3','時間起');
    $objPHPExcel->getActiveSheet()->setCellValue('E3','時間迄');

    $i = 4;

    $sql = $_REQUEST[sqltx];
    $rs = db_query($sql);

    while($rs && $r=db_fetch_array($rs)) {
      //去除標籤
      $r[Content] = strip_tags($r[Content]);
      $eid = $r[empID];
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,($i-3));
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$r[title]);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$r[Content]);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$r[beginDate]);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$r[endDate]);

      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':E'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
    }


    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
    //設定欄寬
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->save('php://output');




  }


  
?>