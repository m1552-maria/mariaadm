﻿function HTMLEdit(thText,root,url) {
	var aobj = new Object();
	aobj.value = thText.value;
	//var root = '/var/www/data/sys/'; 改由menu.php決定
	//var url = 'http://'+location.hostname+'/data/sys/'
	var upath = "/htmledit/builded.php";
	ReturnStr = window.showModalDialog(upath,aobj,"dialogWidth=800px;dialogHeight=600px;scrollbars=no;help=no;status=no;edge=raised");
	if((ReturnStr != null) && (ReturnStr != "")) {
		//alert(ReturnStr);
		thText.value = ReturnStr
	}
}

function OpenWin(adate) {
	var dateStr = adate.value;
	ReturnStr = window.showModalDialog("../selectTime.htm",dateStr,"dialogWidth=295px;dialogHeight=250px;scrollbars=no;help=no;status=no;edge=raised");
	if((ReturnStr != null) && (ReturnStr != "")) {
	  adate.value = ReturnStr;
  }
}

function formvalid(thfm) {
	if($('#id').val()=="" || $('#id').val()==undefined){	alert("員工編號不存在，請先點選員工，再進行經歷維護");	return false;	}
	if($('#title').val()==""){	alert("請輸入訓練名稱");	return false;	}
	if($('#bdate').val()==""){	alert("請選擇訓練起日");	return false;	}
	if($('#edate').val()==""){	alert("請選擇訓練迄日");	return false;	}
	var bdate= new Date($('#bdate').val());
	var edate= new Date($('#edate').val());
	if(bdate>edate){	alert('訓練起迄日設定錯誤，請注意起迄時間');	return false;}
	if(isNaN($('#hours').val())==true && $('#hours').val()!=""){	alert("時數請輸入數字");	return false;	}
	if($('#trainUnit').val()==""){	alert("請輸入辦訓單位");	return false;	}
	if(isNaN($('#fee').val())==true && $('#fee').val()!=""){	alert("費用請輸入數字");	return false;	}
	var duBdate=new Date($('#duBdate').val());
	var duEdate=new Date($('#duEdate').val());
	if($('#duBdate').val()!="" || $('#duEdate').val()!=""){
		if($('#duBdate').val()==""){	alert('請選擇延長服務起日');	return false;}
		if($('#duEdate').val()==""){	alert('請選擇延長服務迄日');	return false;}
		if(duBdate>duEdate){	alert('延長服務起迄日設定錯誤，請注意起迄時間');	return false;}
	}
	return true;
}
