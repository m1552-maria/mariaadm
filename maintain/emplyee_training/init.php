<?php 
	// for Access Permission Control
	if(!session_id()) session_start();
	if ($_SESSION['privilege']<10) {
	  	header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	$bE2 = isset($_SESSION["canEditEmp"])?true:false;
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "教育訓練";
	$tableName = "emplyee_training";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['EE_EID'] = $eid;
	} else $eid = $_SESSION['EE_EID'];
	$filter = "empID='$eid'";
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "trainUnit";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,trainType,trainClass,title,bdate,edate,hours,trainUnit,fee,source,rdate");
	$ftAry = explode(',',"ID,內/外訓,課程分類,課程名稱,訓練起日,訓練迄日,時數,辦訓單位,費用,經費來源,登錄日期");
	$flAry = explode(',',",40,100,140,80,80,50,50,50,50,");
	$ftyAy = explode(',',"id,select,select,text,text,text,text,text,text,select,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"empID,trainType,trainClass,title,bdate,edate,hours,teacher,trainUnit,fee,source,duBdate,duEdate,isDoc1,isDoc2,isDoc3");
	$newftA = explode(',',"員工編號,內/外訓,課程分類,課程名稱,訓練起日,訓練迄日,時數,講師,辦訓單位,訓練費用,經費來源,延長服務起日,延長服務迄日,講義繳回,心得繳交,研習證明影本繳交");
	$newflA = explode(',',",,,,30,30,,,,,,,,,,");
	$newfhA = explode(',',",,,3,3,,,,,,,,,,");
	if(isset($_REQUEST['nwID'])) {	//here ID is 'apply_train' ID 
		$ID = $_REQUEST['nwID'];
  	$sql = "select * from apply_train where id='$ID'";
		$rsN = db_query($sql);
		$rN = db_fetch_array($rsN);
		$newfvA = array('',$trainType,$trainClass,$rN['title'],$rN['bdate'],$rN['edate'],$rN['hours'],'',$rN['trainUnit'],$rN['fee'],$source,'','','','','','');
	} else $newfvA = array('',$trainType,$trainClass,'','','','','','','',$source,'','','','','','');	
	$newetA = explode(',',"empID,select,select,text,date,date,text,text,text,text,select,date,date,checkbox,checkbox,checkbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,trainType,trainClass,title,bdate,edate,hours,teacher,trainUnit,fee,source,duBdate,duEdate,isDoc1,isDoc2,isDoc3");
	$editftA = explode(',',"ID,內/外訓,課程分類,課程名稱,訓練起日,訓練迄日,時數,講師,辦訓單位,訓練費用,經費來源,延長服務起日,延長服務迄日,講義繳回,心得繳交,研習證明影本繳交");
	$editflA = explode(',',",,,,30,30,,,,,,,,,,");
	$editfhA = explode(',',",,,3,3,,,,,,,,,,");
	$editfvA = array('',$trainType,$trainClass,'','','','','','','',$source,'','','','','','');
	$editetA = explode(',',"id,select,select,text,date,date,text,text,text,text,select,date,date,checkbox,checkbox,checkbox");
?>