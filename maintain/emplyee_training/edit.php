<?php
	session_start();
	include("../../config.php");
	include("init.php");
	$ID = $_REQUEST['ID'];
  $sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
	$icons = array('doc'=>'icon_doc.gif','docx'=>'icon_doc.gif','pdf'=>'icon_pdf.gif','ppt'=>'icon_ppt.gif','pptx'=>'icon_ppt.gif',
	  'txt'=>'icon_txt.gif','xls'=>'icon_xls.gif','xlsx'=>'icon_xls.gif','zip'=>'icon_zip.gif','rar'=>'icon_rar.gif');
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
.photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
.photoPreview .photoDesc { float:left; margin-left:5px; }
.photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
.photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
.photoPreview .photoItem .photoDel { margin:5px 0 0 5px; cursor:pointer; height:30px;}
.photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?php echo $extfiles?>edit.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <title><?php echo $pageTitle?> - 編輯</title>
 <script type="text/javascript">
  $(function() {
	  var icons=[];
	  icons['doc']='icon_doc.gif';
	  icons['docx']='icon_doc.gif';
	  icons['pdf']='icon_pdf.gif';
	  icons['ppt']='icon_ppt.gif';
	  icons['pptx']='icon_ppt.gif';
	  icons['txt']='icon_txt.gif';
	  icons['xls']='icon_xls.gif';
	  icons['xlsx']='icon_xls.gif';
	  icons['zip']='icon_zip.gif';
	  icons['rar']='icon_rar.gif';
 	$(".multiPhoto").change(function(){
		var that = $(this);
		var oTD = that.parents("td").eq(0);
		var fName=$(this.files)[0]['name'];
		var ary=fName.split(".");
		$(this.files).each(function(){
			var fReader = new FileReader();
			fReader.readAsDataURL(this);
			fReader.onloadend = function(event){
				var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);
				oItem.after(oItem.clone(true)).show();
				var img = oItem.find("img").get(0);
				img.style.padding='1';
				if(icons[ary[1]]){
					img.height=16;
					img.src='/images/'+icons[ary[1]];
				}else if(ary[1].toLowerCase()=="jpg" || ary[1].toLowerCase()=="png" || ary[1].toLowerCase()=="gif"){
					img.height='80';
					img.src=event.target.result;	
				}else{
					img.height=16;
					img.src='/images/icon_txt.gif';
				}
				var span=oItem.find("span").get(0);
				$(span).html(fName);
				oItem.get(0).input = that;
				var oNewFile = that.clone(true);
				that.hide().after(oNewFile);
			}
		});
	});
	
	$(".photoDel").click(function(){
		var oItem = $(this).parents(".photoItem").eq(0);
		if (oItem.get(0).input){ oItem.get(0).input.remove();}
		oItem.remove();
	});
  });
	
 </script>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?php echo $pageTitle?> -【編輯作業】</td></tr>
	<tr>
 		<td align="right" class="colLabel"><?php echo $editftA[0]?></td>
 		<td class="colContent"><?php echo $ID?></td>
	</tr>
	<?php 
		$docAry=array("isDoc1","isDoc2","isDoc3");
		$attachAry=array("attach1","attach2","attach3");
		$folderAry=array("doc1","doc2","doc3");
		for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?php echo  $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?php echo $editftA[$i]?></td>
  	<td><?php switch($editetA[$i]) { 
  	 		case "text" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' id='".$editfnA[$i]."' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea> <input type='button' value='編輯' onClick='HTMLEdit($editfnA[$i])'>"; break;
  			case "checkbox": 
				echo "<input type='checkbox' name='".$editfnA[$i]."' class='optBtn'"; 
				if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; 
				$index=array_search($editfnA[$i],$docAry);
				echo "<input type='file' name='".$attachAry[$index]."[]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview' >";
				if($r[$attachAry[$index]]!=""){
					$attach=explode(",",$r[$attachAry[$index]]);
					$attLen=count($attach);
					for($j=0;$j<$attLen;$j++){
						$finf = pathinfo($attach[$j]); 
						echo "<div class='photoItem' style='display:block;'>";
						echo "<input type='hidden' name='".$attachAry[$index]."_".md5($attach[$j])."' value='1' />";
						$ext = $finf['extension']; 
						if(strtolower($ext)=="png" || strtolower($ext)=="jpg"){
							echo "<img src='".$ulpath."/".$_SESSION['EE_EID']."/training/".$folderAry[$index]."/".md5($attach[$j]).".".$finf['extension']."' height='80' style='padding:1px'/>".$attach[$j]." ";
							echo '<a href="/data/emplyee/'.$_SESSION['EE_EID'].'/training/'.$folderAry[$index]."/".md5($attach[$j]).".".$finf['extension'].'" target="_new">大圖</a>';
						}else{
							$fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; 
							echo "<img src='/images/$fn' align='absmiddle'/>".$attach[$j]." ";
							echo '<a href="/data/emplyee/'.$_SESSION['EE_EID'].'/training/'.$folderAry[$index]."/".md5($attach[$j]).".".$finf['extension'].'" target="_new">下載</a>';
						}
						echo "<a class='photoDel' href='#' >刪除</a><div class='clearfx'></div>";
						echo "</div>";
					}
				}
				echo "<div class='photoItem'><img /><span id='fName'></span><a class='photoDel' href='#' >刪除</a><div class='clearfx'></div></div>";
				echo "</div>";
				break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				if($r[$editfnA[$i]]=="" || $r[$editfnA[$i]]=="0000-00-00"){
					echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='' class='input'>";
				}else{
					echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				}
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";	
				break;			
  	} ?></td>
  </tr><?php } ?>
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" id='selid' value="<?php echo $ID?>">
  	<input type="hidden" name="pages" value="<?php echo $_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?php echo $_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?php echo $_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?php echo $_REQUEST['sortDirection']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
  	<input type="button" class="btn" onClick="history.back()" value="取消編輯">
  </td>
</tr>
</table>
</form>
</body>
</html>
