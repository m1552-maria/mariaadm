<?php
	session_start();
	include("../../config.php");
	include("init.php");
	
	//處理上傳的附件
	$delOld = false;	//可否刪除舊檔
	$newAttach=array();
	if(!is_dir($ulpath."/".$_SESSION['EE_EID'])){	mkdir($ulpath."/".$_SESSION['EE_EID']);	}
	if(!is_dir($ulpath."/".$_SESSION['EE_EID']."/training")){	mkdir($ulpath."/".$_SESSION['EE_EID']."/training");	}
	if(!is_dir($ulpath."/".$_SESSION['EE_EID']."/training/doc1")){	mkdir($ulpath."/".$_SESSION['EE_EID']."/training/doc1");	}
	if(!is_dir($ulpath."/".$_SESSION['EE_EID']."/training/doc2")){	mkdir($ulpath."/".$_SESSION['EE_EID']."/training/doc2");	}
	if(!is_dir($ulpath."/".$_SESSION['EE_EID']."/training/doc3")){	mkdir($ulpath."/".$_SESSION['EE_EID']."/training/doc3");	}
	
	$attachAry=array("attach1","attach2","attach3");
	$folderAry=array("doc1","doc2","doc3");
	$attach1=array();
	$attach2=array();
	$attach3=array();
	$attAryLen=count($attachAry);
	//::處理刪除的附件檔
	$sql="select * from $tableName where id=".$_REQUEST['selid'];
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$delAry=array();
		$delFolderAry=array();
		$r=db_fetch_array($rs);
		for($k=0;$k<$attAryLen;$k++){
			$extAttach=explode(",",$r[$attachAry[$k]]);
			//echo "r[".$attachAry[$k]."]=".$r[$attachAry[$k]]."<br>";
			if($r[$attachAry[$k]]!=""){
				$extAttLen=count($extAttach);
				for($i=0;$i<$extAttLen;$i++){
					$finf = pathinfo($extAttach[$i]);
					//echo 'request: '.$attachAry[$k].'_'.md5($extAttach[$i])."<br>";
					if(!isset($_REQUEST[$attachAry[$k].'_'.md5($extAttach[$i])])){
						//echo 'del:'.md5($extAttach[$i]).'.'.$finf['extension']."<br>";
						array_push($delAry,md5($extAttach[$i]).'.'.$finf['extension']);
						array_push($delFolderAry,$folderAry[$k]);
					}else{//還保留的圖檔
						array_push($$attachAry[$k],$extAttach[$i]);
						//echo '保留:'.$attachAry[$k]."-".$extAttach[$i]."<br>";
					}
				}
			}
			//echo "----<br>";
		}
		
		for($i=0;$i<count($delAry);$i++){
			//echo "delpath:".$ulpath."/".$_SESSION['EE_EID']."/training/".$delFolderAry[$i]."/".$delAry[$i]."<br>";
			if(is_file($ulpath."/".$_SESSION['EE_EID']."/training/".$delFolderAry[$i]."/".$delAry[$i])){	
				unlink($ulpath."/".$_SESSION['EE_EID']."/training/".$delFolderAry[$i]."/".$delAry[$i]);
			}
		}
	}
	$bMultiFile=false;
	for($i=0;$i<$attAryLen;$i++){
		if($_FILES[$attachAry[$i]]){//::多檔上傳 attach1,attach2,attach3
			$bMultiFile=true;
			foreach($_FILES[$attachAry[$i]]["name"] as $k=>$v){
				$finf = pathinfo($v); 
				$dstfn = md5($_FILES[$attachAry[$i]]["name"][$k]).'.'.$finf['extension'];
				if($_FILES[$attachAry[$i]]["tmp_name"][$k]=="")	continue;
				//echo "mutiple:".$ulpath."/".$_SESSION['EE_EID']."/training/".$folderAry[$i]."/".$dstfn."<br>";
				copy($_FILES[$attachAry[$i]]["tmp_name"][$k],$ulpath."/".$_SESSION['EE_EID']."/training/".$folderAry[$i]."/".$dstfn);
				if(!in_array($_FILES[$attachAry[$i]]["name"][$k],$$attachAry[$i])){
					array_push($$attachAry[$i],$_FILES[$attachAry[$i]]["name"][$k]);
				}
			}
		}
	}
	if(!$bMultiFile){
		foreach($_FILES as $k=>$v) {
			$fn = $_FILES[$k]['name'];
			if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
			if ($$k != "") {
				if (!copy($$k,"$ulpath$fn2")) { //上傳失敗
					echo '附件上傳作業失敗 !'; exit;
				} else $delOld=true;
			}
		}
	}
		
	$sql = "update $tableName set ";
  	$fields=array();
	$n = count($editfnA);
	for($i=1; $i<$n; $i++) {
		if ( ($fn=='') && ($editfnA[$i]==$delField)) continue;	//沒有上傳的檔案
		switch($editetA[$i]) {
			case "hidden" :
			case "select" :		
	  		case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
			case "textbox" :
  			case "textarea" :	$fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
  			case "checkbox" : 	if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
			case "file" : $fldv = "'$fn'"; break;
		}
		array_push($fields,$editfnA[$i].'='.$fldv);
	}
	array_push($fields,"attach1='".join(",",$attach1)."'");
	array_push($fields,"attach2='".join(",",$attach2)."'");
	array_push($fields,"attach3='".join(",",$attach3)."'");
	$sql .= join(",",$fields)." where $editfnA[0]='".$_REQUEST['selid']."'";
	
	db_query($sql);
	db_close(); 
	//echo $sql;exit;	
	header("Location: list.php?pages=".$_REQUEST['pages']."&keyword=".$_REQUEST['keyword']."&fieldname=".$_REQUEST['fieldname']."&sortDirection=".$_REQUEST['sortDirection']."&selid=".$_REQUEST['selid']);
	
?>