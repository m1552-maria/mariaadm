<?
	session_start();
	//::秀出清單
	// $nowYr = date('Y');
	// $curYr = $_REQUEST['year']?$_REQUEST['year']:$curYr=$nowYr;
	// if($_REQUEST['month']) $curMh = $_REQUEST['month']; else $curMh = date('m');
	// $wkMonth=$curYr.str_pad($curMh,2,"0",STR_PAD_LEFT);
	// $depID=$_REQUEST[depID];
	// $empID=$_REQUEST[empID];
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	$depID = array();
	$temp=array();
	if($_REQUEST[depFilter]){
		$temp[] = "and e.depID like '$_REQUEST[depFilter]%'";
		if(count($temp)>0) $filter .=" ". join(' and ',$temp);
	}else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v){
				if($v)	$aa[]="e.depID like '$v%'";	
				$depID[] = $v;
			}
			$temp[] = " (".join(' or ',$aa).")"; 	
		} else {
			$temp[] = "and e.depID=''";
		}
		if(count($temp)>0) $filter .=" and ". join(' and ',$temp);
	}
	$depID = implode(',', $depID);

	//過濾條件
	if(isset($filter)){
		$sql="select *,r.empID as empID,i.id as id from $tableName where ($filter) ";
		$sql.="and i.wkMonth='$wkMonth' ";
	}  else	$sql="select *,r.empID as empID,i.id as id from $tableName where i.wkMonth='$wkMonth' ";
	// 搜尋處理
  	if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')) ";
  	$sql.= "group by i.id ";
  	// 排序處理
  	if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	$outsql = $sql;
	//分頁
  	$sql .= " limit ".$pageSize*$pages.",$pageSize"; 
  	 //echo $sql; 
  	$rs = db_query($sql,$conn);
  	$rows=db_num_rows($rs);

  	//執行鎖定或關帳作業
  	$cMan = $_SESSION["Account"]?$_SESSION["Account"]:$_SESSION["empID"];
  	$fCheck = checkAcctClose('salary',$wkMonth,$cMan);

?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script> 
 <title><?=$pageTitle?></title>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	var wage="<?=$_REQUEST[wage]?>";	//時薪
	function doSubmit() {
		form1.submit();
	}
	function filterSubmit(tSel) {
		tSel.form.submit();
	}
  	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		url+="&<?=$paramsStr?>";
		url +='&year=<?=$curYr?>';
		url +='&month=<?=$curMh?>';
		// url+="&fid=<?=$_REQUEST[fid]?>";
		// url+="&type=<?=$_REQUEST[type]?>";
		// url+="&detail=<?=$_REQUEST[detail]?>";
		// url+="&depID=<?=$_REQUEST[depID]?>";
		location.href = url;

	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>&<?=$paramsStr?>&wage="+wage; 
	}
	function clickEdit(aid) {	
		selid = aid;	
		<? if($bE){ ?> DoEdit();	<? } ?>
	}	
	function exportExcel(type){
		var depID,excelUrl;
		if("<?=$depID?>"){
			depID="<?=$depID?>";
		}else {
			depID= $('select#depFilter option:selected').val();
		}
			
		var data = {
			dep: depID, 
			wkMonth:"<?=$wkMonth?>", 
			empID:"<?=$empID?>",
			detail:"<?=$_REQUEST[detail]?>"
		};

		if(type=='overtime'){
			excelUrl="../emplyee_salary_report/excel.php";
			data['showZero'] = ($('#showZero').prop('checked')) ?　1 : 0;
		}else if(type=='absence'){
			excelUrl="../emplyee_salary_report/excel_absence.php";
			data['showZero'] = ($('#showZero').prop('checked')) ?　1 : 0;
		}
		if("<?=$_REQUEST[detail]?>"!="Y" && "<?=$_SESSION['privilege']?>"<=10){//不是在明細表，是在全表
			if(depID==""){//全部部門
				var opts=document.getElementById("depFilter").options;
				var depAry=[];
				for(var i=0;i<opts.length;i++){
					if(opts[i].value==0)	continue;
					depAry.push(opts[i].value);
				}
				depID=depAry.join(",");
			}
		}
		
		$.ajax({
		  url: excelUrl,
		  method: "POST",
		  dataType:"text",
		  data: data
		}).done(function(rs) {
			var host=window.location.hostname;
			window.open('http://'+host+"/data/caches/"+rs+".xlsx",'_blank' );
		});
	}
	function doAppend(){
		location.href="append.php?<?=$paramsStr?>&wage="+wage;
	}
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body bgcolor="#EEEEEE">
<a name="top"/>
<form name="excelfm" action="../emplyee_salary_report/excel.php" method="post" target="excel">
  	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
	<input type="hidden" name="sqltx" value="<?=$outsql?>">
	<input type="hidden" name='wkMonth' value="<?=$wkMonth?>"/>
</form>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
	<?
  foreach ($params as $k => $v) {
  ?>
  	<input type='hidden' name="<?=$k?>" value="<?=$v?>" />
  <?
	}
  ?>
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="<?=$keyword?>">
		<input type="hidden" name="curYr" value="<?=$curYr?>">
		<input type="hidden" name="curMh" value="<?=$curMh?>">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)">
		<input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)">
		<input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)">
		<input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    	<input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <? 
    	if($_REQUEST[detail]){	
    		if($fCheck==0 || ( $fCheck<>1 && strpos($fCheck,$cMan)===false) ) { 
    ?>
    	<input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'sxBtn'?>" onclick='doAppend()'>
    	<input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="MsgBox()">
    <?
    		} 
    	}
    ?>
    	<? 
    	if(!$_REQUEST[detail]){	?>
    	<select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
			<? 
			  if ($_SESSION['privilege'] > 10) {
		        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
		      } else {
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
		      }
			?>
	    </select>
	    <select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
	    <select name="month" id="month" onChange="doSubmit()">
	    	<? 
	    		if($curYr<$nowYr){	$m=12;	}else{	$m=date('m');}
				
	    		for($i=1;$i<=$m;$i++) { 
	    			$sel=($curMh==str_pad($i,2,"0",STR_PAD_LEFT)?'selected':''); 
	    			echo $curMh.'=='.str_pad($i,2,"0",STR_PAD_LEFT);
	    			echo $sel;
	    			echo "<option $sel value='$i'>$i 月</option>"; 
	    		} 
	    	?>
	    </select>
	    <?	}
		if($_REQUEST[type]=="overtime" && $rows>0){
	    ?>
    		<input type="button" name="toExcel" value="匯出Excel" onClick="exportExcel('overtime')">
    		<input type="checkbox" id="showZero" value="1">EXCEL列出加班費合計為0員工
    	<?
		}else if($_REQUEST[type]=="absence"  && $rows>0){	?>
			<input type="button" name="toExcel2" value="匯出Excel" onClick="exportExcel('absence')">
			<input type="checkbox" id="showZero" value="1">EXCEL列出扣薪合計為0員工
		<?
		}
    	?>
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr bgcolor="#336699" height="25" valign="bottom" align="center" bordercolordark="#336699">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<td width="<?=$flAry[$i]?>">
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</td>
	<? } ?>
  </tr>
<?
	
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)" <? if($id==$selid) echo ' class="onSel"'?>>
   	<td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>>
   		<? if($r[isExpand]>0 && $_REQUEST[detail]=="Y"){//手動擴充的 && 在下方明細檔檢視時，才可以刪除	?>
	    	<input type="checkbox" value="<?=$id?>" class='opt' name="ID[]" <? if ($nextfg) echo "checked" ?>>
	    	<a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a>
   		<?	}else{	
   				echo $id;
   		}    ?>
    </td>
    	<?
		$tr="<td>".$r[empID]."</td>";
		$tr.="<td>".$emplyeeinfo[$r[empID]]."</td>";
		$tr.="<td>".$r[mBasic]."</td>";
		$tr.="<td>".$r[wage]."</td>";
		if($_REQUEST[type]=="insurence"){			//::-------------勞健退-------------
			$tr.="<td>$r[title]</td>";
			$tr.="<td>$r[money]</td>";
		}else if($_REQUEST[type]=="absence"){		//::-------------出缺勤-------------
			$tr.="<td>$r[title]</td>";
			$tr.="<td>$r[hours]</td>";
			if($r[addsub]=="-"){//減項
				$tr.="<td>$r[money]</td>";
				$tr.="<td>0</td>";
			}else if($r[addsub]=="+"){//加項
				$tr.="<td>0</td>";
				$tr.="<td>$r[money]</td>";
			}
		}else if($_REQUEST[type]=='allowance'  || $_REQUEST[type]=="otherfee"){//::-------------津貼、雜項費用-------------
			$tr.="<td>$r[title]</td>";
			$tr.="<td>$r[hours]</td>";
			$tr.="<td>$r[money]</td>";
		}else if($_REQUEST[type]=="overtime"){	//::-------------加班費-------------
			$tr.="<td>".$r[title]."</td>";
			if($r['subClass']=='A'){//前二小時加班
				$tr.="<td>$r[hours]</td>";
				$tr.="<td>$r[money]</td>";
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
			}else if($r['subClass']=='B'){//第3-8小時加班
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
				$tr.="<td>$r[hours]</td>";
				$tr.="<td>$r[money]</td>";
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
			}else if($r['subClass']=='C'){//第9-12小時加班
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
				$tr.="<td>0</td>";
				$tr.="<td>$r[hours]</td>";
				$tr.="<td>$r[money]</td>";
			}
		}else if($_REQUEST[type]=='addsub'){//::-------------加減項-------------
			$tr.="<td>$r[title]</td>";
			$tr.="<td>$r[money]</td>";
		}
		echo $tr;
		?>
  </tr>    
<? } 
?>   
 </table>
</form>
</body>
</Html>