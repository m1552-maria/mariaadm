<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	//$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	$bE = true;
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include('../../getEmplyeeInfo.php');	
	include '../inc_vars.php';
	$pageTitle = "薪資試算明細";
	$tableName2="emplyee_salary_items";
	$tableName = "emplyee_salary_items as i ";
	$tableName.= ",emplyee_salary_report as r ";
	$tableName.= "left join emplyee as e ";
	$tableName.= "on r.empID=e.empID ";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	

	if($_REQUEST[type])	$filter="i.fid=r.id and i.iType='$_REQUEST[type]' ";
	// 需要記住的值
	// if($_REQUEST[fid]) {
	// 	$fid = $_REQUEST['fid'];
	// 	$_SESSION['emplyee_salary_items_fid'] = $fid;
	// } else $fid = $_SESSION['emplyee_salary_items_fid'];
	if($_REQUEST[fid]) $filter .= "and i.fid=$_REQUEST[fid] ";
	$isClose=0;
	if($_REQUEST[isClose]) {
		$isClose = $_REQUEST['isClose'];
	} 
	// Here for List.asp ======================================= //
	$defaultOrder = "i.addsub,i.id";
	$searchField1 = "e.empID ";
	$searchField2 = "i.title ";
	if(isset($_REQUEST[depID]) && $_REQUEST[depID]!=""){//下方的明細表
		$pageSize = 10;	//每頁的清單數量
	}else{
		$pageSize = 20;	//每頁的清單數量
	}
	// 注意 primary key shpuld be first one
	$addsub=array('+'=>'加項','-'=>'減項');
	if($_REQUEST[type]=="insurence"){//-------------------------勞健退
		$iType='insurence';
		$titleType = array('勞保自負額'=>'勞保自負額','健保自負額'=>'健保自負額','勞退自提額'=>'勞退自提額');
		$fnAry = explode(',',"id,empID,empName,mBasic,wage,title,money");
		$ftAry = explode(',',"ID,員編,姓名,月薪,時薪,項目名稱,金額");
		$flAry = explode(',',",,40,100,");
		$ftyAy = explode(',',"id,text,text,text,text");
		//Append
		$newfnA = explode(',',"fid,title,money,notes,isExpand,iType,addsub");
		$newftA = explode(',',"ID,項目名稱,金額,補充說明,是否自訂增加,類型,加減項");
		$newflA = explode(',',"60,,,25,");
		$newfhA = explode(',',",,,4,");
		$newfvA = array($_REQUEST[fid],$titleType,'','',1,$iType,$addsub);
		$newetA = explode(',',"hidden,select,text,textbox,hidden,hidden,select");
		//Edit
		$editfnA = explode(',',"id,title,money,notes,isExpand,iType,addsub");
		$editftA = explode(',',"ID,項目名稱,金額,補充說明,是否自訂增加,類型,加減項");
		$editflA = explode(',',",,,25,60");
		$editfhA = explode(',',",,,4,3");
		$editfvA = array($_REQUEST[fid],$titleType,'','',1,$iType,$addsub);	
		$editetA = explode(',',"hidden,select,text,textbox,hidden,hidden,select");
	}else if($_REQUEST[type]=="absence"){//-------------------------出缺勤
		$iType='absence';
		$fnAry = explode(',',"id,empID,empName,mBasic,wage,title,hours,money,money");
		$ftAry = explode(',',"ID,員編,姓名,月薪,時薪,假別,時數,扣薪,加項");
		$flAry = explode(',',",,80,100,");
		$ftyAy = explode(',',"id,text,text,text,text");
		//Append
		$absAry=array();
		$exclude=array("特休",'公假','婚假','喪假','補休','例假休','產檢假','陪產假');//排除的假種
		foreach($absType as $k=>$v){
			if(in_array($v, $exclude)) continue;
			$absAry[$v]=$v;
		}	
		$newfnA = explode(',',"fid,title,wage,hours,money,addsub,iType,isExpand,notes");
		$newftA = explode(',',"ID,假別,時薪,時數,扣薪,加減項,類型,是否自訂增加,補充說明");
		$newflA = explode(',',"60,,,,,,,,25");
		$newfhA = explode(',',",,,,,,,,4");
		$newfvA = array($_REQUEST[fid],$absAry,$_REQUEST[wage],'','',$addsub,$iType,1,'');
		$newetA = explode(',',"hidden,select,id,text,text,select,hidden,hidden,textbox");
		//Edit
		$editfnA = explode(',',"id,title,wage,hours,money,addsub,iType,isExpand,notes");
		$editftA = explode(',',"ID,假別,時薪,時數,扣薪,加減項,類型,是否自訂增加,補充說明");
		$editflA = explode(',',",,,,25,");
		$editfhA = explode(',',",,,,4,");
		$editfvA = array($_REQUEST[fid],$absAry,$_REQUEST[wage],'','',$addsub,$iType,1,'');	
		$editetA = explode(',',"hidden,select,id,text,text,select,hidden,hidden,textbox");
	}else if($_REQUEST[type]=="overtime"){//-------------------------加班費
		$iType='overtime';
		$titleType = array('加班-休息日'=>'加班-休息日','加班-上班日'=>'加班-上班日','加班-國定假日'=>'加班-國定假日');
		$subClassAry = array('A'=>'前二小時時數','B'=>'第3-8小時時數','C'=>'第9-12小時時數');
		$fnAry = explode(',',"id,empID,empName,mBasic,wage,title,subClassA,moneyA,subClassB,moneyB,subClassC,moneyC");
		$ftAry = explode(',',"ID,員編,姓名,月薪,時薪,加班別,前二小時時數,加班費A,第3-8小時時數,加班費B,第9-12小時時數,加班費C");
		$flAry = explode(',',"50,40,65,40,40,60,60,60,60,60,60,60");
		$ftyAy = explode(',',"id,text,text,text,text");
		//Append
		$newfnA = explode(',',"fid,title,subClass,wage,hours,money,notes,isExpand,iType,addsub");
		$newftA = explode(',',"ID,加班別,時段,時薪,時數,加班費,補充說明,是否自訂增加,類型,加減項");
		$newflA = explode(',',"60,,,,,,25");
		$newfhA = explode(',',",,,,,,4");
		$newfvA = array($_REQUEST[fid],$titleType,$subClassAry,$_REQUEST[wage],'','','',1,$iType,$addsub);
		$newetA = explode(',',"hidden,select,select,id,text,text,textbox,hidden,hidden,select");
		//Edit
		$editfnA = explode(',',"id,title,subClass,wage,hours,money,notes,isExpand,iType,addsub");
		$editftA = explode(',',"ID,加班別,時段,時薪,時數,加班費,補充說明,是否自訂增加,類型,加減項");
		$editflA = explode(',',",,,,,,25,60");
		$editfhA = explode(',',",,,,,,4,3");
		$editfvA = array($_REQUEST[fid],$titleType,$subClassAry,$_REQUEST[wage],'','','',1,$iType,$addsub);	
		$editetA = explode(',',"hidden,select,select,id,text,text,textbox,hidden,hidden,select");
	}else if($_REQUEST[type]=="allowance"){//-------------------------津貼
		$iType='allowance';
		$fnAry = explode(',',"id,empID,empName,mBasic,wage,title,hours,money");
		$ftAry = explode(',',"ID,員編,姓名,月薪,時薪,名稱,數量,金額");
		$flAry = explode(',',",,40,100,");
		$ftyAy = explode(',',"id,text,text,text,text,text,text,text");
		//Append
		$newfnA = explode(',',"fid,title,hours,money,notes,isExpand,iType,addsub");
		$newftA = explode(',',"ID,名稱,數量,金額,補充說明,是否自訂增加,類型,加減項");
		$newflA = explode(',',"60,,,,25,");
		$newfhA = explode(',',",,,,4,");
		$newfvA = array($_REQUEST[fid],'','','','',1,$iType,$addsub);
		$newetA = explode(',',"hidden,text,text,text,textbox,hidden,hidden,select");
		//Edit
		$editfnA = explode(',',"id,title,hours,money,notes,isExpand,iType,addsub");
		$editftA = explode(',',"ID,名稱,數量,金額,補充說明,是否自訂增加,類型,加減項");
		$editflA = explode(',',",,,,25,");
		$editfhA = explode(',',",,,,4,");
		$editfvA = array($_REQUEST[fid],'','','','',1,$iType,$addsub);	
		$editetA = explode(',',"hidden,text,text,text,textbox,hidden,hidden,select");
	}else if($_REQUEST[type]=="otherfee"){//-------------------------雜項費用
		$iType='otherfee';
		$fnAry = explode(',',"id,empID,empName,mBasic,wage,title,hours,money");
		$ftAry = explode(',',"ID,員編,姓名,月薪,時薪,名稱,數量,金額");
		$flAry = explode(',',",,40,100,");
		$ftyAy = explode(',',"id,text,text,text,text");
		//Append
		$newfnA = explode(',',"fid,title,hours,money,notes,isExpand,iType,addsub");
		$newftA = explode(',',"ID,名稱,數量,金額,補充說明,是否自訂增加,類型,加減項");
		$newflA = explode(',',"60,,,,25,");
		$newfhA = explode(',',",,,,4,");
		$newfvA = array($_REQUEST[fid],'','','','',1,$iType,$addsub);
		$newetA = explode(',',"hidden,text,text,text,textbox,hidden,hidden,select");
		//Edit
		$editfnA = explode(',',"id,title,hours,money,notes,isExpand,iType,addsub");
		$editftA = explode(',',"ID,名稱,數量,金額,補充說明,是否自訂增加,類型,加減項");
		$editflA = explode(',',",,,,25,");
		$editfhA = explode(',',",,,,4,");
		$editfvA = array($_REQUEST[fid],'','','','',1,$iType,$addsub);	
		$editetA = explode(',',"hidden,text,text,text,textbox,hidden,hidden,select");
	}else if($_REQUEST[type]=="addsub"){//-------------------------加減項
		$iType='addsub';
		$fnAry = explode(',',"id,empID,empName,mBasic,wage,title,money");
		$ftAry = explode(',',"ID,員編,姓名,月薪,時薪,名稱,金額");
		$flAry = explode(',',",,40,100,");
		$ftyAy = explode(',',"id,text,text,text");
		//Append
		$newfnA = explode(',',"fid,title,money,notes,isExpand,iType,addsub");
		$newftA = explode(',',"ID,名稱,金額,補充說明,是否自訂增加,類型,加減項");
		$newflA = explode(',',"60,,,25,");
		$newfhA = explode(',',",,,4,");
		$newfvA = array($_REQUEST[fid],'','','',1,$iType,$addsub);
		$newetA = explode(',',"hidden,text,text,textbox,hidden,hidden,select");
		//Edit
		$editfnA = explode(',',"id,title,money,notes,isExpand,iType,addsub");
		$editftA = explode(',',"ID,名稱,金額,補充說明,是否自訂增加,類型,加減項");
		$editflA = explode(',',",,,25,");
		$editfhA = explode(',',",,,4,");
		$editfvA = array($_REQUEST[fid],'','','',1,$iType,$addsub);	
		$editetA = explode(',',"hidden,text,text,textbox,hidden,hidden,select");
	}

	
	$params=array();
	$params[fid]=$_REQUEST[fid];
	$params[empID]=$_REQUEST[empID];
	$params[depID]=$_REQUEST[depID];
	$params[year]=$_REQUEST[year];
	$params[month]=$_REQUEST[month];
	$params[type]=$_REQUEST[type];
	$params[detail]=$_REQUEST[detail];
	$params[wage]=$_REQUEST[wage];
	$paramsAry=array();
	foreach ($params as $k => $v) {
		array_push($paramsAry,"$k=$v");
	}
	$paramsStr=join("&",$paramsAry);
?>