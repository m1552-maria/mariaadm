<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "資源類別管理";
	$tableName = "resource_class";
	$extfiles = '../';				//js及css檔的路徑位置
	
	$PID = 'PID';
	$CLASS_PID = 'resource_class_PID';
	// for classid.php
	$ID  = 'ID';
	$Title = 'Title';
	
	// Here for List.asp ======================================= //
	//if($_SESSION['domain_Host']) { 不管制Root
	//	$RootClass = $_SESSION['user_classdef'][0];		
	//	$rsT = db_query("select Title from docclasses where ID='$RootClass'"); 
	//	$rT = db_fetch_array($rsT); 
	//	$ROOT_NAME = $rT[0];
	//} else {
		$RootClass = 0;
		$ROOT_NAME = '資源類別';
	//}

	if (isset($_REQUEST[filter])) $_SESSION[$CLASS_PID]=$_REQUEST[filter]; 
	if (!$_SESSION[$CLASS_PID]) $_SESSION[$CLASS_PID]=$RootClass;
	$filter = "$PID='$_SESSION[$CLASS_PID]'"; 
	if ($_REQUEST[cnm]) $_SESSION[cnm]=$_REQUEST[cnm]; else {	
		if(count($_REQUEST)<2 || $_SESSION[$CLASS_PID]==0) $_SESSION[cnm]=$ROOT_NAME;
	}
	
	// for Upload files and Delete Record use
	$ulpath = '../map_icon/';
	$delField = 'path';
	$delFlag = false;
	
	$ALL_FIRLDS = array(
		'sub_class'=>'次類別',
		'name'=>'名稱',
		'email'=>'E-Mail',
		'tel'=>'電話',
		'mobile'=>'行動電話',
		'county'=>'縣市',
		'city'=>'鄉鎮區域',
		'address'=>'地址',
		'PosCode'=>'郵遞區號',
		'title'=>'職稱',
		'company'=>'現職單位',
		'contact'=>'聯絡人',
		'contact_tel'=>'聯絡電話',
		'contact_address'=>'聯絡地址',
		'experience'=>'學經歷',
		'skill'=>'擅長領域',
		'area'=>'服務區域',
		'target'=>'服務對象',
		'business'=>'服務內容',
		'service_time'=>'服務時間',
		'apply_doc'=>'申請表格及文件',
		'service_status'=>'使用概況',
		'notes'=>'備註',
		'notes2'=>'備註2',
		'notes3'=>'備註3',
		'notes4'=>'備註4',
		'notes5'=>'備註5',
		'notes6'=>'備註6',
		'notes7'=>'備註7',
		'file1'=>'上傳影像'
	);
	
	// Here for List.asp ======================================= //
	
	$defaultOrder = "Seque";
	$searchField1 = "Title";
	$searchField2 = "Title";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,Title,icon,description,Seque");
	$ftAry = explode(',',"編號,類別名稱,地圖圖標,說明,順序");
	$flAry = explode(',',"60,,,,");
	$fhAry = explode(',',",,24,,");
	$ftyAy = explode(',',"ID,text,image,text,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"PID,ID,Title,icon,description,Seque,fields");
	$newftA = explode(',',"上層代碼,編號,類別名稱,地圖圖標,說明,順序,選擇欄位");
	$newflA = explode(',',",,60,60,,5,6");
	$newfhA = explode(',',",,,,5,,");
	$newfvA = array($_SESSION[$CLASS_PID],'','','','','',$ALL_FIRLDS);
	$newetA = explode(',',"readonly,text,text,file,textbox,text,multiCheckboxText");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,Title,icon,description,Seque,fields");
	$editftA = explode(',',"編號,類別名稱,地圖圖標,說明,順序,選擇欄位");
	$editflA = explode(',',",60,60,,5,6");
	$editfhA = explode(',',",,,5,,");
	$editfvA = array('','','','','',$ALL_FIRLDS);
	$editetA = explode(',',"readonly,text,file,textbox,text,multiCheckboxText");
?>