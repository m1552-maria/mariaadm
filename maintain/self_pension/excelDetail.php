<?php
  session_start();
  if(empty($_REQUEST['year'])) exit;
 
  $filename = $_REQUEST['year'].'年勞退自提額'.date("Y-m-d H:i:s",time()).'.xlsx';
  

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');
  include ('../inc_vars.php');
  include("../salary_bonus_export/getData.php");
  include("getData.php");
  
  $result = getSelfRetire($_REQUEST['year'], $_REQUEST['search_list'], $_REQUEST['projIDList']);
  $year = $_REQUEST['year'];
  $dataList = $result['dataList'];

  $monthList = array(($year-1).'12' => ($year-1).'12');
  for($i=1; $i<=11; $i++) { 
    $v = $year.str_pad($i,2,'0',STR_PAD_LEFT);
    $monthList[$v] = $v;
  }

  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();


  $letter = 65;
  $tLetter = chr($letter+15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);

  $i = 1;
  $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
  $sheet->setCellValueExplicit('A'.$i,$_REQUEST['year'].'年勞退自提額清冊');
 // $sheet->getStyle('A1:R1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A'.$i.':'.$tLetter.$i)->getFont()->setSize(16);

  $i++;
  $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
  $sheet->setCellValueExplicit('A'.$i,'計劃:'.$_REQUEST['projIDList']);
  $sheet->getStyle('A'.$i.':'.$tLetter.$i)->getFont()->setSize(14);

  $nYear = $year -1911;
  $i++;
  $sheet->setCellValueExplicit(chr($letter++).$i,'姓名');
  $sheet->setCellValueExplicit(chr($letter++).$i,'員編');
  $sheet->setCellValueExplicit(chr($letter++).$i,'身分證字號');
  foreach ($monthList as $key => $value) {
    $sheet->setCellValueExplicit(chr($letter++).$i, ($value-191100));
  }
  $sheet->setCellValueExplicit(chr($letter++).$i,'自提合計');
 

  $allSum = array('money'=>array());
  foreach($dataList as $key => $emplyee) {
    $showList = explode('=*=', $key);
    $sum = array('money'=>array());
    $depID = $showList[0];
    $projID = $showList[1];
    $i++;
    $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
    $sheet->setCellValueExplicit('A'.$i,'部門:'.$departmentinfo[$depID].',  計劃編號:'.$projID);
    foreach($emplyee as $k => $r) { 
      $letter = 65;
      $i++;
      $data = $r[key($r)];
     
      $sheet->setCellValueExplicit(chr($letter++).$i,$data['empName']);
      $sheet->setCellValueExplicit(chr($letter++).$i,$data['empID']);
      $sheet->setCellValueExplicit(chr($letter++).$i,$data['incomeNum']);
      $mTotal = 0;


      foreach ($monthList as $mKey => $mV) {
        if(!isset($sum['money'][$mV])) $sum['money'][$mV] = 0;
        $mD = '';
        if(isset($r[$mV])) {
          $mData = $r[$mV];
          $mD = $mData['money'];
          $mTotal+=$mD;
          $sum['money'][$mV] += $mD;
          $mD = number_format($mD);
        }
        $sheet->setCellValueExplicit(chr($letter++).$i,$mD);
      }
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($mTotal));
      
     
    }
    $letter = 65;
    $i++;

    $sheet->mergeCells(chr($letter).$i.':'.chr($letter+2).$i);//合併
    $sheet->setCellValueExplicit(chr($letter++).$i,'小計：');
    $letter += 2;
    $alltotal = 0;
    foreach ($sum['money'] as $sk => $sv) {
      if(!isset($allSum['money'][$sk])) $allSum['money'][$sk] = 0;
      $allSum['money'][$sk] += $sv;
      $alltotal+=$sv;
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($sv));
    }
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
    $i++;
  }

  $letter = 65;
  $i++;

  $sheet->mergeCells(chr($letter).$i.':'.chr($letter+2).$i);//合併
  $sheet->setCellValueExplicit(chr($letter++).$i,'總計：');
  $letter += 2;
  $alltotal = 0;
  foreach ($allSum['money'] as $sk => $sv) {
    $alltotal+=$sv;
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($sv));
  }
  $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));



  $objPHPExcel->getActiveSheet()->getStyle('B4:'.$tLetter.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $objPHPExcel->getActiveSheet()->getStyle('B4:'.$tLetter.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

  $sheet->getStyle('D3:P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');


?>