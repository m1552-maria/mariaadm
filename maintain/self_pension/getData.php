<?php 
	function getSelfRetire($year, $search_list = '', $projIDList = '') {
		$result = array();
		$dataList = array();

        $sql = "select r.wkMonth, r.id as rID, e.empID, e.empName, e.perID as incomeNum, i.money
        from emplyee_salary_report as r left join emplyee as e on r.empID=e.empID left join emplyee_salary_items i on r.id = i.fid ";
        $sql.= " where r.wkMonth >= '".($year-1)."12' and r.wkMonth <= '".$year."11' and i.title='勞退自提額'";
        if(trim($search_list) != '') $sql.= " and ".$search_list;
        $sql.= " order by e.projID,e.depID,e.empID, r.wkMonth";

       /* print_r($sql);
        exit;*/

    	$rs = db_query($sql);
    	$empIDList = array();
    	while ($r=db_fetch_array($rs)) {
      		$empIDList[$r['empID']] = "'".$r['empID']."'";
            if(!isset($dataList[$r['empID']][$r['wkMonth']])) $dataList[$r['empID']][$r['wkMonth']] = $r;
            else $dataList[$r['empID']][$r['wkMonth']]['money'] += $r['money'];
    	}

    	//取員工每月的計劃ID&部門ID
    	$empHistory = getEmpHistory($empIDList);
    	//塞入計劃ID&部門ID
    	$dataList = setProjIDAndDepID($dataList, $empHistory);


    	foreach ($dataList as $empID => $v1) {
	     	foreach ($v1 as $wkMonth => $v2) {
	     		$wm = explode('_', $wkMonth);
	     		$wm = $wm[0];

	        	$result['dataList'][$v2['depID'].'=*='.$v2['projID']][$empID][$wm] = $v2;
	      	}
	    }

	   /* echo "<pre>";
    print_r($result);
    exit;*/

	     //整理資料
    	$result = organisingData($result, $projIDList);

    	return $result;
	}
?>