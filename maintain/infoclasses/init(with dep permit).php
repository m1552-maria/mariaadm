<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	
	$bE = true;	//異動權限	
	$pageTitle = "資訊查閱分類表";
	$tableName = "infoclasses";
	$extfiles = '../';
	
	$PID = 'PID';
	$CLASS_PID = 'infoclasses_PID';
	// for classid.php
	$ID  = 'ID';
	$Title = 'Title';
	
	// Here for List.asp ======================================= //
	if($_SESSION['domain_Host']) {
		$RootClass = $_SESSION['user_classdef'][4];
		$rsT = db_query("select Title from infoclasses where ID='$RootClass'"); 
		$rT = db_fetch_array($rsT); 
		$ROOT_NAME = $rT[0];
	} else {
		$RootClass = 0;
		$ROOT_NAME = '資訊查閱';
	}
	if (isset($_REQUEST[filter])) $_SESSION[$CLASS_PID]=$_REQUEST[filter]; 
	if (!$_SESSION[$CLASS_PID]) $_SESSION[$CLASS_PID]=$RootClass;
	$filter = "$PID='$_SESSION[$CLASS_PID]'";
	if ($_REQUEST[cnm]) $_SESSION[cnm]=$_REQUEST[cnm]; else {
		if(count($_REQUEST)<2 || $_SESSION[$CLASS_PID]==0) $_SESSION[cnm]=$ROOT_NAME;
	}
	
	$defaultOrder = "ID";
	$searchField1 = "Title";
	$searchField2 = "TitleEn";
	$pageSize = 20;	//每頁的清單數量	
	//$channel = array(1=>'門市',2=>'網站');
	
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,PID,Title,shortName,TitleEn,description,rdate,Seque");
	$ftAry = explode(',',"代碼,上層類別,全稱,簡稱,英文名稱,敘述,建立日期,次序");
	$flAry = explode(',',"70,70,100,,,,100,");
	$ftyAy = explode(',',"ID,text,text,text,text,text,date,text");
	
	// Here for Append.asp =========================================================================== //
	//::pre-con in append.php
	$newfnA = explode(',',"PID,ID,Title,shortName,TitleEn,description,Seque");
	$newftA = explode(',',"上層類別,代碼,全稱,簡稱,英文名稱,敘述,次序");
	$newflA = explode(',',"20,20,80,,,80,");
	$newfhA = explode(',',",,,0,,6,");
	$newfvA = array($_SESSION[$CLASS_PID],'','','','','');
	$newetA = explode(',',"readonly,text,text,text,text,textbox,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,PID,Title,shortName,TitleEn,description,Seque");
	$editftA = explode(',',"代碼,上層類別,全稱,簡稱,英文名稱,敘述,次序");
	$editflA = explode(',',",,80,,,80,");
	$editfhA = explode(',',",,,,,6,");
	$editfvA = array('','','','','','','');
	$editetA = explode(',',"ID,text,text,text,text,textbox,text");
?>