<?
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include("../../miclib.php");
	
	$bE = true;	//異動權限	
	$pageTitle = "知識平台 維護";
	$tableName = "knowledges";
	$extfiles = '../';
	
	$PID = 'ClassID';
	$CLASS_PID = 'kwgclasses_PID';
	// for classid.php
	$className = "kwgclasses";
	$ID  = 'ID';
	$Title = 'Title';
	
	// Here for List.asp ======================================= //
	if($_SESSION['domain_Host']) {
		$RootClass = $_SESSION['user_classdef'][1];
		$rsT = db_query("select Title from kwgclasses where ID='$RootClass'"); 
		$rT = db_fetch_array($rsT); 
		$ROOT_NAME = $rT[0];
	} else {
		$RootClass = 0;
		$ROOT_NAME = '知識平台';
	}
	if (isset($_REQUEST[filter])) $_SESSION[$CLASS_PID]=$_REQUEST[filter]; 
	if (!$_SESSION[$CLASS_PID]) $_SESSION[$CLASS_PID]=$RootClass;
	$filter = "$PID like '$_SESSION[$CLASS_PID]'";
	if ($_REQUEST[cnm]) $_SESSION[cnm]=$_REQUEST[cnm]; else {
		if(count($_REQUEST)<2 || $_SESSION[$CLASS_PID]==0) $_SESSION[cnm] = $_SESSION[$CLASS_PID]=='%'?'所有類別':$ROOT_NAME;
	}

	// for Upload files and Delete Record use
	$ulpath = '../../data/knowledges/'. ($_REQUEST[ClassID]?$_REQUEST[ClassID]:$_SESSION[$CLASS_PID]) .'/';
	if (count($_FILES)>0) { //有上傳圖檔時才需	
	  if(!is_dir($ulpath)) mkdir($ulpath, 0755); 
	}
	
	// Here for List.asp ======================================= //		
	$defaultOrder = "ID";
	$searchField1 = "ID";
	$searchField2 = "title";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,ClassID,title,SimpleText,R_Date,beginDate,unValidDate,PID,Seque,isReady");
	$ftAry = explode(',',"編號,分類,名稱,內容說明,登錄時間,上架日,失效日,授權,次序,上架");
	$flAry = explode(',',"80,,,,,,,,,40");
	$ftyAy = explode(',',"ID,text,text,text,datetime,date,date,text,text,bool");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"ClassID,title,SimpleText,beginDate,unValidDate,filepath,Content,Seque");
	$newftA = explode(',',"分類,名稱,內容說明,上架日,失效日,上傳檔案,完整內容,次序");
	$newflA = explode(',',",80,80,,,70,80,");
	$newfhA = explode(',',",,3,,,,6,");	
	$newfvA = array($_SESSION[$CLASS_PID],'','','','','','',0);
	$newetA = explode(',',"readonly,text,textbox,date,date,file,textarea,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,title,SimpleText,beginDate,unValidDate,filepath,Content,Seque,isReady");
	$editftA = explode(',',"編號,名稱,內容說明,上架日,失效日,上傳檔案,完整內容,次序,上架");
	$editflA = explode(',',",80,80,,,,,,");
	$editfhA = explode(',',",,3,,,,6,,");	
	$editfvA = array('','','','','','','','');
	$editetA = explode(',',"text,text,textbox,date,date,file,textarea,text,checkbox");
?>