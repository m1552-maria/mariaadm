<?php 
	include("getData.php");

  $result = getData($year, implode(' and ', $search_list), implode(',', $projIDList));
	$dataList = $result['dataList'];
  $yearEnd = $result['yearEnd'];


  $dragonBoat = isset($result['dragonBoat']) ? $result['dragonBoat'] : array();
  $midAutumn = isset($result['midAutumn']) ? $result['midAutumn'] : array();
	$otherList = isset($result['otherList']) ? $result['otherList'] : array();

  $monthList = array(($year-1).'12' => ($year-1).'12');
  for($i=1; $i<=11; $i++) { 
    $v = $year.str_pad($i,2,'0',STR_PAD_LEFT);
    $monthList[$v] = $v;
  }

?> 
<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?php echo $pageTitle?></title>
 <link rel="stylesheet" href="<?php echo $extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link href="tuning.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script type="text/javascript" src="tuning.js" ></script> 
 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?php echo $pages?>;		//目前所在頁次
	var selid = '<?php echo $selid?>';	
	var keyword = '<?php echo $keyword?>';
	var cancelFG = true;	
	
  /*function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?php echo $pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?php echo $pages?>+1; if(pp><?php echo $pageCount?>) pp=<?php echo $pageCount?>; break;
      case 3 : pp=<?php echo $pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var url = "<?php echo $PHP_SELF?>?pages="+pp+"&keyword=<?php echo $keyword?>&fieldname=<?php echo $fieldname?>&sortDirection=<?php echo $sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(yrflt) url = url+'&year='+yrflt;

		location.href = url;		
	}*/

	/*$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<?php  if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?php echo $_REQUEST[empID]?>]')[0];
		<?php  } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<?php  } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
		
		
	});	*/
	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}


 </script>
 <script Language="JavaScript" src="<?php echo $extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<!--<input type="button" value="第一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?php echo $pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?php echo $pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">-->
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <!--<select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>全部部門</option>
		<?php  
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
		foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>-->
    
    <select name="year" id="year" onChange="doSubmit()">
    	<?php  foreach ($yearList as $key => $value) {
    		$sel = ($year == $key) ? 'selected':'';
    	 	 echo "<option $sel value='".$key."'>".$value." 年</option>"; 
    	} ?>
    	
    </select>
    <input type="text" name="projIDFilter" id="projIDFilter" value="<?php echo $projIDFilter?>" readonly style="background-color: #eae8e8;"> 
    <input type="button" value="點我選擇計畫" onClick="getExcelV()">
    <input type="button" value="匯出清冊" onclick="excelfm2.submit()">
    
  	</td>
  	<td align="right">總筆數：<?php echo $result['con']?><?php  if ($keyword!="") echo " / 搜尋：$keyword "; ?></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<th>薪資</th>
	<th>姓名</th>
	<th>員編</th>
	<th>十二月</th>
	<th>一月</th>
	<th>二月</th>
	<th>三月</th>
	<th>四月</th>
	<th>五月</th>
	<th>六月</th>
	<th>七月</th>
	<th>八月</th>
	<th>九月</th>
	<th>十月</th>
	<th>十一月</th>
	<th>薪資合計</th>
	<th>年終獎金</th>
	<th>端午節金</th>
	<th>中秋節金</th>
<?php  
	foreach ($otherList as $key => $value) {
		echo '<th>'.$key.'</th>';
	}
?>

	<th>節金合計</th>
	<th>總計</th>

</tr>
<?php 
$allSum = array('money'=>array(), 'taxPay'=>array());
foreach($dataList as $key => $emplyee) {
  $showList = explode('=*=', $key);
  $sum = array('money'=>array(), 'taxPay'=>array());
  $depID = $showList[0];
  $projID = $showList[1];
  echo '<tr valign="top"><td colspan="'.(21+count($otherList)).'" style="font-weight:bold;font-size: 13px;">部門:'.(isset($departmentinfo[$depID]) ? $departmentinfo[$depID] : $depID).',  計劃編號:'.$projID.'</td><tr>';
  foreach ($emplyee as $k => $r) {
		$data = $r[key($r)];

?>
  <tr valign="top">
    	<td>薪資小計＼節金</td><td rowspan="2"><?php echo $data['empName'];?></td><td rowspan="2"><?php echo $data['empID'];?></td>
  <?php 
  
  	$mTotal = 0;
    foreach ($monthList as $mKey => $mV) {
      if(!isset($sum['money'][$mV])) $sum['money'][$mV] = 0;
  		$mD = '';
  		if(isset($r[$mV])) {
  			$mData = $r[$mV];
  			$mD = $mData['mBasic'] - $mData['food'] + $mData['allowance'] - $mData['absence'];
  			$mTotal+=$mD;
        $sum['money'][$mV] += $mD;
  			$mD = number_format($mD);
  		}
 		  echo '<td>'.$mD.'</td>';
    }


  	echo '<td>'.number_format($mTotal).'</td>';
    $alltotal = 0;
    $money = '';
    if(!isset($sum['money']['yearEnd'])) $sum['money']['yearEnd'] = 0;
    if(isset($yearEnd[$key][$k])) {
      $money = $yearEnd[$key][$k]['bonus'];
      $alltotal+=$money;
      $sum['money']['yearEnd'] += $money;
    } 
    if($money != '') $money = number_format($money);
    echo '<td>'.$money.'</td>';

    $money = '';
    if(!isset($sum['money']['dragonBoat'])) $sum['money']['dragonBoat'] = 0;
    if(isset($dragonBoat[$key][$k])) {
      $money = $dragonBoat[$key][$k]['money'];
      $alltotal+=$money;
      $sum['money']['dragonBoat'] += $money;
    } 
    if($money != '') $money = number_format($money);
    echo '<td>'.$money.'</td>';

    $money = '';
    if(!isset($sum['money']['midAutumn'])) $sum['money']['midAutumn'] = 0;
    if(isset($midAutumn[$key][$k])) {
      $money = $midAutumn[$key][$k]['money'];
      $alltotal+=$money;
      $sum['money']['midAutumn'] += $money;
    } 
    if($money != '') $money = number_format($money);
    echo '<td>'.$money.'</td>';

  	foreach ($otherList as $o => $value) {
      if(!isset($sum['money'][$o])) $sum['money'][$o] = 0;
  		$other = '';
  		if(isset($value[$key][$k])) {
        $other = $value[$key][$k]['money'];
  			$alltotal+=$other;
        $sum['money'][$o] += $other;
  		}
      if($other != '') $other = number_format($other);
  		echo '<td>'.$other.'</td>';
  	}

  	echo '<td>'.number_format($alltotal).'</td>';
  	$alltotal+=$mTotal;
  	echo '<td>'.number_format($alltotal).'</td>';
  ?>
  </tr>   
 <tr valign="top">
    	<td>代扣所得稅</td>
   <?php 
  	$mTotal = 0;
  

  	foreach ($monthList as $mKey => $mV) {
  		if(!isset($sum['taxPay'][$mV])) $sum['taxPay'][$mV] = 0;
  		$mD = '';
  		if(isset($r[$mV])) {
  			$mData = $r[$mV];
  			$mD = $mData['taxPay'];
  			$mTotal+=$mD;
        $sum['taxPay'][$mV] += $mD;
  			$mD = number_format($mD);
  		}
 		  echo '<td>'.$mD.'</td>';
  	}

  	echo '<td>'.number_format($mTotal).'</td>';
    $alltotal = 0;
    $money = '';
    if(!isset($sum['taxPay']['yearEnd'])) $sum['taxPay']['yearEnd'] = 0;
    if(!isset($sum['taxPay']['dragonBoat'])) $sum['taxPay']['dragonBoat'] = 0;
    if(!isset($sum['taxPay']['midAutumn'])) $sum['taxPay']['midAutumn'] = 0;
    if(isset($yearEnd[$key][$k])) {
      $money = $yearEnd[$key][$k]['tax'];
      $alltotal+=$money;
      $sum['taxPay']['yearEnd']+=$money;
    } 
    if($money != '') $money = number_format($money);
  	echo '<td>'.$money.'</td>';
  	echo '<td></td>';
  	echo '<td></td>';
  	foreach ($otherList as $o => $value) {
      if(!isset($sum['taxPay'][$o])) $sum['taxPay'][$o] = 0;
  		$other = '';
  		if(isset($value[$key][$k])) {
        $other = $value[$key][$k]['taxMoney'];
        $alltotal+=$other;
        $sum['taxPay'][$o] += $other;
      }
      if($other != '') $other = number_format($other);
      echo '<td>'.$other.'</td>';
  	}	

  	echo '<td>'.number_format($alltotal).'</td>';
  	$alltotal+=$mTotal;
  	echo '<td>'.number_format($alltotal).'</td>';
  ?>
  </tr> 
<?php  
  } 

  echo '<tr valign="top">';
  echo '<td>薪資小計＼節金</td><td colspan="2" rowspan="2">小計：</td>';
  $alltotal = 0;
  $bonusTotal = 0;
  $s = 0;
  foreach ($sum['money'] as $sk => $sv) {
    if(!isset($allSum['money'][$sk])) $allSum['money'][$sk] = 0;
    $allSum['money'][$sk] += $sv;
    if(empty($monthList[$sk])) {
      $bonusTotal+=$sv;
      if($s == 0) {
        $s = 1;
        echo '<td>'.number_format($alltotal).'</td>';
      }
    }
    $alltotal+=$sv;
    echo '<td>'.number_format($sv).'</td>';
  }
  echo '<td>'.number_format($bonusTotal).'</td>';
  echo '<td>'.number_format($alltotal).'</td>';
  echo '</tr>';

  echo '<tr valign="top">';
  echo '<td>代扣所得稅</td>';
  $alltotal = 0;
  $bonusTotal = 0;
  $s = 0;

  foreach ($sum['taxPay'] as $sk => $sv) {
    if(!isset($allSum['taxPay'][$sk])) $allSum['taxPay'][$sk] = 0;
    $allSum['taxPay'][$sk] += $sv;
    if(empty($monthList[$sk])) {
      $bonusTotal+=$sv;
      if($s == 0) {
        $s = 1;
        echo '<td>'.number_format($alltotal).'</td>';
      }
    }
    $alltotal+=$sv;
    echo '<td>'.number_format($sv).'</td>';
  }
  echo '<td>'.number_format($bonusTotal).'</td>';
  echo '<td>'.number_format($alltotal).'</td>';
  echo '</tr>';
}
  echo '<tr valign="top"><td colspan="'.(21+count($otherList)).'">-</td></tr>';
  echo '<tr valign="top">';
  echo '<td>薪資小計＼節金</td><td colspan="2" rowspan="2">總計：</td>';
  $alltotal = 0;
  $bonusTotal = 0;
  $s = 0;
  foreach ($allSum['money'] as $sk => $sv) {
    if(empty($monthList[$sk])) {
      $bonusTotal+=$sv;
      if($s == 0) {
        $s = 1;
        echo '<td>'.number_format($alltotal).'</td>';
      }
    }
    $alltotal+=$sv;
    echo '<td>'.number_format($sv).'</td>';
  }
  echo '<td>'.number_format($bonusTotal).'</td>';
  echo '<td>'.number_format($alltotal).'</td>';
  echo '</tr>';

  echo '<tr valign="top">';
  echo '<td>代扣所得稅</td>';
  $alltotal = 0;
  $bonusTotal = 0;
  $s = 0;

  foreach ($allSum['taxPay'] as $sk => $sv) {
    if(empty($monthList[$sk])) {
      $bonusTotal+=$sv;
      if($s == 0) {
        $s = 1;
        echo '<td>'.number_format($alltotal).'</td>';
      }
    }
    $alltotal+=$sv;
    echo '<td>'.number_format($sv).'</td>';
  }
  echo '<td>'.number_format($bonusTotal).'</td>';
  echo '<td>'.number_format($alltotal).'</td>';
  echo '</tr>';

?> 

 </table>
</form>


<form name="excelfm2" action="excelDetail.php" method="post" target="excel">
	<input type="hidden" name="year" value="<?php echo $year;?>">
	<input type="hidden" name="projIDList" value="<?php echo implode(',', $projIDList);?>">
  <input type="hidden" name="search_list" value="<?php echo implode(' and ', $search_list);?>">
</form>

<script type="text/javascript">
	var rztExcel = false;
	function getExcelV(){
		var url = 'selectExcel.php?'; 
        if(rztExcel){
        	rztExcel.close();
        }
        if(rztExcel == false){
        	rztExcel = window.open(url, "web",'width=600,height=750');
        }else{
        	rztExcel = window.open(url, "web",'width=600,height=750');
        	rztExcel.focus();
        }
	}
</script>


</body>
</Html>