<?php 
  session_start();
  if(empty($_REQUEST['year'])) exit;
 
  $filename = $_REQUEST['year'].'年薪資、獎金統計(50)'.date("Y-m-d H:i:s",time()).'.xlsx';
  

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');
  include ('../inc_vars.php');

  include("getData.php");
  
  $result = getData($_REQUEST['year'], $_REQUEST['search_list'], $_REQUEST['projIDList']);
  $year = $_REQUEST['year'];
  $dataList = $result['dataList'];
  $yearEnd = $result['yearEnd'];
  $dragonBoat = isset($result['dragonBoat']) ? $result['dragonBoat'] : array();
  $midAutumn = isset($result['midAutumn']) ? $result['midAutumn'] : array();
  $otherList = isset($result['otherList']) ? $result['otherList'] : array();

  $monthList = array(($year-1).'12' => ($year-1).'12');
  for($i=1; $i<=11; $i++) { 
    $v = $year.str_pad($i,2,'0',STR_PAD_LEFT);
    $monthList[$v] = $v;
  }

  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();


  $letter = 65;
  $tLetter = chr($letter+20+count($otherList));
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);

  $i = 1;
  $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
  $sheet->setCellValueExplicit('A'.$i,$_REQUEST['year'].'年薪資、獎金統計(50)清冊');
 // $sheet->getStyle('A1:R1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A'.$i.':'.$tLetter.$i)->getFont()->setSize(16);

  $i++;
  $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
  $sheet->setCellValueExplicit('A'.$i,'計劃:'.$_REQUEST['projIDList']);
  $sheet->getStyle('A'.$i.':'.$tLetter.$i)->getFont()->setSize(14);

  $i++;
  $sheet->setCellValueExplicit(chr($letter++).$i,'薪資');
  $sheet->setCellValueExplicit(chr($letter++).$i,'姓名');
  $sheet->setCellValueExplicit(chr($letter++).$i,'員編');
  $sheet->setCellValueExplicit(chr($letter++).$i,'十二月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'一月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'二月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'三月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'四月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'五月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'六月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'七月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'八月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'九月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'十月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'十一月');
  $sheet->setCellValueExplicit(chr($letter++).$i,'薪資合計');
  $sheet->setCellValueExplicit(chr($letter++).$i,'年終獎金');
  $sheet->setCellValueExplicit(chr($letter++).$i,'端午節金');
  $sheet->setCellValueExplicit(chr($letter++).$i,'中秋節金');
  foreach ($otherList as $key => $value) {
    $sheet->setCellValueExplicit(chr($letter++).$i, $key);
    $objPHPExcel->getActiveSheet()->getStyle(chr($letter).$i)->getAlignment()->setWrapText(true);
  }
  $sheet->setCellValueExplicit(chr($letter++).$i,'節金合計');
  $sheet->setCellValueExplicit(chr($letter++).$i,'總計');

  $allSum = array('money'=>array(), 'taxPay'=>array());
  foreach($dataList as $key => $emplyee) {
    $showList = explode('=*=', $key);
    $sum = array('money'=>array(), 'taxPay'=>array());
    $depID = $showList[0];
    $projID = $showList[1];
    $i++;
    $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
    $sheet->setCellValueExplicit('A'.$i,'部門:'.$departmentinfo[$depID].',  計劃編號:'.$projID);
    foreach($emplyee as $k => $r) { 
      $letter = 65;
      $i++;
      $data = $r[key($r)];
     

      $sheet->setCellValueExplicit(chr($letter++).$i,'薪資小計＼節金');
      $sheet->setCellValueExplicit(chr($letter++).$i,$data['empName']);
      $sheet->setCellValueExplicit(chr($letter++).$i,' '.$data['empID']);
      $mTotal = 0;


      foreach ($monthList as $mKey => $mV) {
        if(!isset($sum['money'][$mV])) $sum['money'][$mV] = 0;
        $mD = '';
        if(isset($r[$mV])) {
          $mData = $r[$mV];
          $mD = $mData['mBasic'] - $mData['food'] + $mData['allowance'] - $mData['absence'];
          $mTotal+=$mD;
          $sum['money'][$mV] += $mD;
          $mD = number_format($mD);
        }
        $sheet->setCellValueExplicit(chr($letter++).$i,$mD);
      }
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($mTotal));
      $alltotal = 0;
      $money = '';
      if(!isset($sum['money']['yearEnd'])) $sum['money']['yearEnd'] = 0;
      if(isset($yearEnd[$key][$k])) {
        $money = $yearEnd[$key][$k]['bonus'];
        $alltotal+=$money;
        $sum['money']['yearEnd'] += $money;
      } 
      if($money != '') $money = number_format($money);
      $sheet->setCellValueExplicit(chr($letter++).$i,$money);

      $money = '';
      if(!isset($sum['money']['dragonBoat'])) $sum['money']['dragonBoat'] = 0;
      if(isset($dragonBoat[$key][$k])) {
       $money = $dragonBoat[$key][$k]['money'];
        $alltotal+=$money;
        $sum['money']['dragonBoat'] += $money;
      } 
      if($money != '') $money = number_format($money);
      $sheet->setCellValueExplicit(chr($letter++).$i,$money);

      $money = '';
      if(!isset($sum['money']['midAutumn'])) $sum['money']['midAutumn'] = 0;
      if(isset($midAutumn[$key][$k])) {
        $money = $midAutumn[$key][$k]['money'];
        $alltotal+=$money;
        $sum['money']['midAutumn'] += $money;
      } 
      if($money != '') $money = number_format($money);
      $sheet->setCellValueExplicit(chr($letter++).$i,$money);

      foreach ($otherList as $o => $value) {
        if(!isset($sum['money'][$o])) $sum['money'][$o] = 0;
        $other = '';
        if(isset($value[$key][$k])) {
          $other = $value[$key][$k]['money'];
          $alltotal+=$other;
          $sum['money'][$o] += $other;
        }
        $sheet->setCellValueExplicit(chr($letter++).$i,$other);
      }

      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
      $alltotal+=$mTotal;
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));

      $i++;
      $letter = 65;
      $sheet->setCellValueExplicit(chr($letter++).$i,'代扣所得稅');
      $letter++;
      $letter++;

      $mTotal = 0;

      foreach ($monthList as $mKey => $mV) {
        if(!isset($sum['taxPay'][$mV])) $sum['taxPay'][$mV] = 0;
        $mD = '';
        if(isset($r[$mV])) {
          $mData = $r[$mV];
          $mD = $mData['taxPay'];
          $mTotal+=$mD;
          $sum['taxPay'][$mV] += $mD;
          $mD = number_format($mD);
        }
        $sheet->setCellValueExplicit(chr($letter++).$i,$mD);
      }
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($mTotal));
      $alltotal = 0;
      $money = '';
      if(!isset($sum['taxPay']['yearEnd'])) $sum['taxPay']['yearEnd'] = 0;
      if(!isset($sum['taxPay']['dragonBoat'])) $sum['taxPay']['dragonBoat'] = 0;
      if(!isset($sum['taxPay']['midAutumn'])) $sum['taxPay']['midAutumn'] = 0;
      if(isset($yearEnd[$key][$k])) {
        $money = $yearEnd[$key][$k]['tax'];
        $alltotal+=$money;
        $sum['taxPay']['yearEnd']+=$money;
      } 
      if($money != '') $money = number_format($money);
      $sheet->setCellValueExplicit(chr($letter++).$i,$money);
      $letter++;
      $letter++;

      foreach ($otherList as $o => $value) {
        if(!isset($sum['taxPay'][$o])) $sum['taxPay'][$o] = 0;
        $other = '';
        if(isset($value[$key][$k])) {
          $other = $value[$key][$k]['taxMoney'];
          $alltotal+=$other;
          $sum['taxPay'][$o] += $other;
        }
         $sheet->setCellValueExplicit(chr($letter++).$i,$other);
      }

      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
      $alltotal+=$mTotal;
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));

     
    }
    $letter = 65;
    $i++;

    $sheet->setCellValueExplicit(chr($letter++).$i,'薪資小計＼節金');
    $sheet->mergeCells(chr($letter).$i.':'.chr($letter).($i+1));//合併
    $sheet->setCellValueExplicit(chr($letter++).$i,'小計：');
    $letter++;
    $alltotal = 0;
    $bonusTotal = 0;
    $s = 0;
    foreach ($sum['money'] as $sk => $sv) {
      if(!isset($allSum['money'][$sk])) $allSum['money'][$sk] = 0;
      $allSum['money'][$sk] += $sv;
      if(empty($monthList[$sk])) {
        $bonusTotal+=$sv;
        if($s == 0) {
          $s = 1;
          $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
        }
      }
      $alltotal+=$sv;
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($sv));
    }

    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($bonusTotal));
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
 

    $letter = 65;
    $i++;
    $sheet->setCellValueExplicit(chr($letter++).$i,'代扣所得稅');
    $letter++;
    $letter++;

    $alltotal = 0;
    $bonusTotal = 0;
    $s = 0;
    foreach ($sum['taxPay'] as $sk => $sv) {
      if(!isset($allSum['taxPay'][$sk])) $allSum['taxPay'][$sk] = 0;
      $allSum['taxPay'][$sk] += $sv;
      if(empty($monthList[$sk])) {
        $bonusTotal+=$sv;
        if($s == 0) {
          $s = 1;
          $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
        }
      }
      $alltotal+=$sv;
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($sv));
    }

    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($bonusTotal));
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
    $i++;
  }

  $letter = 65;
  $i++;

  $sheet->setCellValueExplicit(chr($letter++).$i,'薪資小計＼節金');
  $sheet->mergeCells(chr($letter).$i.':'.chr($letter).($i+1));//合併
  $sheet->setCellValueExplicit(chr($letter++).$i,'總計：');
  $letter++;
  $alltotal = 0;
  $bonusTotal = 0;
  $s = 0;
  foreach ($allSum['money'] as $sk => $sv) {
    if(empty($monthList[$sk])) {
      $bonusTotal+=$sv;
      if($s == 0) {
        $s = 1;
        $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
      }
    }
    $alltotal+=$sv;
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($sv));
  }

  $sheet->setCellValueExplicit(chr($letter++).$i,number_format($bonusTotal));
  $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));


  $letter = 65;
  $i++;
  $sheet->setCellValueExplicit(chr($letter++).$i,'代扣所得稅');
  $letter++;
  $letter++;

  $alltotal = 0;
  $bonusTotal = 0;
  $s = 0;
  foreach ($allSum['taxPay'] as $sk => $sv) {
    if(empty($monthList[$sk])) {
      $bonusTotal+=$sv;
      if($s == 0) {
        $s = 1;
        $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
      }
    }
    $alltotal+=$sv;
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($sv));
  }

  $sheet->setCellValueExplicit(chr($letter++).$i,number_format($bonusTotal));
  $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));



  $objPHPExcel->getActiveSheet()->getStyle('B4:'.$tLetter.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $objPHPExcel->getActiveSheet()->getStyle('B4:'.$tLetter.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');


?>