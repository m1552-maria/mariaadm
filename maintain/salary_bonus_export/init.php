<?php 	
	session_start();
  	header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	include ('../inc_vars.php');

	
	$bE = false;	//異動權限	
	$pageTitle = "薪資、獎金統計(50)";
	$tableName = "";


	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '';
	$delField = '';
	$delFlag = false;


	$year = date('Y');
	$yearList = array();
	for ($i=$year; $i >= ($year-3); $i--) { 
		$yearList[$i] = $i;
	}
	
	



	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	/*if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="b.depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "b.depID=''";
	}*/
	$projIDList = array();
	$projIDFilter = '';
	$temp = array();
	if(!empty($_REQUEST['projIDFilter'])) {
		$pList = explode(',', $_REQUEST['projIDFilter']);
		//$projIDList = $pList;
		$aa = array();
		foreach ($pList as $v) {
			if(trim($v) != '') $projIDList[] = $v;
		}
		$projIDFilter = $_REQUEST['projIDFilter'];
	} elseif ($_SESSION['privilege'] <= 10) {
		if($_SESSION['user_classdef'][7]) {
			$projIDList = $_SESSION['user_classdef'][7];
			$aa = array();
			foreach($_SESSION['user_classdef'][7] as $v){
				if($v) $projIDList[] = $v;
			}
		} else {
			$projIDList[] = '-1';
		}
	}

	if(!empty($_REQUEST['year'])){
		//$temp[] = "r.wkYear='$_REQUEST[year]'";	
		$year = $_REQUEST['year'];
	} else {
		//$temp[] = "r.wkYear='".$year."'";	
	}

	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //


	$searchField1 = "e.empID"; 
	$searchField2 = "e.empName";
	$sortDirection = 'ASC';

	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	// $fnAry = explode(',',"empID,empName,depID,spHolidays,actDays,trueDays,action");
	// $ftAry = explode(',',"編號,姓名,部門,今年度特休,會計年資,實際年資,調整");
	// $flAry = explode(',',"50,,,,,,,,,");
	// $ftyAy = explode(',',"id,text,text,text,text,text,text,text,text");
?>