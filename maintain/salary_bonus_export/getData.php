<?php  
  function getData($year, $search_list = '', $projIDList = '') {
    $result = getSalary($year, $search_list);
    $bonusData = getBonus($year, $search_list);
   	
   	foreach ($bonusData as $source => $v1) {
   		if($source != 'otherList') $result[$source] = $v1;
   		foreach ($v1 as $depProjID => $v2) {
   			foreach ($v2 as $empID => $v3) {
   				if(!isset($result['dataList'][$depProjID][$empID])) {
   					$result['dataList'][$depProjID][$empID][] = array(
   						'empID' => $empID,
   						'empName' => $v3['empName'],
   					);
   				}
   				if($source == 'otherList') $result[$source][$v3['bcTitle']][$depProjID][$empID] = $v3;
   			}
   		}
   	}


    //整理資料
    $result = organisingData($result, $projIDList);
    
    return $result;

  }

  function organisingData($result = array(), $projIDList = '') {
    if(!isset($result['dataList'])) {
      $result['con'] = 0;
      return $result;
    }
    //篩選所選計畫
    if($projIDList != '') {
      $projIDList = explode(',', $projIDList);
      $pCount = count($projIDList);
      foreach ($result['dataList'] as $key => $value) {
        $projID = explode('=*=', $key);
        $projID = $projID[1];
        $i = $pCount;
        foreach ($projIDList as $k => $v) {
          if(strpos((string)$projID, (string)$v) === 0) break; //有符合搜尋計畫編號條件
          $i--;
        }
        if($i == 0) {
          unset($result['dataList'][$key]);
        }

      }
    }
    
    //計算筆數
    $result['con'] = 0;
    foreach ($result['dataList'] as $depProjID => $v1) {
      foreach ($v1 as $empID => $v2) {
        $result['con']++;
      }
    }

    //排序
    $sql = "select depID, projID from emplyee_history where 1 order by projID asc, depID asc";
    $rs = db_query($sql);
    $DPList = array('=*=30'=>'=*=30');
    while ($r=db_fetch_array($rs)) {
      $DPList[$r['depID'].'=*='.$r['projID']] = $r['depID'].'=*='.$r['projID'];
    }

    $dataList = array_merge(array(),$result['dataList']);

    $result['dataList'] = array();

    foreach ($DPList as $key => $value) {
      if(isset($dataList[$key])) {
        $result['dataList'][$key] = $dataList[$key];
      }
    }

    return $result;
  }

  function getSalary($year, $search_list = '') {
    $result = array();

    //取每月薪資
    $sql = "select r.*, r.id as rID, e.empName , e.perID, e.perID as incomeNum, e.county, e.city, e.address 
    from emplyee_salary_report as r left join emplyee as e on r.empID=e.empID ";
    $sql.= " where r.wkMonth >= '".($year-1)."12' and r.wkMonth <= '".$year."11' ";
    if(trim($search_list) != '') $sql.= " and ".$search_list;
    $sql.= " order by e.projID,e.depID,e.empID, r.wkMonth";
    $rs = db_query($sql);
    $empIDList = array();
    $dataList = array();
    $rIDList = array();
    while ($r=db_fetch_array($rs)) {
      $rIDList[] = $r['rID'];
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $addsubList = array(
          'allowance' =>$r['addTotal'], //津貼及加項 +
          'overtime' =>0, //加班費 +
          'absence' =>0, //請假調整 -
          'taxPay' =>0, //所得稅 -
          'jobPay' =>0, //勞保 -
          'healthPay' =>0, //健保 -
          'mRetirePay' =>0, //自提退休金 -
          'otherPay' =>$r['subTotal'] //其他代扣 -
      );
      $r['source'] = 'dataList';
      $dataList[$r['empID']][$r['wkMonth']] = array_merge($r,$addsubList);
    }


    //取得薪資其他加減項
    $sql2 = "select * from emplyee_salary_items where fid in (".implode(',', $rIDList).")";
    $rs2=db_query($sql2);
    while ($r2=db_fetch_array($rs2)) {
      $itemList[$r2['fid']][$r2['id']] = $r2;
    }

    foreach ($dataList as $empID => $second) {
      foreach ($second as $wkMonth => $third) {
        if(!isset($itemList[$third['rID']])) continue;
        foreach ($itemList[$third['rID']] as $k2 => $r2) {
            if($r2['addsub'] == '+') {
                switch ($r2['iType']) {
                  case 'absence':
                    $dataList[$empID][$wkMonth]['absence'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;
                  case 'overtime':
                    $dataList[$empID][$wkMonth]['overtime'] += $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;  
                  case 'otherfee': //雜項費用
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;  
                  default:
                    switch ($r2['title']) {
                      case '勞保自負額':
                        $dataList[$empID][$wkMonth]['jobPay'] -= $r2['money'];
                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                        break;
                      case '健保自負額':
                        $dataList[$empID][$wkMonth]['healthPay'] -= $r2['money'];
                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                        break;
                      case '勞退自提額':
                        $dataList[$empID][$wkMonth]['mRetirePay'] -= $r2['money'];
                        $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                        break;
                      break;
                    }
                }
                    
            } else {
                switch ($r2['iType']) {
                  case 'absence':
                    $dataList[$empID][$wkMonth]['absence'] += $r2['money'];
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    break;
                  case 'overtime':
                    $dataList[$empID][$wkMonth]['overtime'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    break;
                  case 'allowance':
                    $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                    $dataList[$empID][$wkMonth]['allowance'] -= $r2['money'];
                    break;
                  default:
                    switch ($r2['title']) {
                      case '所得稅':
                          $dataList[$empID][$wkMonth]['taxPay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;
                      case '勞保自負額':
                          $dataList[$empID][$wkMonth]['jobPay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;
                      case '健保自負額':
                          $dataList[$empID][$wkMonth]['healthPay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;
                      case '勞退自提額':
                          $dataList[$empID][$wkMonth]['mRetirePay'] += $r2['money'];
                          $dataList[$empID][$wkMonth]['otherPay'] -= $r2['money'];
                          break;   
                      break;
                    }   
                }
            }
        }
      }
    }

    //取員工每月的計劃ID&部門ID
    $empHistory = getEmpHistory($empIDList);
    //塞入計劃ID&部門ID
    $dataList = setProjIDAndDepID($dataList, $empHistory);

    foreach ($dataList as $empID => $v1) {
      foreach ($v1 as $wkMonth => $v2) {
        $result[$v2['source']][$v2['depID'].'=*='.$v2['projID']][$empID][$wkMonth] = $v2;
      }
    }

    return $result;

  }

  function getBonus($year, $search_list = '') {
    $result = array();

    //年終獎金
  	$sql = "select y.bonus, y.tax, y.empID, e.empName  from emplyee_bonus_year_end y inner join bonus_festival b on y.wkYear = b.wkYear inner join emplyee e on y.empID = e.empID where b.wkYear='".($year-1)."' and b.type='year_end' and bonus > 0 ";
  	if(trim($search_list) != '') $sql.= " and ".$search_list;
  	$sql .= " order by e.empID ";
    $rs = db_query($sql);
    $dataList = array();
    $empIDList = array();
    while ($r=db_fetch_array($rs)) {
       //年終獎金要算在當年12月時所在的單位
      $r['wkMonth'] = ($year-1).'12';
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $r['source'] = 'yearEnd';
      $dataList[$r['empID']][$r['wkMonth']] = $r;
    }
    //三節獎金
    $sql = "select f.money, f.empID, date_format(b.giveDate, '%Y%m') as wkMonth, e.empName  from emplyee_bonus_festival f inner join bonus_festival b on f.fid = b.id inner join emplyee e on f.empID = e.empID where f.wkYear='".$year."' and f.title like '%端午%' ";
    if(trim($search_list) != '') $sql.= " and ".$search_list;
    $sql .= " order by e.empID";
    $rs = db_query($sql);
    while ($r=db_fetch_array($rs)) {
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $r['source'] = 'dragonBoat';
      $dataList[$r['empID']][$r['wkMonth']] = $r;
    }

    $sql = "select f.money, f.empID, date_format(b.giveDate, '%Y%m') as wkMonth, e.empName  from emplyee_bonus_festival f inner join bonus_festival b on f.fid = b.id inner join emplyee e on f.empID = e.empID where f.wkYear='".$year."' and f.title like '%中秋%' ";
    if(trim($search_list) != '') $sql.= " and ".$search_list;
    $sql .= " order by e.empID ";
    $rs = db_query($sql);
    while ($r=db_fetch_array($rs)) {
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $r['source'] = 'midAutumn';
      $dataList[$r['empID']][$r['wkMonth']] = $r;
    }

    //其他獎金
    $sql = "select b.money, b.empID, b.id, b.bcTitle, b.wkMonth, b.taxMoney, e.empName, c.invNo as checkInvNo  from emplyee_bonus b inner join bonus_class c on b.bcID = c.bcID inner join emplyee as e on b.empID = e.empID where b.wkMonth >= '".($year-1)."12' and b.wkMonth <= '".$year."11' and c.type='50' ";
    if(trim($search_list) != '') $sql.= " and ".$search_list;
    $sql .= " order by b.bcTitle, b.empID ";
    $rs = db_query($sql);
    while ($r=db_fetch_array($rs)) {
      $empIDList[$r['empID']] = "'".$r['empID']."'";
      $r['source'] = 'otherList';
      $dataList[$r['empID']][$r['wkMonth'].'_'.$r['id']] = $r;
    }


    //取員工每月的計劃ID&部門ID
    $empHistory = getEmpHistory($empIDList);
    //塞入計劃ID&部門ID
    $dataList = setProjIDAndDepID($dataList, $empHistory);

    foreach ($dataList as $empID => $v1) {
      foreach ($v1 as $wkMonth => $v2) {
        $result[$v2['source']][$v2['depID'].'=*='.$v2['projID']][$empID] = $v2;
      }
    }

    return $result;

  }


  function getEmpHistory($empIDList = array()) {
  	$result = array();
  	$sql = "select *, date_format(rdate, '%Y%m') as wkMonth from emplyee_history where empID in (".implode(',', $empIDList).") ";

    $sql .= " order by empID, rdate";
    $rs = db_query($sql);

    while ($r=db_fetch_array($rs)) {
    	$result[] = $r;
    }

    return $result;

  }

  function setProjIDAndDepID($dataList = array(), $empHistory = array()) {
  	global $UniformNumbersProjID;
    $firstData = array();
    foreach($empHistory as $k => $r) {
      if(!isset($dataList[$r['empID']])) continue;
      if(!isset($firstData[$r['empID']])) $firstData[$r['empID']] = $r;
      foreach ($dataList[$r['empID']] as $wkMonth => $value) {
        //先都預設給第一筆的計畫ID跟部門ID
        if(!isset($dataList[$r['empID']][$wkMonth]['projID'])) {
          $dataList[$r['empID']][$wkMonth]['projID'] = $firstData[$r['empID']]['projID'];
          $dataList[$r['empID']][$wkMonth]['depID'] = $firstData[$r['empID']]['depID'];
        }

        $_wkMonth = explode('_', $wkMonth);
        $_wkMonth = $_wkMonth[0];
        if((int)$_wkMonth >= (int)$r['wkMonth']) {
          $dataList[$r['empID']][$wkMonth]['projID'] = $r['projID'];
          $dataList[$r['empID']][$wkMonth]['depID'] = $r['depID'];
        }

        /*特殊狀況 其他獎金有統編者 直接歸類在該統編的部門*/
        if(!empty($value['checkInvNo'])) {
          $uList = explode('/', $UniformNumbersProjID[$value['checkInvNo']]);
          if(!in_array(substr($dataList[$r['empID']][$wkMonth]['projID'],0,2), $uList)) {
            $dataList[$r['empID']][$wkMonth]['projID'] = $uList[0];
            $dataList[$r['empID']][$wkMonth]['depID'] = '';
          }
        }

        /*特殊狀況 2019年12月(含12月)前的所有報稅金額 都要全算在統編05557110 計畫編號算30*/
        if((int)$wkMonth <= 201912) {
          $uList = explode('/', $UniformNumbersProjID['05557110']);
          if(!in_array(substr($dataList[$r['empID']][$wkMonth]['projID'],0,2), $uList)) {
         
            $dataList[$r['empID']][$wkMonth]['projID'] = '30';
          	$dataList[$r['empID']][$wkMonth]['depID'] = '';
   
          }
          
        }
        
      }
    }
    return $dataList;

  }

?>