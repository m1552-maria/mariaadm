<?php
	@session_start();
	include "../../config.php";
	header("Content-type: text/html; charset=utf-8");
	if($_REQUEST['Submit']) {
		$ulpath   = '../../data/overtime/';
		//處理上傳的檔案
		if ($_FILES[file1][tmp_name] != "") {
			$fsrc = $ulpath.'import.xls';
			if (!copy($_FILES[file1][tmp_name],$fsrc)) { //上傳失敗
				echo '附件上傳作業失敗 !'; exit;
			}
		}		
		
		require_once '../Excel/reader.php';  
		//建立excel檔的物件
		$data = new Spreadsheet_Excel_Reader();  
		//設定輸出編碼，指的是從excel讀取後再進行編碼
		$data->setOutputEncoding('UTF-8');  
		//載入要讀取的檔案
		$data->read($fsrc);  
		
		

		
		$msg = '';
		$LogTX = "";		
		//下面範例則是先讀取欄位再讀取列，因此i代表列的數目，j則代表欄位的數目
		for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
			//如下圖因為excel的表格第一列都會寫上欄位名稱，所以這邊預設不會讀取第一列
			if($i==1) {	//表格名稱
				continue;
			} else if($i==2) { //欄位名稱
				continue;
			} else {
				for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) { //員編 員工姓名 日期 開始時間 結束時間 加班事由 預計時數
					$ba[$i][$j-1] = $data->sheets[0]['cells'][$i][$j];	
				}
				//判斷欄位是否有問題
				if(is_empty($ba[$i])){
					$msg .='第'.$i.'列，此筆資料有欄位為空白\n';
					unset($ba[$i]);//刪除此筆資料
				}else{
					//判斷欄位格式有沒有問題
					$dAry=explode("-",$ba[$i][2]);
					if(checkdate($dAry[1],$dAry[2],$dAry[0])==false){//如果不是日期
						$msg .= '第'.$i.'列，此筆資料有欄位資料格式錯誤\n';
						unset($ba[$i]);//刪除此筆資料
					}
				}
			}
		}

		
		//處理資料日期//檢查匯入的資料中有哪些日期
		$new_list = array();
		foreach($ba as $key1=>$val1){
			///list($date_exp,$time_list) = explode(' ', $ba[$key1][2]);
			$date_exp = $ba[$key1][2];
			if($date_exp !=''){
				$new_list[$date_exp][$key1] = $val1;
			}
		}
		
		

		//撈已經存在資料庫的資料
		$overtime = array();
		$sql = 'select `id`,`empID`,`bDate`,`eDate`,`aType`,`content`,`isOK`,`hours` FROM `overtime` WHERE `isOK`=1';//從資料庫存進去一定是IS_OK
		$rs = db_query($sql);
		if(!db_eof($rs)){	
			while($r=db_fetch_array($rs)){
				list($date_exp,$time_list) = explode(' ', $r['bDate']);
				foreach($r as $key1=>$val1) $overtime[$date_exp][$r['id']][$key1] = $val1; 
			}
		}
		foreach($new_list as $key1=>$val1){
			foreach ($new_list[$key1] as $key2 => $val2) {
				if(isset($overtime[$key1])){
					//判斷存不存在
					$is_live = is_live($overtime[$key1],$new_list[$key1][$key2][0]);
					if($is_live[0] == 1){
						//要複寫過去
						$sql = 'update `overtime` SET `bDate`='."'".($new_list[$key1][$key2][2]." ".$new_list[$key1][$key2][3])."'".','.'`eDate`='."'".($new_list[$key1][$key2][2]." ".$new_list[$key1][$key2][4])."'".','.'`content`='."'".$new_list[$key1][$key2][5]."'".','.'`hours`='.round($new_list[$key1][$key2][6],1).' WHERE `id`='.$is_live[1];
						db_query($sql);
					}else{
						//無此筆資料 新增
						$sql = 'insert into overtime(empID,bDate,eDate,content,hours,isOK) values('."'".$new_list[$key1][$key2][0]."'".','."'".($new_list[$key1][$key2][2]." ".$new_list[$key1][$key2][3])."'".','."'".($new_list[$key1][$key2][2]." ".$new_list[$key1][$key2][4])."'".','."'".$new_list[$key1][$key2][5]."'".','.round($new_list[$key1][$key2][6],1).',1)';
						db_query($sql);
					}

				}else{//無此筆資料 新增
					$sql = 'insert into overtime(empID,bDate,eDate,content,hours,isOK) values('."'".$new_list[$key1][$key2][0]."'".','."'".($new_list[$key1][$key2][2]." ".$new_list[$key1][$key2][3])."'".','."'".($new_list[$key1][$key2][2]." ".$new_list[$key1][$key2][4])."'".','."'".$new_list[$key1][$key2][5]."'".','.round($new_list[$key1][$key2][6],1).',1)';
							db_query($sql);
				}
			}
		}
		if($msg == ''){
			$msg = '匯入成功';
		}
		$LogTX .= "<script>alert('$msg');location.href='$_SERVER[HTTP_REFERER]';</script>";  
		echo $LogTX;
	} 

	function is_empty($data){
		$empty_value = 0;
		foreach($data as $key1=>$val1){
			if($val1 == ''){
				$empty_value = 1;
			}
		}
		return $empty_value;
	}
	function is_live($overtime,$empID){
		$is_live = 0;
		$id = 0;
		foreach($overtime as $key3=>$val3){
			if($overtime[$key3]['empID'] == $empID){
				$is_live =1;
				$id = $overtime[$key3]['id'];
				break;
			}
		}
		$list = array("0"=>$is_live,"1"=>$id);
		return $list;
	}
?>
