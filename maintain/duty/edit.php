<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
	if($_GET[diffMonth]==1)	{	//下個月
		$diffMonth=$_GET["diffMonth"];
		$table = 'duty_days2';
	} else {	//本月
		$diffMonth=0;
		$table = 'duty_days';
	}
	//uType 0:週班表,1:特殊班表,2:輪班表
	if(isset($_REQUEST['uType']) && $_REQUEST['uType']=='2'){ //輪班表
	  $sql = "select *,p.id as pId,d.title as title from dutys as d,$table as days,duty_period as p,duty_map as m ";
	  $sql.="where d.dutyID='$ID' and days.dutyID=$ID and p.id=days.periodID and m.dutyID=$ID ";
		$rs = db_query($sql);
		$data=array();
		while($r=db_fetch_array($rs))	$data[$r["periodID"]]=$r["dutyDays"];
		//如果沒有下個月，就讀取本月資料(已經排除每日設定)
		//::toDo 但duty_map只有一份
		if(db_eof($rs)) $sql = "select *,p.id as pId,d.title as title from dutys as d,duty_days as days,duty_period as p,duty_map as m where d.dutyID='$ID' and days.dutyID=$ID and p.id=days.periodID and m.dutyID=$ID ";
		$rs = db_query($sql, $conn);
		$r = db_fetch_array($rs);
		
		//：：下個月的日期
		date_default_timezone_set("Asia/Taipei");
		$day_array=array("日","一","二","三","四","五","六"); 
		$this_year=date("Y",time()); 
		$this_month=date("m",time());
		$this_day=date("j",time()); 
		$this_month+=$diffMonth;
		if($this_month>12) { $this_month=1;	$this_year+=1; }
		$title_string=$this_year."年".$this_month."月"; 
		$what_day_is_fisrt_day=date("w",mktime(0,0,0,$this_month,1,$this_year)); //這個月第一天是星期幾
		$day_counter=1-$what_day_is_fisrt_day; //顯示這個月第一天的位置
		$how_many_days=mktime(0,0,0,$this_month+1,0,$this_year);
		$how_many_days_this_month=(strftime("%d",$how_many_days)); //這個月有幾天 
		
		//::時段
		$sql="select * from duty_period ";
	  $rs2=db_query($sql);
	  if(!db_eof($rs2)){
			$periodAry=array(0=>'請選擇時段');
			while($r2=db_fetch_array($rs2))	$periodAry[$r2['id']]=$r2['title'];
		}else{
			$periodAry[]="尚未設定時段";
		}
		$duty_uType=$_REQUEST['uType'];
	}else{//週班表,特殊班表
		$sql = "select * from dutys as d,duty_map as m where d.dutyID='$ID' and m.dutyID=$ID ";
		$rs = db_query($sql, $conn);
		$r = db_fetch_array($rs);
		$duty_uType=$r["uType"];
	}
	
	if ($_SESSION['privilege'] <= 10){
		$readonly='readonly';
		$class='readonly';
	}else{
		$readonly='';	
		$class='';
	} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<Head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7" />
<link rel="stylesheet" href="<?=$extfiles?>edit.css">
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<style type="text/css">
 body{margin:10px;}
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 .disabled{	background:#dedede;color:#666;}
 table.grid{  margin:10px;	border: 1px solid #ccc; border-collapse: collapse; }
 table.grid td {   border: 1px solid #999;	text-align:center;font-family:Verdana, Geneva, sans-serif;padding:3px;	font-size:12px;}
 .optbtn { width: 17px; height: 17px; vertical-align: middle; }
 .calendar_td{text-align:left;vertical-align: top;}
 .colLabel{	width:160px;}
 .nowrap{	display: inline;}
 .del_btn{width:25px;	cursor: pointer;}
 .readonly{	color:#999;}
</style>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
<script type="text/javascript" src="/Scripts/validation.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<title><?=$pageTitle?> - 編輯</title>
<script type="text/javascript">
	var diffMonth='<?=$diffMonth?>';
	var uType="<?=$_REQUEST['uType']?>"; //uType 0:週班表,1:特殊班表,2:輪班表
 	$(document).ready(function(){
 		if(diffMonth=='0'){
 			$('#btnNext').val("下個月");
 		}else{
 			$('#btnNext').val("本月");
 		}
 	});
 	function addItem(tbtn,fld) {
		var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6' <?=$readonly?> class='<?=$class?>'>"
			+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode;
		$(elm).remove();
	}
	function goBack(){
		window.parent.topFrame.location.href="list.php";
		window.parent.mainFrame.location.href='details.php';
	}
	function doclick(val){
		$('#next_action').val(val);
	}
	function formvalid(obj){
		if($('#apply').val()=="Y"){//立即套用
			if(!confirm("確定立即套用班表嗎？"))	return false;
		}
		if($('#title').val()==""){
			alert('請輸入班表名稱');
			return false;
		}
		if($('#depID').val()=="" && $('#empID').val()=="" && $('#jobID').val()==""){
			alert('請選擇適用對象');
			return false;
		}
		if(uType=='2'){//輪班表
			var cks=document.getElementsByName('ck');
			var periods;
			var dutyDays=[];
			var periodId;
			
			var bol=false;
			for(var i=0;i<cks.length;i++){
				if(cks[i].checked){
					bol=true;
					periods=document.getElementsByName( 'period_'+(i+1));
					for(var j=0;j<periods.length;j++){//多時段
						periodId=$(periods[j]).val();
						if(!dutyDays[periodId])	dutyDays[periodId]=[];
						dutyDays[periodId].push(cks[i].value);
					}
				}
			}
			if(!bol)	{	alert("請勾選要班表套用的日期");	return false;}

			$('#dutyDays').val(JSON.stringify(dutyDays));
		}
		return true;
	}
	function back(){
		if('<?=$_GET[uType]?>'=='2'){//月班表
			window.close();
		}else{
			history.back();
		}
	}

	function chgPeriod(obj){
		var diffMonth='<?=$diffMonth?>';
		var today=new Date();
		today=today.getDate();
		
		var cks=document.getElementsByName('ck');
		var periodId=$(obj).val();
		var periods,day_counter;
		for(var j=0;j<cks.length;j++){
			day_counter=j+1;
			if((diffMonth=="0" && day_counter>today) || diffMonth=="1"){//當月只能修改>當日的日期
				periods=document.getElementsByName('period_'+(j+1));
				for(var i=0;i<periods.length;i++){
					$(periods[i]).val(periodId);
				}
			}
		}

	}
	function selAll(obj){
		var checkItem = document.getElementsByName("ck");
		for(var i=0;i<checkItem.length;i++){
			checkItem[i].checked=obj.checked;
		}
	}
	function doNext(){
		var query_string='<?=$_SERVER['QUERY_STRING']?>';
		var qAry=query_string.split("&");
		var tmp;
		var newQ=[];
		for(var i=0;i<qAry.length;i++){
			tmp=qAry[i].split('=');
			if(tmp[0]=="diffMonth")	continue;
			newQ.push(qAry[i]);
		}

		if(diffMonth=='1')	diffMonth=0;	else diffMonth=1;
		location.href="edit.php?"+newQ.join("&")+"&diffMonth="+diffMonth;
	}
	function doApply(){
		$('#apply').val("Y");
	}
	function add_period(tbtn){
		var tdItem=$(tbtn).parents("td").eq(0);
		var newItem=$(tbtn).parents("td").eq(0).find("div").eq(0).clone(true);
		$(newItem).find("img").eq(0).css("display","");
		var elm = tbtn.parentNode;
		$(tdItem).append(newItem);
	}
	function del_period(tbtn){
		var item=tbtn.parentNode;
		$(item).remove();

	}
</script>
</Head>
<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>
	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>
	<? for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? 
		switch($editetA[$i]) { 
  	  case "text" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  		case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;			 	 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : break;
			default:	
				if($editfnA[$i]=="uType"){	
					if($r['uType']=="1"){//特殊班表
						echo $uType[$r['uType']]." - ".$specialType[$r['dType']];
					}else{
						echo $uType[$r['uType']];
					}
				}else{
					echo $r[$editfnA[$i]];
				}
				break;
  	} ?></td>
  </tr>
  <? } 
  if($duty_uType=="2"){//輪班表 ?>
    <tr valign="top" id='tr_date'><td align="right" class="colLabel" ></td>
      <td><table class='grid' >
      <?
      	$select="";
      	foreach($periodAry as $k=>$v){ $select.="<option value='".$k."' >".$v."</option>"; }
        echo "<tr><td colspan='7' style='text-align:left'>全部切換：<select onChange='chgPeriod(this)'>".$select."</select></td></tr>";
        echo "<tr><td colspan='7' class='title'>$title_string&nbsp;<input type='checkbox' class='optbtn' onclick='selAll(this)'>全選<input type='button' id='btnNext' onclick='doNext()' style='float:right' value='下個月'></td></tr>";
        echo "<tr>";
        for($list_week=0;$list_week<7;$list_week++){ 
          echo "<td>".$day_array[$list_week]."</td>"; //列出 日、一、二、三、四、五、六 
        } 
        echo "</tr>";
        for($pcol=1;$pcol<=6;$pcol++){ //一個月最多有六週 (在月曆上顯示的)，最多印出六週（六列） 
        	if($day_counter>$how_many_days_this_month){ 
          	//只要遞增的day_counter變數大於本月份的天數（例如一月是31天），就會跳出迴圈；若是天數在6週內結束，就不會全都印完（每一列） 
            break; 
         	} 
         	echo "<tr>"; 
         	for($prow=0;$prow<7;$prow++){ //一個星期有七天，所以是七行 
          	if($day_counter>0 && $day_counter<=$how_many_days_this_month){
            	$today=date("d",time());
          		if($diffMonth=="0" && $day_counter<=$today){//當月只能修改>當日的日期
						 		$disabled='disabled';
          		}else{
          			$disabled='';
          		}
              $periodID=0;
              $periodStr='';
              $c=0;
              foreach($data as $k=>$v){
              	$ary=explode(",",$v);
               	if(in_array($day_counter,$ary)){//有勾選的
               		$periodID=$k;
									$periodStr.="<div class='nowrap'><select name='period_".$day_counter."' id='period' style='width:100px' ".$disabled.">"; 
									foreach($periodAry as $k2=>$v2){
										if($k2==$periodID){	$selected='selected';}else{	$selected='';}
										$periodStr.="<option value='".$k2."' $selected>".$v2."</option>"; 
									}
									if($c>0 && $disabled==""){
										$periodStr.="</select><img class='del_btn' align='absmiddle' src='../images/del.png' onclick='del_period(this)'/><p>"; 
									}else{
										$periodStr.="</select><img class='del_btn' align='absmiddle' src='../images/del.png' style='display:none;' onclick='del_period(this)'/><p>"; 
									}
									$periodStr.="</div>";
               		$c++;
               	}
              }		
              if($periodID>0)	$checked='checked';	else $checked='';
              $select="<td class='calendar_td' style='text-align:left'>";
              $select.="<input type='checkbox' class='optbtn' name='ck' $checked $disabled value='$day_counter'/>".$day_counter."&nbsp;";
              if($disabled=="")	$select.="<input type='button' value='新增時段' onclick='add_period(this)'/>&nbsp;";
							if(!$periodID>0){//未勾選
								$select.="<div class='nowrap'><select name='period_".$day_counter."' id='period' style='width:100px' ".$disabled.">"; 
								foreach($periodAry as $k=>$v){
								if($k==$periodID){	$selected='selected';}else{	$selected='';}
								$select.="<option value='".$k."' $selected>".$v."</option>"; 
							}
							$select.="</select><img class='del_btn' align='absmiddle' src='../images/del.png' style='display:none;' onclick='del_period(this)'/><p>"; 
							$select.="</div>";
						}
						$select.=$periodStr;
						$select.="</td>"; 
						echo $select;			
          }else{ 
           	echo "<td>　</td>"; 
          } 
        	$day_counter++; 
        } 
        echo "</tr>"; 
    	} 
    ?>
    </table></td></tr>
  <? } ?>
   
  <tr valign="top">
  	<td align="right" class="colLabel">選取部門及員工</td>	
  	<td>
		<ul class='dep-emp-ul'>
		　<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
		　<li class='dep-emp-li'>
			<div class='query-title'>部門</div>
			<div class="depID_list">
			<? 
				if($r['depID']!=''){
					$depAry=explode(',',$r['depID']); 
					echo '<ul>';
					for($i=0; $i<count($depAry); $i++) {
						echo '<li><img src="/images/remove.png" width="15" align="top" onclick="d_remove(this)"><input type="hidden" name="depID[]" value="'.$depAry[$i].'">'.$departmentinfo[$depAry[$i]].'</li>';
					} 
					echo '</ul>';
				}else{
					echo 'NO DATA';
				}
			?>
			</div>
			</li>
      
			<li class='dep-emp-li'>
				<div class='query-title'>員工</div>
				<div class="empID_list">
				<? 
					if($r['empID']!=''){
						$depAry=explode(',',$r['empID']);
						echo '<ul>';
						for($i=0; $i<count($depAry); $i++) {
							echo '<li><img src="/images/remove.png" width="15" align="top" onclick="d_remove(this)"><input type="hidden" name="empID[]" value="'.$depAry[$i].'">'.$emplyeeinfo[$depAry[$i]].'</li>';
						} 
						echo '</ul>';	
					}else{
						echo 'NO DATA';
					}
				?>
				</div>
			</li>
		</ul>
  	</td>
  </tr>
  
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
    <input type="hidden" name='week_day_v' id='week_day_v'>
    <input type="hidden" name='tData' id='tData'>
    <input type="hidden" name='dutyDays' id='dutyDays'>
    <input type="hidden" name='uType' id='uType' value='<?=$r[uType]?>'>
    <input type="hidden" name='next_action' id='next_action'>
    <input type="hidden" name='apply' id='apply'>
    <input type="hidden" name='diffMonth' id='diffMonth' value="<?=$diffMonth?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="button" value="取消編輯" class="btn" onClick="back();">&nbsp;
		<!-- input type="submit" value="立即套用" class="btn" onclick="doApply()" -->
  </td>
</tr>
</table>
</form>
</body>
</html>
