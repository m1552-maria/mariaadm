<?php
	include("init.php"); 
	//：：下個月的日期
	date_default_timezone_set("Asia/Taipei");
	$day_array=array("日","一","二","三","四","五","六"); 
	$this_year=date("Y",time()); 
	$this_month=date("m",time());
	$this_day=date("j",time()); 
	$this_month+=1;
	if($this_month>12)	{
		$this_month=1;
		$this_year+=1;
	}
	$title_string=$this_year."年".$this_month."月"; 
	$what_day_is_fisrt_day=date("w",mktime(0,0,0,$this_month,1,$this_year)); //這個月第一天是星期幾
	$day_counter=1-$what_day_is_fisrt_day; //顯示這個月第一天的位置
	$how_many_days=mktime(0,0,0,$this_month+1,0,$this_year); 
	$how_many_days_this_month=(strftime("%d",$how_many_days)); //這個月有幾天 
	
	//::時段
	$sql="select * from duty_period ";
    $rs=db_query($sql);
    if(!db_eof($rs)){
	    $select="<option value='0'>請選擇時段</option>";
      while($r=db_fetch_array($rs)) $select.="<option value='$r[id]'>$r[title]</option>"; 
    }else{
      $select="<option>尚未設定時段</option>"; 
    }
    if ($_SESSION['privilege'] <= 10){
		$readonly='readonly';
		$class='readonly';
	}else{
		$readonly='';	
		$class='';
	} 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7" />
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
	.ui-widget {font-size:12px; }
	.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
	table.grid{  margin:10px;	border: 1px solid #ccc; border-collapse: collapse; }
	table.grid td {   border: 1px solid #999;	text-align:center;font-family:Verdana, Geneva, sans-serif;padding:3px;	font-size:12px; }
	.optbtn {  width: 18px; height: 18px; vertical-align: middle; }
	.title{	background:#ccc;}
	#tr_date{	display:none;	}
	.colLabel{	width:160px;}
	.nowrap{	display: inline;}
	.del_btn{width:25px;	cursor: pointer;}
	.calendar_td{text-align:left;vertical-align: top;}
	.readonly{	color:#999;}
 </style>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <script>
 var uType='<?=$_GET[uType]?>';
 	$(document).ready(function(){
 		if(uType=="2"){//月班表
 			$('#tr_date').show();
 		}
 	});
 	function addItem(tbtn,fld) {
		var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6' alt='multi' <?=$readonly?> class='<?=$class?>'>"
			+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode;
		$(elm).remove();
	}
	function formvalid(obj){
		if($('#title').val()==""){
			alert('請輸入班表名稱');
			return false;
		}
		if($('#depID').val()=="" && $('#empID').val()=="" && $('#jobID').val()==""){
			alert('請選擇適用對象');
			return false;
		}
		if(uType==2){//月班表
			var ck=document.getElementsByName("ck");
			var bol=false;
			var dutyDays=[];
			var periodId;
			for(var i=0;i<ck.length;i++){
				if(ck[i].checked){
					bol=true;
					periods=document.getElementsByName('period_'+(i+1));
					for(var j=0;j<periods.length;j++){//多時段
						periodId=$(periods[j]).val();
						if(!dutyDays[periodId])	dutyDays[periodId]=[];
						dutyDays[periodId].push(ck[i].value);
					}
				}
			}
			if(!bol)	{	alert("請勾選要班表套用的日期");	return false;}
			$("[name='dutyDays']").val(JSON.stringify(dutyDays));
			return true;
		}
		return true;
	}
	function chgPeriod(obj){
		var cks=document.getElementsByName('ck');
		var periodId=$(obj).val();
		var periods;
		for(var j=0;j<cks.length;j++){
			periods=document.getElementsByName('period_'+(j+1));
			for(var i=0;i<periods.length;i++){
				$(periods[i]).val(periodId);
			}
		}
	}

	function selAll(obj){
		var checkItem = document.getElementsByName("ck");
		for(var i=0;i<checkItem.length;i++){
			checkItem[i].checked=obj.checked;
		}
	}

	function back(){
		if('<?=$_GET[uType]?>'=='2'){//月班表
			window.close();
		}else{
			history.back();
		}
	}

	function add_period(tbtn){
		var tdItem=$(tbtn).parents("td").eq(0);
		var newItem=$(tbtn).parents("td").eq(0).find("div").eq(0).clone(true);
		$(newItem).find("img").eq(0).css("display","");
		var elm = tbtn.parentNode;
		$(tdItem).append(newItem);
	}
	function del_period(tbtn){
		var item=tbtn.parentNode;
		$(item).remove();
	}
 </script>
 <title><?=$pageTitle?> - 新增</title>
</Head>
<body class="page">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>
	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><? switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    :
				echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' class='input'>"; 
				break;
			case "textbox" :echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  			case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; break;
  			case "select":
  				if($newfnA[$i]=="uType"){
					echo $uType[$_GET[uType]];
					if($_GET[uType]=="1"){ //特殊班表
						echo "<select id='dType' name='dType' size='$newflA[$i]' class='input'>"; foreach($specialType as $k=> $v) echo "<option value='$k'>$v</option>"; echo "</select>"; 
					}
				}
 				break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
	</tr><? } ?>
	
  <tr valign="top" id='tr_date'><td align="right" class="colLabel" ></td>
    <td><table class='grid' >
    <?
    echo "<tr><td colspan='7' style='text-align:left'>切換：<select style='width:100px' onChange='chgPeriod(this)'>".$select."</select></td></tr>";
    echo "<tr><td colspan='7' class='title'>$title_string&nbsp;<input type='checkbox' class='optbtn' onclick='selAll(this)'>全選</td></tr>";
    echo "<tr>";
    for($list_week=0;$list_week<7;$list_week++){ 
      echo "<td>".$day_array[$list_week]."</td>"; //列出 日、一、二、三、四、五、六 
    } 
    echo "</tr>";
		for($pcol=1;$pcol<=6;$pcol++) { //一個月最多有六週 (在月曆上顯示的)，最多印出六週（六列） 
			if($day_counter>$how_many_days_this_month){ 
				//只要遞增的day_counter變數大於本月份的天數（例如一月是31天），就會跳出迴圈；若是天數在6週內結束，就不會全都印完（每一列） 
				break; 
			} 
			echo "<tr>"; 
			for($prow=0;$prow<7;$prow++){ //一個星期有七天，所以是七行 
				if($day_counter>0 && $day_counter<=$how_many_days_this_month){
					echo "<td class='calendar_td' style='text-align:left'><input type='checkbox' class='optbtn' name='ck' checked value='$day_counter'/>".$day_counter."&nbsp;";
					echo "<input type='button' value='新增時段' onclick='add_period(this)'/>&nbsp;";
					echo "<div class='nowrap'><select name='period_".$day_counter."' id='period' style='width:100px'>".$select."</select>";
					echo "<img class='del_btn' align='absmiddle' src='../images/del.png' style='display:none;' onclick='del_period(this)'/><p></div></td>"; 
				}else{ 
					echo "<td>　</td>"; 
				} 
				$day_counter++; 
			} 
			echo "</tr>"; 
		} 
    ?></table></td>
	</tr>
	
  <tr valign="top">
		<td align="right" class="colLabel">選取部門及員工</td>
		<td>
			<ul class='dep-emp-ul'>
				　<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
				　<li class='dep-emp-li'>
					<div class='query-title'>部門</div>
					<div class="depID_list">NO DATA</div>
				</li>
				<li class='dep-emp-li'>
					<div class='query-title'>員工</div>
					<div class="empID_list">NO DATA</div>
				</li>
			</ul>
	  	</td>
	</tr>

	<tr>
		<td colspan="2" align="center" class="rowSubmit">
      <input type="hidden" id='daysVal' name='daysVal'>
      <input type="hidden" id='periodVal' name='periodVal'>
      <input type="hidden" id='uType' name='uType' value="<?=$_GET[uType]?>">
			<input type="submit" value="確定新增" class="btn">&nbsp;
    	<input type="hidden" name='dutyDays' >
      <input type='button' class="btn" onClick="back()" value="取消新增">
		</td>
	</tr>
</table>
</form>
</body>
</html>
