<?
session_start();
if ($_SESSION['privilege']<1) {
	header('Content-type: text/html; charset=utf-8');
	echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
	exit; 	
}

include "../../config.php";
include('../../getEmplyeeInfo.php');	
header("Content-Type:text/html; charset=utf-8");
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');

$searchField1="e.empID";
$searchField2="e.empName";

//:: to 執行過濾功能 --------------------------------------------------------------------- >
if($_REQUEST[depFilter]){
	$temp[] = "d.depID like '$_REQUEST[depFilter]%'";
	$temp2[]	= "depID like '$_REQUEST[depFilter]%'";
}else if ($_SESSION['privilege'] <= 10) {	
	if($_SESSION['user_classdef'][2]) {
		$aa = array();
		foreach($_SESSION['user_classdef'][2] as $v){
			if($v){
				$aa[]="d.depID like '$v%'";	
				$aa2[]="depID like '$v%'";	
			} 
		}
		$temp[] = "(".join(' or ',$aa).")"; 	
		$temp2[] = "(".join(' or ',$aa2).")"; 	
	} else {
		// $temp[] = "d.depID=''";
		// $temp2[] = "depID=''";
	}
}
if(count($temp)>0) $filter = join(' and ',$temp);
if(count($temp2)>0) $filter2 = join(' and ',$temp2);
	
// echo 'filter:'.$filter."<br>";
//echo 'filter2:'.$filter2."<br>";
//::秀出清單
$nowYr = date('Y');
$curYr = $_REQUEST['year']?$_REQUEST['year']:$curYr=$nowYr;
if($_REQUEST['month']) $curMh = $_REQUEST['month']; else $curMh = date('m');
$curMh=str_pad($curMh,2,"0",STR_PAD_LEFT);

//關帳檢查
$cMan = $_SESSION["Account"]?$_SESSION["Account"]:$_SESSION["empID"];
$fCheck = checkAcctClose('abnormal',$curYr.$curMh, $cMan);
if (isset($_REQUEST["keyword"])) $keyword = $_REQUEST["keyword"]; else $keyword='';
if (!isset($_REQUEST['pages'])) $pages = 0; else $pages = $_REQUEST['pages'];

$sql="select *,ab.id as aId,ab.notes as aNotes,e.empName,ab.empID as empID,dt.id as dtId ";
$sql.="from emplyee_duty_abNormal as ab ";
$sql.="left join emplyee as e on ab.empID=e.empID ";
$sql.="left join department as d on e.depID=d.depID ";
$sql.="left join emplyee_duty as dt on dt.id=ab.fid ";
$sql.="where ab.abMonth='".$curYr.$curMh."' ";
if ($keyword!="") $sql.="and (($searchField1 like '%$keyword') or ($searchField2 like '%$keyword%')) ";
if($filter!="")	$sql.="and $filter ";
if(isset($_REQUEST[empFilter]) && $_REQUEST[empFilter]!=""){
	$sql.="and e.empID='".$_REQUEST[empFilter]."' and e.isOnduty=1 ";
}
if(isset($_REQUEST[sel_normal]))	$sel_normal=$_REQUEST[sel_normal];	else $sel_normal=0;
$sql.="and ab.normal=$sel_normal ";
$sql.="order by e.depID,e.empID,dt.startTime  ";
$excelSql=$sql;
$rs=db_query($sql);
$totals=db_num_rows($rs);
$pageSize=20;
$pageCount=(int)($totals / $pageSize)-1; if($pageCount<0) $pageCount=0;
if(($totals % $pageSize) and ($totals>$pageSize)) $pageCount++; 
$sql .= " limit ".$pageSize*$pages.",$pageSize"; 
// echo $sql."<br>";
$rs=db_query($sql);
$sql2 = "select empID,empName from emplyee ";
$sql2.="where isOnduty=1 ";
if($filter2!="")	$sql2.="and ".$filter2;

$rsX = db_query($sql2);
while( $rx = db_fetch_array($rsX) ) $emplyees[$rx[0]] = $rx[1];
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="cache-Control" content="no-cache, no-store, must-revalidate" />
 <meta http-equiv="pragma" content="no-cache" />
 <meta http-equiv="expires" content="0" />
 <title>出缺勤異常作業</title>
 <link rel="stylesheet" href="../list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
 <style type="text/css">
 	table.grid{  margin:10px;	border: 1px solid #ccc; border-collapse: collapse; width:800px;}
	table.grid td {   border: 1px solid #999;	text-align:center;font-family:Verdana, Geneva, sans-serif;padding:3px;	font-size:12px; width:110px;}
	#d_ck{	display:none;overflow: auto;height:400px; width:400px; position: fixed;top:80px;left:150px;}
	.divTitle{	padding:5px;}
	#d_setting{	display:none;overflow: auto;height:400px; width:400px; position: fixed;top:80px;left:120px;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script> 
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/jquery.blockUI.js"></script> 
 <script Language="JavaScript" src="list.js"></script> 
 <title></title>
 <script type="text/javascript">
 	var abId=0;
	var keyword = '<?=$keyword?>';
	var submitBtn;	//which submit

 	$(document).ready(function() {
		$("#d_ck").draggable();
	});

	function doSubmit() {
		var parms=[];
		parms.push("year="+$('select#year option:selected').val());
		parms.push("month="+$('select#month option:selected').val());
		parms.push("depFilter="+$('select#depFilter option:selected').val());
		parms.push("empFilter="+$('select#empFilter option:selected').val());
		parms.push("sel_normal="+$('select#sel_normal option:selected').val());
		parms.push("keyword=<?=$keyword?>");
		parms.push("sql=<?=$sql?>");
		location='abnormal.php?'+parms.join("&");
		// form1.submit();	
	}

	function filterSubmit(tSel) {
		// tSel.form.submit();
	}

	function insert_data(now_month){
		$.post( '../punch/api.php', { now_month: now_month,act:'insert_data' })
	}
	function doCheck(id,normal){
		$('[name="normal"]')[normal].checked = true;
		$.ajax({
		  url: "../emplyee_duty/api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act: "get_abNotes", id: id }
		}).done(function(rs) {
			var notes=rs.split("(by");
			$("#notes").val(notes[0]);
			abId=id;
			$('#d_ck').show();
		});

	}
	function ckAbnormal(){
		$.ajax({
		  url: "api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act: "ckAbnormal", id: abId, normal:$("input[name='normal']:checked").val() , notes:$("#notes").val()}
		}).done(function() {
			location.replace(location.href);
		});
	}
	  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var empflt = $('select#empFilter option:selected').val();
		var typflt = $('select#aType option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var mtflt = $('select#month option:selected').val();
		var abflt = $('select#sel_normal option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(empflt) url = url+'&empFilter='+empflt;
		if(typflt) url = url+'&aType='+typflt;
		if(yrflt) url = url+'&year='+yrflt;
		if(mtflt) url = url+'&month='+mtflt;
		if(abflt) url = url+'&sel_normal='+abflt;
		location.href = url;		
	}
	function SearchKeyword() {
		rzlt = prompt("請輸入關鍵字",keyword);
	 	if (rzlt!=null) {
			//location.href="list.php?keyword="+encodeURIComponent(rzlt);
			//::過濾條件要跟
			var depflt = $('select#depFilter option:selected').val();
			var empflt = $('select#empFilter option:selected').val();
			var typflt = $('select#aType option:selected').val();
			var yrflt = $('select#year option:selected').val();
			var mtflt = $('select#month option:selected').val();
			var url = "abnormal.php?keyword="+encodeURIComponent(rzlt);
			if(depflt) url = url+'&depFilter='+depflt;
			if(empflt) url = url+'&empFilter='+empflt;
			if(typflt) url = url+'&aType='+typflt;
			if(yrflt) url = url+'&year='+yrflt;
			if(mtflt) url = url+'&month='+mtflt;
			location.href = url;		
	 	}	 
	}
 </script> 
</Head>

<body>
<form name="form1" method="post" action="doAbnormal.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="<?=$keyword?>">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)">
		<input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)">
		<input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)">
		<input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <select name="depFilter" id="depFilter" onChange="doSubmit()"><option value=''>-全部部門-</option>
		<? 
	 if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
     } else {
		foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
  	 }

    ?>
    </select>
		<select name="empFilter" id="empFilter" onChange="doSubmit(this)"><option value=''>-全部員工-</option>
		<? foreach($emplyees as $k=>$v) echo "<option value='$k'".($_REQUEST[empFilter]==$k?' selected ':'').">$v</option>"; ?>
    </select>
    <select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    <select name="month" id="month" onChange="doSubmit()"><? 
    	if($curYr<$nowYr){	$m=12;	}else{	$m=date('m');}
    	for($i=1;$i<=$m;$i++) { $sel=($curMh==$i?'selected':''); echo "<option $sel value='$i'>$i 月</option>"; } ?></select>
    <select name="sel_normal" id="sel_normal" onChange="doSubmit()">
    	<option <?php echo ($_REQUEST[sel_normal]==0?'selected':'');?> value='0'>異常</option>
    	<option <?php echo ($_REQUEST[sel_normal]==1?'selected':'');?> value='1'>正常</option>
    </select>
		<!--<input type='button' value="匯入加班單" onClick="document.getElementById('d_setting').style.display='block'"/>&nbsp;-->
    <? if($fCheck==0 || ( $fCheck<>1 &&	strpos($fCheck,$cMan)===false) ) { ?>
    	<input type='submit' value="執行出缺勤作業" name='btn_abNormal' id='btn_abNormal' onClick="submitBtn=this.value"/>
    <? } else { ?>
    	<input type="button" value="執行出缺勤作業" disabled />
    <? } ?>
 		<input type="button" name="toExcely" value="匯出出缺勤異常" onClick="excelfm.submit()">
        
    <? if ($_SESSION['privilege'] <= 10) {
    		/*switch($fCheck) { 
      		case 0:  echo '<input type="submit" name="btn_abNormal" value="鎖定" onClick="submitBtn=this.value">'; break; 
					case 1:  echo '<input type="button" value="已關帳" disabled>'; break;
					default: 
						if( strpos($fCheck,$cMan)===false ) {
							echo '<input type="submit" name="btn_abNormal" value="鎖定" onClick="submitBtn=this.value">'; 
							$fCheck = 0;
						} else {
							echo '<input type="submit" name="btn_abNormal" value="解除鎖定" onClick="submitBtn=this.value">';
							$fCheck = 1;
						}
						break; 
				}*/
   	 	} else { 
				switch($fCheck) { 
	    		case 0: echo '<input type="submit" name="btn_abNormal" value="關帳" onClick="submitBtn=this.value">'; break;
					case 1: echo '<input type="button" value="已關帳" disabled>'; break;
  			  default: 
						echo '<input type="submit" name="btn_abNormal" value="關帳" onClick="submitBtn=this.value">'." <span class='sText' style='color:#00F'>已設鎖的帳號：$fCheck</span>";
				}
      } ?>
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$totals)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
</form>
<table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<tr class="colHD">
		<td width='30'>序號</td>
		<td width='50'>月份</td>
		<td width='80'>部門</td>
		<td width='50'>姓名</td>
		<td width='100'>狀態</td>
		<!-- <td width='30'>時數</td> -->
		<td >上班(排班)</td>
		<td>下班(排班)</td>
		<td>上班(打卡)</td>
		<td>下班(打卡)</td>
		<td>稽核</td>
	</tr>
<?
if(!db_eof($rs)){	
	$i=1;
	$now=date("Y-m-d H:i:s",time());
	while($r=db_fetch_array($rs)){
		// if($r["bDate"]>=$now)	continue;	?>
		<tr align="center">
			<td><?=$i?></td>
			<td><?=$r[abMonth]?></td>
			<td><?=$r[depTitle]?>&nbsp;</td>
			<td><?=$r[empName]?></td>
			<td><?=$r[abStatus]?></td>
			<!-- <td><?=$r[abHours]?></td> -->
			<td><?=$r['startTime']?></td>
			<td><?=$r['endTime']?></td>
			<?
			$st2=$r['startTime2']?$r['startTime2']:"未打卡";
			$ed2=$r['endTime2']?$r['endTime2']:"未打卡";
			?>
			<td><?=$st2?></td>
			<td><?=$ed2?></td>
			<td><? if(!$fCheck) { ?><input type='button' value='稽核' onClick='doCheck(<?=$r[aId]?>,<?=$r[normal]?>)'/><? } ?>
      </td>	
		</tr>
	<?
		$i++;
	}
}
?>
</table>
<div id='d_ck' class='divBorder1'>
	<div class='divTitle'>出缺勤異常稽核</div>
	<div class='divBorder2'>
		<table bgcolor="#F0F0F0" style="font-size:12px;padding:3px;" width='380'>
          <tr><td width='80'>核定出缺勤</td><td><input type='radio' name='normal' value='0'/>異常<input type='radio' name='normal' value="1"/>正常</td></tr>
          <tr><td>員工說明</td><td id='td_notes'><textarea id='notes' rows='5' cols='32'></textarea></td></tr>
          <tr height="40" valign="bottom">
          	<td align="center" colspan="2">
            <input type="button" value="確定" onclick='ckAbnormal()'>
            <button type="button" onClick="document.getElementById('d_ck').style.display='none'">取消</button>
          </td></tr>
         </table>
	</div>
</div>

<div id='d_setting' class='divBorder1'>
	<div class='divTitle'>匯入加班單資料</div>
	<div class='divBorder2'>
    	<form id="fm" action="importExcel.php" method="post" enctype="multipart/form-data" style="margin:0">
		<table bgcolor="#F0F0F0" style="font-size:12px;padding:3px;" width='380'>
          <tr><td>選擇要匯入的Excel檔：<br/><span style="color:#666666; font-size:12px">請選擇格式為 xls 的 excel 檔案，內容必須遵守範本的排法。</span>
          		<br/><a href="/data/overtime/template.xls" target="new">下載範本</a></td></tr>
          <tr><td><input type="file" name="file1" size="30" /></td></tr>
          <tr height="40" valign="bottom"><td align="center">
            <input type="submit" name="Submit" value="匯入">
            <button type="button" onClick="document.getElementById('d_setting').style.display='none'">取消</button>
          </td></tr>
         </table>
         </form>
	</div>
</div>
<form name="excelfm" action="excel.php" method="post" target="excel">
	<input type="hidden" name="fmTitle" value="員工出缺勤異常表">
  	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
  	<input type="hidden" name="empID" value="<?=$_REQUEST[empFilter]?>">
  	<input type="hidden" name="year" value='<?=$curYr?>'>
  	<input type="hidden" name="month" value='<?=$curMh?>'>
	<input type="hidden" name="sql" value="<?=$excelSql?>">
</form>
</body>
</Html>