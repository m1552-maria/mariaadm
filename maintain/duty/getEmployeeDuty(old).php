<?php
header("Content-Type:text/html; charset=utf-8");
class empData{	public $empID;	public $dutyID;	public $uType;	public $dType; }
class depData{	public $depID;	public $dutyID;	public $dType; }

/* --------------------------------
	從班表設定重建員工值班表
  --------------------------------- */
function getEmployeeDuty($dutyID){
	$ary=array();
	$empAry=array();
	$depAry=array();
	$sql="select * from dutys as d left join duty_map as m on d.dutyID=m.dutyID  ";
	// echo $sql."\n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$uType=$r['uType'];
			if($r["depID"]!=""){
				if(!$depAry[$uType])	$depAry[$uType]=array();
				$dAry=explode(",",$r['depID']);
				for($i=0;$i<count($dAry);$i++){
					$depObj = new depData;
					$depObj->depID = $dAry[$i];
					$depObj->dutyID = $r["dutyID"];
					$depObj->dType= $r["dType"];
					if(!in_array($depObj, $depAry[$uType]))	array_push($depAry[$uType],$depObj);
				}
			}
			if($r["empID"]!=""){
				if(!$empAry[$uType])	$empAry[$uType]=array();
				$eAry=explode(",",$r['empID']);
				for($i=0;$i<count($eAry);$i++){
					$empObj = new empData;
					$empObj->empID = $eAry[$i];
					$empObj->dutyID = $r["dutyID"];
					$empObj->uType = $r["uType"];
					$empObj->dType= $r["dType"];
					array_push($empAry[$uType],$empObj);
				}
			}
		}
	}

	// ::依部門取得個人資料
	foreach($depAry as $k=>$v){//k:uType 
		foreach($v as $k1=>$v1){//v1:depObj
			if(!$v1->depID)	continue;
			$sql="select * from emplyee where isOnduty=1 and depID like '".$v1->depID."%' ";
			$rs=db_query($sql);
			if(!db_eof($rs)){
				$depLen=strlen($v1->depID);
				while($r=db_fetch_array($rs)){
					$duplicate=false;
					if(!$empAry[$k])	$empAry[$k]=array();
					foreach($empAry[$k] as $objK=>$eItem){//檢查是否有重覆排班
						if($eItem->depID){
							if($eItem->empID==$r["empID"] && $k==$r["uType"] ){
									// echo $v1->dutyID.','.$empItem->empID.',emp depID:'.$eItem->depID.",v1 depID:".$v1->depID."\n";
								if(($eItem->depLen < $depLen && strpos($v1->depID,$eItem->depID)!==false)){//若有多部門都複選員工，以較小的部門為主
							    	$duplicate=false;
							    	// echo 'unset dType:'.$k.',objK:'.$objK.','.$eItem->empID.','.$eItem->dutyID.',eItem depID:'.$eItem->depID.',v1->depID:'.$v1->depID."\n";
							    	unset($empAry[$k][$objK]);
							    	break;
							    }
							}
						}else{
							if($eItem->empID==$r["empID"] && $k==$r["uType"] ){
						    	 // echo 'duplicate:'.$duplicate.",empID:".$r["empID"].",uType:".$k.',dutyID:'.$r[dutyID].','.$r["uType"]."\n";
						    	$duplicate=true;
						    	break;
						    }	
					    }
					}
					// echo "\n";
					if(!$duplicate){
						$empObj = new empData;
						$empObj->empID = $r["empID"];
						$empObj->dutyID = $v1->dutyID;
						$empObj->depID=$v1->depID;
						$empObj->depLen=strlen($v1->depID);
						$empObj->uType = $k;
						$empObj->dType = $v1->dType;
						// echo 'addEmp k:'.$k.',dutyID:'.$v1->dutyID.',empID:'.$r["empID"]."\n";
						array_push($empAry[$k],$empObj);

					}
				}
			}
		}
	}
	
	$nowY=date("Y");
	$nowM=str_pad(date("m"),2,"0",STR_PAD_LEFT);
	$days_in_month = cal_days_in_month(CAL_GREGORIAN, $nowM, $nowY);
	//::輪班班表
	$sql="select *,days.id as dayId,days.periodID as periodID ";
	$sql.="from dutys as d ";
	$sql.="left join duty_map as m on d.dutyID=m.dutyID ";
	$sql.="left join duty_days as days on d.dutyID=days.dutyID ";
	$sql.="left join duty_period as p on days.periodID=p.id ";
	$sql.="where d.uType=2 and d.dType=0 ";
	$rs_emp=db_query($sql);
	$rs=db_query($sql);
	
	$ary=array();
	$ary['empID']=array();
	$ary['startTime']=array();
	$ary['endTime']=array();
	$ary['year']=array();
	$ary['month']=array();
	$ary['day']=array();
	$ary['dutyID']=array();

	$has_shift_duty=array();//記錄有輪班的員工，不需要再排特殊&週班表
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if($r["depID"]=="" && $r["empID"]=="")	continue;
			$days=explode(",",$r["dutyDays"]);
			$dayC=count($days);
			foreach($empAry[2] as $eItem){
				if($r[dutyID]!=$eItem->dutyID)	continue;
				$empID=$eItem->empID;
				for($i=0;$i<$dayC;$i++){
					$days[$i]=str_pad($days[$i],2,"0",STR_PAD_LEFT);
					$startTime=$nowY."-".$nowM."-".str_pad($days[$i],2,"0",STR_PAD_LEFT)." ".$r["startTime"];
					$endTime=$nowY."-".$nowM."-".str_pad($days[$i],2,"0",STR_PAD_LEFT)." ".$r["endTime"];
					// echo '輪班:'.$empID.',startTime:'.$startTime.',endTime:'.$endTime."\n";	
					array_push($ary['empID'],$empID);
					array_push($ary['startTime'],$startTime);
					if(strtotime($startTime)>strtotime($endTime)){//有跨天時，處理endTime
						if(($days[$i]+1)<=$days_in_month){//跨天時，是否跨月
							$endTime=$nowY."-".$nowM."-".str_pad($days[$i]+1,2,"0",STR_PAD_LEFT)." ".$r['endTime'];
						}else{
							$endTime=$nowY."-".intval($nowM+1)."-01 ".$r['endTime'];
						}
					}else{
						$endTime=$nowY."-".$nowM."-".str_pad($days[$i],2,"0",STR_PAD_LEFT)." ".$r['endTime'];
					}
					array_push($ary['endTime'],$endTime);
					array_push($has_shift_duty,$empID);
					array_push($ary['year'],$nowY);
					array_push($ary['month'],$nowM);
					array_push($ary['day'],str_pad($days[$i],2,"0",STR_PAD_LEFT));
					array_push($ary['dutyID'],$r[dutyID]);
				}
			}
		}
	}
	//：：週班表
	$sql="select * from dutys as d ";
	$sql.="left join duty_map as m on d.dutyID=m.dutyID ";
	$sql.="left join duty_week as w on d.dutyID=w.dutyID ";
	$sql.="where d.uType=0 and d.dType=0 ";
	// echo $sql."\n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if($r["depID"]=="" && $r["empID"]=="")	continue;
			foreach($empAry[0] as $eItem){
				if($r[dutyID]!=$eItem->dutyID)	continue;
				$empID=$eItem->empID;
				if(in_array($empID,$has_shift_duty))	continue;
				for($i=1;$i<=$days_in_month;$i++){
					$dayOfWeek = date('w', strtotime($nowY."-".$nowM."-".$i));
					if($dayOfWeek==$r['week_day']){
						$startTime=$nowY."-".$nowM."-".str_pad($i,2,"0",STR_PAD_LEFT)." ".$r['startTime'];
						$endTime=$nowY."-".$nowM."-".str_pad($i,2,"0",STR_PAD_LEFT)." ".$r['endTime'];
						array_push($ary['empID'],$empID);
						array_push($ary['startTime'],$startTime);
						if(strtotime($startTime)>strtotime($endTime)){//有跨天時，處理endTime
							if(($i+1)<=$days_in_month){//跨天時，是否跨月
								$endTime=$nowY."-".$nowM."-".str_pad(($i+1),2,"0",STR_PAD_LEFT)." ".$r['endTime'];
							}else{
								$endTime=$nowY."-".intval($nowM+1)."-01 ".$r['endTime'];
							}
						}else{
							$endTime=$nowY."-".$nowM."-".str_pad($i,2,"0",STR_PAD_LEFT)." ".$r['endTime'];
						}
						// echo $r[dutyID].','.$empID.','.$startTime.'~'.$endTime."\n";
						array_push($ary['endTime'],$endTime);
						array_push($ary['year'],$nowY);
						array_push($ary['month'],$nowM);
						array_push($ary['day'],str_pad($i,2,"0",STR_PAD_LEFT));
						array_push($ary['dutyID'],$r[dutyID]);
					}
				}
			}
		}
	}

	//::特殊報表-上班
	$sql="select *,m.id as mId from dutys as d ";
	$sql.="left join duty_month as m on d.dutyID=m.dutyID ";
	$sql.="left join duty_map as map on map.dutyID=d.dutyID ";
	// $sql.="where d.uType=1 and d.dType=0 and m.startTime>cast(CURRENT_TIMESTAMP as date) ";
	$sql.="where d.uType=1 and d.dType=0 and m.startTime>='".$nowY."-".$nowM."-01'";
	 // echo $sql."\n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if($r["depID"]=="" && $r["empID"]=="")	continue;
			$addDate=getdate(strtotime($r[addDate]));
			$addM=str_pad($addDate['mon'],2,"0",STR_PAD_LEFT);
			$addD=str_pad($addDate['mday'],2,"0",STR_PAD_LEFT);
			foreach($ary['year'] as $k=>$v){
				$empID=$ary['empID'][$k];
				if(in_array($empID,$has_shift_duty))	continue;
				if($ary['year'][$k]==$addDate["year"] && $ary['month'][$k]==$addM && $ary['day'][$k]==$addD && $ary["empID"][$k]==$empID){
					$st=getdate(strtotime($ary['startTime'][$k]));	//取得被補班的日期-開始時間
					$stH=str_pad($st["hours"],2,"0",STR_PAD_LEFT);
					$stM=str_pad($st['minutes'],2,"0",STR_PAD_LEFT);

					$et=getdate(strtotime($ary['endTime'][$k]));	//取得被補班的日期-結束時間
					$etH=str_pad($et["hours"],2,"0",STR_PAD_LEFT);
					$etM=str_pad($et['minutes'],2,"0",STR_PAD_LEFT);

					array_push($ary['empID'],$empID);
					array_push($ary['startTime'],date("Y-m-d", strtotime($r[startTime]))." ".$stH.":".$stM);
					array_push($ary['endTime'],date("Y-m-d", strtotime($r[startTime]))." ".$etH.":".$etM);
					array_push($ary['dutyID'],$r[dutyID]);

					// echo "被補班:".$empID.','.$k.",".$ary['startTime'][$k].",".$ary['year'][$k]."\n";
					// echo "補班 s:".date("Y-m-d", strtotime($r[startTime]))." ".$stH.":".$stM.":".$stS."\n";
					// echo "補班 e:".date("Y-m-d", strtotime($r[startTime]))." ".$etH.":".$etM.":".$etS."\n\n";
					unset($ary['startTime'][$k]);
					unset($ary['endTime'][$k]);
					unset($ary['empID'][$k]);
					unset($ary['year'][$k]);
					unset($ary['month'][$k]);
					unset($ary['day'][$k]);
					unset($ary['dutyID'][$k]);
				}
			}
		}
	}

	//::特殊報表-放假
	$sql="select *,m.id as mId from dutys as d ";
	$sql.="left join duty_month as m on d.dutyID=m.dutyID ";
	$sql.="left join duty_map as map on map.dutyID=d.dutyID ";
	// $sql.="where d.uType=1 and d.dType=1 and m.startTime>cast(CURRENT_TIMESTAMP as date) ";
	$sql.="where d.uType=1 and d.dType=1 and m.startTime>='".$nowY."-".$nowM."-01'";
	//echo $sql; // exit;
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if($r["depID"]=="" && $r["empID"]=="")	continue;
			$addDate=getdate(strtotime($r[startTime]));
			$addM=str_pad($addDate['mon'],2,"0",STR_PAD_LEFT);
			$addD=str_pad($addDate['mday'],2,"0",STR_PAD_LEFT);
			// echo 'empID:'.$empID.",Y:".$ary['year'][$k].",addY:".$addDate["year"].",addM:".$addM.',startTime:'.$r[startTime].',depID:'.$r[depID].',empID:'.$r[empID]."\n";
			// echo $r[mId].',empID:'.$empID.",addY:".$addDate["year"].",addM:".$addM.',addD:'.$addD.',startTime:'.$r[startTime].',depID:'.$r[depID]."\n";
			foreach($ary['year'] as $k=>$v){
				$empID=$ary['empID'][$k];
				if(in_array($empID,$has_shift_duty)) continue;
				$endDate=getdate(strtotime($r[endTime]));
				$endM=str_pad($endDate['mon'],2,"0",STR_PAD_LEFT);
				$endD=str_pad($endDate['mday'],2,"0",STR_PAD_LEFT);
				if($ary['year'][$k]==$endDate["year"] && $ary['month'][$k]==$endM && ($ary['day'][$k]>=$addD && $ary['day'][$k]<=$endD ) && $ary["empID"][$k]==$empID){
					// echo "@unset empID:".$empID.',startTime:'.$ary['startTime'][$k].',endTime:'.$ary['endTime'][$k]."\n";
					unset($ary['startTime'][$k]);
					unset($ary['endTime'][$k]);
					unset($ary['empID'][$k]);
					unset($ary['year'][$k]);
					unset($ary['month'][$k]);
					unset($ary['day'][$k]);
					unset($ary['dutyID'][$k]);
				}
			}
		}
	}

	$empID=$ary['empID'];		//要寫入的資料
	$startTime=$ary['startTime'];
	$endTime=$ary['endTime'];
	$dutyIDAry=$ary['dutyID'];
	// print_r($ary);
	if($dutyID>0){//::立即套用班表,今天以後
		//::toDO 今天到月底
		$sql="delete from emplyee_duty where empID in ('".join("','",$empID)."') and cast(startTime as date) > cast(now() as date) ";
		db_query($sql);
		$today=date("d",time());
		foreach($empID as $k=>$v){
			$getdate=getdate(strtotime($startTime[$k]));
			if($getdate["mday"]>$today){//更新大於今天的
				if($endTime[$k]=="") continue;//若是有特殊班表-補班，但沒有取得對應的週班表時段，略過
				$sql="insert into emplyee_duty(empID,startTime,endTime,dutyID) values ('".$v."','".$startTime[$k]."','".$endTime[$k]."',$dutyIDAry[$k])";
				// echo $sql."\n";
				db_query($sql);
			}
		}
	}else{
		$execDate = date("Y-m", time());
		$sql="delete from emplyee_duty where startTime>'".$execDate."'";
		// echo $sql."\n";
		db_query($sql);
		
		foreach($empID as $k=>$v){
			if($endTime[$k]=="") continue;//若是有特殊班表-補班，但沒有取得對應的週班表時段，略過
			$sql="insert into emplyee_duty(empID,startTime,endTime,dutyID) values ('".$v."','".$startTime[$k]."','".$endTime[$k]."',$dutyIDAry[$k])";
			// echo $sql."\n";
			db_query($sql);
		}
	}
}
?>