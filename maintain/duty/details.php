<?php
	@session_start();
	if(!isset($_GET[id]))	exit;
	include 'init.php';	
	$id = $_GET[id];
	$type=$_GET[uType];
	$dType=$_GET[dType];
	if($type=="0"){	$tb='duty_week';	}else if($type=="1"){	$tb="duty_month";	}else if($type=="2"){ $tb='dutys';	}
	if($type=="0" || $type=="1" ){//週班表及特殊班表
		$sql2="select *,d.id as id,d1.dType as dType from dutys as d1 ,$tb as d left join duty_map as m ";
		$sql2.="on d.dutyID=m.dutyID ";
		$sql2.="where d.dutyID=".$id;
		$sql2.=" and d1.dutyID=".$id;
		$sql2.=" order by d.dutyID ";
		//echo $sql2;
		$rs2 = db_query($sql2);
	}
	$dayData=array();
	$timeStart=array();
	$timeEnd=array();
	$day=array();
	if($type=="0"){//week
		$evtlst = array();
		if(!db_eof($rs2)){
			while($r2 = db_fetch_array($rs2)){
				$day[]=$r2['week_day'];
				if(!is_array($timeStart[$r2['week_day']])){
					$timeStart[$r2['week_day']]=array();
				}
				if(!is_array($timeEnd[$r2['week_day']])){
					$timeEnd[$r2['week_day']]=array();
				}
				array_push($timeStart[$r2['week_day']],$r2['startTime']);
				array_push($timeEnd[$r2['week_day']],$r2['endTime']);
			}
			foreach ($timeStart as $k=>$v){//記算每天有幾個時段
				$dayData[$k]=count($timeStart[$k]);
			}
			$dayData=json_encode($dayData);
			$timeStart=json_encode($timeStart);
			$timeEnd=json_encode($timeEnd);
		}
	}else if($type=="1"){//特殊班表
		$evtlst = array();
		if(!db_eof($rs2)){
			while($r2=db_fetch_array($rs2)) {
				 $bD = date('Y/m/d H:i',strtotime($r2[startTime]));
				 $short_bD=date('Y/m/d',strtotime($r2[startTime]));
				 if($r2["empID"]!=""){	$bgColor="#18F";	}else if($r2["jobID"]!=""){	$bgColor="#00A800";	}else{	$bgColor="#F90";}
				 //echo 'dtype:'.$r2[dType]."<br>";
				 if($r2[dType]=="0"){//補班
				 	$eD = date('Y/m/d H:i',strtotime($r2[addDate]));
				 	$short_eD=date('Y/m/d',strtotime($r2[addDate]));
					$evtlst[] = "{id:'$r2[id]',title:'放假日期：".$short_eD."',start:'$bD',end:'$bD',allDay:false,backgroundColor:'$bgColor',  borderColor :'$bgColor',dType:'$r2[dType]',addDate:'".$r2[addDate]."'}";
				 }else if($r2[dType]=="1"){//休假
				 	$eD = date('Y/m/d 23:59:59',strtotime($r2[endTime]));
				 	$short_eD=date('Y/m/d',strtotime($r2[endTime]));
				 	$evtlst[] = "{id:'$r2[id]',title:'$short_bD~$short_eD',start:'$bD',end:'$eD',allDay:false,backgroundColor:'$bgColor',  borderColor :'$bgColor',dType:'$r2[dType]'}";
				 }
				 
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Details</title>
  <link rel="stylesheet" href="<?=$extfiles?>edit.css">
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link rel='stylesheet' type='text/css' href='/scripts/fullcalendar/fullcalendar.css' />
  <link rel='stylesheet' type='text/css' href='/scripts/jquery-ui-timepicker-addon.css' />
  
  <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
.title{	text-align:center;	background-color:#E1E1E1;}
.sTable{background:#fff;	margin:10px;}
.colLabel{	width:60px;}

table.grid{  margin:5px;	border: 1px solid #ccc; border-collapse: collapse; }
table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; text-align:center;}

.divBorder2 { /* 內容區 第二層外框 */
	padding:8px; 
	border:1px solid #CCC;
	background:#FFFFFF;
	font-size:12px;
}
.delBtn{	cursor:pointer;height:28px;}
.divTitle{
	color:#fff;
	background-color:#900;
	border: 0px;
	padding: 5px;
	margin: 0px;
	font-size:12px;
}
#d_setting_time{	
	position:fixed;
	left:20%;
	/*margin-left:-200px;*/
	top:20px;
	width:400px;
	display:none;
	padding: 8px;
    background: #ffffff;
    border-radius: 4px;
    box-shadow: black;
    box-shadow: 1px 1px 5px 1px #999999;
    cursor: pointer;

}
#tb_setting_time{	width:98%;}
.dBottom{	background:#dedede; text-align:center;padding:8px;border:1px solid #ccc;}

.nonTip{	color:#C00;}
#d_setting_date{	display:none;position:fixed;top:50px;left:300px;	z-index:1000; }
.fc-event{    cursor: pointer; }
.color_tip{	width:18px; height:18px;float:left;}
.text_tip{	float:left;}
#calendar{	background-color:#fff;border:1px solid #666;margin:8px;padding:5px; display:none;}
#color_box{	margin:8px; margin-bottom:1px;}
#img_add{	cursor:pointer;}
.fc-event-time{   display : none;}
#ui-datepicker-div{	z-index:9999;}
.del{	background:#900;color:#fff;float:left; }
#btn_apply{	background:#060;color:#fff;}
.btn{cursor:pointer;}
.optbtn {
    width: 17px;
    height: 17px;
    vertical-align: middle;
}
.w-title{	background:#000040;	color:#ffffff; }
</style>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src='/scripts/fullcalendar/fullcalendar.min.js'></script>
 
<script type="text/javascript">
	var day;			//目前正在編輯哪一天
	var type='<?=$type?>';
	var dayData;	//記錄有哪幾天有開放時段
	var timeStart;	//記錄開始時間
	var timeEnd;
	var bCancel=false;	
	var dType='<?=$dType?>';//特殊班表型態 0:補班,1:休假
  $(function() {
	  	if(type=="0"){//week
			dayData=<?=$dayData?>;
			timeStart=<?=$timeStart?>;
			timeEnd=<?=$timeEnd?>;
			showList();
			$('#d_setting_time').draggable();
		}else if(type=="1"){//特殊班表	
			$('#d_setting_date').draggable();	
			$('#calendar').show();	
			$('#calendar').fullCalendar({
				header: {left:'prev,next today', center:'title', right:'month,agendaWeek,agendaDay'},
				editable: false,
				buttonText : { today: '今日',month: '月',agendaWeek: '周',	agendaDay: '日'	},
				monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
				monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
				dayNames : ['周日','週一','週二','週三','週四','週五','週六'],
				dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],
				height: 500,
				eventClick: function(event) {
					if(bCancel) {
						bCancel=false;
						return;
					}
					$('#btn_del').show();
					$('#d_setting_date').show();
					var obj=event;
					$('#m_id').val(obj['id']);
					var date = obj['start'];
					var y=date.getFullYear();
					var M=(date.getMonth()+1);
					var d=date.getDate();
					if(M<10){	M="0"+M}
					if(d<10){	d="0"+d;}
					$('#m_startTime').val(y+"/"+M+"/"+d);
					$('#m_startTime').datepicker();
					//console.log('dType:'+obj['dType']);
					if(dType=='0'){//補班
						date = new Date(obj['addDate']);
					}else if(dType=='1'){//休假
						date = obj['end'];
					}
					y=date.getFullYear();
					M=(date.getMonth()+1);
					d=date.getDate();
					if(M<10){	M="0"+M}
					if(d<10){	d="0"+d;}
					if(dType=='0'){//補班
						$('#m_addDate').val(y+"/"+M+"/"+d);
						$('#m_addDate').datepicker();
					}else if(dType=='1'){//休假
						$('#m_endTime').val(y+"/"+M+"/"+d);
						$('#m_endTime').datepicker();
					}
				},
				eventRender: function(event, element) {
					var obj=event;
					element.find(".fc-event-title").after($("<span class=\"fc-event-icons\"></span>").html("&nbsp;&nbsp;<img class='delBtn' src='../images/del.png' align='absmiddle' onclick='delMonth(\""+obj['id']+"\")'/>"));
				},
				events: [ <? echo join(',',$evtlst);?>
				]
			});
		}
	});
	function delDay(id,day){
		bCancel=true;
		if(!confirm("確定刪除班表嗎？")){
			return ;
		}
		$.ajax({ 
		  type: "POST", 
		  url: "doDays.php", 
		  data: "act=del&id="+id+"&delDays="+day
		}).done(function( rs ) {
			location.reload();
		});
	}
	//--------------------------------特殊班表-----------------------------
	function delMonth(id){
		bCancel=true;	
		if(!confirm("確定刪除班表嗎？")){
			return ;
		}
		$.ajax({ 
		  type: "POST", 
		  url: "doMonth.php", 
		  data: "act=del&id="+id
		}).done(function( rs ) {
			location.reload();
		});
	}
	function addDate(){
		console.log("@in");
		$('#m_id').val("");	
		$('#btn_del').hide();
		$('#d_setting_date').show();
		var date = new Date();
		var y=date.getFullYear();
		var M=date.getMonth()+1;
		var d=date.getDate()+1;
		if(M<10){	M="0"+M;}
		if(d<10){	d="0"+d;}
		$('#m_startTime').val(y+"/"+M+"/"+d);
		$('#m_startTime').datepicker();
		console.log('dType:'+dType);
		if(dType=="0"){//補班
			$('#m_addDate').val(y+"/"+M+"/"+d);
			$('#m_addDate').datepicker();
		}else if(dType=='1'){//休假
			$('#m_endTime').val(y+"/"+M+"/"+d);
			$('#m_endTime').datepicker();
		}
	}
	function doMonth(id){
		if(!confirm("確定刪除班表嗎？"))	return;
		$.ajax({ 
			  type: "POST", 
			  url: "doMonth.php", 
			  data: "act=del&id="+id
			}).done(function( rs ) {
				location.reload();
			});
	}
	function saveDate(){
		var act;
		var now=Date.now();
		var startTime=$('#m_startTime').val();
		var endTime=$('#m_endTime').val();
		var addDate=$('#m_addDate').val();
		if(dType=='1'){//放假
			if(startTime==""){	alert('請選擇開始日期');}
			if(endTime==""){	alert('請選擇結束日期');}
			if(Date.parse(startTime)>Date.parse(endTime)){	alert('開始日期或結束日期錯誤');return;}
			if(now>Date.parse(startTime)){alert('開始日期已過期，無法設定');return;}
			if(now>Date.parse(endTime)){alert('結束日期已過期，無法設定');return;}
		}else if(dType=='0'){//補班
			if(startTime==""){	alert('請選擇上班日期');}
			if(addDate==""){	alert('請選擇補班日期');}
			//if(Date.parse(startTime)>=Date.parse(addDate)){	alert('上班日期或補班日期錯誤');return;}
			if(now>Date.parse(startTime)){alert('日期已過期，無法設定');return;}
		}
		if($('#m_id').val()==""){	act="add";}else{	act="upd";}	
		$.ajax({ 
		  type: "POST", 
		  url: "doMonth.php", 
		  data: "act="+act+"&id="+$('#m_id').val()+"&startTime="+startTime+'&endTime='+endTime+"&dutyID='<?=$id?>'&dType="+dType+"&addDate="+addDate
		}).done(function( rs ) {
			location.reload();
		});
	}
	//--------------------------------week-----------------------------
	function showList(){//讓使用者更好識別
		var objs=document.getElementsByName('week_day');
			
		var str=[];
		var tStr;
		var d;
		var word=['日','一','二','三','四','五','六'];
		for(var i=0;i<objs.length;i++){
			if(objs[i].checked){
				tStr=[];
				d=objs[i].value;
				if(timeStart[d]!=undefined){	
					for(var k in timeStart[d]){	tStr.push(timeStart[d][k]+" ~ "+timeEnd[d][k]);	}
				}else{
					tStr.push("<span class='nonTip'>時段尚未設定</span>");
				}
				str.push("週"+word[d]+"："+tStr.join(" , "));
			}
		}
		$('#td_list').html(str.join("<br>"));
	}
	function formvalid(tfm) {
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	function settingTime(d,obj){//設定時段
		if(timeStart[d]==undefined){	dayData[d]=undefined;}
		day=d;
		var word=['日','一','二','三','四','五','六'];
		var str="<table class='grid' id='tb_setting_time'>";
		str+="<tr><td colspan='3'><input type='button' value='新增時段' onclick='addItem()' style='float:left'/></td></tr>";
		str+="<tr><td >開放時間</td><td>結束時間</td><td>刪除</td></tr>";
		str+="</table>";
		$("#d_container").html(str);
		var start,end;
		if(dayData[day]==undefined){
			addItem();
		}else{//秀出已設好的時段
			for(var i in timeStart[day]){ 
				start=timeStart[day][i].split(":");
				end=timeEnd[day][i].split(":");
				newItem(i,start[0],start[1],end[0],end[1]);
			}
		}
		
		$('#d_setting_time').show();
		$('#dTitle').html("週"+word[d]);
	}
	function newItem(index,start_h,start_m,end_h,end_m){
		var tb=document.getElementById('tb_setting_time');
		var tbObj=tb.tBodies[0];
		var newRow=document.createElement('tr');
		var newcell1=document.createElement('td');
		var newcell2=document.createElement('td');
		var newcell3=document.createElement('td');
		newRow.style.height='30px';
		newRow.bgColor='#E3E3E3'; 
		var str="<select class='input' id='sel_start_h_"+index+"'/>"; 
		str+="<option selected></option>";
		var selected='';
		for(var j=0;j<=23;j++){
			if(start_h==j){	selected='selected';}else{selected='';}
			str+="<option "+selected+">"+j+"</option>";
		}
		str+="</select>&nbsp;時&nbsp;";
		str+="<select class='input' id='sel_start_m_"+index+"'/>"; 
		str+="<option selected></option>";
		
		var  k='';
		for(var j=0;j<60;j+=10){
			if(start_m==j){	selected='selected';}else{selected='';}
			if(j==0){	k="00";}else{	k=j;}
			str+="<option "+selected+">"+k+"</option>";
		}
		str+="</select>&nbsp;分&nbsp;";
		newcell1.align='center';
		newcell1.innerHTML=str
		str="<select class='input' id='sel_end_h_"+index+"'/>"; 
		str+="<option selected></option>";
		for(var j=0;j<=23;j++){
			if(end_h==j){	selected='selected';}else{selected='';}
			str+="<option "+selected+">"+j+"</option>";
		}
		str+="</select>&nbsp;時&nbsp;";
		str+="<select class='input' id='sel_end_m_"+index+"'/>"; 
		str+="<option selected></option>";
		for(var j=0;j<60;j+=10){
			if(end_m==j){	selected='selected';}else{selected='';}
			if(j==0){	k="00";}else{	k=j;}
			str+="<option "+selected+">"+k+"</option>";
		}
		str+="</select>&nbsp;分&nbsp;";
		newcell2.align='center';
		newcell2.innerHTML=str;
		newcell3.align='center'; 
		newcell3.innerHTML='<img src="/images/del.png" class="delBtn" onClick="delItem(this,event)">'; 
		newRow.appendChild(newcell1);
		newRow.appendChild(newcell2);
		newRow.appendChild(newcell3);
		tbObj.appendChild(newRow);
	}
	function addItem(){//新增開放時段
		var index=0;
		if(dayData[day]){
			index=parseInt(dayData[day])+1;
			dayData[day]=index;
		}else{
			dayData[day]=index.toString();
		}
		newItem(index,"9","","12","");
	}
	function delItem(obj,e){//刪除開放時段
		var evt = e || window.event;
		var current = evt.target || evt.srcElement;
		var currRowIndex=current.parentNode.parentNode.rowIndex; 
		var tb = document.getElementById("tb_setting_time");
		if (tb.rows.length > 0) {  tb.deleteRow (currRowIndex);}
		if(timeStart[day]!=undefined) {timeStart[day].splice(currRowIndex, 1);}
		if(timeEnd[day]!=undefined) {timeEnd[day].splice(currRowIndex, 1);}
		if(dayData[day]>0){	
			dayData[day]=parseInt(dayData[day])-1;
		}else if(dayData[day]==0){	dayData[day]=undefined;}

	}
	
	function doCancel(id){
		if(type=="0"){
			if(timeStart[day]==undefined){	dayData[day]=undefined;}
		}
		$('#'+id).hide();
	}
	function saveTime(){
		var bSave=true;
		if(dayData[day]!=undefined){
			if(typeof(timeStart)!="object"){	timeStart=[];	}
			if(typeof(timeEnd)!="object"){	timeEnd=[];	}
			timeStart[day]=[];
			timeEnd[day]=[];
		}
		var start_h,start_m,end_h,end_m;
		for(var i=0;i<=dayData[day];i++){
			if($('#sel_start_h_'+i).val()==undefined)	continue;
			if($('#sel_start_h_'+i).val()==""){alert('請選擇幾點開放');	bSave=false;break;}
			if($('#sel_end_h_'+i).val()==""){alert('請選擇幾點結束');	bSave=false;break;}
			start_h=parseInt($('#sel_start_h_'+i).val());
			if(start_h<10){ start_h="0"+start_h;}
			start_m=parseInt($('#sel_start_m_'+i).val());
			if(start_m<10){ start_m="0"+start_m;}
			end_h=parseInt($('#sel_end_h_'+i).val());
			if(end_h<10){ end_h="0"+end_h;}
			end_m=parseInt($('#sel_end_m_'+i).val());
			if(end_m<10){ end_m="0"+end_m;}
			if(start_h==end_h && start_m>=end_m){	alert('開始時間與結束時間有誤，請重新選擇');bSave=false;	break;}
			timeStart[day][i]=start_h+":"+start_m;
			timeEnd[day][i]=end_h+":"+end_m;
		}
		if(bSave){	$('#d_setting_time').hide();}
		showList();
	}
	function formvalid(tfm) { 
		//::檢查是否有設定時段
		var objs=document.getElementsByName('week_day');	
		var day;
		var dData=[];
		var tData=[];
		var str;
		var bTime=true;
		for(var i=0;i<objs.length;i++){
			if(objs[i].checked){	
				day=objs[i].value 
				str=[];
				if(dayData[day]){
					for(var k in timeStart[day]){
						str.push(timeStart[day][k]+","+timeEnd[day][k]);
					}
					dData.push(day);
					tData.push(str.join(";"));
				}else{
					bTime=false;break;
				}
			}
		}
		if(!bTime){	alert('開放時段未設定');return false;}
		$('#week_day_v').val(dData.join(","));
		$('#tData').val(tData.join("#"));
		return true;
	}
	
</script>
</head>

<body bgcolor="#eeeeee">
<?
if($type=="0"){//-------------------------- 週班表 -------------------------- 
	
?>
<form method="post" action="doWeek.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
	<table align="center" class="sTable" width="98%" border="0" CellSpacing="0" CellPadding="4">
		<tr><td colspan='2' class='w-title'>開始設定時段</tr>
        <tr>
            <td class="colContent">
            	<input type='checkbox' name='week_day' onclick='showList()' value='1' class='input optbtn' <? if (in_array('1',$day)) $ck='checked'; else $ck=''; echo $ck;?>>週一<input value="設定" type="button" onClick="settingTime(1,this)" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='week_day' onclick='showList()' value='2' class='input optbtn' <? if (in_array('2',$day)) $ck='checked'; else $ck=''; echo $ck;?>>週二<input value="設定" type="button" onClick="settingTime(2,this)" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='week_day' onclick='showList()' value='3' class='input optbtn' <? if (in_array('3',$day)) $ck='checked'; else $ck=''; echo $ck;?>>週三<input value="設定" type="button" onClick="settingTime(3,this)" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='week_day' onclick='showList()' value='4' class='input optbtn' <? if (in_array('4',$day)) $ck='checked'; else $ck=''; echo $ck;?>>週四<input value="設定" type="button" onClick="settingTime(4,this)" /><p>
                <input type='checkbox' name='week_day' onclick='showList()' value='5' class='input optbtn' <? if (in_array('5',$day)) $ck='checked'; else $ck=''; echo $ck;?>>週五<input value="設定" type="button" onClick="settingTime(5,this)" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='week_day' onclick='showList()' value='6' class='input optbtn' <? if (in_array('6',$day)) $ck='checked'; else $ck=''; echo $ck;?>>週六<input value="設定" type="button" onClick="settingTime(6,this)" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='week_day' onclick='showList()' value='0' class='input optbtn' <? if (in_array('0',$day)) $ck='checked'; else $ck=''; echo $ck;?>>週日<input value="設定" type="button" onClick="settingTime(0,this)" />
        </tr>
        <tr><td colspan='2' class='title'>目前班表</tr>
        <tr>
    		<td id='td_list'><span class='nonTip'>尚未設定</span></td>
        </tr>
        <tr>
            <td colspan="2" align="center" class="rowSubmit">
            <input type="hidden" name="selid" value="<?=$id?>">
            <input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
            <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
            <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
            <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
            <input type="hidden" name='week_day_v' id='week_day_v'>
            <input type="hidden" name='tData' id='tData'>
            <input type="submit" value="確定變更" class="btn">&nbsp;
          </td>
        </tr>
    </table>
</form>

<div id='d_setting_time'>
    <div id='dTitle' class='divTitle'></div>
    <div class='divBorder2' id='d_container'></div>
    <div class='dBottom'><input type="button" value='確定' onclick='saveTime()' class="btn"/>&nbsp;<input type="button" value='關閉' onclick='doCancel("d_setting_time")' class="btn"/></div>
</div>

<?
}else if($type=="1"){//-------------------------- 特殊班表	-------------------------- 
	if($dType=='0'){//補班	?>
	<div id='d_setting_date'>
	    <div id='dTitle' class='divTitle'>設定日期</div>
	    <div class='divBorder2' style='width:300px;'>
	    	<table class='grid' style='width:98%'>
	    		<tr><td width="100">選擇上班日期<br>(上班日)</td><input type='hidden' id='m_id'/>
	            	<td ><input type="text" name="m_startTime" id="m_startTime" class='input'/></td>
	            </tr>
	            <tr><td width="100">選擇補假日期<br>(放假日)</td><input type='hidden' id='m_id'/>
	            	<td ><input type="text" name="m_addDate" id="m_addDate" class='input'/></td>
	            </tr>
	    	</table>
	    </div>
	    <div class='dBottom'><input type="button" value='確定' onclick='saveDate()' class="btn"/>&nbsp;<input type="button" value='關閉' class="btn" onclick='doCancel("d_setting_date")'/></div>
	</div>
<?	}else if($dType=='1'){//放假
?>
	<div id='d_setting_date'>
	    <div id='dTitle' class='divTitle'>設定放假日期</div>
	    <div class='divBorder2' style='width:300px;'>
	    	<table class='grid' style='width:98%'>
	    		<tr><td width="100">開始日期</td><input type='hidden' id='m_id'/>
	            	<td ><input type="text" name="m_startTime" id="m_startTime" class='input'/></td>
	            </tr>
	    		<tr><td>結束日期</td>
	            	<td ><input type="text" name="m_endTime" id="m_endTime" class='input'/></td>
	            </tr>
	    	</table>
	    </div>
	    <div class='dBottom'><input type="button" value='確定' onclick='saveDate()' class="btn"/>&nbsp;<input type="button" value='關閉' class="btn" onclick='doCancel("d_setting_date")'/></div>
	</div>
<?
	}
?>
<div id='color_box'>
    <span class='color_tip' style='background:#18F'></span><span class='text_tip'>員工班表&nbsp;&nbsp;&nbsp;&nbsp;</span><span class='color_tip' style='background:#00A800'></span><span class='text_tip'>職掌班表&nbsp;&nbsp;&nbsp;&nbsp;</span><span class='color_tip' style="background:#F90"></span><span class='text_tip'>部門班表&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<img id='img_add' src="../images/add.png" onclick="addDate()" style="width:25px;height:25px;" align="absmiddle"/>&nbsp;新增班表
</div>
<?
}
?>
<div id='calendar'></div>
</body>
</html>