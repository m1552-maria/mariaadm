<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <style type="text/css">
 .optbtn { width: 17px; height: 17px; vertical-align: middle; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script Language="JavaScript" src="list.js"></script> 
 <title><?=$pageTitle?></title>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	var seluType;
	var nextId ;	//上/下一筆Id
	var uType="<?=$_REQUEST[uType]?>";
	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
			case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
			case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
			case 3 : pp=<?=$pageCount?>;
		}
		var url="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(uType) url +='&uType='+uType;
		location.href=url;
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else{ 		
			seluType=uType;
			if(uType=="2"){//輪班表
				window.open('edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>&uType="+uType);
				//window.parent.location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>&uType="+uType; 
			}else{
				window.parent.topFrame.location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
				window.parent.mainFrame.location.href='details.php';
			}
		}
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	

	function doAppend(){
		if('<?=$_GET[uType]?>'=="2"){//月班表
			window.open('append.php?uType=<?=$_GET[uType]?>'); 
		}else{
			window.parent.location.href='append.php?uType=<?=$_GET[uType]?>'; 
		}
	}
	function setClose(visitId){
		location.href="booking_close.php?visitId="+visitId;
	}

	function nextTr(act){//處理上下筆資料
		if(act=="prev"){
			nextId= $('#tr_'+selid+'_'+seluType).prev("tr").attr("id");
		}else if(act=="next"){
			nextId= $('#tr_'+selid+'_'+seluType).next("tr").attr("id");
		}
		if(nextId==undefined || nextId==""){//沒有上/下一筆，提示換頁或do Nothing
			if(act=="prev"){	
				if($('#btnPrev').attr("class")=="sBtn"){
					alert('請切換至上一頁');
				}else{
					alert('沒有上一筆資料了');	
				}
			}else{
				if($('#btnNext').attr("class")=="sBtn"){
					alert('請切換至下一頁');
				}else{
					alert('沒有下一筆資料了');
				}		
			}
		}else{//有下一筆
			var ary=nextId.split("_");
			SelRow(ary[1],$('#'+nextId)[0],ary[2],ary[3]);
			window.open('edit.php?ID='+ary[1]+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>&uType="+ary[2]);
		}
	}
 </script>
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="95%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)" id='btnPrev'><input type="button" value="下一頁" id='btnNext' class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='doAppend()'><!--<input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()">--><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { 
			if ($_GET[uType]!='1' && $fnAry[$i]=="dType") continue;
	?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
			<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?><? } ?>
		</a>
	</th>
	<? } ?>
	<? 	if($_REQUEST['uType']=="2") echo "<th>下個月班表</th>" ?>
  </tr>
<?
	//取的下個月有排班的id列表
	$rs = db_query("select dutyID from duty_days2");
	$aa = array();
	while($r=db_fetch_array($rs)) $aa[$r[0]]=1;
	
	//過濾條件
	if(isset($filter)) $sql="select * from $tableName where ($filter)"; else $sql="select * from $tableName where uType=".$_GET[uType]." ";
  // 搜尋處理
  if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')) ";
  if ($_SESSION['privilege'] <=10)  {
		$sql.=" and d.dutyID=dm.dutyID ";
		if(count($dutyAry)==0){
			$sql.="and d.dutyID = ''";
		}else{
			$sql.=" and d.dutyID in (".join(",",$dutyAry).") ";
		}
		$sql.=" group by d.dutyID ";
	}
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";

	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize"; 

  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { 
		$id = $r[$fnAry[0]]; 
?>
  <tr valign="top" id="tr_<?=$id?>_<?=$r[uType]?>_<?=$r[dType]?>" style='cursor:pointer;' onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this,'<?=$r['uType']?>','<?=$r[dType]?>')"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" class='optbtn' value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
			<? if($_REQUEST[uType]=='2'){ //月班表	?>
					<a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a>
				<? } else {	?>
					<?=$id?>
				<? } ?>
    </td>

		<? for($i=1; $i<count($fnAry); $i++) { 
			$fldValue = $r[$fnAry[$i]];
			if ($_GET[uType]!='1' && $fnAry[$i]=="dType" ) continue;
		?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? 
				switch ($ftyAy[$i]) {
					case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
					case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
					case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
					case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
					case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
					case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
					case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
					default :
						if ($fnAry[$i]=="uType"){	
							echo $uType[$fldValue];	
						}else if($fnAry[$i]=="dType"){
							echo $specialType[$fldValue];	
						}else{	
							echo $fldValue?$fldValue:"&nbsp;";	}
						break;
				}
			?>
		</td>
		<? } ?>	
		<? 	if($_REQUEST['uType']=="2") { ?>		
			<td width="50"><img src="<?= $aa[$r['dutyID']]?'checked.gif':'uncheck.gif' ?>"/></td>
		<? } ?>
  </tr>    
<? } ?>   
 </table>
 <input type="hidden" name='uType' value='<?=$_GET[uType]?>' />
</form>
<div align="right"><a href="#top">Top ↑</a></div>
</body>
</Html>