<?	
	@session_start();
  header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../getEmplyeeInfo.php');	
	include('../../Utilities/authority.php');
	$bE = true;	//異動權限
	$pageTitle = "員工排班作業";

  if ($_SESSION['privilege'] <=10) {//前台
		$tableName = "dutys as d , duty_map as dm ";
	}else{
		$tableName = "dutys as d  ";
	}
	// $tableName.="on d.dutyID=dm.dutyID ";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
	//$filter="uType=".$_GET['uType'];
			
	// Here for List.asp ======================================= //
	$defaultOrder = "d.dutyID";
	$searchField1 = "d.title";
	$searchField2 = "d.dutyID";
	$pageSize = 15;	//每頁的清單數量
	// 注意 primary key should be first one
	$fnAry = explode(',',"dutyID,title,dType,uType,notes,createMan,rDate");
	$ftAry = explode(',',"編號,班表名稱,類別,班表方式,說明,建立者,建立日期");
	$flAry = explode(',',"60,200,,60,300,,");
	$ftyAy = explode(',',"dutyID,text,text,text,text,text,text");
	
	$uType=array("0"=>'週班表','2'=>'輪班班表','1'=>'特殊班表');
	$specialType=array("0"=>'補班','1'=>'放假');
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,uType,notes");
	$newftA = explode(',',"班表名稱,班表方式,說明");
	$newflA = explode(',',"55,,40");
	$newfhA = explode(',',",,3");
	$newfvA = array('',$uType,'','');
	$newetA = explode(',',"text,select,textarea");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"dutyID,title,uType,notes");
	$editftA = explode(',',"編號,班表名稱,班表方式,說明");
	$editflA = explode(',',",55,,40");
	$editfhA = explode(',',",,,3");
	$editfvA = array('','',$uType,'','');
	$editetA = explode(',',"text,text,label,textarea");
?>