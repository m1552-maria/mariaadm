<?
  if($_REQUEST['sql']) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");

    include '../../config.php';
    include('../../getEmplyeeInfo.php');  
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    
    $sql = $_REQUEST[sql];
    $rs = db_query($sql);

    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
    $objPHPExcel->getActiveSheet()->getStyle('A4:K4')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:K1");
    $objPHPExcel->getActiveSheet()->mergeCells("A2:B2");
    $objPHPExcel->getActiveSheet()->mergeCells("A3:B3");
    $objPHPExcel->getActiveSheet()->mergeCells("C2:K2");
    $objPHPExcel->getActiveSheet()->mergeCells("C3:K3");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    
    $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:K2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A3:K3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A4:K4')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    /*
    VERTICAL_CENTER 垂直置中
    VERTICAL_TOP
    HORIZONTAL_CENTER
    HORIZONTAL_RIGHT
    HORIZONTAL_LEFT
    HORIZONTAL_JUSTIFY
    */
    $objPHPExcel->getActiveSheet()->setCellValue('A1',$_REQUEST[fmTitle].' - '.($_REQUEST[depID]?$departmentinfo[$_REQUEST[depID]]:'全部部門').' - '.($_REQUEST[empID]?$emplyeeinfo[$_REQUEST[empID]]:'全部員工'));
    $objPHPExcel->getActiveSheet()->setCellValue('A2','製作日期');
    $objPHPExcel->getActiveSheet()->setCellValue('C2',date("Y/m/d"));
    $objPHPExcel->getActiveSheet()->setCellValue('A3','報表日期');
    $objPHPExcel->getActiveSheet()->setCellValue('C3',$_REQUEST['year'].'年'.$_REQUEST['month'].'月');
     

    //跑header 迴圈用
    $headerstart = 4;
    $header = array('A','B','C','D','E','F','G','H','I','J','K');
    $headervalue = array('序號','月份','部門','姓名','狀態','時數','上班(排班)','下班(排班)','上班(打卡)','下班(打卡)','說明');
    
    foreach($header as $k=>$v){
      $objPHPExcel->getActiveSheet()->setCellValue($v.$headerstart,$headervalue[$k]);   
    }

 
    $i = 5;
    $curEmp = '';
    $curTyp = '';
    $count  = 1;
    $color = 0;
    
    while($rs && $r=db_fetch_array($rs)) {
      $eid = $r[empID];

      if($curEmp!=$eid) {
        $curEmp = $eid;
        $curTyp = '';
        $color=!$color;
      }

      $content = array($count,$_REQUEST['month'],$departmentinfo[$r[depID]],$r[empName],$r[abStatus],$r[abHours],$r[startTime],$r[endTime],$r[startTime2]?$r[startTime2]:'未打卡',$r[endTime2]?$r[endTime2]:'未打卡',$r[aNotes]);
      foreach($header as $k=>$v){
        
        $objPHPExcel->getActiveSheet()->setCellValue($v.$i,$content[$k]);
      }
      
      if($color != 0){
        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':K'.$i)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'efefef')
                )
            )
        );
      }

    
      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':K'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
      $count++;
    }
   
   
  
    
    //設定欄寬
    $width = array(20,20,20,20,20,20,20,20,20,20,20);
    foreach($header as $k=>$v){
      $objPHPExcel->getActiveSheet()->getColumnDimension($v)->setWidth($width[$k]);  
    }


    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->save('php://output');




  }


  
?>