<?php
//$logtx = "---LOG Begin--- \n";	//LOG
$curYr = $_REQUEST['year']?$_REQUEST['year']:$curYr=date('Y');
$curMh = $_REQUEST['month']?$_REQUEST['month']:$curMh=date('m');
$curMh=str_pad($curMh, 2, "0", STR_PAD_LEFT);  

//關帳檢查
$fCheck = checkAcctClose('abnormal',$curYr.$curMh, $_SESSION["Account"]);
if($fCheck) {
	echo "<script>alert('本作業已關閉，無法執行!'); history.back();</script>";
	exit;
}

//鎖定作業
$LOCKNAME = 'proAbnormal';
$rs = db_query("select GET_LOCK('$LOCKNAME',10)");
$r = db_fetch_array($rs);
$rzt = false;
if($r[0]==1) {
	//董事長、執行長、園長、副園長、主任、副主任，不需要出異常
	$noDutyJobId=array("G001","G002","G003","G004","G005","G006");
	$table='emplyee_duty_abnormal';
	//檢查是否已做過出缺勤檢查
	$sql="select id from emplyee_duty_abnormal where abMonth='".$curYr.$curMh."' ";
	$rs=db_query($sql);
	$rowsCount=db_num_rows($rs);
	if($rowsCount>0) $table='emplyee_duty_abnormal_x';
	
	//找出需要出缺勤的人 e.isOnduty=1 and e.jobID not in ('G001','G002','G003','G004','G005','G006') and e.noSignIn=0 
	$sql="select * from emplyee as e left join department as d on e.depID=d.depID ";
	$sql.="where e.isOnduty=1 and e.jobID not in ('".join("','",$noDutyJobId)."') ";
	$sql.="and e.noSignIn=0 ";
	if(isset($_REQUEST['depFilter']) && $_REQUEST['depFilter']!=""){
		$sql.="and e.depID like '".$_REQUEST['depFilter']."%' ";
	}
	if(isset($_REQUEST['empFilter']) && $_REQUEST['empFilter']!=""){
		$sql.="and e.empID='".$_REQUEST['empFilter']."' ";
	}
	//$logtx.="$sql \n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			//$logtx.="$r['empID'] : \n";
			do_duty_abnormal($r['empID'],$curYr,$curMh,$table);		//核對打卡、加班記錄
		}
		
		if($table=="emplyee_duty_abnormal_x"){
			rewrite_abnormal($curYr,$curMh,$_REQUEST['depFilter'],$_REQUEST['empFilter']);
		}
	}
	//解除鎖定
	db_query("select RELEASE_LOCK('$LOCKNAME')");
} else $rzt='目前有其他人作業中，請稍候再執行...';

function rewrite_abnormal($nowY,$nowM,$depFilter,$empFilter){
	$abMonth=$nowY.$nowM;
	$sql="SELECT ab.*,duty.*,ab.notes as notes,ab.id as abId 
		FROM	emplyee_duty_abnormal as ab
		LEFT JOIN emplyee as e
		ON ab.empID=e.empID 
		LEFT JOIN emplyee_duty as duty
		ON ab.fid=duty.id 
		WHERE ab.abMonth='".$abMonth."' ";
  if(isset($depFilter) && $depFilter!="") $sql.="and e.depID like '".$depFilter."%' ";
  if(isset($empFilter) && $empFilter!="")	$sql.="and e.empID='".$empFilter."' ";
	$abIdAry=array();
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			//將原本abnormal的notes寫至abnormal_x
			$sql="UPDATE emplyee_duty_abnormal_x SET notes='".$r['notes']."',normal='".$r['normal']."' WHERE fid='".$r['fid']."' and abStatus='".$r['abStatus']."' ";
			db_query($sql);
			array_push($abIdAry,$r['abId']);
			//$n = mysql_affected_rows();	if($n==1) array_push($abIdAry,$r[abId]); 需要清掉沒註計的
		}
	}
	
	//把當月abnormal資料刪掉
	$sql = "DELETE FROM emplyee_duty_abnormal WHERE id in (".join(",",$abIdAry).") "; db_query($sql);
	
  //把abnormal_x寫回abnormal
	$sql="INSERT INTO emplyee_duty_abnormal	SELECT * FROM emplyee_duty_abnormal_x";	db_query($sql);
	
	//清空暫存表
	$sql="DELETE FROM emplyee_duty_abnormal_x";	db_query($sql);
}

function do_duty_abnormal($empID,$nowY,$nowM,$table){
	global $logtx;
	$abMonth=$nowY.$nowM;
	$today=date('Y-m-d');
	//::取得外出資料
	$sql="select *,o.empID as empID,o.id as id ";
	$sql.="from emplyee as e ";
	$sql.="left join outoffice as o on e.empID=o.empID ";
	$sql.="where YEAR(o.bDate)=".$nowY." and MONTH(o.bDate) = ".$nowM." and o.bDate<'".$today."' ";
	$sql.="and e.empID='".$empID."' and is_leadmen=1 group by o.id";
	$rs=db_query($sql);
	$outoffice_data=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if(!$outoffice_data[$r['id']])	$outoffice_data[$r['id']]=array();
			$outoffice_data[$r['id']]['empID']=$r['empID'];
			$outoffice_data[$r['id']]['bDate']=$r['bDate'];
			$outoffice_data[$r['id']]['eDate']=$r['eDate'];
			$outoffice_data[$r['id']]['rtnDate']=$r['rtnDate'];
			$outoffice_data[$r['id']]['offwork']=$r['offwork'];
		}
		//$logtx.="取得外出資料\n";
	}

	//::取得請假資料
	$sql="select *,a.aType as aType,a.empID as empID,a.id as id ";
	$sql.="from emplyee as e ";
	$sql.="left join absence as a on e.empID=a.empID ";
	$sql.="where a.isOK=1 and a.isDel=0 ";
	$sql.="and YEAR(a.bDate)=".$nowY." and MONTH(a.bDate) = ".$nowM." and a.bDate<'".$today."' ";
	$sql.="and e.empID='".$empID."' group by a.id";
	$rs=db_query($sql);
	$absence_data=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if(!$absence_data[$r['id']])	$absence_data[$r['id']]=array();
			$absence_data[$r['id']]['empID']=$r['empID'];
			$absence_data[$r['id']]['aType']=$r['aType'];
			$absence_data[$r['id']]['bDate']=$r['bDate'];
			$absence_data[$r['id']]['eDate']=$r['eDate'];
		}
		//$logtx.="$empID : 取得請假資料\n";
	}
	
	//::打卡資料 - 主回圈
	$sql="select * from emplyee_duty where empID='".$empID."' ";
	$sql.="and ((YEAR(startTime)=".$nowY." and MONTH(startTime) = ".$nowM.") ";
	$sql.="or (YEAR(endTime)=".$nowY." and MONTH(endTime) = ".$nowM.") ";
	$sql.="or (YEAR(startTime2)=".$nowY." and MONTH(startTime2) = ".$nowM.") ";
	$sql.="or (YEAR(endTime2)=".$nowY." and MONTH(endTime2) = ".$nowM.")) ";
	$sql.="and (startTime<'".$today."' or startTime2<'$today') order by startTime";
	//$logtx.="$sql \n";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$startTime=strtotime($r['startTime']);
			$endTime=strtotime($r['endTime']);
			$startTime2=strtotime($r['startTime2'])?strtotime($r['startTime2']):0;
			$endTime2=strtotime($r['endTime2'])?strtotime($r['endTime2']):0;
			if(!$startTime && !$endTime)	continue;	//無排班資料直接跳過			
			//::檢查外出紀錄
			foreach($outoffice_data as $k1=>$v1){
				$ot_bTime = strtotime($v1['bDate']);
				$ot_eTime = strtotime($v1['eDate']);
				$rtnDate = strtotime($v1['rtnDate']);
				if( date('Ymd',$startTime)==date('Ymd',$ot_bTime) ) {	//當天排班有外出資料
					if($startTime2==0 || $startTime2>$ot_bTime) $startTime2=$ot_bTime;	//直接外出，以外出時間為之
					if($v1['offwork']==1) $endTime2=$endTime;		//直接下班
					if($endTime2==0 || $endTime2<$ot_eTime) {	//沒打下班，以直接下班或返回時間為之
						if($rtnDate) $endTime2=$rtnDate;
					}
				}
			}
			//$logtx .= date('Y-m-d H:i',$startTime).','.date('Y-m-d H:i',$startTime2)."\n";
			$abHours=0; $abId=0; $abTable='';	$st='';	$et='';
			if($startTime2==0 && $endTime2==0){ //------------沒有打卡記錄----------------			
				$flag1 = false;	
				foreach($absence_data as $k1=>$v1){ //掃請假資料
					$st=$v1['bDate'];	$et=$v1['eDate'];
					$ab_bTime=strtotime($st);
					$ab_eTime=strtotime($et);
					//::使用範圍檢查法
					if( ($startTime>=$ab_bTime) && ($endTime<=$ab_eTime) ){	//落在請假區間正常退出
						$flag1 =  true;
						break;	
					}	else if( $endTime<=$ab_bTime ){	//落在請假區間之前 跳下一筆
						continue;
					} else if( $startTime>$ab_eTime){	//落在請假區間之後 跳下一筆
						continue;
					} else {	//部分落在請假區間 請假異常
						$abId=$k1; $abTable='absence';
						$abHours=ckAbsence($ab_bTime,$ab_eTime,$startTime,$endTime);
						$abStatus='未打卡-請假異常';
						$sql="insert into $table (empID,fid,abTable,abID,abStatus,abHours,abMonth,bDate,eDate) ";
						$sql.="values('".$r["empID"]."','".$r['id']."','".$abTable."','".$abId."','".$abStatus."','".$abHours."','".$abMonth."','".$st."','".$et."')";
						db_query($sql);
						$flag1 =  true;
						break;
					}
				}
				if(!$flag1) {	//本時段未請假
					$abStatus='未打卡';
					$sql="insert into $table (empID,fid,abTable,abID,abStatus,abHours,abMonth,bDate,eDate) ";
					$sql.="values('".$r["empID"]."','".$r['id']."','".$abTable."','".$abId."','".$abStatus."','".$abHours."','".$abMonth."','".$r['startTime']."','".$r['endTime']."')";
					db_query($sql);								
				}
			}else{ //-------------------------有打卡記錄----------------------
				foreach($absence_data as $k1=>$v1){ //掃請假資料
					$st=$v1['bDate'];	$et=$v1['eDate'];
					$ab_bTime=strtotime($st);
					$ab_eTime=strtotime($et);
					//::使用範圍檢查法
					if( ($startTime>=$ab_bTime) && ($endTime<=$ab_eTime) ){	//全天請假區
						$startTime2=$startTime;	$endTime2=$endTime;
						break;
					}	else if( $endTime<=$ab_bTime ){	//沒打卡 落在請假區間之前 跳下一筆
						continue;
					} else if( $startTime>$ab_eTime){	//沒打卡 落在請假區間之後 跳下一筆
						continue;
					} else {	//部分請假,要檢查是否同一天
						if (date('Y-m-d',$startTime) != date('Y-m-d',$ab_bTime) ) continue;
						if (date('Y-m-d',$endTime) != date('Y-m-d',$ab_eTime) ) continue;					
						if($startTime2==0 || $startTime2>$ab_bTime) $startTime2=$ab_bTime;
						if($endTime2==0 || $endTime2<$ab_eTime) $endTime2=$ab_eTime;
						$abId=$k1; $abTable='absence';
						break;
					}
				}	
									
				if(($startTime2==0 && $endTime2>0) || ($startTime2>0 && $endTime2==0)) {//只打到上/下班
					$abHours=ceil_dec(($endTime-$startTime)/3600);
					$sql="insert into $table (empID,fid,abTable,abID,abStatus,abHours,abMonth,bDate,eDate) ";
					$sql.="values('".$r["empID"]."','".$r['id']."','','".$r["id"]."','上班或下班未打卡','".$abHours."','".$abMonth."','".$r['startTime']."','".$r['endTime']."')";
					db_query($sql);
				}else{
					if($startTime>0 && ($startTime2-$startTime)>59){//遲到
						if(!isset($ab_bTime)) {
							$ab_bTime = $startTime;
							$ab_eTime = $startTime;
						}
						$abHours=ckAbsence($ab_bTime,$ab_eTime,$startTime,$startTime2);
						$sql="insert into $table (empID,fid,abTable,abID,abStatus,abHours,abMonth,bDate,eDate) ";
						$sql.="values('".$r["empID"]."','".$r['id']."','".$abTable."','".$abId."','遲到','".$abHours."','".$abMonth."','".$r['startTime']."','".$r['endTime']."')";
						db_query($sql);
						unset($ab_bTime);
						unset($ab_eTime);
					}
					if($endTime>0 && $endTime2<$endTime){//早退
						if(!isset($ab_bTime)) {
							$ab_bTime = $endTime;
							$ab_eTime = $endTime;
						}
						$abHours=ckAbsence($ab_bTime,$ab_eTime,$endTime2,$endTime);
						$sql="insert into $table (empID,fid,abTable,abID,abStatus,abHours,abMonth,bDate,eDate) ";
						$sql.="values('".$r["empID"]."','".$r['id']."','".$abTable."','".$abId."','早退','".$abHours."','".$abMonth."','".$r['startTime']."','".$r['endTime']."')";
						db_query($sql);
						unset($ab_bTime);
						unset($ab_eTime);
					}
				}
			}
		}
	}
	//file_put_contents('log.txt',$logtx);
}

function ckAbsence($bDate,$eDate,$time1,$time2){//檢查請假單時段
	$abHours=0;
	if(($bDate<=$time1 && $eDate>=$time2)){//有請假
		$abHours=-1;
	}else if($bDate<=$time1 && $eDate<$time2){//部分請假：前半段
		$abHours=ceil_dec(abs(($time2-$eDate)/3600));
	}else if($bDate>$time1 && $eDate<=$time2){//部分請假：後半段
		$abHours=ceil_dec(abs(($bDate-$time1)/3600));
	}else if($bDate>$time1 && $eDate<$time2){//部分請假：中半段
		$abHours=ceil_dec(abs(($bDate-$time1)/3600+($time2-$eDate)/3600));	
	}
	return $abHours;
}
function ceil_dec($num){//0.5進位
	$wholeNumber = (int) $num;
	$dec = $num - $wholeNumber;
	if ($dec == 0){
		$finalNumber = $wholeNumber;
	}else if ($dec <= 0.5){
		$finalNumber = $wholeNumber + 0.5;
	}else if ($dec > 0.5){
		$finalNumber = $wholeNumber + 1;
	}
	return $finalNumber ;
}
?>