<?php 
	// for Access Permission Control
	if(!session_id()) session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "勞保費負擔金額表";
	$tableName = "map_job";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	
	// Here for List.asp ======================================= //
	$defaultOrder = "value";
	$searchField1 = "title";
	$searchField2 = "value";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one

	$fnAry = explode(',',"id,title,value,levels");
	$ftAry = explode(',',"ID,投保薪資,自付額,等級");
	$flAry = explode(',',"60,,,,");
	$ftyAy = explode(',',"ID,text,text,text");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"levels,title,value");
	$newftA = explode(',',"等級,投保薪資,自付額");
	$newflA = explode(',',",,");
	$newfhA = explode(',',",,");
	$newfvA = array('','','');
	$newetA = explode(',',"text,text,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,levels,title,value");
	$editftA = explode(',',"ID,等級,投保薪資,自付額");
	$editflA = explode(',',",,,");
	$editfhA = explode(',',",,,");
	$editfvA = array('','','','');	
	$editetA = explode(',',"hidden,text,readonly,text");
?>	