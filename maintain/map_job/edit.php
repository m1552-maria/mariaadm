<?php
	session_start();
	include("../../config.php");
	include("init.php");
	$ID = $_REQUEST['ID'];
	$sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?php echo $extfiles?>edit.js"></script> 
 <script type="text/javascript">
 	$(document).ready(function(){
 		var old = $('input[name="levels"]').val();
 		$('input[name="levels"]').change(function(){
 			if(old != $(this).val()){
	 			$.ajax({
					type: 'post',
					url: '/maintain/map_job/levelCheck.php',
					dataType: 'json',
					data:{ levels: $(this).val() },
					success:function(d){
						if(d==1) alert('等級重複');
					}
				});
			}
		});
 	});
 </script>
 <title><?php echo $pageTitle?> - 編輯</title>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data">
<table align="center" class="sTable" width="640" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?php echo $pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?php echo $editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?php echo $editfnA[0]?>" value="<?php echo $ID?>"><?php echo $ID?></td>
	</tr>

	<?php for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?php echo  $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?php echo $editftA[$i]?></td>
  	<td><?php switch($editetA[$i]) { 
  		case "readonly" : echo $r[$editfnA[$i]]."<input type='hidden' name='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
  	  	case "text" : echo "<input type='text' name='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea> <input type='button' value='編輯' onClick='HTMLEdit($editfnA[$i])'>"; break;
  		case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";				
  	} ?></td>
  </tr><?php } ?>

<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?php echo $ID?>">
  	<input type="hidden" name="pages" value="<?php echo $_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?php echo $_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?php echo $_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?php echo $_REQUEST['sortDirection']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <button type="button" class="btn" onClick="history.back()">取消編輯</button>
  </td>
</tr>
</table>
</form>
</body>
</html>
