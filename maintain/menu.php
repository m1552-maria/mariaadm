<?php 
	session_start();
	//::取的權限設定
	$str = file_get_contents('priviege.txt');
	$priviege = json_decode($str,true);
	$plb = $_SESSION["privilege"];
		
	//::帶参數傳入(整頁方式)
  $cpPage = "http://".$_SERVER['SERVER_NAME'];  		//首頁 網址
  $cpRoot = str_replace("\\","/",realpath('../'));  //檔案 真實路徑		
	//::直接開(編輯按鈕)的圖檔路徑
	$_SESSION[heroot] = $cpRoot.'/data/sys/';
	$_SESSION[heurl]  = $cpPage.'/data/sys/';	

	// Type, Link Path, Target, bullet picture
	if($_SESSION['Account']!='') {
		if(in_array('A',$priviege[$plb])) {
			$Menu['基本資料維護'] = array();
			if(in_array('A1',$priviege[$plb])) $Menu['基本資料維護']['部門 資料維護'] =	array(0,'department/list.php','main','');
			if(in_array('A2',$priviege[$plb])) $Menu['基本資料維護']['職銜 資料維護'] =	array(0,'jobs/list.php','main','');
			if(in_array('A3',$priviege[$plb])) $Menu['基本資料維護']['員工 資料維護'] =	array(0,'emplyee/index.php','main','');
			if(in_array('A4',$priviege[$plb])) $Menu['基本資料維護']['欄位資料對應表'] =	array(0,'varsSet.php','main','');
			$Menu['基本資料維護']['健保費負擔金額表'] =	array(0,'map_health/list.php','main','');
			$Menu['基本資料維護']['勞保費負擔金額表'] =	array(0,'map_job/list.php','main','');
			$Menu['基本資料維護']['退休金工資分級表'] =	array(0,'map_retire/list.php','main','');
			$Menu['基本資料維護']['年資/特休'] =	array(0,'emplyee_annual/list.php','main','');
			$Menu['基本資料維護']['特休信件'] =	array(0,'schedule_send_mail/list.php','main','');
			
		}
		if(in_array('B',$priviege[$plb])) {
			$Menu['制度規章維護'] = array();
			if(in_array('B1',$priviege[$plb])) $Menu['制度規章維護']['制度規章 分類表'] =	array(0,'docclasses/list.php','main','');
			if(in_array('B2',$priviege[$plb])) $Menu['制度規章維護']['發佈 制度規章'] =	array(0,'documents/list.php','main','');
		}
		// if( in_array('C',$priviege[$plb]) ) {
		// 	$Menu['知識平台維護'] = array();
			
		// }
		// if( in_array('L',$priviege[$plb]) ) {
		// 	$Menu['資訊查閱維護'] = array();
			
		// }
		if( in_array('D',$priviege[$plb]) ) {
			$Menu['後台帳號'] = array();
			if(in_array('D1',$priviege[$plb])) $Menu['後台帳號']['後台帳號維護'] = array(0,'admins/list.php','main','');
			if(in_array('D2',$priviege[$plb])) $Menu['後台帳號']['等級功能設定'] = array(0,'priviege.php','main','');
      $Menu['說明檔編輯'] = array();
		  $Menu['說明檔編輯']['請假須知'] = array(0,'absence/notice.php?ftype=0','main','');
		  $Menu['說明檔編輯']['加班、補修須知'] = array(0,'absence/notice.php?ftype=1','main','');
		  $Menu['說明檔編輯']['影像事件須知'] = array(0,'absence/notice.php?ftype=2','main','');
		  $Menu['首頁輪撥圖片'] = array(0,'newspic/list.php','main','');
			$Menu['網頁編輯'] = array();
		  $Menu['網頁編輯']['制度規章說明'] = array(0,"../htmledit.php?fn=admins/main.html",'main','');
		  $Menu['網頁編輯']['專案管理說明'] = array(0,"../htmledit.php?fn=projects/main.html",'main','');	
		  $Menu['網頁編輯']['知識平台說明'] = array(0,"../htmledit.php?fn=knowledges/main.html",'main','');      
		}		
		if( in_array('E',$priviege[$plb]) ) $Menu['最新公告'] = array(0,'bullet1/list.php','main','');
		if( in_array('F',$priviege[$plb]) ) $Menu['最新消息'] = array(0,'bullets/list.php','main','');
		if( in_array('G',$priviege[$plb]) ) $Menu['授權資料維護'] = array(0,'permits/list.php','main','');
		if( in_array('H',$priviege[$plb]) ) $Menu['系統參數設定'] = array(0,'/set_system.php','main','');
		if( in_array('I',$priviege[$plb]) ) $Menu['全員行事曆'] = array(0,'schedule_all/list.php','main','');
		if( in_array('J',$priviege[$plb]) )	$Menu['保險系統'] = array(0,'insurances/index.php','main','');
		if( in_array('K',$priviege[$plb]) )	$Menu['請假系統'] = array(0,'absence/index.php','main','');
		if( in_array('K',$priviege[$plb]) )	$Menu['加班系統'] = array(0,'overtime/index.php','main','');
		if( in_array('K1',$priviege[$plb]) ) $Menu['綜合申請單'] = array(0,'integratform/index.php','main','');
		if( in_array('K2',$priviege[$plb]) ) $Menu['外出單'] = array(0,'outoffice/index.php','main','');
		
		if(in_array('C',$priviege[$plb]) || in_array('L',$priviege[$plb]) || in_array('M',$priviege[$plb]) || in_array('N',$priviege[$plb])) $Menu['文件管理'] = array();
		if(in_array('C',$priviege[$plb])) $Menu['文件管理']['知識平台'] = array();
		if(in_array('C1',$priviege[$plb])) $Menu['文件管理']['知識平台']['知識平台分類表'] =	array(0,'kwgclasses/list.php','main','');
		if(in_array('C2',$priviege[$plb])) $Menu['文件管理']['知識平台']['發佈知識平台'] =	array(0,'knowledges/list.php','main','');

		if(in_array('L',$priviege[$plb])) $Menu['文件管理']['資訊查閱'] = array();
		if(in_array('L1',$priviege[$plb])) $Menu['文件管理']['資訊查閱']['資訊查閱分類表'] =	array(0,'infoclasses/list.php','main','');
		if(in_array('L2',$priviege[$plb])) $Menu['文件管理']['資訊查閱']['發佈資訊查閱'] =	array(0,'information/list.php','main','');
		if(in_array('L2',$priviege[$plb])) $Menu['文件管理']['資訊查閱']['發佈資訊查閱執行長專區'] =	array(0,'information2/list.php','main','');
		
		if(in_array('M',$priviege[$plb])) $Menu['文件管理']['資源管理'] = array();
		if(in_array('M1',$priviege[$plb])) $Menu['文件管理']['資源管理']['資源管理 分類表'] =	array(0,'resource_class/list.php','main','');
		if(in_array('M2',$priviege[$plb])) $Menu['文件管理']['資源管理']['發佈 資源內容'] =	array(0,'resources/index.php','main','');

		if(in_array('N',$priviege[$plb])) $Menu['文件管理']['影像事件'] = array();
		if(in_array('N1',$priviege[$plb])) $Menu['文件管理']['影像事件']['影像事件 分類表'] = array(0,"event_class/list.php",'main','');
		if(in_array('N2',$priviege[$plb])) $Menu['文件管理']['影像事件']['發佈 影像事件'] = array(0,"event/index.php",'main','');	
		
		if( in_array('O',$priviege[$plb]) ) {	
			$Menu['員工排班作業'] = array();
			$Menu['員工排班作業']['輪班班表'] = array(0,"duty/list.php?uType=2",'main','');
			$Menu['員工排班作業']['週班表'] = array(0,"duty/index.php?uType=0",'main','');
			$Menu['員工排班作業']['特殊班表']= array(0,"duty/index.php?uType=1",'main','');
			$Menu['員工排班作業']['時段設定'] = array(0,"duty_period/list.php",'main','');
			$Menu['員工排班作業']['代換班紀錄'] = array(0,"emplyee_duty_change/list.php",'main','');
		}
		
		if( in_array('P',$priviege[$plb]) ) {	
			$Menu['雜項費用'] = array();
			$Menu['雜項費用']['項目維護'] = array(0,"otherfee_class/list.php",'main','');
			$Menu['雜項費用']['設定作業'] = array(0,"otherfee/list.php",'main','');
			$Menu['雜項費用']['清冊作業'] = array(0,"emplyee_otherfee/list.php",'main','');
		}
		
		if( in_array('Q',$priviege[$plb]) ) {	
			$Menu['薪資作業'] = array();
			$Menu['薪資作業']['津貼'] = array();
			$Menu['薪資作業']['津貼']['項目維護'] = array(0,"allowance_class/list.php",'main','');
			$Menu['薪資作業']['津貼']['設定作業'] = array(0,"allowance/list.php",'main','');
			$Menu['薪資作業']['津貼']['清冊作業'] = array(0,"emplyee_allowance/list.php",'main','');
			$Menu['薪資作業']['年終獎金'] = array();
			$Menu['薪資作業']['年終獎金']['設定作業'] = array(0,"bonus_festival/list.php?type=year_end",'main','');
			$Menu['薪資作業']['年終獎金']['清冊作業'] = array(0,"emplyee_bonus_year_end/list.php",'main','');
			$Menu['薪資作業']['三節獎金'] = array();
			$Menu['薪資作業']['三節獎金']['設定作業'] = array(0,"bonus_festival/list.php?type=festival",'main','');
			$Menu['薪資作業']['三節獎金']['清冊作業'] = array(0,"emplyee_bonus_festival/list.php?type=festival",'main','');
			$Menu['薪資作業']['其他獎金'] = array();
			$Menu['薪資作業']['其他獎金']['項目維護'] = array(0,"bonus_class/list.php",'main','');
			$Menu['薪資作業']['其他獎金']['設定作業'] = array(0,"bonus/list.php",'main','');
			$Menu['薪資作業']['其他獎金']['清冊作業'] = array(0,"emplyee_bonus/list.php",'main','');
			$Menu['薪資作業']['未補休加班單'] = array(0,"emplyee_salary_report/overdue.php",'main','');
			$Menu['薪資作業']['異常補修單'] = array(0,"emplyee_salary_report/fixAbsent.php",'main','');
			//$Menu['薪資作業']['即將逾期補休加班單'] = array(0,"emplyee_salary_report/willOverdue.php",'main','');
			$Menu['薪資作業']['薪資'] = array();
			$Menu['薪資作業']['薪資']['試算'] = array(0,"emplyee_salary_report/index.php",'main','');
			$Menu['薪資作業']['薪資']['加班費'] = array(0,"emplyee_salary_items/list.php?type=overtime",'main','');
			$Menu['薪資作業']['薪資']['出缺勤'] = array(0,"emplyee_salary_items/list.php?type=absence",'main','');
			$Menu['薪資作業']['薪資']['勞健退'] = array(0,"emplyee_salary_insurence/list.php",'main','');
			$Menu['薪資作業']['薪資']['離職專區'] = array(0,"emplyee_salary_report/index.php?fieldname=1",'main','');
		}
		
		if( in_array('R',$priviege[$plb]) ) {
			$Menu['打卡系統'] = array();
			// $Menu['打卡系統']['前台打卡系統(測試用)'] = array(0,"../emplyee_dutylog",'_blank','');
			$Menu['打卡系統']['打卡機位置設定'] = array(0,"punch_card_setting/list.php",'main','');
			$Menu['打卡系統']['打卡異動紀錄'] = array(0,"punch_card_log/list.php",'main','');
			if(in_array('R1',$priviege[$plb])) $Menu['打卡系統']['打卡資料'] = array(0,"emplyee_dutylog/list.php",'main','');
			if(in_array('R4',$priviege[$plb])) $Menu['打卡系統']['出勤資料'] = array(0,"emplyee_duty_list/list.php",'main','');
			$Menu['打卡系統']['員工識別證清單'] = array(0,"emplyee_card_log/list.php",'main','');
			$now_day = date('Y-m-d');
			$start_day = date('Y-m-d H:i:s',strtotime($now_day));
      		$end_day = date('Y-m-d H:i:s',mktime(23, 59, 59, date('m',strtotime($now_day)), date('d',strtotime($now_day)),date('Y',strtotime($now_day))));
			if(in_array('R2',$priviege[$plb])) $Menu['打卡系統']['匯入當天打卡資料'] = array(0,"punch/api.php?start_day=".$start_day.'&end_day='.$end_day,'main','');
			$Menu['打卡系統']['識別卡版型'] = array(0,"emplyee_card/templet.php",'main','');
			if(in_array('R3',$priviege[$plb])) $Menu['打卡系統']['列印識別卡'] =	array(0,'emplyee_card/first.php','main','');
			$Menu['打卡系統']['出缺勤異常作業'] = array(0,"duty/abnormal.php",'main','');
			$Menu['打卡系統']['月餅打卡檢核表'] = array(0,"../maintain/emplyee_dutylog/list_cake.php",'main','');
			$Menu['打卡系統']['月餅打卡檢核表<br/>(浩棟加速版)'] = array(0,"../maintain/emplyee_dutylog/list_cake-haotung.php",'main','');
		}

		if( in_array('S',$priviege[$plb]) ) {	
			$Menu['出納作業'] = array();
			$Menu['出納作業']['人員基本資料'] = array(0,"emplyee_cashier/index.php",'main','');
			$Menu['出納作業']['機構資料維護'] = array(0,"../psi/psi_company/list.php",'main','');
			$Menu['出納作業']['銀行媒體轉帳'] = array();
		//	$Menu['出納作業']['銀行媒體轉帳']['設定作業'] = array(0,"transfer/list.php",'main','');
			$Menu['出納作業']['銀行媒體轉帳']['清冊作業'] = array(0,"emplyee_transfer/list.php",'main','');
		}

		if( in_array('T',$priviege[$plb]) ) {	 
      $Menu['郵件編輯'] = array();
		  // $Menu['打卡系統']['前台打卡系統(測試用)'] = array(0,"../emplyee_dutylog",'_blank','');
		  $Menu['郵件編輯']['滿六個月特休假通知'] = array(0,"mails/template.php?type=sixMonths",'main','');
		  $Menu['郵件編輯']['1/1特休假通知'] = array(0,"mails/template.php?type=day0101",'main','');
		  $Menu['郵件編輯']['7/15通知特休假未休天數'] = array(0,"mails/template.php?type=day0715",'main','');
		  $Menu['郵件編輯']['11/1通知特休假未休天數'] = array(0,"mails/template.php?type=day1101",'main','');
    }
    	if( in_array('U',$priviege[$plb]) ) {	
    		$Menu['綜合所得稅'] = array();
    		if(in_array('U1',$priviege[$plb])) $Menu['綜合所得稅']['薪資、獎金統計(50)'] = array(0,"salary_bonus_export/list.php",'main','');
    		if(in_array('U2',$priviege[$plb])) $Menu['綜合所得稅']['薪資、獎金統計(91)'] = array(0,"salary_bonus_export2/list.php",'main','');
    		if(in_array('U3',$priviege[$plb])) {
    			$Menu['綜合所得稅']['媒體檔作業']['媒體作業'] = array(0,"income_tax/list.php",'main','');
    			/*$Menu['綜合所得稅']['媒體檔作業']['獎金資料'] = array(0,"income_tax/list.php?type=bonus",'main','');
    			$Menu['綜合所得稅']['媒體檔作業']['excel匯入'] = array(0,"income_tax/list.php?type=excel",'main','');*/
    		}
    		if(in_array('U4',$priviege[$plb])) $Menu['綜合所得稅']['退休金自提'] = array(0,"self_pension/list.php",'main','');

    	}

    	if( in_array('V',$priviege[$plb]) ) $Menu['各類報表'] = array(0,'variousReports/list.php','main','');
		
		//$ecStr = json_encode($_SESSION);
		//$ecStrF = mic_crypt($_SESSION['Account'],$ecStr,'e');
		if( in_array('Z',$priviege[$plb]) ) $Menu['預算管理系統'] = array(0,'../budget/index.php','_blank','');
		//if( in_array('Z1',$priviege[$plb]) ) $Menu['公文系統'] = array(0,"/tt2.php?act=$_SESSION[Account]&cryp=$ecStrF",'_blank','');
	}

	$Menu['登出']    = array(0,'logout.php','main','logoutB.gif');
	$Menu['訪客首頁'] = array(0,'/','_new','homeB.gif');
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="matintain.css" rel="stylesheet" type="text/css">
<title>主選單</title>
<script type="text/javascript">
var ahref;
function swtichDiv(ly,img) {
	ly.style.display = (ly.style.display=='none'?'':'none');
	img.src = (ly.style.display=='none'?'images/fold.gif':'images/open.gif');
}
function setfcn(pt) { top.topfrm.setfuncname('<< '+pt+' >>'); }
function chkpage() {
	 if(ahref != top.main.location.href) {
	 		ahref = top.main.location.href;
			setfcn(top.main.document.title);
	 }
}
window.onload = function(){	window.setInterval('chkpage()',3000);	}
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
a:link { color: #224B59; text-decoration:none }
a:visited {	color: #224B59;	text-decoration: none;}
a:hover {	color: #FF0000;	text-decoration: none;}
a:active { color: #FFFF00; text-decoration: none;	font-weight: bold;}
.itemTxt { font-family: Verdana, "細明體";	font-size: 16px; }
.itemTxt2 {	font-size: 13px; }
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>

<body>
<table width="175" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="4" class="top"></td>
    <td valign="top" class="map-c">
			<table width="100%" border="0">
			<? $seq =1;?>
			<? foreach($Menu as $k => $v) 
				if ($v[0]===0) { //項目
			   	 $img = $v[3]=='' ? 'table.gif' : $v[3]; 
				   echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt'><a href='$v[1]'";
					 if($v[2]>'') echo "target='$v[2]'";
					 echo ">$k</a></td></tr>";
				} else {	//目錄
					 $lyName = "ly$seq";
					 $imName = "im$seq";
					 $img = 'fold.gif'; 
				   echo "<tr><td width='16'><img name='im$seq' src='images/$img' onclick='swtichDiv($lyName,this)' style='cursor:hand'></td>";
					 echo "<td class='itemTxt'><a href='javascript:swtichDiv($lyName,$imName)'>$k</a></td></tr>";
 					 $seq++;
					 //目錄清單
					 echo "<tr><td width='16'></td><td>";
					 echo "<div id='$lyName' style='display:none'><table width='100%'>";
					 foreach($v as $k2 => $v2)  {
					 	if ($v2[0]===0) {//項目
					 		$img = $v2[3]=='' ? 'table.gif' : $v2[3];
							echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt2'><a href='$v2[1]'";
					 		if($v2[2]>'') echo "target='$v2[2]'";
					 		echo ">$k2</a></td></tr>";
					 	}else{//目錄
					 		$lyName = "ly2$seq";
							 $imName = "im$seq";
							 $img = 'fold.gif'; 
						   echo "<tr><td width='16'><img name='im$seq' src='images/$img' onclick='swtichDiv($lyName,this)' style='cursor:hand'></td>";
							 echo "<td class='itemTxt'><a href='javascript:swtichDiv($lyName,$imName)'>$k2</a></td></tr>";
		 					 $seq++;
							 //目錄清單
							 echo "<tr><td width='16'></td><td>";
							 echo "<div id='$lyName' style='display:none'><table width='100%'>";
							 foreach($v2 as $k3 => $v3)  {
							 	if ($v3[0]===0) {
							 		$img = $v3[3]=='' ? 'table.gif' : $v3[3];
									echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt2'><a href='$v3[1]'";
							 		if($v3[2]>'') echo "target='$v3[2]'";
							 		echo ">$k3</a></td></tr>";
							 	}else{
							 		
							 	}
							 }
							 echo "</table></div>";
							 echo "</td></tr>";
					 	}
					 }
					 echo "</table></div>";
					 echo "</td></tr>";
				} 
			?>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
