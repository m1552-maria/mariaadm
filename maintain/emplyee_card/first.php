
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 	body{
 		font-family: '微軟正黑體';
 	}
 	.row{
 		margin: 20px 0;
 		border-radius: 5px;
 	}
 	
 	.title{
 		background-color: #e8d6b7;
    	color: #000000;
 	}
 	.number{
 		display: inline-block;
	    background-color: #988462;
	    color: white;
	    width: 25px;
	    height: 25px;
	    text-align: center;
	    padding: 5px;
	}
 	.title1{
 		display: inline-block;
 	}
 	.content{
 		padding: 10px;
 	}
 	.print{
 		text-align: center;
 	}
 	
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <script type="text/javascript">
 	function formvalid(now){
 		
 		if(confirm('確定要列印嗎?')){
 			return true;
 		}
 		return false;
 	}

 	
 </script>
 <title>列印識別卡</title>
 
</Head>

<body class="page">


<form action="final.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)" target="_blank">

<p style="color:red;font-size:18px;font-weight: bolder;">列印識別卡</p>

<div class="row" style="border:1px solid black;">
	
	<div class="title">
		<div class="number">1</div>
		<div class="title1">選取部門及員工</div>
	</div>
	<div style="position: relative;padding:0 0 20px 0;">
		<ul class='dep-emp-ul'>
			<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
			<li class='dep-emp-li'>
				<div class='query-title'>部門</div>
				<div class="depID_list">NO DATA</div>
			</li>
			<li class='dep-emp-li'>
				<div class='query-title'>員工</div>
				<div class="empID_list">NO DATA</div>
			</li>
		</ul>
		<div style="clear: both;"></div>
	</div>
</div>
<div class="row" style="border:1px solid black;">
	<div class="title">
		<div class="number">2</div>
		<div class="title1">選擇列印版型</div>
	</div>
	<div class="content">
		<label><input type="radio" name="web" value="positive" checked>正面</label>
		<label><input type="radio" name="web" value="negative">反面</label>
		<!-- <label><input type="radio" name="web" value="both">正反面</label> -->
	</div>
</div>



<div style="text-align: center;">
<input type="submit" name="" value="預覽列印">
</div>

</form>
</body>
</html>

