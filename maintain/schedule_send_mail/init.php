<?	
	// for Access Permission Control
	/*$a = (strtotime('2018-01-01') - strtotime('2017-06-15')) /3600/24;
	echo ($a).'<br>';
	$a = (strtotime('2017-06-15') - strtotime('2017-01-10')) /3600/24;
	echo ($a).'<br>';
	$a = (strtotime('2018-01-10') - strtotime('2018-01-01')) /3600/24;
	echo $a;
	exit;*/

	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}

	include("../../config.php");
	include('../../getEmplyeeInfo.php');
	

	/*include("emplyeeAnnualApi.php");
	$a = array('empID'=>99998,'wkYear'=>2018,'insertDB'=>1);
	$data = emplyeeAnnualApi($a);
	echo "<pre>";
	print_r($data);
	exit;*/
	
	$bE = false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "特休信件";
	$tableName = "emplyee_annual as a inner join emplyee as b on b.empID=a.empID inner join schedule_send_mail as c on c.empID=a.empID";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/schedule_send_mail/';
	$delField = '';
	$delFlag = false;
	$imgpath = '../../data/schedule_send_mail/';

	//預設年度
	$nowYr = date('Y');
	$curYr = $_REQUEST['year']?$_REQUEST['year']:$nowYr;
	
	$yearList = array($nowYr+1=>$nowYr+1);
	$sql = "select year from schedule_send_mail group by year order by year desc";
	$rs = db_query($sql);
	while ($r = db_fetch_array($rs)) {
		$yearList[$r['year']] = $r['year'];
	}
	if(empty($yearList[date('Y')])) $yearList[date('Y')] = date('Y');

	if(!isset($yearList[$curYr])) $curYr = key($yearList);

	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "depID=''";
	}

	if($_REQUEST['year']!=''){
		$temp[] = "c.year='$_REQUEST[year]'";	
		$temp[] = "a.year='$_REQUEST[year]'";	
	}else{
		$temp[] = "c.year='$curYr'";
		$temp[] = "a.year='$curYr'";
	}

	$isSendType = -2;
	if($_REQUEST['isSendType'] != '') {
		$isSendType = $_REQUEST['isSendType'];
		$temp[] = "c.isSend='".$_REQUEST['isSendType']."'";
	}

	/*print_r($_REQUEST['isSendType']);
	exit;*/
	$sendType = array(-1=>'尚未同意',0=>'等待寄出',1=>'已通知');
	$temp[] = "(b.isOnduty=1 OR b.isOnduty=2)";//不要看到離職員工的資料

	$notes = (isset($_REQUEST['notes'])) ? $_REQUEST['notes'] : '';
	if($notes != '') {
		$temp[] = "sendType like '%".$notes."%'";
	}

	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //

	//left join 條件
	//$defWh = "b.empID=a.empID";


	$defaultOrder = "b.empID,b.depID";
	$searchField1 = "b.empID";
	$searchField2 = "b.empName";

	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	// $fnAry = explode(',',"empID,empName,depID,spHolidays,actDays,trueDays,action");
	// $ftAry = explode(',',"編號,姓名,部門,今年度特休,會計年資,實際年資,調整");
	// $flAry = explode(',',"50,,,,,,,,,");
	// $ftyAy = explode(',',"id,text,text,text,text,text,text,text,text");

	$fnAry = explode(',',"cID,empID,empName,depID,hours,actDays,trueDays,hireDate,sendDate,sender,isSend,sendType");
	$ftAry = explode(',',"編號,員編,姓名,部門,年度特休,會計年資,實際年資(算到今天),到職日,預計通知日,通知人員,通知狀態,備註");
	$flAry = explode(',',"80,,,,,,,,,,,,,,,");
	$ftyAy = explode(',',"ID,text,text,text,text,text,text,text,text,,text,text,text");
?>