<?php
  session_start();
  if(empty($_REQUEST['year'])) exit;
 
  $filename = '年終獎金-詳細資料'.date("Y-m-d H:i:s",time()).'.xlsx';
  

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');

  //取得主檔
  $sql = "select * from bonus_festival where wkYear='".$_REQUEST['year']."' and type='year_end'";
  $rs = db_query($sql);
  if(db_num_rows($rs) == 0) exit;
  $bonusData = db_fetch_array($rs);

  $sql = "select * from bonus_festival_basic where bfID='".$bonusData['id']."' order by id";
  $rs = db_query($sql);
  $basicData = array();
  while ($r = db_fetch_array($rs)) {
    $basicData[$r['className']][$r['object']] = $r['depiction'];
  }


  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();

  $sheet->mergeCells('A1:R1');//合併
  $sheet->setCellValue('A1',$bonusData['title'].'清冊');
  $sheet->getStyle('A1:R1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A1:R1')->getFont()->setSize(16);

  $header = array(
    'A'=>array('key'=>'serial','width'=>5,'val'=>array('序號')),
    'B'=>array('key'=>'empID','width'=>10,'val'=>array('員編')),
    'C'=>array('key'=>'empName','width'=>10,'val'=>array('姓名')),
    'D'=>array('key'=>'hireDate','width'=>12,'val'=>array('就職日期')),
    'E'=>array('key'=>'stopRange','width'=>15,'val'=>array('留停日期')),
    'F'=>array('key'=>'leaveDate','width'=>12,'val'=>array('離職日期')),
    'G'=>array('key'=>'actDays','width'=>15,'val'=>array($_REQUEST['year'].'/12/31','服務年資')),
    'H'=>array('key'=>'jobID','width'=>15,'val'=>array('職稱')),
    'I'=>array('key'=>'jobClass','width'=>10,'val'=>array('工時身份')),
    'J'=>array('key'=>'jobLevel','width'=>8,'val'=>array('主管職')),
    'K'=>array('key'=>'salary','width'=>9,'val'=>array('薪資/考核','月薪資')),
    'L'=>array('key'=>'score','width'=>9,'val'=>array('考核等級')),
    'M'=>array('key'=>'base','width'=>9,'val'=>array('考核基數')),
    'N'=>array('key'=>'sRatio','width'=>15,'val'=>array(((int)$_REQUEST['year']-1911).'年服務比例')),
    'O'=>array('key'=>'bonus','width'=>10,'val'=>array('基數: '.$bonusData['base'], '年終獎金')),
    'P'=>array('key'=>'bTax','width'=>15,'val'=>array($bonusData['taxRatio'].'%,起扣點'.$bonusData['deduct_base'].',', '扣稅金額')),
    'Q'=>array('key'=>'otherDeduct','width'=>10,'val'=>array('其他扣除')),
    'R'=>array('key'=>'money','width'=>15,'val'=>array('其他扣除備註')),
    'S'=>array('key'=>'money','width'=>10,'val'=>array('實領年終')),
    'T'=>array('key'=>'bNotes','width'=>20,'val'=>array('備註'))
  );

  foreach ($header as $k => $v) {
    $sheet->getColumnDimension($k)->setWidth($v['width']);  
    switch ($k) {
      case 'G':
      case 'O':
      case 'P':
        $sheet->setCellValue($k.'2', $v['val'][0]);
        $sheet->setCellValue($k.'3', $v['val'][1]);
        break;
      case 'K':
        $sheet->mergeCells('K2:M2');//合併
        $sheet->setCellValue('K2', $v['val'][0]);
        $sheet->setCellValue('K3', $v['val'][1]);
        $sheet->getStyle('K2:M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        break;
      case 'L':
      case 'M':
        $sheet->setCellValue($k.'3', $v['val'][0]);
        break;
      default:
        $sheet->mergeCells($k.'2:'.$k.'3');//合併
        $sheet->setCellValue($k.'2', $v['val'][0]);
        break;
    }

  }
  /*$sheet->getStyle('A1')->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'50696B')));
  $sheet->getStyle('A1')->getFont()->getColor()->setARGB('FFFFFF');*/
  //$sheet->getStyle('A2:Q3')->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'DFEDFD')));

  $gradeList = array('grade4'=>'優','grade3'=>'佳','grade2'=>'可','grade1'=>'差');
 /* $sql = "select b.*,b.tax as bTax, b.notes as bNotes, e.* from emplyee_bonus_year_end b left join emplyee e on b.empID=e.empID where b.wkYear='".$_REQUEST['year']."' and e.empID <> '00001' and b.empID <> '99998' and (e.isOnduty > 0 or (e.isOnduty = 0 and e.leaveDate >= '".$basicData['no']['leave']."'))";

  if(!empty($_REQUEST['depFilter'])) $sql.= " and e.depID like '".$_REQUEST['depFilter']."%'";
  else if ($_SESSION['privilege'] <= 10) {  
    if($_SESSION['user_classdef'][2]) {
      $aa = array();
      foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="e.depID like '$v%'";
      $sql.= " and (".join(' or ',$aa).")"; 
    } else $sql.= " and e.depID=''";
  }*/

  /*echo "<pre>";
  print_r($sql);
  exit;*/

  $sql = $_REQUEST['sql'];
  $sql.= " order by td.empID";
  $rs = db_query($sql);


  $i = 4;
  $serial = 1;
  while ($r=db_fetch_array($rs)) {
    foreach ($header as $k => $v) {
      switch ($v['key']) {
        case 'serial':
          $value = $serial;
          break;
        case 'actDays':
          $value = stohis($r[$v['key']]*24*60*60);
          break;
        case 'jobID':
          $value = isset($jobinfo[$r[$v['key']]]) ? $jobinfo[$r[$v['key']]] : '' ;
          break;
        case 'jobLevel':
          $value = ($r[$v['key']] == '主管') ? $r[$v['key']] : '';
          break;
        case 'score':
          $value = isset($gradeList[$r[$v['key']]]) ? $gradeList[$r[$v['key']]] : '';
          break;
        case 'empID':
        case 'base' :
          $value = " ".$r[$v['key']];
          break;
        case 'bonus':
          $value = ($r[$v['key']] == -1) ? '' : $r[$v['key']];
          break;
        default:
          $value = ($r[$v['key']] != '0000-00-00') ? $r[$v['key']] : '';
          break;
      }
      $sheet->setCellValueExplicit($k.$i, $value);
    }
    $i++;
    $serial++;
  }

  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');

function stohis($s){
  $year = intval($s/31536000);
  $month = intval(($s - ($year*31536000))/2592000);
  $day = intval(($s - ($year*31536000)-($month*2592000))/86400);
  return $year.'年'.$month.'月'.$day.'日';
}

?>