<?php
	session_start();
	include("../../config.php");

	$nowYr = date('Y');
	$curYr = $_REQUEST['year']?$_REQUEST['year']:$nowYr;
	if(isset($_REQUEST['month'])) $curMh = $_REQUEST['month']; else $curMh = date('m');
	if($_SESSION['privilege'] <= 10 && count($_SESSION['user_classdef'][7]) == 0) {
		echo "請設定管理的計畫編號";
		exit;
	}

	$yearList = array();
	$sql = "select wkYear,base from bonus_festival where type='year_end' order by wkYear desc";
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)) {
		$yearList[$r['wkYear']] = $r['wkYear'];
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<form name="excelfm2" action="exportExcel.php" method="post" target="excel">
<div>

清冊類別:
<select name="printType" id="printType">
	<option value="excel"> EXCEL </option>
	<option value="pdf"> PDF </option>
</select>

</div>
<br>
計畫編號:
<?php if($_SESSION['privilege'] > 10) { ?>
	<input type="text" name="projIDFilter" id="projIDFilter" value="">
<?php }else{?>
	<div>
		<label><input type="checkbox"  value="全選" onclick="allCheck(this)">全選</label>
		<?php 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo '<label><input type="checkbox" name="projIDFilter" value="'.$v.'">'.$v.'</label>';
	    ?>
	</div>


<?php } ?>

 <select name="year" id="year"><?php foreach($yearList as $key => $value) { $sel=($curYr==$key?'selected':''); echo "<option $sel value='$key'>$key 年</option>"; } ?></select>

<input type="button" onclick="excelfm2Smi()" value="確定">
<script src="../../Scripts/jquery-1.12.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
	function excelfm2Smi() {
<?php if($_SESSION['privilege'] > 10) { ?>
		window.opener.excelfm.projID.value=$('#projIDFilter').val();
<?php } else { ?> 
		var projID = [];
		$('[name="projIDFilter"]').each(function(){
			if(this.checked) projID.push($(this).val());
		});
		window.opener.excelfm.projID.value=projID.join(',');
<?php } ?>

		window.opener.excelfm.year.value=$('#year').val();
		window.opener.excelfm.printType.value=$('#printType').val();
		window.opener.excelfm.submit();
		window.close();
	}

	function allCheck(obj) {
		var pList = document.getElementsByName('projIDFilter');
		for(var i=0;i<pList.length;i++) {
			pList[i].checked = obj.checked;
		} 
	}
</script>
</form>
</body>
</html>