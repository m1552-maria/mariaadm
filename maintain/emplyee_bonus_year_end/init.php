<?php 
	session_start();
  	header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	

	
	$bE = false;	//異動權限	
	$pageTitle = "年終獎金清冊";
	$tableName = "emplyee_bonus_year_end a left join emplyee b on b.empID=a.empID left join emplyee_history h on b.empID=h.empID ";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee_bonus_year_end/';
	$delField = '';
	$delFlag = false;


	$yearList = array();
	$baseList = array();
	$sql = "select wkYear,base from bonus_festival where type='year_end' order by wkYear desc";
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)) {
		$yearList[$r['wkYear']] = $r['wkYear'];
		$baseList[$r['wkYear']] = $r['base'];
	}

	$year = (count($yearList) > 0) ? key($yearList) : '';
	$yearBase = (count($yearList) > 0) ? $baseList[key($baseList)] : '';


	$sType = 'depID';
	if(isset($_REQUEST['sType'])) $sType = $_REQUEST['sType'];
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	$temp = array();
	if($sType == 'depID') {
		if(!empty($_REQUEST['depFilter'])) $temp[] = "td._depID like '".$_REQUEST['depFilter']."%'";
		else if ($_SESSION['privilege'] <= 10) {	
			if($_SESSION['user_classdef'][2]) {
				$aa = array();
				foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="td._depID like '$v%'";
				$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
			} else $temp[] = "td._depID='-1'";
		}

	} elseif ($sType == 'projID') {	
		if(!empty($_REQUEST['projIDFilter'])) {
			$projList = explode(',', $_REQUEST['projIDFilter']);
			$aa = array();
			foreach($projList as $v) if($v) $aa[]="td._projID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 
		} elseif ($_SESSION['privilege'] <= 10) {
			if($_SESSION['user_classdef'][7]) {
				$aa = array();
				foreach($_SESSION['user_classdef'][7] as $v){
					if($v)	$aa[]="td._projID like '$v%'";	
				}
				$temp[] = "(".join(' or ',$aa).")"; 	
			} else {
				$temp[] = "td._projID='-1'";
			}
		}
	}

	if(!empty($_REQUEST['year'])){
		$year = $_REQUEST['year'];
		$yearBase = $baseList[$_REQUEST['year']];
	} 

	$t_filter = array();
	$t_filter[] = "h.rdate <='".$year."-12-31 23:59:59'";
	$t_filter[] = "a.wkYear='".$year."'";	

	if(count($temp)>0) $filter = join(' and ',$temp);

	// Here for List.asp ======================================= //


	$defaultOrder = "td.empID";
	$searchField1 = "b.empID";
	$searchField2 = "b.empName";
	$sortDirection = 'ASC';

	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	// $fnAry = explode(',',"empID,empName,depID,spHolidays,actDays,trueDays,action");
	// $ftAry = explode(',',"編號,姓名,部門,今年度特休,會計年資,實際年資,調整");
	// $flAry = explode(',',"50,,,,,,,,,");
	// $ftyAy = explode(',',"id,text,text,text,text,text,text,text,text");

	$fnAry = explode(',',"empID,empName,_projID,_depID,hireDate,isOnduty,stopRange,leaveDate,actDays,jobID,jobClass,salary,bBase,score,base,sRatio,bonus,bTax,otherDeduct,otherDeductNotes,money,isPay,bNotes");
	$ftAry = explode(',',"員編,姓名,計畫編號,部門,就職日期,職況,留停日期,離職日期,服務年資,職稱,工時身份,本薪,核定基數,考核等級,考核基數,服務比例,年終獎金,扣稅金額,額外扣除,額外扣除原因,實發金額,發放,備註");
	$flAry = explode(',',"50,,,,,,,,,,,,,,,,,,,,,,");
	$ftyAy = explode(',',"text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text");

	$gradeList = array('grade4'=>'優','grade3'=>'佳','grade2'=>'可','grade1'=>'差');
?>