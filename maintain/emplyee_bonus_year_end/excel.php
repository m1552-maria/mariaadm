<?php  
  session_start();
  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');
  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
    
  $list = array();
  $sql = "select td.* from(select t.* from (select h.projID _projID, h.depID _depID, a.*,a.tax as bTax, a.notes as bNotes,  e.hireDate,e.empName from emplyee_bonus_year_end as a left join emplyee as e on e.empID=a.empID left join emplyee_history h on e.empID=h.empID where a.isPay = '1' and a.isPayHand = '1' and e.empID <> '00001' and e.empID <> '99998' and a.wkYear='".$_REQUEST['year']."' and h.rdate <='".$_REQUEST['year']."-12-31 23:59:59' order by a.projID ASC, e.empID ASC, h.rdate desc limit 10000000000000) as t group by t.empID) as td ";

  if(!empty($_REQUEST['projID'])) {
      $pList = explode(',', $_REQUEST['projID']);
      $aa = array();
      foreach ($pList as $key => $v) {
        if($v)  $aa[]="td._projID like '$v%'";  
      }
      $sql .= " where (".join(' or ',$aa).")";   

    } elseif ($_SESSION['privilege'] <= 10) {
      if($_SESSION['user_classdef'][7]) {
        $aa = array();
        foreach($_SESSION['user_classdef'][7] as $v){
          if($v)  $aa[]="td._projID like '$v%'";  
        }
        $sql .= " where (".join(' or ',$aa).")";   
      } else {
        $sql .=  " where td._projID='-1'";
      }
    }

 $sql .= " order by td._projID asc, td._depID asc, td.empID asc"; 


  $rs=db_query($sql);
  if(!db_eof($rs)){
    while($r=db_fetch_array($rs)){//還要分類別
      if($r['_projID']!=''){
        $projID = explode(',', $r['_projID']);
        foreach($projID as $k=>$v){
          $list[$v][$r['_depID']]['wkYear'] = $r['wkYear'];
          $list[$v][$r['_depID']]['sub_list'][$r['empID']] = $r;
        }
      }else{
        $list['no_proj'][$r['_depID']]['wkYear'] = $r['wkYear'];
        $list['no_proj'][$r['_depID']]['sub_list'][$r['empID']] = $r;
      }
    }
  }

 /* echo "<pre>";
  print_r($list);
  exit;*/


  $now_date_time = date('Y-m-d H:i:s');
  $lastYear=intval(date('Y')-1);

  if($_REQUEST['printType'] == 'excel') { 
      $filename = '年終獎金清冊'.date("Y-m-d H:i:s",time()).'.xlsx';
  
      header("Content-type:application/vnd.ms-excel");
      header("Content-Disposition:attachment;filename=$filename");
      header("Pragma:no-cache");
      header("Expires:0");

      $objPHPExcel = new PHPExcel(); 
      $objPHPExcel->setActiveSheetIndex(0);
      $sheet = $objPHPExcel->getActiveSheet();

      //跑header 迴圈用
      $header = array('A','B','C','D','E','F','G','H','I','J');
      $headervalue = array('流水號','員工編號','姓名','到職日','月薪資','服務比例','年終獎金','所得稅5%','其他扣除','實領匯款金額');

      $i=1;
      $number = 0;
      $sum = 0;//合計
      $sum_bonus = 0;
      $sum_other = 0;
      $sum_tax = 0;
      $final = 0; //為了最後一筆
      $final1 = 0;
      foreach($list as $k=>$v){
        $final++;
        $final1=0;
        foreach($v as $k1=>$v1){
          $final1++;
          //頭
          $sheet->mergeCells('A'.$i.':J'.$i);//合併
          $sheet->setCellValueExplicit('A'.$i,'財團法人瑪利亞社會福利基金會');
          $sheet->getStyle('A'.$i.':J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
          $i++;
          $sheet->mergeCells('A'.$i.':J'.$i);
          $sheet->setCellValueExplicit('A'.$i,($v1['wkYear']-1911).'年終獎金清冊');
          $sheet->getStyle('A'.$i.':J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
          $i++;
          $sheet->mergeCells('A'.$i.':B'.$i);//合併
          if($k == 'no_proj') $sheet->setCellValueExplicit('A'.$i,'計畫編號:無');
          else $sheet->setCellValueExplicit('A'.$i,'計畫編號:'.$k);
          $sheet->setCellValueExplicit('H'.$i,'印製日期：'.$now_date_time);
          $sheet->getStyle('H'.$i)->getAlignment()->setWrapText(true);//要有這個才能換行
      
      
          $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
          $i++;
          $sheet->mergeCells('A'.$i.':B'.$i);//合併
          $sheet->setCellValueExplicit('A'.$i,'部門名稱:'.$departmentinfo[$k1]);
          $sheet->setCellValueExplicit('H'.$i,'年資結算止日:'.$v1['wkYear'].'-12-31');
          $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
          $i++;
          //表頭
          foreach($header as $k3=>$v3){
            $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
            $sheet->getStyle($v3.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValueExplicit($v3.$i,$headervalue[$k3]);
          }
          $sheet->getStyle('A'.$i.':J'.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
          $i++;

          //需要用到的變數
          $sub_sum = 0;//小計
          $sub_other = 0;//其他
          $sub_bonus = 0;//小計
          $sub_tax = 0;//小計
          $people = 0;//人數

          //員工列表
          foreach($v1['sub_list'] as $k2=>$v2){
            if($v2['bonus'] == -1) $v2['bonus'] = 0;
            $number++;//跟總人數一樣
            $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
            $sheet->setCellValueExplicit('A'.$i,$number);
            $sheet->setCellValueExplicit('B'.$i, $v2['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
            $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValueExplicit('C'.$i,$v2['empName']);
            $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValueExplicit('D'.$i,date("Y-m-d",strtotime($v2['hireDate'])));
            $sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValueExplicit('E'.$i,$v2['salary']);
            $sheet->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValueExplicit('F'.$i,$v2['sRatio']);
            $sheet->getStyle('F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValueExplicit('G'.$i,$v2['bonus']);
            $sheet->setCellValueExplicit('H'.$i,$v2['bTax']);
            $sheet->setCellValueExplicit('I'.$i,$v2['otherDeduct']);
            $sheet->setCellValueExplicit('J'.$i,$v2['money']);
            //需要加總的
            $sub_sum += $v2['money'];
            $sum += $v2['money'];

            $sub_bonus += $v2['bonus'];
            $sum_bonus += $v2['bonus'];

            $sub_tax += $v2['bTax'];
            $sum_tax += $v2['bTax'];

            $sub_other += $v2['otherDeduct'];
            $sum_other += $v2['otherDeduct'];


            $people++;
            $i++;
          }

          $sheet->setCellValueExplicit('B'.$i,'小計');
          $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->setCellValueExplicit('C'.$i,$people.'人');
          $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->setCellValueExplicit('D'.$i,'');
          $sheet->setCellValueExplicit('E'.$i,'0');
          $sheet->setCellValueExplicit('F'.$i,'0');
          $sheet->setCellValueExplicit('G'.$i,$sub_bonus);
          $sheet->setCellValueExplicit('H'.$i,$sub_tax);
          $sheet->setCellValueExplicit('I'.$i,$sub_other);
          $sheet->setCellValueExplicit('J'.$i,$sub_sum);
          $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
          $sheet->getStyle('A'.$i.':J'.$i)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
          if($final == count($list) && $final1==count($v)){
            $i++;
            //最後
            $sheet->setCellValueExplicit('B'.$i,'總計');
            $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValueExplicit('C'.$i,$number.'人');
            $sheet->setCellValueExplicit('D'.$i,'');
            $sheet->setCellValueExplicit('E'.$i,'');
            $sheet->setCellValueExplicit('F'.$i,'');
            $sheet->setCellValueExplicit('G'.$i,$sum_bonus);
            $sheet->setCellValueExplicit('H'.$i,$sum_tax);
            $sheet->setCellValueExplicit('I'.$i,$sum_other);
            $sheet->setCellValueExplicit('J'.$i,$sum);
            $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);
          }
          $i = $i+3;

          $sheet->setCellValueExplicit('A'.$i,'經辦:');
          $sheet->setCellValueExplicit('E'.$i,'機構/中心主管:');
          $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);

          $i = $i+4;

          $sheet->setCellValueExplicit('A'.$i,'覆核經辦:');
          $sheet->setCellValueExplicit('E'.$i,'覆核主管:');
          $sheet->setCellValueExplicit('H'.$i,'執行長:');
          $sheet->getStyle('A'.$i.':J'.$i)->getFont()->setSize(12);


          $i = $i+4;
          
        }

      }



      //設定欄寬
      $width = array(10,15,15,15,10,10,10,15,15,20);
      foreach($header as $k=>$v){
        $sheet->getColumnDimension($v)->setWidth($width[$k]);  
      }
      
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
      $objWriter->setPreCalculateFormulas(false);
      $objWriter->save('php://output');

  } elseif($_REQUEST['printType'] == 'pdf') {
      include_once '../../mpdf/mpdf.php';
      $mpdf = new mPDF('UTF-8','A4-L','','DFKai-sb',10,10,10,10);
      $mpdf->useAdobeCJK = true;
      $mpdf->SetAutoFont(AUTOFONT_ALL);
      $mpdf->SetDisplayMode('fullpage');
      $html = file_get_contents('pdfHtml.html');
      $_html = array();

      $header = array('流水號','員工編號','姓名','到職日','年終獎金','所得稅5%','其他扣除','實領匯款金額');

      $number = 0;
      $sum = 0;//合計
      $sum_bonus = 0;
      $sum_tax = 0;
      $sum_other = 0;
      $final = 0; //為了最後一筆
      $final1 = 0;
      
      //頭
      $_html[] = '<h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3>';
      $_html[] = '<h3 style="text-align: center;">'.($_REQUEST['year']-1911).'年終獎金清冊</h3>';
      $br = '<tr class="trBr"><td class="cLabel" colspan="'.count($header).'"><br></td></tr>';
      foreach($list as $k=>$v){
        $final++;
        $final1=0;
        foreach($v as $k1=>$v1){
          $final1++;
          $_html[] = '<table class="tableData" width="1250px;" border="1">';
          $projText = ($k == 'no_proj') ? '計畫編號:無' : '計畫編號:'.$k;
          $_html[] = '<tr><td colspan="3">'.$projText.'</td><td colspan="3">部門名稱'.$departmentinfo[$k1].'</td><td colspan="2">年資結算止日:'.$v1['wkYear'].'-12-31</td></tr>';
          $_html[] = '<tr>';

          //表頭
          foreach($header as $k3=>$v3){
            $_html[] = '<td class="cLabel">'.$v3.'</td>';
          }
          $_html[] = '</tr>';

          //需要用到的變數
          $sub_sum = 0;//小計
          $sub_bonus = 0;//小計
          $sub_oher = 0;
          $sub_tax = 0;//小計
          $people = 0;//人數

          //員工列表
          foreach($v1['sub_list'] as $k2=>$v2){
            $_html[] = '<tr>';
            if($v2['bonus'] == -1) $v2['bonus'] = 0;
            $number++;//跟總人數一樣
            
            $_html[] = '<td class="cLabel">'.$number.'</td>';
            $_html[] = '<td class="cLabel">'.$v2['empID'].'</td>';
            $_html[] = '<td class="cLabel">'.$v2['empName'].'</td>';
            $_html[] = '<td class="cLabel">'.date("Y-m-d",strtotime($v2['hireDate'])).'</td>';
            $_html[] = '<td style="text-align:right">'.number_format($v2['bonus']).'</td>';
            $_html[] = '<td style="text-align:right">'.number_format($v2['bTax']).'</td>';
            $_html[] = '<td style="text-align:right">'.number_format($v2['otherDeduct']).'</td>';
            $_html[] = '<td style="text-align:right">'.number_format($v2['money']).'</td>';

            //需要加總的
            $sub_sum += $v2['money'];
            $sum += $v2['money'];

            $sub_bonus += $v2['bonus'];
            $sum_bonus += $v2['bonus'];

            $sub_tax += $v2['bTax'];
            $sum_tax += $v2['bTax'];

            $sub_other += $v2['otherDeduct'];
            $sum_other += $v2['otherDeduct'];

            $people++;
            $_html[] = '</tr>';
          }
          $_html[] = '<tr><td></td><td class="cLabel">小計</td><td class="cLabel">'.$people.'人</td><td></td><td style="text-align:right">'.number_format($sub_bonus).'</td><td style="text-align:right">'.number_format($sub_tax).'</td><td style="text-align:right">'.number_format($sub_other).'</td><td style="text-align:right">'.number_format($sub_sum).'</td></tr>';

          if($final == count($list) && $final1==count($v)){
            //最後
            $_html[] = '<tr><td></td><td class="cLabel">總計</td><td class="cLabel">'.$number.'人</td><td></td><td style="text-align:right">'.number_format($sum_bonus).'</td><td style="text-align:right">'.number_format($sum_tax).'</td><td style="text-align:right">'.number_format($sum_other).'</td><td style="text-align:right">'.number_format($sum).'</td></tr>';
          }
 
          /*$_html[] = $br;
          $_html[] = $br;
          $_html[] = '<tr><td>經辦:</td><td colspan="2"></td><td>機構/中心主管:</td><td colspan="3"></td></tr>';
          $_html[] = '<tr><td>覆核經辦:</td><td colspan="2"></td><td>覆核主管:</td><td></td><td></td><td>執行長:</td><td></td></tr>';*/
          $_html[] = '</table><br><br>';

      
        }

      }
      $_html[] = '<table class="tableData" width="1250px;" border="1">';
      $_html[] = '<tr><td style="width:10%;" class="cLabel">經辦:</td><td colspan="2" style="width:18%;" class="cLabel"></td><td style="width:13%;" class="cLabel">機構/中心主管:</td><td colspan="4"  style="width:22%;" class="cLabel"></td></tr>';
      $_html[] = '<tr><td class="cLabel">覆核經辦:</td><td colspan="2" class="cLabel"></td><td class="cLabel">覆核主管:</td><td colspan="2" class="cLabel"></td><td style="width:10%;" class="cLabel">執行長:</td><td style="width:22%;" class="cLabel"></td></tr>';
      $_html[] = '</table><br><br>';

      $_html = implode('', $_html);
      $html = str_replace('{#html#}', $_html, $html);
        //print_r($html);
          $mpdf->SetWatermarkText('MARIA');
          $mpdf->showWatermarkText = true;
          $mpdf->SetHTMLFooter('<p style="text-align:right;">{PAGENO}</p>');
          $mpdf->WriteHTML($html);
          $mpdf->Output();

  }
  
  
 
?>