<?php
	$sql="select base from bonus_festival where type='year_end' and wkYear = '".$year."'";
	$rs = db_query($sql);
	$bonusData=db_fetch_array($rs);

	function stohis($s){
		$year = intval($s/31536000);
		$month = intval(($s - ($year*31536000))/2592000);
		$day = intval(($s - ($year*31536000)-($month*2592000))/86400);
		return $year.'年'.$month.'月'.$day.'日';
	}

	$sql = "select td.* from (select t.* from (select a.*,a.tax as bTax, a.notes as bNotes, b.empName, h.projID _projID, h.depID _depID,b.hireDate,b.isOnduty,b.leaveDate,b.jobID,b.jobLevel from $tableName ";
	if(count($t_filter)>0){
		$sql.=" where ".implode(' and ', $t_filter);
	}
	$sql.="order by b.empID ASC, h.rdate desc limit 10000000000000) as t group by t.empID ) as td ";
	if(isset($filter)){
		$sql.=" where ".$filter;
	}

	$sqlExcel = $sql;
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";

	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  

?> 
<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?php echo $pageTitle?></title>
 <link rel="stylesheet" href="<?php echo $extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link href="tuning.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script type="text/javascript" src="tuning.js" ></script> 
 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?php echo $pages?>;		//目前所在頁次
	var selid = '<?php echo $selid?>';	
	var keyword = '<?php echo $keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?php echo $pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?php echo $pages?>+1; if(pp><?php echo $pageCount?>) pp=<?php echo $pageCount?>; break;
      case 3 : pp=<?php echo $pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var url = "<?php echo $PHP_SELF?>?pages="+pp+"&keyword=<?php echo $keyword?>&fieldname=<?php echo $fieldname?>&sortDirection=<?php echo $sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(yrflt) url = url+'&year='+yrflt;

		location.href = url;		
	}

	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}


 </script>
 <script Language="JavaScript" src="<?php echo $extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?php echo $pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?php echo $pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
   
   <select name="year" id="year" onChange="doSubmit()">
    	<?php foreach ($yearList as $key => $value) {
    		$sel = ($year == $key) ? 'selected':'';
    	 	 echo "<option $sel value='".$key."'>".$value." 年</option>"; 
    	} ?>
    	
    </select>

    <select name="sType" onChange="filterSubmit(this)">
    	<option value="depID" <?php if($sType=='depID') echo 'selected'?>>依部門</option>
    	<option value="projID" <?php if($sType=='projID') echo 'selected'?>>依計劃</option>
    </select>
<?php if($sType == 'depID') { ?>    
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>全部部門</option>
		<?php 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST['depFilter']==$k?' selected ':'').">$v</option>";
      } else {
		foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST['depFilter']==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>
<?php } elseif($sType == 'projID') { ?>

    <?php if($_SESSION['privilege'] <= 10) { ?>
		<select name="projIDFilter" id="projIDFilter" onChange="filterSubmit(this)"><option value=''>-全部計畫-</option>
			<?php 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo "<option value='$v'".(isset($_REQUEST['projIDFilter']) && ($_REQUEST['projIDFilter']==$v) ? ' selected ':'').">$v</option>";
	    	?>
	    </select>
	<?php } else { ?>
		<input type="text" name="projIDFilter" value="<?php echo (isset($_REQUEST['projIDFilter'])) ? $_REQUEST['projIDFilter'] : '';?>" placeholder="ex:30,31">
		<input type="button" value="確定" onclick="filterSubmit(this)">
	<?php } ?>
<?php } ?>
    
 
    <input type="button" value="匯出詳細資料" onclick="excelfm2.submit()">
    <input type="button" value="匯出年終清冊" onclick="getExcelV();">
    
  	</td>
  	<td align="right"><?php $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?php echo $ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<?php for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>

		<?php echo $ftAry[$i]?>
	</th>
	<?php } ?>
  </tr>
<?php
	 //echo $sql; //exit;
  	$rs = db_query($sql);

	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?php echo $id?>" onMouseOver="SelRowIn('<?php echo $id?>',this)" onMouseOut="SelRowOut('<?php echo $id?>',this)" onClick="SelRow('<?php echo $id?>',this)"<?php if($id==$selid) echo ' class="onSel"'?>>
    <td <?php if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><?php echo $id?></td>
		<?php for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <?php if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<?php $fldValue = (isset($r[$fnAry[$i]])) ? $r[$fnAry[$i]] : ''; ?>
			<?php switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:i',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						case "empID" : echo $emplyeeinfo[$fldValue]; break;
						default : 
							switch ($fnAry[$i]) {
								case '_depID':
									echo (isset($departmentinfo[$fldValue])) ? $departmentinfo[$fldValue] : '';		
									break;
								case 'jobID':
									echo (isset($jobinfo[$fldValue])) ? $jobinfo[$fldValue] : '';	
									break;
								case 'actDays':
									echo stohis($fldValue*24*60*60);//會計年資
									break;
								case 'score':
									echo (isset($gradeList[$fldValue])) ? $gradeList[$fldValue] : '';	
									break;
								case 'bonus':
									echo ($fldValue == -1) ? '' : (int)$fldValue;	
									break;
								case 'bBase':
									echo (!empty($r['isAssess'])) ? $bonusData['base'] : '';
								case 'isOnduty':
									echo (isset($jobStatus[$fldValue])) ? $jobStatus[$fldValue] : '';
									break;
								case 'leaveDate':
									if($r['isOnduty'] == 1 || $r['isOnduty'] == 2) echo '';
									break;
								case 'isPay':
									if($r['isPay']!=1 || $r['isPayHand'] !=1) echo "否";
									else echo $r['isPay'];
									break;
								default:
									echo $fldValue;	
									break;
							}
							break;
			    } 
			?>
			</td>
		<?php } ?>	
  </tr>    
<?php } ?> 

 </table>
</form>

<form name="excelfm" action="excel.php" method="post" target="excel">
	<input type="hidden" name="sql" value="<?php echo $sqlExcel;?>">
	<input type="hidden" name="projID" value="">
	<input type="hidden" name="year" value="">
	<input type="hidden" name="printType" value="">
</form>

<form name="excelfm2" action="excelDetail.php" method="post" target="excel">
	<input type="hidden" name="sql" value="<?php echo $sqlExcel;?>">
	<input type="hidden" name="year" value="<?php echo $year;?>">

</form>

<script type="text/javascript">
	var rztExcel = false;
	function getExcelV(){
		var url = 'selectExcel.php?'; 
        if(rztExcel){
        	rztExcel.close();
        }
        url += 'year='+$('#year').val();
        if(rztExcel == false){
        	rztExcel = window.open(url, "web",'width=600,height=750');
        }else{
        	rztExcel = window.open(url, "web",'width=600,height=750');
        	rztExcel.focus();
        }
	}
</script>


</body>
</Html>