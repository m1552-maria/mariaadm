<?php
	@session_start();
	header("Content-type: text/html; charset=utf-8");
	if($_REQUEST['Submit']) {
		include 'init.php';
		//處理上傳的檔案
		if ($_FILES[file1][tmp_name] != "") {
			$fsrc = $ulpath.'import.xls';
			if (!copy($_FILES[file1][tmp_name],$fsrc)) { //上傳失敗
				echo '附件上傳作業失敗 !'; exit;
			}
		}		
		
		require_once '../Excel/reader.php';  
		//建立excel檔的物件
		$data = new Spreadsheet_Excel_Reader();  
		//設定輸出編碼，指的是從excel讀取後再進行編碼
		$data->setOutputEncoding('UTF-8');  
		//載入要讀取的檔案
		$data->read($fsrc);  
		
		//if exists override tableName
		if($_REQUEST['tbName']) $tableName = $_REQUEST['tbName'];
		$announcer = $_REQUEST['announcer'];
		$PID = "";
		switch($_REQUEST[evtobj]) {
			case 'center': $PID = $_REQUEST[depCenter]; break;
			case 'dep': $PID = join(',',$_SESSION[user_classdef][2]); break;
			case 'all': $PID = "0";
		}
		
		$LogTX = "";		
		//下面範例則是先讀取欄位再讀取列，因此i代表列的數目，j則代表欄位的數目
		for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
			//如下圖因為excel的表格第一列都會寫上欄位名稱，所以這邊預設不會讀取第一列
			if($i==1) {	//表格名稱
				continue;
			} else if($i==2) { //欄位名稱
				continue;
			} else {
				for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) { 
					$ba[0] = $data->sheets[0]['cells'][$i][1];	//title
					$ba[1] = $data->sheets[0]['cells'][$i][2];	//ClassID
					$ba[2] = $data->sheets[0]['cells'][$i][3];	//Content
					$ba[3] = $data->sheets[0]['cells'][$i][4];	//beginDate
					$ba[4] = $data->sheets[0]['cells'][$i][5];	//endDate
				}
				if($ba[0] && $ba[1]) {	//有資料
					$sql = "insert into $tableName(ClassID,title,Content,announcer,beginDate,endDate,PID) values('$ba[1]','$ba[0]','$ba[2]','$announcer','$ba[3]','$ba[4]','$PID')";
					//echo $sql;
					db_query($sql);
				}
			}
		}
		$LogTX .= "<p>Excel 匯入成功!<br>&gt;&gt; <a href='$_SERVER[HTTP_REFERER]'>返回</a></p>";  
		echo $LogTX;
	} 
?>
