<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	var filterFG = false;

	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var jobflt = $('select#jobFilter option:selected').val();
		var typflt = $('select#typFilter option:selected').val();
		var sttflt = $('select#sttFilter option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(jobflt) url = url+'&jobFilter='+jobflt;
		if(typflt) url = url+'&typFilter='+typflt;
		if(sttflt) url = url+'&sttFilter='+sttflt;
		location.href = url;
	}

	// function doAdvFilter() {
	// 	var obj = new Object();  
	// 	var retval = window.open("advFilter.php",obj,"dialogWidth=320px;dialogHeight=500px");  
	// 	if(retval == undefined) retval = window.returnValue;  
	// 	if(retval) {
	// 		location.href="list.php?advFilter="+retval;
	// 		//alert(retval); 
	// 	}
	// }

	$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<? if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[empID]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>

		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
	});	

	//處理進階搜尋
	var rzt = false;
	function doAdvFilter(aObj) {
		if(rzt){//處裡不要一直開視窗
			rzt.close();
			rzt = false;
		}
		if (rzt == false) {
	   		rzt = openBrWindow1('advFilter.php','',650,600);
	   	}else{
	   		rzt = openBrWindow1('advFilter.php','',650,600);
			rzt.focus();
	 	}
	}
	function openBrWindow1(theURL,inParam,win_width,win_height) { 
	  var PosX = (document.body.clientWidth-win_width)/2; 
	  var PosY = (document.body.clientHeight-win_height)/2; 
	  features = "width="+win_width+",height="+win_height+",top="+PosY+",left=-400,scrollbars=yes"; 
	  var newwin = window.open(theURL,'',features);
	  return newwin;
	}
	function returnValue(data) {//傳回來的值
		if(data) {
			location.href="list.php?advFilter="+data[0]+"&depFilter="+data[1]+"&sttFilter="+data[2]+"&jobFilter="+data[3]+"&typFilter="+data[4];
		}
	}

	var rztExcel = false;
	function getExcelV(){
        if(rztExcel){
        	rztExcel.close();
        }
        if(rztExcel == false){
        	rztExcel = window.open("selectExcel.php", "web",'width=600,height=750');
        }else{
        	rztExcel = window.open("selectExcel.php", "web",'width=600,height=750');
        	rztExcel.focus();
        }
	}

 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
    <? if(!$_REQUEST['advFilter']) { ?>
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <? } ?>
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
   	<select name="depFilter" id="depFilter"><option value=''>-全部部門-</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>
    <select name="sttFilter" id="sttFilter">
		<? $def = 1; if(isset($_REQUEST[sttFilter])) $def = $_REQUEST[sttFilter]; foreach($jobStatus as $k=>$v) echo "<option value='$k'".($def==$k?' selected ':'').">$v</option>" ?>
    </select>
    <select name="jobFilter" id="jobFilter"><option value=''>-全部職銜-</option>
			<? foreach($jobinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[jobFilter]==$k?' selected ':'').">$v</option>" ?>
    </select>
    <select name="typFilter" id="typFilter"><option value=''>--身分別--</option>
			<? foreach($jobType as $k=>$v) echo "<option value='$k'".($_REQUEST[typFilter]==$k?' selected ':'').">$v</option>" ?>
    </select>
    <input type="submit" name="action" value="確定" onClick="DoFilter()">
    <input type="button" value="進階搜尋" onClick="doAdvFilter()">
    <input type="button" value="匯出基本表雜項" onclick="excelfm.submit()">
  </td></tr>
  <tr>
   <? if(!$_REQUEST['advFilter']) { ?>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
   <? } else { ?>
	  <td><font size="2" color="gray">&lt;&lt;進階搜尋&gt;&gt; 如欲離開進階搜尋，請變更上方過濾條件</font></td>
		<td align="right" class="sText">總筆數：<?=$recordCt?> &nbsp;</td>   
   <? } ?>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="1" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//搜尋部門找出id
	$dep_list = array();
	$sql1 = "select depID,depName,leadID from department where `depName` like '%$keyword%'";
	$rs1 = db_query($sql1,$conn);
	while ($r1=db_fetch_array($rs1)) {
		$dep_list[$r1['depID']] = $r1['depID'];
	}
	$dep_list = implode("','", $dep_list);


	//過濾條件
	$sql = "select * from $tableName"; 
	if(isset($filter)){
		$filter = explode(",",$filter);
		if($filter[1]){
			$sql .= " left join emplyee_education edu on(emplyee.empID = edu.empID) where ";
			if(!empty($filter[0])) $sql .= "(".$filter[0]." AND edu.".$filter[1].")";
			else $sql .= "(edu.".$filter[1].")";
		}else $sql .= " where ($filter[0])";
	}else $sql .= " where 1";
	
  // 搜尋處理
  if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 IN ('$dep_list')))";
  $sqlExcel = $sql;
  // 排序處理
	if($filter[1]){
		if ($fieldname==""){
			$sql.=" order by emplyee.$defaultOrder $sortDirection";
		}else $sql .=" order by emplyee.$fieldname $sortDirection";
	}else{
		if ($fieldname==""){
			$sql.=" order by $defaultOrder $sortDirection";
		}else $sql .=" order by $fieldname $sortDirection";
	}
	//分頁

  if(!$_REQUEST['advFilter']) $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>><a href="details.php?empID=<?=$r[empID]?>&keyword=<?=$_REQUEST[keyword]?>&depFilter=<?=$_REQUEST[depFilter]?>&sttFilter=<?=$_REQUEST[sttFilter]?>&jobFilter=<?=$_REQUEST[jobFilter]?>&typFilter=<?=$_REQUEST[typFilter]?>" target="mainFrame"><?=$id?></a></td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo $fldValue&&$fldValue!='0000-00-00'?date('Y/m/d',strtotime($fldValue)):'&nbsp;'; break;
						case "deptitle" : echo $departmentinfo[$fldValue];	break;
						case "jobtitle" : echo $jobinfo[$fldValue];	break;
						default : echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>
<form id="excelfm" name="excelfm" action="excel.php" method="post" target="emplyeeExcel">
	<input type="hidden" name="sql" value="<?php echo $sqlExcel;?>">
</form>

</body>
</Html>