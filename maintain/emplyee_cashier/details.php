<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	include('../../getEmplyeeInfo.php');	
	$tabidx = $_SESSION['emplyee_tab']?$_SESSION['emplyee_tab']:0;
	$empID = $_REQUEST[empID];
	if($empID) { //修改
		$actfn = 'doedit.php?keyword='.$_REQUEST['keyword'].'&depFilter='.$_REQUEST['depFilter'].'&sttFilter='.$_REQUEST['sttFilter'].'&jobFilter='.$_REQUEST['jobFilter'].'&typFilter='.$_REQUEST['typFilter'];
		$rs = db_query("select * from emplyee where empID='$empID'");
		if($rs) $r=db_fetch_array($rs);
	} else { //新增
		$actfn = 'doappend.php?keyword='.$_REQUEST['keyword'].'&depFilter='.$_REQUEST['depFilter'].'&sttFilter='.$_REQUEST['sttFilter'].'&jobFilter='.$_REQUEST['jobFilter'].'&typFilter='.$_REQUEST['typFilter'];
	}

  //特休
  $emplyee_annual = 0;
  $sql1 = 'select * from `emplyee_annual` where `empID`='.$empID.' and `year`='.(date('Y'));
  $rs1 = db_query($sql1);
  if($rs) $r1=db_fetch_array($rs1);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta Http-Equiv="Cache-Control" Content="no-cache">
  <meta Http-Equiv="Pragma" Content="no-cache">
  <meta Http-Equiv="Expires" Content="0">
  <meta Http-Equiv="Pragma-directive: no-cache">
  <meta Http-Equiv="Cache-directive: no-cache"> 
  
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>
	$(function(){
		var n = TabbedPanels1.getCurrentTabIndex();
		if(n==0||n==2||n==3) {
			document.getElementById('Submit').style.display=''
			document.getElementById('Reset').style.display=''
		}else {
			document.getElementById('Submit').style.display='none'
			document.getElementById('Reset').style.display='none'
		}
		
		$('.TabbedPanelsTab').click(function(){
			var n=this.getAttribute('tabindex');
			if(n==0||n==2||n==3) {
				document.getElementById('Submit').style.display=''
				document.getElementById('Reset').style.display=''
			}else {
				document.getElementById('Submit').style.display='none'
				document.getElementById('Reset').style.display='none'
			}
		});
		
		//擴展iframe到最大
		var iframeH = $(window).height()-47;
		$('.TabbedPanelsContent>iframe:not(.iframe2)').height(iframeH);
	});
	
	function formvalid(tfm) {
		if(!tfm.empID.value) { alert('請輸入員工編號'); tfm.empID.focus(); return false; }
		if(!tfm.empName.value) { alert('請輸入員工姓名'); tfm.empName.focus(); return false; }
    if(!tfm.depID.value) { alert('請輸入員工所屬部門'); tfm.depID.focus(); return false; }
		if(tfm.isDonate.checked){
      if(tfm.donate.value<=0) { alert('捐款金額必須大於0'); return false; } 
    }	
    if((tfm.court.value > 0 && !tfm.courtSdate.value)) {
      alert('法院扣薪大於0 請輸入起(訖)日期'); tfm.depID.focus(); return false;
    }


		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	
	function setnail(n) {
		$('.tabimg').attr('src','../images/unnail.gif');
		$('#tab'+n).attr('src','../images/nail.gif');
		$.get('setnail.php',{'idx':n});
	}
  function runScript(e) {
    if (e.keyCode == 13) { return false;  }
  }

  function isDel(empID,cardNo,type){
    if(confirm('確定要變更嗎?')){
      //版本問題只能用這種的
      $.ajax({
        method: "POST",
        url: "api.php",
        data: { 'empID': empID,'type':type,'cardNo':cardNo},
        success: function(data){
          if(data!=''){
            alert(data);
          } 
          location.reload();
        }
      })
    }
  }
	</script>
<style type="text/css">
	body { margin: 0; }
	.card td,th{ padding:3px; }
	.card th{ background-color: #cd9696;}
	.tabCorner { border-radius: 0 10px 0 0; }
</style>
</head>

<body>
<form action="<?=$actfn?>" method="post" target="_parent" style="margin:0" onsubmit="return formvalid(this)">
<table width="100%" border="0">
  <tr>
    <td>
    <div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab tabCorner" tabindex="0">基本資料<img id="tab0" class="tabimg" src="../images/<?= $tabidx==0?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(0)" /></li>
       
      </ul>
      
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent" title="---基本資料---">
          <table width="100%" border="0" cellpadding="5" cellspacing="0" class="formTx">
            <tr>
              <td width='60'>編號</td><td ><input name="empID" type="text" id="empID" value="<?=$r[empID]?>" size="10" <?= $empID?'readonly class="readonlyField"':'' ?>/></td>
              <td width='50'>中文姓名</td><td width='220'><input name="empName" type="text" id="empName" size="10" value="<?=$r[empName]?>" readonly class="readonlyField"/></td>
              <td width='50'>部門</td><td><input name="depID" type="text" id="depID" size="10" value="<?=$departmentinfo[$r[depID]]?>" readonly class="readonlyField"/></td>
              <td width='80'>職銜</td><td><input name="jobID" type="text" id="jobID" size="10" value="<?=$jobinfo[$r[jobID]]?>" readonly class="readonlyField"/></td>
             
            </tr>
           
            
            <tr>
               <td>職況</td>
              <td>
                <input name="isOnduty" type="text" id="isOnduty" size="10" value="<?=$jobStatus[$r[isOnduty]]?>" readonly class="readonlyField"/></td>

                 <td>計畫編號</td>
              <td>
                <input name="projID" type="text" id="projID" size="10" value="<?=$r[projID]?>" readonly class="readonlyField"/></td>


              <td>薪資發放</td>
              <td>
                <select name="payType">
            <?php foreach($payTypeList as $key => $value) {
              $selected = ($key == $r['payType']) ? 'selected' : '';
            ?>
                <option value="<?=$key;?>" <?=$selected;?> ><?=$value;?></option>
            <?php } ?>
                </select>
              </td>
              
              <td>薪資帳號</td>
              <td>
                <select name='salaryBank' id='salaryBank'>
                  <option value="">--無--</option>
                <? foreach($bankID as $key => $value) echo "<option value='$key'".($r[salaryBank]==$key?'selected':'').">$value</option>"; ?>
                </select>
                <input name="salaryAct" type="text" id="salaryAct" value="<?=$r[salaryAct]?>" size="16" />
              </td>

               

            </tr>
            <tr>
              <td>扣所得稅</td>
                <td><input type='text' value='<?=$r[tax]?$r[tax]:0?>' id='tax' name='tax' size='8'/>(金額)</td>

              <td>固定捐款</td>
                <td><input name="isDonate" type="checkbox" id="isDonate" value="1" <?=$r[isDonate]?'checked':''?>/>
                捐款金額<input type='text' value='<?=$r[donate]?$r[donate]:0?>' id='donate' name='donate' size='8'/>
                </td>
              <td>餐費</td>
              <td><input type='text' value='<?=$r[mealFee];?>' id='mealFee' name='mealFee' size='8'/>(金額)</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>法院扣薪</td>
              <td>
                <div>
                  <input type='text' value='<?=$r[court];?>' id='court' name='court' size='8'/>(金額)
                 
                </div>
              </td>
              <td>法院扣薪日期</td>
              <td colspan="3">
                <input name="courtSdate" type="text" class="queryDate" id="courtSdate" value="<?=($r[courtSdate] != '') ? date('Y/m/d',strtotime($r[courtSdate])) : ''?>" size="7" maxlength="10" />~
                  <input name="courtEdate" type="text" class="queryDate" id="courtEdate" value="<?=($r[courtEdate] != '') ? date('Y/m/d',strtotime($r[courtEdate])) : ''?>" size="7" maxlength="10" />
              </td> 
              <td></td>
              <td></td>
            </tr>


          </table>
        </div>
        <div class="TabbedPanelsContent" title="---通訊資料---">
          <iframe width="100%" height="255"  frameborder="0" src="../community/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
        <div class="TabbedPanelsContent" title="---照片及密碼---">
        	<table class="formTx">
          	<tr valign="top">
            	<td><iframe width="260" height="220" frameborder="0" src="headpic.php?empID=<?=$empID?>"></iframe></td>
          		<td><table><tr><td>登入密碼：</td><td><input name="password" type="text" id="password" value="<?=$r[password]?>"/></td></tr>
              <tr><td>識別卡卡號：</td><td>
              <? if(isset($_SESSION['user_classdef'][2])){ //是不是從前台登入的 ?>
              <input name="cardNo" type="password" id="cardNo" value="<?=$r[cardNo]?>"/ onkeypress="return runScript(event);" disabled >
              <input name="cardNo_before" type="hidden" id="cardNo_before" value="<?=$r[cardNo]?>"/>
              <? } else { ?>
              <input name="cardNo" type="tel" id="cardNo" value="<?=$r[cardNo]?>"/ onkeypress="return runScript(event);"  onfocus="this.select()" style="ime-mode:disabled;-webkit-ime-mode::disabled;-moz-ime-mode:disabled;-o-ime-mode:disabled;-ms-ime-mode:disabled;">
              <input name="cardNo_before" type="hidden" id="cardNo_before" value="<?=$r[cardNo]?>"/>
              <input type="submit" name="button" id="button" value="確認變更卡號" />
              <?
                if(stripos($r[cardNo],'--')){
                  echo '<input type="button" id="button" value="恢復使用"  onclick="isDel('.$r[empID].','."'".$r[cardNo]."'".','."'".'reuse'."'".')" />';
                }else{
                  echo '<input type="button" id="button" value="暫停使用"  onclick="isDel('.$r[empID].','."'".$r[cardNo]."'".','."'".'isDel'."'".')" />';
                }
               } ?>
              <tr>
                <td style="vertical-align: top;">換卡紀錄:</td>
                <td>
                <?
                  echo '<div style="overflow-y:auto;height:160px;">';
                  $sql1 = 'select * from `emplyee_card_log` where `empID`="'.$empID.'" order by `rdate` desc';
                  $rs1 = db_query($sql1,$conn);
                  if(!db_eof($rs1)){
                    echo '<table width="100%" border="1" CELLPADDING="0" CELLSPACING="0" class="card"><tr><th>卡號</th><th>日期</th></tr>';
                    while ($r1=db_fetch_array($rs1)){
                      if(isset($_SESSION['user_classdef'][2])){//是不是從前台登入的
                        $r1['cardNo'] = substr_replace ( $r1['cardNo'], '****', 4);
                        echo '<tr><td>'.$r1['cardNo'].'</td> <td>'.$r1['rdate'].'</td></tr>';
                      }else{
                        echo '<tr><td>'.$r1['cardNo'].'</td> <td>'.$r1['rdate'].'</td></tr>';
                      }
                    }
                    echo '</table>';
                  }else{
                    echo 'No data';
                  }
                  echo '</div>';
                ?>
                </td>
              </tr>
              </table></td>  
        		</tr>
          </table>
        </div>
        <div class="TabbedPanelsContent" title="---權限設定---">
        	<table class="formTx">
            <tr valign="top">
            	<td width="120">
            		<input type="checkbox" name="canProject" value="1" <?= $r[canProject]?'checked':''?>/>建立新專案權限<br />
            		<input type="checkbox" name="canSchedule" value="1" <?= $r[canSchedule]?'checked':''?>/>部門管理權限<br />
                <input type="checkbox" name="canEditEmp" value="1" <?= $r[canEditEmp]?'checked':''?>/>修改員工資料權限
              </td>
              <td><iframe width="780" height="360" frameborder="0" src="priviege.php?eid=<?=$r[empID]?>"></iframe></td>
            </tr>
          </table>
        </div>
         <div class="TabbedPanelsContent" title="---經歷---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_experience/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---學歷---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_education/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---證照/著作---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_license/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---教育訓練---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_training/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---職況---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_status/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
         <div class="TabbedPanelsContent" title="---薪資異動---">
          <iframe width="100%" height="255" frameborder="0" src="../emplyee_salary/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
        <div class="TabbedPanelsContent" title="---勞健退---">
		    	<iframe width="100%" id="ifins" height="165" scrolling="no" frameborder="0" src="insurence.php?eid=<?=$r[empID]?>"  class='iframe2'></iframe>
          <iframe width="100%" height="180" scrolling="auto" frameborder="0" src="../emplyee_dependents/list.php?eid=<?=$r[empID]?>" class='iframe2'></iframe>
        </div>
        <div class="TabbedPanelsContent" title="---出勤表---">
          <iframe width="100%" height="508" frameborder="0" scrolling="auto" src="../emplyee_duty/list.php?eid=<?=$r[empID]?>"></iframe>
        </div>
      </div>
    </div>
    </td>
  </tr>
 <? if ($_SESSION['privilege']>10 || $_SESSION["canEditEmp"]) {	?>
  <tr>
    <td>
    	<input type="hidden" name="params" id="params" value="" />
    	<input type="submit" name="button" id="Submit" value="<?=$empID?'確定修改':'確定新增'?>" />
      <input type="reset" name="button2" id="Reset" value="重設" />
    </td>
  </tr>
 <? } ?> 
</table>
</form>
</body>
<script type="text/javascript">var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");TabbedPanels1.showPanel(<?=$tabidx?>);</script>
</html>