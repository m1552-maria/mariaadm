<?php 

  session_start();
  $filename = '基本表雜項清冊'.date("Y-m-d H:i:s",time()).'.xlsx';
    
  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
  require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);

  $sql = $_POST['sql']." order by projID asc, depID asc";
  $rs = db_query($sql);
  $data = array();
  while ($r=db_fetch_array($rs)) { 
    $r['month'] = date('Ym');
    $r['donate'] = ($r['isDonate'] == 1) ? $r['donate'] : 0;

    $now = strtotime(date('Y-m-d'));
    if($r['courtSdate'] == '') $r['court'] = 0;
    elseif($now < strtotime($r['courtSdate']) || ($r['courtEdate'] != '' && $now > strtotime($r['courtEdate']))) {
      $r['court'] = 0;
    }

    $r['total'] = $r['tax']+$r['donate']+$r['mealFee']+$r['court'];
    if($r['total'] != 0) $data[$r['projID']][$r['empID']] = $r;
  }

  $header = array(
    'A' =>array('field'=>'empID','title'=>'編號','width'=>'10'),
    'B' =>array('field'=>'empName','title'=>'姓名','width'=>'10'),
    'C' =>array('field'=>'month','title'=>'執行月份','width'=>'12'),
    'D' =>array('field'=>'tax','title'=>'扣所得稅','width'=>'12'),
    'E' =>array('field'=>'donate','title'=>'捐款','width'=>'10'),
    'F' =>array('field'=>'mealFee','title'=>'餐費','width'=>'10'),
    'G' =>array('field'=>'court','title'=>'法院代扣','width'=>'12'),
    'H' =>array('field'=>'total','title'=>'費用總額','width'=>'12'),
  );

  $i=1;
  $sheet = $objPHPExcel->getActiveSheet();
  $sheet->mergeCells('A'.$i.':H'.$i);//合併
  $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
  $sheet->getStyle('A'.$i.':H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(14);

  $countList = array();
  foreach ($header as $kk => $vv) {
    $countList[$kk] = array(
      'count'=>0,
      'allCount'=>0
    );
  }

  foreach ($data as $key => $value) {
    $i++;
    $sheet->mergeCells('A'.$i.':H'.$i);//合併
    $sheet->setCellValue('A'.$i,'計劃編號：'.$key);
    $i++;
    foreach ($header as $kk => $vv) {
      $sheet->setCellValue($kk.$i, $vv['title']);
      $sheet->getColumnDimension($kk)->setWidth($vv['width']);
      $countList[$kk]['count'] = 0;
    }
    $sheet->getStyle('A'.$i.':H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    foreach ($value as $k2 => $v2) {

      $countList['B']['count']++;
      $countList['B']['allCount']++;
      $i++;
      foreach ($header as $kk => $vv) {
        if($kk == 'A')$sheet->setCellValueExplicit($kk.$i, $v2[$vv['field']], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
        else {
          switch ($vv['field']) {
            case 'tax':
            case 'donate':
            case 'mealFee':
            case 'court':
            case 'total':
              $sheet->getCell($kk.$i)->setValueExplicit($v2[$vv['field']], PHPExcel_Cell_DataType::TYPE_NUMERIC);
              break;
            
            default:
              $sheet->setCellValue($kk.$i, $v2[$vv['field']]);
              break;
          }
        }
        if($vv['field'] != 'empName' ) {
          $countList[$kk]['count']+=$v2[$vv['field']];
          $countList[$kk]['allCount']+=$v2[$vv['field']];
        }
      }
    }
    $sheet->getStyle('A'.($i-$countList['B']['count']).':H'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
    $i++;
    foreach ($header as $kk => $vv) {
      $title = '';
      switch ($kk) {
        case 'A':
          $title = '小計：';
          $sheet->setCellValue($kk.$i, $title);
          break;
        case 'B':
          $title = $countList['B']['count'].'人';
          $sheet->setCellValue($kk.$i, $title);
          break;
        case 'C':
          break;
        default:
          $title = $countList[$kk]['count'];
          $sheet->getCell($kk.$i)->setValueExplicit($title, PHPExcel_Cell_DataType::TYPE_NUMERIC);
          break;
      }
    }
    $sheet->getStyle('A'.$i.':H'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'82D900'))); 
    $i++;
  }
  $i++;
  //$sheet->setCellValue($kk.$i, $title);
  foreach ($header as $kk => $vv) {
    $title = '';
    switch ($kk) {
      case 'A':
        $title = '總計：';
        $sheet->setCellValue($kk.$i, $title);
        break;
      case 'B':
        $title = $countList['B']['allCount'].'人';
        $sheet->setCellValue($kk.$i, $title);
        break;
      case 'C':
        break;
      default:
        $title = $countList[$kk]['allCount'];
        $sheet->getCell($kk.$i)->setValueExplicit($title, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        break;
    }
  }
  $sheet->getStyle('A2:H'.$i)->getFont()->setSize(12);
  $sheet->getStyle('A'.$i.':H'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'FF9224'))); 

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');
?>