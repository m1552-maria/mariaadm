<?	
	//::for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	$bE = true;	//異動權限
	$pageTitle = "首頁輪撥圖片";
	$tableName = "newspic";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../'.$newspic_Path;
	$delField = 'purl';
	$delFlag = true;
	
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "title";
	$target = array('_blank'=>'新開視窗','_self'=>'本視窗');
	
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,RDate,purl,link,previge");
	$ftAry = explode(',',"編號,名稱,登錄日期,圖檔,連結,次序");
	$flAry = explode(',',"60,,,,,50");
	$ftyAy = explode(',',"ID,text,date,image,text,text");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,purl,link,previge");
	$newftA = explode(',',"名稱,圖檔,連結,次序");
	$newflA = explode(',',"60,60,60,");
	$newfhA = explode(',',",,,");
	$newfvA = array('','','','');
	$newetA = explode(',',"text,file,text,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,purl,link,previge");
	$editftA = explode(',',"編號,名稱,圖檔,連結,次序");
	$editflA = explode(',',",60,60,60,");
	$editfhA = explode(',',",,,,");
	$editfvA = array('','','','','');
	$editetA = explode(',',"text,text,file,text,text");
?>