<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");

	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "保險系統";
	$tableName = "insurances";
	$bE = true;	//異動權限
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/insurances/';
	$delField = '';				//用ID存檔，沒有欄位
	$delFlag = true;
	$imgpath = '../../data/insurances/';
	
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[aType]) $temp = "aType='$_REQUEST[aType]'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][3]) {
			$aa = array();
			$ar = explode(',',$_SESSION['user_classdef'][3]);
			foreach($ar as $v) $aa[] = "aType='$v'";
			$temp = join(' or ',$aa); 
		} else $temp = "aType='insure_travel'";		
	}
	if($_REQUEST[bType]) {
		if($_REQUEST[bType]=='1') $temp .= " and status is null";
		else if($_REQUEST[bType]=='2') $temp .= " and status is not null";
	}
	$filter = $temp;
		
	//::	----------------------------------------------------------------------------------- <
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "title";
	$pageSize = 10;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,allFee,status,createMan,rDate,aType");
	$ftAry = explode(',',"編號,名稱,總保費,辦理情形,建立者,登錄時間,保單種類");
	$flAry = explode(',',"50,,,,,,,");
	$ftyAy = explode(',',"ID,text,text,text,text,date,aType");	
?>