<?php
  @session_start();
  $permit=false;
  
	include '../inc_vars.php';
	include '../../config.php';
  include('../../getEmplyeeInfo.php');	
  
	$tabidx = $_SESSION['insurances_tab']?$_SESSION['insurances_tab']:0;
	$id = $_REQUEST['id'];
	$act = $_REQUEST['act'];
	if($id) { //修改
		if($act=='續約') $actfn = 'dorenew.php'; else $actfn = 'doedit.php';
		$rs = db_query("select * from insurances where id=$id");
    if($rs) $r=db_fetch_array($rs);
    if($r['createMan']==$_SESSION["empID"]) $permit=true;       //本人
    $crtLead = $departmentlead[$emplyeedepID[$r['createMan']]]; //建立者所屬主管
    if($crtLead==$_SESSION['empID']) $permit=true;
    if( strpos($_SESSION['user_classdef'][3],'insure_travel')!== false) $permit=true;

		$aType = $r[aType];
		$rs2 = db_query("select * from $aType where fid=$id");
		if($rs2) $r2=db_fetch_array($rs2);
	} else { //新增
		$actfn = 'doappend.php';
    $aType = $_REQUEST['aType'];
    if($_REQUEST['sid']) { //活動帶入模式
      $sid = $_REQUEST['sid'];
      $rs = db_query("select title,beginDate,endDate from schedule where ID=$sid");
      $r = db_fetch_array($rs);
    }
    $r2=array(
      'sid'=>$sid, 'bDate'=>$r['beginDate'], 'eDate'=>$r['endDate'],
      'contactMan'=>$emplyeeinfo[$_SESSION["empID"]],
      'applyDep'=>$departmentinfo[$_SESSION["depID"]] );
  }

  //文件管理權限 本人、後台、所屬部門主管
  if($_SESSION['privilege'] > 200) $permit=true;    
  if(!$aType && !$id) exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
	<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">  
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
	<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>  
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>  
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>
	function formvalid(tfm) {
		<? if($aType=='insure_travel') { ?>
		if(!tfm.title.value) tfm.title.value = '未設定';
		<? } ?>
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	function setnail(n) {
		$('.tabimg').attr('src','../images/unnail.gif');
		$('#tab'+n).attr('src','../images/nail.gif');
		$.get('setnail.php',{'idx':n});
	}	
	</script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
</style>
</head>

<body>
<form action="<?=$actfn?>" method="post" target="_parent" style="margin:0" onsubmit="return formvalid(this)">
<table width="100%" border="0">
  <tr>
    <td>
    <div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0">基本資料 <img id="tab0" class="tabimg" src="../images/<?= $tabidx==0?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(0)" /></li>
        <li class="TabbedPanelsTab" tabindex="1"><?=$insTbl[$aType]?>-保單資料 <img id="tab1" class="tabimg" src="../images/<?= $tabidx==1?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(1)" /></li>
        <? if($id && $permit) { ?>
        <li class="TabbedPanelsTab" tabindex="2">相關文件 <img id="tab2" class="tabimg" src="../images/<?= $tabidx==2?'nail.gif':'unnail.gif'?>" align="absmiddle" onclick="setnail(2)" /></li>
        <? } ?>
      </ul>
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent">
          <table width="100%" border="0" cellpadding="4" cellspacing="0" class="formTx">
            <tr>
              <td align="right">保單名稱：</td><td colspan="3"><input name="title" type="text" id="title" size="60" value="<?=$r['title']?>" /></td>
              <td align="right">總保費：</td>
              <td><input name="allFee" type="text" id="allFee" size="10" value="<?=($r['allFee']?$r['allFee']:0)?>"/></td>
            </tr>
            
            <tr>
              <td align="right" valign="top">說明：</td>
              <td colspan="3" valign="top"><textarea name="description" id="description" rows="6" cols="60"><?=$r['description']?></textarea></td>
              <td colspan="2" valign="top"><? if($id) {?>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr><td width="60">辦理情形：</td><td style="font-weight:bold; font-size:14px"><?=$r['status']?></td></tr>
                  <tr><td>異動時間：</td><td><?=$r['sDate']?></td></tr>
                  <tr><td>異動說明：</td><td valign="top"><?=$r['notes']?></td></tr>
                  <? if($r['mid']) { ?><tr><td>主約編號：</td><td valign="top"><?=$r['mid']?></td></tr><? } ?>
                </table>  
             	<? } else echo "&nbsp;" ?></td>
            </tr>
              
            <tr>
              <td align="right">保險公司：</td><td colspan="3"><input name="company" type="text" id="company" size="60" value="<?=$r2['company']?>"/></td>
              <td align="right">建立者：</td>
              <td>
                <? $crm=$id?($r['createMan']?$r['createMan']:$_SESSION["empID"]):$_SESSION["empID"] ?>
              	<input name="createMan" type="hidden" id="createMan" value="<?=$crm?>"/><?=($r['createMan']?$r['createMan']:$crm)?>
              </td>
            </tr>
            <tr>
              <td align="right">保險起日：</td><td>
              <input type="date" name="bDate" id="bDate" value="<?=$r2['bDate']?date('Y-m-d',strtotime($r2['bDate'])):''?>"/>
              </td>
              <td align="right">保險迄日：</td><td>
              <input type="date" name="eDate" id="eDate" value="<?=$r2['eDate']?date('Y-m-d',strtotime($r2['eDate'])):''?>"/>
							</td>
              <td align="right">建立日期：</td><td><input name="rDate" type="text" id="rDate" size="10" value="<? if($r[rDate]) echo date('Y-m-d',strtotime($r[rDate])); else echo date('Y-m-d')?>" readonly="readonly"/></td>
              </tr>
          </table>
        </div>
        <div class="TabbedPanelsContent"><? include "$aType.php" ?></div>

        <div class="TabbedPanelsContent">
          <iframe width="100%" height="240" frameborder="0" src="uploadfiles.php?fid=<?=$id?>&title=<?=$r['title']?>"></iframe>
        </div>

      </div>
    </div>
    </td>
  </tr>
  <tr>
    <td>
      <input type="hidden" name="params" id="params" value="" />
    	<input type="hidden" name="aType" id="aType" value="<?=$aType?>" />
      <? if($id && $permit) { ?>
        <input type="hidden" name="id" id="id" value="<?=$id?>"/>
        <input type="submit" name="button" id="button" value="<?=$id?($act?'確定續約':'確定修改'):'確定新增'?>" />
        <input type="reset" name="button2" id="button2" value="重設" />
      <? } else if($aType && !$id){ ?>
    	  <input type="submit" name="button" id="button" value="確定新增"/>
      <? } ?>
    </td>
  </tr>
</table>
</form>
</body>
<script type="text/javascript">var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");TabbedPanels1.showPanel(<?=$tabidx?>);</script>
</html>