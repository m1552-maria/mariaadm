<?
  if($_REQUEST['sqltx']) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");

    include '../../config.php';
    include '../inc_vars.php';
    include '../../getEmplyeeInfo.php';
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    
    
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
    // $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray(
    //     array(
    //         'fill' => array(
    //             'type' => PHPExcel_Style_Fill::FILL_SOLID,
    //             'color' => array('rgb' => '969696')
    //         ),
    //         'font'   => array('bold' => true,
    //             'size' => '12',
    //             'color' => array('argb' => 'FFFFFF')
    //         )
    //     )
    // );

    $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:M1");
    $objPHPExcel->getActiveSheet()->mergeCells("B2:M2");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    /*
    VERTICAL_CENTER 垂直置中
    VERTICAL_TOP
    HORIZONTAL_CENTER
    HORIZONTAL_RIGHT
    HORIZONTAL_LEFT
    HORIZONTAL_JUSTIFY
    */
    $objPHPExcel->getActiveSheet()->setCellValue('A1','保單-清冊');
    $objPHPExcel->getActiveSheet()->setCellValue('A2','製作日期');
    $objPHPExcel->getActiveSheet()->setCellValue('B2',date("Y/m/d"));
    $objPHPExcel->getActiveSheet()->setCellValue('A3','流水號'); 
    $objPHPExcel->getActiveSheet()->setCellValue('B3','保單種類'); 
    $objPHPExcel->getActiveSheet()->setCellValue('C3','保單名稱'); 
    $objPHPExcel->getActiveSheet()->setCellValue('D3','說明');
    $objPHPExcel->getActiveSheet()->setCellValue('E3','總保費');
    $objPHPExcel->getActiveSheet()->setCellValue('F3','辦理情形');
    $objPHPExcel->getActiveSheet()->setCellValue('G3','建立者');
    $objPHPExcel->getActiveSheet()->setCellValue('H3','登錄時間');
    $objPHPExcel->getActiveSheet()->setCellValue('I3','異動時間');
    $objPHPExcel->getActiveSheet()->setCellValue('J3','異動說明');
    $objPHPExcel->getActiveSheet()->setCellValue('K3','保險公司');
    $objPHPExcel->getActiveSheet()->setCellValue('L3','保險起日');
    $objPHPExcel->getActiveSheet()->setCellValue('M3','保險迄日');

    $i = 4;

    $sql = $_REQUEST[sqltx];
    $rs = db_query($sql);

    while($rs && $r=db_fetch_array($rs)) {
      $rs2 = db_query("select * from $r[aType] where fid=$r[id]");
      $r2 = db_fetch_array($rs2);

      $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,($i-3));
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$insTbl[$r[aType]]);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$r[title]);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$r[description]);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$r[allFee]);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$r[status]);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$emplyeeinfo[$r[createMan]]);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,intval($r['rDate'])?date('Y/m/d',strtotime($r['rDate'])):'');
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,intval($r['sDate'])?date('Y/m/d',strtotime($r['sDate'])):'');
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$r[notes]);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$i,$r2[company]);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$i,date('Y/m/d H:i',strtotime($r2[bDate])));
      $objPHPExcel->getActiveSheet()->setCellValue('M'.$i,date('Y/m/d H:i',strtotime($r2[eDate])));


      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
    }


    
    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
    //設定欄寬
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);


    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->save('php://output');




  }


  
?>