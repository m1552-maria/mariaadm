<?php
	include '../../config.php';
	$rsT = db_query("select * from docclasses");
	while( $rT=db_fetch_array($rsT) ) $docTitle[$rT[ID]]=$rT[Title];
	$rsT = db_query("select * from kwgclasses");
	while( $rT=db_fetch_array($rsT) ) $kwgTitle[$rT[ID]]=$rT[Title];	

	$funcs = array(
	 	'A' => '基本資料維護','A1'=>'部門 資料維護','A2'=>'職掌 資料維護','A3'=>'員工 資料維護',
		'B' => '制度規章維護','B1'=>'制度規章 分類表','B2'=>'發佈 制度規章',
		'C' => '知識平台維護','C1'=>'知識平台 分類表','C2'=>'發佈 知識平台');
	
	$eid = $_REQUEST[eid];
	$pvfn = '../../maintainUser/previege/'.$eid.'.txt';
	$csfn = '../../maintainUser/previege/'.$eid.'_class.txt';
	
	//:: Database handles
	//print_r($_REQUEST); exit;
	if($_REQUEST[DoSubmit]) {
		foreach($_REQUEST as $k=>$v) {
			$varAR = explode('~',$k);
			if($varAR[1]) $prvilige[] = $varAR[1];
			if($k=='NewFunc') {
				$varAR2 = explode('~',$v);
				if($varAR2[1]) $prvilige[] = $varAR2[1];
			}
			if($k=='docID') $classdef[]=$_REQUEST[$k];
			if($k=='kwgID') $classdef[]=$_REQUEST[$k];
			if($k=='depID') $classdef[]=$_REQUEST[$k];
		}
		$JSstr = json_encode($prvilige); file_put_contents($pvfn,$JSstr);
		$JSstr = json_encode($classdef); file_put_contents($csfn,$JSstr);
	}
	
	if(!file_exists($pvfn)) file_put_contents($pvfn,'');
	$str = file_get_contents($pvfn);
	if($str) {
		$priviege = json_decode($str);
		sort($priviege);
	} else $priviege=array();
	$unList = array_diff(array_keys($funcs),$priviege); 
	
	if(!file_exists($csfn)) file_put_contents($csfn,'');
	$str = file_get_contents($csfn); 
	if($str) $classdef = json_decode($str); else $classdef=array(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>等級功能設定</title>
	<link rel="stylesheet" href="list.css" />
	<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>   
	<script src="/Scripts/form.js" type="text/javascript"></script>  
	<script>
	function addNewFunc(btn,level) {
		var fc = $(btn).prev().val(); 
		$('#NewFunc').val('fc~'+fc);
		$('#DoSubmit').click();
	}
	function addItem(tbtn,fld) {
		var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6'>"
			+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode;
		$(elm).remove();
	}
	</script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
}
</style>
</head> 
<body bgcolor="#F0F0F0">
<form id="funcFM" name="funcFM" method="post" action="priviege.php" onsubmit="return true">
<table class="sText">
 <tr valign="middle">
 	<td colspan="2">
	<?
		$char = '';	//funtions list char 1
		foreach($priviege as $v) {
			if(!$char) {
				$char = substr($v,0,1);
				echo "<input type='checkbox' name='fc".'~'."$v' checked>$funcs[$v]"; 
			} else {
				if($char!=substr($v,0,1)) { echo '<br>'; $char=substr($v2,0,1); }
				echo "<input type='checkbox' name='fc".'~'."$v' checked>$funcs[$v]"; 
			}
		}
	?>
  </td>
 </tr>
<? if( count($unList) > 0 ) { ?>
<tr>
	<td colspan="2"><select size="1"><? foreach($unList as $vv) echo "<option value='$vv'>$funcs[$vv]</option>" ?></select> <span onclick="addNewFunc(this,'<?=$k?>')">[ 新增 ]</span></td>
</tr>
<? } ?>
<tr>
	<td align="right">制度規章維護分類</td>
	<td width="75%"><input name="docID" type="text" class="queryID" id="docID" size="6" value="<?=$classdef[0]?>" title="<?=$docTitle[$classdef[0]]?>"/></td>
</tr>
<tr>
	<td align="right">知識平台維護分類</td>
	<td width="75%"><input name="kwgID" type="text" class="queryID" id="kwgID" size="6" value="<?=$classdef[1]?>" title="<?=$kwgTitle[$classdef[1]]?>"/></td>
</tr>

<tr valign="top"><td align="right" class="colLabel">可維護人員之部門</td>	<td>
  <? $aa = $classdef[2] ?>
  <input name="depID[]" type="text" class="queryID" id="depID" size="6" value="<?=$aa[0]?>" title="<?=$departmentinfo[$aa[0]]?>"/> <span class="btn" onClick="addItem(this,'depID')"> +新增</span>
  <? for($i=1; $i<count($aa); $i++) {
     echo "<div><input name='depID[]' id='depID' type='text' size='6' value='$aa[$i]'>"
     . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$departmentinfo[$aa[$i]]."</span>"
     . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
  } ?>
</td></tr>
</table>
<div>
	<input type="hidden" name="eid" value="<?=$eid?>" />
	<input type="hidden" id="NewFunc" name="NewFunc" value="" />
	<input type="submit" id="DoSubmit" name="DoSubmit" value="確定" />
	<input type="reset" name="Reset" value="重設" />
</div>
</form>
</body>
</html>