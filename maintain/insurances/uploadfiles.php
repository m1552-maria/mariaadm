<?php
  include("../../config.php");
  $fid = $_REQUEST['fid'];
  $ulpath = '../../data/insurances/';
  $dir = $ulpath.$fid;
  $icons = array('doc'=>'word.jpg','docx'=>'word.jpg','pdf'=>'pdf.jpg','ppt'=>'ppt.jpg','pptx'=>'ppt.jpg','xls'=>'excel.jpg','xlsx'=>'excel.jpg','zip'=>'zip.jpg','rar'=>'zip.jpg');

	$photo_list = array();
	$sql = "select * from insure_docs where fid='$fid' order by id";
	$rsP = db_query($sql, $conn);
	while($rP = db_fetch_array($rsP)) $photo_list[$rP['id']] = $rP;
?>  
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link rel="stylesheet" type="text/css" media="all" href="draguploadfile.css"/>
<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
<script src="draguploadfile.js"></script>
<form id="form1" action="" method="POST" onsubmit="return formValid(this)">
<script>
  var photo = new draguploadfile( 0,'photo','doc');
  var fid = "<?=$fid?>";
  photo.drag();
  $('#photo_file').change( function(){photo.buttonclick();} )

  function formValid(tfm) {
    //名稱看當初命名的名稱而定 因為我把欄位命名成photo_title所以寫入formdata也用photot_title找
    var x = document.getElementsByName("photo_title[]"); var i;
    for (i = 0; i < x.length; i++) {
      formData.append('photo_title[]',document.getElementsByName('photo_title[]')[i].value);
      formData.append('photo_desc[]',document.getElementsByName('photo_desc[]')[i].value);
    }
  
   	//要塞舊圖片標題描述
    var x = document.getElementsByName("photo_title_before[]");	var i;
		for (i = 0; i < x.length; i++) {
			formData.append('photo_title_before[]',document.getElementsByName('photo_title_before[]')[i].value);
			formData.append('photo_desc_before[]',document.getElementsByName('photo_desc_before[]')[i].value);
			formData.append('photo_id[]',document.getElementsByName('photo_id[]')[i].value);
		}
    
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'uploadfiledo.php', true);
    xhr.onload = function () {
      if (xhr.status === 200) {
        //document.write(xhr.responseText); 
        ra = xhr.responseText.split(',');
        if(ra[0]=='ok') window.location = "uploadfiles.php?fid="+fid;  else alert(ra[1]);
        return false;
      } else { //失敗匯入
        return false;
      }
    };
    formData.append("fid",fid);
    xhr.send(formData);
    return false;
  }
</script>
<?
  foreach($photo_list as $k=>$v){  //處理已存在的檔案//圖片和影片要分開
    $pif = pathinfo($v["filepath"]);
    $ext = $pif['extension'];
    $icon = $icons[$ext]?$icons[$ext]:'files.jpg';
    echo '<div style="clear:both;margin:10px 0;">
      <div style="float: left;margin: 0 10px;cursor:pointer" onClick="delete_pic_before(this,'.$v['id'].')">
      <img src="/images/remove-icon.png" width="20" alt="刪除">
      </div><a href="'.$dir."/".$v["filepath"].'" target="preview">'
      .(($ext=='jpg'||$ext=='jpeg'||$ext=='gif'||$ext=='png')?'<img width="100" src="'.$dir."/".$v["filepath"].'" class="preview" title="檢視">':'<img width="100" src="../../images/'.$icon.'" class="preview" title="檢視">')
      .'</a><div style="float: left;margin: 0 0 0 10px;width:500px;">
      標題:<input type="text" name="photo_title_before[]" style="width:100%;margin: 0 0 10px 0;" value="'.$v["title"].'">
      描述:<textarea rows="4" style="width:100%" name="photo_desc_before[]">'.$v["description"].'</textarea>
      <input name="photo_id[]" type="hidden" value="'.$v['id'].'">
      </div>
      <div style="clear:both"></div>
      </div>';
  }
?>  
<div><input type="Submit" value="確 定" style="font-size:16px"></div>
</form>