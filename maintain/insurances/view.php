<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <style>
	.popupPane {position:absolute; left: 25%; top:10%; background-color:#888;	padding: 8px;	display:none;	}
	</style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>

 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	var filterFG = false;
	var sid = "<?=$_REQUEST['sid']?>";

  function newRecord() {
		var aType = $('select#aType option:selected').val();
		if(!aType) { alert('請先選擇保險種類'); return false; }
		var aUrl = sid ? "details.php?sid="+sid+"&aType="+aType : "details.php?aType="+aType;
		parent.mainFrame.location=aUrl;
	}
	
	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		var aType = $('select#aType option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(aType) url = url+'&aType='+aType;
		location.href = url;
	}

	function showList() {
		window.open('/maintain/schedule/list.php','',"width=820px, height=500px, status=no");
	}

	function stateform(id,act) {
		statefm.id.value = id;
		statefm.status.value = act;
		$('#div1').show();
	}
		
	$(function() {	//點選清單第一項 或 被異動的項目
		if(sid) {newRecord(); return }
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<? if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[empID]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
	});	
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>
<body> 
<div id="div1" class="popupPane">
  <form id="statefm" action="stateform.php" target="_parent">
  	<table bgcolor="#FFFFFF" cellpadding="2" cellspacing="0">
    	<tr><th colspan="2" hight="16" bgcolor="#888" style="color:#FFF">保單異動作業</th></tr>
      <tr><td align="right">單號：</td><td><input type="text" name="id" /></td></tr>
      <tr><td align="right">作業模式：</td><td><input type="text" name="status" id="status" readonly /></td></tr>
      <tr><td align="right">退保時間：</td><td><input type="text" name="sDate" id="sDate" value="<?=date('Y-m-d')?>"/></td></tr>
      <tr><td align="right">異動說明：</td><td><textarea name="notes" rows="5" cols="30"><?= '退保人：'.($_SESSION["Name"]?$_SESSION["Name"]:$_SESSION[empName])."\r\n"?></textarea></td></tr>
      <tr><td colspan="2" align="center">
        <input type="submit" name="Submit" value="確定" />
        <button type="button" onClick="$('#div1').hide()">取消</button>
      </td></tr>
    </table>  
  </form>
</div>

<div id="div2" class="popupPane">
	<form id="filterfm" action="" onSubmit="this.aType.value=form1.aType.value">
  	<table bgcolor="#FFFFFF" cellpadding="4" cellspacing="0">
    	<tr><th colspan="2" hight="18" bgcolor="#888" style="color:#FFF">搜尋條件</th></tr>
			<tr><td>保險起日：</td><td><input type="date"" size="10" name="bDate" value="<?=$bDate?>"> ~ <input type="date" size="10" name="eDate" value="<?=$eDate?>"></td></tr>
      <tr><td>保險迄日：</td><td><input type="date" size="10" name="bDate2" value="<?=$bDate2?>"> ~ <input type="date" size="10" name="eDate2" value="<?=$eDate2?>"></td></tr>
      <tr><td>申請單位：</td><td><input name="applyDep" type="text" /></td></tr>
			<tr><td>保險公司：</td><td><input name="company" type="text" /></td></tr>
      <tr><td colspan="2" align="center">
				<input type="hidden" name="aType" value="" />
				<input type="submit" name="Submit" value="確定" />
				<button type="button" onClick="$('#div2').hide()">取消</button>
			</td></tr>
    </table>  
  </form>
</div>

<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="MsgBox()"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'sxBtn'?>" onclick='newRecord()'>
    <select name="aType" id="aType" onChange="DoFilter()">
		<? //::險種 filter
		if( isset($selTbl) ) echo "<option value='$selTbl'>$insTbl[$selTbl]</option>"; else {
			if ($_SESSION['privilege'] > 10) {	
				echo "<option value=''>--全部--</option>";
				foreach($insTbl as $k=>$v) echo "<option value='$k'".($_REQUEST[aType]==$k?' selected ':'').">$v</option>";
			} else {
				if($_SESSION['user_classdef'][3]) {
					$ar = explode(',',$_SESSION['user_classdef'][3]);
					echo "<option value=''>--全部--</option>";
					foreach($ar as $v) echo "<option value='$v'".($_REQUEST[aType]==$v?' selected ':'').">$insTbl[$v]</option>";
				} else {
					echo "<option value='insure_travel'>旅平險</option>";
				}	
			}
		}
		?>
		</select>
		<!-- ::有效性 -->
		<select name="bType" id="bType" onchange="DoFilter()">
			<option value='' <?=$_REQUEST['bType']==''?'selected':'' ?>>--全部--</option>
			<option value='1' <?=$_REQUEST['bType']=='1'?'selected':'' ?>>有效的</option>
			<option value='2' <?=$_REQUEST['bType']=='2'?'selected':'' ?>>已處理</option>
		</select>


    <input type="button" name="toExcel" value="Excel報表" onClick="excelfm.submit()">
    <input type="button" onClick="showList()" value="活動行事曆"/>
    <input type="button" onClick="$('#div2').show();" value="搜尋"/>
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="1" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  <th style="color:#FFF">保險迄日</th>
  </tr>
<?
	//過濾條件
	if(isset($filter)) $sql="select * from $tableName where $defWh and ($filter)"; else $sql="select * from $tableName where $defWh";
  // 搜尋處理
  if ($keyword!="") $sql.=" and ($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')";
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	$outsql = $sql; //echo $outsql;	
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	//echo $sql; //exit;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>><a href="details.php?id=<?=$r[id]?>" target="mainFrame"><?=$id?></a></td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo $fldValue&&$fldValue!='0000-00-00'?date('Y/m/d',strtotime($fldValue)):'&nbsp;'; break;
						case "aType" : echo $insTbl[$fldValue];	break;
						default : echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
    <td>
		<?
			$rs2 = db_query("select * from $r[aType] where fid=$r[id]");
			$r2 = db_fetch_array($rs2);
			$eDate = strtotime($r2[eDate]);
			$crDate = strtotime(date('Y-m-d'));
			$wmDate = strtotime("+30 day");
			if($r[status]) $color="#A0A0A0";
			else if($crDate>$eDate) $color="#ff0000";
			else if($wmDate>$eDate) $color="#ff00ff";
			else $color = "#000000";
			echo "<span style='color:$color;font-weight:bold'>".date('Y/m/d H:i',$eDate)."</span>";
			if(!$r[status] && $color!="#000000") {
				echo " &nbsp; <a href='#' onClick=\"stateform($r[id],'退保')\">退保</a>";
				echo " | <a href='#' onClick=\"stateform($r[id],'不續約')\">不續約</a>";
				echo " | <a href='details.php?id=$r[id]&act=續約' target='mainFrame'>續約</a>";
			}
		?>
    </td>
    
  </tr>    
<? } ?>   
 </table>
</form>

<form name="excelfm" action="excel.php" method="post">
<input type="hidden" name="sqltx" value="<?=$outsql?>">
</form>
</body>
</Html>