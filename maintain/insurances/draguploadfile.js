var formData = new FormData();
//一定要用物件寫法 不然無法多次宣告
function draguploadfile(sort,input_name,type){
	dragfile = this;
	dragfile.sort = sort;//順序
	dragfile.input_name = input_name;//欄位名稱
	dragfile.dragid = input_name+'_d';//拖曳的id
	dragfile.showid = input_name+'_s';//顯示的id
	dragfile.filename = input_name+'_file[]';
	dragfile.other_name = input_name+'[]';//有陣列的欄位
	dragfile.delet = input_name+'_delete[]';//有陣列的欄位
	dragfile.type = type;//類型 影片或是
}

draguploadfile.prototype.drag = function(){
	//fileinput的ui
	var onefileshow = '<input type="file" name="photo[]" size="30" class="input multiPhoto">';
	onefileshow += '<div class="photoPreview">';
	onefileshow += '<div class="photoItem">';
	onefileshow += '<img/>';
	onefileshow += '<div class="photoDesc">';
	onefileshow += '<input type="text" name="photo_title[]" size=40/><br />';
	onefileshow += '<textarea name="photo_desc[]" cols=50 rows=5></textarea>';
	onefileshow += '</div>';
	onefileshow += '<button type="button" class="photoDel">取消</button>';
	onefileshow += '<div class="clearfx"></div>';
	onefileshow += '</div>';
	onefileshow += '</div>';
	// console.log(dragfile.type);

	//拖曳上傳的ui
	var dragfileshow = '';
	if(dragfile.type=='video'){
		dragfileshow += '<label><input type="radio" name="videoType" value="GAE" checked>自行上傳影片</label><label><input type="radio" name="videoType" value="youtube">youtube網址</label>';
		dragfileshow += '<p style="color:red">*請上傳mp4格式的檔案</p>';
		dragfileshow += '<fieldset class="'+dragfile.input_name+'_out">';
		dragfileshow += '<legend>檔案上傳</legend>';
		dragfileshow += '<div>';
		dragfileshow += '<input type="file" id="'+dragfile.input_name+'_file" name="'+dragfile.filename+'" multiple="multiple"/>';
		dragfileshow += '<div id="'+dragfile.dragid+'" class="filedrag" draggable="true">拖曳檔案到這</div>';
		dragfileshow += '</div>';
		dragfileshow += '</fieldset>';
		//youtube 的介面//預設隱藏
		dragfileshow += '<div class="'+dragfile.input_name+'_yt" style="display:none;margin:10px;">輸入要設定的Youtube網址:<input type="text" name="'+dragfile.input_name+'_ytname" size="50"> <input type="button" value="新增" name="'+dragfile.input_name+'_ytsubmit"></div>';
	} else {
		dragfileshow += '<fieldset class="'+dragfile.input_name+'_out">';
		dragfileshow += '<legend>檔案上傳</legend>';
		dragfileshow += '<div>';
		dragfileshow += '<input type="file" id="'+dragfile.input_name+'_file" name="'+dragfile.filename+'" multiple="multiple"/>';
		dragfileshow += '<div id="'+dragfile.dragid+'" class="filedrag" draggable="true">拖曳檔案到這</div>';
		dragfileshow += '</div>';
		dragfileshow += '</fieldset>';
	}
	dragfileshow += '<div class="fileDisplayArea" id="'+dragfile.showid+'"></div>';//顯示最後結果的

	//先檢查瀏覽器
	var Sys = {};
	var ua = navigator.userAgent.toLowerCase();
	var s;
	(s = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = s[1]:
	  (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
	  (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
	  (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
	  (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
	  (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
	if (Sys.ie) {//除了ie10以下其他都可以傳
	  if (parseInt(Sys.ie) < 10) {
     	document.write(onefileshow);
	  } else {
	    document.write(dragfileshow); filemove(dragfile);
	  }
	}else{
		document.write(dragfileshow);	filemove(dragfile);
	}

	function filemove(dragfile) {
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {	// add file drop events
			document.getElementById(dragfile.dragid).addEventListener("dragover", FileDragHover, false); //監聽檔案滑進事件
			document.getElementById(dragfile.dragid).addEventListener('dragenter', FileDragHover, false); //ie
			document.getElementById(dragfile.dragid).addEventListener('dragend', FileHandlera, false); //ie
			document.getElementById(dragfile.dragid).addEventListener("dragleave", FileDragHover, false); //監聽離開事件
			document.getElementById(dragfile.dragid).addEventListener("drop", FileHandlera, false); //監聽丟入事件
		}

		function FileDragHover(e) { //移動檔案到區塊移動而已沒有放開
			e.stopPropagation(); e.preventDefault();
			if(e.type == 'dragover'){
				e.target.className = 'filedrag hover';
			}else if(e.type == 'hover'){
				e.target.className = 'filedrag hover';
			}else{
				e.target.className = 'filedrag';
			}
		}

		function FileHandlera(e) {
			FileDragHover(e);
			var files = e.dataTransfer.files;
			for (var i = 0, f; f = files[i]; i++) {
				var extIndex = f.name.lastIndexOf('.');
				var ext= f.name.substr(extIndex+1, f.name.length).toLowerCase();
				var fn = f.name.substr(0,extIndex);
				var reader = new FileReader();
				reader.f = f;
				reader.name = f.name;
				reader.onload = function(e) { readerOnload(e,ext,fn) };
				reader.readAsDataURL(f);
			}
		}
	}
}

function readerOnload(e,ext,fn) {
	icons = {'doc':'word.jpg','docx':'word.jpg','pdf':'pdf.jpg','ppt':'ppt.jpg','pptx':'ppt.jpg','xls':'excel.jpg','xlsx':'excel.jpg','zip':'zip.jpg','rar':'zip.jpg'};
	content = '<div style="clear:both;margin:10px 0;">';
	content += '<div style="float: left;margin: 0 10px;cursor:pointer" onClick="delete_pic(this,'+dragfile.sort+','+"'"+dragfile.delet+"'"+')">';
	content += '<img src="/images/remove-icon.png" width="20" alt="刪除">';
	content += '</div>';
	if(ext=='mp4'){
		content += '<img width="100" src="../../images/video_icon.png" style="float:left">';
	} else if(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'){
		content += '<img width="100" src="'+e.target.result+'" style="float:left">';
	} else { content += '<img width="100" src="../../images/'+(icons[ext]?icons[ext]:'files.jpg')+'" style="float:left">' }
	content += '<div style="float: left;margin: 0 0 0 10px;width:500px;">';
	content += '標題:<input type="text" name="'+dragfile.input_name+'_title[]" value="'+fn+'" style="width:100%;margin: 0 0 10px 0;">';
	content += '描述:<textarea rows="4" style="width:100%" name="'+dragfile.input_name+'_desc[]"></textarea>';
	content += '</div>';
	content += '<div style="clear:both"></div>';
	content += '</div>';
	//delete的按鈕
	$("#"+dragfile.showid).append(content);
	dragfile.sort = dragfile.sort+1;//因為跟外面不同步所以要寫在裡面//用來記錄現在是第幾張圖
	//放到form data
	formData.append(dragfile.other_name,e.target.f,e.target.name);//放裡面順序才不會有錯誤
	//if(dragfile.type!='photo') formData.append(dragfile.input_name+'_type[]','GAE');
}

draguploadfile.prototype.buttonclick = function(){	//按鈕的
	dragfile = this;
	for (var i = 0, f; f = document.getElementById(dragfile.input_name+'_file').files[i]; i++) {
		var extIndex = f.name.lastIndexOf('.');
		var ext= f.name.substr(extIndex+1, f.name.length).toLowerCase();//檔名用成小寫
		var fn = f.name.substr(0,extIndex);
		//html5 file reader//載入燈箱的時候
		var reader = new FileReader();
		reader.f = f;
		reader.name = f.name;
		reader.onload = function(e) { readerOnload(e,ext,fn) };
		reader.readAsDataURL(f);
	}
}

//youtube
draguploadfile.prototype.yt_show = function(url){
	if(url.indexOf('?')!=-1){ //處理url
		var ary=url.split('?')[1].split('&');
		for(var i in ary){
			str=ary[i].split('=')[0];
			if (str == 'v') {	str_value = decodeURI(ary[i].split('=')[1]); }
		}
	}else{
		var ary = url.split('/');
		str_value = decodeURI(ary[ary.length-1]);//抓最後一個
	}
	dragfile = this;
	//組產生的內容
	content = '<div style="clear:both;margin:10px 0;">';
	content += '<div style="float: left;margin: 0 10px;cursor:pointer" onClick="delete_pic(this,'+dragfile.sort+','+"'"+dragfile.delet+"'"+')">';
	content += '<img src="/images/remove-icon.png" width="20" alt="刪除">';
	content += '</div>';
	content += '<img width="100" src="http://img.youtube.com/vi/'+str_value+'/hqdefault.jpg" style="float:left">';
	content += '<div style="float: left;margin: 0 0 0 10px;width:500px;">';
	content += '標題:<input type="text" name="'+dragfile.input_name+'_title[]" style="width:100%;margin: 0 0 10px 0;">';
	content += '描述:<textarea rows="5" style="width:100%" name="'+dragfile.input_name+'_desc[]"></textarea>';
	content += '</div>';
	content += '<div style="clear:both">';
	content += '</div>';
	content += '</div>';
	//delete的按鈕
	$("#"+dragfile.showid).append(content);
	dragfile.sort = dragfile.sort+1;//因為跟外面不同步所以要寫在裡面//用來記錄現在是第幾張圖
	//放到form data
	formData.append(dragfile.input_name+'_video[]',str_value);
	formData.append(dragfile.input_name+'_type[]','youtube');
}

//刪除新增圖片
function delete_pic(now,idx,deletename){//刪除圖片
  $(now).parent().remove();//上 一層移除
  //記到formdata裡面
  formData.append(deletename,idx);
}

//刪除舊圖片
function delete_pic_before(now,idx){//刪除圖片
  $(now).parent().remove();//上 一層移除
}
