<?

	session_start();
	include("../../config.php");
	include("init.php");
	include('../../getEmplyeeInfo.php');	



	$id = $_REQUEST['id'];


	$sql = "select r.*, e.empName, e.depID from emplyee_salary_report r inner join emplyee e on r.empID=e.empID where r.id='".$id."'";
	$rs = db_query($sql, $conn);
	$data = db_fetch_array($rs);

	$sql = "select * from emplyee_salary_items where fid='".$id."'";
	$rs = db_query($sql, $conn);
	$addsubList = array(
		'allowance' =>$data['addTotal'], //津貼及加項 +
	    'overtime' =>0, //加班費 +
	    'absence' =>0, //請假調整 -
	    'taxPay' =>0, //所得稅 -
	    'jobPay' =>0, //勞保 -
	    'healthPay' =>0, //健保 -
	    'mRetirePay' =>0, //自提退休金 - 
	    /*'otherfee' =>array(),//雜項費用 -
	    'addsub' =>$data['subTotal'] //加減項 - */
	    'otherPay' =>$data['subTotal'] //其他代扣 -
	 );

	while ($r = db_fetch_array($rs)) {
		if($r['addsub'] == '+') {
	        switch ($r['iType']) {
              case 'absence':
                $addsubList['absence'] -= $r['money'];
                $addsubList['allowance'] -= $r['money'];
                break;
              case 'overtime':
                $addsubList['overtime'] += $r['money'];
                $addsubList['allowance'] -= $r['money'];
                break;  
              case 'otherfee': //雜項費用
                $addsubList['otherPay'] -= $r['money'];
                $addsubList['allowance'] -= $r['money'];
                break;  
              default:
                switch ($r['title']) {
                  case '勞保自負額':
                    $addsubList['jobPay'] -= $r['money'];
                    $addsubList['allowance'] -= $r['money'];
                    break;
                  case '健保自負額':
                    $addsubList['healthPay'] -= $r['money'];
                    $addsubList['allowance'] -= $r['money'];
                    break;
                  case '勞退自提額':
                    $addsubList['mRetirePay'] -= $r['money'];
                    $addsubList['allowance'] -= $r['money'];
                    break;
                  break;
                }
            }
	    } else {
	        switch ($r['iType']) {
              case 'absence':
                $addsubList['absence'] += $r['money'];
                $addsubList['otherPay'] -= $r['money'];
                break;
              case 'overtime':
                $addsubList['overtime'] -= $r['money'];
                $addsubList['otherPay'] -= $r['money'];
                break;
              case 'allowance':
                $addsubList['otherPay'] -= $r['money'];
                $addsubList['allowance'] -= $r['money'];
                break;
              default:
                switch ($r['title']) {
                  case '所得稅':
                      $addsubList['taxPay'] += $r['money'];
                      $addsubList['otherPay'] -= $r['money'];
                      break;
                  case '勞保自負額':
                      $addsubList['jobPay'] += $r['money'];
                      $addsubList['otherPay'] -= $r['money'];
                      break;
                  case '健保自負額':
                      $addsubList['healthPay'] += $r['money'];
                      $addsubList['otherPay'] -= $r['money'];
                      break;
                  case '勞退自提額':
                      $addsubList['mRetirePay'] += $r['money'];
                      $addsubList['otherPay'] -= $r['money'];
                      break;   
                  break;
                }   
            }

	       
	    }
	}

	$data = array_merge($data,$addsubList);

	$wkMonth = (substr($data['wkMonth'],0,4) - 1911).'年'.substr($data['wkMonth'],4,2).'月份';
	/*echo "<pre>";
	
	print_r($wkMonth);
	exit;*/
  	
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
.photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
.photoPreview .photoDesc { float:left; margin-left:5px; }
.photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
.photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
.photoPreview .photoItem .photoDel { margin:5px 0 0 5px; cursor:pointer; height:30px;}
.photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <title>獎金明細</title>
 <script type="text/javascript">
	
 </script>
 <style type="text/css">
 	.bTable, .bTable2 {
 		width:600px;
 	}
 	.bTable tr td:nth-child(even) {
 		text-align: right;
 	}
 	

 </style>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<div style="text-align:center;font-size:18px;margin-top:20px;">財團法人瑪利亞社會福利基金會<br>員工薪資明細表<br><br></div>
<table align="center" class="bTable2" width="100%"  CellSpacing="0" CellPadding="4">
	<tr>
		<td>單位: <?php echo $departmentinfo[$data['depID']];?></td>
		<td></td>
		<td>計薪月份: <?php echo $wkMonth;?></td>
	</tr>
	<tr>
		<td>員工姓名: <?php echo $data['empName'];?></td>
		<td>員工編號: <?php echo $data['empID'];?></td>
		<td>工時比例: <?php echo $data['ratio'];?></td>
	</tr>
</table>


<table align="center" class="bTable" width="100%" border="1" CellSpacing="0" CellPadding="4">
	<tr>
		<th rowspan="8" style="width: 30;text-align: center;">薪資金額</td>
		<th style="text-align: center;background-color:#cfcfcf; width:160px;" >項目</td>
		<th style="text-align: center;background-color:#cfcfcf; width:110px;">金額</td>
		<th rowspan="8" style="width: 30;text-align: center;">扣項</td>
		<th style="text-align: center;background-color:#cfcfcf; width:160px;">項目</td>
		<th style="text-align: center;background-color:#cfcfcf; width:110px;">金額</td>
	</tr>
	<tr>
		<td>核定薪資</td>
		<td><?php echo $data['mBasic'];?></td>
		<td>所得稅</td>
		<td><?php echo $data['taxPay'];?></td>
	</tr>
	<tr>
		<td>津貼及加項</td>
		<td><?php echo $data['allowance'];?></td>
		<td>勞保費</td>
		<td><?php echo $data['jobPay'];?></td>
	</tr>
	<tr>
		<td>加班費</td>
		<td><?php echo $data['overtime'];?></td>
		<td>健保費</td>
		<td><?php echo $data['healthPay'];?></td>
	</tr>
	<tr>
		<td>請假</td>
		<td><?php echo -$data['absence'];?></td>
		<td>自願提繳退休金</td>
		<td><?php echo $data['mRetirePay'];?></td>
	</tr>
	<tr style="height: 25px;">
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>其他</td>
		<td><?php echo $data['otherPay'];?></td>
	</tr>
	<tr>
		<td>薪資合計(A)</td>
		<td><?php echo $data['mBasic'] + $data['allowance'] + $data['overtime'] - $data['absence'];?></td>
		<td style="text-align:center;">小計 (B)</td>
		<td><?php echo $data['taxPay'] + $data['jobPay']+ $data['healthPay']+ $data['mRetirePay']+ $data['otherPay'];?></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td style="text-align:left;" colspan="2">實領金額(A-B)</td>
		<td style="text-align:right;"><?php echo $data['payTotal'];?></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td style="text-align:left;" colspan="2">退休金雇主提撥</td>
		<td style="text-align:right;"><?php echo $data['retireCPay'];?></td>
	</tr>
	<tr><td>備註</td><td colspan="5" style="text-align:left;"><?php echo $data['notes']?></td></tr>


	

</table>
<div style="text-align:center;margin-top:20px;">
	<button type="button" id="print_button" class="button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
        <button type="button" id="print_close" class="button" onClick="history.back()">關閉</button>
</div>
	
</form>
<script type="text/javascript">
	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
    }
</script>

</body>
</html>
