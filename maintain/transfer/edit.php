<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
	$sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	
	/*echo "<pre>";
	print_r($r);*/

	switch ($r['type']) {
		case '薪資':
			$sql = "select wkMonth as id from emplyee_salary_report where `wkMonth`='".$r['year'].str_pad($r['month'],2,'0',STR_PAD_LEFT)."' limit 1";

			break;
		case '年終':
			$sql = "select id,title from bonus_festival where `wkYear`='".$r['year']."' and `type` = 'year_end'";
			break;
		case '三節獎金':
			$sql = "select id,title from bonus_festival where YEAR(festivalDate)=".(int)$r['year']." and `type` = 'festival'";
			break;
	}

	$rs2 = db_query($sql);
	$option = array('<option value="">--請選擇--</option>');
	while ($r2 = db_fetch_array($rs2)) {
		$sel = ($r2['id'] == $r['pID']) ? 'selected' : '';
		switch ($r['type']) {
			case '薪資':
				$option[] = '<option '.$sel.' value="'.$r2['id'].'">'.$r2['id'].'薪資</option>';
				break;
			case '年終':
			case '三節獎金':
				$option[] = '<option '.$sel.' value="'.$r2['id'].'">'.$r2['title'].'</option>';
				break;
		}
		
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 .tip{ color:#666;}
.readonly{	color:#999;}
.colLabel {
	background-color: #F0F0F0;
	border-top-style: none; 
    border-right-style: none; 
}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 編輯</title>
 <script type="text/javascript">
	function formvalid(frm){
		//if($('[name="title"]').val()==""){	alert("請輸入名稱");return false;}
	 	return true;
	}
	
</script>
</Head>
<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<? for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? 
		switch($editetA[$i]) { 
			case "readonly": echo "<input type='text' name='$editfnA[$i]' size='$editflA[$i]' value='".$r[$editfnA[$i]]."' class='input' readonly>"; break;
  	  		case "text" :echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  			case "checkbox": 
  				
  				break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  :
				echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; 
					if($editfnA[$i] == 'pID') {
						echo implode('', $option);
					} else {
						foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} 
					}
				echo "</select>"; 
				break;				 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				$value = ($r[$editfnA[$i]] != '') ? date('Y/m/d',strtotime($r[$editfnA[$i]])) : '';
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".$value."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";				
				break;

			default:	echo $r[$editfnA[$i]];	break;
			
  	} ?></td>
  </tr>
  <? 
  } 
  
  ?>


 
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
    <input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <input type="button" value="取消編輯" class="btn" onClick="history.back()">&nbsp;
  </td>
</tr>
</table>

</form>
<?php include('transfer.php');?>
<script type="text/javascript">
	var notEdit = <?php echo $r['notEdit'];?>;
	$(function(){
		if(notEdit == 1) {
		<?php foreach($editfnA as $key => $val) { ?>
			$('[name="<?php echo $val;?>"]').attr('disabled','disabled');
		<?php } ?>
			$('[type="submit"]').attr('disabled','disabled');
			$('[type="reset"]').attr('disabled','disabled');
		}
		
	})
</script>
</body>
</html>
