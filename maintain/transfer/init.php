<?	
	session_start();

    header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10 && $_REQUEST['type'] == 'festival') {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');
	include('../../getEmplyeeInfo.php');
	include('../inc_vars.php');
	$bE = true;	//異動權限

	/*print_r($TransferList[key($TransferList)]);
	exit;*/
	$tableName = "transfer";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bonus/';
	$delField = '';
	$delFlag = false;
	$thumbW = 150;


	$pageTitle = "銀行媒體轉帳";


	// Here for List.asp ======================================= //

	$yearList = array();
	$sql = "select year from transfer where 1 order by year desc";
	$rs = db_query($sql, $conn);
	while ($r = db_fetch_array($rs)) {
		$yearList[$r['year']] = $r['year'];
	}
	$year = (count($yearList) > 0) ? key($yearList) : '';
	if(isset($_REQUEST['yearSelect'])) $year = $_REQUEST['yearSelect'];

	$typeList = array(''=>'全部類別','薪資'=>'薪資','年終'=>'年終','三節獎金'=>'三節獎金'/*,'其他獎金'=>'其他獎金'*/);
	$monthList = array();
	for($i=1;$i<=12;$i++) {
		$monthList[$i]=$i.'月';
	}
	$typeSel = (isset($_REQUEST['typeSelect'])) ? $_REQUEST['typeSelect'] : '';
	$pIDList = array(''=>'--請選擇--');

	$outAccount = $TransferList[key($TransferList)];
	$unitList = array();
	foreach ($TransferList as $key => $value) {
		$unitList[$key] = $key;
	}

	$defaultOrder = "id";
	$searchField1 = "title";

	$pageSize = 20;	//每頁的清單數量

	$yList = array(date('Y')=>date('Y'), (int)date('Y')-1=>(int)date('Y')-1);
	// 注意 primary key should be first one
	$fnAry = explode(',',"id,title,year,month,type");
	$ftAry = explode(',',"編號,標題,年度,月份,類別");
	$flAry = explode(',',"50,,,,,");
	$ftyAy = explode(',',"id,text,text,text,text,text");
	
	$typeList['']='--請選擇--';
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"type,year,month,pID,title,notes");
	$newftA = explode(',',"類別,年度,月份,類別項目,標題,備註");
	$newflA = explode(',',",,,,,50");
	$newfhA = explode(',',",,,,,5");
	$newfvA = array($typeList,$yList,$monthList,$pIDList,'','');
	$newetA = explode(',',"select,select,select,select,text,textbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,year,month,type,pID,title,notes");
	$editftA = explode(',',"編號,年度,月份,類別,類別項目,標題,備註");
	$editflA = explode(',',",,,,,,50");
	$editfhA = explode(',',",,,,,,5");
	$editfvA = array('',$yList,$monthList,$typeList,$pIDList,'','');
	$editetA = explode(',',"text,readonly,readonly,readonly,readonly,text,textbox");

?>