<?
	include("init.php");
	
	//處理上傳的附件
	$delOld = false;	//可否刪除舊檔
	foreach($_FILES as $k=>$v) {
		$fn = $_FILES[$k]['name'];
		if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
		if ($_FILES[$k][tmp_name] != "") {
			if (!copy($_FILES[$k][tmp_name],"$ulpath$fn2")) { //上傳失敗
				echo '附件上傳作業失敗 !'; exit;
			} else $delOld=true;
		}
	}

	//刪除原先的檔案
	if($delOld) {
		$sql = "select $delField from $tableName where $editfnA[0]='".$_REQUEST[$editfnA[0]]."'";
		$rs = db_query($sql,$conn);
		if($r=db_fetch_array($rs)) {
			$fnn = $r[0];
			if(checkStrCode($fnn,'utf-8')) $fnn=iconv("utf-8","big5",$fnn); 
			$fnn = $ulpath.$fnn;
			if(file_exists($fnn)) @unlink($fnn);			
		}
	}
		
	$sql = "update $tableName set ";
  	$fields = array();
	$n = count($editfnA);
  	for($i=1; $i<$n; $i++) {
	  	if ( ($fn=='') && ($editfnA[$i]==$delField)) continue;	//沒有上傳的檔案
	  	switch($editetA[$i]) {
			case "hidden" :
			case "select" :		
			case "readonly":
	  		case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
			case "textbox" :
	  		case "textarea" :	$fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
	  		case "checkbox" : if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
			case "file" : $fldv = "'$fn'"; break;
		}
			$fields[] = "`".$editfnA[$i]."`=".$fldv;
	}
	$sql = $sql.implode(',', $fields)." where $editfnA[0]='".$_REQUEST[$editfnA[0]]."'";
	//echo $sql; exit;
	db_query($sql, $conn);

	$payType = array('1'=>'轉入帳戶','2'=>'領現','3'=>'匯款');

	switch ($_REQUEST['type']) {
		case '薪資':
			$sql = "delete from emplyee_transfer where tID='".$_REQUEST[$editfnA[0]]."'";
			db_query($sql, $conn);

			$sql = "select r.*,e.salaryAct,e.payType from emplyee_salary_report r inner join emplyee e on r.empID=e.empID where r.wkMonth='".$_POST['pID']."' and e.empID <>'99998' order by e.empID ";
			$rs = db_query($sql);

			while ($r = db_fetch_array($rs)) {
				$fields = array();
				$fields[] = "`empID`='".$r['empID']."'";
				$fields[] = "`tID`='".$_REQUEST[$editfnA[0]]."'";
				$fields[] = "`year`='".$_POST['year']."'";
				$fields[] = "`title`='".$_POST['title']."'";
				$fields[] = "`identifier`='1'";
				$fields[] = "`class`='A2'";
				$fields[] = "`loan`='C'";
				$fields[] = "`inAccount`='".$r['salaryAct']."'";
				$fields[] = "`money`='".$r['payTotal']."'";
				$fields[] = "`payType`='".$payType[$r['payType']]."'";
				$sql = "insert into emplyee_transfer set ".implode(',', $fields);

				db_query($sql,$conn);
			}
			break;
		case '年終':
			$sql = "delete from emplyee_transfer where tID='".$_REQUEST[$editfnA[0]]."'";
			db_query($sql, $conn);

			$sql = "select y.*, e.salaryAct,e.payType from emplyee_bonus_year_end y inner join emplyee e on e.empID = y.empID where y.isPay='1' and y.isPayHand ='1' and y.money > 0 and y.bfID ='".$_POST['pID']."' and y.empID <>'00001' and y.empID <>'99998' order by y.empID ";
			$rs = db_query($sql);

			while ($r = db_fetch_array($rs)) {
				$fields = array();
				$fields[] = "`empID`='".$r['empID']."'";
				$fields[] = "`title`='".$_POST['title']."'";
				$fields[] = "`tID`='".$_REQUEST[$editfnA[0]]."'";
				$fields[] = "`year`='".$_POST['year']."'";
				$fields[] = "`identifier`='1'";
				$fields[] = "`class`='A2'";
				$fields[] = "`loan`='C'";
				$fields[] = "`inAccount`='".$r['salaryAct']."'";
				$fields[] = "`money`='".$r['money']."'";
				$fields[] = "`payType`='".$payType[$r['payType']]."'";
				$sql = "insert into emplyee_transfer set ".implode(',', $fields);

				db_query($sql,$conn);
			}
			break;
		case '三節獎金':
			$sql = "delete from emplyee_transfer where tID='".$_REQUEST[$editfnA[0]]."'";
			db_query($sql, $conn);
			
			$sql = "select y.*, e.salaryAct,e.payType from emplyee_bonus_festival y inner join emplyee e on e.empID = y.empID where y.money > 0 and y.fid ='".$_POST['pID']."' and y.empID <>'00001' and y.empID <>'99998' order by y.empID ";
			$rs = db_query($sql);

			while ($r = db_fetch_array($rs)) {
				$fields = array();
				$fields[] = "`empID`='".$r['empID']."'";
				$fields[] = "`tID`='".$_REQUEST[$editfnA[0]]."'";
				$fields[] = "`year`='".$_POST['year']."'";
				$fields[] = "`title`='".$_POST['title']."'";
				$fields[] = "`identifier`='1'";
				$fields[] = "`class`='A2'";
				$fields[] = "`loan`='C'";
				$fields[] = "`inAccount`='".$r['salaryAct']."'";
				$fields[] = "`money`='".$r['money']."'";
				$fields[] = "`payType`='".$payType[$r['payType']]."'";
				$sql = "insert into emplyee_transfer set ".implode(',', $fields);

				db_query($sql,$conn);
			}
			break;
		default:
			exit;
			break;
	}

	db_close($conn);	
	header("Location: list.php?pages=".$_REQUEST['pages']."&keyword=".$_REQUEST['keyword']."&fieldname=".$_REQUEST['fieldname']."&sortDirection=".$_REQUEST['sortDirection']."&selid=".$_REQUEST['selid']);
?>
