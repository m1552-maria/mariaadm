<script type="text/javascript">
 var transferList = <?php echo json_encode($TransferList);?>;
 $(function(){
 	$('[name="title"],[name="type"],[name="pID"],[name="unit"],[name="outAccount"]').prop('required',true);

 	$('[name="month"]').change(function(){
 		var html = ['<option value="">--請選擇--</option>'];
 		$.ajax({
 			url: 'getItem.php',
	        type: 'post',
	        data:{
	        	type:$('[name="type"]').val(), 
	        	year:$('[name="year"]').val(),
	        	month:$('[name="month"]').val()
	        },
	        dataType: 'json',
	        success: function(d){
	        	if(d[0] == '1'){
	        		alert(d[1]);
	        		$('[name="pID"]').html(html.join(''));
	        	}else{
	        		for(var i in d[1]) {
						html.push('<option value="'+i+'">');
						html.push(d[1][i]);
						html.push('</option>');
		        	}
		        	$('[name="pID"]').html(html.join(''));
	        	}
	        	
	        }
 		});
 	});

 	$('[name="year"]').change(function(){
 		var html = ['<option value="">--請選擇--</option>'];
 		if($(this).val() == '') {
 			$('[name="pID"]').html(html.join(''));
 			return false;
 		}
 		$.ajax({
 			url: 'getItem.php',
	        type: 'post',
	        data:{
	        	type:$('[name="type"]').val(), 
	        	year:$(this).val(), 
	        	month:$('[name="month"]').val()
	        },
	        dataType: 'json',
	        success: function(d){
	        	if(d[0] == '1'){
	        		alert(d[1]);
	        		$('[name="pID"]').html(html.join(''));
	        	}else{
	        		for(var i in d[1]) {
						html.push('<option value="'+i+'">');
						html.push(d[1][i]);
						html.push('</option>');
		        	}
		        	$('[name="pID"]').html(html.join(''));
	        	}
	        	
	        }
 		});
 	});

 	$('[name="type"]').change(function(){
 		var html = ['<option value="">--請選擇--</option>'];
 		if($(this).val() == '') {
 			$('[name="pID"]').html(html.join(''));
 			return false;
 		}
 		$.ajax({
 			url: 'getItem.php',
	        type: 'post',
	        data:{
	        	type:$(this).val(), 
	        	year:$('[name="year"]').val(),
	        	month:$('[name="month"]').val()
	        },
	        dataType: 'json',
	        success: function(d){
	        	if(d[0] == '1'){
	        		alert(d[1]);
	        		$('[name="pID"]').html(html.join(''));
	        	}else{
	        		for(var i in d[1]) {
						html.push('<option value="'+i+'">');
						html.push(d[1][i]);
						html.push('</option>');
		        	}
		        	$('[name="pID"]').html(html.join(''));
	        	}
	        	
	        }
 		});
 	});

 	$('[name="unit"]').change(function(){
 		$('[name="outAccount"]').val(transferList[$(this).val()])
 	});
 	$('[name="pID"]').change(function(){
 		$.ajax({
 			url: 'festival.php',
	        type: 'post',
	        data:{
	        	type:$('[name="type"]').val(),
	        	pID: $(this).val()
	        },
	        dataType: 'json',
	        success: function(d){
	        	if(d[0] == '1'){
	        		alert(d[1]);
	        		$('[name="pID"]').val('');
	        	}
	        }
 		});
 	});

 })
</script>

