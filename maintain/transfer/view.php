<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <style type="text/css">
 
.popupPane {
	position:absolute; 
	left: 25%; top:5%;
	background-color:#888;
	padding: 8px;
	display:none;
}
 
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var type = '<?=$_REQUEST[type]?>';
	var cancelFG = true;	
	$(function(){
		$('#d_setting').draggable();
	});
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
			case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
			case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
			case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else{ 			
			location.href="edit.php?ID="+selid+"&pages="+cp+"&type="+type;
		}
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
	function doAppend(){
		location.href="append.php?type="+type
	}
	function setClose(visitId){
		location.href="booking_close.php?visitId="+visitId;
		
	}
	
	function doSubmit() {
		form1.submit();
	}
	function uploadfile(event){
		console.log(event);
		files = event.target.files;
		console.log(files);
	}
	

 </script>
 <script Language="JavaScript" src="list.js"></script> 
</Head>

<body>


<a name="top"/>
<form name="form1" method="post" action="list.php?type=<?=$_REQUEST[type]?>" onSubmit="return Form1_Validator(this);" enctype="multipart/form-data" style="margin:0">
 <table width="95%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    	<input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><?php if ($_SESSION['privilege'] > 10) { ?><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='doAppend()'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
		<?php } ?>

		<select name="yearSelect" id="yearSelect" onChange="doSubmit()">
			<option value="">--年度--</option>
	    	<? foreach ($yearList as $key => $value) {
	    		$sel = ($year == $key) ? 'selected':'';
	    	 	echo "<option $sel value='".$key."'>".$value." 年</option>"; 
	    	} ?>
	    </select>
		<select name="typeSelect" id="typeSelect" onChange="doSubmit()">
	    	<? foreach ($typeList as $key => $value) {
	    		$sel = ($typeSel == $key) ? 'selected':'';
	    	 	echo "<option $sel value='".$key."'>".$value."</option>"; 
	    	} ?>
	    </select>
		



    	
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	$sql = 'select * from '.$tableName." where 1 ";
	if($year != '') $sql .= " and year='".$year."'";
	if($typeSel != '') $sql .= " and type='".$typeSel."'";
	// 搜尋處理
	if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%'))";
	// 排序處理
	if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
	$sql .= " limit ".$pageSize*$pages.",$pageSize"; 
	 //echo $sql;
	$rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]]; 
?>
  <tr valign="top" id="tr_<?=$id?>" style='cursor:pointer;' onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this,'<?=$r['uType']?>')"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
    <?php if($_SESSION['privilege'] > 10) { ?>
            <a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a>
    <?php } else { ?>
    		<a href="javascript:void(0)"><?=$id?></a>
    <?php } ?>
    </td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? 
				switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;
						case "copy" : 
							echo '<input type="button" value="複製" onClick="copy('.$r['ofID'].');">';
							break;
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo ($fldValue != '') ? date('Y/m/d',strtotime($fldValue)) : ''; break;
						default :	
							if($fnAry[$i] == 'action') {
								echo "<input type='button' value='審核年終' onclick='detail(".$r['id'].")'/>";
							}else echo $fldValue;
							break;
				}
			?>
		</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>

<div align="right"><a href="#top">Top ↑</a></div>
<script type="text/javascript">

/*覆蓋原有在 外層lisy.js 的SearchKeyword*/
var bfID = 0;
/*function SearchKeyword() {
	rzlt = prompt("請輸入關鍵字",keyword);
 	if (rzlt!=null) {
		//location.href="list.php?keyword="+encodeURIComponent(rzlt);
		//::過濾條件要跟
		var yrflt = $('select#year option:selected').val();
		var mtflt = $('select#month option:selected').val();
		var url = "list.php?type=<?php echo $_REQUEST['type'];?>&keyword="+encodeURIComponent(rzlt);
		if(yrflt) url = url+'&year='+yrflt;
		if(mtflt) url = url+'&month='+mtflt;
		location.href = url;		
 	}	 
}*/

$(function(){
});
</script>
</body>
</Html>