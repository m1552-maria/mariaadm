<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?></title>
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link href="tuning.css" rel="stylesheet" type="text/css" />

 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script type="text/javascript" src="tuning.js" ></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();

		var yrflt = $('select#year option:selected').val();
		var mtflt = $('select#month option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;

		if(yrflt) url = url+'&year='+yrflt;
		if(mtflt) url = url+'&month='+mtflt;
		location.href = url;		
	}

	$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<? if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[empID]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
	});	
	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'sxBtn'?>" onclick='location.href="append.php";'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="MsgBox()">
   	<? if ($_SESSION['privilege'] > 10) {	//::check if from maintainUser  ?>
      <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
        <? foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>" ?>
      </select>
     <? } else { ?>
     	<select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
     		<? foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>"; ?>
      </select>  
     <? } ?>
    <select name="year" id="year" onChange="doSubmit()"><? for($i=$curYr-1;$i<=$curYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    <select name="month" id="month" onChange="doSubmit()"><? foreach($monthList as $key =>$val) { $sel=($curMh==$key?'selected':''); echo "<option $sel value='$key'>$val 月</option>"; } ?></select>
    <select name="is_leadmen" id="is_leadmen" onChange="doSubmit()">
    	<?php foreach($isLeadmenList as $key => $val) {
    		$selected = ($is_leadmen == $key) ? 'selected' : '';
    		echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
    	}?>
    </select>
    <input type="button" name="toExcel" value="Excel報表" onClick="excelfm.submit()">
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)) $sql="select A.*,B.empName,B.depID from $tableName where $defWh and $filter"; else $sql="select A.*,B.empName,B.depID from $tableName where $defWh";
  // 搜尋處理
  if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%'))";
	// for excel output use.
	$outsql = $sql; //echo $outsql;
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	//echo $sql; //exit;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>><a href="details.php?id=<?=$r[id]?>" target="mainFrame"><?=$id?></a></td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]];
				 switch ($ftyAy[$i]) {
					case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
					case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
					case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
					case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
					case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
					case "datetime" : echo intval($fldValue)?date('Y/m/d H:i',strtotime($fldValue)):'&nbsp;'; break;
					case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
					case "empID" : echo $emplyeeinfo[$fldValue]; break;
					case "tuning": 
						if($r[is_leadmen]=="1"){
							$flag1 = false;	$flag1Tx = " ";
							$wkMonth = date('Ym',strtotime($r['bDate']));
							$rsT = db_query("select * from acctclose where title='abnormal' and allClose=1 and conditions='$wkMonth'");
							if(db_eof($rsT)) $flag1=true; else $flag1Tx='已關帳';
							if($flag1) echo sprintf("<input type='button' onclick=\"doTuning('%s','%s','%s',%d)\" value='調整'/>", $r[id], $emplyeeinfo[$r[empID]], date('Y/m/d H:i',strtotime($r[rtnDate])),$r[offwork]);
							else echo $flag1Tx;
						} else echo "";
					default : echo $fldValue;	
			  } 
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>

<form name="excelfm" action="excel.php" method="post" target="excel">
<input type="hidden" name="sqltx" value="<?=$outsql?>">
</form>

<div id="d_tuning" class="divBorder1">
	<div class="dtlTitle">外出資料調整</div>
	<div class="divBorder2" id="d_list">
		<input type="hidden" id="id" value="">
		<table class="grid">
			<tr align="center" class="tbTitle"><td>員工</td><td>實際返回/下班時間</td><td>直接下班</td></tr>
			<tr align="center">
					<td id="td_empName" width="80"></td>
					<td><input type="text" size="12" id="rtnDate"></td>
					<td><input type="checkbox" id="offwork" value="1"></td>
			</tr>
		</table>
	</div>
	<div class="divBottom">
		<input type="button" class="red" onclick="save()" value="確定">&nbsp;&nbsp;
		<input type="button" class="red" value="取消" onclick="hide('d_tuning')">
	</div>
</div>
</body>
</Html>