<?
  if($_REQUEST['sqltx']) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");

    include("../../config.php"); 
    include('../../getEmplyeeInfo.php');
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    $sql = $_REQUEST['sqltx'];
    $sql .= " order by bDate Desc";
    $rs = db_query($sql);
    
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
   
    $objPHPExcel->getActiveSheet()->getStyle('A3:J3')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:J1");
    $objPHPExcel->getActiveSheet()->mergeCells("B2:J2");
    
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:J2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A3:J3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    /*
    VERTICAL_CENTER 垂直置中
    VERTICAL_TOP
    HORIZONTAL_CENTER
    HORIZONTAL_RIGHT
    HORIZONTAL_LEFT
    HORIZONTAL_JUSTIFY
    */
    $objPHPExcel->getActiveSheet()->setCellValue('A1','外出單列表');
    $objPHPExcel->getActiveSheet()->setCellValue('A2','製作日期');
    $objPHPExcel->getActiveSheet()->setCellValue('B2',date('Y/m/d'));

   
    //跑header 迴圈用
    $headerstart = 3;
    $header = array('A','B','C','D','E','F','G','H','I','J');
    $headervalue = array('流水號','員工編號','員工名稱','外出時間','預計返回時間','實際返回時間','直接下班','經辦事項','主管','已簽核');

    foreach($header as $k=>$v){
      $objPHPExcel->getActiveSheet()->setCellValue($v.$headerstart,$headervalue[$k]);   
    }

    


    $i = 4;
    $curEmp = '';
    $curTyp = '';
    $color = 0;
    $total=0;

   

    while($rs && $r=db_fetch_array($rs)) {

      $eid = $r['empID'];

      $content = array(($i-3),$eid,$emplyeeinfo[$eid],$r['bDate'],intval($r['eDate'])?date('Y-m-d H:i',strtotime($r['eDate'])):'',intval($r['rtnDate'])?date('Y-m-d H:i',strtotime($r['rtnDate'])):'',$r['offwork']?'v':'',$r['content'],$emplyeeinfo[$r['leadmen']],$r['is_leadmen']?'v':'');
      foreach($header as $k=>$v){
        if($v == 'B'){
            //設定格式用
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($v.$i, $content[$k], PHPExcel_Cell_DataType::TYPE_STRING);
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue($v.$i,$content[$k]);    
        }
      }
      
      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
    }
   
  
    
    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    //設定欄寬

    $width = array(20,20,20,20,20,20,20,20,20,20);
    foreach($header as $k=>$v){
      $objPHPExcel->getActiveSheet()->getColumnDimension($v)->setWidth($width[$k]);  
    }
    
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->save('php://output');
 

  }

  
?>