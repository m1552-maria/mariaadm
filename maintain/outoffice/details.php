<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	include('../../getEmplyeeInfo.php');	

	$id = $_REQUEST[id];
	$rs = db_query("select * from outoffice where id=$id");
	if($rs) $r=db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>
	function formvalid(tfm) {
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	</script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	background-color:#f8f8f8;
}
</style>
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="4" cellspacing="1" class="formTx">
        <tr>
          <td align="right" bgcolor="#F0F0F0">外出人員：</td>
          <td><?=$emplyeeinfo[$r[empID]]?></td>
          <td align="right" bgcolor="#F0F0F0">填單日期：</td>
          <td><?=$r[rdate]?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#F0F0F0">外出地點：</td>
          <td colspan="3"><?=$r[place]?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#F0F0F0">外出時間：</td>
          <td><?=$r[bDate]?></td>
          <td align="right" bgcolor="#F0F0F0">預定返回時間：</td>
          <td><?= intval($r[eDate])?$r[eDate]:'&nbsp;'?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#F0F0F0">實際返回/下班時間：</td>
          <td><?=$r[rtnDate]?></td>
          <td align="right" bgcolor="#F0F0F0">直接下班：</td>
          <td><?= $r["offwork"]?'V':''?></td>
        </tr>
                  
        <tr>
          <td align="right" bgcolor="#F0F0F0">經辦事項：</td>
          <td colspan="3"><?= nl2br($r[content])?></td>
        </tr>
        
        <tr valign="top">
          <td align="right" bgcolor="#F0F0F0">主管：</td>
          <td colspan="3"><?=$emplyeeinfo[$r[leadmen]]?>
            <img src="<?=$r[is_leadmen]?'checked.gif':'uncheck.gif'?>" align="absmiddle" />
          <? if($r[is_leadmen]) echo date('Y/m/d H:i',strtotime($r['is_leadmen_date'])) ?></td>
        </tr>
          
      </table>
    </td>
  </tr>
  
</table>
</body>
</html>