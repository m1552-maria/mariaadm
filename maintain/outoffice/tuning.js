var avHours=0;			//可補休時數 
var orig_aType='';		//原始假別
var spHolidays=0;		//可特休時數 
$(function() {
	$( "#d_tuning" ).draggable();
	$('#rtnDate').datetimepicker();
});
function doTuning(id,empName,rtnDate,offwork){
	$('#rtnDate').val(''); $('#offwork').prop('checked', false);
	$('#id').val(id);
	$('#td_empName').html(empName);
	if(rtnDate!='1970/01/01 08:00')	$('#rtnDate').val(rtnDate);
	if(offwork) $('#offwork').prop('checked', true);
	$('#d_tuning').show();
}
function save(){
	var offwork = $('#offwork').prop('checked')?1:0;
	if(!confirm("確定修改資料嗎?"))	return;

	$.ajax({ 
	  type: "POST", 
	  url: "tuning.php", 
	  data: "id="+$('#id').val()+"&rtnDate="+$('#rtnDate').val()+"&offwork="+offwork
	}).done(function( rs ) {
		if(rs == 1){
			alert('調整作業失敗!');
		}else{
			location.reload();
		}
	});
}
function hide(id){
	$('#'+id).hide();
}