<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
		
	$bE = false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "外出單";
	$tableName = "outoffice A,emplyee B";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/outoffice/';
	$delField = 'ahead';
	$delFlag = false;
	$imgpath = '../../data/outoffice/';
	
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	$temp = array();
	if ($_SESSION['privilege'] > 10) {	//::check if from maintainUser 
		if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	}	else {
		if($_SESSION['user_classdef'][2]) {
			if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'"; else {
				$aa = array();
				foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
				$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
			}
		} else $temp[] = "depID=''";
	}
	
	$is_leadmen = -1;

	if(isset($_REQUEST['is_leadmen']) && $_REQUEST['is_leadmen'] != '') {
		$temp[] = " is_leadmen = '".(int)$_REQUEST['is_leadmen']."'";
		$is_leadmen = (int)$_REQUEST['is_leadmen'];
	}

	$isLeadmenList = array(
		'' => '審核狀態',
		'0' => '主管未審核',
		'1' => '主管已審核'
	);

	$monthList = array();
	for($i=1;$i<=12;$i++) {
		$monthList[$i] = $i;
	}
	$monthList['allMonth'] = '全';
	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "A.empID";
	$searchField2 = "B.empName";
	$pageSize = 10;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,empID,place,bDate,eDate,rtnDate,offwork,leadmen,is_leadmen,tuning");
	$ftAry = explode(',',"單號,申辦員工,外出地點,外出時間,預定返回時間,實際返回/下班時間,直接下班,主管,主管簽名,調整");
	$flAry = explode(',',"70,80,,,,,,,,");
	$ftyAy = explode(',',"ID,empID,text,datetime,datetime,datetime,bool,empID,bool,tuning");
?>