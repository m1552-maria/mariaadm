<?php
	include("init.php"); 

	if ($_SESSION['privilege'] <= 10){
		$readonly='readonly';
		$class='readonly';
	}else{
		$readonly='';	
		$class='';
	} 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
.readonly{	color:#999;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 新增</title>
 <script type="text/javascript">
 function formvalid(frm){
 	if($('[name="title"]').val()==""){	alert("請輸入津貼名稱");return false;}
 	var awcIDs=document.getElementsByName("awcID[]");
 	var ck=false;
 	for(var i=0;i<awcIDs.length;i++){
 		if(awcIDs[i].checked)	ck=true;
 	}
 	if(!ck){	alert("請選擇津貼類別");return false;}
 	
	return true;
 }
 	function addItem(tbtn,fld) {
		var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6' alt='multi' <?=$readonly?> class='<?=$class?>'>"
			+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode;
		$(elm).remove();
	}
 </script>
</Head>

<body class="page">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>
	<tr>
  	<td align="right" class="colLabel">作業月份</td>
    <td>
    	<?php 
    	 $decrease=strtotime("-1 month", strtotime(date('Y-m-01 00:00:01', time())));
    	?>
      	<input type="radio" name="wkmonth" value="<?=date("Ym", $decrease)?>"><?=date("Y年m月", $decrease)?>
    	<input type="radio" name="wkmonth" value="<?=date('Ym')?>" checked><?=date('Y年m月')?>
      	<input type="radio" name="wkmonth" value="<?=date("Ym", strtotime('+1 month'))?>"><?=date("Y年m月", strtotime('+1 month'))?>
    </td>
  </tr>
	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><?
  	 	switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  		case "checkbox": 
				$sql="select * from allowance_class";
				$rs=db_query($sql);
				$awcAry=array();
				if(!db_eof($rs)){
					$j=0;
					while($r=db_fetch_array($rs)){
						$awcAry[$r[awcID]]=$r[title];
						echo "<input type='checkbox' name='awcID[]' value='".$r[awcID]."' class='input'>".$r[title]; 
						if($j==3)	echo "<p>";
						$j++;
					}
				}
	  		break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : 
				echo "<select id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=> $v) echo "<option value='$k'>$v</option>"; echo "</select>"; 
				break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } 

	
  ?>
  <tr valign="top">
		<td align="right" class="colLabel">選取部門及員工</td>
		<td>
			<ul class='dep-emp-ul'>
			<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
			<li class='dep-emp-li'>
				<div class='query-title'>部門</div>
				<div class="depID_list">NO DATA</div>
			</li>
			<li class='dep-emp-li'>
				<div class='query-title'>員工</div>
				<div class="empID_list">NO DATA</div>
			</li>
		</ul>
		</td>
	</tr>

	<tr>
		<td colspan="2" align="center" class="rowSubmit">
      <input type="hidden" name="startTime" id="startTime"/>
    	<input type="hidden" name="endTime" id="endTime"/>
			<input type="submit" value="確定新增" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
      <input type="button" value="取消新增" class="btn" onClick="history.back()">&nbsp;
		</td>
	</tr>
</table>
</form>
</body>
</html>
