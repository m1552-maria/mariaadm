<Html><Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <style type="text/css">
#d_setting{	display:none;overflow: auto;height:400px; width:400px; position: absolute;top:80px;left:120px;}
.tip{color:#900;}
.divTitle{
	color:#fff;
	background-color:#900;
	border: 0px;
	padding: 8px;
	font-family:'微軟正黑體'; 
	font-size:14px;
	width:364px;
	margin-top:10px;
	margin-left:10px;
}
.divBorder1 { /* 內容區 第一層外框 */
	background-color:#dedede;
	border:1px solid #666;
	padding:0px;
	margin:0px;
	font-size:16px;
}

.divBorder2 { /* 內容區 第二層外框 */
	padding:8px; 
	background:#FFFFFF;
	margin:0px 10px 0px 10px;
	
	height:300px;
	overflow-x:hidden;
	overflow-y:auto;
}
#d_bottom{	padding-top:8px;}
table.grid{  margin:0px;	border: 1px solid #ccc; border-collapse: collapse; width:100%;}
table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; color:#000;background: #fff; text-align:center;}


.copy{
	width: 300px;
	background-color: white;
	position: absolute;
	right: 100px;
	top:100px;
	padding: 10px;
	border: 1px solid black;
	display: none;
}
.copy_button{
	margin: 0 auto;
    text-align: center;
}
.copy_content{
	text-align: center;
	margin: 10px;
}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	$(function(){
		$('#d_setting').draggable();
		$('.copy').draggable();
	});
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>&year=<?=$curYr;?>&month=<?=$curMh;?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else{ 			
			location.href="edit.php?ID="+selid+"&pages="+cp;
		}
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
	function doAppend(){
		location.href="append.php"
	}
	function setClose(visitId){
		location.href="booking_close.php?visitId="+visitId;
		
	}
	function detail(empID,depID,unitName,unitPrice,awcID,awc_title,awID,wkMonth){
		$('.divTitle').html(awc_title);
		$.ajax({ 
		  type: "POST", 
		  url: "func.php", 
		  dataType: "JSON",
		  data: "act=getEmplyee&empID="+empID+"&awcID="+awcID+"&wkMonth="+wkMonth+"&depID="+depID+"&awID="+awID
		}).done(function( rs ) {
			console.log(rs);
			if(rs.length == 0){
				$("#d_allowance").html("<table class='grid'><tr><td>NO DATA</td></tr><tr><td><input type='button' value='取消' onClick='hide(\"d_setting\")'/></td></tr></table>");
			}else{
				var html='<table class="grid"><tr><td>員工編號</td><td>員工名稱</td><td>數量</td><td>單位</td><td>單價</td><td>金額</td</tr>';
				/*var ary=rs.split("#");
				var ary2;*/
				var empIDList = [];
				for(var i in rs) {
					empIDList.push(i);
					var ary2 = rs[i];
					html+="<tr><td>"+ary2['empID']+"</td><td>"+ary2['empName']+"</td><td><input type='text' name='qty' value='"+ary2['qty']+"' size='3' onblur='doSubTotal()'/></td><td>"+unitName+"</td>";
					html+="<td><input type='text' name='unitPrice' size='3' value='"+ary2['unitPrice']+"'><input type='hidden' name='id' value='"+ary2['id']+"'/></td>";
					html+="<td name='subTotal'>"+(ary2['qty']*ary2['unitPrice'])+"</td>";
					html+="</tr>";
				}

				html+="</table>";
				$("#d_allowance").html(html);
				
				$('#d_bottom').html("<input type='button' value='確定' onClick='doAllowance(\""+empIDList.join(',')+"\",\""+unitName+"\",\""+unitPrice+"\","+awcID+",\""+awc_title+"\","+awID+")'/>&nbsp;<input type='button' value='取消' onClick='hide(\"d_setting\")'/>");
			}
			$("#d_setting").show();
		}).fail(function( msg ) { 
			alert(0);
		});
	}
	function doSubTotal(){
		var qtys=$("[name='qty']");
		var subTotals=$("[name='subTotal']");
		var unitPrices=$("[name='unitPrice']");
		for(var i=0;i<qtys.length;i++){
			$(subTotals[i]).html($(qtys[i]).val() * $(unitPrices[i]).val());
		}

	}
	function doAllowance(d,unitName,unitPrice,awcID,awc_title,awID){
	//	var data=d.split("#");
		var ary = d.split(",");
		var allowance=[];
		var qtys=document.getElementsByName("qty");
		var ids=document.getElementsByName("id");
		var unitPrices=document.getElementsByName("unitPrice");

		var ck=true;
		for(var i in ary) {
			if(isNaN($(qtys[i]).val())){	alert("數量請輸入數字");	$(qtys[i]).focus(); ck=false;break;}
			allowance.push(ary[i]+","+awcID+","+awc_title+","+unitName+","+$(unitPrices[i]).val()+","+$(qtys[i]).val()+","+$(ids[i]).val());
		}

		if(!ck)	return;
		$.ajax({ 
		  type: "POST", 
		  url: "func.php", 
		  data: "act=doAllowance&data="+allowance.join("#")+"&awID="+awID
		}).done(function( rs ) {
			alert("設定成功");
			location.reload();
		}).fail(function( msg ) { 
		});
	}
	function hide(id){
		$('#'+id).hide();
	}
	function doContinue(){
		var ids=document.getElementsByName("ID[]");
		var ary=[];
		for(var i=0;i<ids.length;i++){
			if(ids[i].checked)	ary.push($(ids[i]).val());
		}
		if(ary.length==0){	alert('請選擇要延用的津貼');return;}
		$.ajax({ 
		  type: "POST", 
		  url: "func.php", 
		  data: "act=doContinue&data="+ary.join(",")
		}).done(function( rs ) {
			if(rs!="")	alert(rs);	else location.reload();
			
		}).fail(function( msg ) { 
		});
	}
	function doSubmit() {
		form1.submit();
	}


	function copy(id){
		var y = $('select#year option:selected').val();
		var m = $('select#month option:selected').val();
		if(m.length<2) m='0'+m;
		$(".copy").show();
		$.ajax({ 
		  type: "POST", 
		  url: "api.php", 
		  data: {'id':id,'baseDate':y+'-'+m+'-01'}//判斷要出現什麼資料
		}).done(function( rs ) {
			$('.copy_content').html(rs);
			$('[name="now_id"]').val(id);
		}).fail(function( msg ) { 
		});
	}
	function cancel(){
		$('.copy').hide();
	}
 </script>
 <script Language="JavaScript" src="list.js"></script> 
</Head>

<body>

<form action="copy.php" method="post" enctype="multipart/form-data" id="copy">
<div class="copy">
	<div class="copy_content">
		
	</div>
	<div class="copy_button">
		<input type="hidden" name="now_id" value="">
		<input type="button" name="" value="確定" onClick="document.getElementById('copy').submit();">
		<input type="button" name="" value="取消" onClick="cancel()">
	</div>
</div>
</form>



<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="95%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="<?=$keyword;?>">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    	<input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='doAppend()'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()">
    	<input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
    	<!-- <input type="button" value="沿用" class="<?=$bE?'sBtn':'suBtn'?>" onClick="doContinue()"> -->
    	
    	<select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    	<select name="month" id="month" onChange="doSubmit()"><? for($i=1;$i<13;$i++) { $sel=($curMh==$i?'selected':''); echo "<option $sel value='$i'>$i 月</option>"; } ?></select>
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	///if(isset($filter)) $sql="select * from $tableName where ($filter)"; else $sql="select * from $tableName where 1";
	$sql="select aw.awID,aw.empID,aw.depID,aw.title,awc.title as awc_title,awc.awcID ,aw.state,aw.wkMonth,awc.unitName,awc.unitPrice,aw.state ";
	$sql.="from allowance as aw left join allowance_class  as awc on aw.awcID=awc.awcID ";
	
	if(isset($_REQUEST['year']) && $_REQUEST['year']!=''){
		$sql .= ' where aw.wkMonth="'.$_REQUEST['year'].str_pad($_REQUEST['month'],2,'0',STR_PAD_LEFT).'"';
	}else{
		$sql .= ' where aw.wkMonth="'.$nowYr.str_pad($curMh,2,'0',STR_PAD_LEFT).'"';
	}
	// 搜尋處理
	if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";

  	if ($_SESSION['privilege'] <=10)  {
		if(count($awAry)>0){
			$sql.=" and aw.awID in (".join(",",$awAry).") ";
		}
	}
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize"; 
  $rs = db_query($sql,$conn);
	//var_dump($sql);   

	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]]; 
?>
  <tr valign="top" id="tr_<?=$id?>" style='cursor:pointer;' onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this,'<?=$r['uType']?>')"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
            <a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a>
    </td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? 
				switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						case "copy" : echo '<input type="button" value="複製" onClick="copy('.$r['awID'].');">';	break;
						default :	
							if ($fnAry[$i]=="state"){	
								echo $state[$fldValue];	
							}else if($fnAry[$i]=="awcID"){
								echo $r[awc_title];
							}else if($fnAry[$i]=="action"){
								echo "<input type='button' value='編輯津貼' onclick='detail(\"".$r[empID]."\",\"".$r[depID]."\",\"".$r[unitName]."\",\"".$r[unitPrice]."\",".$r[awcID].",\"".$r[awc_title]."\",".$r[awID].",\"".$r[wkMonth]."\")'/>";
							}else{	echo $fldValue;	}
							break;
				}
			?>
		</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>
<div id='d_setting' class='divBorder1'>
	<div class='divTitle'>設定產品資訊</div>
	<div class='divBorder2'>
		<div id='d_allowance' title='津貼設定'></div>
	</div>
	<div id='d_bottom' style='text-align:center'></div>
</div>
<div align="right"><a href="#top">Top ↑</a></div>
</body>
</Html>