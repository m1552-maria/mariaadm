<?	
	session_start();
  header('Content-type: text/html; charset=utf-8');
  header("Cache-control:private");
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	include('../../getEmplyeeInfo.php');
	include('../../Utilities/authority.php');
	$bE = true;	//異動權限
	$pageTitle = "津貼設定";
	$tableName = "allowance";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;

	//抓emplyee
	// $emplyee_list = array();
	// $sql = "select `empID`,`empName` from emplyee";
	// $rs = db_query($sql);
	// while($r=db_fetch_array($rs)){
	// 	$emplyee_list[$r['empID']] = $r['empName'];
	// }
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "depID=''";
	}
	if(count($temp)>0) $filter = join(' and ',$temp);
	
	// Here for List.asp ======================================= //
	$defaultOrder = "awID";
	$searchField1 = "aw.title";
	$searchField2 = "aw.wkMonth";
	$searchField3 = "awc.title";
	$pageSize = 20;	//每頁的清單數量

	$state=array("0"=>"否","1"=>"是");
	// 注意 primary key should be first one
	$fnAry = explode(',',"awID,title,awcID,action,state,wkMonth,copy");
	$ftAry = explode(',',"編號,名稱,津貼項目,運作,是否套用,執行月份,複製");
	$flAry = explode(',',"50,,,,,,");
	$ftyAy = explode(',',"id,text,text,text,text,text,copy");
	
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,awcID");
	$newftA = explode(',',"津貼名稱,津貼類別");
	$newflA = explode(',',",,");
	$newfhA = explode(',',",,");
	$newfvA = array('','');
	$newetA = explode(',',"text,checkbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"awID,title,awcID");
	$editftA = explode(',',"編號,津貼名稱,津貼類別");
	$editflA = explode(',',",,,");
	$editfhA = explode(',',",,,");
	$editfvA = array('','','');
	$editetA = explode(',',"text,text,checkbox");
?>
