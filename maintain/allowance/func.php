<?php
session_start();
include "../../config.php";
$act=$_POST[act];
switch($act){
	case "getEmplyee":
		$empID=$_POST[empID];
		$depID=$_POST[depID];
		$awcID=$_POST[awcID];
		$wkMonth = $_POST[wkMonth];
		$awID=$_POST[awID];
		getEmplyee($empID,$depID,$awcID,$wkMonth,$awID);
		break;
	case "doAllowance":
		$data=$_POST[data];
		$awID=$_POST[awID];
		doAllowance($data,$awID);
		break;
	case "doContinue":
		$data=$_POST[data];
		doContinue($data);
		break;
	case "delEmplyeeAllowance":
		$ids=$_POST[ids];
		delEmplyeeAllowance($ids);
		break;
}
function delEmplyeeAllowance($ids){
	$sql="delete from emplyee_allowance where id in (".$ids.")";
	db_query($sql);
}
function doContinue($data){
	$ary=explode(",",$data);
	$sql="select title,depID,empID,awcID,wkMonth from allowance where awID in (".$data.") ";
	$rs=db_query($sql);
	$awcID=array();
	$depID=array();
	$empID=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			array_push($depID,$r[depID]);
			array_push($empID,$r[empID]);
			array_push($awcID,$r[awcID]);
		}
	}
	$msg='';
	$ary2=array();
	foreach($ary as $k=>$v){
		$sql="select * from allowance where wkMonth='".date("ym")."' and awcID=".$awcID[$k]." ";
		$sql.="and (depID!='' or empID!='') ";
		$sql.="and (";
		if($depID[$k]!="")	{
			$dep=explode(",",$depID[$k]);
			array_push($ary2,"(depID like '".join("%' or  depID like '",$dep)."%')");
		}
		if($empID[$k]!="")	{
			$emp=explode(",",$empID[$k]);
			array_push($ary2,"empID in ('".join("','",$emp)."')");
		}
		$sql.=join(" or ",$ary2);
		$sql.=")";
		$rs=db_query($sql);
		if(db_eof($rs)){
			$sql="select title,depID,empID,awcID from allowance where awID=".$v;
			$rs2=db_query($sql);
			if(!db_eof($rs2)){
				$r2=db_fetch_array($rs2);
				$sql="insert into allowance (title,depID,empID,awcID,wkMonth) ";
				$sql.="values('".$r2[title]."','".$r2[depID]."','".$r2[empID]."',".$r2[awcID].",'".date("ym")."')";
				db_query($sql);	
			}
		}else{	$msg='此部門或員工已有津貼設定';}
	}
	echo $msg;
}
function doAllowance($data,$awID){
	$sql="select * from allowance where awID=".$awID;
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$wkMonth=$r[wkMonth];
	}
	$sql="update allowance set state=1 where awID=".$awID;
	// echo $sql."\n";
	db_query($sql);
	
	$ary=explode("#",$data);
	$c=count($ary);
	
	$sql="delete from emplyee_allowance where awID=".$awID;
	db_query($sql);
	// echo $sql."\n";

	for($i=0;$i<$c;$i++){
		$ary2=explode(",",$ary[$i]);
		$sql="insert into emplyee_allowance(empID,awcID,awcTitle,unitName,unitPrice,qty,wkMonth,awID) ";
		$sql.="values('".$ary2[0]."',".$ary2[1].",'".$ary2[2]."','".$ary2[3]."','".$ary2[4]."',".$ary2[5].",'".$wkMonth."',$awID)";
		// echo $sql."\n";
		db_query($sql);
	}

}
function getEmplyee($empID,$depID,$awcID,$wkMonth,$awID){
	if ($_SESSION['privilege']<=10) {
		$depAry=array();
	  foreach($_SESSION['user_classdef'][2] as $v){
	   	array_push($depAry,"depID like '".$v."%'");
	  } 
	  $sql="select empID from emplyee where ".join(" or ",$depAry)." ";
	  $rs=db_query($sql);
		$allowEmpAry=array();
	  if(!db_eof($rs)){
	   	while($r=db_fetch_array($rs)){
	  		array_push($allowEmpAry, $r[empID]);
	  	}
	  }
	}
	//抓目前單價
	$default_price = 0;
	$classData = array();
	$sql = ' select * from `allowance_class` where `awcID`='.$awcID;
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$classData=db_fetch_array($rs);
		$default_price = $classData['unitPrice'];
	}

	$emp=explode(",",$empID);

	$sql = "select empID, empName from emplyee where isOnduty=1 ";
	$where = array();
	if($empID != "" ) $where[] = "empID in ('".join("','",$emp)."')";
	if($depID!=""){
		$depID=explode(",",$depID);
		$where[] = "depID like '".join("%' or depID like '",$depID)."%'";
	}
	if(count($where > 0)) $sql.=" and (".implode(" or ", $where).')';
	/*print_r($sql);
	exit;*/
	$data=array();
	$rs=db_query($sql);
	while($r=db_fetch_array($rs)){
		if(isset($allowEmpAry) && !in_array($r['empID'],$allowEmpAry)) continue;
		$data[$r['empID']] = array(
			'empID' => $r['empID'],
			'empName' => $r['empName'],
			'qty' => 0,
			'unitPrice' => $default_price,
			'id' => ''
		);
		
	}

	$sql = "select * from emplyee_allowance where awID='".$awID."'" ;
	$rs=db_query($sql);
	while($r=db_fetch_array($rs)){
		if(isset($data[$r['empID']])) {
			$data[$r['empID']]['qty'] = $r['qty'];
			$data[$r['empID']]['unitPrice'] = $r['unitPrice'];
			$data[$r['empID']]['id'] = $r['id'];
		}
	}

	echo json_encode($data);


	/*$sql="select a.empID,a.empName,b.id,b.awcID,b.qty,b.unitPrice,b.awID ";
	$sql.="from emplyee as a ";
	$sql.="LEFT JOIN emplyee_allowance AS b ON b.empID=a.empID AND b.wkMonth="."'".$wkMonth."' ";
	$sql.="LEFT JOIN allowance AS c ON c.awID=b.awID and c.awID=".$awID." ";
	$ary=array();
	if($empID!="")	array_push($ary,"a.empID in ('".join("','",$emp)."')");
	if($depID!=""){
		$depID=explode(",",$depID);
		array_push($ary,"a.depID like '".join("%' or  a.depID like '",$depID)."%'");
	}
	$data=array();
	if($empID!="" || $depID!=""){
		$sql.=" where (".join(" or ",$ary).")";
		$sql.=" and a.isOnduty=1 ";
		// $sql.="group by empID "; //20180705 sean 原本的group by 會讓資料顯是錯誤
		print_r($sql); exit;
		$rs=db_query($sql);
		if(!db_eof($rs)){
			while($r=db_fetch_array($rs)){
				//echo $awID.':'.$r['awID'].' | ';
				if($allowEmpAry){	if(!in_array($r[empID],$allowEmpAry))	continue;	}
				if(!empty($r['awID']) && $r['awID'] != $awID) {
					continue; //20180705 sean 加入判別 若awID 不是傳入的值 就跳開
					$qty = 1;
					$unitPrice=$default_price;
				} else {
					$qty=$r[qty]>=0?$r[qty]:1;//數量預設1
					$unitPrice=$r[unitPrice]?$r[unitPrice]:$default_price;//數量預設1
				}
				array_push($data,$r[empID].",".$r[empName].",".$qty.",".$unitPrice.",".$r[id]);
			}
		}
	}
	echo join("#",$data);*/
}
?>