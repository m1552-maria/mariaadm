<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">

 <title><?=$pageTitle?></title>
  <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	$(function(){
		$('.div1').draggable();
		$('.cencel-x').click(function(){
			$('.div1').hide();
		})

	});

  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	function clickEdit(aid) {	
		selid = aid;	
		<? if($bE){ ?> DoEdit();	<? } ?>
	}
	function filterSubmit(tSel) {
		tSel.form.submit();
	}

	function history(empID){
		$.ajax({ 
		  type: "POST", 
		  url: "api.php", 
		  data: {'empID':empID}//判斷要出現什麼資料
		}).done(function( rs ) {
			$('.div1').show();
			$('.content').html(rs);
		});
	}


 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
 <style type="text/css">
 	.div1{
 		width: 300px;
 		height: 230px;
 		position: absolute;
 		right: 150px;
 		top: 100px;
 		display: none;
 		/*font-family: '微軟正黑體';*/
 	}
 	.cencel{
	    height: 20px;
	    font-size: 20px;
	    padding: 0 10px 5px 3px;
	    border: 1px solid black;
	    border-bottom:none;
	    background-color: black;
	    color: white;
	    text-align: right;
	    background-color: #4a2f2f;
 	}
 	.cencel-x{
 		color:white;
	    cursor: pointer;
 	}

 	.content{
 		border: 1px solid black;
 		clear: both;
 		height: 165px;
 		overflow:auto;
 		padding: 10px 0;
 		background-color: white;
 		border-top:none;
 	}
 	.content table{
 		/*border: 1px solid #ffffff;*/
 	}
 	.content th{
 		background-color: #cd9696;
 		padding: 3px;
 	}
 	.content td{
 		padding: 3px;
 	}
 </style>
</Head>



<body>
<div class="div1">
	<div style="position: relative;">

		<div class="cencel">
			<span class="cencel-x">x</span>
		</div>
	</div>
		<div class="content">
			
		</div>
	
</div>

<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>


  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)){
		$sql="select a.*,max(a.rdate) as rdate,b.empName,b.cardNo from $tableName as a left join emplyee as b on a.empID=b.empID where ($filter)"; 	
	}else $sql="select a.*,max(a.rdate) as rdate,b.empName,b.cardNo from $tableName as a left join emplyee as b on a.empID=b.empID where 1";
	// 搜尋處理
	if ($keyword!="") $sql.=" and ($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')";
	// 排序處理
	$sql .= ' group by a.empID';
	if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
	$sql .= " limit ".$pageSize*$pages.",$pageSize";  
	$rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>">
    

		<? for($i=0; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]]; ?>
			<? if ($fldValue>"" || $ftyAy[$i]=='history') switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d H:i:s',strtotime($fldValue)); break;
						case "history" :
							
							echo '<input type="button" value="換卡紀錄" onClick="history('."'".$r[empID]."'".')">';
							break;
						default : 
							if($fnAry[$i]=='empID'){
								echo $emplyeeinfo[$fldValue];
							}elseif($fnAry[$i]=='status'){
								echo $status[$fldValue];
							}elseif($fnAry[$i]=='fid'){
								echo $ip_list[$fldValue];
							}else{
								echo $fldValue;		
							}
							break;
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>
<div align="right"><a href="#top">Top ↑</a></div>
</body>
</Html>