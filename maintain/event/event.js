 $(function() {
	 $(".selClass .classItem").hover(
	 	function(){ 
			if (!$(this).is(".selected")) $(this).css({"background-color":"#7ED3E2"});
		},
		function(){
			if (!$(this).is(".selected")) $(this).css({"background-color":""});
		}
	 ).click(function(){
		if ($(this).is(".selected")) {
			$(this).css({"background-color":""}).removeClass("selected");
		} else {
			$(this).css({"background-color":"#7ED3E2"}).addClass("selected");
		}
		reflashClass(this);
	});
	$(".classClose").click(function(){
		$(this).parents(".selClass").fadeOut(300);
	});
	
	$(".multiPhoto").change(function(){
		var that = $(this);
		var oTD = that.parents("td").eq(0);
		$(this.files).each(function(){
			var fReader = new FileReader();
			fReader.readAsDataURL(this);
			fReader.onloadend = function(event){
				var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);
				oItem.after(oItem.clone(true)).show();
				oItem.find("input,textarea").val("");
				var img = oItem.find("img").get(0);
				img.src=event.target.result;
				oItem.get(0).input = that;
				
				var oNewFile = that.clone(true);
				that.hide().after(oNewFile);
			}
		});
	});
	$(".photoDel").click(function(){
		var oItem = $(this).parents(".photoItem").eq(0);
		if (oItem.get(0).input) oItem.get(0).input.remove();
		oItem.remove();
	});
	
	$("input.classInput").each(function(){
		if (this.value != "") {
			var aSel = this.value.split(",");
			var obj = $(this).parent().find(".selClass");
			for(i in aSel) obj.find("#"+aSel[i]).trigger("click");
		}
	});
 });
 
 function doSelClass(obj) {
	 if ($(obj).next().is(":visible")) $(obj).next().fadeOut(300);
	 else $(obj).next().fadeIn(300);
 }
 function reflashClass(obj) {
	 var sClass = "";
	 $(obj).parents(".selClass").find(".selected").each(function(){
		 sClass += ((sClass=="")?"":",")+$(this).attr("id");
	 });
	 $(obj).parents("td").find("input.classInput").val(sClass);
 }
 
 var curInput;	//要選取的input
 function openWindow(elm) {
	 curInput = $(elm).prev();
	 var value = curInput.val();
	 window.open('../department/query_id2.php?param='+value,'query','width=240,height=480,scrollbars=yes').focus();
 }
 function openWindowReturn(result) {
	 curInput.val(result);
 }