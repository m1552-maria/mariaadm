<? 
	session_start();
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 .selClass { display:none; position:absolute; box-shadow:5px 5px 8px #ccc; background-color:#fff; padding:8px; margin:3px 0 0 25px; line-height:31px; z-index:99; border:1px solid #eee; }
 .selClass .topClass { float:left; width:130px; font-size:13px; margin-right:3px; }
 .selClass .topClass .classItem:first-child { background-color:#FCCDC8; }
 .selClass .topClass:nth-child(even) .classItem:first-child { background-color:#C6E789; }
 .selClass .classItem { text-align:center; line-height:28px; cursor:pointer; }
 .selClass .classCurr { background-color:#7ED3E2; }
 .selClass .classClose { clear:both; text-align:center; font-size:15px; font-weight:bold; cursor:pointer; color:#00F; }
 .photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
 .photoPreview .photoDesc { float:left; margin-left:5px; }
 .photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
 .photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
 .photoPreview .photoItem .photoDel { margin:5px 0 0 5px; }
 .photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 .clearfx { clear:both; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script>
 $(function() {
	 $(".selClass .classItem").hover(
	 	function(){ 
			if (!$(this).is(".selected")) $(this).css({"background-color":"#7ED3E2"});
		},
		function(){
			if (!$(this).is(".selected")) $(this).css({"background-color":""});
		}
	 ).click(function(){
		if ($(this).is(".selected")) {
			$(this).css({"background-color":""}).removeClass("selected");
		} else {
			$(this).css({"background-color":"#7ED3E2"}).addClass("selected");
		}
		reflashClass(this);
	});
	$(".classClose").click(function(){
		$(this).parents(".selClass").fadeOut(300);
	});
	
	$(".multiPhoto").change(function(){
		var that = $(this);
		var oTD = that.parents("td").eq(0);
		$(this.files).each(function(){
			var fReader = new FileReader();
			fReader.readAsDataURL(this);
			fReader.onloadend = function(event){
				if (oTD.find(".multiPhoto").length == 1) var oItem = oTD.find(".photoItem").eq(0).show();
				else var oItem = oTD.find(".photoItem").eq(0).clone();
				oItem.find("input,textarea").val("");
				oTD.find(".photoPreview").append(oItem);
				var img = oItem.find("img").get(0);
				img.src=event.target.result;
				oItem.get(0).input = that;
				
				oItem.find(".photoDel").click(function(){
					var oItem = $(this).parents(".photoItem").eq(0);
					oItem.get(0).input.remove();
					oItem.remove();
				});
				
				var oNewFile = that.clone(true);
				that.hide().after(oNewFile);
				//oNewFile.click();
			}
		});
	});
	
	$("input.classInput").each(function(){
		if (this.value != "") {
			var aSel = this.value.split(",");
			var obj = $(this).parent().find(".selClass");
			for(i in aSel) obj.find("#"+aSel[i]).trigger("click");
		}
	});
	
	$('.InputDate').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true, regional: "zh-TW"});
 });
 function doSelClass(obj) {
	 if ($(obj).next().is(":visible")) $(obj).next().fadeOut(300);
	 else $(obj).next().fadeIn(300);
 }
 function reflashClass(obj) {
	 var sClass = "";
	 $(obj).parents(".selClass").find(".selected").each(function(){
		 sClass += ((sClass=="")?"":",")+$(this).attr("id");
	 });
	 $(obj).parents("td").find("input.classInput").val(sClass);
 }
 </script>
 <title><?=$pageTitle?> - 新增</title>
</Head>

<body class="page">
<form action="list_event.php" method="post" name="form1">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<? for ($i=0; $i<count($qryfnA); $i++) { ?><tr class="<?= $qryetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel" nowrap><?=$qryftA[$i]?></td>
  	<td><? switch ($qryetA[$i]) { 
			case "readonly": echo "<input type='text' name='$qryfnA[$i]' size='$qryflA[$i]' value='$qryfvA[$i]' class='input' readonly>"; break;
			case "tag"     :
			case "text"    : echo "<input type='text' name='$qryfnA[$i]' size='$qryflA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$qryfnA[$i]' cols='$qryflA[$i]' rows='$qryfhA[$i]' class='input'></textarea>"; break;
 			case "textarea": echo "<textarea name='$qryfnA[$i]' cols='$qryflA[$i]' rows='$qryfhA[$i]' class='input'></textarea> <input type='button' value='編輯' onClick='HTMLEdit($qryfnA[$i])'>"; break;
			case "checkbox": echo "<input type='checkbox' name='$qryfnA[$i]' value='$qryfvA[$i]' class='input'>'"; break;
			case "multiCheckboxText": foreach($qryfvA[$i] as $k=>$v) {echo "<div style='float:left'><input type='checkbox' name='$qryfnA[$i][]' value='$k' class='input'"; if ($qryfvA[$i]=="true") echo " checked"; echo "><input name='name_$k' size='$qryflA[$i]' type='text' value='$v' /></div>"; } break;
			case "multiClass": 
				echo "<input type='text' name='$qryfnA[$i]' size='$qryflA[$i]' class='input classInput' readonly>";
				echo " <input type='button' name='btn' value='選擇類別' onclick='doSelClass(this)'>";
				echo "<div class='selClass'>";
				$rsC = db_query("select * from $className where parent=0"); 
				while($rC = db_fetch_array($rsC)) {
					echo "<div class='topClass'><div class='classItem' id='".$rC[$ID]."'>".$rC[$Title]."</div>";
					$rsC2 = db_query("select * from $className where parent=".$rC[$ID]); 
					while($rC2 = db_fetch_array($rsC2)) echo "<div class='classItem' id='".$rC2[$ID]."'>".$rC2[$Title]."</div>";
					echo "</div>";
				}
				echo "<div class='classClose'>[確定]</div></div'>";
				break;
			case "multiPhoto"    : 
				echo "<input type='file' name='".$qryfnA[$i]."[]' size='$qryflA[$i]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview'>";
				echo "<div class='photoItem'><img /><div class='photoDesc'><input type='text' name='".$qryfnA[$i]."_title[]' size=40 /><br /><textarea name='".$qryfnA[$i]."_desc[]' cols=50 rows=5></textarea></div><button type='button' class='photoDel'>取消</button><div class='clearfx'></div></div>";
				echo "</div>";
				break;	
			case "date"    : 
				echo "<input type='text' id='$qryfnA[$i]' name='$qryfnA[$i]' size='$qryflA[$i]' value='$qryfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$qryfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$qryfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$qryfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "daterange"    : 
				echo "<input type='text' id='$qryfnA[$i]_start' name='$qryfnA[$i]_start' size='$qryflA[$i]' value='$qryfvA[$i]' class='input InputDate'> ~ ";
				echo "<input type='text' id='$qryfnA[$i]_end' name='$qryfnA[$i]_end' size='$qryflA[$i]' value='$qryfvA[$i]' class='input InputDate'>";
				break;
			case "select"  : echo "<select name='$qryfnA[$i]' size='$qryflA[$i]' class='input'>"; foreach($qryfvA[$i] as $k=>$v) echo "<option value='$k'>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$qryfnA[$i]' size='$qryflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$qryfnA[$i]' value='$qryfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>
	
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<input type="submit" value="查詢" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
      <button class="btn" type="button" onClick="history.back()">取消查詢</button>
		</td>
	</tr>
</table>
</form>
</body>
</html>
