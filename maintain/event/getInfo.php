<?php
	session_start();

	// include 'init.php';
	include '../inc_vars.php';
	include("../../config.php");
	
	//INIT 的內容
	$tableName = "event";
	$tableKey = "id";
	$foreignKey = "eid";
	$classMapTable = "event_class_map";
	$classKey = "ID";
	$classMapKey = "cid";
	$tagTable = "event_tag";
	$tagKey = "id";
	$photoTable = "event_photo";
	$photoKey = "id";
	$extfiles = '';		
	$PID = 'cid';
	$CLASS_PID = 'event_PID';
	// for classid.php
	$className = "event_class";
	$ID  = 'ID';
	$Title = 'Title';

	$editfnA = explode(',',"cid,title,event_date,deps,human,locate,description,tag,photo,video");
	$editftA = explode(',',"類別,事件,日期時間,部門,人物,地點,說明,標籤,照片,影片");
	$editflA = explode(',',",60,,60,60,60,60,60,,");
	$editfhA = explode(',',",,,,,,5,,,");
	$editfvA = array($_SESSION[$CLASS_PID],'','','','','','','','','');
	$editetA = explode(',',"multiClass,text,date,deptext,text,text,textbox,tag,multiPhoto,multiPhoto");

	


	$id = $_REQUEST[id];
	$sql = "select * from $tableName where $tableKey='$id'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	
	$sql = "select c.$Title from $classMapTable cm, $className c where c.$classKey=cm.$classMapKey and $foreignKey='$id'";
	$rsC = db_query($sql, $conn);
	$sClass = "";
	while ($rC = db_fetch_array($rsC)) $sClass .= ((empty($sClass)) ? "" : ",").$rC[$Title];
	
	$sql = "select tag from $tagTable where $foreignKey='$id' order by id";
	$rsT = db_query($sql, $conn);
	$sTag = "";
	while ($rT = db_fetch_array($rsT)) $sTag .= ((empty($sTag)) ? "" : ",").$rT["tag"];
	
	
	$photo_count = 0;//圖片數量
	$video_count = 0;//影片數量
	$photo_list = array();
	$sql = "select * from $photoTable where $foreignKey='$id' order by id";
	$rsP = db_query($sql, $conn);
	while($rP = db_fetch_array($rsP)) {
		//計算影片和圖片有幾張
		if($rP['videoType']==''){
		  $photo_count++;
		}else{
		  $video_count++;
		}
		$photo_list[$rP['id']] = $rP;
	}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table width="700" border="0" align="center">
  <tr><td>
    <table width="100%" border="0" cellpadding="4" cellspacing="1" class="formTx">
    <tr><td align="right" bgcolor="#F0F0F0" nowrap="nowrap">影像事件編號：</td><td><?=$id?></td></tr>
		<? for ($i=0; $i<count($editfnA); $i++) { ?>
		<tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
			<td align="right" bgcolor="#F0F0F0" nowrap="nowrap"><?=$editftA[$i]?>：</td>
			<td><? switch($editetA[$i]) { 
				case "textbox" : echo nl2br($r[$editfnA[$i]]); break;
				case "textarea": echo $r[$editfnA[$i]]; break;
				case "checkbox": echo $editfvA[$editfnA[$i]]; break;
				case "file"    : echo '<img src="'.$r[$editfnA[$i]].'" width="'.$flAry[$i].'" />'; break;
				case "select"  : echo  $editfvA[$editfnA[$i]][$r[$editfnA[$i]]]; break;
				case "hidden"  : echo "<!-- ". ($editfvA[$editfnA[$i]]==''?$r[$editfnA[$i]]:$editfvA[$editfnA[$i]]) ." -->"; break;
				case "date" : echo date('Y/m/d',strtotime($r[$editfnA[$i]])); break;
				case "multiClass" : echo $sClass; break;
				case "multiPhoto" : 
					if($editfnA[$i]=='photo'){
			          echo $photo_count."張";
			        }elseif($editfnA[$i]=='video'){
			          echo $video_count."部";
			        }
					break;
				case "tag" : echo $sTag; break;
				default : echo $r[$editfnA[$i]]; break;
			} ?></td>
		</tr>
		<? } ?>
   </table>
  </td></tr>
</table>

