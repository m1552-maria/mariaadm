<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include("../../config.php");
	include('../../miclib.php');	
	include('../inc_vars.php');	
	$pageTitle = "影像管理";
	$tableName = "event";
	$tableKey = "id";
	$foreignKey = "eid";
	$classMapTable = "event_class_map";
	$classKey = "ID";
	$classMapKey = "cid";
	$tagTable = "event_tag";
	$tagKey = "id";
	$photoTable = "event_photo";
	$photoKey = "id";
	$extfiles = '';				//js及css檔的路徑位置
	
	$PID = 'cid';
	$CLASS_PID = 'event_PID';
	// for classid.php
	$className = "event_class";
	$ID  = 'ID';
	$Title = 'Title';

	// Here for List.asp ======================================= //
	//if($_SESSION['domain_Host']) {
	//	$RootClass = $_SESSION['user_classdef'][0];
	//	$rsT = db_query("select Title from $className where $classKey='$RootClass'"); 
	//	$rT = db_fetch_array($rsT); 
	//	$ROOT_NAME = $rT[0];
	//} else {
		$RootClass = 0;
		$ROOT_NAME = '影像事件管理';		
	//}

	//var_dump($_SESSION[$CLASS_PID]);
	if (isset($_REQUEST[filter])) $_SESSION[$CLASS_PID]=$_REQUEST[filter]; 
	if (!$_SESSION[$CLASS_PID]) $_SESSION[$CLASS_PID]=$RootClass;
	//查詢所有子類別
	$nTreeRecursiveLimit = 0;
	$aChild = getChildClass($_SESSION[$CLASS_PID]);
	$aChild[] = ($_SESSION[$CLASS_PID]=="%") ? 0 : $_SESSION[$CLASS_PID];
	$filter = "$PID in (".join(",", $aChild).")";
	if ($_REQUEST[cnm]) $_SESSION[cnm]=$_REQUEST[cnm]; else {
		if(count($_REQUEST)<2 || $_SESSION[$CLASS_PID]==0) $_SESSION[cnm] = $_SESSION[$CLASS_PID]=='%'?'所有類別':$ROOT_NAME;
	}
	
	// for Upload files and Delete Record use
	$ulpath = '../../data/event/';
	$delField = 'path';
	$delFlag = false;
	
	// Here for List.asp ======================================= //
	
	$defaultOrder = "id Desc";
	$searchField1 = "a.id";
	$searchField2 = "title";
	$pageSize = 20;	//每頁的清單數量
	
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,human,event_date,rdate");
	$ftAry = explode(',',"編號,事件,人物,日期時間,建檔日期");
	$flAry = explode(',',"60,,,,");
	$ftyAy = explode(',',"ID,text,text,text,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"cid,title,event_date,deps,human,locate,description,tag,photo,video");
	$newftA = explode(',',"類別,事件,日期時間,部門,人物,地點,說明,標籤,照片,影片");
	$newflA = explode(',',",60,,60,60,60,60,60,,");
	$newfhA = explode(',',",,,,,,5,,,");
	$newfvA = array($_SESSION[$CLASS_PID],'','','','','','','','','');
	$newetA = explode(',',"multiClass,text,date,deptext,text,text,textbox,tag,multiPhoto,multiPhoto");

	// Here for Edit.asp ============================================================================= //
	$editfnA =$newfnA;
	$editftA = $newftA;
	$editflA = $newflA;
	$editfhA = $newfhA;
	$editfvA = $newfvA;
	$editetA = $newetA;
	
	// Here for query.asp =========================================================================== //
	$qryfnA = explode(',',"cid,title,event_date,human,locate,description,tag");
	$qryftA = explode(',',"類別,事件,日期時間,人物,地點,說明,標籤");
	$qryflA = explode(',',",60,,60,60,60,60");
	$qryfhA = explode(',',",,,,,5,");
	$qryfvA = array($_SESSION[$CLASS_PID],'','','','','','');
	$qryetA = explode(',',"multiClass,text,daterange,text,text,textbox,text");

	function getChildClass($nID = 0) {
		global $ID, $className, $conn, $nTreeRecursiveLimit;
		$nTreeRecursiveLimit++;
		if ($nTreeRecursiveLimit > 100) return false;	
		$rs = db_query("select $ID as ClassID from $className where parent='$nID'",$conn);
		if(db_eof($rs)) return false;
		$aAllChild = array();
		while($r=db_fetch_array($rs)) {
			array_push($aAllChild, $r["ClassID"]);
			$aChild = getChildClass($r["ClassID"]);
			if ($aChild) $aAllChild = array_merge($aAllChild, $aChild);
		}
		$nTreeRecursiveLimit--;
		return $aAllChild;
	}

?>