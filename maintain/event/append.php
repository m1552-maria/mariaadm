<?php
	@session_start();
	include("init.php"); 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=11">
 <title><?=$pageTitle?> - 進階查詢</title>
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link rel="stylesheet" type="text/css" media="all" href="draguploadfile.css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 .selClass { left:0; top:60px; width:96%;
 		display:none; position:absolute; box-shadow:5px 5px 8px #ccc; background-color:#ccc; 
		padding:5px; margin:0 0 0 5px; line-height:20px; z-index:99; border:1px solid #eee; 
	}
 .selClass .topClass { float:left; width:120px; font-size:12px; margin-right:3px; }
 .selClass .topClass .classItem:first-child { background-color:#FCCDC8; }
 .selClass .topClass:nth-child(even) .classItem:first-child { background-color:#C6E789; }
 .selClass .classItem { text-align:center; line-height:25px; cursor:pointer; }
 .selClass .classCurr { background-color:#7ED3E2; }
 .selClass .classClose { clear:both; text-align:center; font-size:15px; font-weight:bold; cursor:pointer; color:#00F; }
 .photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
 .photoPreview .photoDesc { float:left; margin-left:5px; }
 .photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
 .photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
 .photoPreview .photoItem .photoDel { margin:5px 0 0 5px; }
 .photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 .clearfx { clear:both; }
 /*。覆蓋之前的css*/
 .colLabel{	width: 10%; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script src="event.js" type="text/javascript"></script>
 <script src="draguploadfile.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <script type="text/javascript">
 	function formvalid(now){
 		//防呆--------------START
 		if($('#title').val()==''){
 			alert('事件不能為空');
 			return false;
 		}
 		if($('#event_date').val()==''){
 			alert('日期不能為空');
 			return false;
 		}
 		//防呆--------------END

 		//處理要送到後端的資料-------------START
 		<?
 			for ($i=0; $i<count($newfnA); $i++) {
 				if($newetA[$i]!='multiPhoto'){
 					echo 'formData.append('."'".$newfnA[$i]."'".',$("#'.$newfnA[$i].'").val());';	//先把其他資料都放到formdata裡面
 				}
 			}
 		?>
 		formData.append('PID',$('#pmtID').val());
 		//處理要送到後端的資料-----------------END

 		//以下一定要加----------------------------↓
 		//要塞圖片標題描述--------------START
 		//名稱看當初命名的名稱而定 因為我把欄位命名成photo_title所以寫入formdata也用photot_title找
 		var x = document.getElementsByName("photo_title[]");
		var i;
		for (i = 0; i < x.length; i++) {
			formData.append('photo_title[]',document.getElementsByName('photo_title[]')[i].value);
			formData.append('photo_desc[]',document.getElementsByName('photo_desc[]')[i].value);
		}
		//要塞圖片標題描述------------------END
		//要塞影片標題描述--------------START
 		var y = document.getElementsByName("video_title[]");
		var i;
		for (i = 0; i < y.length; i++) {
			formData.append('video_title[]',document.getElementsByName('video_title[]')[i].value);
			formData.append('video_desc[]',document.getElementsByName('video_desc[]')[i].value);
		}
		//要塞影片標題描述------------END
 		
	  var xhr = new XMLHttpRequest();
	  xhr.open('POST', 'doappend.php', true);
	  xhr.onload = function () {
	    if (xhr.status === 200) {
	      window.location = 'index.php';
	      return false;
	    } else {
	      //失敗匯入
	      return false;
	    }
	  };
	  xhr.send(formData);
 		return false;
 	}
 </script>
</Head>

<body>
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" width="100%" class="sTable" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>

	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel" nowrap><?=$newftA[$i]?></td>
  	<td><? switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "tag"     :
			case "text"    : echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]'  id='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' id='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea> <input type='button' value='編輯' onClick='HTMLEdit($newfnA[$i])'>"; break;
			case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' id='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; break;
			case "multiCheckboxText": foreach($newfvA[$i] as $k=>$v) {echo "<div style='float:left'><input type='checkbox' name='$newfnA[$i][]' id='$newfnA[$i][]' value='$k' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo "><input name='name_$k' size='$newflA[$i]' type='text' value='$v' /></div>"; } break;
			case "multiClass": 
				echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input classInput' readonly>";
				echo " <input type='button' name='btn' value='選擇類別' onclick='doSelClass(this)'>";
				echo "<div class='selClass'>";
				$rsC = db_query("select * from $className where parent=0"); 
				while($rC = db_fetch_array($rsC)) {
					echo "<div class='topClass'><div class='classItem' id='".$rC[$ID]."'>".$rC[$Title]."</div>";
					$rsC2 = db_query("select * from $className where parent=".$rC[$ID]); 
					while($rC2 = db_fetch_array($rsC2)) echo "<div class='classItem' id='".$rC2[$ID]."'>".$rC2[$Title]."</div>";
					echo "</div>";
				}
				break;
			case "multiPhoto" :
				$photo = '';	//類型
				if($newfnA[$i]=='photo'){
					$photo = 'photo';
				}elseif($newfnA[$i] == 'video'){
					$photo = 'video';
				}
				echo "<script>
				var $newfnA[$i] = new draguploadfile(0,'$newfnA[$i]','$photo');
				$newfnA[$i].drag();
				$('#$newfnA[$i]_file').change(function(){
					$newfnA[$i].buttonclick();
				})

				$('input[name=\"videoType\"]').click(function(){
					if($(this).val()=='youtube'){
						$('.$newfnA[$i]_out').hide();
						$('.$newfnA[$i]_yt').show();
					}else{
						$('.$newfnA[$i]_out').show();
						$('.$newfnA[$i]_yt').hide();
					}
				})

				$('input[name=\"$newfnA[$i]_ytsubmit\"]').click(function(){
					if($('input[name=\"$newfnA[$i]_ytname\"]').val()!=''){
						$newfnA[$i].yt_show($('input[name=\"$newfnA[$i]_ytname\"]').val());
					}
					$('input[name=\"$newfnA[$i]_ytname\"]').val('');
				})
				</script>";
				break;
			case "deptext" :		
				echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<input type='button' value='部門' onclick='openWindow(this)'>";
				break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : echo "<select name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=>$v) echo "<option value='$k'>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' id='$newfnA[$i]' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>
	

	<tr><td align="right" class="colLabel">授權</td><td><input name="PID" type="text" class="queryID" id="pmtID" size="6" value="0" /></td></tr>
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<input type="submit" value="確定新增" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
      <button class="btn" type="button" onClick="history.back()">取消新增</button>
		</td>
	</tr>
</table>
</form>

</body>
</html>
