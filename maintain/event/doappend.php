<?
	session_start();
	include("init.php");	
	include("../../thumb.php");	

	//主檔處理
	$fields = array();
	$values = array();
	$sql = "insert into $tableName";
	if($_SESSION['empID']) {
		array_push($fields, 'empID');
		array_push($values, "'".$_SESSION[empID]."'");
	}
	$n = count($newfnA);
	for ($i=0; $i<$n; $i++) {
		if ($newetA[$i] != "multiClass" && $newetA[$i] != "multiPhoto" && $newetA[$i] != "tag") {
			array_push($fields, $newfnA[$i]);
		}
		switch($newetA[$i]) {
			case "hidden" :
			case "select" :
			case "readonly" :
			case "textbox":
			case "deptext":
			case "text" :	$fldv = "'".addslashes($_REQUEST[$newfnA[$i]])."'"; break;
			case "textarea" :	$fldv = "'".trim($_REQUEST[$newfnA[$i]])."'"; break;
			case "checkbox" : if(isset($_REQUEST[$newfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".$_REQUEST[$newfnA[$i]]."'"; break;
			case "file" : $fldv = "'$fn'"; break;			
		}
		if ($newetA[$i] != "multiClass" && $newetA[$i] != "multiPhoto" && $newetA[$i] != "tag") {
			array_push($values, $fldv);
		}
	}
	array_push($fields, 'PID');//授權
	array_push($values, $_REQUEST['PID']);
	$sql = $sql.'('.implode(',', $fields).') values('.implode(',', $values).')';
	db_query($sql,$conn);

	//檔案處理
	$parent = mysql_insert_id($conn);
	mkdir($ulpath.$parent."/");	//建立專屬目錄
	for ($i=0; $i<$n; $i++) {
		switch($newetA[$i]) {
			case "multiClass" :
				$aClass = split(",", $_REQUEST[$newfnA[$i]]);//新增到map表的 如果沒有就不會出現
				foreach($aClass as $v) {
					$sql = "insert into $classMapTable ($foreignKey,$classMapKey) values ('".$parent."','".$v."')";
					db_query($sql,$conn);
				}
				break;
			case "multiPhoto" :
				//移除沒有要上傳的檔案
				//處理圖片的部分
				if($newfnA[$i]=='photo'){
					$delete_pic = array();//處理刪除的檔案，看有哪一些檔案被刪除
					if(isset($_POST[$newfnA[$i].'_delete'])){
						foreach ($_POST[$newfnA[$i].'_delete'] as $k=>$v) {
							$delete_pic[$v] = $v;
						}
					}

					$photo_seq = 0;//圖片順序
					foreach($_FILES[$newfnA[$i]]["name"] as $k=>$v) if (!empty($_FILES[$newfnA[$i]]["tmp_name"][$k])) {
						if(!isset($delete_pic[$k])){//排除不要的檔案
							$finf = pathinfo($v); 
							$new_filename = substr(md5(uniqid(rand())),0,10).time();
							$dstfn = $parent."_".$new_filename.'.'.$finf['extension'];
							copy($_FILES[$newfnA[$i]]["tmp_name"][$k],$ulpath.$parent."/".$dstfn);
							$dstfnM = $parent."_".$new_filename.'_m.'.$finf['extension'];
							imageResize($_FILES[$newfnA[$i]]["tmp_name"][$k], $ulpath.$parent."/".$dstfnM, 600, 600);
							$dstfnS = $parent."_".$new_filename.'_s.'.$finf['extension'];
							imageResize($_FILES[$newfnA[$i]]["tmp_name"][$k], $ulpath.$parent."/".$dstfnS, 90, 90);
							$sql = "insert into $photoTable ($foreignKey,title,description,photo,photo_m,photo_s) values ('".$parent."','".$_REQUEST[$newfnA[$i]."_title"][$photo_seq]."','".$_REQUEST[$newfnA[$i]."_desc"][$photo_seq]."','".$dstfn."','".$dstfnM."','".$dstfnS."')";
							db_query($sql,$conn);
							$photo_seq++;
						}
					}
				}elseif($newfnA[$i]=='video'){
					//處理應該要刪掉的檔案
					$delete_video = array();
					if(isset($_POST[$newfnA[$i].'_delete'])){
						foreach ($_POST[$newfnA[$i].'_delete'] as $k=>$v) {
							$delete_video[$v] = $v;
						}
					}
			    $video_seq = 0;//真正剩下來的數量
					//因為可能有gae 還有youtube屬性，目前只支援這兩種屬性
					$gae_seq = 0;//gae順序
					$youtube_seq = 0;//youtube順序
					foreach($_POST[$newfnA[$i].'_type'] as $k=>$v){
						if(!isset($delete_video[$k])){//排除不要的檔案
							if($v=='GAE'){
								$finf = pathinfo($_FILES[$newfnA[$i]]["name"][$gae_seq]);
								$new_filename = substr(md5(uniqid(rand())),0,10).time();
								$dstfn = $parent."_".$new_filename.'.'.$finf['extension'];
								copy($_FILES[$newfnA[$i]]["tmp_name"][$gae_seq],$ulpath.$parent."/".$dstfn);
								$sql = "insert into $photoTable ($foreignKey,title,description,vurl,videoType) values ('".$parent."','".$_REQUEST[$newfnA[$i]."_title"][$video_seq]."','".$_REQUEST[$newfnA[$i]."_desc"][$video_seq]."','".$dstfn."','GAE')";
								db_query($sql,$conn);
								$video_seq++;
								$gae_seq++;
							}elseif($v=='youtube'){
								$url = 'http://img.youtube.com/vi/'.$_REQUEST[$newfnA[$i]."_video"][$youtube_seq].'/hqdefault.jpg';
								$sql = "insert into $photoTable ($foreignKey,title,description,videoType,vurl,purl) values ('".$parent."','".$_REQUEST[$newfnA[$i]."_title"][$video_seq]."','".$_REQUEST[$newfnA[$i]."_desc"][$video_seq]."','youtube','".$_REQUEST[$newfnA[$i]."_video"][$youtube_seq]."','".$url."')";
								db_query($sql,$conn);
								$video_seq++;
								$youtube_seq++;
							}
						}else{//就算被排除掉也要把順序加上去不然會錯誤
							if($v=='GAE'){
								$gae_seq++;//
							}elseif($v=='youtube'){
								$youtube_seq++;
							}
						}
					}
				}
		        
				break;
			case "tag" :
				$aTag = split(",", $_REQUEST[$newfnA[$i]]);
				foreach($aTag as $v) {
					$sql = "insert into $tagTable ($foreignKey,tag) values ('".$parent."','".$v."')";
					db_query($sql,$conn);
				}
				break;
		}
	}
	exit;
	db_close($conn);
	header("Location: index.php");
?>
