<?php
	@session_start();
	include 'init.php';
	include '../inc_vars.php';

	$id = $_REQUEST[id];
	$sql = "select * from $tableName where $tableKey='$id'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	$sql = "select c.$Title from $classMapTable cm, $className c where c.$classKey=cm.$classMapKey and $foreignKey='$id'";
	$rsC = db_query($sql, $conn);
	$sClass = "";
	while ($rC = db_fetch_array($rsC)) $sClass .= ((empty($sClass)) ? "" : ",").$rC[$Title];
	$sql = "select tag from $tagTable where $foreignKey='$id' order by id";
	$rsT = db_query($sql, $conn);
	$sTag = "";
	while ($rT = db_fetch_array($rsT)) $sTag .= ((empty($sTag)) ? "" : ",").$rT["tag"];
	$photo_count = 0;//圖片數量
 	$video_count = 0;//影片數量
	$photo_list = array();
	$sql = "select * from $photoTable where $foreignKey='$id' order by id";
	$rsP = db_query($sql, $conn);
	while($rP = db_fetch_array($rsP)) {	//計算影片和圖片有幾張
		if($rP['videoType']==''){
			$photo_count++;
		}else{
			$video_count++;
		}
		$photo_list[$rP['id']] = $rP;
	}
?>
<!DOCTYPE>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!-- <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"> -->
  <title>Details</title>
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
  <link href="../../css/swiper.css" rel="stylesheet" type="text/css" />
  <script src="../../Scripts/jquery-1.12.3.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="http://www.youtube.com/player_api"></script>
  
 	<script>
 		//處理youtube影片
		var players = new Array();
	    onYouTubeIframeAPIReady();
	    function onYouTubeIframeAPIReady() {
	        var temp = $("iframe.yt_players");
	        for (var i = 0; i < temp.length; i++) {
	            var t = new YT.Player($(temp[i]).attr('id'), {
	                events: {
	                    'onStateChange': onPlayerStateChange
	                }
	            });
	            players.push(t);
	        }
	    }
	    function onPlayerStateChange(event) {
	        if (event.data == YT.PlayerState.PLAYING) {
	            var temp = event.target.a.src;
	            for (var i = 0; i < players.length; i++) {
	                if (players[i].a.src != temp) 
	                    players[i].pauseVideo();//暫停 //stopVideo停止
	            }
	        }
	    }

	    //處理一般影片
	    function video_pause(now){
	    	if ($(now).get(0).paused) {//判斷是否在撥放
					$(now).get(0).play();
				} else {
					$(now).get(0).pause();
				}
	    }
	</script>
	<style type="text/css">
		body {
			/*margin: 30px;*/
			margin: 0;
			background-color:#cacaca;
			font-family: '微軟正黑體';
		}
		
		.out{
			width: 100%;
			margin: 0 auto;
			text-align: center;
		}
		
		.content{
			/*padding: 10px;*/
			/*border: 3px solid #9b9b9b;*/
			text-align: left;
			border-radius: 5px;
			box-shadow:4px 4px 12px -2px rgba(20%,20%,40%,0.5);
			width: 25%;
			margin: 30px 30px 10px 30px;
			padding: 50px;
			display: inline-block;
			vertical-align: top;
			background-color: #eeeeee;
		}
		.content ul{
			padding: 5px;
			margin: 0px;
			list-style: none;
			border-bottom: 1px solid #000000;
		}
		.content li:first-child{
			font-weight: bolder;
			width: 120px;
			vertical-align: top;
			/*background-color: #f8d4d4;*/
		}
		.content li:last-child{
			width: 200px;
			/*border-bottom: 1px solid #f8d4d4;*/
		}
		.content li{
			padding: 2px;
			display: inline-block;
		}

    .swiper-wrapper{
       height: auto;
    }
		.swiper-container {
        /*width: 100%;*/
        padding: 50px;
    }
    .gallery-top{
    	padding: 30px 50px;	
    	/*border:2px solid #db8e8e;*/
    }
		.swiper-slide {
			text-align: center;
			font-size: 18px;
			height: initial;
			display: -webkit-box;
			display: -ms-flexbox;
			display: -webkit-flex;
			display: flex;
			-webkit-box-pack: center;
			-ms-flex-pack: center;
			-webkit-justify-content: center;
			justify-content: center;
			-webkit-box-align: center;
			-ms-flex-align: center;
			-webkit-align-items: center;
			align-items: center;
	  }
	  .gallery-top .swiper-slide img{
	   	max-width:80%;
	   	max-height: 500px;
	   }
	  .gallery-thumbs {
	    box-sizing: border-box;
	    padding: 10px 0;
	    /*background-color: #ead3d3;*/
	   }
	  .gallery-thumbs .swiper-slide {
	    width: 150px;
	    opacity: 0.4;
	    cursor:pointer;
	    position: relative;
	  }
		.gallery-thumbs .swiper-slide img{
			/*width: 100%;*/
			max-height: 150px;
			max-width: 150px;
    }
		.gallery-thumbs .is_selected {  opacity: 1;  }

    .play{
		  width: 150px;
		  position: absolute;
		  height: 75px;
		  text-align: center;
		  /*top:20%;*/
		}
		.play img{
		  margin:10px 0 0 0;
		  width: 50px !important; 
		  vertical-align : middle;

		}
		.bu{
			margin: 20px 0;
			text-align: right;
		}
		.photo{
			display: inline-block;
			vertical-align: top;
			width: 50%;
			min-width:750px;
			padding: 0px 20px;
   		border-radius: 5px;
			box-shadow: 4px 4px 12px -2px rgba(20%,20%,40%,0.5);
	    background-color: #eeeeee;
	    margin: 30px 0 0 0;
		}
		.photo a {
			text-decoration:none;
		}

		@media screen and (max-width: 1400px){
			.content{
	    	width: 30%;
	    	margin: 30px 30px 10px 30px;

	    }
	    .content li:last-child{
	    	width: 150px;
	    }
		}

		@media screen and (max-width: 1200px){
	    .content{
	    	margin: 30px auto;
	    	width: 80%;
	    	display: block;
	    }
	    .photo{
	    	margin: 0px auto;
	    	width: 90%;
	    	display: block;
	    }
		}

		@media all and (-ms-high-contrast:none)	{
			.play{ top:20%; }/* IE10 */
			*::-ms-backdrop, .play{ top:20%; } /* IE11 */
		}

	</style>
</head>

<body>
<div class="out">
	<div class="content">
		<ul>
			<li>影像事件編號</li>
			<li><?=$id?></li>
		</ul>
		<? for ($i=0; $i<count($editfnA); $i++) { ?>
		<ul>
			<li><?=$editftA[$i]?></li>
			<li>
				<? switch($editetA[$i]) { 
					case "textbox" : echo nl2br($r[$editfnA[$i]]); break;
					case "textarea": echo $r[$editfnA[$i]]; break;
					case "checkbox": echo $editfvA[$editfnA[$i]]; break;
					case "file"    : echo '<img src="'.$r[$editfnA[$i]].'" width="'.$flAry[$i].'" />'; break;
					case "select"  : echo  $editfvA[$editfnA[$i]][$r[$editfnA[$i]]]; break;
					case "hidden"  : echo "<!-- ". ($editfvA[$editfnA[$i]]==''?$r[$editfnA[$i]]:$editfvA[$editfnA[$i]]) ." -->"; break;
					case "date" : echo date('Y/m/d',strtotime($r[$editfnA[$i]])); break;
					case "multiClass" : echo $sClass; break;
					case "multiPhoto" : 
						if($editfnA[$i]=='photo'){
				          echo $photo_count."張";
				        }elseif($editfnA[$i]=='video'){
				          echo $video_count."部";
				        }
				        break;
					case "tag" : echo $sTag; break;
					default : echo $r[$editfnA[$i]]; break;
				} ?>
			</li>
		</ul>
		<? }?>
		<div class="bu">
			<a href="toPdf.php?id=<?=$id?>" target="_blank"><button>匯出PDF</button></a> <button onclick="window.close()">關閉</button> 	
		</div>
	</div>

	<div class="photo">
		<div class="swiper-container gallery-top">
				<div class="swiper-wrapper">
				<? foreach($photo_list as $k=>$v){?>
				<?if($v['videoType']==''){?>
					<div class="swiper-slide"><a href="<?=$ulpath.$v[$foreignKey]."/".$v["photo"]?>" target="_blank">
						<?=$v['title']?><br/>
						<img src="<?=$ulpath.$v[$foreignKey]."/".$v["photo"]?>"><br/>
						<?=$v['description']?>
					</a></div>
				<?}elseif($v['videoType']=='GAE'){?>
					<div class="swiper-slide"><video width="640" height="360" onclick="video_pause(this)" controls><source src="<?=$ulpath.$v[$foreignKey]."/".$v["vurl"]?>" type="video/mp4"></video></div>
				<?}elseif($v['videoType']=='youtube'){?>
					<div class="swiper-slide">
						<iframe class="yt_players" id="youtube_<?echo $v['id'];?>" width="640" height="360" src="http://www.youtube.com/embed/<?echo $v['vurl'];?>?rel=0&wmode=Opaque&enablejsapi=1&showinfo=0"	frameborder="0" allowfullscreen></iframe>
					</div>
				<?}?>
				<?}?>
				</div>

				<!-- Add Pagination -->
				
				<!-- Add Arrows -->
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
		</div>

		<div class="swiper-container gallery-thumbs">
				<div class="swiper-wrapper">
						<? 
						$i=0;
						foreach($photo_list as $k=>$v){?>
					<?if($v['videoType']==''){?>
							<div class="swiper-slide <?if($i==0){?> is_selected <?}?>"><img src="<?=$ulpath.$v[$foreignKey]."/".$v["photo"]?>"></div>
					<?$i++;}elseif($v['videoType']=='GAE'){?>
						<div class="swiper-slide <?if($i==0){?> is_selected <?}?>"><div class="play"><img src="../../images/video_play_icon.png"></div><img src="../../images/video_icon.png" / style="max-width: 150px;"></div>		
					<?$i++;}elseif($v['videoType']=='youtube'){?>
						<div class="swiper-slide <?if($i==0){?> is_selected <?}?>"><div class="play"><img src="../../images/video_play_icon.png"></div><img src="http://img.youtube.com/vi/<?echo $v['vurl'];?>/hqdefault.jpg"></div>
					<?$i++;}?>
					<?}?>
				</div>
		</div>
	</div>
</div>
<!-- 一定要寫在下面 -->
<script src="../../Scripts/swiper.min.js"></script>
<script>
    var galleryTop = new Swiper('.gallery-top', {
      slidesPerView:1,
      centeredSlides: true,
      slidesPerView: 'auto',
      spaceBetween: 50,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      onSlideChangeStart: function(swiper){
       	//處理youtube影片
	      for (var i = 0; i < players.length; i++) {
          players[i].pauseVideo();//暫停 //stopVideo停止
        };
		    //處理一般影片
		    //找到全部的影片
		    $('video').each(function(k){ $(this).get(0).pause(); });
    		var activeIndex = swiper.activeIndex;
				//處理遮罩
				$(galleryThumbs.slides).removeClass('is_selected');
    		$(galleryThumbs.slides).eq(activeIndex).addClass('is_selected');
     		galleryThumbs.slideTo(activeIndex,500, false);
      }
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
			spaceBetween: 10,
			centeredSlides: false,
			slidesPerView: 'auto',
			touchRatio: 0.2,
			slideToClickedSlide: true,
			onClick: function (swiper, event){
				var clicked = swiper.clickedIndex;
					//處理遮罩
			$(galleryThumbs.slides).removeClass('is_selected');
					$(galleryThumbs.slides).eq(clicked).addClass('is_selected');
				//處理youtube影片
				for (var i = 0; i < players.length; i++) {
							players[i].pauseVideo();//暫停 //stopVideo停止
					};
			//處理一般影片
			//找到全部的影片
			$('video').each(function(k){
				$(this).get(0).pause();
			});
            
      galleryTop.slideTo(clicked,500, false);
      },onSlideChangeStart: function(swiper){
				//處理youtube影片
				for (var i = 0; i < players.length; i++) {
							players[i].pauseVideo();//暫停 //stopVideo停止
					};
			//處理一般影片
			//找到全部的影片
			$('video').each(function(k){
				$(this).get(0).pause();
			});
				var clicked = swiper.activeIndex;
				//處理遮罩
			$(galleryThumbs.slides).removeClass('is_selected');
					$(galleryThumbs.slides).eq(clicked).addClass('is_selected');

					galleryTop.slideTo(clicked,500, false);
			}
    });
    // galleryTop.params.control = galleryThumbs;
    // galleryThumbs.params.control = galleryTop;
</script>

</body>
</html>