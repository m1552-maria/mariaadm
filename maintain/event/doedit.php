<?
	session_start();
	include("init.php");
	include("../../thumb.php");	
	//主檔處理
	$fields = array();
	$sql = "update $tableName set ";
	$n = count($editfnA);
	for($i=1; $i<$n; $i++) {
		switch($editetA[$i]) {
			case "hidden" :
			case "select" :
			case "deptext" :
			case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
			case "textbox" :
			case "textarea" :	$fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
			case "checkbox" : if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
			case "file" : $fldv = "'$fn'"; break;
		}
		if ($newetA[$i] != "multiClass" && $newetA[$i] != "multiPhoto" && $newetA[$i] != "tag") {
			array_push($fields, $editfnA[$i].'='.$fldv);
		}
	}
	array_push($fields, 'PID='.$_REQUEST['PID']);//授權
	$sql = $sql.implode(',', $fields)." where $tagKey='".$_REQUEST["selid"]."'";
	// echo $sql;	exit;
	db_query($sql, $conn);
	
	//子檔處理
	$parent = $_REQUEST["selid"];
  if(!is_dir($ulpath.$parent."/")) mkdir($ulpath.$parent."/");	//建立專屬目錄
	for ($i=0; $i<$n; $i++) {
		switch($editetA[$i]) {
			case "multiClass" :
				db_query("delete from $classMapTable where $foreignKey='$parent'", $conn);		//先刪除所有類別map，再全部新增
				$aClass = split(",", $_REQUEST[$editfnA[$i]]);
				foreach($aClass as $v) {
					$sql = "insert into $classMapTable ($foreignKey,$classMapKey) values ('".$parent."','".$v."')";
					db_query($sql,$conn);
				}
				break;
			case "multiPhoto" :
				//修改及刪除
				//處理原本應要要修改的東西
				//要增加的東西
				//處理原本的資料------------------------START
				if(isset($_POST['photo_id'])){
					foreach($_POST['photo_id'] as $k=>$v){
						$_REQUEST['photo_'.$v] = $v;
						$_REQUEST['photo_title_'.$v] = $_POST['photo_title_before'][$k];
						$_REQUEST['photo_desc_'.$v] = $_POST['photo_desc_before'][$k];
					}
				}

				if(isset($_POST['video_id'])){
					foreach($_POST['video_id'] as $k=>$v){
						$_REQUEST['video_'.$v] = $v;
						$_REQUEST['video_title_'.$v] = $_POST['video_title_before'][$k];
						$_REQUEST['video_desc_'.$v] = $_POST['video_desc_before'][$k];
					}	
				}
				
				$sql = "select * from $photoTable where $foreignKey='$parent'";
				$rsP = db_query($sql, $conn);
				while ($rP = db_fetch_array($rsP)) {
					//修改
					if (!empty($_REQUEST[$editfnA[$i]."_".$rP["id"]])) {
						echo 'dddd';
						$sql = "update $photoTable set title='".$_REQUEST[$editfnA[$i]."_title_".$rP["id"]]."', description='".$_REQUEST[$editfnA[$i]."_desc_".$rP["id"]]."' where id='".$rP["id"]."'";
						db_query($sql,$conn);
					} else {
						echo 'aaaa';
						echo $editfnA[$i];
						echo $rP['videoType'];
						if($editfnA[$i]=='photo' && $rP['videoType']==''){//要加videoType 不然會導致重複刪除
							//刪除
							unlink($ulpath.$parent."/".$rP["photo"]);
							unlink($ulpath.$parent."/".$rP["photo_m"]);
							unlink($ulpath.$parent."/".$rP["photo_s"]);
							db_query("delete from $photoTable where id='".$rP["id"]."'", $conn);
						}elseif($editfnA[$i]=='video' && $rP['videoType']!=''){//要加videoType 不然會導致重複刪除
							if($rP["videoType"]=='GAE'){
								unlink($ulpath.$parent."/".$rP["vurl"]);
							}
							db_query("delete from $photoTable where id='".$rP["id"]."'", $conn);
						}
					}
				}
				//處理原本的資料------------------------END

				//處理新增的資料------------------------START
				//移除沒有要上傳的檔案
				//處理圖片的部分
				if($editfnA[$i]=='photo'){
					$delete_pic = array();
					if(isset($_POST[$editfnA[$i].'_delete'])){
						foreach ($_POST[$editfnA[$i].'_delete'] as $k=>$v) {
							$delete_pic[$v] = $v;
						}
					}

					$photo_seq = 0;
					foreach($_FILES[$editfnA[$i]]["name"] as $k=>$v) if (!empty($_FILES[$editfnA[$i]]["tmp_name"][$k])) {
						if(!isset($delete_pic[$k])){//移除不要的檔案
							$finf = pathinfo($v); 
							$new_filename = substr(md5(uniqid(rand())),0,10).time();
							$dstfn = $parent."_".$new_filename.'.'.$finf['extension'];
							copy($_FILES[$editfnA[$i]]["tmp_name"][$k],$ulpath.$parent."/".$dstfn);
							$dstfnM = $parent."_".$new_filename.'_m.'.$finf['extension'];
							imageResize($_FILES[$editfnA[$i]]["tmp_name"][$k], $ulpath.$parent."/".$dstfnM, 600, 600);
							$dstfnS = $parent."_".$new_filename.'_s.'.$finf['extension'];
							imageResize($_FILES[$editfnA[$i]]["tmp_name"][$k], $ulpath.$parent."/".$dstfnS, 90, 90);
							$sql = "insert into $photoTable ($foreignKey,title,description,photo,photo_m,photo_s) values ('".$parent."','".$_REQUEST[$editfnA[$i]."_title"][$photo_seq]."','".$_REQUEST[$editfnA[$i]."_desc"][$photo_seq]."','".$dstfn."','".$dstfnM."','".$dstfnS."')";
							db_query($sql,$conn);
							$photo_seq++;
						}
					}
				}elseif($editfnA[$i]=='video'){
					//處理應該要刪掉的檔案
					$delete_video = array();
	        if(isset($_POST[$editfnA[$i].'_delete'])){
            foreach ($_POST[$editfnA[$i].'_delete'] as $k=>$v) {
              $delete_video[$v] = $v;
            }
	        }

	        $video_seq = 0;//真正剩下來的數量
	        //因為可能有gae 還有youtube屬性
					$gae_seq = 0;//順序
					$youtube_seq = 0;
					if(isset($_POST[$editfnA[$i].'_type'])){
						foreach($_POST[$editfnA[$i].'_type'] as $k=>$v){
							if(!isset($delete_video[$k])){
								if($v=='GAE'){
									$finf = pathinfo($_FILES[$editfnA[$i]]["name"][$gae_seq]);
									$new_filename = substr(md5(uniqid(rand())),0,10).time();
									$dstfn = $parent."_".$new_filename.'.'.$finf['extension'];
									copy($_FILES[$editfnA[$i]]["tmp_name"][$gae_seq],$ulpath.$parent."/".$dstfn);
									$sql = "insert into $photoTable ($foreignKey,title,description,vurl,videoType) values ('".$parent."','".$_REQUEST[$editfnA[$i]."_title"][$video_seq]."','".$_REQUEST[$editfnA[$i]."_desc"][$video_seq]."','".$dstfn."','GAE')";
									db_query($sql,$conn);
									$video_seq++;
									$gae_seq++;
								}elseif($v=='youtube'){
									$url = 'http://img.youtube.com/vi/'.$_REQUEST[$editfnA[$i]."_video"][$youtube_seq].'/hqdefault.jpg';
									$sql = "insert into $photoTable ($foreignKey,title,description,videoType,vurl,purl) values ('".$parent."','".$_REQUEST[$editfnA[$i]."_title"][$video_seq]."','".$_REQUEST[$editfnA[$i]."_desc"][$video_seq]."','youtube','".$_REQUEST[$editfnA[$i]."_video"][$youtube_seq]."','".$url."')";
									db_query($sql,$conn);
									$video_seq++;
									$youtube_seq++;
								}
							}else{
								if($v=='GAE'){
									$gae_seq++;//
								}elseif($v=='youtube'){
									$youtube_seq++;
								}
							}
						}
					}
				}
				//處理新增的資料------------------------END
				break;
			case "tag" :
				db_query("delete from $tagTable where $foreignKey='$parent'", $conn);		//先刪除所有tag，再全部新增
				$aTag = split(",", $_REQUEST[$editfnA[$i]]);
				foreach($aTag as $v) {
					$sql = "insert into $tagTable ($foreignKey,tag) values ('".$parent."','".$v."')";
					db_query($sql,$conn);
				}
				break;
		}
	}
	// exit;
	db_close($conn);
	header("Location: index.php");
	//header("Location: list.php?pages=".$_REQUEST['pages']."&keyword=".$_REQUEST['keyword']."&fieldname=".$_REQUEST['fieldname']."&sortDirection=".$_REQUEST['sortDirection']."&selid=".$_REQUEST['selid']);
?>
