<?php
	@session_start();
	include 'init.php';
	include '../inc_vars.php';
	include('../../getEmplyeeInfo.php');	
	
	$id = $_REQUEST[id];
	if(!$id) { exit; }
	$sql = "select * from $tableName where $tableKey='$id'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	
	$sql = "select c.$Title from $classMapTable cm, $className c where c.$classKey=cm.$classMapKey and $foreignKey='$id'";
	$rsC = db_query($sql, $conn);
	$sClass = "";
	while ($rC = db_fetch_array($rsC)) $sClass .= ((empty($sClass)) ? "" : ",").$rC[$Title];
	
	$sql = "select tag from $tagTable where $foreignKey='$id' order by id";
	$rsT = db_query($sql, $conn);
	$sTag = "";
	while ($rT = db_fetch_array($rsT)) $sTag .= ((empty($sTag)) ? "" : ",").$rT["tag"];
	
  $photo_count = 0;//圖片數量
  $video_count = 0;//影片數量
  $photo_list = array();
	$sql = "select * from $photoTable where $foreignKey='$id' order by id";
	$rsP = db_query($sql, $conn);
  while($rP = db_fetch_array($rsP)) {
    //計算影片和圖片有幾張
    if($rP['videoType']==''){
      $photo_count++;
    }else{
      $video_count++;
    }
    $photo_list[$rP['id']] = $rP;
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script>
	function formvalid(tfm) {
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	
	$(window).load(function(){
		var nTotImgWidth = 0;
		$(".thumb").wrapInner("<div class='box'></div>");
		$(".thumb .box .photo").each(function(){
			nTotImgWidth += $(this).outerWidth(true);
		});
		// $(".thumb .box").css("width",nTotImgWidth);
	});
	</script>
<style type="text/css">
.thumb { width:100%; height:120px; overflow:auto; }
.photo { margin:3px 5px 0 0; float:left; }
.video { margin:3px 5px 0 0;  float:left; position: relative;}
.video >div{
  width: 100px;
  position: absolute;
  height: 75px;
  text-align: center;
  background-color:rgba(0, 0, 0, 0.2);
}
.video >div>img{
  margin:20px 0 0 0;
  width: 30px;
  vertical-align : middle;

}
</style>
</head>

<body>

<table width="100%" border="0" cellpadding="4" cellspacing="1" class="formTx">
  <? for ($i=0; $i<count($editfnA); $i++) { ?>
  <? if ($i % 2 == 0) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>"><? } ?>
    <td width="60" align="right" bgcolor="#F0F0F0" nowrap="nowrap"><?=$editftA[$i]?>：</td>
    <td><? switch($editetA[$i]) { 
      case "textbox" : echo nl2br($r[$editfnA[$i]]); break;
      case "textarea": echo $r[$editfnA[$i]]; break;
      case "checkbox": echo $editfvA[$editfnA[$i]]; break;
      case "file"    : echo '<img src="'.$r[$editfnA[$i]].'" width="'.$flAry[$i].'" />'; break;
      case "select"  : echo  $editfvA[$editfnA[$i]][$r[$editfnA[$i]]]; break;
      case "hidden"  : echo "<!-- ". ($editfvA[$editfnA[$i]]==''?$r[$editfnA[$i]]:$editfvA[$editfnA[$i]]) ." -->"; break;
      case "date" : echo date('Y/m/d',strtotime($r[$editfnA[$i]])); break;
      case "multiClass" : echo $sClass; break;
      case "multiPhoto" : 
        if($editfnA[$i]=='photo'){
          echo $photo_count."張";
        }elseif($editfnA[$i]=='video'){
          echo $video_count."部";
        }
        
      break;
      case "tag" : echo $sTag; break;
			case "deptext" : $aa = explode(',',$r[$editfnA[$i]]); foreach($aa as $v) $ba[]=$departmentinfo[$v]; echo join(',',$ba); break;
      default : echo $r[$editfnA[$i]]; break;
    } ?></td>
  <? if ($i % 2 == 1) { ?></tr><? } ?>
  <? } ?>
</table>

<div class="thumb"><div class="box">

  <? 
    foreach($photo_list as $k=>$v){
  		if($v['videoType']==''){
  			?>
  				<div class="photo"><img src="<?=$ulpath.$v[$foreignKey]."/".$v["photo_s"]?>" /></div>			
  			<?
  		}elseif($v['videoType']=='GAE'){
  			?>
  				<div class="video"><div><img src="../../images/video_play_icon.png"></div><img src="../../images/video_icon.png" / style="max-width: 100px;"></div>			
  			<?
  		}elseif($v['videoType']=='youtube'){
  			?>
  				<div class="video"><div><img src="../../images/video_play_icon.png"></div><img src="<?=$v['purl']?>" / style="max-width: 100px;"></div>			
  			<?
  		}
  	}
  ?>


  
  
  </div>
</div>
</body>
</html>