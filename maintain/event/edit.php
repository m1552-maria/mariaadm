<?php
	@session_start();
	include("init.php");
	$ID = $_REQUEST['ID'];
	$sql = "select * from $tableName where $tableKey='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	
	$sql = "select $classMapKey from $classMapTable where $foreignKey='$ID'";
	$rsC = db_query($sql, $conn);
	$sClass = "";
	while ($rC = db_fetch_array($rsC)) $sClass .= ((empty($sClass)) ? "" : ",").$rC[$classMapKey];
	
	$sql = "select tag from $tagTable where $foreignKey='$ID' order by id";
	$rsT = db_query($sql, $conn);
	$sTag = "";
	while ($rT = db_fetch_array($rsT)) $sTag .= ((empty($sTag)) ? "" : ",").$rT["tag"];
	
	$photo_list = array();
	$sql = "select * from $photoTable where $foreignKey='$ID' order by id";
	$rsP = db_query($sql, $conn);
	while($rP = db_fetch_array($rsP)) {
		$photo_list[$rP['id']] = $rP;
	}

	// echo '<pre>';
	// print_r($photo_list);
	// exit;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?> - 編輯</title>
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link rel="stylesheet" type="text/css" media="all" href="draguploadfile.css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 .selClass { left:0; top:60px; width:96%;
 	display:none; position:absolute; box-shadow:5px 5px 8px #ccc; background-color:#ccc; 
	padding:5px; margin:0 0 0 5px; line-height:20px; z-index:99; border:1px solid #eee; 
	}
 .selClass .topClass { float:left; width:120px; font-size:12px; margin-right:3px; }
 .selClass .topClass .classItem:first-child { background-color:#FCCDC8; }
 .selClass .topClass:nth-child(even) .classItem:first-child { background-color:#C6E789; }
 .selClass .classItem { text-align:center; line-height:25px; cursor:pointer; }
 .selClass .classCurr { background-color:#7ED3E2; }
 .selClass .classClose { clear:both; text-align:center; font-size:15px; font-weight:bold; cursor:pointer; color:#00F; }
 .photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
 .photoPreview .photoDesc { float:left; margin-left:5px; }
 .photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
 .photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
 .photoPreview .photoItem .photoDel { margin:5px 0 0 5px; }
 .photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 .clearfx { clear:both; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script>
 <script src="event.js" type="text/javascript"></script>
 <script src="draguploadfile.js"></script>
 <script type="text/javascript">
 	function formvalid(now){
 		if($('#title').val()==''){
 			alert('事件不能為空');
 			return false;
 		}
 		if($('#event_date').val()==''){
 			alert('日期不能為空');
 			return false;
 		}

 		<?
 			for ($i=0; $i<count($editfnA); $i++) {
 				if($newetA[$i]!='multiPhoto'){
 					echo 'formData.append('."'".$editfnA[$i]."'".',$("#'.$editfnA[$i].'").val());';	
 				}
 				
 			}
 		?>

 		//處理一些init沒寫到的東西 hidden input
 		formData.append('selid',$("#selid").val());
 		formData.append('ClassID',$("#ClassID").val());
 		formData.append('pages',$("#pages").val());
 		formData.append('keyword',$("#keyword").val());
 		formData.append('fieldname',$("#fieldname").val());
 		formData.append('sortDirection',$("#sortDirection").val());

 		formData.append('PID',$('#pmtID').val());//授權
 		
 		//要塞圖片標題描述
 		var x = document.getElementsByName("photo_title[]");
		var i;
		for (i = 0; i < x.length; i++) {
			formData.append('photo_title[]',document.getElementsByName('photo_title[]')[i].value);
			formData.append('photo_desc[]',document.getElementsByName('photo_desc[]')[i].value);
		}

		var x = document.getElementsByName("video_title[]");
		var i;
		for (i = 0; i < x.length; i++) {
			formData.append('video_title[]',document.getElementsByName('video_title[]')[i].value);
			formData.append('video_desc[]',document.getElementsByName('video_desc[]')[i].value);
		}

		//要塞舊圖片標題描述
 		var x = document.getElementsByName("photo_title_before[]");
		var i;
		for (i = 0; i < x.length; i++) {
			formData.append('photo_title_before[]',document.getElementsByName('photo_title_before[]')[i].value);
			formData.append('photo_desc_before[]',document.getElementsByName('photo_desc_before[]')[i].value);
			formData.append('photo_id[]',document.getElementsByName('photo_id[]')[i].value);
		}
		var x = document.getElementsByName("video_title_before[]");
		var i;
		for (i = 0; i < x.length; i++) {
			formData.append('video_title_before[]',document.getElementsByName('video_title_before[]')[i].value);
			formData.append('video_desc_before[]',document.getElementsByName('video_desc_before[]')[i].value);
			formData.append('video_id[]',document.getElementsByName('video_id[]')[i].value);
		}

	    var xhr = new XMLHttpRequest();
	    xhr.open('POST', 'doedit.php', true);
	    xhr.onload = function () {
	      if (xhr.status === 200) {
	        window.location = 'index.php?selid='+$("#selid").val();
	        return false;
	      } else {
	        //失敗匯入
	        return false;
	      }
	    };
	    xhr.send(formData);
 		return false;
 	}
 </script>

</Head>
<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="95%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> - <?=$rC[Title]?> 【編輯作業】</td></tr>
	<? for ($i=0; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? 
		switch($editetA[$i]) { 
			case "text" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' id='".$editfnA[$i]."' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' id='".$editfnA[$i]."' cols='$editflA[$i]' rows='$editfhA[$i]' class='input' >".$r[$editfnA[$i]]."</textarea> <input type='button' value='編輯' onClick='HTMLEdit($editfnA[$i])'>"; break;
			case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' id='".$editfnA[$i]."' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' id='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "multiClass": 
				echo "<input type='text' name='$editfnA[$i]' id='".$editfnA[$i]."' size='$newflA[$i]' value='$sClass' class='input classInput' readonly>";
				echo " <input type='button' name='btn' value='選擇類別' onclick='doSelClass(this)'>";
				echo "<div class='selClass'>";
				$rsCl = db_query("select * from $className where parent=0"); 
				while($rCl = db_fetch_array($rsCl)) {
					echo "<div class='topClass'><div class='classItem' id='".$rCl[$classKey]."'>".$rCl[$Title]."</div>";
					$rsCl2 = db_query("select * from $className where parent=".$rCl[$classKey]); 
					while($rCl2 = db_fetch_array($rsCl2)) echo "<div class='classItem' id='".$rCl2[$classKey]."'>".$rCl2[$Title]."</div>";
					echo "</div>";
				}
				break;
			case "multiPhoto"    :
				//類型//處理新增的檔案//拖曳上傳
				$photo = '';
				if($editfnA[$i]=='photo'){
					$photo = 'photo';
				}elseif($editfnA[$i] == 'video'){
					$photo = 'video';
				}
				echo "<script>
				var $editfnA[$i] = new draguploadfile(0,'$editfnA[$i]','$photo');
				$editfnA[$i].drag();
				$('#$editfnA[$i]_file').change(function(){
					$editfnA[$i].buttonclick();
				})

				$('input[name=\"videoType\"]').click(function(){
					if($(this).val()=='youtube'){
						$('.$editfnA[$i]_out').hide();
						$('.$editfnA[$i]_yt').show();
					}else{
						$('.$editfnA[$i]_out').show();
						$('.$editfnA[$i]_yt').hide();
					}
				})

				$('input[name=\"$editfnA[$i]_ytsubmit\"]').click(function(){
					if($('input[name=\"$editfnA[$i]_ytname\"]').val()!=''){
						$editfnA[$i].yt_show($('input[name=\"$editfnA[$i]_ytname\"]').val());
					}
					$('input[name=\"$editfnA[$i]_ytname\"]').val('');
				})
				</script>";

				//組出資料-------------
				foreach($photo_list as $k=>$v){
					if($editfnA[$i] == 'photo' && $v['videoType']==''){//圖片
						//處理已存在的檔案//圖片和影片要分開
						echo '<div style="clear:both;margin:10px 0;">
							<div style="float: left;margin: 0 10px;cursor:pointer" onClick="delete_pic_before(this,'.$v['id'].')">
							<img src="/images/remove-icon.png" width="20" alt="刪除">
							</div>
							<img width="100" src="'.$ulpath.$r[$tableKey]."/".$v["photo_m"].'" style="float:left">
							<div style="float: left;margin: 0 0 0 10px;width:500px;">
							標題:<input type="text" name="'.$editfnA[$i].'_title_before[]" style="width:100%;margin: 0 0 10px 0;" value="'.$v["title"].'">
							描述:<textarea rows="5" style="width:100%" name="'.$editfnA[$i].'_desc_before[]">'.$v["description"].'</textarea>
							<input name="'.$editfnA[$i].'_id[]" type="hidden" value="'.$v['id'].'">
							</div>
							<div style="clear:both">
							</div>
							</div>';
					}elseif($editfnA[$i] == 'video' && $v['videoType']!=''){//影片
						echo '<div style="clear:both;margin:10px 0;">
							<div style="float: left;margin: 0 10px;cursor:pointer" onClick="delete_pic_before(this,'.$v['id'].')">
							<img src="/images/remove-icon.png" width="20" alt="刪除">
							</div>';
						if($v['videoType']=='GAE'){
							echo '<img width="100" src="../../images/video_icon.png" style="float:left">';	
						}elseif($v['videoType']=='youtube'){
							echo '<img width="100" src="'.$v['purl'].'" style="float:left">';	
						}
						echo '<div style="float: left;margin: 0 0 0 10px;width:500px;">
							標題:<input type="text" name="'.$editfnA[$i].'_title_before[]" style="width:100%;margin: 0 0 10px 0;" value="'.$v["title"].'">
							描述:<textarea rows="5" style="width:100%" name="'.$editfnA[$i].'_desc_before[]">'.$v["description"].'</textarea>
							<input name="'.$editfnA[$i].'_id[]" type="hidden" value="'.$v['id'].'">
							</div>
							<div style="clear:both">
							</div>
							</div>';
					}
				}
				break;
		 case "deptext" :		
  		 	echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>";
				echo "<input type='button' value='部門' onclick='openWindow(this)'>";
			break;	
			case "tag" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='$sTag'>"; break;
			case "date" :
				if($r[$editfnA[$i]]=='' || $r[$editfnA[$i]]=='0000-00-00'){
					$date_edit= '';
				}else{
					$date_edit= date('Y/m/d',strtotime($r[$editfnA[$i]]));
				}
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' id='".$editfnA[$i]."' size='$editflA[$i]' value='".$date_edit."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
		}
  ?></td>
  </tr><? } ?>

  <tr><td align="right" class="colLabel">授權</td><td><input name="PID" type="text" class="queryID" id="pmtID" size="6" value="<?=$r[PID]?>" /></td></tr>
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" id="selid" name="selid" value="<?=$ID?>">
  	<input type="hidden" id="ClassID" name="ClassID" value="<?=$rC["ID"]?>">
  	<input type="hidden" id="pages" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" id="keyword" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" id="fieldname" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" id="sortDirection" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <button type="button" class="btn" onClick="history.back()">取消編輯</button>
  </td>
</tr>
</table>
</form>
</body>
</html>