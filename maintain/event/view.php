<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?></title>
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script> 
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	

  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else parent.location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	
	function clickEdit(aid) {	selid = aid;	DoEdit();	}	

	function DoOpen() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else open('photo_details.php?id='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>","_blank"); 
	}

	function doAdvFilter() {
		var obj = new Object();  
		var retval = window.showModalDialog("query.php",obj,"dialogWidth=650px;dialogHeight=400px");  
		if(retval == undefined) retval = window.returnValue;  
		if(retval) {
			location.href="list.php?advFilter="+retval;
		}
	}

	$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) {
			SelRow('',{});
			return;
		}
		
		<? if ($_REQUEST[id]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[id]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
			evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
		$('.InputDate').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true, regional: "zh-TW"});
	});	
	
	function doSubmit() {	form1.submit();	}
	function filterSubmit(tSel) {	tSel.form.submit();	}
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>

<div id="divHelp" style="display:none; overflow:auto; height:200px; width:80%; top:15%; left:9%; font-size:12px; position:absolute; background-color:#f0f0f0; padding:5px; border:solid 1px #808080">
	<table width="100%" bgcolor="#d0d0d0"><tr><td>影像管理說明</td><td align="right" onClick="$('#divHelp').hide()">關閉 Ⅹ</td></table>
	<p><?= nl2br(file_get_contents('../../data/event.txt'))?></p>
</div>


<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
  <tr><td class="headTX"><?=$pageTitle.' - '.$_SESSION[cnm]?></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
		<input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE && $_SESSION[$CLASS_PID] && $_SESSION[$CLASS_PID]!='%'?'sBtn':'suBtn'?>" onclick='parent.location.href="append.php";'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
    <input type="button" name="help" value="說明" class="sBtn" onClick="$('#divHelp').show()">
		<!--input type="button" name="btn" value="進階查詢" onClick="doAdvFilter()" /-->
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取項目，新增請先選取類別。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>

<table width="100%">
<tr><td width="150" valign="top"><? include('classid.php') ?></td><td valign="top"> 
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr bgcolor="#336699" height="25" valign="bottom" align="center" bordercolordark="#336699">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
  
<?
	$sql="select a.* from ($tableName a LEFT JOIN $classMapTable c ON c.$foreignKey=a.$tableKey) LEFT JOIN $tagTable t ON t.$foreignKey=a.$tableKey where 1";
	//過濾條件
	if(isset($filter)) $sql .= " And ($filter)";
  // 搜尋處理
  if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%'))";
  
  if (!empty($_REQUEST["new"])) $sql.=" and rdate >= '".date("Y-m-d", time() - (86400 * $_REQUEST["new"]))."'";
  if (!empty($_REQUEST["cid"])) $sql.=" and c.$classMapKey in (".$_REQUEST["cid"].")";
  if (!empty($_REQUEST["event_date_start"])) $sql.=" and event_date >= '".$_REQUEST["event_date_start"]."'";
  if (!empty($_REQUEST["event_date_end"])) $sql.=" and event_date <= '".$_REQUEST["event_date_end"]."'";
  if (!empty($_REQUEST["title"])) $sql.=" and title like '%".$_REQUEST["title"]."%'";
  if (!empty($_REQUEST["human"])) $sql.=" and human like '%".$_REQUEST["human"]."%'";
  if (!empty($_REQUEST["locate"])) $sql.=" and locate like '%".$_REQUEST["locate"]."%'";
  if (!empty($_REQUEST["description"])) $sql.=" and description like '%".$_REQUEST["description"]."%'";
  if (!empty($_REQUEST["tag"])) $sql.=" and t.tag in ('".str_replace(",","','",$_REQUEST["tag"])."')";
	
	if($_SESSION['empID']) $sql.=" and empID='$_SESSION[empID]'";

  $sql .= " group by $tableKey";  

  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	// echo $sql; //exit;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"';?> onDblClick="this.blur();DoOpen();">
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
    	<a href="details.php?id=<?=$r[id]?>" target="mainFrame"></a><a href="#" onClick="clickEdit(<?=$id?>); return false;"><?=$id?></a>
    </td>

		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "select": $tm=$$fnAry[$i]; echo $tm[$fldValue]; break;
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "cklist": $tm=$$fnAry[$i]; foreach($tm as $k=>$v) if($fldValue & $k) echo $v.'&nbsp;'; else echo "&nbsp;";  break;						
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						default : echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
 
</td></tr></table>
</form>

<form name="excelfm" action="excel.php" method="post" target="excel">
<input type="hidden" name="sqltx" value="<?=$outsql?>">
</form>

<div align="right"><a href="#top">Top ↑</a></div>
</body>
</Html>