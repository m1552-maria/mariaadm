<?php
	$daysA = array(3=>'最近3天',7=>'最近一周',30=>'最近一個月',60=>'最近二個月',9999=>'全部');
	
	include '../../config.php';
	include('../../getEmplyeeInfo.php');

	//授權
	$ID  = $_SESSION[empID];
	
	//建立上層查詢命令
	$dID = $_SESSION[depID];	
	$dIDA= explode('-',$dID);
	$n = count($dIDA);
  	$whDIP = "instr(D.depPM,'".$dIDA[0].":')";	
	if(strlen($dIDA[0])>1) { $tid = substr($dIDA[0],0,1).':'; $whDIP .= " or instr(D.depPM,'$tid')"; }	
	if($n>1) { $tid = $dIDA[0].'-'.$dIDA[1].':';	$whDIP .= " or instr(D.depPM,'$tid')"; }
	if($n>2) { $tid = $dIDA[0].'-'.$dIDA[1].'-'.$dIDA[2].':'; $whDIP .= " or instr(D.depPM,'$tid')"; }
		
	$jID = $_SESSION[jobID];

	// $sql = "select A.* from information A left join permits B on A.PID=B.id where (PID=0 or instr(B.empPM,'$ID:') or $whDIP or instr(B.jobPM,'$jID:')) and ClassID like '$ClassID%' and beginDate<Now() and (unValidDate>Now() or unValidDate=0) and IsReady=1"; 
	// if($_REQUEST[Submit] && $_REQUEST[keyword]) {
	// 	$key = $_REQUEST[keyword];
	// 	$sql .= " and (A.title like '%".$key."%' or SimpleText like '%".$key."%')"; 
	// } 
	// $sql .= " order by ClassID,Seque";


	//處理頁數---------------------------------------------------------start
	// $pageSize = 21; //一頁幾筆
	$pageSize = 21; //一頁幾筆
	if (!isset($_REQUEST['pages'])) $pages = 1; else $pages = $_REQUEST['pages']; //目前在第幾頁
	//處理總頁數
	if((isset($_REQUEST[dateB])&&$_REQUEST[dateB]!='' &&isset($_REQUEST[dateE])&&$_REQUEST[dateE]!='')) { //Search//因為有可能沒有填任何東西
		$keywd =  $_REQUEST['keyword'];
		$sql="SELECT count(*) FROM (`event` A left join event_tag B on A.id=B.eid) left join event_photo C on A.id=C.eid left join permits D on  A.PID=D.id";
    $sql.=" where (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) and (A.title like '%$keywd%' or locate like '%$keywd%' or human like '%$keywd%' or A.description like '%$keywd%' or tag like '%$keywd%') ";
		if(isset($_REQUEST[dateB])&&$_REQUEST[dateB]!='' &&isset($_REQUEST[dateE])&&$_REQUEST[dateE]!=''){
			$sql.=" and event_date between '$_REQUEST[dateB]' and '$_REQUEST[dateE]' ";
		}
		if($_REQUEST['depFilter']) $sql .= "and INSTR(deps,'$_REQUEST[depFilter]') ";
		$sql .= "group by A.id";
	} else { //Classes
		if($_REQUEST[cid]) {	//使用內容分類
			$cid=$_REQUEST[cid];
			$cid_list = getChildClass($_REQUEST[cid]);
			if(isset($cid_list) && count($cid_list)>1){
				//把自己的id也加進去
				array_push($cid_list, $_REQUEST[cid]);
			}else{
				$cid_list = array();
				//把自己的id也加進去
				array_push($cid_list, $_REQUEST[cid]);	
			}
			$sql = "select count(*) from (event A left join event_class_map C on C.eid=A.id) left join event_photo B on A.id=B.eid left join permits D on  A.PID=D.id where (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) and C.cid in(".implode(',', $cid_list).") group by A.id";
		} else if($_REQUEST[days]) { //使用近期
			$days = $_REQUEST[days];
			$sql = "select count(*) from event A left join event_photo B on A.id=B.eid left join permits D on  A.PID=D.id where (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) and event_date>DATE_SUB(Now(), INTERVAL $days DAY) group by A.id";
		} else {
			$sql = "select count(*) from event A left join event_photo B on A.id=B.eid left join permits D on  A.PID=D.id where (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) group by A.id";
		}
	}
	$rs = db_query($sql,$conn);
	$recordCt = db_num_rows($rs);//總筆數

	$pageCount=(int)($recordCt / $pageSize)-1; if($pageCount<0) $pageCount=0;
	if(($recordCt % $pageSize) and ($recordCt>$pageSize)) $pageCount++; 
	//pageCount 總頁數 重0開始的話


	//處理頁數---------------------------------------------------------end
	$sql = "select * from event_class where parent=0";
	$rs = db_query($sql);
	
	if((isset($_REQUEST[dateB])&&$_REQUEST[dateB]!='' &&isset($_REQUEST[dateE])&&$_REQUEST[dateE]!='')) { //Search
		$keywd =  $_REQUEST['keyword'];
		$sql="SELECT A.id ID,A.title,C.photo_s FROM (`event` A left join event_tag B on A.id=B.eid) left join event_photo C on A.id=C.eid left join permits D on  A.PID=D.id";
		$sql.=" where (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) and (A.title like '%$keywd%' or locate like '%$keywd%' or human like '%$keywd%' or A.description like '%$keywd%' or tag like '%$keywd%') ";
		if(isset($_REQUEST[dateB])&&$_REQUEST[dateB]!='' &&isset($_REQUEST[dateE])&&$_REQUEST[dateE]!=''){
			$sql .="and event_date between '$_REQUEST[dateB]' and '$_REQUEST[dateE]' ";	
		}
		
		if($_REQUEST['depFilter']) $sql .= "and INSTR(deps,'$_REQUEST[depFilter]') ";
		$sql .= "group by A.id";
		$sql .= " limit ".$pageSize*($pages-1).",$pageSize"; 
		$captxt = "搜尋 ： $keywd";
	} else { //Classes
		if($_REQUEST[cid]) {	//使用內容分類
			$cid=$_REQUEST[cid];
			$cid_list = getChildClass($_REQUEST[cid]);
			if(isset($cid_list) && count($cid_list)>1){
				//把自己的id也加進去
				array_push($cid_list, $_REQUEST[cid]);
			}else{
				$cid_list = array();
				//把自己的id也加進去
				array_push($cid_list, $_REQUEST[cid]);	
			}
			$sql = "select A.id as ID,A.title,B.photo_s from event as A left join event_class_map as C on C.eid=A.id left join (select * from event_photo where videoType is null) as B on A.id=B.eid left join permits D on  A.PID=D.id where (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) AND C.cid in(".implode(',', $cid_list).") group by A.id";
		} else if($_REQUEST[days]) { //使用近期
			$days = $_REQUEST[days];
			$sql = "select A.id ID,A.title,B.photo_s from event A left join (select * from event_photo where videoType is null) B on A.id=B.eid left join permits D on  A.PID=D.id where (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) AND event_date>DATE_SUB(Now(), INTERVAL $days DAY) group by A.id";
		} else {
			$sql = "select A.id ID,A.title,B.photo_s from event A left join (select * from event_photo where videoType is null) B on A.id=B.eid left join permits D on  A.PID=D.id WHERE (PID=0 or instr(D.empPM,'$ID:') or $whDIP or instr(D.jobPM,'$jID:')) group by A.id ";
		}
		$sql .= ' order by A.event_date DESC,A.rdate DESC';
		$sql .= " limit ".$pageSize*($pages-1).",$pageSize"; 
		$captxt = $_REQUEST['cname']?$_REQUEST['cname']:'全部';
		if($days) $captxt .= ' - '.$daysA[$days];
	}
	
	// echo $sql;
	$rsD = db_query($sql);

	//處理頁碼
	include("../../pagination.php");
	$pagination = new Pagination();
	$pagination->total = ($pageCount+1);
	$pagination->page = $pages;
	$pagination->limit = 1;
	$pagination->url = $PHP_SELF.'?pages={page}&keyword='.$_REQUEST['keyword'].'&cid='.$cid.'&cname='.$_REQUEST['cname'].'&dateB='.$_REQUEST['dateB'].'&dateE='.$_REQUEST['dateE'].'&depFilter='.$_REQUEST['depFilter'].'&days='.$_REQUEST['days'];
	$page_list = $pagination->render2();


	$nTreeRecursiveLimit = 0;
	//抓底層id
	function getChildClass($nID = 0) {
		global $nTreeRecursiveLimit;
		$nTreeRecursiveLimit++;
		if ($nTreeRecursiveLimit > 100) return false;	
		$rs = db_query("select ID as ClassID from event_class where parent='$nID'");
		if(db_eof($rs)) return false;
		$aAllChild = array();
		while($r=db_fetch_array($rs)) {
			array_push($aAllChild, $r["ClassID"]);
			$aChild = getChildClass($r["ClassID"]);
			if ($aChild) $aAllChild = array_merge($aAllChild, $aChild);
		}
		$nTreeRecursiveLimit--;
		return $aAllChild;
	}



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>影像事件瀏覽</title>
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<style>
body {font-family:"微軟正黑體", Verdana;} 
.sText { font-size:14px; background-color:#eee; }
.mText { 
	font-size:14px; font-weight:bold; 
	background-color:#ccc; padding:4px; 
	cursor:pointer; margin-right:5px;
	border-radius: 5px;
 }
.topClass { float:left; width:100px; font-size:13px; margin-right:3px; background-color:#ddd; padding:2px; cursor:pointer; }
.classItem { display:none; text-align:center; line-height:25px; cursor:pointer; position:absolute; background-color:#f0f0f0; font-size:12px;}
.page_number {	text-align: center; }
.page_number ul{ text-decoration: none;	list-style: none;	padding: 0;	margin: 0; }
.page_number ul li{	display: inline-block;	padding: 10px;	cursor: pointer; }
.page_number li.active{	color:red; }

table th{
	text-align: center;
	padding: 5px;
}
table td{
	padding: 5px;
	margin: 10px;
	word-break: break-all;
}
</style>

<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
<script>
	var curCls;
	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$_REQUEST['keyword']?>';
	var cancelFG = true;	

 $(function() {
	 $(".classItem td").hover(
	 	function(){ 
			if (!$(this).is(".selected")) $(this).css({"background-color":"#7ED3E2"});
		},
		function(){
			if (!$(this).is(".selected")) $(this).css({"background-color":""});
		}
	 ).click(function(){ 
		 if(event.target.tagName=='SPAN') return false;
		 if(event.target.tagName=='LI') {
			var cname = event.target.innerText;
			var acid = event.target.title;
		 } else {
		 	var cname = this.innerText;
		 	var acid = this.title;
		 }
		 location.href="browser.php?cid="+acid+"&cname="+cname;
	});
 });
 
 function doSelClass(obj) {
	 if(curCls!=obj) {
	 	 $('.classItem').hide();
		 curCls=obj;
	 } 
	 if ($(obj).next().is(":visible")) $(obj).next().fadeOut(300); else $(obj).next().fadeIn(300);
 }
 
 	function DoOpen(selid) { 
		open('photo_details.php?id='+selid,"_blank"); 
	}
	
	function doDateRange(opts) {
		var days = opts[opts.selectedIndex].value;
		location.href="browser.php?days="+days;
	}
	
	function formValid(tfm) {
		//if(!tfm.keyword.value) {alert('請輸入關鍵字'); return false;} 
		return true;
	}

	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
			case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
			case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
			case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$_REQUEST['keyword']?>&cid=<?=$cid?>&cname=<?=$_REQUEST['cname']?>&dateB=<?=$_REQUEST['dateB']?>&dateE=<?=$_REQUEST['dateE']?>&depFilter=<?=$_REQUEST['depFilter']?>&days=<?=$_REQUEST['days']?>";
	}

	function page_number(p) {
		location.href="<?=$PHP_SELF?>?pages="+p+"&keyword=<?=$_REQUEST['keyword']?>&cid=<?=$cid?>&cname=<?=$_REQUEST['cname']?>&dateB=<?=$_REQUEST['dateB']?>&dateE=<?=$_REQUEST['dateE']?>&depFilter=<?=$_REQUEST['depFilter']?>&days=<?=$_REQUEST['days']?>";
	}

	function imgError(elm) {
    elm.src = "../../images/no-image.png";
  }
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><span class="mText" onClick="location.href='browser.php'">全部</span></td>
    <td>
      <? while($r=db_fetch_array($rs)) { 
        $sql = "select * from event_class where parent=$r[ID] order by Seque";
        $rs2 = db_query($sql);
      ?>
      <div class="topClass" align="center">
			<a href="browser.php?cid=<?=$r['ID']?>&cname=<?=$r[Title]?>"><?=$r[Title]?></a>
			<span onClick="doSelClass(this)"> ▼</span>
			<table class="classItem" style="border:#ccc 1px solid; width:100px;z-index: 4;">
				<? while($r2=db_fetch_array($rs2)) { ?>
					<tr>
						<? 
							$l3 = '';
							$SQL = "select * from event_class where parent= $r2[ID] order by Seque";
							$rs3 = db_query($SQL);
							while($r3=db_fetch_array($rs3)) $l3.="<li style='margin-left:-20px' title='$r3[ID]'>$r3[Title]</li>";
						?>	
						<td title="<?=$r2['ID']?>" align="left"><?=$r2['Title']?>
						<? if($l3) { ?>
							<span id="<?=$r2['ID'] ?>" onClick="$(this).next().toggle()"> ►</span> 
							<ul style="white-space: nowrap; display:none"><?=$l3?></ul>
						<? } ?>
						</td>
					</tr>
				<? } ?>  
			</table>
		</div>
	  <? } ?>  		
    </td>
    <td align="center">
  <select name="new" onChange="doDateRange(this)">
  <option value="9999" <?=$days==9999?'selected':''?>><?=$daysA[9999]?></option>
  <option value="3" <?=$days==3?'selected':''?>><?=$daysA[3]?></option>
  <option value="7" <?=$days==7?'selected':''?>><?=$daysA[7]?></option>
  <option value="30" <?=$days==30?'selected':''?>><?=$daysA[30]?></option>
  <option value="60" <?=$days==60?'selected':''?>><?=$daysA[60]?></option>
	</select></td>
  </tr>
</table>
<div align="right" style="padding-right:10px">
	<form style="margin:0" onSubmit="return formValid(this)">
  	<input type="date" name="dateB" value="<?= $_REQUEST['dateB']?$_REQUEST['dateB']:date('Y-m-d',strtotime('-30 days')) ?>" size="10"> ~
    <input type="date" name="dateE" value="<?= $_REQUEST['dateE']?$_REQUEST['dateE']:date('Y-m-d')?>" size="10">
    <input type="hidden" name="cid" value="<?=$cid?>" />
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>"; ?>
    </select>
  	<input type="text" name="keyword" value="<?=$keywd?>" />&nbsp;<input type="image" src="/images/search.jpg" name="Submit" value="搜尋" align="absmiddle" /> 搜尋
  </form>
</div>

<div style="border:#ccc 1px solid; border-radius:10px;padding: 5px;width: 98%;">
<table width="100%" cellspacing="0" cellpadding="8" >
	<tr><th colspan="7"><div style="width:100%; background-color:#f0f0f0"><?=$captxt?></div></th></tr>
  <tr valign="top">
  <? $count=1; while($rD=db_fetch_array($rsD)) { ?>
    <td><table bgcolor="#F0F0F0" class="sText" onClick="DoOpen(<?=$rD[ID]?>)" style="cursor: pointer;">
    <tr><td align="center"><img src="/data/event/<?=$rD[ID]?>/<?=$rD[photo_s]?>" onerror='imgError(this)'></td></tr>
		<tr><td align="center">
		<?
			//判斷字串長度
			$point = '';
			if(mb_strlen($rD[title],'utf-8')>12){
				$point = '...';
			};
			//取15字元
			echo mb_substr($rD[title], 0,12,"UTF-8").$point;
		?>
		</td></tr>
    </table></td>
  <? $count++; if(($count%8)==0) {echo "</tr><tr valign='top'>"; $count=1;} } ?>  
  </tr>
</table>
</div>
<!-- 頁碼 -->
<div class="col-md-12" style="text-align: center;">
	<?echo $page_list;?>
</div>

</body>
</html>
