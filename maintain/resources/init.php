<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include("../../config.php");
	include('../../miclib.php');	
	include('../inc_vars.php');	
	$pageTitle = "資源資料庫管理";
	$tableName = "resources";
	$tableKey = "id";
	$foreignTable = "resource_detail";
	$foreignKey = "RID";
	$extfiles = '';				//js及css檔的路徑位置
	
	$PID = 'ClassID';
	$CLASS_PID = 'resource_class_PID';
	// for classid.php
	$className = "resource_class";
	$ID  = 'ID';
	$Title = 'Title';

	// Here for List.asp ======================================= //
	//if($_SESSION['domain_Host']) {
		//$RootClass = $_SESSION['user_classdef'][0];
		//$rsT = db_query("select Title from resource_class where ID='$RootClass'"); 
		//$rT = db_fetch_array($rsT); 
		//$ROOT_NAME = $rT[0];
	//} else {
		$RootClass = 0;
		$ROOT_NAME = '資源管理';		
	//}
	if (isset($_REQUEST[filter])) $_SESSION[$CLASS_PID]=$_REQUEST[filter]; 
	if (!$_SESSION[$CLASS_PID]) $_SESSION[$CLASS_PID]=$RootClass;
	$filter = "$PID='$_SESSION[$CLASS_PID]'";
	if ($_REQUEST[cnm]) $_SESSION[cnm]=$_REQUEST[cnm]; else {
		if(count($_REQUEST)<2 || $_SESSION[$CLASS_PID]==0) $_SESSION[cnm] = $_SESSION[$CLASS_PID]=='%'?'所有類別':$ROOT_NAME;
	}
	
	// for Upload files and Delete Record use
	$ulpath = "../../data/resources/$_SESSION[$CLASS_PID]/";
	if (count($_FILES)>0) { //有上傳圖檔時才需	
	  if(!is_dir($ulpath)) mkdir($ulpath, 0755); 
	}
	$delField = 'file1';
	$delFlag = true;
	
	// Here for List.asp ======================================= //
	/*
	if($_SESSION['privilege']<=100)	{
		$PRIVILEGE = array(1=>'基本帳號',100=>'部門管理');
	} else $PRIVILEGE = array(1=>'基本帳號',10=>'部門管理',100=>'機構管理',110=>'人事',255=>'系統總管理');
	*/
	
	$defaultOrder = "id Desc";
	$searchField1 = "name";
	$searchField2 = "empName";
	$pageSize = 20;	//每頁的清單數量
	
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,name,Title,RDate,empName,click_count,grades,longitude");
	$ftAry = explode(',',"編號,名稱,資源類別,修改日期,操作者,點擊數,分數,GPS座標");
	$flAry = explode(',',"60,,,,,,,");
	$ftyAy = explode(',',"ID,text,text,text,text,text,text,latlon");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"isReady");
	$newftA = explode(',',"地圖顯示");
	$newflA = array(
		'sub_class'=>'30',
		'email'=>'60',
		'address'=>'60',
		'PosCode'=>'5',
		'contact_address'=>'60',
		'experience'=>'60',
		'skill'=>'60',
		'target'=>'60',
		'business'=>'60',
		'apply_doc'=>'60',
		'service_status'=>'60',
		'notes'=>'60',
		'notes2'=>'60',
		'notes3'=>'60',
		'notes4'=>'60',
		'notes5'=>'60',
		'notes6'=>'60',
		'notes7'=>'60',
		'file1'=>'60'
	);
	$newfhA = array(
		'experience'=>'2',
		'skill'=>'2',
		'business'=>'3',
		'apply_doc'=>'5',
		'service_status'=>'5',
		'notes'=>'3',
		'notes2'=>'3',
		'notes3'=>'3',
		'notes4'=>'3',
		'notes5'=>'3',
		'notes6'=>'3',
		'notes7'=>'3'
	);
	$newfvA = array(
		'county'=>$county,
		'area' =>$area,
		'isReady'=>1
	);
	$newetA = array(
		'city'=>'select',
		'county'=>'select',
		'area'=>'select',
		'experience'=>'textbox',
		'skill'=>'textbox',
		'business'=>'textbox',
		'apply_doc'=>'textbox',
		'service_status'=>'textbox',
		'notes'=>'textbox',
		'file1'=>'multiPhoto',
		'isReady'=>'checkbox'
	);

	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"isReady");
	$editftA = explode(',',"地圖顯示");
	$editflA = array(
		'sub_class'=>'30',
		'email'=>'60',
		'address'=>'60',
		'PosCode'=>'5',
		'contact_address'=>'60',
		'experience'=>'60',
		'skill'=>'60',
		'target'=>'60',
		'business'=>'60',
		'apply_doc'=>'60',
		'service_status'=>'60',
		'notes'=>'60',
		'notes2'=>'60',
		'notes3'=>'60',
		'notes4'=>'60',
		'notes5'=>'60',
		'notes6'=>'60',
		'notes7'=>'60',
		'file1'=>'60'		
	);
	$editfhA = array(
		'experience'=>'2',
		'skill'=>'2',
		'business'=>'3',
		'apply_doc'=>'5',
		'service_status'=>'5',
		'notes'=>'3',
		'notes2'=>'3',
		'notes3'=>'3',
		'notes4'=>'3',
		'notes5'=>'3',
		'notes6'=>'3',
		'notes7'=>'3'		
	);
	$editfvA = array(
		'county'=>$county,
		'area'=>$area,
		'isReady'=>1
	);
	$editetA = array(
		'city'=>'select',
		'county'=>'select',
		'area'=>'select',
		'experience'=>'textbox',
		'skill'=>'textbox',
		'business'=>'textbox',
		'apply_doc'=>'textbox',
		'service_status'=>'textbox',
		'notes'=>'textbox',
		'file1'=>'multiPhoto',		
		'isReady'=>'checkbox'
	);
?>