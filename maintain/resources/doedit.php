<?php
	@session_start();
	include("init.php");
	
	$mainID = $_REQUEST["selid"];
	$rdID = $_REQUEST['rdID'];
	//處理上傳的新圖檔
	$dstfn = $ulpath.$mainID."/"; if(!is_dir($dstfn)) mkdir($dstfn, 0755); 
	foreach($_FILES as $k=>$v) {
		if( is_array($_FILES[$k]['name']) ) {
			$fnLst = array();	//多檔上傳
			foreach($_FILES[$k]['name'] as $k2=>$v2) {
				$fn = $v2;
				if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
				if ($_FILES[$k][tmp_name][$k2] != "") {
					if (!move_uploaded_file($_FILES[$k]['tmp_name'][$k2],"$dstfn$fn2")) { //上傳失敗
						echo '附件上傳作業失敗 !'; exit;
					}	else $fnLst[] = $fn;
				}				
			}
		} else {	//單檔上傳
			$fn = $_FILES[$k]['name'];
			if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
			if ($_FILES[$k][tmp_name] != "") {
				if (!move_uploaded_file($_FILES[$k]['tmp_name'],"$dstfn$fn2")) { //上傳失敗
					echo '附件上傳作業失敗 !'; exit;
				}	
			}
		}	
	}

	//先查詢類別檔所有欄位
	$sql = "select * from resource_class where ID='".$_REQUEST["ClassID"]."'";
	$rsC = db_query($sql, $conn);
	$rC = db_fetch_array($rsC);
	$aFields = json_decode($rC["fields"]);
	$editfnA = array();
	foreach($aFields as $k=>$v) { $editfnA[] = $k; $editftA[] = urldecode($v); }
	
	if($_REQUEST[ckHist]==1) {	//需建立歷史資料
		//更新主檔
		$sql = "update $tableName set isReady = '".$_REQUEST["isReady"]."' where id = '".$mainID."'";
		db_query($sql, $conn);
	
		//新增子檔
		$sql = "insert into $foreignTable";
		$fields = "RID,empID,longitude,latitude,";
		$values = "'".$mainID."','".$_SESSION["empID"]."','".$_REQUEST["Longitude"]."','".$_REQUEST["Latitude"]."',";
		$n = count($editfnA);
		for ($i=0; $i<$n; $i++) {
			$fields = $fields . $editfnA[$i];
			if ($i<($n-1)) $fields = $fields.',';
			$fldv = "'".$_REQUEST[$editfnA[$i]]."'";
			switch($editetA[$editfnA[$i]]) {
				case "hidden" :
				case "select" :
				case "readonly" :
				case "textbox":
				case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
				case "textarea" :	$fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
				case "checkbox" : if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
				case "date" : $fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
				case "file" : $fldv = "'$fn'"; break;
				case "multiPhoto" : $fldv = "'".join(',',$fnLst)."'"; break;
			}	
			$values = $values . $fldv;
			if ($i<($n-1)) $values = $values.',';
		}
		$sql = $sql.'('.$fields.') values('.$values.')';
		db_query($sql, $conn);
	
		//刪除原先的檔案
		if($_REQUEST[delfns]) {
			$orgfns = $_REQUEST[orgfns];
			$ba = explode(',',$orgfns);		
			$aa = explode(',',$_REQUEST[delfns]);
			foreach($aa as $v) {
				$fnn = $v;
				if(checkStrCode($fnn,'utf-8')) $fnn=iconv("utf-8","big5",$fnn); 
				$fnn = $dstfn.$fnn;
				if(file_exists($fnn)) @unlink($fnn);
				for ($i=0; $i<count($ba); $i++) if($ba[$i]==$v) { unset($ba[$i]); break; }
			}
			//更新舊紀錄的圖檔清單
			$ba_Str = join(',',$ba);
			db_query("update $foreignTable set isDel=1,file1='".$ba_Str."' where ID=$rdID");
		} else db_query("update $foreignTable set isDel=1 where ID = $rdID");

	} else { //直接更新
		$orgfns = $_REQUEST[orgfns];
		$ba = explode(',',$orgfns);		
		//刪除原先的檔案
		if($_REQUEST[delfns]) {
			$aa = explode(',',$_REQUEST[delfns]);
			foreach($aa as $v) {
				$fnn = $v;
				if(checkStrCode($fnn,'utf-8')) $fnn=iconv("utf-8","big5",$fnn); 
				$fnn = $dstfn.$fnn;
				if(file_exists($fnn)) @unlink($fnn);
				for ($i=0; $i<count($ba); $i++) if($ba[$i]==$v) { unset($ba[$i]); break; }
			}
		}
		$fnLst = array_merge($ba,$fnLst);	//合併新舊檔案
		
		$sql = "update $foreignTable set ";
		$fields = "longitude=$_REQUEST[Longitude],latitude=$_REQUEST[Latitude],";
		$n = count($editfnA);
		for($i=0; $i<$n; $i++) {
			$fldv = "'".$_REQUEST[$editfnA[$i]]."'";
			switch($editetA[$editfnA[$i]]) {
				case "hidden" :
				case "select" :
				case "readonly" :
				case "textbox":
				case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
				case "textarea" :	$fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
				case "checkbox" : if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
				case "date" : $fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
				case "file" : $fldv = "'$fn'"; break;
				case "multiPhoto" : $fldv = "'".join(',',$fnLst)."'"; break;
			}
			$fields = $fields.$editfnA[$i].'='.$fldv;
			if ($i<($n-1)) $fields = $fields.',';			
		}
		$sql = $sql.$fields." where ID='$rdID'";
		//echo $sql;  exit;
		db_query($sql, $conn);
	}

	db_close($conn);	
	header("Location: index.php?pages=".$_REQUEST['pages']."&keyword=".$_REQUEST['keyword']."&fieldname=".$_REQUEST['fieldname']."&sortDirection=".$_REQUEST['sortDirection']."&selid=".$mainID);
?>
