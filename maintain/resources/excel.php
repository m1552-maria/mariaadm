<?
  if($_REQUEST['sqltx']) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");

    include("init.php");
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    
    
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
    // $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray(
    //     array(
    //         'fill' => array(
    //             'type' => PHPExcel_Style_Fill::FILL_SOLID,
    //             'color' => array('rgb' => '969696')
    //         ),
    //         'font'   => array('bold' => true,
    //             'size' => '12',
    //             'color' => array('argb' => 'FFFFFF')
    //         )
    //     )
    // );

    $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:G1");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    /*
    VERTICAL_CENTER 垂直置中
    VERTICAL_TOP
    HORIZONTAL_CENTER
    HORIZONTAL_RIGHT
    HORIZONTAL_LEFT
    HORIZONTAL_JUSTIFY
    */
    $objPHPExcel->getActiveSheet()->setCellValue('A1','資源資料庫管理 - 資源管理');

    //跑header 迴圈用
    $headerstart = 2;
    $header = array('A','B','C','D','E','F','G');
    $headervalue = array('流水號','名稱','資源類別','修改日期','操作者','點擊數','分數');

    foreach($header as $k=>$v){
      $objPHPExcel->getActiveSheet()->setCellValue($v.$headerstart,$headervalue[$k]);   
    }

    


    $i = 3;

    $sql = $_REQUEST[sqltx];
    $rs = db_query($sql);

    while($rs && $r=db_fetch_array($rs)) {
      
      $content = array(($i-2),$r[name],$r[Title],$r[RDate],$r[empName],$r['click_count'],$r[grades]);
      foreach($header as $k=>$v){
        $objPHPExcel->getActiveSheet()->setCellValue($v.$i,$content[$k]);
      }
      
      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
    }
    

    
    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    //設定欄寬

    $width = array(20,100,20,20,20,20,20);
    foreach($header as $k=>$v){
      $objPHPExcel->getActiveSheet()->getColumnDimension($v)->setWidth($width[$k]);  
    }
    
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->save('php://output');


  }


  
?>