<? 
	@session_start();
	error_reporting(0);
	include("init.php"); 

	$sql = "select * from resource_class where ID='".$_SESSION[$CLASS_PID]."'";
	$rsC = db_query($sql, $conn);
	$rC = db_fetch_array($rsC);
	
	$aFields = json_decode($rC["fields"]);
	foreach($aFields as $k=>$v) { $aFld[] = $k; $aName[] = urldecode($v); }
	$newfnA = array_merge($aFld, $newfnA);
	$newftA = array_merge($aName, $newftA);
?>
<html>
<head>
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}

 .photoItem { margin:5px 5px 5px 0; display:none; float:left; position:relative }
 .photoItem img { width:200px; }
 .photoDel { position:absolute; top:0; left:0; z-index:99; cursor:pointer }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script>
 <script src="multiphoto.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 新增</title>
</head>

<body class="page">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<input type="hidden" name="ClassID" value="<?=$_REQUEST["ClassID"]?>" />
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> - <?=$rC[Title]?> 【新增作業】</td></tr>

	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><? switch ($newetA[$newfnA[$i]]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='".$newflA[$newfnA[$i]]."' value='".$newfvA[$newfnA[$i]]."' class='input' readonly>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='".$newflA[$newfnA[$i]]."' rows='".$newfhA[$newfnA[$i]]."' class='input'>".$newfvA[$newfnA[$i]]."</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='".$newflA[$newfnA[$i]]."' rows='".$newfhA[$newfnA[$i]]."' class='input'>".$newfvA[$newfnA[$i]]."</textarea> <input type='button' value='編輯' onClick='HTMLEdit($newfnA[$i])'>"; break;
  		case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' value='".$newfvA[$newfnA[$i]]."' class='input'"; if ($newfvA[$newfnA[$i]]=="true") echo " checked"; echo '>'; break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='".$newflA[$newfnA[$i]]."' value='".$newfvA[$newfnA[$i]]."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
		 case "multiPhoto"    : 
				echo "<input type='file' name='".$newfnA[$i]."[]' size='$newflA[$i]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview'>";
				echo "<div class='photoItem'><img /><button type='button' class='photoDel'>取消</button></div>";
				echo "</div>";
				break;	
			case "select"  : echo "<select name='$newfnA[$i]' size='".$newflA[$newfnA[$i]]."' class='input'>"; foreach($newfvA[$newfnA[$i]] as $k=>$v) echo "<option value='$k'>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='".$newflA[$newfnA[$i]]."' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='".$newfvA[$newfnA[$i]]."'>"; break;
			default   : echo "<input type='text' name='$newfnA[$i]' size='".$newflA[$newfnA[$i]]."' value='".$newfvA[$newfnA[$i]]."' class='input'>"; break;
  	} ?></td>
  </tr><? } ?>
  <tr><td valign="top" class="colLabel" colspan="2">
  	經度：<input name="Longitude" type="text" id="Longitude" size="12" value="<?=$r["longitude"]?>" />&nbsp;
  	緯度：<input name="Latitude" type="text" id="Latitude" size="12" value="<?=$r["latitude"]?>" />	&nbsp;
    <input name="Btn" type="button" id="Btn" value="用地址查詢座標" onClick="doSearchMap();" />
    <input type="submit" value="確定新增" class="btn">&nbsp;
    <input type="reset" value="重新輸入" class="btn">&nbsp;
    <button type="button" class="btn" onClick="history.back()">取消新增</button>
		<div id="map_canvas" style="width:100%; height:500px; margin-top:10px;"></div>		
<!--- AIzaSyDnz30ZdxGYG3cq66GSBb-AqK1N3zbzS64  --->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyANhzSqoJ0q0lQEFGd9x9GNtsYJsAo5q_c"></script>
<script type="text/javascript">
	var canGo = false;
	$(function() {
		var nLon = <?=(empty($r["longitude"])) ? 120.649624 : $r["longitude"]?>;
		var nLat = <?=(empty($r["latitude"])) ? 24.162652 : $r["latitude"]?>;
		var sMessage = "<p style='height:25px;'>請移動圖標到指定位置，即可查詢圖標經緯度</p>";
		
		var latlng = new google.maps.LatLng(nLat, nLon);
		var myOptions = {
			zoom: 16,
			center: latlng,
			zoomControlOptions: {
      	style: google.maps.ZoomControlStyle.LARGE
    	},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		var marker = new google.maps.Marker({
			map:map,
			draggable:true,
			animation: google.maps.Animation.DROP,
			position: latlng,
			title: sMessage
		});
		
		// To add the marker to the map, call setMap();
		marker.setMap(map);  
		
		var infowindow = new google.maps.InfoWindow({
			content: sMessage
		});
		infowindow.open(map,marker);
		
		google.maps.event.addListener(marker, 'dragend', getGPS);
		function getGPS(event) {
			document.getElementById('Latitude').value = marker.getPosition().lat().toFixed(6);
			document.getElementById('Longitude').value = marker.getPosition().lng().toFixed(6);
		}
		
		window.googlemap = map;
		window.googlemaker = marker;
	});

	function doSearchMap() {
		sAddr = "";
		$("input").each(function(){
			if ($(this).attr("name").indexOf("address") >= 0) sAddr = this.value;
		});
		if (sAddr == "") return false;
		
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( { 'address': sAddr}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				window.googlemap.setCenter(results[0].geometry.location);
				window.googlemaker.setPosition(results[0].geometry.location);
				document.getElementById('Latitude').value = results[0].geometry.location.lat().toFixed(6);
				document.getElementById('Longitude').value = results[0].geometry.location.lng().toFixed(6);
			}
		});
	}
	
	//::檢查名稱重複
	$("input[name='name']").blur(function(){
		var elm = this;
		var strName = this.value; 
		if(strName) {
			$.ajax({
				url:'chkName.php?name='+strName+'&CLASS_PID=<?=$_SESSION[$CLASS_PID]?>',type:"GET",dataType:'text',
				success: function(rtn){
					if(rtn=='OK') canGo=true; else { canGo=false; elm.focus(); alert(rtn); }
				},
				error:function(xhr, ajaxOptions, thrownError){ alert(xhr.status+','+thrownError); }
			}); 
		}
	});
</script>
</td></tr>
</table>
</form>
</body>
</html>
