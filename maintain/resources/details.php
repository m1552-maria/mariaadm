<?php
	@session_start();
	error_reporting(0);
	include 'init.php';
	include '../inc_vars.php';

	$id = $_REQUEST[id];
	$sql = "select * from resources r, resource_detail rd where rd.RID=r.id And r.id='$id' and rd.isDel=0";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);

	$imgpath = "../../data/resources/$r[ClassID]/$id/";
	
	$sql = "select * from resource_class where ID='".$r["ClassID"]."'";
	$rsC = db_query($sql, $conn);
	$rC = db_fetch_array($rsC);
	
	$aFields = json_decode($rC["fields"]);
	foreach($aFields as $k=>$v) { $aFld[] = $k; $aName[] = urldecode($v); }
	$editfnA = array_merge($aFld, $editfnA);
	$editftA = array_merge($aName, $editftA);
	$editfnA[] = "longitude";
	$editftA[] = "經度";
	$editfnA[] = "latitude";
	$editftA[] = "緯度";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Details</title>
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script src="/Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script>
function formvalid(tfm) {
	tfm.params.value = window.parent.topFrame.location.search;
	return true;
}
</script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	background-color:#f8f8f8;
}
.fieldLabel { 
	background-color:#f0f0f0; text-align:center; width: 10em;
}
</style>
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="4" cellspacing="1" class="formTx">
        <tr>
          <td class="fieldLabel">資源類別</td>
          <td colspan="3"><?=$rC["Title"]?></td>
        </tr>
		<? for ($i=0; $i<count($editfnA); $i++) { ?>
		<? if ($i % 2 == 0) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>"><? } ?>
			<td class="fieldLabel"><?=$editftA[$i]?></td>
			<td><? switch($editetA[$editfnA[$i]]) { 
				case "textbox" : echo nl2br($r[$editfnA[$i]]); break;
				case "textarea": echo $r[$editfnA[$i]]; break;
				case "checkbox": $pic=$editfvA[$editfnA[$i]]?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;	
				case "file"    : echo $r[$editfnA[$i]]; break;
				case "select"  : echo  $editfvA[$editfnA[$i]][$r[$editfnA[$i]]]; break;
				case "hidden"  : echo "<!-- ". ($editfvA[$editfnA[$i]]==''?$r[$editfnA[$i]]:$editfvA[$editfnA[$i]]) ." -->"; break;
				case "date" : echo date('Y/m/d',strtotime($r[$editfnA[$i]]));
				case "multiPhoto" : 
					$orgfns = $r[$editfnA[$i]]; 
					if($orgfns) {
						$fnLst = explode(',',$orgfns);
						foreach($fnLst as $v) {
							$ext = strtolower(pathinfo($v,PATHINFO_EXTENSION));
							if($ext=='jpg' || $ext=='png' || $ext=='gif')	echo "<img width='100' src='$imgpath$v'/>"; else echo "<span style='font:bold 14px; padding:10px'>$v</span>";
						}
					} else echo "&nbsp;";
					break; 
				default : echo $r[$editfnA[$i]]; break;
			} ?></td>
		<? if ($i % 2 == 1) { ?></tr><? } ?>
		<? } ?>
      </table>
    </td>
  </tr>
  
</table>
</body>
</html>