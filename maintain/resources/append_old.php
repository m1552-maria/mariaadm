<? 
	session_start();
	include("init.php"); 
	$sql = "select * from resource_class order by ID";
	$rs = db_query($sql, $conn);
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <title><?=$pageTitle?> - 新增</title>
</Head>

<body class="page">
<form action="append_step2.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="1" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>
	<tr>
		<td align="right" class="colLabel">請先選擇資源類別：</td>
  	<td><select name="ClassID">
		<? while($r = db_fetch_array($rs)) echo "<option value='".$r["ID"]."'>".$r["Title"]."</option>"; ?>
	</select></td>
  </tr>
	
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<input type="submit" value="下一步" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
      <button type="button" class="btn" onClick="history.back()">取消新增</button>
		</td>
	</tr>
</table>
</form>
</body>
</html>
