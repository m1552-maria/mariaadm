<?
  if($_REQUEST['sqltx']) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");

    include '../../config.php';
    include('../../getEmplyeeInfo.php');  
    include('../inc_vars.php');  

    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

   	$fmActAry=array("0"=>"補休","1"=>"加班費");   
    $sql = $_REQUEST[sqltx];
  	$sql .= " order by depID,empID,aType";
  	$rs = db_query($sql);

    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
    $objPHPExcel->getActiveSheet()->getStyle('A4:N4')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:N1");
    $objPHPExcel->getActiveSheet()->mergeCells("B2:N2");
    $objPHPExcel->getActiveSheet()->mergeCells("B3:N3");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    
    $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:N2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A4:N4')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    /*
    VERTICAL_CENTER 垂直置中
    VERTICAL_TOP
    HORIZONTAL_CENTER
    HORIZONTAL_RIGHT
    HORIZONTAL_LEFT
    HORIZONTAL_JUSTIFY
    */
    $objPHPExcel->getActiveSheet()->setCellValue('A1',($_REQUEST[depID]?$departmentinfo[$_REQUEST[depID]]:'全部部門'));
    $objPHPExcel->getActiveSheet()->setCellValue('A2','製作日期');
    $objPHPExcel->getActiveSheet()->setCellValue('B2',date("Y/m/d"));
    $objPHPExcel->getActiveSheet()->setCellValue('A3','報表日期');

    $reg = "((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))";
    preg_match($reg, $sql, $match);
    $aa = explode('/',$match[0]);
    
    $objPHPExcel->getActiveSheet()->setCellValue('B3',$_SESSION['start_day'].'~'.$_SESSION['end_day']);
     

	$colAry=array("A4",'B4','C4',"D4","E4","F4","G4","H4","I4","J4","K4","L4","M4","N4");
	$titleAry=array("流水號",'員工編號','員工名稱',"所屬部門","加班別","類型","起始日期","結束日期","加班時數","事由","辦理情形","已核准","已申請銷單","已銷單");
	for($j=0;$j<count($colAry);$j++){
		$objPHPExcel->getActiveSheet()->setCellValue($colAry[$j],$titleAry[$j]);
	}

    $i = 5;
    $curEmp = '';
    $curTyp = '';
    $tHours=0;
    $count  = 1;
    $color = 0;
    
	$col2Ary=array("A",'B','C',"D","E","F","G","H","I","J","K","L","M","N");
	$wAry=array("20","20","20","20","20","20","20","20","20","20","20","20","20","20");
    while($rs && $r=db_fetch_array($rs)) {
      $eid = $r[empID];

      if($curEmp!=$eid) {
        $curEmp = $eid;
        $curTyp = ''; 
        $color=!$color;
      }

      if($curTyp!=$r[aType]) {
        $curTyp = $r[aType];
        if($count!=1){
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':H'.$i);
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,'時數合計：');
          $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
          $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$tHours);
          $objPHPExcel->getActiveSheet()->mergeCells('J'.$i.':N'.$i);
          $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          $i++;
        }
        $tHours = 0;    
      } 
      if(!$r[isDel] && $r[isOK]) $tHours += floatval($r[hours]);
      if($color != 0){
        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'efefef')
                )
            )
        );
      }
	  $valAry=array($count,$eid,$emplyeeinfo[$eid],$departmentinfo[$r[depID]],$overtimeType[$r[aType]],$fmActAry[$r[fmAct]],$r[bDate],$r[eDate],$r[hours],$r[content],$r[rspContent],$r[isOK]?'v':'',$r[isReqD]?'v':'',$r[isDel]?'v':'');
    for($j=0;$j<count($col2Ary);$j++){
      //echo 'val:'.$valAry[$j]."<br>";
	  	if($col2Ary[$j] == 'B'){
			//設定格式用
			$objPHPExcel->getActiveSheet()->setCellValueExplicit($col2Ary[$j].$i, $valAry[$j], PHPExcel_Cell_DataType::TYPE_STRING);
		}else{
      		$objPHPExcel->getActiveSheet()->setCellValue($col2Ary[$j].$i,$valAry[$j]);
		}
	  }

      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
      $count++;
    }
    //生成最後一筆
    $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':H'.$i);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,'時數合計：');
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$tHours);
    $objPHPExcel->getActiveSheet()->mergeCells('J'.$i.':N'.$i);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
   
  
    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    
    //設定欄寬
  	for($j=0;$j<count($col2Ary);$j++){
  		$objPHPExcel->getActiveSheet()->getColumnDimension($col2Ary[$j])->setWidth($wAry[$j]);
  	}

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->save('php://output');




  }


  
?>