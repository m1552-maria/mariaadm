<?
	include '../inc_vars.php';	
	$overtimeTypeStr="<select name='aType[]' >";
	foreach($overtimeType as $k => $v) $overtimeTypeStr.="<option value='$k'>$v</option>";
	$overtimeTypeStr.="</select>";
	$hoursOfMonth=46;
?>
<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?></title>
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
 <link href="tuning.css" rel="stylesheet" type="text/css" />
<style>
	.depID_list{	border-collapse: collapse;      max-width: none; margin:4px;}
	#d_list{	overflow-x:auto; max-height: 250px;}
	#d_batch{	
		width:765px;
		position: fixed;	
		top:6%;	
		left:100px;
		background: #efefef; 
		margin:6px;	
		display: none;
		background: #ffffff;
		border-radius: 4px;
		box-shadow: black;
		box-shadow: 1px 1px 5px 1px #999999;
		cursor: pointer;
	}
	#d_example{	
		position: fixed;	
		top:8%;	
		left:120px;
		background: #efefef; 
		margin:6px;	
		padding:0px 6px 6px 6px; 
		border-radius: 4px; 
		border:2px solid #999;
		display: none;
		padding: 8px;
		background: #ffffff;
		border-radius: 4px;
		box-shadow: black;
		box-shadow: 1px 1px 5px 1px #999999;
		cursor: pointer;
	}
	.ex_s_title{	color:#ffffff;	}
	table.grid td{	background: #ffffff;}
	.btn-apply {
		background-color: #e68a00;
		border: none;
		color: white;
		padding: 4px 8px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		margin: 4px 2px;
		cursor: pointer;
		border-radius: 6px;
	}

</style>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="../../Scripts/form.js" type="text/javascript"></script> 
<script type="text/javascript" src="tuning.js" ></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	var overtimeTypeStr="<?=$overtimeTypeStr?>";
	var empAry=[];
	var empIDAry=[];
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var typflt = $('select#aType option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var mtflt = $('select#month option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>&start_day=<?=$bDate?>&end_day=<?=$eDate?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(typflt) url = url+'&aType='+typflt;
		if(yrflt) url = url+'&year='+yrflt;
		if(mtflt) url = url+'&month='+mtflt;
		location.href = url;		
	}

	$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<? if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[empID]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
		
		$("#d_batch").draggable();
		$("#d_example").draggable();
	});

	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		$('input[name="type"]').val('dep');
		tSel.form.submit();
	}
	function show(val){
		$("#"+val).show();
	}
	function remove(now){
		$(now).parent().parent().remove();
	}

	function proBatch(data){
		if(data) {
			var depAry=[];
			var obj;
			var d_select='';
			empAry=[];
			$.each(data,function(e,v){
				if(v['type']=='depID'){
					d_select += '<input type="hidden" name="depID[]" value="'+v['id']+'">';
					depAry.push(v['id']);
				}else if(v['type']=='empID'){
					d_select += '<input type="hidden" name="empID[]" value="'+v['id']+'">';
					obj=new Object();
					obj.id=v['id'];
					obj.name=v['name'];
					empAry.push(obj);
				}
			})
			$("#d_select").html(d_select);
			if(depAry.length>0){
				$.ajax({
					url: 'func.php?act=getEmpByDep&deps='+depAry.join(","),
					type:"POST",
					dataType:'text',
					success: function(rs){
						if(rs){
							rs=JSON.parse(rs);
							$.each(rs,function(id,name){
								obj=new Object();
								obj.id=id;
								obj.name=name;
								empAry.push(obj);
							})
							proList();
						}
					},
					error:function(xhr, ajaxOptions, thrownError){  }
				});
			}else proList();
		}
	}
	function proList(){
		if(empAry.length>0) {
			$('.depID_list').empty();
			detail ="<tr><td colspan='8' style='text-align:left'><input  type='button' class='btn-apply' value='我要批次套用' onClick='show(\"d_example\")'/></td></tr>"
			detail += '<tr><td style="width:80px">員工</td><td style="width:110px">開始時間</td><td style="width:110px">結束時間</td><td>加班別</td><td>選擇</td><td style="width:30px">時數</td><td style="width:80px">加班事由</td><td>移除</td></tr>';
			$.each(empAry,function(k,v){
				detail +='<tr>';
				detail += '<td><input type="hidden" name="eID[]" value="'+v.id+'">'+v.name+'</td>';
				detail +='<td><input type="text" name="bDate[]" style="width:110px"/></td>';
				detail +='<td><input type="text" name="eDate[]" style="width:110px"/></td>';
				detail +="<td>"+overtimeTypeStr+"</td>";
				detail +="<td style='text-align:left'><input name='fmAct-"+v.id+"' type='radio' value='0' />補休<br><input name='fmAct-"+v.id+"' type='radio' value='1' />加班費</td>";
				detail +='<td><input type="text" name="hours[]" id="'+v.id+'" usedHour="" style="width:30px"/></td>';
				detail +='<td><textarea name="content[]" cols="10" rows="2"></textarea></td>';
				detail +='<td><img src="/images/del.png" width="25" onclick="remove(this)"/></td>';
				detail +='</tr>';
				$('.depID_list').append(detail);
				detail ='';
	    	empIDAry.push(v.id);
			});
    	$("[name='bDate[]']").datetimepicker();
    	$("[name='eDate[]']").datetimepicker();
			//::toDO not all is work
			$("[name='eDate[]']").on('click',function(){
				this.value = $(this).parent().prev().children()[0].value;
			});
		}
	}
	function hide(id){
		$('#'+id).hide();
	}
	function ck() {
		var bDates = $("[name='bDate[]']");
		var year=new Date($(bDates[0]).val()).getFullYear();
		var month=new Date($(bDates[0]).val()).getMonth()+1;
		//批次取回當月剩餘可加班時數
		$.ajax({
			url: 'func.php?act=get_batch_overtimeHours&empID='+empIDAry.join("','")+'&year='+year+'&month='+month,
			type:"POST",
			dataType:'text',
			success: function(rs){
				if(rs){
					rs=JSON.parse(rs);
					$.each(rs,function(id,hr){
						$("#"+id).attr("usedHour",hr);
					})
					var eDates = $("[name='eDate[]']");
					var hours = $("[name='hours[]']");
					var aType = $("[name='aType[]']");
					var empID=$("[name='eID[]']");
					var bDate,eDate,usedHour,fmActObj,isFmAct;
					for(var i=0;i<bDates.length;i++){
						bDate = $(bDates[i]).val();
						eDate = $(eDates[i]).val();
						if(!bDate) { alert('起始日期 不能為空白！'); $(bDates[i]).focus(); return; }
						if(!eDate) { alert('結束日期 不能為空白！'); $(eDates[i]).focus(); return; }
						var bDateA = bDate.split('/'); if(bDateA.length<3) {alert('起始日期 格式錯誤！'); $(bDates[i]).focus(); return ; }
						var eDateA = eDate.split('/'); if(eDateA.length<3) {alert('結束日期 格式錯誤！'); $(eDates[i]).focus(); return; }
						if( bDateA[2]>eDateA[2] ) {alert('結束日期 不能早於 起始日期'); $(bDates[i]).focus();return; }
						if( (bDateA[0]!=eDateA[0]) || (bDateA[1]!=eDateA[1]) ) {alert('請勿跨月請假，若有此需要請分單'); $(bDates[i]).focus();return; }
						if(!($(hours[i]).val()>0)) { alert('預計時數錯誤'); $(hours[i]).focus(); return; }
						if($(aType[i]).val()==0 && $(hours[i]).val()>4){	alert("上班日加班時數不得大於4小時"); $(hours[i]).focus();	return;}
						/* 已取消
						if($(aType[i]).val()==1){	
							if($(hours[i]).val()==4 || $(hours[i]).val()==8 || $(hours[i]).val()==12){
							}else{
								alert("休息日加班時數有誤(只能為4、8、12小時)");return;	
							}
						}*/
						
						fmActObj=$("[name='fmAct-"+$(empID[i]).val()+"']");
						isFmAct=false;
						for (var j=0; j<fmActObj.length; j++){
						   if (fmActObj[j].checked){
						      isFmAct=true;
						      break;
						   }
						}
						if(!isFmAct){
							alert('請選擇加班單的類型是補休或加班費?');
							$(fmActObj).focus();
							return;
						}
						usedHour=$(hours[i]).attr("usedHour");
						// console.log('usedHour:'+usedHour+",usedOFmonth:<?=$hoursOfMonth?>,剩餘hr:"+('<?=$hoursOfMonth?>'-usedHour-$(hours[i]).val()));
						if(('<?=$hoursOfMonth?>'-usedHour-$(hours[i]).val())<0){
							alert(month+'月可申請的加班時數剩餘：'+('<?=$hoursOfMonth?>'-usedHour)+"小時");
							$(hours[i]).val("");
							$(hours[i]).focus();
							return;
						}
				    	
					}
					$("#frm_batch").submit();
				}
			},
			error:function(xhr, ajaxOptions, thrownError){  }
		});
	}
	
	function set_batch_data(){
		var bDates = $("[name='bDate[]']");
		var eDates = $("[name='eDate[]']");
		var hours = $("[name='hours[]']");
		var aType = $("[name='aType[]']");
		var content = $("[name='content[]']");
		var content = $("[name='content[]']");
		var empID=$("[name='eID[]']");
		var ex_bDate=$("[name='ex_bDate']").val();
		var ex_eDate=$("[name='ex_eDate']").val();
		var ex_hours=$("[name='ex_hours']").val();
		var ex_content=$("[name='ex_content']").val();
		var ex_aType=$("#ex_aType").val();
		var ex_fmAct=$('input[type="radio"][name="ex_fmAct"]:checked').val();
		for(var i=0;i<bDates.length;i++){
			$(bDates[i]).val(ex_bDate);
			$(eDates[i]).val(ex_eDate);
			$(aType[i]).val(ex_aType);
			$(hours[i]).val(ex_hours);
			$(content[i]).val(ex_content);
			$('input:radio[name="fmAct-'+$(empID[i]).val()+'"][value="'+ex_fmAct+'"]').attr("checked", "checked");
		}
	}
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
 		<input type="hidden" name="type" value="">
 		<!-- 為了判斷是否用部門搜尋 -->
		<input type="hidden" name="keyword" value="<?=$keyword;?>">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <?if ($keyword!="") $ss = "搜尋：$keyword ".$ss;?>
    <label class="sText" style="color:red;"><?=$ss?></label>
    <input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'sxBtn'?>" onclick='location.href="append.php";'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="MsgBox()">
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>

    開始日期:<input type="text" name="start_day" id="start_day" onchange="doSubmit()" value="<?=$bDate?>">
    <script type='text/javascript'>
        $(function(){
            $('#start_day').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});
            $('#start_day').datepicker('option', 'duration', '');
            $('#start_day').datepicker($.datepicker.regional['zh-TW']);
        });
    </script>

    ~結束日期:<input type="text" name="end_day" id="end_day" onchange="doSubmit()" value="<?=$eDate?>"> 
    <script type='text/javascript'>
        $(function(){
            $('#end_day').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});
            $('#end_day').datepicker('option', 'duration', '');
            $('#end_day').datepicker($.datepicker.regional['zh-TW']);
        });
    </script>

    <select name="is_leadmen" id="is_leadmen" onChange="doSubmit()">
    	<?php foreach($isLeadmenList as $key => $val) {
    		$selected = ($is_leadmen == $key) ? 'selected' : '';
    		echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
    	}?>
    </select>

    <input type="button" name="toExcel" value="匯出Excel" onClick="excelfm.submit()">
    <input type="button" name="toExcely" value="匯入整批加班單" onClick="show('d_batch')">
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)";?>
  		<label class="sText"><?=$ss?></label>
  	</td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)) {
		$sql="select A.*,B.empName,B.depID from $tableName where $defWh and $filter";

		// $sqly="select A.*,B.empName,B.depID from $tableName where $defWhy and $filter";
	} else {
		$sql="select A.*,B.empName,B.depID from $tableName where $defWh";
		// $sqly="select A.*,B.empName,B.depID from $tableName where $defWhy";
	}
  // 搜尋處理
  if ($keyword!="") {
		$sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";
		// $sqly.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%'))";
	}
	// for excel output use.
	$outsql = $sql; //echo $outsql."<br>";

	// $outsqly = $sqly;//echo $outsqly."<br>";
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	//echo $sql; //exit;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><a href="details.php?id=<?=$r[id]?>" target="mainFrame"><?=$id?></a></td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? switch ($ftyAy[$i]) {
					case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
					case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
					case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
					case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
					case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
					case "datetime" : echo date('Y/m/d H:i',strtotime($fldValue)); break;
					case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
					case "empID" : echo $emplyeeinfo[$fldValue]; break;
					case "tuning":
						if($r[isOK]=="1" && $r[isDel]=="0")	{
							$flag1 = false;	//可調整
							$flag1Tx = " ";
							if( $r['fmAct']==0 ) { //補休
								$rsT = db_query("select * from absence_overtime where otId=$id");
								if(db_eof($rsT)) $flag1=true; else $flag1Tx='已請補休';
							} else { //加班費
								$wkMonth = date('Ym',strtotime($r['bDate']));
								$rsT = db_query("select * from acctclose where title='salary' and allClose=1 and conditions='$wkMonth'");
								if(db_eof($rsT)) $flag1=true; else $flag1Tx='已關帳';
							}
							if($flag1) echo '<input type="button" onclick="doTuning('.$r['id'].',\''.$emplyeeinfo[$r['empID']].'\')" value="調整"/>';
							else echo $flag1Tx;
						}
						break;
					case "logs" :	if($fldValue!="")	echo "<span title='$fldValue'>已調整</span>";	break; 	
					default : 
						if($fnAry[$i]=="aType")	echo $overtimeType[$r['aType']];	
						else if($fnAry[$i]=="fmAct"){
							echo $fmActAry[$fldValue];
						}	else echo $fldValue;	
			    } 
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>

<form name="excelfm" action="excel.php" method="post" target="excel">
 	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
	<input type="hidden" name="sqltx" value="<?=$outsql?>">
</form>

<div id='d_batch' class='divBorder1'>
    <div class='dtlTitle'>匯入整批加班單作業
    	<span style="float: right">請先選擇部門、員工&nbsp;<img class="queryID <?=$class?>" id="depID_other" data-ui-type='batch'/></span>
    </div>
    <form id='frm_batch' action="func.php" method="post">
    	<input type='hidden' name="act" value='add_overtime'/>
	    <div class='divBorder2' id='d_list' >
			<table class="depID_list grid" style='width:99%'><tr><td style='height:30px;'>尚未選擇部門、員工</td></tr></table>
	    </div>
	     <div class='divBottom'>
	     	<input type='button' class='red' onclick='ck()' value="確定"/>&nbsp;&nbsp;
	     	<input type='button' class='red' value="取消" onclick='hide("d_batch")'/></div>
	     </div>
	    <div id='d_select' style='display: none;'></div>
    </form>
</div>

<div id='d_example' >
	<table class='grid tb_example'>
		<tr><td colspan="6" style='background: #e68a00;'><span class='ex_s_title'>設定批次套用資料</span></td></tr>
		<tr>
			<td style="width:110px">開始時間</td>
			<td style="width:110px">結束時間</td>
			<td>加班別</td><td>選擇</td>
			<td style="width:30px">時數</td>
			<td style="width:80px">加班事由</td>
		</tr>
		<tr>
			<td style="width:110px"><input type='text' name='ex_bDate' style="width:110px"/></td>
			<td style="width:110px"><input type='text' name='ex_eDate' style="width:110px"/></td>
			<td><select name='ex_aType' id='ex_aType'>
				<?	foreach($overtimeType as $k => $v) { echo "<option value='$k'>$v</option>";	}?>
				</select>
			</td>
			<td style='text-align: left'><input name='ex_fmAct' type='radio' value='0' />補休<br><input name='ex_fmAct' type='radio' value='1' />加班費</td>
			<td style="width:30px"><input type='text' name='ex_hours' style="width:30px"/></td>
			<td style="width:80px"><textarea name="ex_content" cols="10" rows="2"></textarea></td>
			<script>
				$(function(){
					$("[name='ex_bDate']").datetimepicker();
					$("[name='ex_eDate']").datetimepicker();
				})
				</script>
	    </tr>	
		<tr><td colspan="6" class='ex_bottom'><input type='button' value='套用' onclick='set_batch_data()'/>&nbsp;<input type='button' value='取消' onclick="hide('d_example')" /></td></tr>
	</table>
</div>

<div id='d_tuning' class='divBorder1'>
    <div class='dtlTitle'>加班資料調整</div>
    <div class='divBorder2' id='d_list' >
    <input type="hidden" id='id'/>
    <table class='grid' >
		<tr align='center' class='tbTitle'><td>員工</td><td >時數</td><td>開始時間</td><td >結束時間</td><td >加班別</td><td >類型</td><td width='200'>調整原因</td><td>銷班</td></tr>
		<tr align='center'>
      <td id='td_empName' width='80'></td>
      <td><input type="text" size="2" id="hours" ></td>
      <td><input type="text" size="12" id="bDate"></td>
      <td><input type="text" size="12" id="eDate"></td>
      <td id='td_aType'></td>
      <td id='td_fmAct'></td>
      <td ><input type="text" size="22" id="reason" ></td>
      <td><input type='checkbox' value='1' name='isDel' class='opt'/></td>
		</tr>
    </table>
    </div>
     <div class='divBottom'>
     	<input type='button' class='red' onclick='save()' value="確定"/>&nbsp;&nbsp;
     	<input type='button' class='red' value="取消" onclick='hide("d_tuning")'/></div>
     </div>
</div>

</body>
</Html>