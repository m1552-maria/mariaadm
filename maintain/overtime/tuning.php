<?
session_start();
include "../../config.php";
include "../../getEmplyeeInfo.php";
include "../inc_vars.php";

$fmActAry=array("0"=>"補休","1"=>"加班費");
$act=$_REQUEST[act];
switch($act){
	case "getData":
		$id=$_REQUEST['id'];
		getData($id,$overtimeType, $_REQUEST['year']);
		break;
	case "saveData":
		$id=$_REQUEST['id'];
		$bDate=$_REQUEST['bDate'];
		$eDate=$_REQUEST['eDate'];
		$hours=$_REQUEST['hours'];
		$aType=$_REQUEST['aType'];
		$fmAct=$_REQUEST['fmAct'];
		$reason=$_REQUEST['reason'];
		$isDel=$_REQUEST['isDel'];
		saveData($id,$bDate,$eDate,$hours,$aType,$reason,$isDel,$fmAct);
		break;
}

function getData($id,$aType, $year){
	global $overtimeType,$fmActAry;
	$sql="select * from overtime where id=$id";
	$rs=db_query($sql);
	$ary=array();
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$ary[0]=$r['hours'];
		$ary[1]=date('Y/m/d H:i',strtotime($r['bDate']));
		$ary[2]=date('Y/m/d H:i',strtotime($r['eDate']));
		$ary[3]=$r['aType'];
		$ary[4]=$r['logs'];
		$ary[5]=date('H',strtotime($r['bDate']));
		$ary[6]=date('i',strtotime($r['bDate']));
		$ary[7]=date('H',strtotime($r['eDate']));
		$ary[8]=date('i',strtotime($r['eDate']));
	}
	$str='';
	foreach($aType as $k=>$v) {
		if($k==$ary[3]) $selected='selected';	else $selected='';
		$str.="<option value='".$k."' $selected >".$v."</option>";
	}
	$ary[9]=$str;
	
	$str='';
	foreach($fmActAry as $k=>$v) {
		if($k==$r['fmAct']) $selected='selected';	else $selected='';
		$str.="<option value='".$k."' $selected >".$v."</option>";
	}
	$ary[10]=$str;
	echo join("#",$ary);
}

function saveData($id,$bDate,$eDate,$hours,$aType,$reason,$isDel,$fmAct){
	global $overtimeType,$fmActAry;
	//判斷是否時間有誤，先抓目前這個人的empid
	$sql = 'select * from `overtime` where `id`='.$id;
	$rs = db_query($sql);
	if(!db_eof($rs)){	
		$r = db_fetch_array($rs);
		$empID = $r['empID'];	
		//原單日期
		$ob=date("Y-m-d",strtotime($r["bDate"]));
		$oe=date("Y-m-d",strtotime($r["eDate"]));
	}

	//調整後的日期
	$now_bDate = strtotime($bDate);
	$now_eDate = strtotime($eDate);
	$nb = date("Y-m-d",$now_bDate);
	$ne = date("Y-m-d",$now_eDate);	
		
	//不同天檢查時間重疊
	if($ob!=$nb || $oe!=$ne) {
		$sql = 'select * from `overtime` where `empID`='."'".$empID."' and eDate>'$bDate'"; //比起單日期早的就不用比了
		$rsT = db_query($sql);
		if(!db_eof($rsT)){
			while($rT = db_fetch_array($rsT)) {
				if(!($rT['isDel']==1 or $rT['is_leadmen']==-1 or $rT['is_agent']==-1)){
					$absence_bDate = strtotime($rT['bDate']);
					$absence_eDate = strtotime($rT['eDate']);
					if( ($now_bDate<=$absence_bDate and $now_eDate<=$absence_bDate) or ($now_bDate>=$absence_eDate and $now_eDate>=$absence_eDate) ){
						//正常情況
					}else{
						//時間重疊
						echo 1;	exit; 
					}
				}
			}
		}
	}

	if(isset($_SESSION["empID"])){	$empID=$_SESSION["empID"];	}else{	$empID=$_SESSION["Account"];}
	$data = "[$empID] ".date("Y-m-d H:i")." : $reason".PHP_EOL."(原本)".$r[hours].'小時'.$overtimeType[$aType].' '.$fmActAry[$r['fmAct']].'：'."$ob ~ $oe";
	$logs=$r[logs].$data.PHP_EOL;

	$sql="update overtime ";
	$sql.="set aType='".$aType."',fmAct='".$fmAct."',bDate='".$bDate."',eDate='".$eDate."',hours=".$hours.",logs='".$logs."',isDel=".$isDel.",isReqD=".$isDel." ";
	$sql.="where id=".$id;
	db_query($sql);
}
?>