<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
		
	$bE = false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "員工加班資料";
	$tableName = "overtime A,emplyee B";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/overtime/';
	$delField = 'ahead';
	$delFlag = false;
	$imgpath = '../../data/overtime/';
	$fmActAry=array("0"=>"補休","1"=>"加班費");
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "depID=''";
	}

	$is_leadmen = -1;

	if(isset($_REQUEST['is_leadmen']) && $_REQUEST['is_leadmen'] != '') {
		if($_REQUEST['is_leadmen'] == '0') {
			$temp[] = " is_leadmen = '".(int)$_REQUEST['is_leadmen']."'";
		} else {
			$temp[] = " (is_leadmen = '".(int)$_REQUEST['is_leadmen']."' or is_leadmen = '-1')";
		}
		$is_leadmen = (int)$_REQUEST['is_leadmen'];
	}

	$isLeadmenList = array(
		'' => '審核狀態',
		'0' => '主管未審核',
		'1' => '主管已審核'
	);

	///if($_REQUEST['aType']) $temp[] = "aType='$_REQUEST[aType]'";
	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "B.empName";
	$searchField2 = "A.empID";
	$searchField3 = "A.id";
	$pageSize = 10;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,empID,aType,fmAct,bDate,eDate,hours,content,is_leadmen,rspContent,isOK,isDel,logs,tuning");
	$ftAry = explode(',',"單號,加班員工,加班別,類型,起始日期,結束日期,時數,事由,主管簽名,辦理情形,准班,銷班,調整狀態,調整");
	$flAry = explode(',',"70,80,,,,,,,,,,,,");
	$ftyAy = explode(',',"ID,empID,text,text,datetime,datetime,text,text,bool,text,bool,bool,logs,tuning");
?>