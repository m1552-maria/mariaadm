/*
	人事修改需注意：
	 .時間區間重疊
	 .關帳
	 .已補休
	 ...
*/
var avHours=0;			//可補休時數 
var orig_aType='';		//原始假別
var spHolidays=0;		//可特休時數 

$(function() {
  $( "#d_tuning" ).draggable();
});
function doTuning(id,empName){
	$.ajax({ 
	  type: "POST", 
	  url: "tuning.php", 
	  data: "act=getData&id="+id+"&year="+$('#year').val()
	}).done(function( rs ) {
		var data=rs.split("#");
		var str;
		if(data!=""){
			$('#id').val(id);
			$('#td_empName').html(empName);
			$('#hours').val(data[0]);
			$('#bDate').val(data[1]);
			$('#eDate').val(data[2]);
			$('#td_aType').html('<select id="tuning_aType">'+data[9]+'</select>');
			$('#td_fmAct').html('<select id="tuning_fmAct">'+data[10]+'</select>');
			$('#bDate').datetimepicker({hour:data[5],minute:data[6]});
			$('#eDate').datetimepicker({hour:data[7],minute:data[8]});
			$('#rspContent').val(data[4]);
			$('#td_rspContent').html(data[4]);
			orig_aType=data[3];
		}
		$('#d_tuning').show();
	});
}
function save(){
	if($('#hours').val()==""){	alert('請輸入時數');	$('#hours').focus();	return ;}
	var r =  /^[0-9]*[1-9][0-9]*$/;　//正整數    
	if(!r.test(($('#hours').val()/0.5).toString())){ 
		alert("時數請以0.5為單位");
		document.getElementById('hours').value='';
		document.getElementById('hours').focus();
		return;
	}
	if($('#bDate').val()==""){	alert('請輸入開始時間');	$('#bDate').focus();	return ;}
	if($('#eDate').val()==""){	alert('請輸入結束時間');	$('#eDate').focus();	return ;}
	var bd=new Date($('#bDate').val());
	var ed=new Date($('#eDate').val());
	if(bd>ed){	alert("開始、結束時間有誤");return;}
	var isDel=0;
	if($("[name='isDel']").attr("checked")=="checked")	isDel=1;
	if(!confirm("確定修改資料嗎?"))	return;
	$.ajax({ 
	  type: "POST", 
	  url: "tuning.php", 
	  data: "act=saveData&aType="+$("#tuning_aType").val()+"&fmAct="+$("#tuning_fmAct").val()+"&id="+$('#id').val()
		      +"&bDate="+$('#bDate').val()+"&eDate="+$('#eDate').val()+"&hours="+$('#hours').val()+"&reason="+$('#reason').val()+"&isDel="+isDel+"&orig_aType="+orig_aType
	}).done(function( rs ) {
		if(rs == 1){
			alert('時間區間有誤，此時段已經有請假紀錄');
		}else{
			$('#d_tuning').hide();
			location.reload();
		}
	});	
}

function hide(id){
	$('#'+id).hide();
}