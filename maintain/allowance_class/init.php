<?	
	session_start();
    header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	$bE = true;	//異動權限
	$pageTitle = "津貼項目";
	$tableName = "allowance_class";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$defaultOrder = "awcID";
	$searchField1 = "title";
	$searchField2 = "isTax";
	$pageSize = 20;	//每頁的清單數量

	// 注意 primary key should be first one
	$fnAry = explode(',',"awcID,title,isTax,unitName,unitPrice,notes");
	$ftAry = explode(',',"編號,項目名稱,是否報稅,單位名稱,單位金額,備註");
	$flAry = explode(',',"50,,,,,");
	$ftyAy = explode(',',"id,text,text,text,text,text");
	
	$isTax=array("0"=>"否","1"=>"是");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,isTax,unitName,unitPrice,notes");
	$newftA = explode(',',"項目名稱,是否報稅,單位名稱,單位金額,備註");
	$newflA = explode(',',",,,,30");
	$newfhA = explode(',',",,,,8");
	$newfvA = array('',$isTax,'','','');
	$newetA = explode(',',"text,select,text,text,textbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"awcID,title,isTax,unitName,unitPrice,notes");
	$editftA = explode(',',"編號,項目名稱,是否報稅,單位名稱,單位金額,備註");
	$editflA = explode(',',",,,,,30");
	$editfhA = explode(',',",,,,,8");
	$editfvA = array('','',$isTax,'','','');
	$editetA = explode(',',"text,text,select,text,text,textbox");
?>