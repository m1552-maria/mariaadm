<?php
	@session_start();
	include("../config.php");
	include("inc_vars.php");
	$ulpath = 'map_icon/';
	
	$classID = $_REQUEST['MAP_CLASSID']?$_REQUEST['MAP_CLASSID']:($_SESSION['MAP_CLASSID']?$_SESSION['MAP_CLASSID']:'');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>資源地圖</title>
<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
<script>
	$(function(){
		$("select[name='county']").change(function(){
			var city = $(this).next().val();	//鄉鎮市要緊接縣市
			var cd2 = this.options[this.selectedIndex].value;
			$.getJSON('/getptycd3.php',{'cd2':cd2},function(data){
				var str = '<option selected></option>';
				$.each(data,function(idx,itm){ if(itm==city) str += '<option selected>'+itm+'</option>'; else str += '<option>'+itm+'</option>'; });
				$("select[name='city']").html(str);
			});
		});
	});

	function doComet(RID,Title) {
		window.open('resources/message.php?RID='+RID+'&title='+Title,'Message','width=600,height=600');
	}
</script>
<style>
	#mapFrame { border:1px solid #999; width:96%; padding:5px 20px 20px 20px; -webkit-border-radius: 9px; -moz-border-radius: 9px; border-radius: 9px; font-size:13px; color:#333; }
  a { text-decoration:none}
	a.Class { display:block; float:left; margin:5px 10px 15px 0; font-size:12px; color:#777; cursor:pointer; }
	img { border:0 }
	.item h3 { color:#F86207; margin:5px; cursor:pointer; }
	.item ul { margin:0; padding-left:25px; }
	.item li { list-style:circle; }
	#listTitle { margin:20px 0 20px 0; }
	#itemList .item { width:350px; float:left; margin:0 30px 20px 0; }
	#itemList .item span { color:#E95252; }
	#itemList div.cl { clear:both; }
	.ckSpan {cursor:pointer; padding-left: 5px; }
</style>
</head>

<body>
<div id="mapFrame">
  <div style="padding:5px; margin:0 0 5px 0; border:dotted 1px #333333; border-radius: 5px; background-color:#f8f8f8;font-family:'微軟正黑體',Verdana;">
  <? //::根類別	
    $sql = "select * from resource_class where PID=0 order by Seque";
    $rsC = db_query($sql, $conn);
		$rootIconA = array();	//記住 root icon
    while ($rC = db_fetch_array($rsC)) {
			$rootIconA[$rC['ID']] = $rC['icon'];
			if(!$classID) $classID = $rC['ID'];
			if($classID==$rC['ID']) {
      	echo ' <span><img width="20" src="'.$ulpath.$rC["icon"].'" align="absmiddle" /><font color="#C0C0C0">'.$rC["Title"].'</font></span>';
			} else {
				echo ' <span style="cursor:pointer"><a href="map.php?MAP_CLASSID='.$rC[ID].'"><img width="20" src="'.$ulpath.$rC["icon"].'" align="absmiddle" />'.$rC["Title"].'</a></span>';
			}
    }
		echo "<br><span class='ckSpan' onClick=\"filterNone(this)\"><img src='images/checked.gif' align='absmiddle'>全部</span>";
		//::第二層子類別
		$classAR[] = "'$classID'";
		$sql = "select * from resource_class where PID='$classID' order by Seque";
		$rsC = db_query($sql, $conn);
		while ($rC = db_fetch_array($rsC)) {
			$classAR[] = "'$rC[ID]'";
			echo "<span class='ckSpan' onClick=\"filterMarker('$rC[ID]',this)\"><img src='images/uncheck.gif' align='absmiddle'>".$rC['Title']."</span>";
			//check Level 3 class
			$rs3 = db_query("select * from resource_class where PID='$rC[ID]' order by Seque");
			if(!db_eof($rs3)) {
				echo "<select onchange='level3Change(this)'><option value='$rC[ID]'>--- 全部 ---</option>";
				while($r3=db_fetch_array($rs3)) {
					$classAR[] = "'$r3[ID]'";
					echo "<option value='$r3[ID]'>$r3[Title]</option>";
				}
				echo "</select>";
			}
		}
  ?>
  	<br>縣市：<select name="county" id="county" onchange="doSearchMap();">
      <option value=""></option><? foreach($county as $k=>$v) { ?><option value="<?=$k?>"<?=($k==$_REQUEST["county"]) ? " selected" : "" ?>><?=$v?></option><? } ?>
    </select> &nbsp;
    區域：<select name="city" id="city" onchange="doSearchMap();"></select> &nbsp;
    關鍵字： <input id="Addr" name="Addr" type="text" size="50" />&nbsp; <a href="#" onclick="searchMarker()"><img src="images/search.png" height="16" /></a>
    <div id="logtx"></div>
  </div>
  
	<a name="mapTop"></a>
  <div id="map-search">
    <div id="map_canvas1" style="width:100%; height:650px; border:1px solid #333;"></div>
    <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
    <!-- AIzaSyCFbyptVCgWlp6y66I_UQu6HlXZ7yL2I84 -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyANhzSqoJ0q0lQEFGd9x9GNtsYJsAo5q_c"></script>  
    <script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/src/markerclusterer.js"></script>
    <script type="text/javascript">
      var map;
      var infowindow;
      $(function() {
        //自動調整地圖高度 滿版
        //$("#map_canvas1").css("height", $(window).height() - $("#header").height() - 20);
        var nLon = 120.649624;
        var nLat = 24.162652;
        var latlng = new google.maps.LatLng(nLat, nLon);		
        var myOptions = {zoom:12,center:latlng,zoomControlOptions:{style: google.maps.ZoomControlStyle.LARGE},mapTypeId: google.maps.MapTypeId.ROADMAP};
        map = new google.maps.Map(document.getElementById("map_canvas1"), myOptions);
        infowindow = new google.maps.InfoWindow();
        
        //群組marker
        var mcOptions = {gridSize: 50, maxZoom: 15, imagePath:'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m'};
        map.mc = new MarkerClusterer(map,[],mcOptions);
        map.mkClass = [];
        map.nowClass = 0;
        map.len = 0;
        <? 
        $sql="select rd.*, r.ClassID,r.click_count,r.grades,r.id AS rsID, rc.Title,rc.icon,rc.fields from resources r, resource_detail rd, resource_class rc where rc.ID=r.ClassID And rd.RID=r.id "
				    ."and rd.isDel=0 and r.isReady=1 and rd.longitude>0 and not rd.longitude is null and r.ClassID in (".join(',',$classAR).")";
				echo '/*'.$sql.'*/';		
				//::產生Maker Info(兼List info)
        $rs = db_query($sql, $conn);
        while($r = db_fetch_array($rs)) {
					$aa = explode('-',$r['ClassID']);
					$defIcon = $rootIconA[$aa[0]];
					if(!$defIcon && $r['icon']) $defIcon = $r['icon'];	//預設子類別icon為父類別icon
          if($r['grades']>0 && $r['click_count']) { $stars = ceil($r['grades'] / $r['click_count']); } else $stars = 0; 	         
          echo "sInfo = '<div class=\"item\"><h3 id=\"r_".$r[RID]."\">".nl2br((str_replace("\r\n", "<br>", $r[name])))."</h3>&nbsp;&nbsp;";
          for($i=0; $i<$stars; $i++) echo '<img style="cursor:pointer" src="map_icon/star.png" onclick="setGrades('.$r['rsID'].','.($i+1).')">';
          for($j=$i; $j<5; $j++) echo '<img style="cursor:pointer" src="map_icon/star_off.png" onclick="setGrades('.$r['rsID'].','.($j+1).')">';
					echo '<span onclick=doComet('.$r['rsID'].',"'.$r[name].'") style="float:right; cursor:pointer">[評語] </span>';
          echo '<ul>';
          $aFields = json_decode($r["fields"]);
          $count =1;
          foreach($aFields as $k=>$v) { 
            if (!empty($r[$k]) && $k != "name") {
              switch($k) {
                case 'county': $fldValue = $county[$r[$k]]; break;
                case 'file1': 
									$tmAry = explode(',',$r[file1]);
									$tmStr = '';
									foreach($tmAry as $fn) {
                  	$furl="/data/resources/$r[ClassID]/$r[rsID]/$fn";
										 $tmStr .= "<a href=\"$furl\" target=\"_new\"><img height=\"60\" src=\"$furl\" align=\"absmiddle\"></a>"; 
									}
                  $fldValue = $tmStr; 
                  break;
                default: $fldValue = $r[$k];
              }            
              if($count==4) {
                echo '<div align="right" style="cursor:pointer" onClick="expandList(this)"> ... More▼</div>';						
              } else if($count>3) {
                echo '<li style="display:none">'.urldecode($v).'：'.nl2br((str_replace("\r\n", "<br>", $fldValue))).'</li>';
              } else {
                echo '<li>'.urldecode($v).'：'.nl2br((str_replace("\r\n", "<br>", $fldValue))).'</li>';
              }
              $count++;
            }
          }
          echo "</ul></div>';";	?>	
          setMaker(map,"<?=$r["ClassID"]?>",<?=$r["RID"]?>,"<?=str_replace("\r\n", "",$r["name"])?>",
						<?=$r["longitude"]?>,<?=$r["latitude"]?>,"<?=$ulpath.($r["icon"]?$r["icon"]:$defIcon)?>",
						"<?=$r["county"]?>","<?=$r["city"]?>","<?=str_replace("\r\n","",$r["address"])?>",sInfo);
        <? } ?>
				$('.ckSpan:first').html( $('.ckSpan:first').html()+'('+map.len+')' );
        $(".item h3").click(function(){	mapMoveTo(this.id);	});
      });
      
      function setGrades(id,grade) {
        $.get('resources/grades.php',{'rsID':id,'grade':grade},function(rzt){alert(rzt);});
      }
      
      function expandList(elm) {
        $(elm).parent().children().each(function(idx,child) {	$(this).css('display','block');});
        $(elm).remove();
      }
      
      function setMaker(map, nClass, nID, sTitle, nLon, nLat, sIcon, sCounty, sCity, sAddr, sInfo) {
        var marker = new google.maps.Marker({position: new google.maps.LatLng(nLat,nLon), animation: google.maps.Animation.DROP,title:sTitle});
        marker.setIcon(sIcon);
        marker.RID = nID;
        marker.Title = sTitle;
        marker.County = sCounty;
        marker.City = sCity;
        marker.Addr = sAddr;
        marker.Info = sInfo;
        if(!map.mkClass[nClass]) map.mkClass[nClass] = [];
        map.mkClass[nClass].push(marker);
        map.mc.addMarker(marker);
        
        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(sInfo);
          map.setCenter(marker.getPosition());
          infowindow.marker = marker;
          infowindow.open(map,marker);
          //click count
          //$.post( "doresourcescount.php", { id: marker.RID } );
        });
        
        map.len++;
        $("#itemList").append(sInfo);
        if (map.len % 2 == 0) $("#itemList").append('<div class="cl"></div>');
      }
      
      function doSearchMap(sAddr) {
        if (!sAddr) sAddr = $("#county option:selected").text()+" "+$("#city option:selected").text()+" "+$("#Addr").val();
        if (sAddr == "") return;
        if ($("#city option:selected").text() != "") nZoom = 14; else nZoom = 12;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': sAddr}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setOptions({zoom: nZoom});
          }
        });
      }

      function searchMarker() {
        sCounty = $("#county").val();
        sCity = (!$("#city").val()) ? "" : $("#city").val();
        sKeyword = $("#Addr").val();
        map.mc.clearMarkers();
        $("#itemList").html("");
        map.len = 0;				
				for(k in map.mkClass) {
					if(k && (k == map.nowClass || map.nowClass==0) ) {
						var aa = map.mkClass[k];
						for(itm in aa) {
							var j = aa[itm];
							if ((j.County==sCounty || sCounty=="") && (j.City==sCity || sCity=="") && (j.Title.indexOf(sKeyword) != -1 || j.Addr.indexOf(sKeyword) != -1 || sKeyword == "")) {
								map.mc.addMarker(j);
								map.len++;
								$("#itemList").append(j.Info);
								if (map.len % 2 == 0) $("#itemList").append('<div class="cl"></div>');
							}	
						}
					}
				}
				$('#logtx').html('資源列表數量 ： '+map.len);
        $(".item h3").click(function(){	mapMoveTo(this.id);	});
      }
			
			function level3Change(elm) {
				//alert(elm.options[elm.selectedIndex].value); elm.previousSibling
				var id = elm.options[elm.selectedIndex].value;
				filterMarker(id,elm.previousSibling);
			}
      function filterMarker(nClass,elm) {
				$('.ckSpan img').each(function(index, element) {$(element).attr('src','images/uncheck.gif');});
				$(elm).find('img').attr('src','images/checked.gif');
        sCounty = $("#county").val();
        sCity = (!$("#city").val()) ? "" : $("#city").val();
        sKeyword = $("#Addr").val();
        if (!nClass) nClass = map.nowClass;	else map.nowClass = nClass;
        map.mc.clearMarkers();
        $("#itemList").html("");
        map.len = 0;
				
				for(k in map.mkClass) {
					if(k && k==nClass) {
						var aa = map.mkClass[k];
						for(itm in aa) {
							var j = aa[itm];
							if ((j.County==sCounty || sCounty=="") && (j.City==sCity || sCity=="") && (j.Title.indexOf(sKeyword) != -1 || j.Addr.indexOf(sKeyword) != -1 || sKeyword == "")) {
								map.mc.addMarker(j);
								map.len++;
								$("#itemList").append(j.Info);
								if (map.len % 2 == 0) $("#itemList").append('<div class="cl"></div>');
							}	
						}
					}
				}
				$('#logtx').html('資源列表數量 ： '+map.len);
        $(".item h3").click(function(){	mapMoveTo(this.id);	});
      }
			
			function filterNone(elm) {
				$('.ckSpan img').each(function(index, element) {$(element).attr('src','images/uncheck.gif');});
				$(elm).find('img').attr('src','images/checked.gif');
				sCounty = $("#county").val();
        sCity = (!$("#city").val()) ? "" : $("#city").val();
        sKeyword = $("#Addr").val();
				map.nowClass = 0;
        map.mc.clearMarkers();
        $("#itemList").html("");
        map.len = 0;
				
				for(k in map.mkClass) {
					if(k) {
						var aa = map.mkClass[k];
						for(itm in aa) {
							var j = aa[itm];
							if ((j.County==sCounty || sCounty=="") && (j.City==sCity || sCity=="") && (j.Title.indexOf(sKeyword) != -1 || j.Addr.indexOf(sKeyword) != -1 || sKeyword == "")) {
								map.mc.addMarker(j);
								map.len++;
								$("#itemList").append(j.Info);
								if (map.len % 2 == 0) $("#itemList").append('<div class="cl"></div>');
							}
						}
					}
				}
				$('#logtx').html('資源列表數量 ： '+map.len);
        $(".item h3").click(function(){	mapMoveTo(this.id);	});
			}
			
        
      function mapMoveTo(sID) {
        nID = sID.substr(2);
        $(map.mkClass).each(function(k,v){
          $(v).each(function(i,j){
            if (j.RID == nID) {
              map.setOptions({zoom: 16});
              google.maps.event.trigger(j, 'click');
              location.href="#mapTop";
              return false;
            }
          });
        });
      }
    </script>	
  </div>
  
	<img id="listTitle" src="images/resources_list.jpg" />
	<div id="itemList"></div>
	<div style="clear:both"></div>
</div>
</body>
</html>