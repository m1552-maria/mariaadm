<?php
	include 'init.php';
  $rs = db_query("select * from $tableName",$conn);
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font:15px "微軟正黑體",Verdana;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	#browser li {
		list-style-image: url(../../images/job.png);
		list-style-position: outside;
		padding: 0 4px 0 4px; 
		cursor:pointer;
	}
  </style>
  <script type="text/javascript" src="/Scripts/jquery.js"></script>    
  <script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		$(document).ready(function(){
			selid = dialogArguments;
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
			
	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}		
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
	<tr>
   	<th id="tblHead" bgcolor="#CCCCCC">::: 職掌代碼表 :::</th>    
  </tr>
	<tr><td bgcolor="#FFFFFF">
    <div id="markup" class="whitebg"><ul id="browser">
    <? while($r=db_fetch_array($rs)) echo "<li><a onClick=setID('$r[jobID]','$r[jobName]',this)>$r[jobName]</a></li>"; ?> 
    </ul></div>
	</td></tr>
</table>
</body>
</html>