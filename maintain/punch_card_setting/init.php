<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../getEmplyeeInfo.php');
		
	$bE = $_SESSION['privilege']>10?true:false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "打卡機位置設定";
	$tableName = "punch_card_setting";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/punch_card_setting/';
	$delField = '';
	$delFlag = true;
	$imgpath = '../../data/punch_card_setting/';
	
	$status = array(1=>'是',0=>'否');
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "place";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,place,ip,lon,lat,manager,status");
	$ftAry = explode(',',"編號,地點,ip位址,經度,緯度,管理者,狀態");
	$flAry = explode(',',"60,200,100,100,100,,");
	$ftyAy = explode(',',"ID,text,text,text,text,text,bool");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"place,ip,lon,lat,status");
	$newftA = explode(',',"地點,ip位址,經度,緯度,狀態");
	$newflA = explode(',',"60,60,10,10,");
	$newfhA = explode(',',",,,,");
	$newfvA = array('','','','',$status);
	$newetA = explode(',',"text,text,text,text,radio");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,place,ip,lon,lat,status");
	$editftA = explode(',',"編號,地點,ip位址,經度,緯度,狀態");
	$editflA = explode(',',"60,60,,,,");
	$editfhA = explode(',',",,,,,");
	$editfvA = array('','','','','',$status);
	$editetA = explode(',',"ID,text,text,text,text,radio");
?>