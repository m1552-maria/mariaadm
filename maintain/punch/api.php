<?
/* 打卡資料匯入,後執行出缺勤異常 */
include_once "../../config.php";
header("Content-Type:text/html; charset=utf-8");


if(isset($_REQUEST['now_month'])){
	list($year,$month) = explode('-',$_REQUEST['now_month']);
	//處理日期//一天一天算
	$d=cal_days_in_month(CAL_GREGORIAN,$month,$year);//這個月有幾天
	for($i=1;$i<=$d;$i++){
		$finish['start_day'] = date('Y-m-d H:i:s',mktime(0, 0, 0, $month, $i,$year));
		$finish['end_day'] = date('Y-m-d H:i:s',mktime(23, 59, 59, $month, $i,$year));
		main($finish);
	}

}elseif(isset($_REQUEST['start_day']) && isset($_REQUEST['end_day'])){
	//處理日期
	$start_date = new DateTime($_REQUEST['start_day']);
    $end_date = new DateTime($_REQUEST['end_day']);
    $interval = new DateInterval('P1D'); 
    $daterange = new DatePeriod($start_date, $interval ,$end_date);
    foreach($daterange as $date){
    	$now_day = $date->format("Y-m-d");
    	$finish['start_day'] = date('Y-m-d H:i:s',strtotime($now_day));
		$finish['end_day'] = date('Y-m-d H:i:s',mktime(23, 59, 59, date('m',strtotime($now_day)), date('d',strtotime($now_day)),date('Y',strtotime($now_day))));
		main($finish);
    }


}else{//給排程用//昨天的打卡資料
	$now_day = date('Y-m-d',strtotime(date('Y-m-d') . "-1 days"));
	$finish['start_day'] = date('Y-m-d H:i:s',strtotime($now_day));
	$finish['end_day'] = date('Y-m-d H:i:s',mktime(23, 59, 59, date('m',strtotime($now_day)), date('d',strtotime($now_day)),date('Y',strtotime($now_day))));
	main($finish);
}


// echo '<pre>';
// print_r($finish);

function main($finish){
	//以防重複匯入//針對無排班
	$sql = ' delete from `emplyee_duty` where `startTime` is null and `endTime` is null and ((`startTime2` BETWEEN '."'".$finish['start_day']."'".' AND '."'".$finish['end_day']."'".') OR (`endTime2` BETWEEN '."'".$finish['start_day']."'".' AND '."'".$finish['end_day']."'".'))';
	db_query($sql);

	$punch = array();
	$sql = 'select * from `emplyee_dutylog` where `d_datetime`>="'.$finish['start_day'].'" and `d_datetime`<="'.$finish['end_day'].'"  order by `d_datetime` ';
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if($r['dType']!='outoffice_out' && $r['dType']!='outoffice_back'){//排除外出返回
				$punch[$r['id']]['empID'] = $r['empID'];
				$punch[$r['id']]['time'] = $r['d_datetime'];
				$date = explode(' ', $r['d_datetime']);
				$punch[$r['id']]['day'] = $date[0];	
			}
		}
	}
	$result = $punch;
	// echo '<pre>';
	// print_r($result);

	//班表
	$sql = 'select * from `emplyee_duty` where ((`startTime`>='."'".$finish['start_day']."'".' and `startTime`<='."'".$finish['end_day']."'".') or (`endTime`>='."'".$finish['start_day']."'".' and `endTime`<='."'".$finish['end_day']."'".'))';
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$employee_duty_list[$r['empID']][$r['id']] = $r;//一個人有多班表
		}
	}
	// print_r($employee_duty_list);

	//處理資料
	foreach($result as $key1=>$val1){
		// list($due,$time) = explode(' ', $result[$key1]['time']);//區隔日期
		if(isset($employee_duty_list[$result[$key1]['empID']])){//有排班
			foreach($employee_duty_list[$result[$key1]['empID']] as $key2=>$val2){
				//上班時間//沒遲到//正常上班
				//判斷是否小於且在4小時內
				if($result[$key1]['time']<=$employee_duty_list[$result[$key1]['empID']][$key2]['startTime'] && $result[$key1]['time']>=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."-4 hour"))){
					if($employee_duty_list[$result[$key1]['empID']][$key2]['startTime2'] == ''){//一定是時間最早的
						$employee_duty_list[$result[$key1]['empID']][$key2]['startTime2'] = $result[$key1]['time'];
					}
				//遲到或是下班
				}elseif($result[$key1]['time']>$employee_duty_list[$result[$key1]['empID']][$key2]['startTime'] && $result[$key1]['time']<=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."+4 hour"))){//遲到作業
					//建立在上班未打任何卡的狀況下
					if($employee_duty_list[$result[$key1]['empID']][$key2]['startTime2'] == ''){//遲到
						$employee_duty_list[$result[$key1]['empID']][$key2]['startTime2'] = $result[$key1]['time'];
					}else{//可能是下班卡//所以要記到下班裡面
						//打卡時間必須要大於0.5小時以上
						if($result[$key1]['time']>=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."+30 minutes")) && $result[$key1]['time']<=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['endTime']."+4 hour"))){//必須大於上班時間超過30分鐘
							//記到下班裡面
							$employee_duty_list[$result[$key1]['empID']][$key2]['endTime2'] = $result[$key1]['time'];
						}
					}
				}
				//下班時間//未早退
				//判斷是否大於且在4小時內
				elseif($result[$key1]['time']>=$employee_duty_list[$result[$key1]['empID']][$key2]['endTime'] && $result[$key1]['time']<=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['endTime']."+4 hour"))){
					//一直寫進下班	
					$employee_duty_list[$result[$key1]['empID']][$key2]['endTime2'] = $result[$key1]['time'];
				//早退
				}elseif($result[$key1]['time']<$employee_duty_list[$result[$key1]['empID']][$key2]['endTime'] && $result[$key1]['time']>=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['endTime']."-4 hour"))){//早退作業
					//先判斷上班是不是空的 如果是就是上班時間 如果不是就是早退
					if($employee_duty_list[$result[$key1]['empID']][$key2]['startTime2']==''){//可能是上班卡//所以要記到上班裡面//上班卡一定要是空的
						if($result[$key1]['time']<=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['endTime']."-30 minutes")) && $result[$key1]['time']>=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."-4 hour"))){
							$employee_duty_list[$result[$key1]['empID']][$key2]['startTime2'] = $result[$key1]['time'];
						}
					}else{//早退用時間排序一直update
						$employee_duty_list[$result[$key1]['empID']][$key2]['endTime2'] = $result[$key1]['time'];
					}
				}

				//以上都不符合的情況下
				elseif($employee_duty_list[$result[$key1]['empID']][$key2]['startTime2']!='' && $employee_duty_list[$result[$key1]['empID']][$key2]['endTime2']==''){
					//打卡時間必須比上班時間要大於0.5小時以上
					if($result[$key1]['time']>=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."+30 minutes")) && $result[$key1]['time']>=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime2']."+30 minutes")) && $result[$key1]['time']<=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['endTime']."+4 hour"))){
						//記到下班裡面
						$employee_duty_list[$result[$key1]['empID']][$key2]['endTime2'] = $result[$key1]['time'];
					}
				}elseif($employee_duty_list[$result[$key1]['empID']][$key2]['startTime2']=='' && $employee_duty_list[$result[$key1]['empID']][$key2]['endTime2']==''){
					if($result[$key1]['time']>=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."-4 hour")) && $result[$key1]['time']<=date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['endTime']."+4 hour"))){
						$employee_duty_list[$result[$key1]['empID']][$key2]['startTime2'] = $result[$key1]['time'];	
					}
				}
			}
		}else{//無排班//只能記錄當天的//不能記錄跨天
			//新陣列存資料
			if(!isset($no_duty[$result[$key1]['day']][$result[$key1]['empID']]['start_time'])){//存開始時間//第一次
				$no_duty[$result[$key1]['day']][$result[$key1]['empID']]['start_time'] = $result[$key1]['time'];
			}else{//
				$start_time = $no_duty[$result[$key1]['day']][$result[$key1]['empID']]['start_time'];//這個是紀錄開始時間
				if($result[$key1]['time']> $start_time){
					if(!isset($no_duty[$result[$key1]['day']][$result[$key1]['empID']]['end_time'])){//這個是紀錄結束時間
						$no_duty[$result[$key1]['day']][$result[$key1]['empID']]['end_time'] = $result[$key1]['time'];
					}else{
						$end_time = $no_duty[$result[$key1]['day']][$result[$key1]['empID']]['end_time'];
						if($result[$key1]['time']>$end_time){
							$no_duty[$result[$key1]['day']][$result[$key1]['empID']]['end_time'] = $result[$key1]['time'];	
						}
					}
				}else{
					$no_duty[$result[$key1]['day']][$result[$key1]['empID']]['start_time'] = $result[$key1]['time'];
				}
			}
		}
	}

	foreach($employee_duty_list as $k=>$v){
		foreach($v as $k1=>$v1){
			if($v1['startTime2']!='' && $v1['endTime2']==''){
				$sql = 'update `emplyee_duty` set `startTime2`='."'".$v1['startTime2']."'".' where `empID`='.$k.' AND `id`='.$k1;
				db_query($sql);	
			}elseif($v1['startTime2']!='' && $v1['endTime2']!=''){
				//存到資料庫
				$sql = 'update `emplyee_duty` set `startTime2`='."'".$v1['startTime2']."'".',`endTime2`='."'".$v1['endTime2']."'".' where `empID`='.$k.' AND `id`='.$k1;
				db_query($sql);	
			}elseif($v1['startTime2']=='' && $v1['endTime2']!=''){
				$sql = 'update `emplyee_duty` set `endTime2`='."'".$v1['endTime2']."'".' where `empID`='.$k.' AND `id`='.$k1;
				db_query($sql);	
			}
		}
	}

	// echo '<pre>';
	// print_r($employee_duty_list);

	//處理無排班資料
	foreach($no_duty as $key1=>$val1){
		foreach($no_duty[$key1] as $key2=>$val2){
			//因為一定會有start time所以不用加入判斷 判斷end time就好
			if(isset($no_duty[$key1][$key2]['end_time']) && $no_duty[$key1][$key2]['end_time']!=''){
				//間隔要超過30分鐘
				if(date('Y-m-d H:i:s',strtotime($no_duty[$key1][$key2]['start_time']."+30 minutes"))>=$no_duty[$key1][$key2]['end_time']){
					$no_duty[$key1][$key2]['end_time'] = '';
				}
			}
		}
	}


	//無排班存資料庫
	foreach($no_duty as $key1=>$val1){
		foreach($no_duty[$key1] as $key2=>$val2){
			if(!isset($val2['start_time']) || $val2['start_time']=='') $start_time = 'null';
			else $start_time = "'".$val2['start_time']."'";
			if(!isset($val2['end_time']) || $val2['end_time']=='') $end_time = 'null';
			else $end_time = "'".$val2['end_time']."'";
			$sql = 'insert into `emplyee_duty`(`empID`,`startTime2`,`endTime2`) values ('."'".$key2."'".','.$start_time.','.$end_time.')';
			db_query($sql);	
		}
	}

}

echo '已寫入';
//call 出缺勤異常，異常結果將反應在出勤表
include "../duty/proAbnormal.php";
exit;
?>