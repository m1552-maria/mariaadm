<?
  @session_start();
  if($_REQUEST['excel_type']=='festival'){
    $filename = '三節獎金'.date("Y-m-d H:i:s",time()).'.xlsx';  
  }elseif($_REQUEST['excel_type']=='year_end'){
    $filename = '年終獎金'.date("Y-m-d H:i:s",time()).'.xlsx';
  }
  

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');


  if($_REQUEST['excel_type']=='festival'){
    $list = array();
    $sql = $_REQUEST['sqltx']. ' where a.fid="'.$_REQUEST['fid'].'" ' ;
    if($_REQUEST['projID']) {
      $pList = explode(',', $_REQUEST['projID']);
      $aa = array();
      foreach ($pList as $key => $v) {
        if($v)  $aa[]="b.projID like '$v%'";  
      }
      $sql .= " and (".join(' or ',$aa).")";   

    } elseif ($_SESSION['privilege'] <= 10) {
      if($_SESSION['user_classdef'][7]) {
        $aa = array();
        foreach($_SESSION['user_classdef'][7] as $v){
          if($v)  $aa[]="b.projID like '$v%'";  
        }
        $sql .= " and (".join(' or ',$aa).")";   
      } else {
        $sql .=  " and b.projID='-1'";
      }
    }

    $sql .= ' order by  case when a.projID = "" then 1 else 0 end ,a.projID,a.empID';

    $rs=db_query($sql);
    if(!db_eof($rs)){
      while($r=db_fetch_array($rs)){//還要分類別
        if($r['projID']!=''){
          $projID = explode(',', $r['projID']);
          foreach($projID as $k=>$v){
            $list[$v][$r['depID']]['title'] = $r['title'];
            $list[$v][$r['depID']]['wkYear'] = $r['wkYear'];
            $list[$v][$r['depID']]['giveDate'] = $r['giveDate'];
            $list[$v][$r['depID']]['sub_list'][$r['empID']] = $r;
          }
        }else{
          $list['no_proj'][$r['depID']]['title'] = $r['title'];
          $list['no_proj'][$r['depID']]['wkYear'] = $r['wkYear'];
          $list['no_proj'][$r['depID']]['giveDate'] = $r['giveDate'];
          $list['no_proj'][$r['depID']]['sub_list'][$r['empID']] = $r;
        }
      }
    }

    $now_date_time = date('Y-m-d H:i:s');
    $lastYear=intval(date('Y')-1);
    $headervalue = array('流水號','員工編號','姓名','到職日','類型','實際年資','節金','實領金額');
    if($_REQUEST['printType'] == 'excel') {
      header("Content-type:application/vnd.ms-excel");
      header("Content-Disposition:attachment;filename=$filename");
      header("Pragma:no-cache");
      header("Expires:0");
      $objPHPExcel = new PHPExcel(); 
      $objPHPExcel->setActiveSheetIndex(0);
      $sheet = $objPHPExcel->getActiveSheet();

      //跑header 迴圈用
      $header = array('A','B','C','D','E','F','G','H');
      
      $i=1;
      $number = 0;
      $sum = 0;//合計
      $final = 0; //為了最後一筆
      $final1 = 0;
      foreach($list as $k=>$v){
        $final++;
        $final1=0;
        foreach($v as $k1=>$v1){
          $final1++;
          //頭
          $sheet->mergeCells('A'.$i.':H'.$i);//合併
          $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
          $sheet->getStyle('A'.$i.':H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
          $i++;
          $sheet->mergeCells('A'.$i.':H'.$i);
          $sheet->setCellValue('A'.$i,($v1['wkYear']-1911).'年'.$v1['title'].'節金清冊');
          $sheet->getStyle('A'.$i.':H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
          $i++;
          $sheet->mergeCells('A'.$i.':B'.$i);//合併
          if($k == 'no_proj') $sheet->setCellValue('A'.$i,'計畫編號:無');
          else $sheet->setCellValue('A'.$i,'計畫編號:'.$k);
          $sheet->setCellValue('H'.$i,'印製日期：'.$now_date_time);
          $sheet->getStyle('H'.$i)->getAlignment()->setWrapText(true);//要有這個才能換行
      
      
          $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
          $i++;
          $sheet->mergeCells('A'.$i.':B'.$i);//合併
          $sheet->setCellValue('A'.$i,'部門名稱:'.$departmentinfo[$k1]);
          $sheet->setCellValue('H'.$i,'年資結算止日:'.$lastYear.'-12-31');
          $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
          $i++;
          //表頭
          foreach($header as $k3=>$v3){
            $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
            $sheet->getStyle($v3.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValue($v3.$i,$headervalue[$k3]);
          }
          $sheet->getStyle('A'.$i.':H'.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
          $i++;

          //需要用到的變數
          $sub_sum = 0;//小計
          $people = 0;//人數

          //員工列表
          foreach($v1['sub_list'] as $k2=>$v2){
            $number++;//跟總人數一樣
            $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
            $sheet->setCellValue('A'.$i,$number);
            $sheet->setCellValueExplicit('B'.$i, $v2['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
            $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValue('C'.$i,$v2['empName']);
            $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValue('D'.$i,date("Y-m-d",strtotime($v2['hireDate'])));
            $sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValue('E'.$i,$v2['jobClass']);
            $sheet->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValue('F'.$i,$v2['years']);
            $sheet->getStyle('F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValue('G'.$i,$v2['money']);
            $sheet->setCellValue('H'.$i,$v2['money']);
            //需要加總的
            $sub_sum += $v2['money'];
            $sum += $v2['money'];
            $people++;
            $i++;
          }

          $sheet->setCellValue('B'.$i,'小計');
          $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->setCellValue('C'.$i,$people.'人');
          $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $sheet->setCellValue('D'.$i,'');
          $sheet->setCellValue('E'.$i,'');
          $sheet->setCellValue('F'.$i,'');
          $sheet->setCellValue('G'.$i,$sub_sum);
          $sheet->setCellValue('H'.$i,$sub_sum);
          $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
          $sheet->getStyle('A'.$i.':H'.$i)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
          if($final == count($list) && $final1==count($v)){
            $i++;
            //最後
            $sheet->setCellValue('B'.$i,'總計');
            $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->setCellValue('C'.$i,$number.'人');
            $sheet->setCellValue('D'.$i,'');
            $sheet->setCellValue('E'.$i,'');
            $sheet->setCellValue('F'.$i,'');
            $sheet->setCellValue('G'.$i,$sum);
            $sheet->setCellValue('H'.$i,$sum);
            $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
          }
          $i = $i+3;

          $sheet->setCellValue('A'.$i,'經辦:');
          $sheet->setCellValue('D'.$i,'主任/園長:');
          $sheet->setCellValue('F'.$i,'執行長:');
          $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setSize(12);
          $i = $i+2;
          
        }
      }

      //設定欄寬
      $width = array(10,15,15,15,15,15,15,25);
      foreach($header as $k=>$v){
        $sheet->getColumnDimension($v)->setWidth($width[$k]);  
      }
      
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
      $objWriter->setPreCalculateFormulas(false);
      $objWriter->save('php://output');

    } elseif($_REQUEST['printType'] == 'pdf') {
      include_once '../../mpdf/mpdf.php';
      $mpdf = new mPDF('UTF-8','A4-L','','DFKai-sb',10,10,25,20);
      $mpdf->useAdobeCJK = true;
      $mpdf->SetAutoFont(AUTOFONT_ALL);
      $mpdf->SetDisplayMode('fullpage');
      $html = file_get_contents('pdfHtml.html');
      $_html = array();

      $i=1;
      $number = 0;
      $sum = 0;//合計
      $final = 0; //為了最後一筆
      $final1 = 0;

      foreach($list as $k=>$v){
        $final++;
        $final1=0;
        foreach($v as $k1=>$v1){
          $final1++;
          //頭
          $_html[] = '<table class="tableData" width="1250px;" border="1">';

          $projTitle = ($k =='no_proj') ? '計畫編號:無' : '計畫編號:'.$k;
          $_html[] = '<tr><td colspan="2">'.$projTitle.'</td><td colspan="5"></td><td>印製日期：'.$now_date_time.'</td>';
          $_html[] = '</tr>';

          $_html[] = '<tr><td colspan="2">部門名稱'.$departmentinfo[$k1].'</td><td colspan="5"></td><td>年資結算止日:'.$lastYear.'-12-31'.'</td>';
          $_html[] = '</tr>';
      
          //表頭
          $_html[] = '<tr>';
          foreach($headervalue as $k3=>$v3){
            $_html[] = '<td>'.$v3.'</td>';
          }
          $_html[] = '</tr>';

          //需要用到的變數
          $sub_sum = 0;//小計
          $people = 0;//人數

          //員工列表
          foreach($v1['sub_list'] as $k2=>$v2){
            $_html[] = '<tr>';
            $number++;//跟總人數一樣
            $_html[] = '<td style="text-align:center;">'.$number.'</td>';
            $_html[] = '<td>'.$v2['empID'].'</td>';
            $_html[] = '<td>'.$v2['empName'].'</td>';
            $_html[] = '<td>'.date("Y-m-d",strtotime($v2['hireDate'])).'</td>';
            $_html[] = '<td>'.$v2['jobClass'].'</td>';
            $_html[] = '<td style="text-align:right;">'.$v2['years'].'</td>';
            $_html[] = '<td style="text-align:right;">'.number_format($v2['money']).'</td>';
            $_html[] = '<td style="text-align:right;">'.number_format($v2['money']).'</td>';
            $_html[] = '</tr>';

            //需要加總的
            $sub_sum += $v2['money'];
            $sum += $v2['money'];
            $people++;
          }
          $_html[] = '<tr>';
          $_html[] = '<td></td>';
          $_html[] = '<td>小計</td>';
          $_html[] = '<td>'.$people.'人</td>';
          $_html[] = '<td></td>';
          $_html[] = '<td></td>';
          $_html[] = '<td></td>';
          $_html[] = '<td style="text-align:right;">'.number_format($sub_sum).'</td>';
          $_html[] = '<td style="text-align:right;">'.number_format($sub_sum).'</td>';
          $_html[] = '</tr>';

          if($final == count($list) && $final1==count($v)){
            $_html[] = '<tr>';
            $_html[] = '<td></td>';
            $_html[] = '<td>總計</td>';
            $_html[] = '<td>'.$number.'人</td>';
            $_html[] = '<td></td>';
            $_html[] = '<td></td>';
            $_html[] = '<td></td>';
            $_html[] = '<td style="text-align:right;">'.number_format($sum).'</td>';
            $_html[] = '<td style="text-align:right;">'.number_format($sum).'</td>';
            $_html[] = '</tr>';
            $_html[] = '<tr><td style="height:25px;" colspan="8"></td></tr>';
          
            $_html[] = '<tr>';
            $_html[] = '<td>經辦:</td>';
            $_html[] = '<td></td>';
            $_html[] = '<td></td>';
            $_html[] = '<td>主任/園長:</td>';
            $_html[] = '<td></td>';
            $_html[] = '<td>執行長:</td>';
            $_html[] = '<td></td>';
            $_html[] = '<td></td>';
            $_html[] = '</tr>';
            $_html[] = '</table>';
          } else {
            $_html[] = '</table><br><br>';
          }

        }


      }
      $_html = implode('', $_html);
      $html = str_replace('{#html#}', $_html, $html);
      $mpdf->SetTitle($wkMonth);
       $mpdf->SetHTMLHeader('<h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3><h4  style="text-align: center;">'.($v1['wkYear']-1911).'年'.$v1['title'].'節金清冊</h4>');
      $mpdf->SetWatermarkText('MARIA');
      $mpdf->showWatermarkText = true;
      $mpdf->SetHTMLFooter('<p style="text-align:right;">頁次：{PAGENO}</p>');
      $mpdf->WriteHTML($html);
      $mpdf->Output();
    }





  }elseif($_REQUEST['excel_type']=='duty'){//出勤表
    

  }






  
?>