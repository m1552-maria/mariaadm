<?php
	session_start();
	include("../../config.php");

	if($_SESSION['privilege'] <= 10 && count($_SESSION['user_classdef'][7]) == 0) {
		echo "請設定管理的計畫編號";
		exit;
	}

	$type_list = array();
	$sql = ' select `id`,`title`, YEAR(`festivalDate`) as year from `bonus_festival` where `type`='."'".'festival'."' order by festivalDate desc";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$type_list[$r['id']] = $r['year'].'年度-'.$r['title'];
		}
	}


?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<form name="excelfm2" action="exportExcel.php" method="post" target="excel">
<?php if($_GET['name'] == 'excelfm') { ?>		
<div>
	清冊類別:
	<select name="printType" id="printType">
		<option value="excel"> EXCEL </option>
		<option value="pdf"> PDF </option>
	</select>
</div>
<br>
<?php } ?>
計畫編號:
<?php if($_SESSION['privilege'] > 10) { ?>
	<input type="text" name="projIDFilter" id="projIDFilter" value="">
<?php }else{?>
	<div>
		<label><input type="checkbox"  value="全選" onclick="allCheck(this)">全選</label>
		<?php 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo '<label><input type="checkbox" name="projIDFilter" value="'.$v.'">'.$v.'</label>';
	    ?>
	</div>

<?php } ?>

 <select name="fid" id="fid">
	 <option value="">--請選擇節金--</option>
<?php 
	foreach($type_list as $key => $val) { 
		$selected = ($key == $_GET['fid']) ? 'selected' : '';
		echo '<option value="'.$key.'" '.$selected.' >'.$val.'</option>';
	}
?>

</select>
<input type="button" onclick="excelfm2Smi()" value="確定">
<script src="../../Scripts/jquery-1.12.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
	function excelfm2Smi() {
<?php if($_SESSION['privilege'] > 10) { ?>
		window.opener.excelfm.projID.value=$('#projIDFilter').val();
		window.opener.excelCount.projID.value=$('#projIDFilter').val();
<?php } else { ?> 
		var projID = [];
		$('[name="projIDFilter"]').each(function(){
			if(this.checked) projID.push($(this).val());
		});
		window.opener.excelfm.projID.value=projID.join(',');
		window.opener.excelCount.projID.value=projID.join(',');
<?php } ?>

<?php if($_GET['name'] == 'excelfm') { ?>		
	window.opener.excelfm.printType.value=$('#printType').val();
<?php } ?>
		window.opener.excelfm.fid.value=$('#fid').val();
		window.opener.excelCount.fid.value=$('#fid').val();



		//window.opener.excelfm.submit();
		window.opener.<?php echo $_GET['name']?>.submit();
		window.close();
	}

	function allCheck(obj) {
		var pList = document.getElementsByName('projIDFilter');
		for(var i=0;i<pList.length;i++) {
			pList[i].checked = obj.checked;
		} 
	}
</script>
</form>
</body>
</html>