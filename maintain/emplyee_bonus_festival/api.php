<?php
include "../../config.php";

switch ($_POST['type']) {
	case 'recount':
		recount();
		break;
	case 'handChange':
		handChange();
		break;
}


function handChange() {
	$sql = "update emplyee_bonus_festival set money='".(int)$_POST['money']."', isHand='1' where fid='".(int)$_POST['fid']."' and empID='".$_POST['empID']."'";
	db_query($sql);
}

function recount(){
	//讀取原有資料
	$before_empID = array();
	$before_depID = array();
	$sql = 'select * from `bonus_festival` where `id`='.$_POST['fid'];
	$rs = db_query($sql);
	$r  = db_fetch_array($rs); 
	$festivalDate = $r['festivalDate'];


	//處理應該要新增到清冊的資料
	//emplyee_bonus_festival
	//年資
	//今年和明年會需要的東西的array
	$now_year = date('Y');
	$all_data[$now_year] = array(
		'now_year'=>date('Y'),
		'now_date'=>date('Y').'-12-31',
		'before_date'=>(date('Y')-1).'-12-31',
		'now_start_day'=>strtotime(date('Y').'-01-01'),
		'now_end_day'=> strtotime(date('Y').'-12-31')
	);



	//抓emplyee員工列表
	$emplyee_list = array();
	$sql = "select `empID`,`isOnduty`,`empName`,`hireDate`,`jobClass`,`jobLevel`,`projID` from `emplyee` where `isOnduty` IN (1,2) and `empID`='$_POST[empID]'";//在職 留職停薪 因為這兩種狀態都是在職	
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		if($r['empID']!='' && $r['hireDate']!=''){
			$emplyee_list[$r['empID']] = $r;
		}
	}
	

	
	//留職停薪
	$job_status = array();
	$sql = 'select s.* from `emplyee_status` s inner join `emplyee` e on s.empID = e.empID where s.`jobStatus` IN (3,4) and s.bdate >= e.hireDate and e.`empID`='."'".$_POST['empID']."'";//留職停薪或是延長留停
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)){
		$bdate = strtotime($r['bdate']);
		$edate = strtotime($r['edate']);
		if($edate>$bdate){
			$job_status['all_year'][$r['empID']][$r['id']] = $r;//每一年的留職停薪//當年不能計算
		}
	}

	

	//開始計算特休
	$emplyee_final_list = array();//員工會計年度以及要發多少
	$insertdata = array();
	foreach($emplyee_list as $k=>$v){
		foreach($all_data as $k1=>$v1){//今年和明年特休
			//宣告全部變數
			$now_year = $v1['now_year'];
			$now_date = $v1['now_date'];
			$before_date = $v1['before_date'];
			

			//計算年資
			$now_day = (strtotime($now_date) - strtotime($v['hireDate']))/86400+1;//天數


			//判斷留職停薪//去年之前的
			$leave = 0;//留職停薪日數
			if(isset($job_status['all_year'][$k])){
				foreach($job_status['all_year'][$k] as $k2=>$v2){
					//處理輸入錯誤的
					if($v2['bdate'] == '' || $v2['bdate']=='0000-00-00'){
						$v2['bdate'] = $before_date;//去年年底
					}
					if($v2['edate'] == '' || $v2['edate']=='0000-00-00'){
						$v2['edate'] = $before_date;//去年年底
					}


					//判斷要算到甚麼時候//計算到今年以前的年底//有符合才要扣掉
					$bdate = strtotime($v2['bdate']);
					$edate = strtotime($v2['edate']);

					if($edate>$bdate){//這樣才是有效數據
						if($bdate<strtotime($before_date)){//計算到去年的留職停薪總共有多少天
							if($edate>strtotime($before_date)){//表示超過去年年底了
								$leave = $leave + ((strtotime($before_date)-$bdate))/86400+1;
							}else{//正常情況
								$leave = $leave + ($edate-$bdate)/86400+1;
							}
						}
					}
				}
			}

			
			//計算會計年資
			$actDays = (strtotime($before_date) - strtotime($v['hireDate']))/86400+1-$leave;//會計年資要扣掉留職停薪//天數
			if($actDays<0){
				$actDays = 0;//因為不滿一年
			}
			$year = date('Y',strtotime($festivalDate));

			
			$emplyee_final_list[$v['empID']]['empID'] = $v['empID'];
			$emplyee_final_list[$v['empID']]['jobLevel'] = $v['jobLevel'];
			$emplyee_final_list[$v['empID']]['jobClass'] = $v['jobClass'];
			$emplyee_final_list[$v['empID']]['hireDate'] = $v['hireDate'];
			$emplyee_final_list[$v['empID']]['years'] = intval($actDays/365);
			$emplyee_final_list[$v['empID']]['projID'] = $v['projID'];
			$emplyee_final_list[$v['empID']]['wkYear'] = $year;
			//計算應該給多少
			$emplyee_final_list[$v['empID']]['money'] = money($actDays,$v['hireDate'],$v['jobClass'],$festivalDate);
			
		}
	}

	//寫入清冊 emplyee_bonus_festival
	foreach ($emplyee_final_list as $k => $v) {
		//修改相關的檔案
		$sql = ' update emplyee_bonus_festival set money='."'".$v['money']."'".',hireDate='."'".$v['hireDate']."'".',jobLevel='."'".$v['jobLevel']."'".',jobClass='."'".$v['jobClass']."'".',years='."'".$v['years']."'".',projID='."'".$v['projID']."', isHand='0'".' where fid='.$_POST['fid'].' and empID='."'".$v['empID']."'";
		db_query($sql,$conn);
	}
}


function money($actDays,$hireDate,$jobClass,$festivalDate){
	$money = 0;
	$hireDate = date('Y-m-d',strtotime($hireDate));
	$festivalDate = date('Y-m-d',strtotime($festivalDate));

	$hire_month = date('m',strtotime($hireDate));
	$hire_year = date('Y',strtotime($hireDate));
	$festivalDate_month = date('m',strtotime($festivalDate));
	$festivalDate_year = date('Y',strtotime($festivalDate));

	$year = intval($actDays/365);
	// echo $actDays."\n";
	// echo $hireDate."\n";
	// echo $jobClass."\n";
	// echo $festivalDate."\n";
	// echo $year."\n";
	// echo $hire_month."\n";
	// echo $festivalDate_month."\n";
	// echo $hire_year."\n";
	// echo $festivalDate_year."\n";


	if($year==0 && $hireDate>$festivalDate){
		$money = 0;
	}elseif($year==0 && $hireDate<$festivalDate && ($hire_month==$festivalDate_month && $hire_year==$festivalDate_year)){
		$money = 0;
	}else{
		if($jobClass=='短期人力' || $jobClass=='短期工讀生' || $jobClass=='短期替代性'){
			$money = 0;
		}elseif($jobClass=='庇護性全工時' || $jobClass=='庇護性部分工時' || $jobClass=='外籍' || $jobClass=='多元就業'){
			$money = 500;
		}elseif($jobClass == '支持性全工時' || $jobClass == '支持性部分工時'){
			if($year<1){
				$money = 500;
			}else{
				$money = 1000;
			}
		}elseif($jobClass == '部分工時'){
			if($year<2){
				$money = 500;
			}elseif($year<3){
				$money = 1000;
			}elseif($year>=3){
				$money = 1500;
			}
		}elseif($jobClass == '全工時'){
			if($year<1){
				$money = 500;
			}elseif($year<2){
				$money = 1000;
			}elseif($year<3){
				$money = 2000;
			}elseif($year>=3){
				$money = 3000;
			}
		}else{//沒有填寫任何職位
			$money=0;
		}

	}
	return $money;
}


?>