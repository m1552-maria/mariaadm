<?
  session_start();
 
  $filename = '節金統計表'.date("Y-m-d H:i:s",time()).'.xlsx';

  /*echo "<pre>";
  print_r($_POST);
  exit;*/

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');

  $sql = $_REQUEST['sqltx']. ' where a.fid="'.$_REQUEST['fid'].'" ' ;

  if($_REQUEST['projID']) {
    $pList = explode(',', $_REQUEST['projID']);
    $aa = array();
    foreach ($pList as $key => $v) {
      if($v)  $aa[]="b.projID like '$v%'";  
    }
    $sql .= " and (".join(' or ',$aa).")";   

  } elseif ($_SESSION['privilege'] <= 10) {
    if($_SESSION['user_classdef'][7]) {
      $aa = array();
      foreach($_SESSION['user_classdef'][7] as $v){
        if($v)  $aa[]="b.projID like '$v%'";  
      }
      $sql .= " and (".join(' or ',$aa).")";   
    } else {
      $sql .=  " and b.projID='-1'";
    }
  }

  $sqltx = $sql." and a.money > 0 group by a.money order by a.money";

  /*print_r($sql);
  exit;*/

  $rs = db_query($sqltx);
  if(db_num_rows($rs) == 0) exit;
  $moneyList = array();
  $wkYear = 0;
  $title = '';

  $header = array(
    'A'=>array('key'=>'projID', 'val'=>'計畫編號'),
    'B'=>array('key'=>'depID', 'val'=>'部門'),
    'C'=>array('key'=>'depID2', 'val'=>'組別'),
    'D'=>array('key'=>'projID', 'val'=>'人數', 'con'=>0)
  );

  $i = 4;
  while ($r = db_fetch_array($rs)) {
    $i++;
    $header[chr(64+$i)] = array('key'=>$r['money'], 'val'=>$r['money'], 'con'=>0);

    $moneyList[$r['money']] = array('total'=>0, 'allTotal'=>0);
    $wkYear = $r['wkYear'];
    $title = $r['title'];
  }
  $i++;
  $header[chr(64+$i)] = array('key'=>'total', 'val'=>'節金合計',  'con'=>0);

 /* echo "<pre>";
  print_r($header);
  exit;
*/

  $wkYear -= 1911;

  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();

  $width = 5+count($moneyList);
  $lastField = chr(64+$width);

  $sheet->mergeCells('A1:'.$lastField.'1');//合併
  $sheet->getStyle('A1:'.$lastField.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  //$sheet->getStyle('A1:'.$width.'1')->getFont()->setSize(16);
  $sheet->setCellValue('A1','財團法人瑪利亞社會福利基金會');

  $sheet->mergeCells('A2:'.$lastField.'2');//合併
  $sheet->getStyle('A2:'.$lastField.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  //$sheet->getStyle('A2:'.$width.'3')->getFont()->setSize(16);
  $sheet->setCellValue('A2',$wkYear.'年'.$title.'獎金統計表');

  $j = 3;
  foreach ($header as $key => $value) {
    $sheet->setCellValue($key.$j, $value['val']);
  }

 
  
  $sql .= " and a.money > 0 order by b.projID asc, b.depID asc";
  $rs = db_query($sql);

  $dataList = array();
  $depID = '';
  $i = 0;
  while ($r = db_fetch_array($rs)) {
    $projID = $r['projID'];
    if($depID != $r['depID'] || ($depID == $r['depID'] && $projID != $r['projID'])) {
      $i++;
      $depID = $r['depID'];
      $dataList[$i] = array(
        'projID' => $r['projID'],
        'depTop' => $departmentinfo[substr($r['depID'], 0, 1)],
        'depID' => $departmentinfo[$r['depID']],
        'pCount' => 0 //人數
      );
      if($dataList[$i]['depTop'] == $dataList[$i]['depID']) $dataList[$i]['depID'] = '';
      foreach ($moneyList as $key => $value) {
        $dataList[$i][$key] = 0;
      }
      $dataList[$i]['mCount'] = 0;//節金合計
    }

    $dataList[$i]['pCount'] ++;
    $dataList[$i]['mCount'] += $r['money'];
    $dataList[$i][$r['money']] ++;
  }
//$sheet->setCellValue
  //$departmentinfo

  $projID = 0;
 // $moneyList
  $pCount = array('total'=>0, 'allTotal'=>0);
  $mCount = array('total'=>0, 'allTotal'=>0);
  foreach ($dataList as $key => $value) {
    $i=0;
    $j++;
    if($projID == 0) $projID = substr($value['projID'], 0, 2);
    if(substr($value['projID'], 0, 2) != $projID) {
      $projID = substr($value['projID'], 0, 2);
      $sheet->setCellValue('A'.$j, '小計');
      $sheet->setCellValue('D'.$j, $pCount['total']);
      $z = 4;
      foreach ($moneyList as $mk => $mv) {
        $z++;
        $sheet->setCellValue(chr(64+$z).$j, $mv['total']);
        $moneyList[$mk]['total'] = 0;
      }
      $sheet->setCellValue(chr(64+$z+1).$j, $mCount['total']);
      $sheet->getStyle('A'.$j.':'.chr(64+$z+1).$j)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'DFEDFD')));
      $j++;
      $pCount['total'] = 0;
      $mCount['total'] = 0;

    }

    $pCount['total'] += $value['pCount'];
    $pCount['allTotal'] += $value['pCount'];
    $mCount['total'] += $value['mCount'];
    $mCount['allTotal'] += $value['mCount'];

    foreach ($value as $k2 => $v2) {
      if(isset($moneyList[$k2])) {
        $moneyList[$k2]['total']+=$v2;
        $moneyList[$k2]['allTotal']+=$v2;
      }
      $i++;
      $sheet->setCellValue(chr(64+$i).$j, ($v2) ? $v2 : '');
    }
  }

  $j++;
  $sheet->setCellValue('A'.$j, '小計');
  $sheet->setCellValue('D'.$j, $pCount['total']);
  $z = 4;
  foreach ($moneyList as $mk => $mv) {
    $z++;
    $sheet->setCellValue(chr(64+$z).$j, $mv['total']);
  }
  $sheet->setCellValue(chr(64+$z+1).$j, $mCount['total']);
  $sheet->getStyle('A'.$j.':'.chr(64+$z+1).$j)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'DFEDFD')));

  $j++;
  $sheet->setCellValue('A'.$j, '合計');
  $sheet->setCellValue('D'.$j, $pCount['allTotal']);
  $z = 4;
  foreach ($moneyList as $mk => $mv) {
    $z++;
    $sheet->setCellValue(chr(64+$z).$j, $mv['allTotal']);
  }
  $sheet->setCellValue(chr(64+$z+1).$j, $mCount['allTotal']);
  $sheet->getStyle('A'.$j.':'.chr(64+$z+1).$j)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'a2c770')));


  /*$sheet->getStyle('A1')->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'50696B')));
  $sheet->getStyle('A1')->getFont()->getColor()->setARGB('FFFFFF');
  //$sheet->getStyle('A2:Q3')->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'DFEDFD')));*/

  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');


?>