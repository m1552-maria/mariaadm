<?php 
	if(!session_id()) session_start();
	// for Access Permission Control
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	//$bE = true;
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "經歷";
	$tableName = "emplyee_experience";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['EE_EID'] = $eid;
	} else $eid = $_SESSION['EE_EID'];
	$filter = "empID='$eid'";
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "job";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,job,peoples,isHigh,content,bdate,edate,response,notes,rdate");
	$ftAry = explode(',',"ID,公司名稱,職稱,人數,經歷,工作描述,期間(起),期間(迄),管理責任,備註,登錄日期");
	$flAry = explode(',',"80,120,80,,,,,,,100,");
	$ftyAy = explode(',',"id,text,text,text,bool,text,date,date,text,text,text");
	
	$isHighAry=array("0"=>'一般','1'=>'主要','2'=>'次要');
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"empID,title,job,peoples,isHigh,content,bdate,edate,response,notes,quitReason,attach");
	$newftA = explode(',',"員工編號,公司名稱,職稱,人數,經歷,工作描述,期間(起),期間(迄),管理責任,備註,離職原因,相關文件上傳");
	$newflA = explode(',',",,,,,30,,,,30,30,");
	$newfhA = explode(',',",,,,,5,,,,3,3,");
	$newfvA = array('','','','',$isHighAry,'','','','','','');
	$newetA = explode(',',"empID,text,text,select,select,textbox,date,date,select,textbox,textbox,multiPhoto");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,job,peoples,isHigh,content,bdate,edate,response,notes,quitReason,attach");
	$editftA = explode(',',"ID,公司名稱,職稱,人數,經歷,工作描述,期間(起),期間(迄),管理責任,備註,離職原因,相關文件上傳");
	$editflA = explode(',',",,,,,30,,,,30,30,");
	$editfhA = explode(',',",,,,,5,,,,3,3,");
	$editfvA = array('','','','',$isHighAry,'','','','','','');
	$editetA = explode(',',"id,text,text,select,select,textbox,date,date,select,textbox,textbox,multiPhoto");
?>