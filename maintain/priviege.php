<?php

	$funcs = array(
	 	'A' => '基本資料維護','A1'=>'部門 資料維護','A2'=>'職掌 資料維護','A3'=>'員工 資料維護','A4'=>'欄位資料對應表','A5'=>'年資/特休','A6'=>'特休信件',
		'B' => '制度規章維護','B1'=>'制度規章 分類表','B2'=>'發佈 制度規章',
		'C' => '知識平台維護','C1'=>'知識平台 分類表','C2'=>'發佈 知識平台',
		'D' => '後台帳號維護','D1'=>'後台帳號維護','D2'=>'等級功能設定',
		'E'	=> '最新公告',
		'F' => '最新消息',
		'G' => '授權資料維護',
		'H' => '系統參數設定',
		'I' => '全員行事曆',
		'J' => '保險系統',
		'K' => '請假系統','K1' => '綜合申請單','K2' => '外出單',
		'L' => '資訊查閱維護','L1'=>'資訊查閱 分類表','L2'=>'發佈 資訊查閱',
		'M' => '資源管理維護','M1'=>'資源管理 分類表','M2'=>'發佈 資源內容',
		'N' => '影像事件管理','N1'=>'影像事件分類表','N2'=>'發佈影像事件',
		'O' => '員工排班作業',
		'P' => '雜項費用',
		'Q' => '薪資作業',
		'R' => '打卡系統','R1'=>'打卡資料','R2'=>'匯入打卡資料','R3'=>'列印識別卡','R4'=>'出勤資料',
		'S' => '出納作業',
    	'T' => '郵件編輯',
    	'U' => '綜合所得稅','U1'=>'薪資、獎金統計(50)','U2'=>'薪資、獎金統計(91)','U3'=>'媒體檔作業','U4'=>'退休金自提',
    	'V' => '各類報表',
		'Z' => '預算管理系統','Z1'=>'公文系統','Z2'=>'財管系統'
	);
		
	$prvstr = array(1=>'基本帳號',10=>'部門管理',100=>'機構管理',110=>'人事',120=>'財務管理',130=>'預算管理',140=>'行政管理',141=>'行政管理1',142=>'行政管理2',150=>'出納管理',160=>'稅務管理',255=>'系統總管理');	
	//:: Database handles
	if($_REQUEST[DoSubmit]) {
		foreach($_REQUEST as $k=>$v) {
			$varAR = explode('~',$k);
			if($varAR[1]) $prvilige[$varAR[0]][] = $varAR[1];
			if($k=='NewFunc') {
				$varAR2 = explode('~',$v);
				if($varAR2[1]) $prvilige[$varAR2[0]][] = $varAR2[1];
			}
		}
		$JSstr = json_encode($prvilige);
		file_put_contents('priviege.txt',$JSstr);
	}
	
	/* :: Create
  $priviege = array();
	$priviege[255] = 'ABCDEFG';
	$priviege[100] = 'ABCD';
	$priviege[10] = 'G'; 
	print_r($priviege);
	file_put_contents('priviege.txt',json_encode($priviege)); */
	
	$str = file_get_contents('priviege.txt');
	$priviege = json_decode($str);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>等級功能設定</title>
	<link rel="stylesheet" href="list.css" />
	<script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
	<script>
	function addNewFunc(btn,level) {
		var fc = $(btn).prev().val();
		$('#NewFunc').val(level+'~'+fc);
		$('#DoSubmit').click();
	}
	</script>
</head> 
<body>
<p class="headTX">等級功能設定</p>
<form id="funcFM" name="funcFM" method="post" action="priviege.php" onsubmit="return true">
<table class="sText">
<? foreach($priviege as $k => $v) { ?>
 <tr valign="middle">
 	<td align="right" valign="top" style="font:bold 15px '微軟正黑體',Verdana"><?=$prvstr[$k]?> :</td>
 	<td>
		<?
			sort($v); 
			$unList = array_diff(array_keys($funcs),$v); 
			$char = '';	//funtions list char 1
			foreach($v as $v2) {
				if(!$char) {
					$char = substr($v2,0,1);
					echo "<input type='checkbox' name='$k"."~"."$v2' checked>$funcs[$v2]"; 
				} else {
					if($char!=substr($v2,0,1)) { echo '<br>'; $char=substr($v2,0,1); }
					echo "<input type='checkbox' name='$k"."~"."$v2' checked>$funcs[$v2]"; 
				}
			}
		?>
  </td>
 </tr>
 <? if( count($unList) > 0 ) { ?>
  <tr>
  	<td>&nbsp;</td>
  	<td><select size="1"><? foreach($unList as $vv) echo "<option value='$vv'>$funcs[$vv]</option>" ?></select> <span onclick="addNewFunc(this,'<?=$k?>')">[ 新增 ]</span></td>
  </tr>
 <? } ?>
 <tr height="8"><td colspan="2"></td></tr>  
<? } ?>
</table>
<p>
	<input type="hidden" id="NewFunc" name="NewFunc" value="" />
	<input type="submit" id="DoSubmit" name="DoSubmit" value="確定" />
	<input type="reset" name="Reset" value="重設" />
</p>
</form>
</body>
</html>