<?php
	session_start();
	include("../../config.php");
	include("init.php");	

	//處理上傳的附件
	$attach=array();
	if($_FILES['attach']){//::多檔上傳
		if(!is_dir($ulpath."/".$_SESSION['EE_EID'])){	mkdir($ulpath."/".$_SESSION['EE_EID']);	}
		if(!is_dir($ulpath."/".$_SESSION['EE_EID']."/status")){	mkdir($ulpath."/".$_SESSION['EE_EID']."/status");	}
		foreach($_FILES['attach']["name"] as $k=>$v){
			$finf = pathinfo($v); 
			/*20180424 sean 加入若是檔案名稱有逗號 用底線取代*/
			$_FILES['attach']["name"][$k] = str_replace(',','_',$_FILES['attach']["name"][$k]);
			$dstfn = md5($_FILES['attach']["name"][$k]).'.'.$finf['extension'];
			if($_FILES['attach']["tmp_name"][$k]=="")	continue;
			copy($_FILES['attach']["tmp_name"][$k],$ulpath."/".$_SESSION['EE_EID']."/status/".$dstfn);
			array_push($attach,$_FILES['attach']["name"][$k]);
			
		}
	}else{
		foreach($_FILES as $k=>$v) {
			$fn = $_FILES[$k]['name'];
			if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
			if ($$k != "") {
				if (!copy($$k,"$ulpath$fn2")) { //上傳失敗
					echo '附件上傳作業失敗 !'; exit;
				}	
			}
		}
	}
	$n = count($newfnA);
	$fields=array();
	$valAry=array();
	for ($i=0; $i<$n; $i++) {
		array_push($fields,$newfnA[$i]);
  		switch($newetA[$i]) {
			case "hidden" :
			case "select" :
			case "readonly" :
			case "textbox":
			case "id":
			case "text" :	$fldv = "'".addslashes($_REQUEST[$newfnA[$i]])."'"; break;
			case "textarea" :	$fldv = "'".trim($_REQUEST[$newfnA[$i]])."'"; break;
			case "checkbox" : if(isset($_REQUEST[$newfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".$_REQUEST[$newfnA[$i]]."'"; break;
			case "file" : $fldv = "'$fn'"; break;
			case "empID":	$fldv="'".$_SESSION['EE_EID']."'";	break;	
			case "radio" : 	$fldv="'".$_REQUEST[$newfnA[$i]]."'"; 	break;
			case "multiPhoto":	
				$fldv="'".join(",",$attach)."'";	
				break;
	  	}
		array_push($valAry,$fldv);
	}	
	$sql = "insert into $tableName ";
	$sql .= '('.join(",",$fields).") values(".join(",",$valAry).')';
	db_query($sql);


	//留職停薪才要執行
	if($_POST['jobStatus']==3||$_POST['jobStatus']==4){
		//處理特休
		/*$for_empID = $_SESSION['EE_EID'];
		include("../emplyee_annual/api.php");*/
		//特休年資處理 計算今年與預算下一年
		include_once("../emplyee_annual/emplyeeAnnualApi.php");
		$nowYear = (int)date('Y');
		for($i=$nowYear; $i<=($nowYear+1); $i++) {
			$eAData = array('empID'=>$_SESSION['EE_EID'], 'wkYear'=>$i,'insertDB'=>1);
			$eA = emplyeeAnnualApi($eAData);
		}
	}

	db_close();
	header("Location: list.php");
?>
