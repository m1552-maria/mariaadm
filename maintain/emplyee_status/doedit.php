<?php
	session_start();
	include("../../config.php");
	include("init.php");
	
	//處理上傳的附件
	$delOld = false;	//可否刪除舊檔
	$attach=array();
	$newAttach=array();
	if(!is_dir($ulpath."/".$_SESSION['EE_EID'])){	mkdir($ulpath."/".$_SESSION['EE_EID']);	}
	if(!is_dir($ulpath."/".$_SESSION['EE_EID']."/status")){	mkdir($ulpath."/".$_SESSION['EE_EID']."/status");	}
	//::處理刪除的附件檔
	$sql="select * from $tableName where id=".$_REQUEST['selid'];
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$delAry=array();
		$r=db_fetch_array($rs);
		$extAttach=explode(",",$r['attach']);
		if($r['attach']!=""){
			for($i=0;$i<count($extAttach);$i++){
				$finf = pathinfo($extAttach[$i]);
				if(!isset($_REQUEST['attach_'.md5($extAttach[$i])])){
					array_push($delAry,md5($extAttach[$i]).'.'.$finf['extension']);
				}else{//還保留的圖檔
					array_push($newAttach,$extAttach[$i]);
				}
			}
			for($i=0;$i<count($delAry);$i++){
				if(is_file($ulpath."/".$_SESSION['EE_EID']."/status/".$delAry[$i])){	unlink($ulpath."/".$_SESSION['EE_EID']."/status/".$delAry[$i]);}
			}
		}
	}
	if($_FILES['attach']){//::多檔上傳
		foreach($_FILES['attach']["name"] as $k=>$v){
			$finf = pathinfo($v); 
			/*20180424 sean 加入若是檔案名稱有逗號 用底線取代*/
			$_FILES['attach']["name"][$k] = str_replace(',','_',$_FILES['attach']["name"][$k]);
			$dstfn = md5($_FILES['attach']["name"][$k]).'.'.$finf['extension'];
			if($_FILES['attach']["tmp_name"][$k]=="")	continue;
			copy($_FILES['attach']["tmp_name"][$k],$ulpath."/".$_SESSION['EE_EID']."/status/".$dstfn);
			array_push($attach,$_FILES['attach']["name"][$k]);
		}
	}else{
		foreach($_FILES as $k=>$v) {
			$fn = $_FILES[$k]['name'];
			if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
			if ($$k != "") {
				if (!copy($$k,"$ulpath$fn2")) { //上傳失敗
					echo '附件上傳作業失敗 !'; exit;
				} else $delOld=true;
			}
		}
	}
	$sql = "update $tableName set ";
  	$fields=array();
	$n = count($editfnA);
	$isHighC=0;
	for($i=1; $i<$n; $i++) {
		if ( ($fn=='') && ($editfnA[$i]==$delField)) continue;	//沒有上傳的檔案
		switch($editetA[$i]) {
			case "hidden" :
			case "select" :		
	  		case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
			case "textbox" :
  			case "textarea" :	$fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
  			case "checkbox" : if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
			case "file" : $fldv = "'$fn'"; break;
			case "radio" : 	$fldv="'".$_REQUEST[$editfnA[$i]]."'"; 	break;
			case "multiPhoto":	
				$ary=array();
				if(count($newAttach)>0){	array_push($ary,join(',',$newAttach));	}
				if(count($attach)>0){	array_push($ary,join(',',$attach));	}
				$fldv="'".join(",",$ary)."'";	
				break;
		}
		array_push($fields,$editfnA[$i].'='.$fldv);
	}
	$sql .= join(",",$fields)." where $editfnA[0]='".$_REQUEST['selid']."'";
	db_query($sql);

	//處理特休
	/*$for_empID = $_SESSION['EE_EID'];
	include("../emplyee_annual/api.php");*/
	include_once("../emplyee_annual/emplyeeAnnualApi.php");
	$nowYear = (int)date('Y');
	for($i=$nowYear; $i<=($nowYear+1); $i++) {
		$eAData = array('empID'=>$_SESSION['EE_EID'], 'wkYear'=>$i,'insertDB'=>1);
		$eA = emplyeeAnnualApi($eAData);
	}

	db_close();	
	header("Location: list.php?pages=".$_REQUEST['pages']."&keyword=".$_REQUEST['keyword']."&fieldname=".$_REQUEST['fieldname']."&sortDirection=".$_REQUEST['sortDirection']."&selid=".$_REQUEST['selid']);
?>