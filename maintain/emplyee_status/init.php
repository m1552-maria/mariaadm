<?php 
	// for Access Permission Control
	if(!session_id()) session_start();
	if ($_SESSION['privilege']<10) {
	  	header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	if((isset($_SESSION["canEditEmp"]) && $_SESSION['privilege']<=10) || $_SESSION['privilege']>10){
		$bE2=true;
	}else{
		$bE2=false;
	}
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "職況";
	$tableName = "emplyee_status";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['EE_EID'] = $eid;
	} else $eid = $_SESSION['EE_EID'];
	$filter = "empID='$eid'";
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "jobStatus";
	$searchField2 = "bdate";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,jobStatus,bdate,edate,content,rdate");
	$ftAry = explode(',',"ID,職況類別,起日,迄日,說明,登錄日期");
	$flAry = explode(',',"100,150,150,150,,100");
	$ftyAy = explode(',',"id,select,date,date,text,date");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"empID,jobStatus,bdate,edate,content,attach");
	$newftA = explode(',',"員工編號,職況類別,起日,迄日,說明,相關文件上傳");
	$newflA = explode(',',"100,,,,60,");
	$newfhA = explode(',',",,,,3,");
	$newfvA = array('',$jobStatusChange,'','','','');
	$newetA = explode(',',"empID,select,date,date,textbox,multiPhoto");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,jobStatus,bdate,edate,content,attach");
	$editftA = explode(',',"ID,職況類別,起日,迄日,說明,相關文件上傳");
	$editflA = explode(',',",,,,60,");
	$editfhA = explode(',',",,,,3,");
	$editfvA = array('',$jobStatusChange,'','','','');
	$editetA = explode(',',"id,select,date,date,textbox,multiPhoto");

?>