<?
  session_start();
  $filename = date("Y-m-d").'.xlsx';//ie中文檔名亂碼
  header("Content-type:application/vnd.ms-excel");
  header("Content-Type:text/html; charset=utf-8");
  header('Content-Disposition:attachment;filename='.iconv('utf-8', 'big5', urlencode('出勤表').$filename));//中文檔名亂碼的問題
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');

  //找出範圍內有外出值接下班的資料
  $sql = "select * from outoffice where bDate >= '".$_REQUEST['start_day']."' and bDate<='".$_REQUEST['end_day']." 23:59:59' and offwork=1";
  $rs = db_query($sql);
  $outofficeList = array();
  while ($r = db_fetch_array($rs)) {
    $d = date('Y-m-d', strtotime($r['bDate']));
    $outofficeList[$d][$r['empID']] = 1;
  }


  $filter = array();
  if(!empty($_REQUEST['depID'])) array_push($filter, " b.depID like '".$_REQUEST['depID']."%'");
  elseif ($_SESSION['privilege'] <= 10) {
    if($_SESSION['user_classdef'][2]) {
      $aa = array();
      foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="b.depID like '$v%'";
      array_push($filter, " (".join(' or ',$aa).")");
    } else array_push($filter, " b.depID=''");
  }

  if($_REQUEST['start_day']!='' && $_REQUEST['end_day']!=''){
    array_push($filter,'((`a`.`startTime`>='."'".$_REQUEST['start_day']."'".' or `a`.`startTime2`>='."'".$_REQUEST['start_day']."'".') and (`a`.`endTime`<='."'".$_REQUEST['end_day']." 23:59:59'".' or `a`.`endTime2`<='."'".$_REQUEST['end_day']." 23:59:59'))");
  }elseif($_REQUEST['start_day']!='' && $_REQUEST['end_day']==''){
    array_push($filter,'(`a`.`startTime2`>='."'".$_REQUEST['start_day']."'".' or `a`.`endTime2`>='."'".$_REQUEST['start_day']."'".")");
  }elseif ($_REQUEST['start_day']=='' && $_REQUEST['end_day']!='') {
    array_push($filter,'(`a`.`startTime2`<='."'".$_REQUEST['end_day']." 23:59:59'".' or `a`.`endTime2`<='."'".$_REQUEST['end_day']." 23:59:59'".")");
  }

  if(!empty($_REQUEST['keyword'])) {
    $searchField1 = "b.depID";
    $searchField2 = "b.empID";
    $searchField3 = "b.empName";
    $keyword = $_REQUEST['keyword'];
    $filter[] = "(($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";
  }

  //排除不須打卡
  array_push($filter,'`b`.`noSignIn`=0');  
  
  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();
  // 設定欄位背景色(方法1)
  $sheet->getStyle('A3:L4')->applyFromArray(
    array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'C0C0C0')))
  );
  //合併儲存隔 
  $sheet->mergeCells("A1:L1");
	$sheet->mergeCells("A2:K2");
  $sheet->mergeCells("A3:A4");
  $sheet->mergeCells("B3:B4");
  $sheet->mergeCells("C3:C4");
  $sheet->mergeCells("D3:D4");
	$sheet->mergeCells("E3:E4");
  $sheet->mergeCells("F3:G3");
  $sheet->mergeCells("H3:I3");
  $sheet->mergeCells("J3:K3");
	$sheet->mergeCells("L3:L4");
  //對齊方式
  $sheet->getStyle('A1:L4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER); 

  $dep = '';
  if($_REQUEST['depID']!='') $dep = $departmentinfo[$_REQUEST['depID']]; else $dep = '全部部門';
  
  $sheet->setCellValue('A1','出缺勤資料');
  $sheet->getStyle('A1')->getFont()->setSize(18);
  $sheet->setCellValue('A2','開始時間:'.$_REQUEST['start_day'].' 結束時間:'.$_REQUEST['end_day']);
  $sheet->setCellValue('L2','部門名稱:'.$dep);
  
  //跑header 迴圈用
  $headerstart = 3;
  $header = array('A','B','C','D','E','F','G','H','I','J','K','L');
  $headervalue = array('流水號','所屬部門','員工編號','員工名稱','日期','排班時間','','打卡時間','','外出/返回(下班)','','請假');
  foreach($header as $k=>$v) $sheet->setCellValue($v.$headerstart,$headervalue[$k]);   
  $headerstart = 4;
  $headervalue = array('','','','','','上班','下班','上班','下班','外出','返回');
  foreach($header as $k=>$v) $sheet->setCellValue($v.$headerstart,$headervalue[$k]);   
  $sheet->getStyle('A3:L4')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

	//:: 讀取外出資料
	$sql = "select * from emplyee_dutylog where (dType='outoffice_out' or dType='outoffice_back') and (d_datetime>='$_REQUEST[start_day]' and d_datetime<='$_REQUEST[end_day] 23:59:59')";
	$rsT = db_query($sql);
	$outData = array();
	while($rT=db_fetch_array($rsT)) {
		$dKey = date('ymd',strtotime($rT['d_datetime']));
		if($rT['dType']=='outoffice_out')	$outData[$rT['empID']][$dKey][0] = $rT['d_datetime'];	else $outData[$rT['empID']][$dKey][1] = $rT['d_datetime'];
	}
	//:: 讀取請假資料
	$sql = "select * from absence where (bDate>='$_REQUEST[start_day]' and bDate<='$_REQUEST[end_day] 23:59:59') and isOK=1 and isDel=0";
	$rsT = db_query($sql);
	$absData = array();
	while($rT=db_fetch_array($rsT)) {
		$absData[$rT['empID']][]=array($rT['aType'],$rT['bDate'],$rT['eDate']);	
	}	
	function absDataCK($eid,$aDate){
		global $absData;
		foreach($absData[$eid] as $v) {
      // if($aDate>=$v[1] && $aDate<=$v[1]) return $v;
			//:(2021/3/11 mic)改成同一天就show
			$td1 = explode(' ',$aDate);
			$td2 = explode(' ',$v[1]);
			if( $td1[0] == $td2[0] ) return $v;
		}
		return false;
	}
	
  //抓出勤表
  $sql="select a.*,b.empName,b.depID from emplyee_duty as a left join emplyee as b on a.empID=b.empID where ".implode(' and ', $filter).' order by `b`.`depID`,`a`.`empID`,`a`.`startTime` DESC,`a`.`startTime2` DESC';
	//echo $sql; exit;
  $rs = db_query($sql);
  
  $i = 5;
  $total = 1;
  while($rs && $r=db_fetch_array($rs)) {
		$dKey = date('ymd',strtotime($r['startTime']));
    $sheet->setCellValue('A'.$i,$total);
    $sheet->setCellValue('B'.$i,$departmentinfo[$r['depID']]);
    $sheet->setCellValueExplicit('C'.$i, $r['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
    $sheet->setCellValue('D'.$i,$r['empName']);
		$dLb = $r['startTime'] ? date('Y/m/d',strtotime($r['startTime'])) : date('Y/m/d',strtotime($r['startTime2']));
		$sheet->setCellValue('E'.$i,$dLb);

    $v = ($r['startTime'] != '') ? date('H:i',strtotime($r['startTime'])) : '';
    $sheet->setCellValue('F'.$i, $v);

    $v = ($r['endTime'] != '') ? date('H:i',strtotime($r['endTime'])) : '';
    $sheet->setCellValue('G'.$i, $v);

    $v = ($r['startTime2'] != '') ? date('H:i',strtotime($r['startTime2'])) : '';
    $sheet->setCellValue('H'.$i, $v);

    $v = ($r['endTime2'] != '') ? date('H:i',strtotime($r['endTime2'])) : '';
    $sheet->setCellValue('I'.$i, $v);

   /* $sheet->setCellValue('F'.$i,date('H:i',strtotime($r['startTime'])));
    $sheet->setCellValue('G'.$i,date('H:i',strtotime($r['endTime'])));
    $sheet->setCellValue('H'.$i,date('H:i',strtotime($r['startTime2'])));
    $sheet->setCellValue('I'.$i,date('H:i',strtotime($r['endTime2'])));*/
		
		$v = $outData[$r['empID']][$dKey][0]?date('H:i',strtotime($outData[$r['empID']][$dKey][0])):''; $sheet->setCellValue('J'.$i,$v);  

    $sDate = date('Y-m-d', strtotime($r['startTime']));

    if($outofficeList[$sDate][$r['empID']]) {
      $v = '直接下班'.($outData[$r['empID']][$dKey][1] ? '('.date('H:i',strtotime($outData[$r['empID']][$dKey][1])).')' : '');
    } else {
      $v = $outData[$r['empID']][$dKey][1]?date('H:i',strtotime($outData[$r['empID']][$dKey][1])):'';
    }

    $sheet->setCellValue('K'.$i,$v);  

	//	$v = $outData[$r['empID']][$dKey][1]?date('H:i',strtotime($outData[$r['empID']][$dKey][1])):''; $sheet->setCellValue('K'.$i,$v);  

		$rzt = absDataCK($r['empID'],$r['startTime']);
		if($rzt) {
			$v=$rzt[0]."\n".date('m/d H:i',strtotime($rzt[1])).' - '.date('m/d H:i',strtotime($rzt[2]));
			$sheet->setCellValue('L'.$i,$v);
		}
    //$sheet->getStyle('J'.$i)->getAlignment()->setWrapText(true);//要有這個才能換行
    //$sheet->getStyle('K'.$i)->getAlignment()->setWrapText(true);//要有這個才能換行
		$sheet->getStyle('H'.$i.':I'.$i)->getFont()->getColor()->setRGB('0080C0'); 
		$sheet->getStyle('J'.$i.':K'.$i)->getFont()->getColor()->setRGB('FF8040');
    $sheet->getStyle('A'.$i.':L'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    //$sheet->getStyle('A'.$i.':L'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);//垂直時要用setVertical	
    $i++; $total++;
  }
 
  //設定欄寬
  $width = array(6,10,10,10,12,8,8,8,8,8,8,30);
  foreach($header as $k=>$v){
    $sheet->getColumnDimension($v)->setWidth($width[$k]);  
  }

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  
  $objWriter->save('php://output');  
?>