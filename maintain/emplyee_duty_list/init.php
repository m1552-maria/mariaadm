<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../getEmplyeeInfo.php');
	
	

	$bE = $_SESSION['privilege']>10?true:false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');
	$pageTitle = "出勤資料";
	$tableName = "emplyee_duty";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee_duty/';
	$delField = '';
	$delFlag = true;
	$imgpath = '../../data/emplyee_duty/';


	//日期
	if(isset($_REQUEST['start_day'])){
		$bDate = $_REQUEST['start_day'];
	}else{
		$bDate = $_REQUEST['start_day'] = date('Y/m/d',strtotime(date('Y/m/d').'-7 days'));
	}
	if(isset($_REQUEST['end_day'])){
		$eDate = $_REQUEST['end_day'];
	}else{
		$eDate = $_REQUEST['end_day'] = date('Y/m/d');	
	}

	// date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."-4 hour")

	if($_REQUEST[depFilter]) $temp[] = "b.depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="b.depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "b.depID=''";
	}
	if($_REQUEST['start_day']){
		$temp[] = '(a.startTime >= '."'".str_replace('/', '-', $_REQUEST['start_day'])."' OR a.startTime2>="."'".str_replace('/', '-', $_REQUEST['start_day'])."')";
	}
	if($_REQUEST['end_day']){
		$temp[] = '(a.endTime <= '."'".str_replace('/', '-', $_REQUEST['end_day']).' 23:59:59'."' OR a.endTime2<="."'".str_replace('/', '-', $_REQUEST['end_day'])." 23:59:59')";
	}
	$temp[] = 'b.noSignIn=0';
	if(count($temp)>0) $filter = join(' and ',$temp);


	// Here for List.asp ======================================= //
	$defaultOrder = "b.depID";
	$defaultOrder1 = "b.empID";
	$defaultOrder2 = "a.startTime";
	$searchField1 = "b.depID";
	$searchField2 = "b.empID";
	$searchField3 = "b.empName";

	$pageSize = 25;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,depID,empID,startTime,startTime,endTime,startTime2,endTime2");
	$ftAry = explode(',',"編號,部門,員工編號,日期,排班起,排班迄,上班時間,下班時間");
	$flAry = explode(',',"80,,,,,,,");
	$ftyAy = explode(',',"ID,text,text,date,time,time,time,time");
	

	//找出範圍內有外出值接下班的資料
	$sql = "select * from outoffice where bDate >= '".str_replace('/', '-', $_REQUEST['start_day'])."' and bDate<='".str_replace('/', '-', $_REQUEST['end_day'])." 23:59:59' and offwork=1";
	$rs = db_query($sql);
	$outofficeList = array();
	while ($r = db_fetch_array($rs)) {
		$d = date('Y-m-d', strtotime($r['bDate']));
		$outofficeList[$d][$r['empID']][] = 1;
		$outofficeList[$d][$r['empID']][] = $r['rtnDate'];
	}

	/*echo "<pre>";
	print_r($outofficeList);
	exit;*/

?>
