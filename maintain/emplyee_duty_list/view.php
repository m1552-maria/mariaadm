<!DOCTYPE HTML>
<Html>
<Head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="<?=$extfiles?>list.css">
	<title><?=$pageTitle?></title>

	<link rel="stylesheet" href="<?=$extfiles?>list.css">
	<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
	<link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
	<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
	<script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
 	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
			case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
			case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
			case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>&start_day=<?=$bDate?>&end_day=<?=$eDate?>";
		if(depflt) url = url+'&depFilter='+depflt;
		location.href = url;
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	function clickEdit(aid) {	
		selid = aid;	
		<? if($bE){ ?> DoEdit();	<? } ?>
	}
	function filterSubmit(tSel) {
		tSel.form.submit();
	}

	function doSubmit() {
		form1.submit();
	}

	
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
 <style> 
 	.cencel{
		height: 20px;
		font-size: 20px;
		padding: 0 10px 5px 3px;
		border: 1px solid black;
		border-bottom:none;
		background-color: black;
		color: white;
		text-align: right;
		background-color: #4a2f2f;
 	}
 	.cencel-x{
 		color:white;
    cursor: pointer;
 	}

 	.content{
 		border: 1px solid black;
 		clear: both;
 		height: 165px;
 		overflow:auto;
 		padding: 10px 0;
 		background-color: white;
 		border-top:none;
 	}
 	.content table{
 		/*border: 1px solid #ffffff;*/
 	}
 	.content th{
 		background-color: #cd9696;
 		padding: 3px;
 	}
 	.content td{
 		padding: 3px;
 	}
 </style>
</Head>

<body>

<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">

    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>

   	開始日期:<input type="text" size="10" name="start_day" id="start_day" onchange="doSubmit()" value="<?=$bDate?>">
    <script type='text/javascript'>
			$(function(){
				$('#start_day').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});
				$('#start_day').datepicker('option', 'duration', '');
				$('#start_day').datepicker($.datepicker.regional['zh-TW']);
			});
    </script>

    ~結束日期:<input type="text" size="10" name="end_day" id="end_day" onchange="doSubmit()" value="<?=$eDate?>"> 
    <script type='text/javascript'>
			$(function(){
				$('#end_day').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});
				$('#end_day').datepicker('option', 'duration', '');
				$('#end_day').datepicker($.datepicker.regional['zh-TW']);
			});
    </script>
    
		<input type="button" name="toExcel" value="匯出出勤表" onclick="excelfm1.submit()">
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?><th><a href="#" class="head"><?=$ftAry[$i]?></a></th>	<? } ?>
  <th style="color:#FFF">外出時間</th>
  <th style="color:#FFF">返回時間</th>
  <th style="color:#FFF">請假</th>
  </tr>
<?
	//:: 讀取外出資料
	$sql = "select * from emplyee_dutylog where (dType='outoffice_out' or dType='outoffice_back') and (d_datetime>='$_REQUEST[start_day]' and d_datetime<='$_REQUEST[end_day] 23:59:59')";
	$rsT = db_query($sql);
	$outData = array();
	while($rT=db_fetch_array($rsT)) {
		$dKey = date('ymd',strtotime($rT['d_datetime']));
		if($rT['dType']=='outoffice_out')	$outData[$rT['empID']][$dKey][0] = $rT['d_datetime'];	else $outData[$rT['empID']][$dKey][1] = $rT['d_datetime'];
	}
	//:: 讀取請假資料
	$sql = "select * from absence where (bDate>='$_REQUEST[start_day]' and bDate<='$_REQUEST[end_day] 23:59:59') and isOK=1 and isDel=0";
	$rsT = db_query($sql);
	$absData = array();
	while($rT=db_fetch_array($rsT)) {
		$absData[$rT['empID']][]=array($rT['aType'],$rT['bDate'],$rT['eDate']);	
	}	
	function absDataCK($eid,$aDate){
		global $absData;
		foreach($absData[$eid] as $v) {
			// if($aDate>=$v[1] && $aDate<=$v[1]) return $v;
			//:(2021/3/11 mic)改成同一天就show
			$td1 = explode(' ',$aDate);
			$td2 = explode(' ',$v[1]);
			if( $td1[0] == $td2[0] ) return $v;
		}
		return false;
	}
	//var_dump($absData);
	
	//過濾條件
	if(isset($filter) && $filter!=''){
   	$sql="select a.*,b.empName,b.depID from $tableName as a left join emplyee as b on a.empID=b.empID where ($filter)"; 
		$outsqly = "select a.*,b.empName,b.depID from $tableName as a left join emplyee as b on a.empID=b.empID where ($filter)";

	}else{
		$sql="select a.*,b.empName,b.depID from $tableName as a left join emplyee as b on a.empID=b.empID where 1"; 
		$outsqly = "select a.*,b.empName,b.depID from $tableName as a left join emplyee as b on a.empID=b.empID where 1";
	} 
	// 搜尋處理
	if ($keyword!="") {
		$sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";
		$outsqly.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";
	}
	
	// 排序處理
	if ($fieldname=="") $sql.=" order by $defaultOrder,$defaultOrder1,$defaultOrder2 $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	// echo $fieldname.'</br>';
	//分頁
	$sql .= " limit ".$pageSize*$pages.",$pageSize";
	// echo $sql;
	
	$rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) {$id = $r[$fnAry[0]];
		$dKey = date('ymd',strtotime($r['startTime']));
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
    <!-- <a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a> -->
			<?=$id?>
    </td>

		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"" || $ftyAy[$i]=='history') switch ($ftyAy[$i]) {
					case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
					case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
					case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
					case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
					case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
					case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
					case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
					case "time" : echo date('H:i',strtotime($fldValue)); break;
					case "history":	echo '<input type="button" value="'.date('m/d',strtotime($r['d_datetime'])).'紀錄" onClick="history('.$r['id'].')">'; break;
					default : 
						if($fnAry[$i]=='empID'){
							echo $fldValue.'-'.$emplyeeinfo[$fldValue];
						}elseif($fnAry[$i]=='dType'){
							echo $dType[$fldValue];
						}elseif($fnAry[$i]=='depID'){
							echo $departmentinfo[$fldValue];
						}else{
							echo $fldValue;		
						}
						break;
				} elseif($fnAry[$i]=='startTime'&& $ftyAy[$i]=='date') echo date('Y/m/d',strtotime($r['startTime2'])); else echo "&nbsp";
			?>
			</td>
		<? } ?>
    
    <td><?=$outData[$r['empID']][$dKey][0]?date('H:i',strtotime($outData[$r['empID']][$dKey][0])):''?></td>
    <td>
    <?
    	$sDate = date('Y-m-d', strtotime($r['startTime']));
    	if($outofficeList[$sDate][$r['empID']][0]) {
				echo '直接下班';
				echo '<br>('.date('H:i',strtotime($outofficeList[$sDate][$r['empID']][1])).')';
    	} else {
    		echo $outData[$r['empID']][$dKey][1]?date('H:i',strtotime($outData[$r['empID']][$dKey][1])):'';
    	}
    ?>
    </td>
    
    <td><? 
			$rzt = absDataCK($r['empID'],$r['startTime']);
			if($rzt) {
				echo $rzt[0].'<br>'.date('m/d H:i',strtotime($rzt[1])).' - '.date('m/d H:i',strtotime($rzt[2]));
			} else echo "&nbsp;";
		?></td>
  </tr>    
<? } ?>   
 </table>
</form>
<div align="right"><a href="#top">Top ↑</a></div>

<form name="excelfm1" action="excel.php" method="post" target="excel">
	<input type="hidden" name="excel_type" value="duty">
	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
	<input type="hidden" name="start_day" value="<?=$bDate?>">
	<input type="hidden" name="end_day" value="<?=$eDate?>">
	<input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
</form>

</body>
</Html>