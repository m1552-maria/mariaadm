<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
		
	$bE = false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "員工請假資料";
	$tableName = "absence A,emplyee B";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/absence/';
	$delField = 'ahead';
	$delFlag = false;
	$imgpath = '../../data/absence/';
	
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "depID=''";
	}
	if($_REQUEST['aType']) $temp[] = "aType='$_REQUEST[aType]'";

	$is_leadmen = -1;

	if(isset($_REQUEST['is_leadmen']) && $_REQUEST['is_leadmen'] != '') {
		$temp[] = " is_leadmen = '".(int)$_REQUEST['is_leadmen']."'";
		$is_leadmen = (int)$_REQUEST['is_leadmen'];
	}

	$isLeadmenList = array(
		'' => '審核狀態',
		'0' => '主管未審核',
		'1' => '主管已審核'
	);

	$nowYr = date('Y');
	$curYr = $_REQUEST['year']?$_REQUEST['year']:$curYr=$nowYr;
	if($_REQUEST['month']) {
		$curMh = $_REQUEST['month'];
		$Days  = date('t',strtotime("$curYr/$curMh/01"));
		$bDate = date($curYr.'/'.$curMh.'/01');
		$eDate = date($curYr.'/'.$curMh.'/'.$Days);	
	} else {
		$curMh = date('m');
		$bDate = date('Y/m/01');
		$eDate = date('Y/m/t');
	}

	$afterClose = (!empty($_REQUEST['afterClose'])) ? 1 : 0;
	if($afterClose == 1) {
		$sql = "select rDate from acctclose where title='salary' and conditions='".$curYr.str_pad($curMh,2,"0",STR_PAD_LEFT)."' and allClose = '1'";
		$rs = db_query($sql,$conn);
		$r = db_fetch_array($rs);
		$temp[] = (!empty($r['rDate'])) ? "A.rdate > '".$r['rDate']."'" : "A.rdate = ''";
	}

	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "B.empName";
	$searchField2 = "A.empID";
	$searchField3 = "A.id";
	$pageSize = 10;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,empID,aType,rdate,bDate,eDate,hours,content,file,is_leadmen,rspContent,isOK,isDel,logs,tuning,mapping");
	$ftAry = explode(',',"單號,請假員工,假別,請假日期,起始日期,結束日期,時數,事由,附件,主管簽名,辦理情形,准假,銷假,調整狀態,調整,沖銷明細");
	$flAry = explode(',',"70,64,,,,,,,,,,,,,,");
	$ftyAy = explode(',',"ID,empID,text,date,datetime,datetime,text,text,text,bool,text,bool,bool,text,text,text");
?>