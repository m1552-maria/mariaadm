<?php

	function insertTransferAnnual($data = array()) {
		global $db;
		deleteAnnual($data['wkYear']);
		$insertData = array(
			"`type`='特休代金'",
			"`pID`='".$data['wkYear']."'",
			"`year`='".$data['wkYear']."'",
			"`title`='".$data['wkYear']."特休代金'"
		);
		$sql = "insert into transfer set ".implode(',', $insertData);
		db_query($sql);
		$tID = mysql_insert_id();//新增的id 舊版
		//$tID = $db->lastInsertId(); // 使用POD php 7以後 
		$payType = array('1'=>'轉入帳戶','2'=>'領現','3'=>'匯款');

		foreach ($data['emplyee_annual'] as $key => $value) {
			$money = $value['money']+$value['lastMoney'];
			if($money <= 0) continue;
			$fields = array();
			$fields[] = "`empID`='".$value['empID']."'";
			$fields[] = "`tID`='".$tID."'";
			$fields[] = "`year`='".$data['wkYear']."'";
			$fields[] = "`title`='".$data['wkYear']."特休代金'";
			$fields[] = "`identifier`='1'";
			$fields[] = "`class`='A2'";
			$fields[] = "`loan`='C'";
			$fields[] = "`inAccount`='".$value['salaryAct']."'";
			$fields[] = "`money`='".$money."'";
			$fields[] = "`payType`='".$payType[$value['payType']]."'";
			$sql = "insert into emplyee_transfer set ".implode(',', $fields);
			db_query($sql);
		}

	}
	
	function deleteAnnual($year) {
		$sql = "select id from transfer where type='特休代金' and pID='".$year."'";
		$rs = db_query($sql);
		$r = db_fetch_array($rs);
		if(!empty($r['id'])) {
			$tID = $r['id'];
			$sql = "delete from emplyee_transfer where tID='".$tID."'";
			db_query($sql);
			$sql = "delete from transfer where id='".$tID."'";
			db_query($sql);
		}
	}

	function updateAnnual($year, $isClose) {
		$sql = "update emplyee_annual set isClose='".$isClose."' where `year`='".$year."'";
		db_query($sql);
	}
 
	function computeAnnual($year, $data=array()) {
		global $nowY,$nowM,$days_in_month,$base_days;
		$result = array('result'=>1, 'msg'=>'', 'emplyee_annual'=>array());
		/*20171218 sean 增加判別選擇的年度是否已經出現在試算表(emplyee_annual)*/
		$sql = "select 1 from emplyee_annual where year = '".$year."'";
	    $rs = db_query($sql);
	    if(db_num_rows($rs) == 0) {
	    	$result['result'] = 0;
	    	$result['msg'] = $year.'特休尚未試算, 請先到 年資/特休 進行試算';
	     	return $result;
	    }

	    //處理請假
	    $emp_abs = array();
	    $sql = "select empID,SUM(hours) AS abhours from absence where isOK=1 and isDel=0 and aType='特休' and SUBSTR(bDate,1,4)='".$year."' group by empID order by empID";
	    $rs = db_query($sql);
	    while($rs && $r=db_fetch_array($rs)) {
	        $emp_abs[$r['empID']] = $r['abhours'];
	    }


	    //處理特休
    	$emplyee_annual = array();
   		$sql = "select e.empID,ea.hours as spHolidays, ea.lastHours, ea.isDelay ,e.empName,e.depID,e.jobClass,e.hireDate,e.projID,e.salaryAct,e.payType, (select isDelay from emplyee_annual le where le.empID = e.empID and le.year='".($year-1)."') as lastIsDeay 
   		from emplyee as e inner join emplyee_annual as ea on e.empID = ea.empID where ea.year='".$year."'";
   		if(isset($data['isOnduty'])) {
   			$sql .= " and e.isOnduty in (".$data['isOnduty'].")";
   		}

   		if(!empty($data['projID'])) {
   			$pList = explode(',', $data['projID']);
		    $aa = array();
		    foreach ($pList as $key => $v) {
		       if($v)  $aa[]="e.projID like '$v%'";  
		    }
		    $sql .= " and (".join(' or ',$aa).")";   
   		}
   		if(!empty($data['isPrivilege'])) {
   			if($_SESSION['user_classdef'][7]) {
		        $aa = array();
		        foreach($_SESSION['user_classdef'][7] as $v){
		        	if($v)  $aa[]="e.projID like '$v%'";  
		        }
		        $sql .= " and (".join(' or ',$aa).")";   
		    } else {
		        $sql .=  " and e.projID='-1'";
		    }
   		}
   		/*if(!empty($data['empID'])) {
   			$sql .= " and e.empID='".$data['empID']."'";   
   		}*/
   		//增加部門&員編判別
   		$_where = array();
	    if(!empty($data['depID'])) {
	      $depID = explode(',', $data['depID']);
	      $depIDList = array();
	      foreach ($depID as $key => $value) {
	        $depIDList[] = "e.depID like '$value%'";
	      }
	     $_where[] = implode(' or ', $depIDList);
	    }

	    if(!empty($data['empID'])) {
	      $empID = explode(',', $data['empID']);
	      $empIDList = array();
	      foreach ($empID as $key => $value) {
	        $empIDList[] = "'".$value."'";
	      }
	     $_where[] = "e.empID in (".implode(',', $empIDList).")";
	    }

	    if(count($_where) > 0) {
	      $sql.= " and (".implode(' or ', $_where).")";
	    }

   		$sql.= " order by e.projID,e.depID,e.empID";
    	$rs = db_query($sql);

    	if(!db_eof($rs)){
	        include_once('../emplyee_salary_report/api.php');
	        while($r=db_fetch_array($rs)) {
	        	$emplyee_annual[$r['empID']] = $r;

	          	if(isset($emp_abs[$r['empID']])){
	            	$emplyee_annual[$r['empID']]['abhours'] = $emp_abs[$r['empID']];
	          	}else{
	            	$emplyee_annual[$r['empID']]['abhours'] = 0;
	          	}
	          
	          	$emplyee_annual[$r['empID']]['lastWage'] = 0;
	          	$emplyee_annual[$r['empID']]['money'] = 0; //計算年應付代金
		        $emplyee_annual[$r['empID']]['lastMoney'] = 0; //前年應付代金
	          	/*當年延修就試算特休薪資就規0*/
	          	if($r['isDelay'] == 1) {
	            	$emplyee_annual[$r['empID']]['wage'] = 0;
	            	continue;
	          	}

		        /* 引用 /emplyee_salary_report/api.php 的basic來計算出時薪水多少
		          $nowY~ $leaveD 為basic需要的變數
		          需要用全薪去計算出時薪 所以basic 的 $hireD 統一用1/1日 $leaveD不需要給
		           */
		        $nowY = $year;
		        $nowM = 12;
		        $days_in_month = 31;
		        $base_days = 30;
		        $hireD = $year.'-01-01'; //
		        $leaveD = '';

		        //$wage=round(basic($r[empID]));
		        $basicData = basic($hireD,$leaveD,$r['empID']);
		        $wage = (isset($basicData[3])) ? $basicData[3] : 0;
		          
		        $emplyee_annual[$r['empID']]['wage']=$wage;

		        /*新增 判斷是否有去年的剩餘時數 未休完*/
		        if($emplyee_annual[$r['empID']]['lastIsDeay'] == 1 && $emplyee_annual[$r['empID']]['abhours'] < $emplyee_annual[$r['empID']]['lastHours']) {
		        	$nowY = $year-1;
		        	$hireD = ($year-1).'-01-01'; //
		        	$basicData = basic($hireD,$leaveD,$r['empID']);
		        	$emplyee_annual[$r['empID']]['lastWage'] = (isset($basicData[3])) ? $basicData[3] : 0;
		        }

		        if($emplyee_annual[$r['empID']]['lastWage'] > 0) { // 前年有延休 剩餘時數也未休完
		        	$emplyee_annual[$r['empID']]['lastMoney'] = round(($r['lastHours'] - $emplyee_annual[$r['empID']]['abhours']) * $emplyee_annual[$r['empID']]['lastWage']);
		        	$emplyee_annual[$r['empID']]['money'] =  round(($r['spHolidays'] - $r['lastHours']) * $emplyee_annual[$r['empID']]['wage']);
		        } else {
		        	$emplyee_annual[$r['empID']]['money'] = round(($r['spHolidays'] - $emplyee_annual[$r['empID']]['abhours']) * $emplyee_annual[$r['empID']]['wage']);
		        }

	        }
	    }
	    $result['emplyee_annual'] = $emplyee_annual;

	    return $result;
	}

?>