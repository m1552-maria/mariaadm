<?php
	@session_start();
	include '../inc_vars.php';
	include '../../config.php';
	include('../../getEmplyeeInfo.php');	

	$id = $_REQUEST[id];
	$rs = db_query("select * from absence where id=$id");
	if($rs) $r=db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Details</title>
  <link href="../../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
  <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<script src="../../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
  <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  <script>
	function formvalid(tfm) {
		tfm.params.value = window.parent.topFrame.location.search;
		return true;
	}
	</script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	background-color:#f8f8f8;
}
.td-heavy{	background:#dedede; text-align:right;}
</style>
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="4" cellspacing="1" class="formTx">
        <tr>
          <td class='td-heavy'>請假人員：</td>
          <td><?=$emplyeeinfo[$r[empID]]?></td>
          <td class='td-heavy'>代理人：</td>
          <td><?
          	$ct=0; foreach(split(',',$r[agent]) as $v) {
							echo $emplyeeinfo[$v];
							//echo "<img src='uncheck.gif'><br>"; 
							$fn = (pow(2,$ct)&$r['is_agent']) ? 'checked.gif':'uncheck.gif';
							echo "<img src='$fn' align='absmiddle'>";
							echo "<br>"; 
							$ct++;
						} 
						
					?></td>
          <td class='td-heavy'>主管：</td>
          <td><?=$emplyeeinfo[$r[leadmen]]?> <img src="<?=$r[is_leadmen]?'checked.gif':'uncheck.gif'?>" align="absmiddle"> <? if($r[is_leadmen]) echo date('Y/m/d H:i',strtotime($r['is_leadmen_date'])) ?></td>
          <td class='td-heavy'>假別：</td>
          <td><?=$r[aType]?></td>
        </tr>
        <tr>
          <td class='td-heavy'>請假起始時間：</td>
          <td><?= $r['bDate']?date('Y/m/d H:i',strtotime($r['bDate'])):''?></td>
          <td class='td-heavy'>請假結束時間：</td>
          <td><?= $r['eDate']?date('Y/m/d H:i',strtotime($r['eDate'])):''?></td>
          <td class='td-heavy'>請假日期：</td>
          <td><?= $r['rdate']?date('Y/m/d H:i',strtotime($r['rdate'])):''?></td>
          <td class='td-heavy'>請假時數：</td>
          <td><?=$r[hours]?></td>
        </tr>   
        <tr valign="top">
          <td class='td-heavy'>准假：</td>
          <td><img src="<?=$r[isOK]?'checked.gif':'uncheck.gif'?>" align="absmiddle"></td>
          <td class='td-heavy'>申請銷假：</td>
          <td><img src="<?=$r[isReqD]?'checked.gif':'uncheck.gif'?>" align="absmiddle"></td>
          <td class='td-heavy'>銷假：</td>
          <td colspan="3"><img src="<?=$r[isDel]?'checked.gif':'uncheck.gif'?>" align="absmiddle"></td>
        </tr>   
        <tr>
          <td class='td-heavy'>事由 / 說明：</td>
          <td colspan="7"><?= nl2br($r[content])?></td>
          </tr>
        <tr>
          <td class='td-heavy'>交辦事項：</td>
          <td colspan="7"><?= nl2br($r[notes])?></td>
        </tr>
        <tr>
          <td class='td-heavy'>辦理情形：</td>
          <td colspan="7"><?=$r[rspContent]?></td>
        </tr> 
         <tr>
          <td class='td-heavy'>調整記錄：</td>
          <td colspan="7"><?=$r[logs]?></td>
        </tr>            
      </table>
    </td>
  </tr>
  
</table>
</body>
</html>