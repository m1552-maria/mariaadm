<?php
	session_start();
	include("../../config.php");
	include('../inc_vars.php');	
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<form name="excelfm2" action="exportExcel.php" method="post" target="excel">
<div>
	清冊類別:
	<select name="printType" id="printType">
		<option value="excel"> EXCEL </option>
		<option value="pdf"> PDF </option>
	</select>
</div>
<br>
<div>
	職況:
	<?php 
		foreach($jobStatus as $key => $val) { 
			$checked = ($key == 1) ? 'checked' : '';
	?>

		<label><input type="checkbox" name="isOnduty[]" value="<?php echo $key?>" <?php echo $checked;?>><?php echo $val;?></label>
	<?php } ?>
</div>
<br>
計畫編號:
<?php if($_SESSION['privilege'] > 10) { ?>
	<input type="text" name="projIDFilter" id="projIDFilter" value="">
<?php }else{?>
	<div>
		<label><input type="checkbox"  value="全選" onclick="allCheck(this)">全選</label>
		<?php 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo '<label><input type="checkbox" name="projIDFilter" value="'.$v.'">'.$v.'</label>';
	    ?>
	</div>


	<!--<select name="projIDFilter" id="projIDFilter" ><option value=''>-全部計畫-</option>
		<? 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo "<option value='$v'".($_REQUEST[projIDFilter]==$v?' selected ':'').">$v</option>"; 
	    ?>
	    </select>-->
<?php } ?>

<br><br>
<div>選取部門及員工:</div>

<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr valign="top">
  			<td>
  				<ul class='dep-emp-ul'>
					<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
					<li class='dep-emp-li'>
						<div class='query-title'>部門</div>
						<div class="depID_list">NO DATA</div>
					</li>
					<li class='dep-emp-li'>
						<div class='query-title'>員工</div>
						<div class="empID_list">NO DATA</div>
					</li>
				</ul>
  			</td>
  		</tr>
</table>

<input type="button" onclick="excelfm2Smi()" value="確定">
<script src="../../Scripts/jquery-1.12.3.min.js" type="text/javascript"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script type="text/javascript">
	function excelfm2Smi() {
<?php if($_SESSION['privilege'] > 10) { ?>
		window.opener.annual.projID.value=$('#projIDFilter').val();
<?php } else { ?> 
		var projID = [];
		$('[name="projIDFilter"]').each(function(){
			if(this.checked) projID.push($(this).val());
		});
		window.opener.annual.projID.value=projID.join(',');
<?php } ?>
		window.opener.annual.printType.value=$('#printType').val();


		var depID = [];
		var empID = [];
		$('[name="depID[]"]').each(function(){
			depID.push($(this).val());
		});
		depID = (depID.length>0) ? depID.join(',') : '';

		$('[name="empID[]"]').each(function(){
			empID.push($(this).val());
		});
		empID = (empID.length>0) ? empID.join(',') : '';

		window.opener.annual.depID.value = depID;
		window.opener.annual.empID.value = empID;

		var isOnduty = [];
		$('[name="isOnduty[]"]:checked').each(function(){
			isOnduty.push($(this).val());
		});
		isOnduty = (isOnduty.length>0) ? isOnduty.join(',') : '';
		
		window.opener.annual.isOnduty.value = isOnduty;

		window.opener.annual.submit();
		window.close();
	}

	function allCheck(obj) {
		var pList = document.getElementsByName('projIDFilter');
		for(var i=0;i<pList.length;i++) {
			pList[i].checked = obj.checked;
		} 
	}
</script>
</form>
</body>
</html>