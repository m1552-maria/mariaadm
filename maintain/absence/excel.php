<?php
  session_start();
  include_once('../../config.php');
  include_once('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');
  include_once('annualFunc.php');  
  include_once('getYears.php');  
  if(isset($_REQUEST['sqltx'])) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");
    header("Pragma:no-cache");
    header("Expires:0");

    
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);


    $sql = $_REQUEST['sqltx'];
    $sql .= " order by depID,empID,aType";
    $rs = db_query($sql);

    $sheet = $objPHPExcel->getActiveSheet();

    if($_REQUEST['fmTitle']=='員工請假 年報表' || $_REQUEST['fmTitle']=='員工請假 月報表') {
          //設定字型大小
          $sheet->getStyle('A1')->getFont()->setSize(18);
          
          //設定欄位背景色(方法1)
          $sheet->getStyle('A4:N4')->applyFromArray(
              array(
                  'fill' => array(
                      'type' => PHPExcel_Style_Fill::FILL_SOLID,
                      'color' => array('rgb' => 'C0C0C0')
                  )
              )
          );
          
          //合併儲存隔 
          $sheet->mergeCells("A1:N1");
          $sheet->mergeCells("A2:B2");
          $sheet->mergeCells("A3:B3");
          $sheet->mergeCells("C2:N2");
          $sheet->mergeCells("C3:N3");
          //對齊方式
          $sheet->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          
          $sheet->getStyle('A1:N1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          $sheet->getStyle('A2:N2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          $sheet->getStyle('A3:N3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          $sheet->getStyle('A4:N4')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          /*
          VERTICAL_CENTER 垂直置中
          VERTICAL_TOP
          HORIZONTAL_CENTER
          HORIZONTAL_RIGHT
          HORIZONTAL_LEFT
          HORIZONTAL_JUSTIFY
          */
          $sheet->setCellValue('A1',$_REQUEST['fmTitle'].' - '.(!empty($_REQUEST['depID'])?$departmentinfo[$_REQUEST['depID']]:'全部部門'));
          $sheet->setCellValue('A2','製作日期');
          $sheet->setCellValue('C2',date("Y/m/d"));
          $sheet->setCellValue('A3','報表日期');

          $reg = "((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))";
          preg_match($reg, $sql, $match);
          $aa = explode('/',$match[0]);
          if($_REQUEST['fmTitle']=='員工請假 年報表') {
            $sheet->setCellValue('C3',$aa[0].' 全年');
          }elseif($_REQUEST['fmTitle']=='員工請假 月報表'){
            $sheet->setCellValue('C3',$aa[0].' 年'.$aa[1].' 月');
          } 

          //跑header 迴圈用
          $headerstart = 4;
          $header = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N');
          $headervalue = array('流水號','員工編號','員工名稱','所屬部門','請假時間','起始日期','結束日期','請假時數','假別','事由','辦理情形','已准假','已申請銷假','已銷假');
          
          foreach($header as $k=>$v){
            $sheet->setCellValue($v.$headerstart,$headervalue[$k]);   
          }

       
          $i = 5;
          $curEmp = '';
          $curTyp = '';
          $tHours=0;
          $count  = 1;
          $color = 0;
          
          while($rs && $r=db_fetch_array($rs)) {
            $eid = $r['empID'];

            if($curEmp!=$eid) {
              $curEmp = $eid;
              $curTyp = '';
              $color=!$color;
            }

            if($curTyp!=$r['aType']) {
              $curTyp = $r['aType'];
              if($count!=1){

                $sheet->setCellValueExplicit('A'.$i,$count);
                $sheet->mergeCells('A'.$i.':G'.$i);
                $sheet->setCellValue('A'.$i,'時數合計：');
                $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $sheet->setCellValueExplicit('H'.$i,$tHours);
                $sheet->mergeCells('I'.$i.':N'.$i);
                $sheet->getStyle('A'.$i.':N'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $i++;
              }
              $tHours = 0;    
            } 
            if(!$r['isDel'] && $r['isOK']) $tHours += floatval($r['hours']);
            if($color != 0){
              $sheet->getStyle('A'.$i.':N'.$i)->applyFromArray(
                  array(
                      'fill' => array(
                          'type' => PHPExcel_Style_Fill::FILL_SOLID,
                          'color' => array('rgb' => 'efefef')
                      )
                  )
              );
            }


            $content = array($count,$eid,$emplyeeinfo[$eid],$departmentinfo[$r['depID']],$r['rdate'],$r['bDate'],$r['eDate'],$r['hours'],$r['aType'],$r['content'],$r['rspContent'],(!empty($r['isOK']))?'v':'',(!empty($r['isReqD']))?'v':'',(!empty($r['isDel']))?'v':'');
            foreach($header as $k=>$v){
              if($v == 'B'){
                  //設定格式用
                  $sheet->setCellValueExplicit($v.$i, $content[$k], PHPExcel_Cell_DataType::TYPE_STRING);
              }else{
                  $sheet->setCellValueExplicit($v.$i,$content[$k]);    
              }
            }

          
            $sheet->getStyle('A'.$i.':N'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $i++;
            $count++;
          }
         
          //生成最後一筆
          $sheet->setCellValueExplicit('A'.$i,$count);
          $sheet->mergeCells('A'.$i.':G'.$i);
          $sheet->setCellValue('A'.$i,'時數合計：');
          $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
          $sheet->setCellValueExplicit('H'.$i,$tHours);
          $sheet->mergeCells('I'.$i.':N'.$i);
          $sheet->getStyle('A'.$i.':N'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          
         
           
          //設定欄寬
          $width = array(20,20,20,20,20,20,20,20,20,20,20,20,20,20);
          foreach($header as $k=>$v){
            $sheet->getColumnDimension($v)->setWidth($width[$k]);  
          }


          
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
          $objWriter->setPreCalculateFormulas(false);
          
          $objWriter->save('php://output');
    }elseif($_REQUEST['fmTitle']=='員工請假 年合計表'){

          //設定字型大小
          $sheet->getStyle('A1')->getFont()->setSize(18);
          
          //設定欄位背景色(方法1)
          $sheet->getStyle('A4:F4')->applyFromArray(
              array(
                  'fill' => array(
                      'type' => PHPExcel_Style_Fill::FILL_SOLID,
                      'color' => array('rgb' => 'C0C0C0')
                  )
              )
          );
          
          //合併儲存隔 
          $sheet->mergeCells("A1:F1");
          $sheet->mergeCells("A2:B2");
          $sheet->mergeCells("A3:B3");
          $sheet->mergeCells("C2:F2");
          $sheet->mergeCells("C3:F3");
          //對齊方式
          $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          
          $sheet->getStyle('A1:F1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          $sheet->getStyle('A2:F2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          $sheet->getStyle('A3:F3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          $sheet->getStyle('A4:F4')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
          /*
          VERTICAL_CENTER 垂直置中
          VERTICAL_TOP
          HORIZONTAL_CENTER
          HORIZONTAL_RIGHT
          HORIZONTAL_LEFT
          HORIZONTAL_JUSTIFY
          */
          $sheet->setCellValue('A1',$_REQUEST['fmTitle'].' - '.($_REQUEST['depID']?$departmentinfo[$_REQUEST['depID']]:'全部部門'));
          $sheet->setCellValue('A2','製作日期');
          $sheet->setCellValue('C2',date("Y/m/d"));
          $sheet->setCellValue('A3','報表日期');

          $reg = "((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))";
          preg_match($reg, $sql, $match);
          $aa = explode('/',$match[0]);
          $sheet->setCellValue('C3',$aa[0].' 全年合計表');
          

          //跑header 迴圈用
          $headerstart = 4;
          $header = array('A','B','C','D','E','F');
          $headervalue = array('流水號','員工編號','員工名稱','所屬部門','假別','請假時數');
          
          foreach($header as $k=>$v){
            $sheet->setCellValue($v.$headerstart,$headervalue[$k]);   
          }


          $result = array();
          while($rs && $r=db_fetch_array($rs)) {
            //組成我要的陣列//要排除已經銷單的和未准假

            if(!$r['isDel'] && $r['isOK']){
              if(isset($result[$r['empID']][$r['aType']])){
                $result[$r['empID']][$r['aType']]['hours'] = $result[$r['empID']][$r['aType']]['hours']+$r['hours'];
              }else{
                $result[$r['empID']][$r['aType']] = array('empID'=>$r['empID'],'empName'=>$r['empName'],'aType'=>$r['aType'],'hours'=>$r['hours'],'depID'=>$r['depID']);    
              }
            }
          }

          $i = 5;
          $count = 1;
          foreach($result as $k1=>$v1){
            
            foreach($result[$k1] as $k2=>$v2){
              $content = array($count,$k1,$emplyeeinfo[$k1],$departmentinfo[$v2['depID']],$v2['aType'],$v2['hours']);
              foreach($header as $k=>$v){
                if($v == 'B'){
                    //設定格式用
                    $sheet->setCellValueExplicit($v.$i, $content[$k], PHPExcel_Cell_DataType::TYPE_STRING);
                }else{
                    $sheet->setCellValueExplicit($v.$i,$content[$k]);    
                }
              }

              
            
              $sheet->getStyle('A'.$i.':F'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
              $count++;
              $i++;
            }

            
          }
          

          //設定欄寬
          $width = array(20,20,20,20,20,20,20,20,20,20,20,20,20,20);
          foreach($header as $k=>$v){
            $sheet->getColumnDimension($v)->setWidth($width[$k]);  
          }


          
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
          $objWriter->setPreCalculateFormulas(false);
          
          $objWriter->save('php://output');

    }

  }

  //特休
  if($_REQUEST['fmTitle']=='員工特休時數管理'){
    /*20171218 sean 增加判別選擇的年度是否已經出現在試算表(emplyee_annual)*/
    $cData = array();
    if($_REQUEST['projID']) $cData['projID'] = $_REQUEST['projID'];
    elseif ($_SESSION['privilege'] <= 10) $cData['isPrivilege'] = 1;
    if($_REQUEST['empID'] != '')  $cData['empID'] = $_REQUEST['empID'];
    if($_REQUEST['depID'] != '')  $cData['depID'] = $_REQUEST['depID'];
    $cData['isOnduty'] = ($_REQUEST['isOnduty'] == '') ? -1 : $_REQUEST['isOnduty'];
    $cAnnyualData = computeAnnual($_REQUEST['year'], $cData);
    if($cAnnyualData['result'] == 0) {
      echo $cAnnyualData['msg'];
      exit;
    }
    $emplyee_annual = $cAnnyualData['emplyee_annual'];
    $years = getYears($emplyee_annual);

    $headervalue = array('流水號','員工編號','員工名稱','所屬部門','到職日','工時身分','總年資',$_REQUEST['year'].'特休時數',($_REQUEST['year']-1).'延休時數',$_REQUEST['year'].'特休假總時數','已申請特休時數','剩餘特休時數','未休工資('.$_REQUEST['year'].'年)','未休工資('.($_REQUEST['year']-1).'延休)',$_REQUEST['year'].'未休特休工資總額','申請延休');
    if($_REQUEST['depID']!=''){
      $dep = $departmentinfo[$_REQUEST['depID']];
    }else{
      $dep = '';
    }

    if($_REQUEST['printType'] == 'excel') {

      $filename = date("Y-m-d").'.xlsx';//ie中文檔名亂碼
      header("Content-type:application/vnd.ms-excel");
      header("Content-Type:text/html; charset=utf-8");
      header('Content-Disposition:attachment;filename='.iconv('utf-8', 'big5', urlencode('員工特休假未休代金清冊').$filename));//中文檔名亂碼的問題
      header("Pragma:no-cache");
      header("Expires:0");

      $objPHPExcel = new PHPExcel(); 
      $objPHPExcel->setActiveSheetIndex(0);
      $sheet = $objPHPExcel->getActiveSheet();
      // 設定欄位背景色(方法1)
      $sheet->getStyle('A5:P5')->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'C0C0C0')
              )
          )
      );
      //合併儲存隔 
      $sheet->mergeCells("A1:P1");
      $sheet->mergeCells("B2:P2");
      $sheet->mergeCells("B3:P3");
      $sheet->mergeCells("B4:P4");
      //對齊方式
      $sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->getStyle('A5:J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      


      $sheet->setCellValue('A1','員工特休假未休代金清冊');
      $sheet->getStyle('A1')->getFont()->setSize(18);

      $sheet->setCellValue('A2','製作日期');
      $sheet->setCellValue('A3','製表日期');
      $sheet->setCellValue('A4','部門名稱');
      $sheet->setCellValue('B2',date('Y/m/d'));
      $sheet->setCellValue('B3',$_REQUEST['year'].'全年合計表');
      $sheet->setCellValue('B4',$dep);



      //跑header 迴圈用
      $headerstart = 5;
      $header = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P');
      foreach($header as $k=>$v){
        $sheet->setCellValue($v.$headerstart,$headervalue[$k]);   
      }
      $sheet->getStyle('A1:P5')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

      $i = 6;
      $total = 1;
      foreach($emplyee_annual as $k=>$v){
        $sheet->setCellValueExplicit('A'.$i,$total);
        $sheet->setCellValueExplicit('B'.$i, $v['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
        $sheet->setCellValue('C'.$i,$v['empName']);
        $sheet->setCellValue('D'.$i,$departmentinfo[$v['depID']]);
        $sheet->setCellValue('E'.$i,$v['hireDate']);
        $sheet->setCellValueExplicit('F'.$i,$v['jobClass']);
        $sheet->setCellValueExplicit('G'.$i,$years[$v['empID']]);

        $nowHours = ($v['lastIsDeay'] == 1) ? $v['spHolidays']-$v['lastHours'] : $v['spHolidays'];
        $sheet->setCellValueExplicit('H'.$i, $nowHours);
        $sheet->setCellValueExplicit('I'.$i, (($v['lastIsDeay'] == 1) ? $v['lastHours'] : 0));
        $sheet->setCellValueExplicit('J'.$i,$v['spHolidays']);
        $sheet->setCellValueExplicit('K'.$i,$v['abhours']);

        $remainHour = $v['spHolidays']-$v['abhours'];
        $sheet->setCellValueExplicit('L'.$i,$remainHour);

        $sheet->setCellValueExplicit('M'.$i,$v['money']);
        $sheet->setCellValueExplicit('N'.$i,$v['lastMoney']);


        $sheet->setCellValueExplicit('O'.$i,($v['money']+$v['lastMoney']));
        $sheet->setCellValue('P'.$i,($v['isDelay']) ? 'V' : '');


        $sheet->getStyle('A'.$i.':P'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
        $total++;

      }
     
     
      //設定欄寬
      $width = array(10,10,10,20,20,20,20,20,20,20,20,20,20,20,20,10);
      foreach($header as $k=>$v){
        $sheet->getColumnDimension($v)->setWidth($width[$k]);  
      }

      
      
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
      $objWriter->setPreCalculateFormulas(false);
      
      $objWriter->save('php://output');

    } elseif($_REQUEST['printType'] == 'pdf') {

      $list = array();

      
      foreach ($emplyee_annual as $key => $value) {
        if($value['isDelay'] == 1 || ($value['spHolidays']-$value['abhours']) == 0) continue;
        $list[$value['projID']][$value['depID']][$key] = $value;
      }

      $headervalue = array('序號','員工編號','姓名','到職日','工時身分','未休特休代金',);
      include_once '../../mpdf/mpdf.php';
      $mpdf = new mPDF('UTF-8','A4-L','','DFKai-sb',10,10,10,10);
      $mpdf->useAdobeCJK = true;
      $mpdf->SetAutoFont(AUTOFONT_ALL);
      $mpdf->SetDisplayMode('fullpage');
      $html = file_get_contents('pdfHtml.html');

      $hCount = count($headervalue);
      $br = '<tr class="trBr"><td colspan="'.$hCount.'"><br></td></tr>';
      $totalList = array();
     
      $totalList['empName'] = array('total'=>0,'allTotal' =>0);
      $totalList['payMonry'] = array('total'=>0,'allTotal' =>0);

      $_html = array();
      $_html[] = '<h2 style="text-align: center;">財團法人瑪利亞社會福利基金會</h2>';
      $_html[] = '<h3 style="text-align: center;">'.($_REQUEST['year']-1911).'員工特休假未休代金清冊</h3>';
      $_html[] = '<table class="tableData" width="1250px;" border="1">';

      foreach ($list as $k => $v) {
        $totalList['empName']['total'] = 0;
        $totalList['payMonry']['total'] = 0;
        $i = 0;
        foreach ($v as $k2 => $v2) {
          $i++;
          if($i>1) $_html[] = $br;
          $_html[] = '<tr><td colspan="3">計畫編號:'.$k.'</td><td colspan="'.($hCount-3).'">部門:'.$departmentinfo[$k2].'</td></tr>';
          $_html[] = '<tr>';
          foreach ($headervalue as $key => $value) {
            $_html[] = '<td class="cLabel">'.$value.'</td>';
          }
          $_html[] = '</tr>';

          foreach ($v2 as $k3 => $v3) {
            $totalList['empName']['total']++;
            $totalList['empName']['allTotal']++;

            $payMonry = $v3['money']+$v3['lastMoney'];
            if($payMonry <= 0) continue;

            $totalList['payMonry']['total'] += $payMonry;
            $totalList['payMonry']['allTotal'] += $payMonry;

            $_html[] = '<tr>';
            $_html[] = '<td class="tdRight">'.$totalList['empName']['total'].'</td>';
            $_html[] = '<td>'.$v3['empID'].'</td>';
            $_html[] = '<td>'.$v3['empName'].'</td>';
            $_html[] = '<td>'.$v3['hireDate'].'</td>';
            $_html[] = '<td>'.$v3['jobClass'].'</td>';
            $_html[] = '<td class="tdRight">'.number_format($payMonry).'</td>';
            $_html[] = '</tr>';
          }
        }
        $_html[] = '<tr><td></td><td>小計</td><td>'.$totalList['empName']['total'].'人</td><td></td><td></td><td class="tdRight">'.number_format($totalList['payMonry']['total']).'</td></tr>';
        $_html[] = $br;
      }
      $_html[] = $br;
      $_html[] = '<tr><td></td><td>總計</td><td>'.$totalList['empName']['allTotal'].'人</td><td></td><td></td><td class="tdRight">'.number_format($totalList['payMonry']['allTotal']).'</td></tr>';
      $_html[] = $br;
      $_html[] = '<tr><td></td><td colspan="2">經辦:</td><td colspan="3">機構/中心主管:</td></tr>';
      $_html[] = '<tr><td></td><td colspan="2">覆核經辦:</td><td colspan="2">覆核主管:</td><td>執行長:</td></tr>';
      $_html[] = '</table>';

      $html = str_replace('{#html#}', implode('', $_html), $html);
      $mpdf->SetWatermarkText('MARIA');
      $mpdf->showWatermarkText = true;
    //  $mpdf->SetHTMLFooter('<p>test</p>');
      $mpdf->WriteHTML($html);
      $mpdf->Output();

    }

  }


?>