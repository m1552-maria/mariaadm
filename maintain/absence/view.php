<?php
/*setAcctClose('annual','2021', 'sa',1);
setAcctClose('annual','2022', 'sa',1);*/
	if($_SESSION['privilege'] > 10){
		if(isset($_POST['btn_abNormal'])) {
			include_once('annualFunc.php');
			$cMan = $_SESSION["Account"];
			if($_POST['btn_abNormal']=='特休代金解鎖')  {
				deleteAnnual($_POST['year']);
				updateAnnual($_POST['year'], 0);
				setAcctClose('annual',$_POST['year'], $cMan,0);
			} elseif($_POST['btn_abNormal']=='特休代金鎖定') {
				$data = computeAnnual($_POST['year']);
				$data['wkYear'] = $_POST['year'];
				insertTransferAnnual($data);
				updateAnnual($_POST['year'], 1);
				setAcctClose('annual',$_POST['year'], $cMan,1);

			} elseif($_POST['btn_abNormal']=='特休代金關帳') {
				setAcctClose('annual',$_POST['year'], $cMan,1,1);
			}
		}
		$cMan = $_SESSION["Account"];
		$fCheck = checkAcctClose('annual',$curYr,$cMan);
	}

	

?>
<!DOCTYPE HTML>
<Html>
<Head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?=$pageTitle?></title>
	<link rel="stylesheet" href="<?=$extfiles?>list.css">
	<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
	<link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
	<link href="tuning.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
	<link href="/css/mapping.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
	<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
	<script src="../../Scripts/form.js" type="text/javascript"></script> 
	<script type="text/javascript" src="tuning.js" ></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var typflt = $('select#aType option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var mtflt = $('select#month option:selected').val();
		var iLflt = $('select#is_leadmen option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(typflt) url = url+'&aType='+typflt;
		if(yrflt) url = url+'&year='+yrflt;
		if(mtflt) url = url+'&month='+mtflt;
		if(iLflt) url = url+'&is_leadmen='+iLflt;
		location.href = url;		
	}

	$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<? if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[empID]?>]')[0];
		<? } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
		$("#detail").draggable();
		
	});	
	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}
	function showMapping(empID){
		var dateB,dateE;
		if($("#dateB").val()){
			dateB=$("#dateB").val();
			dateE=$("#dateE").val();
		}else{	dateB='';	dateE='';}

		$.ajax({
			url: 'tuning.php?act=getMapping&dateB='+dateB+'&dateE='+dateE+'&empID='+empID,
			type:"POST",
			dataType:'text',
			success: function(data){
				$('#detail').html(data);
				showDetail();
			},
			error:function(xhr, ajaxOptions, thrownError){  }
		});
		
	}

	function showDetail(){
		detail.style.display='block';
	}
	function closePnl(){
		detail.style.display='none';
	}
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="<?=$keyword;?>">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'sxBtn'?>" onclick='location.href="append.php";'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="MsgBox()">
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>

    <select name="aType" id="aType" onChange="filterSubmit(this)"><option value=''>-全部假別-</option>
			<? foreach($absType as $v) echo "<option ".($_REQUEST[aType]==$v?'selected':'').">$v</option>" ?>
    </select>  
    <select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-5;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    <select name="month" id="month" onChange="doSubmit()"><? for($i=1;$i<13;$i++) { $sel=($curMh==$i?'selected':''); echo "<option $sel value='$i'>$i 月</option>"; } ?></select>
    <select name="is_leadmen" id="is_leadmen" onChange="doSubmit()">
    	<?php foreach($isLeadmenList as $key => $val) {
    		$selected = ($is_leadmen == $key) ? 'selected' : '';
    		echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
    	}?>
    </select>
    <label><input type="checkbox" name="afterClose" value="1" onChange="doSubmit()" <?php if($afterClose == 1) { echo 'checked'; }?>>薪資關帳後請假</label>
   
    <input type="button" name="toExcel" value="月明細表" onClick="excelfm.submit()"><input type="button" name="toExcely" value="年明細表" onClick="excelfmy.submit()">
    <input type="button" name="toExcely" value="年合計表" onClick="excelfmy_detail.submit()">
    <input type="button" name="toExcel" value="匯出特休請假" onclick="getExcelV()">
<?php 
	if ($_SESSION['privilege'] > 10) { 
		echo '<input name="btn_abNormal" type="hidden" value="">';
		switch ($fCheck) {
			case '0':
				echo '<input type="button" value="特休代金鎖定" onClick="checkAcctClose(this)">'; 
				break;
			case '1':
				echo '<input type="button" value="特休代金已關帳" disabled>';
				break;
			default:
				echo '<input type="button" value="特休代金解鎖" onClick="checkAcctClose(this)">&nbsp;'; 
				echo '<input type="button" value="特休代金關帳" onClick="checkAcctClose(this)">'; 
				break;
		}
	}
?>
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)) {
		$sql="select A.*,B.empName,B.depID from $tableName where $defWh and $filter";
		$sqly="select A.*,B.empName,B.depID from $tableName where $defWhy and $filter";
	} else {
		$sql="select A.*,B.empName,B.depID from $tableName where $defWh";
		$sqly="select A.*,B.empName,B.depID from $tableName where $defWhy";
	}
  // 搜尋處理
  if ($keyword!="") {
		$sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";
		$sqly.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";
	}
	// for excel output use.
	$outsql = $sql; //echo $outsql;
	$outsqly = $sqly; //echo $outsqly;
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	//echo $sql; //exit;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>><a href="details.php?id=<?=$r[id]?>" target="mainFrame"><?=$id?></a></td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;		
						case "datetime" : echo date('Y/m/d H:i',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						case "empID" : echo $emplyeeinfo[$fldValue]; break;
						default : 
							if($fnAry[$i]=="tuning"){
								if($r[isOK]=="1" && $r[isDel]=="0"){
									$flag1 = false;	$flag1Tx = " ";
									$wkMonth = date('Ym',strtotime($r['bDate']));
									$rsT = db_query("select * from acctclose where title='abnormal' and allClose=1 and conditions='$wkMonth'");
									if(db_eof($rsT)) $flag1=true; else $flag1Tx='已關帳';
									if($flag1) echo '<input type="button" onclick="doTuning('.$r['id'].',\''.$emplyeeinfo[$r['empID']].'\')" value="調整"/>';	else echo $flag1Tx;
								}else echo "";
							}else if($fnAry[$i]=="logs"){
								if($fldValue!="")	echo "<span title='$fldValue'>已調整</span>";
							}else if($fnAry[$i]=="mapping" && $r['aType']=="補休"){
								echo "<input type='button' value='明細' onclick='showMapping(\"".$r[empID]."\")'/>";
							}elseif($fnAry[$i]=="file"){
							  $ulpath = '../../data/absence/'.$id.'/';
							  if(file_exists($ulpath)){
							    $dh  = opendir($ulpath);
							    while (false !== ($filename = readdir($dh))) {
							      if ($filename=='.') continue;
							      if ($filename=='..') continue; 
							      $file_path = '../../data/absence/'.$id.'/'.$filename;  
							      echo '<a target="_blank" href="'.$file_path.'"><img src="/images/paper.png" width="15" border="0"/ style="float: left;padding-top: 2px;"></a>';
						      }
						    }
							}else	echo $fldValue;	
			    } 
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>

<form name="excelfm" action="excel.php" method="post" target="excel">
	<input type="hidden" name="fmTitle" value="員工請假 月報表">
  	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
	<input type="hidden" name="sqltx" value="<?=$outsql?>">
</form>
<form name="excelfmy" action="excel.php" method="post" target="excely">
	<input type="hidden" name="fmTitle" value="員工請假 年報表">
	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
	<input type="hidden" name="sqltx" value="<?=$outsqly?>">
</form>
<form name="excelfmy_detail" action="excel.php" method="post" target="excely">
	<input type="hidden" name="fmTitle" value="員工請假 年合計表">
	<input type="hidden" name="depID" value="<?=$_REQUEST[depFilter]?>">
	<input type="hidden" name="sqltx" value="<?=$outsqly?>">
</form>
<form id="annual" name="annual" action="excel.php" method="post" target="excel">
	<input type="hidden" name="fmTitle" value="員工特休時數管理">
	<input type="hidden" name="depID" value="">
	<input type="hidden" name="empID" value="">
	<input type="hidden" name="year" value="<?=$curYr?>">
	<input type="hidden" name="keyword" value="<?=$keyword?>">
	<input type="hidden" name="sql" value="<?=$outsql?>">
	<input type="hidden" name="printType" value="">
	<input type="hidden" name="projID" value="">
	<input type="hidden" name="isOnduty" value="">
</form>


<div id='d_tuning' class='divBorder1'>
	<div class='dtlTitle'>請假資料調整</div>
	<div class='divBorder2' id='d_list' >
	<input type="hidden" id='id'/>
	<table class='grid' >
	<tr align='center' class='tbTitle'><td>員工</td><td >時數</td><td>開始時間</td><td >結束時間</td><td >假別</td><td width='200'>調整原因</td><td>銷假</td></tr>
	<tr align='center'>
		<td id='td_empName' width='80'></td>
		<td><input type="text" size="2" id="hours" ></td>
		<td><input type="text" size="12" id="bDate"></td>
		<td><input type="text" size="12" id="eDate"></td>
		<td id='td_aType'></td>
		<td ><input type="text" size="22" id="reason" ></td>
		<td><input type='checkbox' value='1' name='isDel' class='opt'/></td>
	</tr>
	</table>
	</div>
		<div class='divBottom'>
		<input type='button' class='red' onclick='save()' value="確定"/>&nbsp;&nbsp;
		<input type='button' class='red' value="取消" onclick='hide("d_tuning")'/></div>
		</div>
</div>

<div id='detail'></div>
<script type="text/javascript">
	var rztExcel = false;
	function getExcelV(){
		var url = 'selectExcel.php?'; 
        if(rztExcel){
        	rztExcel.close();
        }
        if(rztExcel == false){
        	rztExcel = window.open(url, "web",'width=600,height=750');
        }else{
        	rztExcel = window.open(url, "web",'width=600,height=750');
        	rztExcel.focus();
        }
	}

	$(function(){
		$('[name="btn_abNormal"]').click(function(){
			$('[name="form1"]').attr('onSubmit','return true');
		});
	});

	function checkAcctClose(obj) {
		var title = $(obj).val();
		var show = '';
		if(title == '特休代金鎖定') {
			show = '特休代金鎖定後,將會產生銀行媒體轉帳！並鎖定"年資特休",請問是否確定鎖定?';
		} else if(title == '特休代金解鎖') {
			show = '特休代金解鎖後,將會移除銀行媒體轉帳！並解鎖"年資特休",請問是否確定解鎖?';
		} else {
			show = '請問是否確定將特休代金關帳?';
		}

		if(!confirm(show)) return false;
		$('[name="btn_abNormal"]').val(title);
		$('[name="form1"]').attr('onSubmit','');
		$('[name="form1"]').submit();
	}
</script>
</body>
</Html>