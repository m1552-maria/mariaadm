<?php
/*
計算年資
參考琬珍計算年資寫法 (/maintain/bonus_festival/doappend.php)
*/
function getYears($emplyee_list){
    //今年和明年會需要的東西的array
    $now_year = date('Y');
    $all_data[$now_year] = array(
        'now_year'=>date('Y'),
        'now_date'=>date('Y').'-12-31',
        'before_date'=>(date('Y')-1).'-12-31',
        'now_start_day'=>strtotime(date('Y').'-01-01'),
        'now_end_day'=> strtotime(date('Y').'-12-31')
    );

    //留職停薪
    $job_status = array();
    $sql = 'select * from `emplyee_status` where `jobStatus` IN (3,4)';//留職停薪或是延長留停
    $rs = db_query($sql);
    while($r=db_fetch_array($rs)){
        $bdate = strtotime($r['bdate']);
        $edate = strtotime($r['edate']);
        if($edate>$bdate){
            $job_status['all_year'][$r['empID']][$r['id']] = $r;//每一年的留職停薪//當年不能計算
        }
    }

    //開始計算特休
    $years=array();
    foreach($emplyee_list as $k=>$v){
        foreach($all_data as $k1=>$v1){//今年和明年特休
            //宣告全部變數
            $now_year = $v1['now_year'];
            $now_date = $v1['now_date'];
            $before_date = $v1['before_date'];

            //計算年資
            $now_day = (strtotime($now_date) - strtotime($v['hireDate']))/86400+1;//天數
            //判斷留職停薪//去年之前的
            $leave = 0;//留職停薪日數
            if(isset($job_status['all_year'][$k])){
                foreach($job_status['all_year'][$k] as $k2=>$v2){
                    //處理輸入錯誤的
                    if($v2['bdate'] == '' || $v2['bdate']=='0000-00-00'){
                        $v2['bdate'] = $before_date;//去年年底
                    }
                    if($v2['edate'] == '' || $v2['edate']=='0000-00-00'){
                        $v2['edate'] = $before_date;//去年年底
                    }


                    //判斷要算到甚麼時候//計算到今年以前的年底//有符合才要扣掉
                    $bdate = strtotime($v2['bdate']);
                    $edate = strtotime($v2['edate']);

                    if($edate>$bdate){//這樣才是有效數據
                        if($bdate<strtotime($before_date)){//計算到去年的留職停薪總共有多少天
                            if($edate>strtotime($before_date)){//表示超過去年年底了
                                $leave = $leave + ((strtotime($before_date)-$bdate))/86400+1;
                            }else{//正常情況
                                $leave = $leave + ($edate-$bdate)/86400+1;
                            }
                        }
                    }
                }
            }

            
            //計算會計年資
            $actDays = (strtotime($before_date) - strtotime($v['hireDate']))/86400+1-$leave;//會計年資要扣掉留職停薪//天數
            if($actDays<0){
                $actDays = 0;//因為不滿一年
            }
            $years[$v[empID]] = intval($actDays/365);
        }
    }
    return $years;
}
?>