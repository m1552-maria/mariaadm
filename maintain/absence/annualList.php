<?php
	@session_start();
	include "../../config.php";
	include('../../getEmplyeeInfo.php');	
	include '../inc_vars.php';
	//include 'Fee.php';
	include_once('annualFunc.php');  
	include_once('getYears.php');
		
	$cData = array('empID'=>$_REQUEST['empID']);
	$cAnnyualData = computeAnnual($_REQUEST['year'], $cData);

	if($cAnnyualData['result'] == 0) { echo $cAnnyualData['msg']; exit; }
  $data = $cAnnyualData['emplyee_annual'][$_REQUEST['empID']];
	$years = getYears($cAnnyualData['emplyee_annual']);
	
	$key = "annual_".$_REQUEST['year']."_".$_REQUEST['empID'];
?>
<Html>
<Head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>未休特休</title>
	<link rel="stylesheet" href="<?php echo $extfiles?>list.css">
	<style>
		body {  margin: 0px; }
		#list { border-collapse:collapse; font-size: 10px; }
		#bgCover {
			position: absolute;
			display: none;
			left: 0; top: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(32, 32, 32, 0.75);
		}
		#formtbl {
			font-size: 12px;
			padding: 10px;
			border-radius: 10px;
		}
	</style>
	<script src="/Scripts/jquery.min.js"></script>
</head>

<body>
<table id="list" width="100%" align="center" border="1" cellpadding="2" cellspacing="0">
	<tr bgcolor="#C0C0C0">
		<th>流水號</th><th>員編</th><th>姓名</th><th>到職日</th><th>工時身分</th><th>總年資</th>
		<th>特休假總時數</th><th>已申請特休時數</th><th>剩餘特休時數</th><th>未休特休工資</th><th>申請延休</th>
	</tr>
	<?php
	$conntent = "<tr><td>1</td><td>".$data['empID']."</td><td>".$data['empName']."</td><td>".$data['hireDate']."</td><td>".$data['jobClass']."</td><td>"
		. $years[$data['empID']] . "</td><td>".$data['spHolidays']."</td><td>".$data['abhours']."</td><td>"
		. ($data['spHolidays']-$data['abhours']) ."</td><td>"
		. (number_format($data['money']+$data['lastMoney'])) ."</td><td>"
		. ($data['isDelay']?'V':'') . "</td></tr>"; 
	echo $conntent;
	?>		
</table>
<br/>
<?
	$rsX = db_query("select * from pay_record where id='$key'");
?>
<table id="list" width="100%" align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="#C0C0C0">
	<th colspan="10" style="font-size: larger; "><div style="padding-top: 4px;">付款紀錄</div></th>
</tr>
<?php if(!db_eof($rsX)) while($rX=db_fetch_array($rsX)) { ?>
<tr>
	<td bgcolor="#ddd" width="50">付款編號</td><td width="150"><?php echo $rX['id']?></td>
	<td bgcolor="#ddd" width="50">付款日期</td><td width="150"><?php echo $rX['rDate']?></td>
	<td bgcolor="#ddd" width="50">付款金額</td><td><?php echo $rX['money']?></td>
	<td bgcolor="#ddd" width="50">付款人員</td><td><?php echo $rX['createMan']?></td>
	<td bgcolor="#ddd" width="50">備註</td><td><?php echo $rX['notes']?></td>
<tr>
<?php } else { ?>
	<th colspan="10" style="font-size: larger; "><button style="float: left;" onclick="$('#bgCover').fadeIn(500)">新增</button></th>
<?php } ?>
</table>

<div id="bgCover">
	<br/>
	<form id="form1" action="../pay_record/doappend.php" method="POST">
	<table id="formtbl" width="50%" bgcolor="#f0f0f0" align="center" cellpadding="4" cellspacing="0">
		<tr><th>付款金額</th><td><input type="text" name="money" value="0"></td></tr>
		<tr><th>備　　註</th><td><textarea name="notes" rows="3" cols="50"></textarea></td></tr>
		<tr><td>&nbsp;</td><td>
			<input type="hidden" name="key" value="<?php echo $key?>">
			<input type="hidden" name="empID" value="<?php echo $_REQUEST['empID']?>">
			<input type="hidden" name="year" value="<?php echo $_REQUEST['year']?>">
			<input type="hidden" name="createMan" value="<?php echo $_SESSION['Account']?>">
			<input type="hidden" name="backPath" value="../absence/annualList.php">
			<input type="submit" name="Submit" value="確定">
			<button type="button" onclick="$('#bgCover').fadeOut(500)">取消</button>
		</td></tr>
	</table>
	</form>
</div>
</body>
</Html>