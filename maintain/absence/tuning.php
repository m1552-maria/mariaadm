<?
session_start();
include "../../config.php";
include "../../getEmplyeeInfo.php";
include "../inc_vars.php";
include "../../emplyee/absence/func.php";

$act=$_REQUEST['act'];
switch($act){
	case "getData":
		$id=$_REQUEST['id'];
		getData($id,$absType, $_REQUEST['year']);
		break;
	case "saveData":
		$id=$_REQUEST['id'];
		$bDate=$_REQUEST['bDate'];
		$eDate=$_REQUEST['eDate'];
		$hours=$_REQUEST['hours'];
		$aType=$_REQUEST['aType'];
		$reason=$_REQUEST['reason'];
		$isDel=$_REQUEST['isDel'];
		$orig_aType=$_REQUEST['orig_aType'];
		saveData($id,$bDate,$eDate,$hours,$aType,$reason,$isDel,$orig_aType);
		break;
	case "getMapping":
		$empID=$_REQUEST["empID"];
		$empName=$emplyeeinfo[$empID];
		$dateB=$_REQUEST['dateB'];
		$dateE=$_REQUEST['dateE'];
		echo getMapping($empID,$empName,$dateB,$dateE);
		break;
}
function getMapping($empID,$empName,$dateB,$dateE){
	class otData {
    function do_something() { }
	}
	if($dateB==""){
		//$time=time();
		//$dateB = date("Y-m-d",strtotime("-6 month",$time));
		//$dateE = date("Y-m-d",strtotime("+1 days",$time));
		//20180701 變更規則
		$dateB = date("Y-01-01");
		$dateE = date("Y-12-31");	
	}else{
		$dateB = date("Y-m-d",strtotime($dateB));
		$dateE = date("Y-m-d",strtotime($dateE));;		
	}
	//加班單-已通過簽核
	$sql="select * from overtime ";
	$sql.="where empID='$empID' and isOK=1 and isDel=0 and fmAct=0 ";
	$sql.="and bDate between '$dateB' and '$dateE' ";
	$sql.="order by id ";
	$rs=db_query($sql);
	$otAry=array();
	$otIds=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$ot = new otData;
			$ot->id = $r[id];
			$ot->bDate = date("Y-m-d H:i",strtotime($r[bDate]));
			$ot->eDate = date("Y-m-d H:i",strtotime($r[eDate]));
			$ot->isOK = $r[isOK];
			$ot->hours = $r[hours];
			$ot->avHours=$r[hours];
			$otAry[$r[id]]=$ot;
			array_push($otIds,$r[id]);
		}
	}
	//加班單-未通過簽核
	$sql="select * from overtime where empID='$empID' and (isOK=0 and is_leadmen=0)  and isDel=0 and bDate between '$dateB' and '$dateE' order by id";
	$rs=db_query($sql);
	$otAry2=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$ot2 = new otData;
			$ot2->id = $r[id];
			$ot2->bDate = date("Y-m-d H:i",strtotime($r[bDate]));
			$ot2->eDate = date("Y-m-d H:i",strtotime($r[eDate]));
			$ot2->isOK = $r[isOK];
			$ot2->hours = $r[hours];
			$ot2->avHours=$r[hours];
			$otAry2[$r[id]]=$ot2;
		}
	}
	//mapping表-加班
	$color=array("#ffe6e6","#ffcccc","#ffffe6","#ffffcc","#e6ffe6","#ccffcc","#e6f2ff","#cce6ff","#f2e6ff","#FFD2FF");
	$sql="select * from absence_overtime where otId in (".join(",",$otIds).") order by otId";
	$rs=db_query($sql);
	$abIds=array();
	$aColor=array();
	if(!db_eof($rs)){
		$i=-1;
		while ($r=db_fetch_array($rs)) {
			$o=$otAry[$r[otId]];
			if($o->avHours-$r[hours]>=0){
				if($last_otId!=$r[otId]){
					if($i>8)	$i=-1;
					$i++;	
				}
				$o->avHours-=$r[hours];
				$o->color=$color[$i];
				$aColor[$r[otId]]=$color[$i];
			}
			$o->abId=$r['abId'];
			array_push($abIds,$r['abId']);
			$last_otId=$r[otId];
		}
	}
	//請假表
	$abAry=array();
	$sql="select *, ab.id as id,ao.hours as hours,ab.hours as hours2,ab.rdate as rdate ";
	$sql.="from absence as ab left join absence_overtime as ao ";
	$sql.="on ab.id=ao.abId ";
	$sql.="where ab.empID='$empID' and ab.isDel=0 and ab.aType='補休' and ab.bDate>'$dateB' and ab.isOK=1 ";
	$sql.="order by CASE WHEN ao.otId IS NULL THEN 1 ELSE 0 END ,ao.otId ";
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$ab = new otData;
			$ab->id = $r[id];
			$ab->bDate = date("Y-m-d H:i",strtotime($r[bDate]));
			$ab->eDate = date("Y-m-d H:i",strtotime($r[eDate]));
			$ab->rDate = date("Y-m-d H:i",strtotime($r[rdate]));
			$ab->isOK = $r[isOK];
			$ab->hours = $r[hours]?$r[hours]:$r[hours2];
			if($aColor[$r[otId]])	$ab->color=$aColor[$r[otId]]; 
			array_push($abAry,$ab);
		}
	}
	//請假單-未通過簽核
	$sql="select * from absence ";
	$sql.="where empID='$empID' and (isOK=0 and is_leadmen=0)  and isDel=0 ";
	$sql.="and aType='補休' ";
	$sql.="order by bDate ";
	$rs=db_query($sql);
	$abAry2=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$ab2 = new otData;
			$ab2->id = $r[id];
			$ab2->bDate = date("Y-m-d H:i",strtotime($r[bDate]));
			$ab2->eDate = date("Y-m-d H:i",strtotime($r[eDate]));
			$ab2->rDate = date("Y-m-d H:i",strtotime($r[rdate]));
			$ab2->isOK = $r[isOK];
			$ab2->hours = $r[hours];
			$ab2->avHours=$r[hours];
			$abAry2[$r[id]]=$ab2;
		}
	}
	$html="<table class='tb-container'>";
	$html.="<tr class='title'><td colspan='2'>$empID-$empName-補休明細&nbsp;&nbsp;<input type='date' id='dateB' value='".$dateB."' > ~ <input type='date' id='dateE' value='".$dateE."'><input type='button' onclick='showMapping(\"".$empID."\")' value='搜尋'/><img src='/maintain/images/del.png' class='del' onclick='closePnl()'/></td></tr>";
	$html.="<tr><td colspan='2'><span class='tip'>* 加班單時數會於請假單「簽核過」後扣除，請注意請假單中「未簽核」的單據，避免可補休時數認知有誤。</span></td></tr>";
	$html.="<tr><td style='vertical-align: top;'>";
		$html.="<div class='d_container'>";
			$html.="<div class='d_container'>";
				$html.="<table class='fancytable'>";
					$html.="<tr><td align='center' colspan='6' class='title2'>加班表</td></tr>";
					$html.="<tr><td>編號</td><td>開始時間</td><td>結束時間</td><td>時數</td><td>狀態</td></tr>
					<tr><td class='sub-title' colspan='5'>已簽核</td></tr>";
					$sum=0;
					$sum_av=0;
					if(count($otAry)>0){
						foreach($otAry as $k=>$v){
							if($v->color){
								$str="<tr bgcolor='".$v->color."'>";
							}else $str="<tr>";
							//$str="<tr bgcolor='".$color[$i]."'>";
							//$i++;
							$str.="<td>".$v->id."</td>";
							$str.="<td>".$v->bDate."</td>";
							$str.="<td>".$v->eDate."</td>";
							$sum+=$v->hours-$v->avHours;
							if($v->hours==$v->avHours){//未沖銷
								$str.="<td>".$v->hours."</td>";
								$str.="<td>未沖銷</td>";
								$str.="</tr>";
								$sum_av+=$v->avHours;
							}else if($v->avHours>0){//已有補休，但有餘額
								$str.="<td>".($v->hours-$v->avHours)."</td>";
								$str.="<td class='tip'>已補休</td>";
								$str.="</tr>";
								$str.="<tr>";
								$str.="<td>".$v->id."</td>";
								$str.="<td>".$v->bDate."</td>";
								$str.="<td>".$v->eDate."</td>";
								$str.="<td>".$v->avHours."</td>";
								$str.="<td>未沖銷</td>";
								$str.="</tr>";
								$sum_av+=$v->avHours;
							}else{//沒有餘額
								$str.="<td>".$v->hours."</td>";
								$str.="<td class='tip'>已補休</td>";
								$str.="</tr>";
							}
							$html.=$str;
						}
					}else $html.="<tr><td colspan='6'>目前沒資料</td></tr>";

					//$html.="<tr class='sumbg'><td>總計</td><td colspan='5' style='text-align: left'>已補休".$sum."小時，未沖銷".$sum_av."小時 <br>(請假單未簽核的時數尚未扣除)</td></tr>";

					$html.="<tr>";
						$html.="<td class='sub-title' colspan='6'>已申請未簽核</td>";
					$html.="</tr>";
					if(count($otAry2)>0){
						foreach($otAry2 as $k=>$v){
							$str="<tr>";
							$str.="<td>".$v->id."</td>";
							$str.="<td>".$v->bDate."</td>";
							$str.="<td>".$v->eDate."</td>";
							$str.="<td>".$v->hours."</td>";
							$str.="<td>未簽核</td>";
							$str.="</tr>";
							$html.= $str;
						}
					}else $html.="<tr><td colspan='6'>目前沒資料</td></tr>";
				$html.="</table>";
			$html.="</div>";
		$html.="</td>";
		$html.="<td style='vertical-align: top;'>";
			$html.="<div class='d_container'>";
				$html.="<table class='fancytable2'>";
					$style="style='border:1px solid #666;'";
					$html.="<tr ><td align='center' colspan='5' class='title2'>請假表</td></tr>";
					$html.="<tr ><td ".$style.">編號</td><td ".$style.">開始時間</td><td ".$style.">結束時間</td><td ".$style.">申請時間</td><td ".$style.">時數</td></tr>";
					$html.="<tr><td class='sub-title' colspan='5'>已簽核</td></tr>";
						$id=0;
						$style='';
						if(count($abAry)>0){
							foreach($abAry as $k=>$v){
								if($v->color){
									$str="<tr bgcolor='".$v->color."'>";
								}else $str="<tr>";
								if($id!=$v->id){
									$id=$v->id;
									$style="style='border-top:1px solid #666;'";
									$str.="<td $style>".$id."</td>";
								}else{
									$style="";
									$str.="<td $style>&nbsp;</td>";
									$style="";
								}
								$str.="<td $style>".$v->bDate."</td>";
								$str.="<td $style>".$v->eDate."</td>";
								$str.="<td $style>".$v->rDate."</td>";
								$str.="<td $style>".$v->hours."</td>";
								$str.="</tr>";
								$html.=$str;
							}
						}else $html.="<tr><td colspan='5'>目前沒資料</td></tr>";
					$html.="<tr>";
						$html.="<td class='sub-title' colspan='5'>已申請未簽核</td>";
					$html.="</tr>";
						if(count($abAry2)>0){
							foreach($abAry2 as $k=>$v){
								$str="<tr>";
								$str.="<td>".$v->id."</td>";
								$str.="<td>".$v->bDate."</td>";
								$str.="<td>".$v->eDate."</td>";
								$str.="<td>".$v->rDate."</td>";
								$str.="<td>".$v->hours."</td>";
								$html.= $str;
							}
						}else $html.= "<tr><td colspan='5'>目前沒資料</td></tr>";
				$html.="</table>";
			$html.="</div>";
		$html.="</td>";
	$html.="</tr>";
	$html.="</table>";
	return $html;
}
function getData($id,$absType, $year){
	$sql="select * from absence ";
	$sql.="where id=".$id;
	//echo $sql."\n";
	$rs=db_query($sql);
	$ary=array();
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$ary[0]=$r['hours'];
		$ary[1]=date('Y/m/d H:i',strtotime($r['bDate']));
		$ary[2]=date('Y/m/d H:i',strtotime($r['eDate']));
		$ary[3]=$r['aType'];
		$ary[4]=$r['logs'];
		$ary[5]=date('H',strtotime($r['bDate']));
		$ary[6]=date('i',strtotime($r['bDate']));
		$ary[7]=date('H',strtotime($r['eDate']));
		$ary[8]=date('i',strtotime($r['eDate']));
		$empID=$r["empID"];
	}
	$avHours = get_rest_Hours(strtotime($r['bDate']),$empID);
	$str='';
	foreach($absType as $v) {
		if($v==$ary[3]){ $selected='selected';	}else{	$selected='';}
		$str.="<option value='".$v."' $selected >".$v."</option>";
	}
	$ary[9]=$str;
	$ary[10]=$avHours;

	//計算特休時數
	//$r=db_fetch_array(db_query("select spHolidays from emplyee where empID='$empID'"));
	/*20180410 修改特修取得時數 改抓 emplyee_annual資料表的值*/
	$r=db_fetch_array(db_query("select hours, userYear from emplyee_annual where year='".$year."' and empID='$empID'"));

	$spHolidays = $r[0]?$r[0]:0;
	/*20180417 sean 修正 年度取傳入年*/
	$sql = "SELECT sum(hours) as thours,aType FROM `absence` where empID='$empID' and ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and YEAR(bDate)=".$year." and aType='特休' ";
	//$sql = "SELECT sum(hours) as thours,aType FROM `absence` where empID='$empID' and ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and YEAR(bDate)=YEAR(NOW()) and aType='特休' ";
	$rs = db_query($sql);
	$usedHour=0;
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
	 	$usedHour=$r[thours];
	}
	$ary[11]=$spHolidays-$usedHour;
	echo join("#",$ary);
}
function saveData($id,$bDate,$eDate,$hours,$aType,$reason,$isDel,$orig_aType){


	//判斷是否時間有誤
	//先抓目前這個人的empid
	$sql = 'select * from `absence` where `id`='.$id;
	$rs = db_query($sql);
	if(!db_eof($rs)){
		$r = db_fetch_array($rs);
		$empID = $r['empID'];
	}

	//判斷時數對不對
	$now_bDate = strtotime($bDate);
	$now_eDate = strtotime($eDate);
	$result = 0;
	if(!$isDel){//非銷單，才需要做時段檢查
		//檢查日期是否有誤
		$sql = 'select * from `absence` where `empID`='."'".$empID."' and isDel=0 ";
		$rs = db_query($sql);
		if(!db_eof($rs)){
			while($r = db_fetch_array($rs)) {
				if($result == 0){
					if(!($r['is_leadmen']==1 && $r['isOK']==0)){
						$absence_bDate = strtotime($r['bDate']);
						$absence_eDate = strtotime($r['eDate']);
						if(($now_bDate<=$absence_bDate and $now_eDate<=$absence_bDate) or ($now_bDate>=$absence_eDate and $now_eDate>=$absence_eDate)){
							//正常情況
						}else{
							if($r['id']!=$id){//排除此筆的時段
								$result = 1;	
								echo $result;
								exit;
							}
						}
					}
				}
			}
		}
	}

	$sql="select * from absence ";
	$sql.="where id=".$id;
	$rs=db_query($sql);
	$r=db_fetch_array($rs);
	//判斷起迄是否同年月，用來縮短字數顯示
	$ob=date("Y-m-d",strtotime($r["bDate"]));
	$oe=date("Y-m-d",strtotime($r["eDate"]));
	$oldBDate = date("Y-m-d H:i", strtotime($r[bDate]));
	if($ob==$oe){	
		$oldEDate = date("H:i", strtotime($r[eDate]));
		$old=$oldBDate."~".$oldEDate;
	}else{
		$oldEDate = date("Y-m-d H:i", strtotime($r[eDate]));
		$old=$oldBDate."~".$oldEDate;
	}
	$nb=date("Y-m-d",strtotime($bDate));
	$ne=date("Y-m-d",strtotime($eDate));
	$newBDate = date("Y-m-d H:i", strtotime($bDate));
	if($nb==$ne){	
		$newEDate = date("H:i", strtotime($eDate));
		$new=$newBDate."~".$newEDate;
	}else{
		$newEDate = date("Y-m-d H:i", strtotime($eDate));
		$new=$newBDate."~".$newEDate;
	}
	if(isset($_SESSION["empID"])){	$empID=$_SESSION["empID"];	}else{	$empID=$_SESSION["Account"];}
	$data=$r['hours'].'小時'.$r['aType'].'：'.$old." 改為：";
	$data.=$hours.'小時'.$aType.'：'.$new."(".$empID."於".$_SESSION["empName"]."".date("Y-m-d H:i")."修改,原因:".$reason.")。";
	$logs=$r[logs]."* ".$data."<br>";

	

	$sql="update absence ";
	$sql.="set aType='".$aType."',bDate='".$bDate."',eDate='".$eDate."',hours=".$hours.",logs='".$logs."',isDel=".$isDel.",isReqD=".$isDel." ";
	$sql.="where id=".$id;
	//echo $sql;exit;
	db_query($sql);
	
	if($aType!="補休" && $orig_aType=="補休"){//原本是補休，後來變更假別，歸還加班單時數
		$sql="delete from absence_overtime where abId=".$id;
		db_query($sql);
	}else if($aType=="補休"){
		$sql="delete from absence_overtime where abId=".$id;
		db_query($sql);
		do_absence_overtime($id);//補休沖銷加班單
	}
}
?>