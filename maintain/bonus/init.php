<?	
	session_start();
    header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');
	include('../../getEmplyeeInfo.php');
	$bE = true;	//異動權限
	$pageTitle = "其他獎金設定";
	$tableName = "bonus";
	$jointableName = "bonus_class";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bonus/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
	


	

	// Here for List.asp ======================================= //
	$defaultOrder = "bID";
	$searchField1 = "b.title";
	$searchField2 = "bc.title";
	$searchField3 = "b.wkMonth";
	$pageSize = 20;	//每頁的清單數量

	$state=array("0"=>"否","1"=>"是");
	// 注意 primary key should be first one
	$fnAry = explode(',',"bID,title,bcID,action,wkMonth");
	$ftAry = explode(',',"編號,說明,類別,動作,執行月份");
	$flAry = explode(',',"50,,,,");
	$ftyAy = explode(',',"id,text,text,text,text");
	
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,bcID,isTax,taxOver,taxNum,isTransfer");
	$newftA = explode(',',"說明,類別,是否收稅,超過多少金額即收稅,扣稅百分比,匯入媒體轉帳");
	$newflA = explode(',',",,,,,,");
	$newfhA = explode(',',",,,,,,");
	$newfvA = array('','');
	$newetA = explode(',',"text,checkbox,radiobutton,taxtext,taxpercentage,radiobutton");


	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"bID,title,bcID,isTax,taxOver,taxNum,isTransfer");
	$editftA = explode(',',"編號,說明,類別,是否收稅,超過多少金額即收稅,扣稅百分比,匯入媒體轉帳");
	$editflA = explode(',',",,,,,,,");
	$editfhA = explode(',',",,,,,,,");
	$editfvA = array('','','');
	$editetA = explode(',',"text,text,checkbox,radiobutton,taxtext,taxpercentage,radiobutton");
?>