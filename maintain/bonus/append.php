<?php
	include("init.php"); 
	if ($_SESSION['privilege'] <= 10){
		$readonly='readonly';
		$class='readonly';
	}else{
		$readonly='';	
		$class='';
	}
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
.readonly{	color:#999;}
	
	
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 新增</title>
 <script type="text/javascript">
 function formvalid(frm){
 	if($('[name="title"]').val()==""){	alert("請輸入名稱");return false;}

	if($("input[name='isTax[]']:checked").val() == 1 && $("input[name='taxOver']").val() == ''){
		alert("請輸入超過的金額");
		return false;
	}
	if($("input[name='isTax[]']:checked").val() == 1 && $("input[name='taxNum']").val() == ''){
		alert("請輸入扣稅百分比");
		return false;
	}
	
 	var bcIDs=document.getElementsByName("bcID[]");
 	var ck=false;
 	for(var i=0;i<bcIDs.length;i++){
 		if(bcIDs[i].checked)	ck=true;
 	}
 	if(!ck){	alert("請選擇類別");return false;}
 	
	return true;
 }
 	
 </script>
</Head>

<body class="page">



<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>
	<tr>
  	<td align="right" class="colLabel">作業月份</td>
    <td>
      	<input type="radio" name="wkmonth" value="<?=date("Ym", strtotime('-1 month'))?>"><?=date("Y年m月", strtotime('-1 month'))?>
    	<input type="radio" name="wkmonth" value="<?=date('Ym')?>" checked><?=date('Y年m月')?>
      <input type="radio" name="wkmonth" value="<?=date("Ym", strtotime('+1 month'))?>"><?=date("Y年m月", strtotime('+1 month'))?>
    </td>
  </tr>
	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><?
  	 	switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "taxtext"    : echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' pattern=[0-9]* >".'元'; break;
			case "taxpercentage"    : echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='1' value='$newfvA[$i]' class='input' pattern=[0-9]* >".'%'; break;

			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  			case "checkbox": 
  				$first_bcID = '';
				$sql="select * from ".$jointableName;
				$rs=db_query($sql);
				if(!db_eof($rs)){
					$j=1;
					while($r=db_fetch_array($rs)){
						$checked = '';
						if($first_bcID == ''){
							$first_bcID = $r[bcID];
							$checked = 'checked';	
						} 

						echo "<label><input type='radio' name='bcID[]' value='".$r[bcID]."' class='input' $checked>".$r[title].'</label>'; 
						if($j%3==0)	echo "<p>";
						$j++;
					}
				}
				  break;
			case "radiobutton":
				echo "<label><input type='radio' name='".$newfnA[$i]."[]' value='1' class='input'>".'是'.'</label>'; 
				echo "<label><input type='radio' name='".$newfnA[$i]."[]' value='0' class='input' checked>".'否'.'</label>'; 
				break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : 
				echo "<select id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=> $v) echo "<option value='$k'>$v</option>"; echo "</select>"; 
				break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>

  		<tr valign="top">
  			<td align="right" class="colLabel">選取部門及員工</td>
  			<td>
  				<ul class='dep-emp-ul'>
					<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
					<li class='dep-emp-li'>
						<div class='query-title'>部門</div>
						<div class="depID_list">NO DATA</div>
					</li>
					<li class='dep-emp-li'>
						<div class='query-title'>員工</div>
						<div class="empID_list">NO DATA</div>
					</li>
				</ul>
  			</td>
  		</tr>
	

	
  		</td></tr>

  		

	<tr>
		<td colspan="2" align="center" class="rowSubmit">
      <input type="hidden" name="startTime" id="startTime"/>
    	<input type="hidden" name="endTime" id="endTime"/>
			<input type="submit" value="確定新增" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
      <input type="button" value="取消新增" class="btn" onClick="history.back()">&nbsp;
		</td>
	</tr>
</table>
</form>
</body>
</html>
<script>
$(document).ready(function() {
	// console.log($("input[name='isTax[]']:checked").val());
	// $("input[name='taxOver']").val(0);
	// $("input[name='taxNum']").val(0);

	// $("input[name='isTax[]']").change(function() {
	// 	if(this.value == 0){
	// 		// $("input[name='taxOver']").attr('disabled', true);
	// 		// $("input[name='taxNum']").attr('disabled', true);
	// 		$("input[name='taxOver']").val(0);
	// 		$("input[name='taxNum']").val(0);
	// 		// console.log(this.value);
	// 	}else{
	// 		// $("input[name='taxOver']").attr('disabled', false);
	// 		// $("input[name='taxNum']").attr('disabled', false);
	// 		$("input[name='taxOver']").val('');
	// 		$("input[name='taxNum']").val('');
	// 		// console.log(this.value);
	// 	}
	// });

});
</script>
