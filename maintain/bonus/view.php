<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>list.css">
 <title><?php echo $pageTitle?></title>
 <style type="text/css">
 
.popupPane {
	position:absolute; 
	left: 25%; top:5%;
	background-color:#888;
	padding: 8px;
	display:none;
}
 
#d_setting{	display:none;overflow: auto;height:500px; width:500px; position: absolute;top:80px;left:120px;}
.tip{color:#900;}
.divTitle{
	color:#fff;
	background-color:#900;
	border: 0px;
	padding: 8px;
	font-family:'微軟正黑體'; 
	font-size:14px;
	width:464px;
	margin-top:10px;
	margin-left:10px;
}
.divBorder1 { /* 內容區 第一層外框 */
	background-color:#dedede;
	border:1px solid #666;
	padding:0px;
	margin:0px;
	font-size:16px;
}

.divBorder2 { /* 內容區 第二層外框 */
	padding:8px; 
	background:#FFFFFF;
	margin:0px 10px 0px 10px;
	
	height:400px;
	overflow-x:hidden;
	overflow-y:auto;
}
#d_bottom{	padding-top:8px;}
table.grid{  margin:0px;	border: 1px solid #ccc; border-collapse: collapse; width:100%;}
table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; color:#000;background: #fff; text-align:center;}


 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?php echo $pages?>;		//目前所在頁次
	var selid = '<?php echo $selid?>';	
	var keyword = '<?php echo $keyword?>';
	var cancelFG = true;	
	$(function(){
		$('#d_setting').draggable();

		$('body').on('keyup','#taxID',function(){
			editMoney = $(this).closest('td').prev('td').prev('td').find('input').val();
			handTax = this.value;
			// console.log(editMoney);
			// console.log(handTax);
			$(this).closest('td').next('td').html((editMoney - handTax)); 
		});
	});

  	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; 
				break;
			case 1 : pp=<?php echo $pages?>-1; if(pp<0) pp=0;
				break;
			case 2 : pp=<?php echo $pages?>+1; if(pp><?php echo $pageCount?>) pp=<?php echo $pageCount?>; 
				break;
			case 3 : pp=<?php echo $pageCount?>;
				break;
		}
		location.href="<?php echo $PHP_SELF?>?pages="+pp+"&keyword=<?php echo $keyword?>&fieldname=<?php echo $fieldname?>&sortDirection=<?php echo $sortDirection?>&ybk=<?php echo empty($_REQUEST['ybk']) ? $curYr : $_REQUEST['ybk']?>&mbk=<?php echo empty($_REQUEST['mbk']) ? $curMh : $_REQUEST['mbk'] ?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else{ 			
			location.href="edit.php?ID="+selid+"&pages="+cp;
		}
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
	function doAppend(){
		location.href="append.php"
	}
	function setClose(visitId){
		location.href="booking_close.php?visitId="+visitId;
		
	}
	function detail(empID,depID,money,bcID,bc_title,bID,wkMonth){
		$('.divTitle').html(bc_title);
		$.ajax({ 
		  type: "POST", 
		  url: "func.php", 
		  data: "act=getEmplyee&empID="+empID+"&bcID="+bcID+"&wkMonth="+wkMonth+"&depID="+depID+"&bID="+bID
		}).done(function( rs ) {
			// console.log(rs);
			if(rs==""){
				$("#d_allowance").html("<table class='grid'><tr><td>NO DATA</td></tr><tr><td><input type='button' value='取消' onClick='hide(\"d_setting\")'/></td></tr></table>");
			}else{
				var html='<table class="grid"><tr><td>員工編號</td><td>員工名稱</td><td>獎金</td><td>手動更改稅金</td><td>扣稅金額</td><td>實領金額</td><td>媒體轉帳</td></tr>';
				var ary=rs.split("#");
				var ary2;
				var isTax;
				var taxOver;
				var taxNum;
				var handSetTax = [];
				for(var i=0;i<ary.length;i++){
					ary2=ary[i].split(',');
					isTax = parseInt(ary2[5]);
					taxOver = parseInt(ary2[6]);
					taxNum = parseInt(ary2[7]);
					isCheck = parseInt(ary2[4]);
					handSetTax[ary2[0]] = parseInt(ary2[3]);
					trueChecked = (isCheck == 1 ? 'checked' : '');
					var isTransfer = (ary2[9] == 1) ? 'checked' : '';
					isTransfer = '<input type="checkbox" value="1" name="isTransfer['+ary2[0]+']" '+isTransfer+'>';

					// falseChecked = (isCheck == 0 ? 'checked' : '');
					// if( trueChecked == '' && falseChecked == ''){
					// 	defaultChecked = 'checked';
					// }else{
					// 	defaultChecked = '';
					// }

					// if(ary2[3]){
					// 	// 用taxMoney判斷是否在資料表中是否有紀錄    有的情況
					// 	if(trueChecked == 'checked'){
					// 		// 判斷是否勾選手動填寫金額  動態給input text
					// 		html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'>是<input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+falseChecked+" "+defaultChecked+" value='0'>否</td> <td><input type='text' id='taxID' name='taxCut' size='3' value="+(ary2[3])+"></td> <td>"+(ary2[2] - ary2[3])+"</td><td>"+isTransfer+"</td></tr>";
					// 	}else{
					// 		// 無勾選給TD
					// 		html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'>是<input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+falseChecked+" "+defaultChecked+" value='0'>否</td> <td>"+(ary2[3])+"</td> <td>"+(ary2[2] - ary2[3])+"</td><td>"+isTransfer+"</td></tr>";
					// 	}
					// }else{
					// 	// 無紀錄的情況
					// 	if(isTax == 1 && ((parseInt(ary2[2]) > taxOver))) {
					// 		if(trueChecked == 'checked'){
					// 			// 判斷是否勾選手動填寫金額  動態給input text
					// 			html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'>是<input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+falseChecked+" "+defaultChecked+" value='0'>否</td> <td><input type='text' id='taxID' name='taxCut' size='3' value="+(ary2[2] * taxNum / 100)+"></td> <td>"+(ary2[2] - (ary2[2] * taxNum / 100) )+"</td><td>"+isTransfer+"</td></tr>";
					// 		}else{
					// 			// 無勾選給TD
					// 			html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'>是<input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+falseChecked+" "+defaultChecked+" value='0'>否</td> <td>"+(ary2[2] * taxNum / 100)+"</td> <td>"+(ary2[2] - (ary2[2] * taxNum / 100) )+"</td><td>"+isTransfer+"</td></tr>";
					// 		}
					// 	}else{
					// 		html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'>是<input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+falseChecked+" "+defaultChecked+" value='0'>否</td> </td> <td>"+0+"</td> <td>"+ary2[2]+"</td><td>"+isTransfer+"</td></tr>";
					// 	}
					// }

					if(ary2[3]){
						// 用taxMoney判斷是否在資料表中是否有紀錄    有的情況
						if(trueChecked == 'checked' && isTax != 0){
							// 判斷是否勾選手動填寫金額  動態給input text
							html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='checkbox' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'></td> <td><input type='text' id='taxID' name='taxCut' size='3' value="+(ary2[3])+"></td> <td>"+(ary2[2] - ary2[3])+"</td><td>"+isTransfer+"</td></tr>";
						}else{
							// 無勾選給TD
							html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='checkbox' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'></td> <td>"+(ary2[3])+"</td> <td>"+(ary2[2] - ary2[3])+"</td><td>"+isTransfer+"</td></tr>";
						}
					}else{
						// 無紀錄的情況
						if(isTax == 1 && ((parseInt(ary2[2]) > taxOver))) {
							if(trueChecked == 'checked'){
								// 判斷是否勾選手動填寫金額  動態給input text
								html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='checkbox' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'></td> <td><input type='text' id='taxID' name='taxCut' size='3' value="+(ary2[2] * taxNum / 100)+"></td> <td>"+(ary2[2] - (ary2[2] * taxNum / 100) )+"</td><td>"+isTransfer+"</td></tr>";
							}else{
								// 無勾選給TD
								html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='checkbox' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'></td> <td>"+(ary2[2] * taxNum / 100)+"</td> <td>"+(ary2[2] - (ary2[2] * taxNum / 100) )+"</td><td>"+isTransfer+"</td></tr>";
							}
						}else{
							html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='checkbox' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' "+trueChecked+" value='1'></td> </td> <td>"+0+"</td> <td>"+ary2[2]+"</td><td>"+isTransfer+"</td></tr>";
						}
					}


					// if(isTax == 1 && ( (parseInt(ary2[2]) > taxOver )) ){
					// 	html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]'  value='1'>是 <input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' checked value='0'>否 </td> <td>"+(ary2[2] * ary2[5] / 100)+"</td> <td>"+(ary2[2] - (ary2[2] * ary2[5] / 100) )+"</td><td>"+isTransfer+"</td></tr>";
					// }else{
					// 	html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td><input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]'  value='1'>是 <input type='radio' id="+ary2[0]+" name='isHandTax["+ary2[0]+"]' checked value='0'>否 </td> </td> <td>"+0+"</td> <td>"+ary2[2]+"</td><td>"+isTransfer+"</td></tr>";
					// }


					// html+="<tr><td>"+ary2[0]+"</td><td>"+ary2[1]+"</td><td><input type='text' name='money' size='3' value='"+ary2[2]+"'></td> <td>"+(ary2[2] * ary2[5] / 100)+"</td> <td>"+(ary2[2] - (ary2[2] * ary2[5] / 100) )+"</td><td>"+isTransfer+"</td></tr>";
				}
				html+="</table>";
				$("#d_allowance").html(html);
				$('#d_bottom').html("<input type='button' id='AllowanceBtn' value='確定' onClick='doAllowance(\""+rs+"\",\""+money+"\","+bcID+",\""+bc_title+"\","+bID+")'/>&nbsp;<input type='button' value='取消' onClick='hide(\"d_setting\")'/>");
			}
			$("#d_setting").show();

			// 編輯視窗  獎金手動設定
			$("input[name='money']").keyup(function() {
				// console.log(this.value);
				editMoney = this.value;
				// console.log(isTax);
				if(isTax == 1 && (editMoney > taxOver) ){
					$(this).closest('td').next('td').next('td').html(editMoney * taxNum / 100);
					$(this).closest('td').next('td').next('td').next('td').html(editMoney - (editMoney * taxNum / 100));
				}else{
					$(this).closest('td').next('td').next('td').html(0);
					$(this).closest('td').next('td').next('td').next('td').html(editMoney);
				}
			});

			if(isTax == 0){
				// alert('此類別獎金在設定中為不扣稅!');
				$("input[type=checkbox][name=isHandTax[]]").attr('disabled',true);
			}

			// 編輯視窗 是否手動更改稅額checkbox
			$("input[type=checkbox][name=isHandTax[]]").change(function() {
				editMoney = $(this).closest('td').prev('td').find('input').val();
				// this.value:1 => 是  this.value:0 => 否

				if($(this).is(":checked")){
					// console.log($(this).closest('td').prev('td').prev('td').prev('td').html());
					empNO = $(this).closest('td').prev('td').prev('td').prev('td').html();
					// 動態改變 扣稅金額 加入input text
					if(handSetTax[empNO]){
						var input = $('<input type="text" id="taxID" name="taxCut" size="3" value="'+handSetTax[empNO]+'">')
						$(this).closest('td').next('td').html(''); 
						$(this).closest('td').next('td').html(input);
						$(this).closest('td').next('td').next('td').html((editMoney - handSetTax[empNO])); 
					}else{
						var input = $('<input type="text" id="taxID" name="taxCut" size="3" value="'+$(this).closest('td').next('td').html()+'">')
						$(this).closest('td').next('td').html(''); 
						$(this).closest('td').next('td').html(input);
						// $(this).closest('td').next('td').next('td').html((editMoney - handSetTax[empNO])); 
					}
				}else{
					// 動態改變 扣稅金額 拿掉input text 
					$(this).closest('td').next('td').html(''); 
					if(isTax == 1 && (editMoney > taxOver) ){
						$(this).closest('td').next('td').html(editMoney * taxNum / 100);
						$(this).closest('td').next('td').next('td').html(editMoney - (editMoney * (taxNum / 100)));
					}else{
						$(this).closest('td').next('td').html(0);
						$(this).closest('td').next('td').next('td').html(editMoney);
					}
				}
			});

		}).fail(function( msg ) { 
		});

	}	

	function doAllowance(d,money,bcID,bc_title,bID){
		var data=d.split("#");
		var ary;
		var allowance = [];
		var money     = document.getElementsByName("money");
		var isHandTax = [];
		var radioVal  = [];
		var taxMoney  = [];
		var isTransfer = [];
		for(var i=0;i<data.length;i++){
			ary=data[i].split(",");
			isHandTax[ary[0]] = document.getElementsByName("isHandTax["+ary[0]+"]");
			for(var j=0; j < isHandTax[ary[0]].length; j++){
				if(isHandTax[ary[0]][j].checked ){
					radioVal[i] = isHandTax[ary[0]][j].value;
				}
			}

			isTransfer[ary[0]] = ($('[name="isTransfer['+ary[0]+']"]').prop('checked')) ? '1' : '0';

			// 塞 扣稅金額的值到陣列
			if($(isHandTax[ary[0]]).closest('td').next('td').find('input').val()){
				taxMoney[i] = $(isHandTax[ary[0]]).closest('td').next('td').find('input').val();
			}else{
				taxMoney[i] = $(isHandTax[ary[0]]).closest('td').next('td').html();
			}
			// console.log(taxMoney[i]);
		}

		

		var ck=true;
		for(var i=0;i<data.length;i++){
			if(isNaN($(money[i]).val())){	alert("數量請輸入單價");	$(money[i]).focus(); ck=false;break;}
			ary=data[i].split(",");
			// console.log(handTax[i]);
			allowance.push(ary[0]+","+bcID+","+bc_title+","+$(money[i]).val()+","+radioVal[i]+","+taxMoney[i]+","+isTransfer[ary[0]]);
		}

		console.log(allowance);

		if(!ck)	return;
		$.ajax({ 
		  type: "POST", 
		  url: "func.php", 
		  data: "act=doAllowance&data="+allowance.join("#")+"&bID="+bID
		}).done(function( rs ) {
			// console.log(rs);
			alert("設定成功");
			// location.reload();
			hide('d_setting');
		}).fail(function( msg ) { 
		});
	}
	function hide(id){
		$('#'+id).hide();
	}
	
	function doSubmit() {
		form1.submit();
	}
	function uploadfile(event){
		console.log(event);
		files = event.target.files;
		console.log(files);
	}
	
	

 </script>
 <script Language="JavaScript" src="list.js"></script> 
</Head>

<body>


<div id="popup" class="popupPane" style="display: none;">
<form id="fm" action="importExcel.php" method="post" enctype="multipart/form-data" style="margin:0">
  <input type="hidden" name="fid" value="<?php echo $fid?>"/>
  <table bgcolor="#F0F0F0" style="font-size:12px">
  <tr><td>選擇要匯入的Excel檔：<br/><span style="color:#666666; font-size:12px">請選擇格式為 xls 的 excel 檔案，內容必須遵守範本的排法。</span><br/><a href="templet.xls" target="new">下載範本</a></td></tr>
  <tr><td><input type="file" name="file1" id="file1" size="30" /></td></tr>
  <tr height="40" valign="bottom"><td>
  	<input type="submit" name="Submit" onclick="excel();return false;" value="匯入">
    <button type="button" onClick="document.getElementById('popup').style.display='none'">取消</button>
  </td></tr>
  </table>
</form>
</div>

<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" enctype="multipart/form-data" style="margin:0">
	<!-- 驗證 -->
	<!-- <input type="hidden" name="yearBK" value="<?php echo $_REQUEST['year']?>">
	<input type="hidden" name="monthBK" value="<?php echo $_REQUEST['month']?>"> -->

 <table width="95%">
  <tr><td><font class="headTX"><?php echo $pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?php echo $pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?php echo $pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    	<input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?php echo $bE?'sBtn':'suBtn'?>" onclick='doAppend()'><input type="button" name="edit" value="－修改" class="<?php echo $bE?'sBtn':'suBtn'?>" onClick="DoEdit()">
    	<input type="submit" name="action" value="Ｘ刪除" class="<?php echo $bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
    	
    	
    	<select name="year" id="year" onChange="doSubmit()"><?php for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    	<select name="month" id="month" onChange="doSubmit()"><?php for($i=1;$i<13;$i++) { $sel=($curMh==$i?'selected':''); echo "<option $sel value='$i'>$i 月</option>"; } ?></select>

    	<!-- <input type="button" value="匯入excel" id="file" onClick="document.getElementById('popup').style.display='block'"> -->
    	
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><?php $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?php echo $ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<?php for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?php echo $fnAry[$i]?>","<?php echo $sortFG?>","<?php echo $keyword?>");' class="head">
		<?php echo $ftAry[$i]?>&nbsp;&nbsp;<?php if ($fieldname==$fnAry[$i]) { ?><?php if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?><?php } ?>
		</a>
	</th>
	<?php } ?>
  </tr>
<?php

	//過濾條件
	$sql = 'select b.bID,b.title,b.depID,b.empID,b.wkMonth,b.isTax,b.taxOver,b.taxNum,bc.title as bc_title,bc.bcID,bc.money ';
	$sql .= ' from '.$tableName.' as b left join '.$jointableName.' as bc on b.bcID=bc.bcID ';
	

	if(isset($_REQUEST['year']) && $_REQUEST['year']!=''){
		$sql .= ' where b.wkMonth="'.$_REQUEST['year'].str_pad($_REQUEST['month'],2,'0',STR_PAD_LEFT).'"';
	}else if(isset($_REQUEST['ybk']) && $_REQUEST['ybk']!='' && isset($_REQUEST['mbk']) && $_REQUEST['mbk']!=''){
		// $sql .= ' where 1';
		$sql .= ' where b.wkMonth="'.$_REQUEST['ybk'].str_pad($_REQUEST['mbk'],2,'0',STR_PAD_LEFT).'"';
	}else{
		$sql .= ' where b.wkMonth="'.date('Y').date('m').'"';
	}

	// 搜尋處理
	if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%'))";
	// 排序處理
	if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
	$sql .= " limit ".$pageSize*$pages.",$pageSize";
	
	$rs = db_query($sql);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]]; 
?>
  <tr valign="top" id="tr_<?php echo $id?>" style='cursor:pointer;' onMouseOver="SelRowIn('<?php echo $id?>',this)" onMouseOut="SelRowOut('<?php echo $id?>',this)" onClick="SelRow('<?php echo $id?>',this,'<?php echo $r['uType']?>')"<?php if($id==$selid) echo ' class="onSel"'?>>
    <td <?php if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?php echo $id?>" name="ID[]" <?php if (!empty($nextfg)) echo "checked" ?>>
            <a href="javascript: clickEdit('<?php echo $id?>')"><?php echo $id?></a>
    </td>
		<?php for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <?php if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<?php $fldValue = isset($r[$fnAry[$i]]) ? $r[$fnAry[$i]] : ''; ?>
			<?php 
				switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;
						case "copy" : 
							echo '<input type="button" value="複製" onClick="copy('.$r['ofID'].');">';
							break;
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						default :	
							if ($fnAry[$i]=="state"){	
								echo $state[$fldValue];	
							}else if($fnAry[$i]=="bcID"){
								echo $r['bc_title'];
							}else if($fnAry[$i]=="action"){
									echo "<input type='button' value='編輯' onclick='detail(\"".$r['empID']."\",\"".$r['depID']."\",\"".$r['money']."\",".$r['bcID'].",\"".$r['bc_title']."\",".$r['bID'].",\"".$r['wkMonth']."\")'/>";
							}else{	echo $fldValue;	}
							break;
				}
			?>
		</td>
		<?php } ?>	
  </tr>    
<?php } ?>   
 </table>
</form>
<div id='d_setting' class='divBorder1'>
	<div class='divTitle'>設定產品資訊</div>
	<div class='divBorder2'>
		<div id='d_allowance' title='津貼設定'></div>
	</div>
	<div id='d_bottom' style='text-align:center'></div>

</div>
<div align="right"><a href="#top">Top ↑</a></div>
</body>
</Html>