<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
	//抓全部的emplyee
	$emp_list = array();
	$sql1 = "select * from `emplyee` where `isOnduty`=1";
	$rs1 = db_query($sql1, $conn);
	while ($r1 = db_fetch_array($rs1)) {
		$emp_list[$r1['empID']] = $r1['depID'];
	}

  	$sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	if ($_SESSION['privilege'] <= 10){
		$readonly='readonly';
		$class='readonly';
	}else{
		$readonly='';	
		$class='';
	}
	// echo '<pre>';
	// print_r($_SESSION['user_classdef'][2]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 .tip{ color:#666;}
.readonly{	color:#999;}
 </style>
<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 編輯</title>
 <script type="text/javascript">
	function formvalid(frm){
		if($('[name="title"]').val()==""){	alert("請輸入名稱");return false;}

		if($("input[name='isTax[]']:checked").val() == 1 && $("input[name='taxOver']").val() == ''){
			alert("請輸入超過的金額");
			return false;
		}
		if($("input[name='isTax[]']:checked").val() == 1 && $("input[name='taxNum']").val() == ''){
			alert("請輸入扣稅百分比");
			return false;
		}

	 	var awcIDs=document.getElementsByName("bcID[]");
	 	var ck=false;
	 	for(var i=0;i<awcIDs.length;i++){
	 		if(awcIDs[i].checked)	ck=true;
	 	}
	 	if(!ck){	alert("請選擇類別");return false;}
		return true;
	}
	$(document).ready(function() {
		// console.log($("input[name='isTax[]']:checked").val());
		// $("input[name='taxOver']").val(0);
		// $("input[name='taxNum']").val(0);

		// $("input[name='isTax[]']").change(function() {
		// 	if(this.value == 0){
		// 		// $("input[name='taxOver']").attr('disabled', true);
		// 		// $("input[name='taxNum']").attr('disabled', true);
		// 		$("input[name='taxOver']").val(0);
		// 		$("input[name='taxNum']").val(0);
		// 		// console.log(this.value);
		// 	}else{
		// 		// $("input[name='taxOver']").attr('disabled', false);
		// 		// $("input[name='taxNum']").attr('disabled', false);
		// 		$("input[name='taxOver']").val('');
		// 		$("input[name='taxNum']").val('');
		// 		// console.log(this.value);
		// 	}
		// });
	});
	
</script>
</Head>
<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<? for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? 
		switch($editetA[$i]) { 
  	  		case "text" :echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  			case "checkbox": 
  				$sql="select * from bonus_class";
				$rs2=db_query($sql);
				$awcAry=array();
				if(!db_eof($rs2)){
					$j=1;
					while($r2=db_fetch_array($rs2)){
						$awcAry[$r2[bcID]]=$r2[title];
						echo "<input type='checkbox' name='".$editfnA[$i]."[]' class='input'"; if ($r[$editfnA[$i]]==$r2[bcID]) echo " checked"; echo " value='true' disabled><span class='tip'>".$r2[title]."</span>"; 
						if($j%3 == 0)	echo "<p>";
						$j++;
					}
				}
  				break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  :echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;				 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "radiobutton":
				echo "<input type='radio' name='".$editfnA[$i]."[]' class='input'"; if ($r[$editfnA[$i]]==1) echo " checked";  echo " value='1' ><span class='tip'>".'是'."</span>"; 
				echo "<input type='radio' name='".$editfnA[$i]."[]' class='input'"; if ($r[$editfnA[$i]]==0) echo " checked";  echo " value='0' ><span class='tip'>".'否'."</span>"; 
				break;
			case "taxtext" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."' pattern=[0-9]*>" .'元'; break;
			case "taxpercentage" : echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='1' class='input' value='".$r[$editfnA[$i]]."' pattern=[0-9]*>" .'%';  break;

			case "date" : 
				break;
			default:	echo $r[$editfnA[$i]];	break;
			
  	} ?></td>
  </tr>
  <? 
  } 
  
  ?>


  <tr valign="top">
	<td align="right" class="colLabel">選取部門及員工</td>
	<td>
		<ul class='dep-emp-ul'>
			　<li class='dep-emp-li'><div class="queryID <?=$class?>" id="depID_other"></div></li>
			　<li class='dep-emp-li'>
				<div class='query-title'>部門</div>
				<div class="depID_list">
			    	<? 
				    	if($r['depID']!=''){
				    		$depAry=explode(',',$r['depID']); 
				    		echo '<ul>';
					    	for($i=0; $i<count($depAry); $i++) {
					    		//前台需要判斷權限
					    		if(isset($_SESSION['user_classdef'][2])){
					    			if(in_array($depAry[$i],$_SESSION['user_classdef'][2])){
					    				echo '<li><img src="/images/remove.png" width="15" align="top" onclick="d_remove(this)"><input type="hidden" name="depID[]" value="'.$depAry[$i].'">'.$departmentinfo[$depAry[$i]].'</li>';	
					    			}else{
					    				$now_privilege = 0;
					    				foreach($_SESSION['user_classdef'][2] as $k2=>$v2){//判斷子層有沒有權限
					    					if(strpos($depAry[$i],$v2)!==false){
					    						$now_privilege = 1;
					    					}
					    				}
					    				if($now_privilege == 0){
					    					echo '<li style="display:none"><input type="hidden" name="depID[]" value="'.$depAry[$i].'">'.$departmentinfo[$depAry[$i]].'</li>';
					    				}else{
					    					echo '<li><img src="/images/remove.png" width="15" align="top" onclick="d_remove(this)"><input type="hidden" name="depID[]" value="'.$depAry[$i].'">'.$departmentinfo[$depAry[$i]].'</li>';
					    				}
					    				
					    			}

					    		}else{
					    			echo '<li><img src="/images/remove.png" width="15" align="top" onclick="d_remove(this)"><input type="hidden" name="depID[]" value="'.$depAry[$i].'">'.$departmentinfo[$depAry[$i]].'</li>';	
					    		}
					    		
							} 
							echo '</ul>';
						}else{
							echo 'No Data';
						}
					?>
				</div>
			</li>
			<li class='dep-emp-li'>
				<div class='query-title'>員工</div>
				<div class="empID_list">
					<? 
						if($r['empID']!=''){
							$depAry=explode(',',$r['empID']);
				    		echo '<ul>';
					    	for($i=0; $i<count($depAry); $i++) {
					    		if(isset($_SESSION['user_classdef'][2])){
					    			
				    				$now_privilege = 0;//判斷有沒有權限
				    				foreach($_SESSION['user_classdef'][2] as $k2=>$v2){//判斷子層有沒有權限
				    					if(strpos($emp_list[$depAry[$i]],$v2)!==false){
				    						$now_privilege = 1;
				    					}
				    				}
				    				if($now_privilege == 0){
				    					echo '<li style="display:none"><input type="hidden" name="empID[]" value="'.$depAry[$i].'">'.$emplyeeinfo[$depAry[$i]].'</li>';	
				    				}else{
				    					echo '<li><img src="/images/remove.png" width="15" align="top" onclick="d_remove(this)"><input type="hidden" name="empID[]" value="'.$depAry[$i].'">'.$emplyeeinfo[$depAry[$i]].'</li>';	
				    				}
					    		}else{
					    			echo '<li><img src="/images/remove.png" width="15" align="top" onclick="d_remove(this)"><input type="hidden" name="empID[]" value="'.$depAry[$i].'">'.$emplyeeinfo[$depAry[$i]].'</li>';	
					    		}


					    		
							} 
							echo '</ul>';	
						}else{
							echo 'No Data';
						}
					?>
				</div>
			</li>
		</ul>
	</td>
  </tr>


 <?	

 if($r[state]==1){?>
  <tr><td colspan='2' align='center' class="rowSubmit"><input type="button" value="取消編輯" class="btn" onClick="history.back()"> 已套用設定，無法再修改</td></tr>
 <?	}else{	?>
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
    <input type="hidden" name="startTime" id="startTime"/>
    <input type="hidden" name="endTime" id="endTime"/>
    <input type="hidden" name='week_day_v' id='week_day_v'>
    <input type="hidden" name='tData' id='tData'>
	<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <input type="button" value="取消編輯" class="btn" onClick="history.back()">&nbsp;
  </td>
</tr>
<?	}	?>
</table>

</form>
</body>
</html>
