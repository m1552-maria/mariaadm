<?php 
	if(empty($_REQUEST['id'])) exit;

	$handList = (isset($_REQUEST['handList'])) ? $_REQUEST['handList'] : array();
	$payList = (isset($_REQUEST['payList'])) ? $_REQUEST['payList'] : array();
	$empList = (isset($_REQUEST['empList'])) ? $_REQUEST['empList'] : array();
	$otherList = (isset($_REQUEST['otherList'])) ? $_REQUEST['otherList'] : array();
	$otherNotesList = (isset($_REQUEST['otherNotesList'])) ? $_REQUEST['otherNotesList'] : array();


	$result = array('result'=>1, 'id'=>$_REQUEST['id']);
	include("../../config.php");
	include("yearEndCalculate.php");

	$auditList = array();
	foreach ($_REQUEST['auditList'] as $key => $value) {
		$auditList[$key]['score'] = $value;
	}

	//$sql = "select score,empID from emplyee_bonus_year_end where score is not null and score <> '' and bfID='".$_REQUEST['id']."'";
	$sql = "select * from emplyee_bonus_year_end where bfID='".$_REQUEST['id']."'";
	$rs = db_query($sql);
	$oldEmpList = array();
	while($r=db_fetch_array($rs)) {
		$oldEmpList[$r['empID']] = $r['empID'];
		/*if(empty($auditList[$r['empID']]['score']) && $r['score'] != '') {
			$auditList[$r['empID']]['score'] = $r['score'];
		}*/

		/*加入資料表內 是否手動更新*/
		/*if(empty($handList[$r['empID']])) {
			$handList[$r['empID']]['isHand'] = 0;
			if($r['isHand'] == 1) {
				$handList[$r['empID']]['isHand'] = 1;
				$handList[$r['empID']]['bonus'] = $r['bonus'];
				$handList[$r['empID']]['tax'] = $r['tax'];
				$handList[$r['empID']]['money'] = $r['money'];
			}
		}*/

		/*加入資料表內 是否不發放*/
		/*if(!isset($payList[$r['empID']])) {
			$payList[$r['empID']] = $r['isPayHand'];
		}*/
	}

	$yearEndData = yearEndCalculate($_REQUEST['id'], $auditList);

	if(isset($yearEndData['error'])) exit;
	//重新計算部分年終
	/*$sql="delete from emplyee_bonus_year_end where bfID='".$_REQUEST['id']."'";
	db_query($sql);*/

	foreach ($empList as $key => $value) {
		if(empty($oldEmpList[$key])) continue;
		if(empty($yearEndData[$key])) continue;

		$fields = array();
		//$fields[] = "empID ='".$key."'";
		$fields[] = "jobClass ='".$yearEndData[$key]['jobClass']."'";
		if($yearEndData[$key]['score'] != '') $fields[] = "score ='".$yearEndData[$key]['score']."'";
		$fields[] = "salary ='".(($yearEndData[$key]['setSalary'] == 1) ? $yearEndData[$key]['salaryYear'] : $yearEndData[$key]['salary'])."'";
		if($yearEndData[$key]['base'] != '') $fields[] = "base ='".$yearEndData[$key]['base']."'";
		$fields[] = "wkYear ='".$yearEndData[$key]['wkYear']."'";
		$fields[] = "cTax ='".$yearEndData[$key]['tax']."'";
		$fields[] = "sRatio ='".$yearEndData[$key]['serviceRatio']."'";
		$fields[] = "cBonus ='".$yearEndData[$key]['pay']."'";
		if($yearEndData[$key]['pay'] > -1) $fields[] = "cMoney ='".($yearEndData[$key]['pay']-$yearEndData[$key]['tax'])."'";
		//$fields[] = "bfID ='".$_REQUEST['id']."'";
		$fields[] = "projID ='".$yearEndData[$key]['projID']."'";
		$fields[] = "stopRange ='".implode(',', $yearEndData[$key]['stopRange'])."'";
		$fields[] = "actDays ='".$yearEndData[$key]['actDays']."'";
		$fields[] = "isAssess ='".$yearEndData[$key]['isAssess']."'";
		$fields[] = "isPay ='".$yearEndData[$key]['isPay']."'";

		$money = -1;
		$moneyChange = 0;
		if(isset($payList[$key])) {
			$money = 0;
			$fields[] = "isPayHand ='0'";
			$fields[] = "tax ='0'";
			$fields[] = "bonus ='0'";
			$fields[] = "`notes` ='手動不發放'";
			$moneyChange++;
		} else {
			$fields[] = "isPayHand ='1'";
		}

		if(isset($handList[$key]['isHand'])) {
			$fields[] = "isHand ='1'";
			if($moneyChange < 1) {
				$money = (int)$handList[$key]['money'];
				$fields[] = "notes ='手動調整年終獎金'";
				$fields[] = "tax ='".(int)$handList[$key]['tax']."'";
				$fields[] = "bonus ='".(int)$handList[$key]['bonus']."'";
				$moneyChange++;
			}
		} else {
			$fields[] = "isHand ='0'";
		} 

		if($moneyChange == 0) {
			$fields[] = "notes ='".$yearEndData[$key]['type']."'";
			$fields[] = "tax ='".$yearEndData[$key]['tax']."'";
			$fields[] = "bonus ='".$yearEndData[$key]['pay']."'";
			if($yearEndData[$key]['pay'] > -1) $money = $yearEndData[$key]['pay']-$yearEndData[$key]['tax'];
		}

		if(isset($otherList[$key]) && $money != -1) {
			if((int)$otherList[$key] > $money)  $otherList[$key] = $money;
			$money -= (int)$otherList[$key];
			$fields[] = "otherDeduct ='".(int)$otherList[$key]."'";
		} else {
			$fields[] = "otherDeduct ='0'";
		}

		if(isset($otherNotesList[$key])) {
			$fields[] = "otherDeductNotes ='".$otherNotesList[$key]."'";
		} else {
			$fields[] = "otherDeductNotes =''";
		}
	
		if($money != -1) $fields[] = "money ='".$money."'";
		//$sql = "insert into emplyee_bonus_year_end set ".implode(',', $fields);
		$sql = "update emplyee_bonus_year_end set ".implode(',', $fields)." where empID='".$key."' and bfID='".$_REQUEST['id']."'";

		db_query($sql);
	}

	echo json_encode($result);
?>