<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
	$sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	if ($_SESSION['privilege'] <= 10){
		$readonly='readonly';
		$class='readonly';
	}else{
		$readonly='';	
		$class='';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0">
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 .tip{ color:#666;}
.readonly{	color:#999;}
.colLabel {
	background-color: #F0F0F0;
	border-top-style: none; 
    border-right-style: none; 
}
 </style>
 <link href="grade.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 編輯</title>
 <script type="text/javascript">
 	$(function(){
 		$('[name="title"],[name="base"],[name="deduct_base"],[name="taxRatio"]').prop('required',true);
	 	$('[name="base"],[name="taxRatio"]').attr('pattern',"^[0-9]+(.[0-9]{1,1})?$");
	 	$('[name="base"],[name="taxRatio"]').attr('title',"請輸入數字最多到小數點第一位");
	 	$('[name="deduct_base"]').attr('pattern',"^\\+?[1-9][0-9]*$");
	 	$('[name="deduct_base"]').attr('title',"請輸入整數");
 	})
	function formvalid(frm){
		//if($('[name="title"]').val()==""){	alert("請輸入名稱");return false;}
	 	return true;
	}
	
</script>
</Head>
<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<? for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? 
		switch($editetA[$i]) { 
  	  		case "text" :echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  			case "checkbox": 
  				
  				break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  :echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;				 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				$value = ($r[$editfnA[$i]] != '') ? date('Y/m/d',strtotime($r[$editfnA[$i]])) : '';
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".$value."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";				
				break;

			default:	echo $r[$editfnA[$i]];	break;
			
  	} ?></td>
  </tr>
  <? 
  } 
  
  ?>
<?php if($_REQUEST['type'] == 'year_end') { ?>
	<?php
		$noData = array();
		$sql = "select * from bonus_festival_basic where bfID='".$ID."' and className='no' order by id";
		$rsNo = db_query($sql, $conn);
		while ($rNo = db_fetch_array($rsNo)) $noData[$rNo['object']] = $rNo['depiction'];
	?>
	<tr>
  		<td colspan="2" class="colLabel" align="center">
  			<div style="margin-top:10px;width:840px;">
	  			<h3 style="float: left;">不發放對象 : </h3>
	  			<table class="yearTab" cellpadding="0" cellspacing="0">
	  				<tr>
	  					<th>對象</th>
					    <th>條件</th>					    
					</tr>
					<tr>
						<td>離職</td>
						<td>
							於日期
							<input style="width:150px" name="no[leave]" type="text" class="queryDate" id="no[leave]" size="10" value="<?php echo $noData['leave'];?>" required pattern="[0-9]{4}/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01])" title="ex:2017/12/31">前離職者
						</td>
					</tr>
					<tr>
						<td>年資</td>
						<!--<td>
							未滿<input type="text" name="no[year]" style="width:80px;" required pattern="^\+?[1-9][0-9]*$" title="請輸入正整數" value="<?php echo $noData['year'];?>">月
						</td>-->
						<td>
							於日期
							<input style="width:150px" name="no[hireDate]" type="text" class="queryDate" id="no[hireDate]" size="10" value="<?php echo $noData['hireDate'];?>" required pattern="[0-9]{4}/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01])" title="ex:2017/12/01">後到職者
						</td>
					</tr>
					<tr>
						<td>事假</td>
						<td>
							全年度請事假超過<input type="text" name="no[thing]" style="width:80px;" required pattern="^\+?[1-9][0-9]*$" title="請輸入正整數" value="<?php echo $noData['thing'];?>">(含)日
						</td>
					</tr>
	  			</table>
	  		</div>
	  	</td>
	</tr>

	<tr>
		<td colspan="2" class="colLabel" align="center">
  			<div style="margin-top:10px;width:840px;">
	  			<h3 style="float: left;">發放基數設定 : </h3>
	  			<table id="assessTable" class="yearTab" cellpadding="0" cellspacing="0">
	  				<tr>
					    <th class="lineTh">
					    	<div class="out">
					    		<b>考核等級</b>
					    		<span class="line"></span>
					    		<em>職銜</em>
					    	</div>
					    </th>
					    <th>優</th>
					    <th>佳</th>
					    <th>可</th>
					    <th>差</th>
					    <th width="80px"></th>
					</tr>
	<?php 
		//取得發放基數設定
		$sql = "select * from bonus_festival_basic where bfID='".$ID."' and className='assess' order by id";
		$rsAss = db_query($sql, $conn);
		$i=0;
		while ($rAss = db_fetch_array($rsAss)) {
			$job = explode(',', $rAss['object']);
			foreach ($job as $key => $value) {
				$job[$key] = $jobinfo[$value];
			}
			$depiction = explode(',', $rAss['depiction']);
			$depiction[0] = explode(':',$depiction[0]);
			$depiction[1] = explode(':',$depiction[1]);
			$depiction[2] = explode(':',$depiction[2]);
			$depiction[3] = explode(':',$depiction[3]);
	?>
					<tr data-num="<?php echo $i;?>">
						<td><?php echo ($i==0) ? '一般員工' : implode('<br>', $job);?></td>
						<td><input type="text" name="grade4[<?php echo $i;?>]" value="<?php echo $depiction[0][1];?>" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td><input type="text" name="grade3[<?php echo $i;?>]" value="<?php echo $depiction[1][1];?>" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td><input type="text" name="grade2[<?php echo $i;?>]" value="<?php echo $depiction[2][1];?>" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td><input type="text" name="grade1[<?php echo $i;?>]" value="<?php echo $depiction[3][1];?>" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td>
							<input type="hidden" name="jobID[<?php echo $i;?>]" value="<?php echo $rAss['object'];?>" data-job>
							<?php if($i==0) { ?>
							<a href="javascript:void(0)" onclick="openWindow('assess')">新增</a>
							<?php } else { ?>
							<a style="color:red;" href="javascript:void(0)" onclick="gradeDel(this)">刪除</a>
							<?php } ?>
						</td>
					</tr>
	<?php 
			$i++;
		} 
	?>
	  			</table>
	  		</div>
  			
  		</td>
	</tr>
	<tr>
  		<td colspan="2" class="colLabel" align="center">
  			<div id="specialDiv" style="margin-top:10px;width:840px;">
	  			<h3 style="float: left;">特殊發放規則 :<a href="javascript:void(0)" onclick="openSpecial();">新增</a> </h3>
	<?php 
		//取得發放基數設定
		$sql = "select * from bonus_festival_basic where bfID='".$ID."' and className='special' order by id";
		$rsSpe = db_query($sql, $conn);
		$i=0;
		while ($rSpe = db_fetch_array($rsSpe)) {
			$job = explode(',', $rSpe['object']);
			foreach ($job as $key => $value) {
				$job[$key] = $jobClass[$value];
			}
			$depiction = explode(',', $rSpe['depiction']);
			foreach ($depiction as $key => $value) {
				$depiction[$key] = explode(':', $value);
			}
	?>
				<table data-num="<?php echo $i;?>" class="yearTab specialTab" cellpadding="0" cellspacing="0">
					<tr>
						<th class="lineTh"><div class="out"><b>年資</b><span class="line"></span><em>職銜</em></div></th>
		<?php foreach ($depiction as $k => $v) { ?>
						<th><?php echo $years[$v[0]];?><input type="hidden" name="sJob[<?php echo $i;?>][year][<?php echo $v[0];?>]" value="<?php echo $v[0];?>"></th>
		<?php } ?>
						<th width="80px"></th>
					</tr>
					<tr>
						<td><?php echo implode('<br>', $job);?></td>
		<?php foreach ($depiction as $k => $v) { ?>	
						<td>
							<input type="text" name="sJob[<?php echo $i;?>][num][<?php echo $v[0];?>]" value="<?php echo $v[1];?>" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字" style="width:80px;">
							<select name="sJob[<?php echo $i;?>][unit][<?php echo $v[0];?>]"><option value="dollars" <?php if($v['2']=='dollars') echo 'selected';?>>台幣</option><option value="month" <?php if($v['2']=='month') echo 'selected';?>>月</option><option value="average" <?php if($v['2']=='average') echo 'selected';?>>年度平均薪</option></select>
						</td>	
		<?php } ?>	
						<td>
							<input type="hidden" name="sJob[<?php echo $i;?>][id]" value="<?php echo $rSpe['object'];?>" data-sjob>
							<a style="color:red;" href="javascript:void(0)" onclick="specialDel(this)">刪除</a>
						</td>
					</tr>
				</table>
	<?php
			$i++;
		}
	?>
	  		</div>
  		</td>
  	</tr>
<?php } ?>


 
<tr>
	<td colspan="2" align="center" class="rowSubmit">
	<input type="hidden" name="type" id="type" value="<?=$_REQUEST['type']?>" />
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
<?php if($r['isClose'] == 0) { ?>    
    <input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
<?php } ?> 
    <input type="button" value="取消編輯" class="btn" onClick="history.back()">&nbsp;
  </td>
</tr>
</table>

</form>
<div id='d_setting' class='divBorder1'>
	<div class='divTitle'>年資條件<input type="button" value="取消" style="float:right;cursor:pointer;margin-left:5px;" onclick="$('#d_setting').hide()"><input type="button" value="確定" onclick="selectYear()" style="float:right;cursor:pointer;"></div>
	<div class='divBorder2'>
	  <div class="divf1">
		<h4>選擇類型:</h4>
		<ul class="browser">
<?php
	foreach ($jobClass as $key => $value) {
		echo "<li><label><input type='checkbox' name='jClass[]' value='".$key."' data-name='".$value."'>$value</label></li>";
	}

?>	
		</ul>
	  </div>
	  <div class="divf2">
		<h4>選擇年資:</h4>
		<ul class="browser">
<?php 
    	foreach ($years as $key => $value) {
    		echo "<li><label><input type='checkbox' name='year[]' value='".$key."' data-name='".$value."'><span class='spanIm'></span><a>$value</a></label></li>"; 
    	}
?>
		</ul>
	  </div>
	</div>

</div>
<script type="text/javascript" src="grade.js"></script>
<script type="text/javascript">
	$(function(){
		$('#d_setting').draggable();
	});
</script>
</body>
</html>
