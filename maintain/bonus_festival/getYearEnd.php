<?php
	if(empty($_POST['id'])) exit;
	if(!isset($_SESSION)) session_start();
	$result = array('taxRatio'=>0, 'deductBase'=>0, 'emp'=>array());
	include("../../config.php");

	$sql = "select taxRatio,deduct_base from bonus_festival where id='".$_POST['id']."'";
	$rs = db_query($sql);
	$r=db_fetch_array($rs);

	$result['taxRatio'] = $r['taxRatio'];
	$result['deductBase'] = $r['deduct_base'];
	
	$fields = array("b.bfID='".$_POST['id']."'");

	if(!empty($_POST['depID'])) {
		$fields[] = "e.depID like '".$_POST['depID']."%'";
	} elseif($_SESSION['privilege'] <= 10) {
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="e.depID like '$v%'";
			$fields[] = "(".join(' or ',$aa).")"; 
		} else {
			$fields[] = "e.depID=''";
		}
	}
	if(!empty($_POST['emp'])) $fields[] = "(e.empID='".$_POST['emp']."' or e.empName like '%".$_POST['emp']."%')";

	$sql = "select e.empName,b.* from emplyee e inner join emplyee_bonus_year_end b on e.empID=b.empID where ".implode(' and ', $fields)." order by e.empID";


	$rs = db_query($sql);
	//if(db_num_rows($rs) == 0) exit;
	$i = 0;
	while ($r=db_fetch_array($rs)) {
		$result['emp'][$i]['empID'] = $r['empID'];
		$result['emp'][$i]['name'] = $r['empName'];
		$result['emp'][$i]['isAssess'] = $r['isAssess'];
		$result['emp'][$i]['score'] = $r['score'];
		$result['emp'][$i]['assess'] = $r['notes'];
		$result['emp'][$i]['tax'] = ($r['tax'] == '') ? '' : (int)$r['tax'];
		$result['emp'][$i]['bonus'] = ($r['bonus'] < 0) ? '' : (int)$r['bonus'];
		$result['emp'][$i]['money'] = ($r['money'] == '') ? '' : $r['money'];
		$result['emp'][$i]['isHand'] = $r['isHand']; 
		$result['emp'][$i]['isPayHand'] = ($r['isPay'] == 1) ? $r['isPayHand'] : -1; 
		$result['emp'][$i]['otherDeduct'] = ($r['otherDeduct'] != '') ? $r['otherDeduct'] : 0;
		$result['emp'][$i]['otherDeductNotes'] = ($r['otherDeductNotes'] != '') ? $r['otherDeductNotes'] : '';
		$i++;
	}

	echo json_encode($result);

?>