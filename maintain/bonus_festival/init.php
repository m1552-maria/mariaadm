<?	
	session_start();
    header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10 && $_REQUEST['type'] == 'festival') {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');
	include('../../getEmplyeeInfo.php');
	include('../inc_vars.php');
	$bE = true;	//異動權限


	$tableName = "bonus_festival";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bonus/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;

	//類別
	if($_REQUEST['type'] == 'festival'){
		$pageTitle = "三節獎金設定";
		// Here for List.asp ======================================= //
	
		$defaultOrder = "id";
		$searchField1 = "title";

		$pageSize = 20;	//每頁的清單數量

		$state=array("0"=>"否","1"=>"是");
		// 注意 primary key should be first one
		$fnAry = explode(',',"id,YEAR,title,festivalDate,giveDate,isClose");
		$ftAry = explode(',',"編號,年度,標題,節日日期,發放日期,關帳");
		$flAry = explode(',',"50,,,,");
		$ftyAy = explode(',',"id,text,text,date,date,text");
		
		
		// Here for Append.asp =========================================================================== //
		$newfnA = explode(',',"title,festivalDate,giveDate,notes");
		$newftA = explode(',',"標題,節日日期,發放日期,說明");
		$newflA = explode(',',",,,50");
		$newfhA = explode(',',",,,5");
		$newfvA = array('','','');
		$newetA = explode(',',"text,date,date,textbox");
		
		// Here for Edit.asp ============================================================================= //
		$editfnA = explode(',',"id,title,festivalDate,giveDate,notes");
		$editftA = explode(',',"編號,標題,節日日期,發放日期,說明");
		$editflA = explode(',',",,,,50");
		$editfhA = explode(',',",,,,5");
		$editfvA = array('','','','','');
		$editetA = explode(',',"text,text,date,date,textbox");
	}elseif($_REQUEST['type'] == 'year_end'){
		$pageTitle = "年終獎金設定";

		$sql = "select year from emplyee_annual group by year order by year desc";
		$rs = db_query($sql, $conn);
		$r = db_fetch_array($rs);


		// Here for List.asp ======================================= //
		$annualY = (int)$r['year'] - 1;
		$yearList = array();

		$sql = "select 1 from bonus_festival where type='year_end' and wkYear='".$annualY."'";
		$rs = db_query($sql, $conn);
		if(db_num_rows($rs) == 0) $yearList[$annualY] = $annualY;
	
		$sql = "select 1 from bonus_festival where type='year_end' and wkYear='".($annualY-1)."'";
		$rs = db_query($sql, $conn);
		if(db_num_rows($rs) == 0) $yearList[$annualY-1] = $annualY-1;

		$wkYearList = array();
		$sql = "select wkYear from bonus_festival where type='year_end' group by wkYear order by wkYear desc";
		$rs = db_query($sql, $conn);
		while ($r = db_fetch_array($rs)) {
			$wkYearList[$r['wkYear']] = $r['wkYear'];
		}
		$wkYear = (count($wkYearList) > 0) ? key($wkYearList) : '';
		if(isset($_REQUEST['wkYearSelect'])) $wkYear = $_REQUEST['wkYearSelect'];

		$defaultOrder = "id";
		$searchField1 = "title";

		$pageSize = 20;	//每頁的清單數量

		$state=array("0"=>"否","1"=>"是");
		// 注意 primary key should be first one
		$fnAry = explode(',',"id,title,wkYear,base,deduct_base,taxRatio,action,giveDate,isClose");
		$ftAry = explode(',',"編號,標題,計算年度,核定基數,所得稅的起扣金額,所得稅扣除%數,年終審核,發放日期,關帳");
		$flAry = explode(',',"50,,,,,,,,");
		$ftyAy = explode(',',"id,text,text,text,text,text,text,date,text");
		
		
		// Here for Append.asp =========================================================================== //
		$newfnA = explode(',',"title,wkYear,base,deduct_base,taxRatio,giveDate,notes");
		$newftA = explode(',',"標題,計算年度,核定基數,所得稅的起扣金額,所得稅扣除%數,發放日期,備註");
		$newflA = explode(',',",,,,,,50");
		$newfhA = explode(',',",,,,,,5");
		$newfvA = array('',$yearList,'','','','','');
		$newetA = explode(',',"text,select,text,text,text,date,textbox");
		
		// Here for Edit.asp ============================================================================= //
		$editfnA = explode(',',"id,title,wkYear,base,deduct_base,taxRatio,giveDate,notes");
		$editftA = explode(',',"編號,標題,計算年度,核定基數,所得稅的起扣金額,所得稅扣除%數,發放日期,備註");
		$editflA = explode(',',",,,,,,,50");
		$editfhA = explode(',',",,,,,,,5");
		$editfvA = array('','',$yearList,'','','','','');
		$editetA = explode(',',"text,text,readonly,text,text,text,date,textbox");

		//年終設定需要
		//$jobClass['hour'] = '鐘點制人力';
		$years = array(
			'all' => '忽略年資',
			'0'	  => '未滿一年',	
			'1'   => '一年以上',
			'2'	  => '兩年以上',
			'3'	  => '三年以上',
			'4'	  => '四年以上',
			'5'   => '五年以上'
		);
		$gradeList = array('grade4'=>'優','grade3'=>'佳','grade2'=>'可','grade1'=>'差');
	}

?>