<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <style type="text/css">
 
.popupPane {
	position:absolute; 
	left: 25%; top:5%;
	background-color:#888;
	padding: 8px;
	display:none;
}
 

#d_setting{	display:none;overflow: auto;height:500px; width:800px; position: absolute;top:80px;left:120px;}
.tip{color:#900;}
.divTitle{
	color:#fff;
	background-color:#900;
	border: 0px;
	padding: 8px;
	font-family:'微軟正黑體'; 
	font-size:14px;
	width:764px;
	margin-top:10px;
	margin-left:10px;
}
.divBorder1 { /* 內容區 第一層外框 */
	background-color:#dedede;
	border:1px solid #666;
	padding:0px;
	margin:0px;
	font-size:16px;
}

.divBorder2 { /* 內容區 第二層外框 */
	padding:8px; 
	background:#FFFFFF;
	margin:0px 10px 0px 10px;
	
	height:400px;
	overflow-x:hidden;
	overflow-y:auto;
}
#d_bottom{	padding-top:8px;}
table.grid{  margin:0px;	border: 1px solid #ccc; border-collapse: collapse; width:100%;}
table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; color:#000;background: #fff; text-align:center;}


 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var type = '<?=$_REQUEST[type]?>';
	var cancelFG = true;	
	$(function(){
		$('#d_setting').draggable();
	});
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
			case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
			case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
			case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else{ 			
			location.href="edit.php?ID="+selid+"&pages="+cp+"&type="+type;
		}
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
	function doAppend(){
		location.href="append.php?type="+type
	}
	function setClose(visitId){
		location.href="booking_close.php?visitId="+visitId;
		
	}
	
	function doSubmit() {
		form1.submit();
	}
	function uploadfile(event){
		console.log(event);
		files = event.target.files;
		console.log(files);
	}
	

 </script>
 <script Language="JavaScript" src="list.js"></script> 
</Head>

<body>


<a name="top"/>
<form name="form1" method="post" action="list.php?type=<?=$_REQUEST[type]?>" onSubmit="return Form1_Validator(this);" enctype="multipart/form-data" style="margin:0">
 <table width="95%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    	<input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><?php if ($_SESSION['privilege'] > 10) { ?><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='doAppend()'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
		<?php } ?>
	<?php if($_REQUEST['type'] == 'year_end') { ?>
		<select name="wkYearSelect" id="wkYearSelect" onChange="doSubmit()">
			<option value="">--全部--</option>
	    	<? foreach ($wkYearList as $key => $value) {
	    		$sel = ($wkYear == $key) ? 'selected':'';
	    	 	echo "<option $sel value='".$key."'>".$value." 年</option>"; 
	    	} ?>
	    </select>
	<?php } ?>


    	
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	$sql = 'select * from '.$tableName." where type='".$_REQUEST['type']."'";
	if($wkYear != '') $sql .= " and wkYear='".$wkYear."'";
	// 搜尋處理
	if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%'))";
	// 排序處理
	if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
	$sql .= " limit ".$pageSize*$pages.",$pageSize"; 
	 //echo $sql;
	$rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]]; 
?>
  <tr valign="top" id="tr_<?=$id?>" style='cursor:pointer;' onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this,'<?=$r['uType']?>')"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
    <?php if($_SESSION['privilege'] > 10) { ?>
            <a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a>
    <?php } else { ?>
    		<a href="javascript:void(0)"><?=$id?></a>
    <?php } ?>
    </td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? 
				switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;
						case "copy" : 
							echo '<input type="button" value="複製" onClick="copy('.$r['ofID'].');">';
							break;
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo ($fldValue != '') ? date('Y/m/d',strtotime($fldValue)) : ''; break;
						default :	
							if($fnAry[$i] == 'action') {
								if($r['isClose'] == 0) echo "<input type='button' value='審核年終' onclick='detail(".$r['id'].")'/>";
							} elseif($fnAry[$i] == 'YEAR') {
								echo date('Y', strtotime($r['festivalDate']));
							} elseif($fnAry[$i] == 'isClose') {
								/*if($fldValue == 1) {
									echo '<div style="text-align:center;"><input type="checkbox"  value="1" disabled checked ></div>';
								} else {
									echo '<div style="text-align:center;"><input type="checkbox" onClick="closeBonus(this,'.$r['id'].','."'".$_REQUEST['type']."'".')" value="1" ></div>';
								}*/

								$checked = ($fldValue == 1) ? 'checked' : '';
								echo '<div style="text-align:center;"><input type="checkbox" onClick="closeBonus(this,'.$r['id'].','."'".$_REQUEST['type']."'".')" value="1" '.$checked.'></div>';
							}else echo $fldValue;
							break;
				}
			?>
		</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>
<div id='d_setting' class='divBorder1'>
	<div id="div_loadding" style="position: absolute;width: 100%;height: 100%;text-align: center;background-color: rgba(0, 0, 0, .1);display: none;"><img style="margin-top: 200px;" src="../images/loadding.gif"></div>
	<div class='divTitle'>年終審核<input type="button" value="取消" style="float:right;cursor:pointer;margin-left:5px;" onclick="yearEndDisplay()"><input type="button" value="確定" onclick="auditYearEnd()" style="float:right;cursor:pointer;"></div>
	<div class='divBorder2'>
		<div id="selectDiv" style="padding: 8px 0;">
			<select name="depID" style="height: 21px;" onchange="detail(0)">
			<option value="">-全部部門-</option>
<?php
	if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'>$v</option>";
    } else {
		foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'>$departmentinfo[$v]</option>";
    }
?>

			</select>
			<input type="text" name="emp" placeholder="輸入員編或名字">
			<input type="button" name="empSelect" value="確定搜尋" onclick="detail(0)">
		</div>
		
		<div id="eList"></div>
	</div>

</div>
<div align="right"><a href="#top">Top ↑</a></div>
<script type="text/javascript">

/*覆蓋原有在 外層lisy.js 的SearchKeyword*/
var bfID = 0;
function SearchKeyword() {
	rzlt = prompt("請輸入關鍵字",keyword);
 	if (rzlt!=null) {
		//location.href="list.php?keyword="+encodeURIComponent(rzlt);
		//::過濾條件要跟
		var yrflt = $('select#year option:selected').val();
		var mtflt = $('select#month option:selected').val();
		var url = "list.php?type=<?php echo $_REQUEST['type'];?>&keyword="+encodeURIComponent(rzlt);
		if(yrflt) url = url+'&year='+yrflt;
		if(mtflt) url = url+'&month='+mtflt;
		location.href = url;		
 	}	 
}

function yearEndDisplay() {
	$('#selectDiv [name="depID"]').val('')
	$('#selectDiv [name="emp"]').val('');
	$('#d_setting').hide();
}

var taxRatio = 0;
var deductBase = 0;
function detail(id) {
	if(id != 0) bfID = id;
	var assessList = ['差','可','佳','優'];
	$.ajax({
        url: 'getYearEnd.php',
        type: 'post',
        data:{
        	id: bfID,
        	depID: $('#selectDiv [name="depID"]').val(),
        	emp: $('#selectDiv [name="emp"]').val()
        },
        dataType: 'json',
        success: function(d){
        	//console.log(d);
        	taxRatio = d['taxRatio'];
        	deductBase = d['deductBase'];
        	var html = [];
        	html.push('<table class="grid"><tr><td style="width:12%">員編</td><td style="width:14%">名稱</td><td>考核</td><td style="width:14%">年終獎金</td><td style="width:10%">扣稅金額</td><td>額外扣除</td><td>額外扣除原因</td><td style="width:14%">實發金額</td><td>手動</td><td>不發放</td></tr>');
        	for(var i in d['emp']) {
        		html.push('<tr>');
        		html.push('<td data-empid="'+d['emp'][i]['empID']+'">'+d['emp'][i]['empID']+'</td>');
        		html.push('<td>'+d['emp'][i]['name']+'</td>');

        		if(d['emp'][i]['isAssess'] == 1) {
        			html.push('<td><select name="assess['+d['emp'][i]['empID']+']" data-id="'+d['emp'][i]['empID']+'">');
        			html.push('<option value="">--選擇基數--</option>');
        			for(var j in assessList) {
        				$k = parseInt(j)+1;
        				var selected = ('grade'+$k == d['emp'][i]['score']) ? 'selected': '';
        				html.push('<option '+selected+' value="grade'+$k+'">'+assessList[j]+'</option>');
        			}
        			html.push('</select></td>');
        		} else {
        			html.push('<td>'+d['emp'][i]['assess']+'</td>');
        		}    
        		html.push('<td><input style="width:60px;" '+((d['emp'][i]['isPayHand'] == 1) ? '' : 'disabled')+'  type="text" data-bonus="'+d['emp'][i]['empID']+'" value="'+d['emp'][i]['bonus']+'" ></td>');

        		html.push('<td><input style="width:50px;" disabled type="text" data-tax="'+d['emp'][i]['empID']+'" value="'+d['emp'][i]['tax']+'" ></td>');  

        		html.push('<td><input style="width:60px;" '+((d['emp'][i]['isPayHand'] == 1) ? '' : 'disabled')+'  type="text" data-other="'+d['emp'][i]['empID']+'" value="'+d['emp'][i]['otherDeduct']+'" ></td>');

        		html.push('<td><input style="width:120px;" '+((d['emp'][i]['isPayHand'] == 1) ? '' : 'disabled')+'  type="text" data-other-notes="'+d['emp'][i]['empID']+'" value="'+d['emp'][i]['otherDeductNotes']+'" ></td>');

        		html.push('<td><input style="width:60px;" disabled type="text" data-money="'+d['emp'][i]['empID']+'" value="'+d['emp'][i]['money']+'" ></td>');

        		html.push('<td><input style="cursor:pointer;" type="checkbox" data-hand="'+d['emp'][i]['empID']+'" value="1" '+((d['emp'][i]['isHand'] == 1) ? 'checked' : '')+'></td>');

        		if(d['emp'][i]['isPayHand'] == -1) {
        			html.push('<td></td>');
        		} else {
        			html.push('<td><input style="cursor:pointer;" type="checkbox" data-pay="'+d['emp'][i]['empID']+'" value="1" '+((d['emp'][i]['isPayHand'] == 1) ? '' : 'checked')+'></td>');
        		}

        		html.push('</tr>');
        	}
        	html.push('</table>');
        	//console.log(html);
        	$('#eList').html(html.join(''));
        	$('#d_setting').show();
        }
    })  
}

function auditYearEnd() {
	$('#div_loadding').css({'display':''});
	var auditList = {};
	var handList = {};
	var payList = {};
	$('#d_setting .divBorder2 [data-id]').each(function(){
		if($(this).val() != '') {
			auditList[$(this).attr('data-id')] = $(this).val();
		} 
	});

	$('[data-hand]:checked').each(function(){
		var empID = $(this).attr('data-hand');
		if($(this).prop('checked')) {
			handList[empID] = {
				'isHand':1,
				'bonus':$('[data-bonus="'+empID+'"]').val(),
				'tax':$('[data-tax="'+empID+'"]').val(),
				'money':$('[data-money="'+empID+'"]').val()
			};
		} 
	});

	$('[data-pay]:checked').each(function(){
		var empID = $(this).attr('data-pay');
		payList[empID] = $(this).val();
	});

	var empList = {};
	$('[data-empid]').each(function(){
		var empID = $(this).attr('data-empid');
		empList[empID] = empID;
	});

	var otherList = {};
	$('[data-other]').each(function(){
		if($(this).val() > 0) {
			var empID = $(this).attr('data-other');
			otherList[empID] =  $(this).val();
		}
	});

	var otherNotesList = {};
	$('[data-other-notes]').each(function(){
		if($(this).val() != '') {
			var empID = $(this).attr('data-other-notes');
			otherNotesList[empID] =  $(this).val();
		}
	});

	$.ajax({
        url:'auditYearEnd.php',
        type:'post',
        data:{id:bfID, auditList:auditList, handList:handList, payList:payList, empList:empList, otherList:otherList, otherNotesList:otherNotesList},
        dataType:'json',
        success:function(d){
        	if(d['result'] == 1) {
        		detail(d['id']);
        		alert('計算完成');
        		$('#div_loadding').css({'display':'none'});
        	}
        }
    })  
	
}

$(function(){
	var oldMoney = '';
	var oldEmpID = '';
	var oldOther = 0;
	var num = /^\+?[0-9][0-9]*$/;		
	$(document).on('focus', '[data-bonus]', function(){
		oldMoney = $(this).val();
		oldEmpID = $(this).attr('data-bonus');
	});

	$(document).on('change', '[data-bonus]', function(){
		taxRatio = parseInt(taxRatio);
		deductBase = parseInt(deductBase);
		var empID = $(this).attr('data-bonus');
		var bonus = $('[data-bonus="'+empID+'"]').val();

		if(!num.test(bonus) && bonus != '') {
			alert('請輸入數字');
			$('[data-bonus="'+empID+'"]').val(oldMoney);
			return false;
		}
		var tax = (bonus >= deductBase) ? Math.round(bonus/100*taxRatio) : 0;
		var other = $('[data-other="'+empID+'"]').val();

		$('[data-tax="'+empID+'"]').val(tax);
		var money = bonus-tax;
		if(money < other) {
			alert('(年終獎金 - 扣稅金額) 不得小於額外扣除');
			$(this).val(oldMoney);

		} else {
			$('[data-money="'+empID+'"]').val(money-other);
		}
		
	});

	$(document).on('focus', '[data-other]', function(){
		oldOther = $(this).val();
	});

	$(document).on('change', '[data-other]', function(){
		var other = $(this).val();
		if(!num.test(other) && other != '') {
			alert('請輸入數字');
			$(this).val(oldOther);
			return false;
		}
		var empID = $(this).attr('data-other');
		var money = $('[data-money="'+empID+'"]').val();
		if(money != '') {
			if(parseInt($(this).val()) > parseInt(money)) {
				alert('額外扣除 不得大於 實發金額');
				$(this).val(oldOther);
			} else {
				 $('[data-money="'+empID+'"]').val(parseInt(money)-parseInt($(this).val()));
			}
		}
		
	});

	$(document).on('click', '[data-pay]', function(){
		var empID = $(this).attr('data-pay');
		if($(this).prop('checked')) {
			$('[data-bonus="'+empID+'"]').val(0);
			$('[data-tax="'+empID+'"]').val(0);
			$('[data-money="'+empID+'"]').val(0);
			$('[data-other="'+empID+'"]').val(0);
			$('[data-other-notes="'+empID+'"]').val('');
			$('[data-bonus="'+empID+'"]').attr('disabled','disabled');
			$('[data-other="'+empID+'"]').attr('disabled','disabled');
			$('[data-other-notes="'+empID+'"]').attr('disabled','disabled');
		} else {
			$('[data-bonus="'+empID+'"]').removeAttr('disabled');
			$('[data-other="'+empID+'"]').removeAttr('disabled');
			$('[data-other-notes="'+empID+'"]').removeAttr('disabled');
		}
	});
});

//關帳
function closeBonus(obj, id, type) {

	var title = '';
	if(!$(obj).prop('checked')) title = '確定要重新開帳?';
	else title = '關帳後將不能修改資訊,確定關帳?';

 	if(!confirm(title)) {
		$(obj).prop('checked',!$(obj).prop('checked'));
		return false;
	}

	var isClose = ($(obj).prop('checked')) ? 1 : 0;

	$.ajax({
        url:'closeBonus.php',
        type:'POST',
        data:{id:id,type:type,isClose:isClose},
        dataType:'json',
        success:function(d){
        	console.log(d);
        	if(d['result'] == 0) alert(d['msg']);
        	else location.reload();
        }
    }) 
}
</script>
</body>
</Html>