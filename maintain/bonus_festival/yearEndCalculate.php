<?php
	//計算年終
	function yearEndCalculate($bfID, $auditList = array()) {
		$result = array();

		$leaveList = array(0,5); //離職狀態

		//取得主檔
		$sql = "select * from bonus_festival where id = '".$bfID."' and type='year_end'";
		$rs = db_query($sql);
		if(db_num_rows($rs) == 0) {
			$result['error'] = 'noMain';
			return $result;
			exit;
		}
		$bonusData = db_fetch_array($rs);
		$wkYear = (int)$bonusData['wkYear'];

		//抓本薪
		$salaryList = getSalary($wkYear, $leaveList);
		if(count($salaryList) == 0) {
			$result['error'] = $wkYear.'未有薪資試算';
			return $result;
			exit;
		}

		$jobinfo = array();
		$sql = "select jobID,jobName from jobs";
		$rs = db_query($sql);
		while($r=db_fetch_array($rs)) $jobinfo[$r['jobID']] = $r['jobName'];

		//取得相關設定
		$sql = "select * from bonus_festival_basic where bfID = '".$bfID."' order by id";
		$rs = db_query($sql);
		$noPay = array();
		$special = array();
		$assess = array();
		while ($r = db_fetch_array($rs)) {
			switch ($r['className']) {
				case 'no':
					$noPay[$r['object']] = $r['depiction'];
					break;
				case 'special':
					$special[$r['object']] = array();
					$special[$r['object']] = explode(',', $r['depiction']);
					foreach ($special[$r['object']] as $key => $value) {
						$special[$r['object']][$key] = explode(':', $value);
					}
					break;
				case 'assess':
					$assess[$r['object']] = array();
					$depiction = explode(',', $r['depiction']);
					foreach ($depiction as $key => $value) {
						$depiction[$key] = explode(':', $value);
						$assess[$r['object']]['grade'.(4-(int)$key)] = $depiction[$key][1];
					}
					break;
			}
		}

		//取取得工時比例
		$ratioList = getHourRatio($wkYear, $leaveList);

		//判別事假狀況(不給年終)
		$noPayThing = checkThing($noPay['thing'], $wkYear, $ratioList);

		//$sql = "select * from emplyee where (isOnduty IN (1,2) or (isOnduty IN (".implode(',', $leaveList).") and leaveDate > '".$wkYear."-01-01')) order by empID";
		$sql = "select e.*, a.actDays from emplyee e inner join emplyee_annual a on e.empID = a.empID where (e.isOnduty IN (1,2) or (e.isOnduty IN (".implode(',', $leaveList).") and e.leaveDate > '".$wkYear."-01-01')) and a.year='".($wkYear+1)."' order by e.empID";
		$rs = db_query($sql);
		if(db_num_rows($rs) == 0) {
			$result['error'] = 'noActDays';
			return $result;
			exit;
		}
		while ($r = db_fetch_array($rs)) {
			$emplyee[$r['empID']] = $r;
			$result[$r['empID']] = array(
				'empID' => $r['empID'],
				'projID' => $r['projID'],
				'wkYear' => $wkYear,
				'jobClass' => $r['jobClass'],
				'actDays' => $r['actDays'], //會計年資
				'salary' => 0, //本薪
				'salaryYear' => 0, //年平均薪
				'salaryType' => -1, //薪資類別
				'setSalary' => 0, //最後寫入資料庫的本薪 0:salary, 1:salaryYear
				'pay' => -1, //年終金額 -1 代表尚未計算基數
				'tax' => 0, //扣歲金額
				'base' => '', //審核基數
				'score' => '',//考核等級
				'type' => '', //給付類別
				'serviceRatio' => 0, //當年服務比例,
				'hireDate' => $r['hireDate'], //就職時間
				'leaveDate' => $r['leaveDate'], // 離職時間,
				'stopRange' => array(),
				'isAssess' => 0, //計算方式是否為發放基數
				'isPay' => 1 //是否發放
			);

			//帶入本薪與給薪類別
			if(isset($salaryList[$r['empID']])){
				$result[$r['empID']]['salary'] = $salaryList[$r['empID']]['salary'];
				$result[$r['empID']]['salaryYear'] = $salaryList[$r['empID']]['salaryYear'];
				$result[$r['empID']]['salaryType'] = $salaryList[$r['empID']]['salaryType'];
			} 

			//判別離職狀況(不給年終)
			if(in_array($r['isOnduty'], $leaveList) && strtotime($noPay['leave']) > strtotime($r['leaveDate'])) {
				$result[$r['empID']]['pay'] = 0;
				$result[$r['empID']]['isPay'] = 0;
				$result[$r['empID']]['type'] = '於'.$noPay['leave'].'前離職';
				continue;
			}

			//判別未滿N個(不給年終)
			/*if($noPay['year'] * 30 > (int)$r['actDays']) {
				$result[$r['empID']]['pay'] = 0;
				$result[$r['empID']]['isPay'] = 0;
				$result[$r['empID']]['type'] = '年資未滿'.$noPay['year'].'個月';
				continue;
			}*/

			//判別到職日之後(不給年終)
			if(strtotime($noPay['hireDate']) < strtotime($r['hireDate'])) {
				$result[$r['empID']]['pay'] = 0;
				$result[$r['empID']]['isPay'] = 0;
				$result[$r['empID']]['type'] = '於'.$noPay['hireDate'].'後到職';
				continue;
			}

			//判別事假狀況(不給年終)
			if(isset($noPayThing[$r['empID']]) && isset($noPayThing[$r['empID']]['type'])) {
				$result[$r['empID']]['pay'] = 0;
				$result[$r['empID']]['isPay'] = 0;
				$result[$r['empID']]['type'] = $noPayThing[$r['empID']]['type'];
				continue;
			}
			
			$eData = array(
				'empID'=>$r['empID'],
				'hireDate'=>$r['hireDate'],
				'leaveDate'=>$r['leaveDate']
			);

			//計算服務比例(扣掉留職停薪)
			$sRatio = calculateServiceRatio($eData, $wkYear);
			$result[$r['empID']]['serviceRatio'] = $sRatio['proportion'];
			if(isset($sRatio['stopRange'])) $result[$r['empID']]['stopRange'] = $sRatio['stopRange'];
			

			//特殊設定發放計算
			if(count($special) > 0) {
				$eData = array(
					'salary'=>$result[$r['empID']]['salary'],
					'salaryYear'=>$result[$r['empID']]['salaryYear'],
					'salaryType'=>$result[$r['empID']]['salaryType'],
					'jobClass'=>$r['jobClass'],
					'actDays'=>$r['actDays'],
					'serviceRatio'=>$result[$r['empID']]['serviceRatio'],
					'deduct_base'=>$bonusData['deduct_base'],
					'taxRatio'=>$bonusData['taxRatio']
				);
				$sData = specialCalculate($eData, $special);
				if($sData['result'] == 1) {
					$result[$r['empID']]['tax'] = $sData['tax'];
					$result[$r['empID']]['pay'] = $sData['pay'];
					$result[$r['empID']]['type'] = $sData['type'];
					$result[$r['empID']]['setSalary'] = $sData['setSalary'];
					continue;
				}
			}
			
			//基數設定發放計算
			if(isset($auditList[$r['empID']]['score'])) {
				$result[$r['empID']]['score'] = $auditList[$r['empID']]['score'];
				$eData = array(
					'salary'=>$result[$r['empID']]['salary'],
					'jobID'=>$r['jobID'],
					'assess'=>$auditList[$r['empID']]['score'],
					'base'=>$bonusData['base'],
					'serviceRatio'=>$result[$r['empID']]['serviceRatio'],
					'deduct_base'=>$bonusData['deduct_base'],
					'taxRatio'=>$bonusData['taxRatio']
				);

				$aData = assessCalculate($eData, $assess, $jobinfo);
				$result[$r['empID']]['tax'] = $aData['tax'];
				$result[$r['empID']]['pay'] = $aData['pay'];
				$result[$r['empID']]['type'] = $aData['type'];
				$result[$r['empID']]['base'] = $aData['base'];
				$result[$r['empID']]['isAssess'] = 1;
			}

			if($result[$r['empID']]['pay'] == -1) {
				$result[$r['empID']]['isAssess'] = 1;
			}

		}
		return $result;
	}

	//直接抓薪資試算
	// 抓當年度最後一個月的本薪, 若是時薪制 需要抓7~12月的平均薪資
	//新增 抓全年月平均
	function getSalary($wkYear, $leaveList) {
		include_once "../emplyee_salary_report/api.php";
		$result = array();
		//7~12月薪資 (類別為時薪 需要)
		$mList = array();
		for($i=7;$i<=12;$i++) $mList[] = "'".$wkYear.str_pad($i,2,'0',STR_PAD_LEFT)."'";
		$sql = "select empID,sum(mBasic) as total,count(*) as con from emplyee_salary_report where wkMonth in (".implode(',', $mList).") and sType='1' group by empID order by empID";

		$rs = db_query($sql);
		$hourList = array();
		while ($r = db_fetch_array($rs)) $hourList[$r['empID']] = round($r['total']/$r['con']);

		//$sql = "select * from emplyee_salary_report where wkMonth = '".$wkYear."12'";
		//$sql = "select * from (select * from emplyee_salary_report where wkMonth like '".$wkYear."%' order by empID asc, wkMonth desc) as esr group by empID";

		$sql = "SELECT t.* FROM (SELECT s.* FROM `emplyee_salary` s inner join emplyee e on s.empID= e.empID where (e.isOnduty IN (1,2) or (e.isOnduty IN (".implode(',', $leaveList).") and e.leaveDate > '".$wkYear."-01-01')) and s.changeDate <= '".$wkYear."-12-31' order by s.empID asc, s.changeDate desc LIMIT 100000000000000) t group by t.empID";

		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {

			$result[$r['empID']] = array();
			$result[$r['empID']]['salaryType'] = $r['salaryType'];
			$basicData = basic('','',$r['empID'],$r);
			$result[$r['empID']]['salary'] = $basicData[1]; //20200104 sean 將取的本薪與薪資試算同步

			//$result[$r['empID']]['salary'] = $r['salary'];
			if($r['salaryType'] == 1 ) $result[$r['empID']]['salary'] = $hourList[$r['empID']];
		}


		/*$sql = "select * from emplyee_salary_report where wkMonth like '".$wkYear."%' order by empID asc, wkMonth asc";
		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {
			$result[$r['empID']] = array();
			$result[$r['empID']]['salaryType'] = $r['sType'];
			$result[$r['empID']]['salary'] = $r['mBasic'];
			if($r['sType'] == 1) $result[$r['empID']]['salary'] = $hourList[$r['empID']];
		}*/



		//抓全年月平均
		$sql = "select empID,sum(mBasic) as total,count(*) as con from emplyee_salary_report where wkMonth like '".$wkYear."%' group by empID order by empID";
		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) $result[$r['empID']]['salaryYear'] = round($r['total']/$r['con']);
		return $result;
	}

	//取得工時比例
	function getHourRatio($wkYear, $leaveList) {
		$result = array();
		$sql = "select s.* from emplyee_salary s inner join emplyee e on s.empID=e.empID where (e.isOnduty IN (1,2) or (e.isOnduty IN (".implode(',', $leaveList).") and e.leaveDate > '".$wkYear."-01-01')) and changeDate < '".$wkYear."-12-31' and changeDate >= '".$wkYear."-01-01' order by s.empID asc, changeDate desc, id asc";
		$rs = db_query($sql);
		while ($r = db_fetch_array($rs)) {
			$result[$r['empID']][$r['changeDate']] = array('ratio'=>$r['ratio'], 'cDate'=>$r['changeDate']);
		}

		/*$sql = "select * from (select s.* from emplyee_salary s inner join emplyee e on s.empID=e.empID where (e.isOnduty IN (1,2) or (e.isOnduty IN (".implode(',', $leaveList).") and e.leaveDate > '".$wkYear."-01-01')) and changeDate < '".$wkYear."-01-01' order by s.empID asc, changeDate desc, id desc) as ss group by empID";*/
		$sql = "select s.* from emplyee_salary s inner join emplyee e on s.empID=e.empID where (e.isOnduty IN (1,2) or (e.isOnduty IN (".implode(',', $leaveList).") and e.leaveDate > '".$wkYear."-01-01')) and changeDate < '".$wkYear."-01-01' order by s.empID asc, changeDate desc, id desc";
		$rs = db_query($sql);
		$empID = '';
		while ($r = db_fetch_array($rs)) {
			if($empID == $r['empID']) continue;
			$empID = $r['empID'];
			$result[$r['empID']][$r['changeDate']] = array('ratio'=>$r['ratio'], 'cDate'=>$r['changeDate']);	
		}
		return $result;
	}

	//判別事假狀況(不給年終) 一天工時為8 但要按照工時比例 與月份
	function checkThing($thing, $wkYear, $ratioList = array()) {
		$result = array();
		/*$sql = "select empID, sum(hours) as total from absence where bDate >='".$wkYear."-01-01' and bDate <='".$wkYear."-12-31' and isDel = '0' and isOK='1' and aType='事假' and empID='00512' group by empID order by empID";*/
		$sql = "select empID, hours, bDate, eDate from absence where bDate >='".$wkYear."-01-01' and bDate <='".$wkYear."-12-31' and isDel = '0' and isOK='1' and aType='事假' order by empID asc, bDate asc";

		$rs = db_query($sql);
		//$result = array();
		while ($r = db_fetch_array($rs)) {
			$ratio = 1;
			if(!isset($ratioList[$r['empID']])) { //代表沒有工時比例
				$result[$r['empID']]['thing'][1] = (!isset($result[$r['empID']]['thing'][1])) ? $r['hours'] : $result[$r['empID']]['thing'][1] + $r['hours'];
			} else {
				$bDate = strtotime($r['bDate']);
				
				foreach ($ratioList[$r['empID']] as $k => $v) {
					$wkDate = strtotime($k);		
					if($bDate >= $wkDate) {
						$ratio = $v['ratio'];
						$result[$r['empID']]['thing'][$v['ratio']] = (!isset($result[$r['empID']]['thing'][$v['ratio']])) ? $r['hours'] : $result[$r['empID']]['thing'][$v['ratio']] + $r['hours'];
						break;
					}
				}
			}

			if(!isset($result[$r['empID']]['thingDay'])) {
				$result[$r['empID']]['thingDay'] = $r['hours'] / (8 * $ratio);
			} else {
				$result[$r['empID']]['thingDay'] += ($r['hours'] / (8 * $ratio));
			}
			if((int)$thing <= $result[$r['empID']]['thingDay']) {
				$result[$r['empID']]['pay'] = 0;
				$text = array();
				foreach ($result[$r['empID']]['thing'] as $k => $v) {
					if($k == 'thingDay') continue;
					$text[] = $v.'小時(工時比例:'.$k.')';
				}
				$result[$r['empID']]['type'] = '事假超過'.$thing.'(含)天, 事假總天數:'.$result[$r['empID']]['thingDay'].'天, 事假時數=> '.implode(', ', $text);	
			}

		}
		/*foreach ($result as $key => $value) {
			if((int)$thing <= $value['thingDay']) {
				$result[$key]['pay'] = 0;
				$text = array();
				foreach ($value as $k => $v) {
					if($k == 'thingDay') continue;
					$text[] = $v.'小時(工時比例:'.$k.')';
				}
				$result[$key]['type'] = '事假超過'.$thing.'(含)天, 事假時數=> '.implode(', ', $text);	
				echo "<pre>";
				print_r($value);
			}
		}*/

		return $result;
	}

	//計算服務比例(扣掉留職停薪) 以月來算 不滿一個月直接扣掉
	/*function calculateServiceRatio($empData=array(), $wkYear) {
		$result = array();
		$empID = $empData['empID'];
		$wkYearB = strtotime($wkYear.'-01-01');
		$wkYearE = strtotime($wkYear.'-12-31');

		$sql = "select * from emplyee_status where empID ='".$empID."' and jobStatus in ('3','4') order by id";

		$rs = db_query($sql);
		$result = array('empID'=>$empData['empID'],'serviceMonth'=>0,'stopMonth'=>array(),'proportion'=>0);
		if(db_num_rows($rs) > 0)  {
			while($r = db_fetch_array($rs)) {
				$sM = 0;
				$eM = 0;
				$bDate = strtotime($r['bdate']);
				$eDate = strtotime($r['edate']);
				//判斷四個狀況
				if($bDate >= $wkYearB && $eDate <= $wkYearE) { //正常範圍
					$sM = (int)date('m',$bDate);
					$eM = (int)date('m',$eDate);
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				} elseif ($bDate < $wkYearB && $eDate <= $wkYearE && $eDate >= $wkYearB) { //前年留停跨到今年
					$sM = (int)date('m',$wkYearB);
					$eM = (int)date('m',$eDate);
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];

				} elseif($bDate >= $wkYearB && $eDate > $wkYearE && $bDate <= $wkYearE) {  //今年跨到明年
					$sM = (int)date('m',$bDate);
					$eM = (int)date('m',$wkYearE);
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				} elseif($bDate < $wkYearB && $eDate > $wkYearE) {  //整年都不在
					$sM = (int)date('m',$wkYearB);
					$eM = (int)date('m',$wkYearE);
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				}

				if($sM != 0) {
					for($i=$sM; $i<=$eM; $i++) {
						$result['stopMonth'][$i] = $i;
					}
				}
			}
		}

		//扣掉未滿一年的服務月數
		if(!empty($empData['hireDate']) && strtotime($empData['hireDate']) > $wkYearB) {
			$sM = (int)date('m',$wkYearB);
			$eM = (int)date('m',strtotime($empData['hireDate']));
			if((int)date('d',strtotime($empData['hireDate'])) === 1) $eM-=1; //排除該月1號就職的月份
			for($i=$sM; $i<=$eM; $i++) {
				$result['stopMonth'][$i] = $i;
			}
		}
		if(!empty($empData['leaveDate']) && strtotime($empData['leaveDate']) < $wkYearE) {
			$sM = (int)date('m',strtotime($empData['leaveDate']));
			$eM = (int)date('m',$wkYearE);
			if((int)date('t',strtotime($empData['leaveDate'])) === (int)date('d',strtotime($empData['leaveDate']))) $sM+=1; //排除該月最後一天離職的月份
			for($i=$sM; $i<=$eM; $i++) {
				$result['stopMonth'][$i] = $i;
			}
		}

		//扣掉留停月數
		for($i=1;$i<=12;$i++) {
			if(!isset($result['stopMonth'][$i])) {
				$result['serviceMonth']++;
			}
		}
		
		//比例 四捨五入小數點第二位
		$result['proportion'] = round($result['serviceMonth']/12, 2);

		return $result;
	}*/

	//計算服務比例(扣掉留職停薪) 扣完天數 轉換成月來算
	function calculateServiceRatio($empData=array(), $wkYear) {
		$result = array();
		$empID = $empData['empID'];
		$wkYearB = strtotime($wkYear.'-01-01');
		$wkYearE = strtotime($wkYear.'-12-31');
		$totalDay = ($wkYearE - $wkYearB) / (24*60*60) + 1;
		$sql = "select * from emplyee_status where empID ='".$empID."' and jobStatus in ('3','4') order by id";

		$rs = db_query($sql);
		$result = array('empID'=>$empData['empID'],'serviceDay'=>$totalDay, 'serviceMonth'=>0, 'stopDay'=>0, 'proportion'=>0);
		if(db_num_rows($rs) > 0)  {
			while($r = db_fetch_array($rs)) {
				$bDate = strtotime($r['bdate']);
				$eDate = strtotime($r['edate']);
				//判斷四個狀況
				if($bDate >= $wkYearB && $eDate <= $wkYearE) { //正常範圍
					$result['stopDay'] += ($eDate - $bDate) / (24*60*60) + 1; //換算成天
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				} elseif ($bDate < $wkYearB && $eDate <= $wkYearE && $eDate >= $wkYearB) { //前年留停跨到今年
					$result['stopDay'] += ($eDate - $wkYearB) / (24*60*60) + 1; 
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];

				} elseif($bDate >= $wkYearB && $eDate > $wkYearE && $bDate <= $wkYearE) {  //今年跨到明年
					$result['stopDay'] += ($wkYearE - $bDate) / (24*60*60) + 1; 
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				} elseif($bDate < $wkYearB && $eDate > $wkYearE) {  //整年都不在
					$result['stopDay'] = $totalDay;
					$result['stopRange'][] = $r['bdate'] .'~'.$r['edate'];
				}
			}
		}

		//扣掉未滿一年的服務天數
		if(!empty($empData['hireDate']) && strtotime($empData['hireDate']) > $wkYearB) {
			$noServiceDay = (strtotime($empData['hireDate']) - $wkYearB) / (24*60*60);
			$result['serviceDay'] -= $noServiceDay;
		}
		if(!empty($empData['leaveDate']) && strtotime($empData['leaveDate']) > strtotime($empData['hireDate']) && strtotime($empData['leaveDate']) < $wkYearE) {
			$noServiceDay = ($wkYearE - strtotime($empData['leaveDate'])) / (24*60*60);
			$result['serviceDay'] -= $noServiceDay;
		}

		//扣掉留停天數
		$result['serviceDay'] -= $result['stopDay'];
		$result['serviceMonth'] = ($result['serviceDay'] > 0) ? floor($result['serviceDay']/30) : 0; //轉換成月
		//比例 四捨五入小數點第二位
		//$result['proportion'] = round($result['serviceDay']/$totalDay, 2);
		$result['proportion'] = round($result['serviceMonth']/12, 2);
		
		return $result;
	}

	//特殊給年終計算
	function specialCalculate($empData=array(), $special=array()) {
		$result = array('result'=>0, 'pay'=>0, 'tax'=>0, 'type'=>'');
		foreach($special as $key => $value) {
			//若有 鐘點制人員額外判斷 (時薪類別為1) 
			/*20180103 鐘點制定義待修改 目前不會用到*/
			if(($key == 'hour' && $empData['salaryType'] == 1) || in_array($empData['jobClass'], explode(',', $key))) {
				$result['result'] = 1;
				$year = round($empData['actDays']/365, 1);
				$sData = array();
				foreach($value as $k => $v) {
					if($v[0] == 'all') {
						$sData = $v;
						break;
					}
					if(count($sData) == 0) $sData = $v;
					elseif($year >= (int)$v[0]) $sData = $v;
				}

				if($sData[2] == 'dollars') $result['pay'] = $sData[1];
				elseif($sData[2] == 'month') {
					//單位若是月份 本薪(最後一個月)*給定比例*當年服務比例
					$result['pay'] = round((int)$empData['salary']*$sData[1]*$empData['serviceRatio']);
				} elseif($sData[2] == 'average') { //加入判別所選得是年度平均薪(average)
					//單位若是 本薪(最後一個月)*給定比例*當年服務比例
					$result['pay'] = round((int)$empData['salaryYear']*$sData[1]*$empData['serviceRatio']);
					$result['setSalary'] = 1;
				}

				if($key=='hour') {
					if($sData[0] == 'all') {
						$result['type'] = '鐘點制發放(忽略年資)';
					} elseif($sData[0] == 0) {
						$result['type'] = '鐘點制發放(未滿一年)';
					} else {
						$result['type'] = '鐘點制發放('+$sData[0]+'年以上)';
					}

				} else {
					if($sData[0] == 'all') {
						$result['type'] = $empData['jobClass'].'發放(忽略年資)';
					} elseif($sData[0] == 0) {
						$result['type'] = $empData['jobClass'].'發放(未滿一年)';
					} else {
						$result['type'] = $empData['jobClass'].'發放('.$sData[0].'年以上)';
					}
				} 
				break;
			}		
		}
		if($result['pay'] >= $empData['deduct_base']) {
			$result['tax'] = round($result['pay']/100*$empData['taxRatio']);
		}
		return $result;
	}
	//基數設定計算
	//本薪*基數*考核基數*服務比例			
	function assessCalculate($empData=array(), $assess=array(), $jobinfo=array()) {
		$result = array('pay'=>0, 'tax'=>0, 'base'=>0, 'type'=>'一般人員基數發放');
		$aData = $assess[0]; //先取得一般人審核
		foreach ($assess as $key => $value) {
			if($key === 0) continue;
			if(in_array($empData['jobID'], explode(',', $key))) {
				$result['type'] = $jobinfo[$empData['jobID']].'基數發放';
				$aData = $value;
				break;
			}
		}
		$result['base'] = $aData[$empData['assess']];
		$result['pay'] = round($empData['salary']*$empData['base']*$result['base']*$empData['serviceRatio']);
		if($result['pay'] >= $empData['deduct_base']) {
			$result['tax'] = round($result['pay']/100*$empData['taxRatio']);
		}
		return $result;
	}
?>