var openType = '';
//接收回傳的職掌
function returnPluralVal(data) {
	switch(openType) {
		case 'assess':
			gradeAdd(data);
			break;
	}
}

var wd = false;
function openWindow(type) {
	if(wd){
		wd.close();
		wd = false;
	} 
	openType = type;
	var procP = '/maintain/jobs/query_id_other.php';
 	wd = window.open(procP,'','width=300,height=450');
}

//防止選擇到相同職掌
function checkJob(data){
	if(data.length == 0) return false;
	var ret = 0;
	$('[data-job]').each(function(){
		var thisHid = this;
		for(var i in data){
			if($(thisHid).val().indexOf(data[i]['id']) > -1) {
				alert("'"+data[i]['title']+"'設定已經存在");
				ret = 1;
				return false;
			}
		}
		if(ret) return false;
	});
	if(ret) return false;
	else return true;
}

//發放基數設定
function gradeAdd(data) {
	var cJob = checkJob(data);
	if(!cJob) return false;

	var gradeNum = parseInt($('#assessTable tr:last-child').attr('data-num'))+1;
	var nameList = [];
	var idList = [];
	for(var i in data){
		nameList.push(data[i]['title']);
		idList.push(data[i]['id']);
	}

	var html = [];
	html.push('<tr data-num="'+gradeNum+'">');
	html.push('<td>'+nameList.join('<br>')+'</td>');
	for (var i=4; i>0; i--) {
		html.push('<td><input type="text" name="grade'+i+'['+gradeNum+']" value="0" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>');
	}
	html.push('<td><input type="hidden" name="jobID['+gradeNum+']" value="'+idList.join(',')+'" data-job>');
	html.push('<a style="color:red;" href="javascript:void(0)" onclick="gradeDel(this)">刪除</a></td>')
	html.push('</tr>');
	$('#assessTable').append(html.join(''));
}

//刪除發放基數設定
function gradeDel(obj) {
	if(!confirm('確定要刪除?')) return false; 
	$(obj).parent('td').parent('tr').remove();
}

//新增特殊發放規則
function specialAdd(yearData, jClassData) {

	$('#d_setting').hide();
	var nameList = [];
	var idList= [];
	for(var i in jClassData){
		nameList.push(jClassData[i]);
		idList.push(i);
	}

	var specialNum = ($('#specialDiv [data-num]:last-child').length == 0) ? 0 : parseInt($('#specialDiv [data-num]:last-child').attr('data-num'))+1;
	
	var html = [];
	html.push('<table data-num="'+specialNum+'" class="yearTab specialTab" cellpadding="0" cellspacing="0">');
	html.push('<tr><th class="lineTh"><div class="out"><b>年資</b><span class="line"></span><em>職銜</em></div></th>');
	for(var i in yearData) {
		html.push('<th>'+yearData[i]+'<input type="hidden" name="sJob['+specialNum+'][year]['+i+']" value="'+i+'"></th>');
	}
	html.push('<th width="80px"></th>');
	html.push('</tr>');

	html.push('<tr><td>'+nameList.join('<br>')+'</td>');
	for(var i in yearData) {
		html.push('<td>');
		html.push('<input type="text" name="sJob['+specialNum+'][num]['+i+']" value="0" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字" style="width:80px;">');
		html.push('<select name="sJob['+specialNum+'][unit]['+i+']"><option value="dollars">台幣</option><option value="month">月</option><option value="average">年度平均薪</option></select>');
		html.push('</td>')
	}
	html.push('<td>');
	html.push('<input type="hidden" name="sJob['+specialNum+'][id]" value="'+idList.join(',')+'" data-sjob>');
	html.push('<a style="color:red;" href="javascript:void(0)" onclick="specialDel(this)">刪除</a>');
	html.push('</td>');
	html.push('</tr>');
	html.push('</table>');

	$('#specialDiv').append(html.join(''));
}

//刪除特殊發放規則
function specialDel(obj) {
	if(!confirm('確定要刪除?')) return false; 
	$(obj).parents('.specialTab').remove();
}

function openSpecial(){
	$('#d_setting [type="checkbox"]').prop('checked',false);
	$('#d_setting').show();
}
//特殊發放規則 選取年資
function selectYear() {
	var chYearList = $('#d_setting [name="year[]"]:checked');
	var chjClassList = $('#d_setting [name="jClass[]"]:checked');
	if(chYearList.length == 0 || chjClassList.length == 0) {
		alert('請勾選類別與年資');
		return false;
	}

	var jClassData = [];
	var ret = 0;
	chjClassList.each(function(){
		var thisClass = $(this);
		$('#specialDiv [data-sjob]').each(function(){
			if($(this).val().indexOf(thisClass.val()) > -1) {
				alert(thisClass.val()+'已經存在');
				ret = 1;
				return false;
			}
		});
		if(ret) return false;
		jClassData[$(this).val()] = $(this).attr('data-name');
	});
	if(ret) return false;

	var yearList = [];
	chYearList.each(function(){
		yearList[$(this).val()] = $(this).attr('data-name');
	});


	var yearData = [];
	if(yearList['all']) yearData['all'] = yearList['all'];
	else yearData = yearList;
	specialAdd(yearData, jClassData);
	//console.log(yearData);
}
