<?php
	include("init.php");	
	include('../../thumb.php');	

	$n = count($newfnA);
	$fields=array();
	$valAry=array();
	for ($i=0; $i<$n; $i++) {
		if($newfnA[$i]=="bcID" || $newfnA[$i]=="depID")	continue;
		if($newetA[$i] == 'date' && $_POST[$newfnA[$i]] == '') continue;
		array_push($fields,$newfnA[$i]);
		switch($newetA[$i]) {
			case "hidden" :
			case "select" :
			case "readonly" :
			case "textbox":
			case "text" :	$fldv = "'".($_POST[$newfnA[$i]])."'"; break;
			case "textarea" :	$fldv = "'".trim($_POST[$newfnA[$i]])."'"; break;
			case "checkbox" :
				if(isset($_POST[$newfnA[$i]])) $fldv=1; else $fldv=0; 
				break;
			case "date" : $fldv = "'".$_POST[$newfnA[$i]]."'"; break;
			case "file" : $fldv = "'".$fnlst[$newfnA[$i]]."'"; break;			
		}	
		array_push($valAry,$fldv);
	}	
	
	$sql = "insert into $tableName ";
	$sql .= '('.join(",",$fields).",type) values(".join(",",$valAry).",'$_REQUEST[type]')";
	db_query($sql,$conn);
	$new_id=mysql_insert_id();//新增的id


	if($_REQUEST[type] == 'festival'){//如果是三節
		//處理應該要新增到清冊的資料
		//emplyee_bonus_festival
		//年資
		//今年和明年會需要的東西的array
		$now_year = date('Y');
		$all_data[$now_year] = array(
			'now_year'=>date('Y'),
			'now_date'=>date('Y').'-12-31',
			'before_date'=>(date('Y')-1).'-12-31',
			'now_start_day'=>strtotime(date('Y').'-01-01'),
			'now_end_day'=> strtotime(date('Y').'-12-31')
		);



		//抓emplyee員工列表
		$emplyee_list = array();
		$sql = "select `empID`,`isOnduty`,`empName`,`hireDate`,`jobClass`,`jobLevel`,`projID`,`leaveDate` from `emplyee` where `isOnduty` IN (1) and empID<>'99998' and empID<>'00001' ";//在職，排除99998饒恕人、00001莊宏達
		$rs = db_query($sql);
		while($r=db_fetch_array($rs)){
			if($r['empID']!='' && $r['hireDate']!=''){
				if($r[leaveDate] && (strtotime($_REQUEST[festivalDate])>strtotime($r[leaveDate])))	continue;
				$emplyee_list[$r['empID']] = $r;
			}
		}

		
		//留職停薪
		$job_status = array();
		$sql = 'select s.* from `emplyee_status` s inner join `emplyee` e on s.empID = e.empID where s.`jobStatus` IN (3,4) and s.bdate >= e.hireDate ';//留職停薪或是延長留停
		$rs = db_query($sql);
		while($r=db_fetch_array($rs)){
			$bdate = strtotime($r['bdate']);
			$edate = strtotime($r['edate']);
			if($edate>$bdate){
				$job_status['all_year'][$r['empID']][$r['id']] = $r;//每一年的留職停薪//當年不能計算
			}
		}

		

		//開始計算特休
		$emplyee_final_list = array();//員工會計年度以及要發多少
		$insertdata = array();
		foreach($emplyee_list as $k=>$v){
			foreach($all_data as $k1=>$v1){//今年和明年特休
				//宣告全部變數
				$now_year = $v1['now_year'];
				$now_date = $v1['now_date'];
				$before_date = $v1['before_date'];
				

				//計算年資
				$now_day = (strtotime($now_date) - strtotime($v['hireDate']))/86400+1;//天數
				//判斷留職停薪//去年之前的
				$leave = 0;//留職停薪日數
				if(isset($job_status['all_year'][$k])){
					foreach($job_status['all_year'][$k] as $k2=>$v2){
						//處理輸入錯誤的
						if($v2['bdate'] == '' || $v2['bdate']=='0000-00-00'){
							$v2['bdate'] = $before_date;//去年年底
						}
						if($v2['edate'] == '' || $v2['edate']=='0000-00-00'){
							$v2['edate'] = $before_date;//去年年底
						}

						//判斷要算到甚麼時候//計算到今年以前的年底//有符合才要扣掉
						$bdate = strtotime($v2['bdate']);
						$edate = strtotime($v2['edate']);

						if($edate>$bdate){//這樣才是有效數據
							if($bdate<strtotime($before_date)){//計算到去年的留職停薪總共有多少天
								if($edate>strtotime($before_date)){//表示超過去年年底了
									$leave = $leave + ((strtotime($before_date)-$bdate))/86400+1;
								}else{//正常情況
									$leave = $leave + ($edate-$bdate)/86400+1;
								}
							}
						}
					}
				}
				
				//計算會計年資
				$actDays = (strtotime($before_date) - strtotime($v['hireDate']))/86400+1-$leave;//會計年資要扣掉留職停薪//天數
				if($actDays<0){
					$actDays = 0;//因為不滿一年
				}
				$year = date('Y',strtotime($_POST['festivalDate']));
				
				$emplyee_final_list[$v['empID']]['empID'] = $v['empID'];
				$emplyee_final_list[$v['empID']]['jobLevel'] = $v['jobLevel'];
				$emplyee_final_list[$v['empID']]['jobClass'] = $v['jobClass'];
				$emplyee_final_list[$v['empID']]['hireDate'] = $v['hireDate'];
				$emplyee_final_list[$v['empID']]['years'] = intval($actDays/365);
				$emplyee_final_list[$v['empID']]['projID'] = $v['projID'];
				$emplyee_final_list[$v['empID']]['wkYear'] = $year;
				//計算應該給多少
				$emplyee_final_list[$v['empID']]['money'] = money($actDays,$v['hireDate'],$v['jobClass'],$_POST['festivalDate']);				
			}
		}

		//寫入清冊 emplyee_bonus_festival
		foreach ($emplyee_final_list as $k => $v) {
			$sql = "insert into emplyee_bonus_festival (empID,title,jobLevel,jobClass,hireDate,years,money,wkYear,projID,fid) values ('$v[empID]','$_POST[title]','$v[jobLevel]','$v[jobClass]','$v[hireDate]','$v[years]',$v[money],'$v[wkYear]','$v[projID]',$new_id)";
			db_query($sql,$conn);
		}

	} elseif($_REQUEST[type] == 'year_end') {
		/* depiction 寫入方式
			assess(基數設定) -> ex: 等級(grade):月數(2.5), ...
			special(特殊發放) -> ex: 年(1):數字(600):單位(dollars),....
		*/
		//寫入不發放對象設定
		foreach ($_REQUEST['no'] as $key => $value) {
			$sql = "insert into bonus_festival_basic set ";
			$fields = array();
			$fields[] = "bfID='".$new_id."'";
			$fields[] = "className='no'";
			$fields[] = "object='".$key."'";
			$fields[] = "depiction='".$value."'";
			$sql.= implode(',', $fields);
			db_query($sql);
		}

		// 寫入發放基數設定
		foreach ($_REQUEST['jobID'] as $key => $value) {
			$sql = "insert into bonus_festival_basic set ";
			$fields = array();
			$fields[] = "bfID='".$new_id."'";
			$fields[] = "className='assess'";
			$fields[] = "object='".$value."'";
			$condition = array();
			for($i=4;$i>0;$i--) {
				$condition[] = 'grade'.$i.':'.$_REQUEST['grade'.$i][$key];
			}
			$fields[] = "depiction='".implode(',', $condition)."'";
			$sql.= implode(',', $fields);
			db_query($sql);
		}

		//寫入特殊發放規則
		if(!empty($_REQUEST['sJob'])) {
			foreach ($_REQUEST['sJob'] as $key => $value) {
				$sql = "insert into bonus_festival_basic set ";
				$fields = array();
				$fields[] = "bfID='".$new_id."'";
				$fields[] = "className='special'";
				$fields[] = "object='".$value['id']."'";
				$condition = array();
				foreach ($value['year'] as $k2 => $v2) {
					$condition[] = $v2.':'.$value['num'][$k2].":".$value['unit'][$k2];
				}
				$fields[] = "depiction='".implode(',', $condition)."'";
				$sql.= implode(',', $fields);
				db_query($sql);
			}
		}

		//計算部分年終
		include_once("yearEndCalculate.php");
		$yearEndData = yearEndCalculate($new_id);
		if(isset($yearEndData['error'])) {
			print_r($yearEndData['error']);
			exit;
		}
		foreach ($yearEndData as $key => $value) {
			$fields = array();
			$fields[] = "empID ='".$value['empID']."'";
			$fields[] = "jobClass ='".$value['jobClass']."'";
			$fields[] = "salary ='".(($value['setSalary'] == 1) ? $value['salaryYear'] : $value['salary'])."'";
			$fields[] = "wkYear ='".$value['wkYear']."'";
			$fields[] = "tax ='".$value['tax']."'";
			$fields[] = "cTax ='".$value['tax']."'";
			$fields[] = "sRatio ='".$value['serviceRatio']."'";
			$fields[] = "bonus ='".$value['pay']."'";
			$fields[] = "cBonus ='".$value['pay']."'";
			if($value['pay'] > -1) {
				$fields[] = "money ='".($value['pay']-$value['tax'])."'";
				$fields[] = "cMoney ='".($value['pay']-$value['tax'])."'";
			}
			$fields[] = "bfID ='".$new_id."'";
			$fields[] = "projID ='".$value['projID']."'";
			$fields[] = "stopRange ='".implode(',', $value['stopRange'])."'";
			$fields[] = "actDays ='".$value['actDays']."'";
			$fields[] = "notes ='".$value['type']."'";
			$fields[] = "isAssess ='".$value['isAssess']."'";
			$fields[] = "isPay ='".$value['isPay']."'";

			$sql = "insert into emplyee_bonus_year_end set ".implode(',', $fields);
			db_query($sql);
		}

	}
	

	function money($actDays,$hireDate,$jobClass,$festivalDate){
		$money = 0;
		$hireDate = date('Y-m-d',strtotime($hireDate));
		$festivalDate = date('Y-m-d',strtotime($festivalDate));

		$hire_month = date('m',strtotime($hireDate));
		$festivalDate_month = date('m',strtotime($festivalDate));

		$year = intval($actDays/365);
		if($year==0 && $hireDate>$festivalDate){
			$money = 0;
		}elseif($year==0 && $hireDate<=$festivalDate && $hire_month==$festivalDate_month){
			$money = 0;
		}else{
			if($jobClass=='短期人力' || $jobClass=='短期工讀生' || $jobClass=='短期替代性'){
				$money = 0;
			}elseif($jobClass=='庇護性全工時' || $jobClass=='庇護性部分工時' || $jobClass=='外籍' || $jobClass=='多元就業'){
				$money = 500;
			}elseif($jobClass == '支持性全工時' || $jobClass == '支持性部分工時'){
				if($year<1){
					$money = 500;
				}else{
					$money = 1000;
				}
			}elseif($jobClass == '部分工時'){
				if($year<2){
					$money = 500;
				}elseif($year<3){
					$money = 1000;
				}elseif($year>=3){
					$money = 1500;
				}
			}elseif($jobClass == '全工時'){
				if($year<1){
					$money = 500;
				}elseif($year<2){
					$money = 1000;
				}elseif($year<3){
					$money = 2000;
				}elseif($year>=3){
					$money = 3000;
				}
			}else{//沒有填寫任何職位
				$money=0;
			}
		}
		
		return $money;
	}
	
	
	
	db_close($conn);
	
	echo "<script>window.location.href='list.php?type=$_REQUEST[type]'</script>";
	
?>
