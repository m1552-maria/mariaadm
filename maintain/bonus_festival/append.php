<?php
	include("init.php"); 
	if ($_SESSION['privilege'] <= 10){
		$readonly='readonly';
		$class='readonly';
	}else{
		$readonly='';	
		$class='';
	}


	
	/*include("yearEndCalculate.php");
	$yearEndData = yearEndCalculate(18);
	echo "<pre>";
	print_r($yearEndData);
	exit;*/

?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
.readonly{	color:#999;}
	
 </style>
 <link href="grade.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>

 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 新增</title>
 <script type="text/javascript">
 $(function(){
 	$('[name="title"],[name="wkYear"],[name="base"],[name="deduct_base"],[name="taxRatio"]').prop('required',true);
 	$('[name="base"], [name="taxRatio"]').attr('pattern',"^[0-9]+(.[0-9]{1,1})?$");
 	$('[name="base"], [name="taxRatio"]').attr('title',"請輸入數字最多到小數點第一位");
 	$('[name="deduct_base"]').attr('pattern',"^\\+?[1-9][0-9]*$");
 	$('[name="deduct_base"]').attr('title',"請輸入整數");
 })
 function formvalid(frm){
 	/*if($('[name="title"]').val()==""){	alert("請輸入名稱");return false;}
 	if($('[name="base"]').length ==1 && !NumValidate($('[name="base"]').val())) return false;
 	if($('[name="deduct_base"]').length ==1 && !NumValidate($('[name="deduct_base"]').val())) return false;*/
	return true;
 }
 	
 </script>
</Head>

<body class="page">



<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>
<?php if(count($yearList) == 0) { ?>
	<tr><td align="right" class="colLabel"></td><td style="color:red:font-size:16px;">無可新增的年份,或是尚未試算"年資/特休"</td></tr>
<?php } ?>
	
	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><?
  	 	switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  			
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : 
				echo "<select id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=> $v) echo "<option value='$k'>$v</option>"; echo "</select>"; 
				break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>

	
  		</td></tr>
<?php if($_REQUEST['type'] == 'year_end') { ?>
	<tr>
  		<td colspan="2" class="colLabel" align="center">
  			<div style="margin-top:10px;width:840px;">
	  			<h3 style="float: left;">不發放對象 : </h3>
	  			<table class="yearTab" cellpadding="0" cellspacing="0">
	  				<tr>
	  					<th>對象</th>
					    <th>條件</th>					    
					</tr>
					<tr>
						<td>離職</td>
						<td>
							於日期
							<input style="width:150px" name="no[leave]" type="text" class="queryDate" id="no[leave]" size="10" value="<?php echo ($annualY).'/12/31'?>" required pattern="[0-9]{4}/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01])" title="ex:2017/12/31">前離職者
						</td>
					</tr>
					<tr>
						<td>年資</td>
						<!--<td>
							未滿<input type="text" name="no[year]" style="width:80px;" required pattern="^\+?[1-9][0-9]*$" title="請輸入正整數" value="1">月
						</td>-->
						<td>
							於日期
							<input style="width:150px" name="no[hireDate]" type="text" class="queryDate" id="no[hireDate]" size="10" value="<?php echo ($annualY).'/12/01'?>" required pattern="[0-9]{4}/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01])" title="ex:2017/12/01">後到職者
						</td>
					</tr>
					<tr>
						<td>事假</td>
						<td>
							全年度請事假超過<input type="text" name="no[thing]" style="width:80px;" required pattern="^\+?[1-9][0-9]*$" title="請輸入正整數" value="23">(含)日
						</td>
					</tr>
	  			</table>
	  		</div>
	  	</td>
	</tr>

  	<tr>
  		<td colspan="2" class="colLabel" align="center">
  			<div style="margin-top:10px;width:840px;">
	  			<h3 style="float: left;">發放基數設定 : </h3>
	  			<table id="assessTable" class="yearTab" cellpadding="0" cellspacing="0">
	  				<tr>
					    <th class="lineTh">
					    	<div class="out">
					    		<b>考核等級</b>
					    		<span class="line"></span>
					    		<em>職銜</em>
					    	</div>
					    </th>
					    <th>優</th>
					    <th>佳</th>
					    <th>可</th>
					    <th>差</th>
					    <th width="80px"></th>
					</tr>
					<tr data-num="0">
						<td>一般員工</td>
						<td><input type="text" name="grade4[0]" value="0" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td><input type="text" name="grade3[0]" value="0" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td><input type="text" name="grade2[0]" value="0" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td><input type="text" name="grade1[0]" value="0" required pattern="^[0-9]+(.[0-9]{1,1})?$" title="請填數字"></td>
						<td>
							<input type="hidden" name="jobID[0]" value="0" data-job>
							<a href="javascript:void(0)" onclick="openWindow('assess')">新增</a>
						</td>
					</tr>
	  			</table>
	  		</div>
  			
  		</td>
  	</tr>
  	<tr>
  		<td colspan="2" class="colLabel" align="center">
  			<div id="specialDiv" style="margin-top:10px;width:840px;">
	  			<h3 style="float: left;">特殊發放規則 :<a href="javascript:void(0)" onclick="openSpecial();">新增</a> </h3>
	  			
	  		</div>
  		</td>
  	</tr>
<?php } ?>
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
      	<input type="hidden" name="type" id="type" value="<?=$_REQUEST['type']?>" />
		<input type="submit" value="確定新增" class="btn">&nbsp;
		<input type="reset" value="重新輸入" class="btn">&nbsp;
		<input type="button" value="取消新增" class="btn" onClick="history.back()">&nbsp;
		</td>
	</tr>
</table>
</form>
<div id='d_setting' class='divBorder1'>
	<div class='divTitle'>年資條件<input type="button" value="取消" style="float:right;cursor:pointer;margin-left:5px;" onclick="$('#d_setting').hide()"><input type="button" value="確定" onclick="selectYear()" style="float:right;cursor:pointer;"></div>
	<div class='divBorder2'>
	  <div class="divf1">
		<h4>選擇類型:</h4>
		<ul class="browser">
<?php
	foreach ($jobClass as $key => $value) {
		echo "<li><label><input type='checkbox' name='jClass[]' value='".$key."' data-name='".$value."'>$value</label></li>";
	}

?>	
		</ul>
	  </div>
	  <div class="divf2">
		<h4>選擇年資:</h4>
		<ul class="browser">
<?php 
    	foreach ($years as $key => $value) {
    		echo "<li><label><input type='checkbox' name='year[]' value='".$key."' data-name='".$value."'><span class='spanIm'></span><a>$value</a></label></li>"; 
    	}
?>
		</ul>
	  </div>
	</div>

</div>
<script type="text/javascript" src="grade.js"></script>
<script type="text/javascript">
	$(function(){
		$('#d_setting').draggable();
		
	});
</script>
</body>
</html>
