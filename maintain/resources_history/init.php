<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include("../../config.php");
	include('../../miclib.php');	
	$pageTitle = "資源資料庫管理";
	$tableName = "resources";
	$tableKey = "id";
	$foreignTable = "resource_detail";
	$foreignKey = "RID";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	
	// Here for List.asp ======================================= //
	/*
	if($_SESSION['privilege']<=100)	{
		$PRIVILEGE = array(1=>'基本帳號',100=>'部門管理');
	} else $PRIVILEGE = array(1=>'基本帳號',10=>'部門管理',100=>'機構管理',110=>'人事',255=>'系統總管理');
	*/
	$county = array('A'=>'台北市','B'=>'台中市','C'=>'基隆市','D'=>'台南市','E'=>'高雄市','F'=>'新北市','G'=>'宜蘭縣',
 'H'=>'桃園縣','I'=>'嘉義市','J'=>'新竹縣','K'=>'苗栗縣','M'=>'南投縣','N'=>'彰化縣','O'=>'新竹市','P'=>'雲林縣',
 'Q'=>'嘉義縣','T'=>'屏東縣','U'=>'花蓮縣','V'=>'台東縣','W'=>'金門縣','X'=>'澎湖縣','Z'=>'連江縣');
 
	$defaultOrder = "id Desc";
	$searchField1 = "name";
	$searchField2 = "empName";
	$pageSize = 20;	//每頁的清單數量
	
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,name,Title,R_Date,empName,isDel");
	$ftAry = explode(',',"編號,名稱,資源類別,修改日期,操作者,紀錄狀態");
	$flAry = explode(',',"60,,,,,");
	$ftyAy = explode(',',"ID,text,text,text,text,select");
	$isDel = array(0=>"最新紀錄",1=>"歷史紀錄");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"isReady");
	$editftA = explode(',',"地圖顯示");
	$editflA = array(
		'sub_class'=>'30',
		'email'=>'60',
		'address'=>'60',
		'PosCode'=>'5',
		'contact_address'=>'60',
		'experience'=>'60',
		'skill'=>'60',
		'area'=>'60',
		'target'=>'60',
		'business'=>'60',
		'apply_doc'=>'60',
		'service_status'=>'60',
		'notes'=>'60',
		'notes2'=>'60',
		'notes3'=>'60',
		'notes4'=>'60',
		'notes5'=>'60',
		'notes6'=>'60',
		'notes7'=>'60',
		'file1'=>'60'		
	);
	$editfhA = array(
		'experience'=>'2',
		'skill'=>'2',
		'business'=>'3',
		'apply_doc'=>'5',
		'service_status'=>'5',
		'notes'=>'3',
		'notes2'=>'3',
		'notes3'=>'3',
		'notes4'=>'3',
		'notes5'=>'3',
		'notes6'=>'3',
		'notes7'=>'3'		
	);
	$editfvA = array(
		'county'=>$county,
		'isReady'=>1
	);
	$editetA = array(
		'city'=>'select',
		'county'=>'select',
		'experience'=>'textbox',
		'skill'=>'textbox',
		'business'=>'textbox',
		'apply_doc'=>'textbox',
		'service_status'=>'textbox',
		'notes'=>'textbox',
		'file1'=>'multiPhoto',		
		'isReady'=>'checkbox'
	);
?>