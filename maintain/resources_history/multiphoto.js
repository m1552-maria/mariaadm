$(function() {
	$(".multiPhoto").change(function(){
		var that = $(this);	//old file input
		var oTD = that.parents("td").eq(0);
		$(this.files).each(function(){
			var fReader = new FileReader();
			fReader.readAsDataURL(this);
			fReader.onloadend = function(event){
				var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);	//最後一項
				oItem.after(oItem.clone(true)).show();

				var img = oItem.find("img").get(0);
				img.src=event.target.result;
				oItem.get(0).input = that;
				
				//new一份新的Input file
				var oNewFile = that.clone(true);
				that.hide().after(oNewFile);
			}
		});
	});
	$(".photoDel").click(function(){
		var oItem = $(this).parents(".photoItem").eq(0);
		if (oItem.get(0).input) oItem.get(0).input.remove();
		oItem.remove();
	});
});

var delfns = new Array(); 
function delPhoto(elm,fn) {
	//elm.form.delfns.value .= fn ;
	delfns.push(fn);
	var oItem = $(elm).parents(".photoItem").eq(0);
	if (oItem.get(0).input) oItem.get(0).input.remove();
	oItem.remove();
}

function formValid(tfm) { 
	tfm.delfns.value = delfns.join();
	return true; 
}