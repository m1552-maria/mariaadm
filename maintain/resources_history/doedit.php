<?php
	@session_start();
	include("init.php");
	
	//處理上傳的新圖檔
	$rdID = $_REQUEST['r_id'];
	$ulpath = "../../data/resources/$_REQUEST[ClassID]/";
	$dstfn = $ulpath.$rdID."/"; if(!is_dir($dstfn)) mkdir($dstfn, 0755); 
	foreach($_FILES as $k=>$v) {
		if( is_array($_FILES[$k]['name']) ) {
			$fnLst = array();	//多檔上傳
			foreach($_FILES[$k]['name'] as $k2=>$v2) {
				$fn = $v2;
				if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
				if ($_FILES[$k][tmp_name][$k2] != "") {
					if (!move_uploaded_file($_FILES[$k]['tmp_name'][$k2],"$dstfn$fn2")) { //上傳失敗
						echo '附件上傳作業失敗 !'; exit;
					}	else $fnLst[] = $fn;
				}				
			}
		} else {
			$fn = $_FILES[$k]['name'];
			if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
			if ($_FILES[$k][tmp_name] != "") {
				if (!move_uploaded_file($_FILES[$k]['tmp_name'],"$dstfn$fn2")) { //上傳失敗
					echo '附件上傳作業失敗 !'; exit;
				}	
			}
		}	
	}
	
	//刪除原先的檔案
	$orgfns = $_REQUEST[orgfns];
	if($orgfns) {
		$ba = explode(',',$orgfns);		
		if($_REQUEST[delfns]) {
			$aa = explode(',',$_REQUEST[delfns]);
			foreach($aa as $v) {
				$fnn = $v;
				if(checkStrCode($fnn,'utf-8')) $fnn=iconv("utf-8","big5",$fnn); 
				$fnn = $dstfn.$fnn;
				if(file_exists($fnn)) @unlink($fnn);
				for ($i=0; $i<count($ba); $i++) if($ba[$i]==$v) { unset($ba[$i]); break; }
			}
		}		
	}
	

	if($fnLst) {
		if($ba)	$fnLst = array_merge($fnLst,$ba);
	} else $fnLst = $ba;
	
	//先查詢類別檔所有欄位
	$sql = "select * from resource_class where ID='".$_REQUEST["ClassID"]."'";
	$rsC = db_query($sql, $conn);
	$rC = db_fetch_array($rsC);
	$aFields = json_decode($rC["fields"]);
	$editfnA = array();
	foreach($aFields as $k=>$v) { $editfnA[] = $k; $editftA[] = urldecode($v); }
	
	//更新
	$sql = "update $foreignTable set ";
	$fields = "";
	$n = count($editfnA);
	for($i=0; $i<$n; $i++) {
		$fldv = "'".$_REQUEST[$editfnA[$i]]."'";
		switch($editetA[$editfnA[$i]]) {
			case "hidden" :
			case "select" :
			case "readonly" :
			case "textbox":
			case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
			case "textarea" :	$fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
			case "checkbox" : if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
			case "file" : $fldv = "'$fn'"; break;
			case "multiPhoto" : $fldv = "'".join(',',$fnLst)."'"; break;
		}
		$fields = $fields.$editfnA[$i].'='.$fldv;
		if ($i<($n-1)) $fields = $fields.',';			
	}
	$sql = $sql.$fields." where ID='".$_REQUEST["selid"]."'";
	//echo $sql;  exit;

	db_query($sql, $conn);
	db_close($conn);	
	header("Location: list.php?r_id=$rdID&pages=".$_REQUEST['pages']."&keyword=".$_REQUEST['keyword']."&fieldname=".$_REQUEST['fieldname']."&sortDirection=".$_REQUEST['sortDirection']."&selid=".$_REQUEST['selid']);
?>
