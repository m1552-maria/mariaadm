<?
	@session_start();
	error_reporting(0);
	include("init.php");
	$ID = $_REQUEST['ID'];	//detail ID
	$sql = "select * from resources r, resource_detail rd where rd.RID=r.id And rd.ID='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
	//override $ulpath for List ALL (session reason)
	$ulpath = "../../data/resources/$r[ClassID]/$id/";
	
	$sql = "select * from resource_class where ID='".$r["ClassID"]."'";
	$rsC = db_query($sql, $conn);
	$rC = db_fetch_array($rsC);
	
	$aFields = json_decode($rC["fields"]);
	foreach($aFields as $k=>$v) { $aFld[] = $k; $aName[] = urldecode($v); }
	$editfnA = array_merge($aFld, $editfnA);
	$editftA = array_merge($aName, $editftA);
?>
	
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
 
 .photoItem { margin:5px 5px 5px 0; display:none; float:left; position:relative }
 .photoItem img { max-width:200px; max-height:200px; }
 .photoDel { position:absolute; top:0; left:0; z-index:99; cursor:pointer }
 .delPhoto { position:absolute; top:0; left:0; z-index:99; cursor:pointer }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script>
 <script src="multiphoto.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 編輯</title>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formValid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel">資源類別</td>
 		<td class="colContent"><?=$rC["Title"]?></td>
	</tr>

	<? for ($i=0; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? switch($editetA[$editfnA[$i]]) { 
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='".$editflA[$editfnA[$i]]."' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='".$editflA[$editfnA[$i]]."' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea> <input type='button' value='編輯' onClick='HTMLEdit($editfnA[$i])'>"; break;
			case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='".$editfvA[$editfnA[$i]]."'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='".$editflA[$editfnA[$i]]."' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='".$editflA[$editfnA[$i]]."' class='input'>"; foreach($editfvA[$editfnA[$i]] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$editfnA[$i]]==''?$r[$editfnA[$i]]:$editfvA[$editfnA[$i]]) ."'>"; break;
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='".$editflA[$editfnA[$i]]."' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";	break;
			case "multiPhoto"    : 
				echo "<input type='file' name='".$editfnA[$i]."[]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview'>";
				$orgfns = $r[$editfnA[$i]]; 
				if($orgfns) {
					$fnLst = explode(',',$orgfns);
					foreach($fnLst as $v) {
						echo "<div class='photoItem' style='display:block'><img src='".$ulpath.$r[$tableKey]."/".$v."' />"."<button type='button' class='delPhoto' onClick='delPhoto(this,\"$v\")'>刪除</button></div>";
					};
				}
				echo "<div class='photoItem'><img /><button type='button' class='photoDel'>取消</button></div>";
				echo "</div>";
				break;	
			default : echo "<input type='text' name='".$editfnA[$i]."' size='".$editflA[$editfnA[$i]]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
  	} ?></td>
  </tr><? } ?>
  <tr>
  	<td valign="top" class="colLabel" colspan="2">
		經度：<input name="Longitude" type="text" id="Longitude" size="12" value="<?=$r["longitude"]?>" />&nbsp;
    緯度：<input name="Latitude" type="text" id="Latitude" size="12" value="<?=$r["latitude"]?>" />&nbsp;
    <input name="Btn" type="button" id="Btn" value="用地址查詢座標" onClick="doSearchMap();" />
    <input type="hidden" name="orgfns" value="<?=$orgfns?>">
    <input type="hidden" name="delfns" value="">
  	<input type="hidden" name="selid" value="<?=$ID?>">
    <input type="hidden" name="r_id" value="<?=$_REQUEST['r_id']?>">
  	<input type="hidden" name="ClassID" value="<?=$rC["ID"]?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <button type="button" class="btn" onClick="history.back()">取消編輯</button>    
    
		<div id="map_canvas" style="width:100%; height:500px; margin-top:10px;"></div>
		<!--- AIzaSyDnz30ZdxGYG3cq66GSBb-AqK1N3zbzS64  --->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script type="text/javascript">
	$(function() {
		var nLon = <?=(empty($r["longitude"])) ? 120.649624 : $r["longitude"]?>;
		var nLat = <?=(empty($r["latitude"])) ? 24.162652 : $r["latitude"]?>;
		var sMessage = "<p style='width:300px; height:25px;'>請移動圖標到指定位置，即可查詢圖標經緯度</p>";
		
		var latlng = new google.maps.LatLng(nLat, nLon);
		var myOptions = {
			zoom: 16,
			center: latlng,
			zoomControlOptions: {
      	style: google.maps.ZoomControlStyle.LARGE
    	},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		var marker = new google.maps.Marker({
			map:map,
			draggable:true,
			animation: google.maps.Animation.DROP,
			position: latlng,
			title: sMessage
		});
		
		// To add the marker to the map, call setMap();
		marker.setMap(map);  
		
		var infowindow = new google.maps.InfoWindow({
			content: sMessage
		});
		infowindow.open(map,marker);
		
		google.maps.event.addListener(marker, 'dragend', getGPS);
		function getGPS(event) {
			document.getElementById('Latitude').value = marker.getPosition().lat().toFixed(6);
			document.getElementById('Longitude').value = marker.getPosition().lng().toFixed(6);
		}
		
		window.googlemap = map;
		window.googlemaker = marker;
	});

	function doSearchMap() {
		sAddr = "";
		$("input").each(function(){
			if ($(this).attr("name").indexOf("address") >= 0) sAddr = this.value;
		});
		if (sAddr == "") return false;
		
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( { 'address': sAddr}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				window.googlemap.setCenter(results[0].geometry.location);
				window.googlemaker.setPosition(results[0].geometry.location);
				document.getElementById('Latitude').value = results[0].geometry.location.lat().toFixed(6);
				document.getElementById('Longitude').value = results[0].geometry.location.lng().toFixed(6);
			}
		});
	}
</script>
	</td>
  </tr>
</table>
</form>
</body>
</html>
