<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?></title>
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script> 
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?r_id<?=$_REQUEST["r_id"]?>&pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else parent.location.href='edit.php?r_id=<?=$_REQUEST["r_id"]?>&ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}
	
	function chkSel() {
		if(selid==-1) {
			alert('請選擇一個項目！'); 
			return false;
		}
		else return true; 
	}
	function goBack() {
		location.href='../resources/list.php'; 
	}	
	function doRecovery() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else if(selid==-2) {
			return false;
		}
		else location.href='dorecovery.php?r_id=<?=$_REQUEST["r_id"]?>&ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	
	$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;
		
		<? if ($_REQUEST[id]) { ?>
			var obj = $('a[href*=<?=$_REQUEST[id]?>]')[0];
		<? } else { ?>
			var obj = $('a[del=0]')[0];
		<? } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
			evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
	});	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}
	
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="r_id" value="<?=$_REQUEST["r_id"]?>">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="if (chkSel()) {MsgBox(); } else {return false; }"><input type="button" name="action" value="←回復" class="<?=$bE?'sBtn':'suBtn'?>" onClick="if (chkSel()) doRecovery()"><input type="button" name="back" value="↑回上頁" class="<?=$bE?'sBtn':'suBtn'?>" style="width:68px" onClick="goBack()">
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr bgcolor="#336699" height="25" valign="bottom" align="center" bordercolordark="#336699">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	if (!empty($_REQUEST["r_id"])) $_SESSION["resources_history_r_id"] = $_REQUEST["r_id"];	else $_REQUEST["r_id"] = $_SESSION["resources_history_r_id"];
	$sql = "select rd.RID, rd.ID, rd.name, rc.Title, rd.R_Date, e.empName, rd.isDel "
			 . "from resources r, resource_detail rd, resource_class rc, emplyee e "
			 . "where e.empID=rd.empID And rc.ID=r.ClassID And rd.RID=r.id And r.id='".$_REQUEST["r_id"]."' and isDel=1";	//只選歷史紀錄
	//過濾條件
	if(isset($filter)) $sql .= " And ($filter)";
  // 搜尋處理
  if ($keyword!="") $sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%'))";
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";
	//echo $sql; exit;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r["ID"];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
			<a href="details.php?r_id=<?=$_REQUEST["r_id"]?>&id=<?=$r[ID]?>" del="<?=$r[isDel]?>" target="mainFrame"></a><?=$id?>
    </td>

		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "select": $tm=$$fnAry[$i]; echo $tm[$fldValue]; break;
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						default : echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>
<div align="right"><a href="#top">Top ↑</a></div>
</body>
</Html>