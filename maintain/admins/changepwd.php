<?php
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	
	if($_REQUEST[Submit]) {
		include("../../config.php");
		header("Content-type: text/html; charset=utf-8");
		$sql = "update admins set PWD='$_POST[pass]' where ACCOUNT='$_SESSION[Account]'";		
		db_query($sql,$conn);
		echo "<p>密碼已經變更成功 ！<br>下次登入請使用新的密碼。</p>";
		exit;
	}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>變更密碼</title>
<script type="text/javascript">
function formvalid(tfm) {
	var v = tfm.pass.value;	if(!v) { alert('密碼不能為空字串！'); return false; }
	var v2 = tfm.pass2.value; if(!v2) { alert('密碼不能為空字串！'); return false; }	
	if(v != v2) { alert('密碼再確認錯誤！'); return false; }	
	return true;
}
</script>
</head>

<body>
<h3>變更密碼</h3>
<form name="form1" method="post" onSubmit="return formvalid(this)">
  <table width="300" border="0" cellspacing="0" cellpadding="8">
    <tr>
      <td align="right">請輸入新密碼：</td>
      <td>
        <input type="password" name="pass" id="pass">
      </td>
    </tr>
    <tr>
      <td align="right">請再次確認：</td>
      <td><input type="password" name="pass2" id="pass2"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" id="Submit" value="送出"></td>
    </tr>
  </table>
</form>
</body>
</html>
