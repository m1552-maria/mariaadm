<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "帳號設定";
	$tableName = "admins";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	
	// Here for List.asp ======================================= //
	if($_SESSION['privilege']<=100)	{
		$filter = "PRIVILEGE<=100";	
		$PRIVILEGE = array(1=>'基本帳號',100=>'部門管理');
	} else $PRIVILEGE = array(1=>'基本帳號',10=>'部門管理',100=>'機構管理',110=>'人事',120=>'財務管理',130=>'預算管理',140=>'行政管理',141=>'行政管理1',142=>'行政管理2',150=>'出納管理',160=>'稅務管理',255=>'系統總管理'); 
	$defaultOrder = "ACCOUNT";
	$searchField1 = "ACCOUNT";
	$searchField2 = "NAME";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ACCOUNT,NAME,R_DATE,UNIT,PRIVILEGE");
	$ftAry = explode(',',"帳號,名稱,登錄日期,所屬單位,等級");
	$flAry = explode(',',"80,,,,");
	$ftyAy = explode(',',"ID,text,date,text,select");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"ACCOUNT,NAME,PWD,PRIVILEGE,isReady");
	$newftA = explode(',',"帳號,名稱,密碼,等級,帳號開放");
	$newflA = explode(',',",60,,");
	$newfhA = explode(',',",,,");
	$newfvA = array('','','',$PRIVILEGE,true);
	$newetA = explode(',',"text,text,text,select,checkbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ACCOUNT,NAME,PWD,PRIVILEGE,isReady");
	$editftA = explode(',',"帳號,名稱,密碼,等級,帳號開放");
	$editflA = explode(',',",60,,,");
	$editfhA = explode(',',",,,,");
	$editfvA = array('','','',$PRIVILEGE);	
	$editetA = explode(',',"text,text,text,select,checkbox");
?>