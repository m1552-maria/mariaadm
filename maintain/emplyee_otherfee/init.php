<?	
	session_start();
  header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	$bE = true;	//異動權限
	$pageTitle = "津貼設定";
	$tableName = "emplyee_otherfee";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$defaultOrder = "a.id";
	$searchField1 = "a.ofcTitle";
	$searchField2 = "a.empID";
	$searchField3 = "b.empName";
	$pageSize = 20;	//每頁的清單數量

	$state=array("0"=>"否","1"=>"是");
	// 注意 primary key should be first one
	// $fnAry = explode(',',"id,title,ofcID,action,wkMonth");
	// $ftAry = explode(',',"編號,名稱,雜項項目,運作,執行月份");
	// $flAry = explode(',',"50,,,,,");
	// $ftyAy = explode(',',"id,text,text,text,text");
	
	// Here for Edit.asp ============================================================================= //
	// $editfnA = explode(',',"id,ofcTitle,unitName,unitPrice,qty,wkMonth");
	// $editftA = explode(',',"編號,雜項名稱,雜項單位,雜項金額,數量,執行月份");
	// $editflA = explode(',',",,,,,,");
	// $editfhA = explode(',',",,,,,,");
	// $editfvA = array('','','','','','','');
	// $editetA = explode(',',"label,label,label,label,text,label");
?>