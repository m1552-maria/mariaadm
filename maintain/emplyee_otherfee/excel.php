<?php
	header("Content-type: text/html; charset=utf-8");
	session_start();
	//include class
	include '../../config.php';
	include '../inc_vars.php';
	require_once '../Classes/PHPExcel.php';
	require_once '../Classes/PHPExcel/IOFactory.php';
	include("../salary_bonus_export/getData.php");
  	include("getData.php");


	$wkMonth = $_REQUEST['year']. $_REQUEST['month'];
	
	$filterList = array();
	if($_REQUEST['projIDList'] != '') {
		$projIDList = explode(',', $_REQUEST['projIDList']);
		foreach ($projIDList as $key => $value) {
			$filterList[$value] = 'projID';
		}
	}
	if($_REQUEST['depIDList'] != '') {
		$depIDList = explode(',', $_REQUEST['depIDList']);
		foreach ($depIDList as $key => $value) {
			$filterList[$value] = 'depID';
		}
	}

	$result = getSalaryItemData($wkMonth, '', $filterList, 'otherfee');

	$header = $result['header'];
	$dataList = $result['dataList'];


	//設定變數
	$objPHPExcel = new PHPExcel();

	//抓部門
	$sql = "select depID,depName,depTitle from department";
	$rs = db_query($sql,$conn);
	$depName = array();
	$depTitle = array();
	while ($r = db_fetch_array($rs)){
		$depName[$r['depID']] = $r['depName'];
		$depTitle[$r['depID']] = $r['depTitle'];
	}
	

	if(empty($filename)) $filename = $_POST['year'].str_pad($_POST['month'],2,'0',STR_PAD_LEFT);
	
	

	$colEnd = "A";
	for($i = 0; $i<(count($header)-1); $i++){
		$colEnd++;
	}

	/*$Col = 'E';
	$startCol = array();
	// $SQL = "select ofcID,title from otherfee_class order by ofcID asc";
	$SQL = "select ofcID from otherfee_class order by ofcID asc";
	$rs = db_query($SQL,$conn);
	$countRow = db_num_rows($rs);
	while ($ofcID = db_fetch_array($rs)){
		$startCol[$ofcID['ofcID']] = $Col;
		$Col++;
	}*/

	//創建資料工作表
	$objPHPExcel->setActiveSheetIndex(0);
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->setTitle('雜項費用清冊');

	$i = 1;
	//基本資料內容
	$objPHPExcel->setActiveSheetIndex(0);
	$sheet->mergeCells('A'.$i.':'.$colEnd.$i);//合併
	$sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
	$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getFont()->setSize(14);

	$pCon = 0;
	$allTotal = 0;
	foreach ($dataList as $projID => $v1) {
		$titleTotal = 0;
		$i++;
		$sheet->mergeCells('A'.$i.':'.$colEnd.$i);//合併
		$sheet->setCellValue('A'.$i,'計畫編號：'.$projID);
		$i++;
		$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
		$letter = 65;
		foreach ($header as $key => $value) {
			$totalList[$key] = 0;
			$sheet->setCellValue(chr($letter).$i, $value);
			$letter++;
		}
		foreach ($v1 as $empID => $v2) {
			$i++;
			$letter = 65;
			$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
			foreach ($header as $key => $value) {
				/*echo "<pre>";
				print_r($header);
				print_r($v2);*/
				$setVal = 0;
				switch ($key) {
					case 'depID':
						$setVal = $depTitle[$v2['depID']];
						break;
					
					default:
						if(isset($v2[$key])) $setVal = $v2[$key];
						break;
				}

				switch ($key) {
					case 'empID':
					case 'empName':
					case 'depID':
					case 'wkMonth':
						$sheet->setCellValueExplicit(chr($letter).$i, $setVal);
						break;
					
					default:
						$sheet->getCell(chr($letter).$i)->setValueExplicit($setVal, PHPExcel_Cell_DataType::TYPE_NUMERIC);						
						break;
				}

				$sheet->getStyle(chr($letter).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
				$letter++;
			}
		}

		$i++;
		$letter = 65;
		$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'82D900'))); 	
		$sheet->setCellValueExplicit('A'.$i, '小計');
		$sheet->setCellValueExplicit('B'.$i, count($v1).'人');
		$pCon+=count($v1);
		foreach ($header as $key => $value) {
			if(isset($result['titleTotal'][$projID][$key])) {
				$m = $result['titleTotal'][$projID][$key];
				$titleTotal+=$m;
				$allTotal+=$m;
				$sheet->getCell(chr($letter).$i)->setValueExplicit($m, PHPExcel_Cell_DataType::TYPE_NUMERIC);	
				$sheet->getStyle(chr($letter).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
			} elseif($key == $value) {
				$sheet->getCell(chr($letter).$i)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_NUMERIC);	
				$sheet->getStyle(chr($letter).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
			}
			$letter++;
		}
		$letter-=1;
		$sheet->getCell(chr($letter).$i)->setValueExplicit($titleTotal, PHPExcel_Cell_DataType::TYPE_NUMERIC);	
		$sheet->getStyle(chr($letter).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
		$i++;
	}

	$i++;
	$letter = 65;
	$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'FF9224')));
	$sheet->setCellValueExplicit('A'.$i, '總計');
	$sheet->setCellValueExplicit('B'.$i, $pCon.'人');

	foreach ($header as $key => $value) {
		if(isset($result['allTotal'][$key])) {
			$m = $result['allTotal'][$key];
			$sheet->getCell(chr($letter).$i)->setValueExplicit($m, PHPExcel_Cell_DataType::TYPE_NUMERIC);	
			$sheet->getStyle(chr($letter).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
		} elseif($key == $value) {
			$sheet->getCell(chr($letter).$i)->setValueExplicit(0, PHPExcel_Cell_DataType::TYPE_NUMERIC);	
			$sheet->getStyle(chr($letter).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
		}
		$letter++;
	}
	$letter-=1;
	$sheet->getCell(chr($letter).$i)->setValueExplicit($allTotal, PHPExcel_Cell_DataType::TYPE_NUMERIC);	
	$sheet->getStyle(chr($letter).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );



/*	$countList = array();
	foreach ($header as $kk => $vv) {
	  $countList[$kk] = array(
		'count'=>0,
		'allCount'=>0
	  );
	}
  
	$i = 2;  // 列編號  $a.$i = A1 ;
	$costAlltotal = 0;
	$allPepele = 0; 
	$isWrite = false;
	foreach ($temp as $key => $value) {
		$isWritePeople = 0;
		foreach($value as $C => $D){
			if($D['total'] > 0) $isWrite = true;
		}
		// 該計畫所有人數的費用跑過以後，假如寫入變數=false 則跳過該迴圈
		if($isWrite == false) continue;
			$costTotal = 0;
			$a = 'A'; // excel $a欄位  => A欄位 => A1
			$merStart = $a.$i;
			$merEnd = $colEnd.$i;
			$sheet->mergeCells("$merStart:$merEnd"); 
			$sheet->setCellValue($a.$i, '計畫編號：'.$key);
			$i++;
			//標題
			foreach ($header as $k => $v) {
				$sheet->setCellValue($a.$i, $v);
				$a++;
				$countList[$k]['count'] = 0;
			}
			$i++;
		
		foreach ($value as $k => $v) {
			if($v['total'] == 0) { continue; }
			$isWritePeople++;
			$allPepele++;
			// var_dump($v);
				$a = 'A';
				foreach ($header as $k1 => $v1) {
					switch ($k1) {
						case 'empID':
							$sheet->setCellValueExplicit($a.$i, $k);
							break;

						case 'empName':
							$sheet->setCellValueExplicit($a.$i, $v[$k1]);
							break;

						case 'cost':
							$cost = $v['total'];
							$sheet->setCellValue($a.$i, $cost);
							$costAlltotal += $cost;
							$costTotal += $cost;
							break;

						case 'depID':
							if($depName[$v[$k1]]=='') $sheet->setCellValue($a.$i, $depTitle[$v[$k1]]);
							else $sheet->setCellValue($a.$i, $depName[$v[$k1]]);
							break;

						case 'wkMonth':	
							$sheet->setCellValueExplicit($a.$i, $v[$k1]);
							break;

						// case 'qty':
						// 	$sheet->setCellValueExplicit($a.$i, $v[$k1]);
						// 	break;

						// case 'unitPrice':
						// 	$sheet->setCellValueExplicit($a.$i, $v[$k1]);
						// 	break;

						default:
							if($v[$k1] != null){
								// var_dump($k1);
								// var_dump($v[$k1]);
								$sheet->setCellValue($a.$i, $v[$k1]);
								$countList[$k1]['count']+=$v[$k1];
								$countList[$k1]['allCount']+=$v[$k1];
							}else{
								$price = 0;
								$sheet->setCellValue($a.$i, $price);
								$countList[$k1]['count']+=$price;
								$countList[$k1]['allCount']+=$price;	
							}
							break;
					}
					$sheet->getStyle($a.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
					$a++;
				}
			$sheet->getStyle('A'.($i-1).':'.$colEnd.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
			$i++;
		}
		
			$sheet->setCellValue('A'.$i, '小計');
			$sheet->setCellValue('B'.$i, $isWritePeople.'人');

			// foreach($startCol as $a => $b){
			// 	$sheet->setCellValue($b.$i, $countList[$a]['count']);
			// }
			$A = 'E';
			foreach ($header as $kk => $vv) {
				if($kk == 'empID' || $kk == 'empName' || $kk == 'depID' || $kk == 'wkMonth' || $kk == 'cost') continue;
				$sheet->setCellValue($A.$i, $countList[$kk]['count']);
				$A++;
			}
			$sheet->setCellValue($colEnd.$i, $costTotal);

			$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'82D900'))); 	

			$i++;
			//跟上個單位的間隔
			$sheet->mergeCells("A$i:H$i"); 
			$i++;
	}

	$sheet->setCellValue('A'.$i, '總計');
	$sheet->setCellValue('B'.$i, $allPepele.'人');

	// foreach($startCol as $a => $b){
	// 	$sheet->setCellValue($b.$i, $countList[$a]['allCount']);
	// }
	$A = 'E';
	foreach ($header as $kk => $vv) {
		if($kk == 'empID' || $kk == 'empName' || $kk == 'depID' || $kk == 'wkMonth' || $kk == 'cost') continue;
		$sheet->setCellValue($A.$i, $countList[$kk]['allCount']);
		$A++;
	}
	$sheet->setCellValue($colEnd.$i, $costAlltotal);*/



	/*$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'82D900'))); 	
	$sheet->getStyle('A'.$i.':'.$colEnd.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'FF9224'))); */

	// $sheet->getColumnDimension('C')->setWidth('15');  
	// $sheet->getColumnDimension('D')->setWidth('16');  
	
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=".$filename.".xlsx");
	header("Pragma:no-cache");
	header("Expires:0");

	$objPHPExcel->setActiveSheetIndex(0);
	// Save Excel 2007 file 保存
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');
?>