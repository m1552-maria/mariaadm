<?php
	function getSalaryItemData($wkMonth, $search_list = '', $filterList = array(), $type = '') {
		$result = array();
		$dataList = array();

		$sql = "select i.*, i.hours as qty, r.empID, e.empName from emplyee_salary_report r inner join emplyee_salary_items i on r.id=i.fid left join emplyee as e on r.empID = e.empID where i.iType='".$type."' and r.wkMonth='".$wkMonth."'";
		if(trim($search_list) != '') $sql.= " and ".$search_list;
		$sql.= " order by e.projID, e.depID, e.empID ";
		$rs = db_query($sql);

	    $empIDList = array();
	    $result['header'] = array('empID'=>'編號','empName'=>'姓名','depID'=>'部門','wkMonth'=>'執行月份');
	    $title = '';
	    while ($r=db_fetch_array($rs)) {
	  		$empIDList[$r['empID']] = "'".$r['empID']."'";
	  		$dataList[$r['empID']][$r['wkMonth'].'_'.$r['id']] = $r;
	  		$rTitle = explode('-', $r['title']);
	  		$rTitle = $rTitle[0];
	  		if($title != $rTitle) {
	  			$title = $rTitle;
	  			$result['header'][$title] = $title;
	  		}

		}
		$result['header']['totalMoney'] = '費用總額';

		//取員工每月的計劃ID&部門ID
	    $empHistory = getEmpHistory($empIDList);
	    //塞入計劃ID&部門ID
	    $dataList = setProjIDAndDepID($dataList, $empHistory);
	    //整理資料
	    $dataList = arrangeData($dataList, $filterList);
	    $result['dataList'] = array();
	    $result['allTotal'] = array();
	    foreach ($dataList as $projID => $v1) {
	    	$result['titleTotal'][$projID] = array();
	    	foreach ($v1 as $empID => $v2) {
	    		foreach ($v2 as $t => $v3) {
	    			if(!isset($result['dataList'][$projID][$empID])) {
	    				$result['dataList'][$projID][$empID] = $v3;
	    				$result['dataList'][$projID][$empID]['totalMoney'] = 0;
	    			}
	    			$result['dataList'][$projID][$empID][$t] = $v3['money'];
	    			$result['dataList'][$projID][$empID]['totalMoney']+=$v3['money'];
	    			$result['titleTotal'][$projID][$t] += $v3['money'];
	    			$result['allTotal'][$t] += $v3['money'];
	    		}
	    	}
	    }


	    //$result['dataList'] 
	   /* echo "<pre>";
	    print_r($filterList);
	    print_r($result);
	    exit;*/

	    return $result;


	}

	//整理資料
	function arrangeData($dataList = array(), $filterList = array()) {
		/*ex:
			filterList = array(
				'30' => 'projID',
				'31' => 'projID',
				'A' => 'depID'
			);
		*/
		$result = array();
		$_data = array();
		if(count($filterList) > 0) $fCount = count($filterList);

		//篩選
		foreach ($dataList as $empID => $v1) {
			foreach ($v1 as $wkMonth => $v2) {
				$i = 1;
				if(count($filterList) > 0) {
					$i = $fCount;
					foreach ($filterList as $k => $v) {
						if(strpos((string)$v2[$v], (string)$k) === 0) break; //有符合搜尋計畫編號條件
          				$i--;
					}
				}
				if($i > 0) {
					$rTitle = explode('-', $v2['title']);
	  				$rTitle = $rTitle[0];
					$_data[$v2['projID']][$empID][$rTitle] = $v2;
				}

			}
		}

		//排序
		if(count($_data) > 0) {
			$sql = "select depID, projID from emplyee_history where 1 order by projID asc, depID asc";
    		$rs = db_query($sql);
    		$DPList = array();
    		while ($r=db_fetch_array($rs)) {
    			$DPList[$r['projID']] = $r['projID'];
    		}

    		foreach ($DPList as $key => $value) {
    			if(isset($_data[$key])) {
			    	$result[$key] = $_data[$key];
			    }
    		}
		}

		return $result;
	}
?>