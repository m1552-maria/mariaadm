<?	
	session_start();
    header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	include '../inc_vars.php';
	$bE = true;	//異動權限
	$pageTitle = "其他獎金項目維護";
	$tableName = "bonus_class";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bonus/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$defaultOrder = "bcID";
	$searchField1 = "title";
	$searchField2 = "money";
	$pageSize = 20;	//每頁的清單數量

	// 注意 primary key should be first one
	$fnAry = explode(',',"bcID,title,giveDate,money,type,invNo,notes");
	$ftAry = explode(',',"編號,項目名稱,發放日期,金額,所得格式,所得統編,備註");
	$flAry = explode(',',"50,,,,,,");
	$ftyAy = explode(',',"id,text,date,text,text,text,text");
	
	
	$invNOList = array(''=>'依各單位');
	foreach ($UniformNumbersProjID as $key => $value) {
		$invNOList[$key] = $key;
	}
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,giveDate,money,type,invNo,notes");
	$newftA = explode(',',"項目名稱,發放日期,金額,所得格式,所得統編,備註");
	$newflA = explode(',',",,,,,30");
	$newfhA = explode(',',",,,,,8");
	$newfvA = array('','','','',$invNOList,'');
	$newetA = explode(',',"text,date,text,text,select,textbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"bcID,title,giveDate,money,type,invNo,notes");
	$editftA = explode(',',"編號,項目名稱,發放日期,金額,所得格式,所得統編,備註");
	$editflA = explode(',',",,,,,,30");
	$editfhA = explode(',',",,,,,,8");
	$editfvA = array('','','','','',$invNOList,'');
	$editetA = explode(',',"text,text,date,text,text,select,textbox");
?>