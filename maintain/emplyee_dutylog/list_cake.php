<?php	
  @session_start();
  $Dayofweek = array('週日', '週一', '週二', '週三','週四','週五', '週六');

	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
  include('../../getEmplyeeInfo.php');

  if ($_SESSION['privilege']<=10) {
    $cman = $_SESSION['empID'];
  } else {
    $cman = $_SESSION['Account'];
  }

  $y = $_REQUEST['yy']?$_REQUEST['yy']:Date('Y');
  $m = $_REQUEST['mm']?$_REQUEST['mm']:Date('m');

	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "depID=''";
	}
	///if($_REQUEST['aType']) $temp[] = "aType='$_REQUEST[aType]'";
	if(count($temp)>0) $filter = join(' and ',$temp);

  //月餅打卡
  if($filter) $sql = "SELECT D.*,E.depID FROM `emplyee_dutylog` D left join emplyee E on D.empID=E.empID where dType like 'cake_%' and  d_datetime between '$y-$m-1' and '$y-$m-31 23:59:59' and $filter order by d_datetime DESC,empID";
  else $sql = "SELECT * FROM `emplyee_dutylog` where dType like 'cake_%' and  d_datetime between '$y-$m-1' and '$y-$m-31 23:59:59' order by d_datetime DESC,empID";
  //var_dump($sql);
  $rs = db_query($sql);
  $inf =array();
  while($r=db_fetch_array($rs)) {
    $empID=$r['empID'];
    $date = date('Y-m-d',strtotime($r[d_datetime]));
    $inf[$date][$empID][$r['dType']][]=$r['d_datetime'];
  }
  //print_r($inf); exit;  
  //排班資料
  $keyA = array_keys($inf);
  $eDate = $keyA[0];
  $bDate = end($keyA);
  $dutyA = array();
  $sql = "select * from emplyee_duty where startTime between '$bDate' and '$eDate 23:59:59'";
  $rs=db_query($sql);
  while($r= db_fetch_array($rs)) {
    $empID=$r['empID'];
    $date = date('Y-m-d',strtotime($r[startTime]));
    $st2 = $r['startTime2']?strtotime($r['startTime2']):'';
    $et2 = $r['endTime2']?strtotime($r['endTime2']):'';
    $dutyA[$date][$empID]=array(
      date('H:i',strtotime($r['startTime'])),
      date('H:i',strtotime($r['endTime'])),
      date('H:i',$st2),
      date('H:i',$et2) );
  }  
  //print_r($dutyA); exit; 
  $yy = intval(Date('Y'));
?>
<html>
<head>
<meta charset="utf-8">
<title>月餅打卡紀錄檢核表</title>
<style>
  select { font-size: 18px; }
  .Ccell { border-top-width: 0; border-bottom-width: 0; }
  .Fcell { border-bottom-width: 0; }
  .bar1 { position: absolute; top: 0px; right: 10px; font-size: 20px; font-weight: bold; }
  .hInp { width:40px}
  .header td { text-align: center; font-weight: bold; }
</style>
<script src="/scripts/jquery-3.3.1.js"></script>
<script src="/scripts/jquery.base64.js"></script>
<script src="/scripts/tableExport.js"></script>

<script>

</script>
</head>

<body>
<h2>
 <form name="form1">
  <select name="yy" onChange="this.form.submit()"><? for($i=$yy; $i>$yy-3; $i--) if($i==$y) echo "<option selected>$i</option>"; else echo "<option>$i</option>"; ?></select>
  <select name="mm" onChange="this.form.submit()"><? for($i=1;$i<13;$i++) { $sel=($m==$i?'selected':''); echo "<option $sel value='$i'>$i 月</option>"; } ?></select>
  <select name="depFilter" id="depFilter" onChange="this.form.submit()"><option value=''>-全部部門-</option>
  <? 
    if ($_SESSION['privilege'] > 10) {
      foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
    } else {
      foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
    }
  ?>
  </select>
  月餅打卡紀錄檢核表  
  <button onclick="$('#table1').tableExport({type:'excel',escape:false,fileName:'月餅打卡紀錄檢核表'})">匯出EXCEL</button>
 </form>  
</h2>

<form name="form2" action="list_cake_do.php" method="post">
<table id="table1" border='1' cellspacing='0' cellpadding='6'>
<tr bgcolor='#bbbbbb' class="header"><td>日期</td><td>員工</td><td>月餅到班</td><td>月餅離班<td>提前到班時間</td><td>延遲離班時間</td><td>加班核實時數</td></tr>
<? $counter=0; $ct=0; $time1 = microtime(true);
  foreach($inf as $k=>$v) {
    $color = ($counter%2) ? '#ffffff' : '#e0e0e0';  
    $counter2=0;
    foreach($v as $k2=>$v2) {
      $dateStr = $counter2?'':($k.'<br>'.$Dayofweek[date('w',strtotime($k))]);
      $clsNm = $counter2?'Ccell':'Fcell';
      echo "<tr><td bgcolor='$color' class='$clsNm'>$dateStr</td><td bgcolor='$color'>$k2<br>$emplyeeinfo[$k2]</td><td bgcolor='#ccffcc'>";
      $dv1 = 0; $dv2=0;
      foreach($v2['cake_in'] as $v3) {
        echo date('H:i',strtotime($v3))."<br>\r";
        $dv=round( (strtotime(date('Y-m-d 08:00:00',strtotime($v3)))-strtotime($v3)) / 60 );
        if($dv>$dv1) $dv1=$dv;
      }
      echo "</td>";
      echo "<td bgcolor='#ccffcc'>";
      foreach($v2['cake_out'] as $v3) {
        echo date('H:i',strtotime($v3))."<br>\r";
        $dv=round( ( strtotime($v3) - strtotime(date('Y-m-d 17:00:00',strtotime($v3))) ) / 60 );
        if($dv>$dv2) $dv2=$dv;
      }
      echo "</td>";
      // 排班資料 use Dictionary Way
      echo "<td bgcolor='#ffcccc'>".($dv1>29?$dv1.'分鐘':'')."</td>";
      echo "<td bgcolor='#ffcccc'>".($dv2>0?$dv2.'分鐘':'')."</td>";
      // 讀取紀錄資料
      $sql = "select * from emplyee_cake_log where workmonth='$y$m' and cman='$cman' and empID='$k2' and rdate='$k'";
      $rs = db_query($sql);
      $chked = ''; $hour = 0;
      if(!db_eof($rs)) {
        $r = db_fetch_array($rs);
        $hour = $r['hours'];
        $chked = "checked";
      }
      echo "<td bgcolor='#f0f0f0'>"."<input type='hidden' name='dates[]' value='$k'/><input type='hidden' name='empID[]' value='$k2'/><input name='hours[]' class='hInp' value='$hour'/>小時<input type='checkbox' name='cked[]' value='$ct' $chked/>確認"."</td></tr>";
      $counter2++; $ct++;
    }
    $counter++;   
  } ?>
</table>
<div style="padding:10px;">
  <input type="hidden" name="workmonth" value="<?=($y.$m)?>"/>
  <input type="submit" value="確定送出" style="font-size:16px"> 
  <input type="reset" value="重設" style="font-size:16px">
</div>
</form>
</body>
</html>