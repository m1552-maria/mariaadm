<?
  
  $filename = urlencode('打卡歷程表'.date("Y-m-d H:i:s",time()).'.xlsx');//ie中文檔名亂碼
  
  
  header("Content-type:application/vnd.ms-excel");
  header("Content-Type:text/html; charset=utf-8");
  header('Content-Disposition:attachment;filename='.iconv('utf-8', 'big5', $filename));//中文檔名亂碼的問題
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');



  if($_REQUEST['sqltx']) {
    //抓setting的資料
    $ip_list = array();
    $sql = ' select * from `punch_card_setting`';
    $rs = db_query($sql,$conn);
    while ($r=db_fetch_array($rs)) {
      $ip_list[$r['ip']] = $r['place'];
    }





    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);


    $sql = $_REQUEST[sqltx];

    $sql .= " order by a.d_datetime DESC";
    $rs = db_query($sql);

    $sheet = $objPHPExcel->getActiveSheet();

    //設定字型大小
    // $sheet->getStyle('A1')->getFont()->setSize(18);
    
    // 設定欄位背景色(方法1)
    $sheet->getStyle('A2:G2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $sheet->mergeCells("A1:G1");
    //對齊方式
    $sheet->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    


    $sheet->setCellValue('A1','打卡資料');


    //跑header 迴圈用
    $headerstart = 2;
    $header = array('A','B','C','D','E','F','G');
    $headervalue = array('流水號','員工編號','員工名稱','所屬部門','打卡狀態','打卡地點','打卡時間');
    
    foreach($header as $k=>$v){
      $sheet->setCellValue($v.$headerstart,$headervalue[$k]);   
    }
    $sheet->getStyle('A2:G2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
 
    $dType = array('duty_in'=>'上班','duty_out'=>'下班','cake_in'=>'月餅到班','cake_out'=>'月餅離班','overtime_in'=>'加班-上班','overtime_out'=>'加班-下班','outoffice_out'=>'外出','outoffice_back'=>'返回');

    $i = 3;
    $total = 1;
    while($rs && $r=db_fetch_array($rs)) {
      $sheet->setCellValue('A'.$i,$total);
      $sheet->setCellValueExplicit('B'.$i, $r['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
      $sheet->setCellValue('C'.$i,$r['empName']);
      $sheet->setCellValue('D'.$i,$departmentinfo[$r['depID']]);
      $sheet->setCellValue('E'.$i,$dType[$r['dType']]);
      $sheet->setCellValue('F'.$i,$r['place']);
      $sheet->setCellValue('G'.$i,$r['d_datetime']);


      $sheet->getStyle('A'.$i.':G'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

      $i++;
      $total++;
  
    }
   
   
    //設定欄寬
    $width = array(20,20,20,20,20,20,20);
    foreach($header as $k=>$v){
      $sheet->getColumnDimension($v)->setWidth($width[$k]);  
    }


    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->setPreCalculateFormulas(false);
    
    $objWriter->save('php://output');
  }






  
?>