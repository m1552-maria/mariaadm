<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../getEmplyeeInfo.php');
	
	//抓setting的資料
	$ip_list = array();
	$sql = ' select * from `punch_card_setting`';
	$rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) {
		$ip_list[$r['ip']] = $r['place'];
	}


	$bE = $_SESSION['privilege']>10?true:false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "打卡資料";
	$tableName = "emplyee_dutylog";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee_dutylog/';
	$delField = '';
	$delFlag = true;
	$imgpath = '../../data/emplyee_dutylog/';


	//日期
	if(isset($_REQUEST['start_day'])){
		$bDate = $_REQUEST['start_day'];
	}else{
		$bDate = $_REQUEST['start_day'] = date('Y/m/d',strtotime(date('Y/m/d').'-7 days'));
	}
	if(isset($_REQUEST['end_day'])){
		$eDate = $_REQUEST['end_day'];
	}else{
		$eDate = $_REQUEST['end_day'] = date('Y/m/d');	
	}

	// date('Y-m-d H:i:s',strtotime($employee_duty_list[$result[$key1]['empID']][$key2]['startTime']."-4 hour")
	if($_REQUEST[depFilter]) $temp[] = "b.depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="b.depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "b.depID=''";
	}
	if($_REQUEST['start_day']){
		$temp[] = 'a.d_datetime >= '."'".str_replace('/', '-', $_REQUEST['start_day'])."'";
	}
	if($_REQUEST['end_day']){
		$temp[] = 'a.d_datetime <= '."'".str_replace('/', '-', $_REQUEST['end_day']).' 23:59:59'."'";
	}
  if($_REQUEST['dTypeFilter']) $temp[] = "dType='".$_REQUEST['dTypeFilter']."'";

	if(count($temp)>0) $filter = join(' and ',$temp);
	
	$dType = array(
    'duty_in'=>'上班','duty_out'=>'下班',
    'cake_in'=>'月餅到班','cake_out'=>'月餅離班',
    'overtime_in'=>'加班-上班','overtime_out'=>'加班-下班',
    'outoffice_out'=>'外出','outoffice_back'=>'返回');

	// Here for List.asp ======================================= //
	$defaultOrder = "a.d_datetime";
	$searchField1 = "a.id";
	$searchField2 = "a.empID";
	$searchField3 = "b.empName";

	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,empID,dType,place,now_d_datetime,history");
	$ftAry = explode(',',"編號,員工編號,打卡狀態,打卡地點,打卡時間,紀錄");
	$flAry = explode(',',"80,,,,,,");
	$ftyAy = explode(',',"ID,text,text,text,date,history");
	
?>