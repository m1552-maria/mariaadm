<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../getEmplyeeInfo.php');	
	$pageTitle = "代換班紀錄";
	$tableName = "emplyee_duty_change";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	$temp = array();
	if ($_SESSION['privilege'] > 10) {	//::check if from maintainUser 
		$bE = true;	//異動權限
		if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
	}	else {
		if($_REQUEST[depFilter]) $temp[] = "depID like '$_REQUEST[depFilter]%'";
		else{
			if($_SESSION['user_classdef'][2]) {
				$aa = array();
				foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
				if(count($aa)) $temp[]="(".join(' or ',$aa).")"; else $temp[] = "depID=''";	
			} else $temp[] = "depID=''";	
		}
	}
	if(count($temp)>0) $filter = join(' and ',$temp);

	//defined funcs =============================================================== //
	function IDtoName($v,$row) {
		global $emplyeeinfo;
		$signed = $row['is_agent']?'<img src="checked.gif" style="vertical-align:middle"/>':'';
		return $emplyeeinfo[$v].$signed;
	}
	function IDtoNames($v,$row) {
		global $emplyeeinfo;
		$aa = explode(',',$v);
		$ba = array();
		$signV = $row['is_leadmen'];
		$pos = 0;
		foreach($aa as $id) {
			$signed = (pow(2,$pos) & $signV)?'<img src="checked.gif" style="vertical-align:middle"/>':'';	
			$ba[] = $emplyeeinfo[$id].$signed;
			$pos++;
		}
		return join(',',$ba);
	}
	function getDutyPeriod($v) {
		if(!$v) return "&nbsp;";
		$sqltx = "select startTime,endTime from emplyee_duty where id=$v";
		$rsX = mysql_query($sqltx);
		$rX = mysql_fetch_array($rsX);
		if($rX[0] && $rX[1]) return date('Y-m-d H:i',strtotime($rX[0])).'~'.date('H:i',strtotime($rX[1]));
		else return "&nbsp;";
	}

	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "empID";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,empID,dutyID,agent,dutyID2,leadmen,isOK");
	$ftAry = explode(',',"編號,換班人員,要換班的時段,換班對象,被換班的時段,需簽名者,已完成");
	$flAry = explode(',',"80,,,,,,30");
	$fvAry = explode(',',",emplyeeinfo,getDutyPeriod,IDtoName,getDutyPeriod,IDtoNames,");
	$ftyAy = explode(',',"ID,select,define,define,define,define,bool");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"ACCOUNT,NAME,PWD,PRIVILEGE,isReady");
	$newftA = explode(',',"帳號,名稱,密碼,等級,帳號開放");
	$newflA = explode(',',",60,,");
	$newfhA = explode(',',",,,");
	$newfvA = array('','','',$PRIVILEGE,true);
	$newetA = explode(',',"text,text,text,select,checkbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ACCOUNT,NAME,PWD,PRIVILEGE,isReady");
	$editftA = explode(',',"帳號,名稱,密碼,等級,帳號開放");
	$editflA = explode(',',",60,,,");
	$editfhA = explode(',',",,,,");
	$editfvA = array('','','',$PRIVILEGE);	
	$editetA = explode(',',"text,text,text,select,checkbox");
?>