<?php
	@session_start();
	include '../../config.php';	
	include 'init.php';
	header("Content-Type:text/html; charset=utf-8");
	$empID=$_GET['eid'];
	$admMd = strpos($_SERVER['HTTP_REFERER'],'maintain')===false ? false : true;
	if($admMd) { //取的時段資料
		$periodOptions = '';
		$rs = db_query("select * from duty_period");
		while($r=db_fetch_array($rs)) $periodOptions .= "<option value='".$r['startTime'].":00-".$r['endTime'].":00'>$r[title]</option>";
	}

	//取得異常表資料
	class dutyAbnormal { public $bDate; public $eDate; public $abStatus; public $id; public $notes;	public $normal;	}
	$sql="select * from emplyee_duty_abnormal where empID='$empID' ";
	/*20180111 sean 增加狀態類別 "上班或下班未打卡"*/
	$sql.="and abStatus in ('異常','遲到','早退','未打卡','上班或下班未打卡','未打卡-請假異常') ";
	$rs=db_query($sql);
	$abData=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if(!isset($abData[$r['fid']]))	$abData[$r['fid']]=array();
			$obj=new dutyAbnormal();
			$obj->bDate=$r['bDate'];
			$obj->eDate=$r['eDate'];
			$obj->abStatus=$r['abStatus'];
			$obj->id=$r['id'];
			$obj->notes=$r['notes'];
			$obj->normal=$r['normal'];
			array_push($abData[$r['fid']],$obj);
		}
	}

	//取的判讀後的打卡資料 ----------------------------------------------------------------
	$dateFrom = date('Y-m-d', strtotime('-6 months'));
	$sql="select * from emplyee_duty where empID='$empID' and (startTime>'$dateFrom' or startTime2>'$dateFrom' or endTime2>'$dateFrom')";
	$rs=db_query($sql);
	$evtlst = array();
	$dutyID = '';
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)) {
			$bD = $r['startTime']; 	$eD = $r['endTime'];
			$bD2= $r['startTime2'];	$eD2 = $r['endTime2'];
			/*20171218 sean 新增logBD2,logED2 用於顯示綠色打卡時間*/
			$logBD2 = $r['startTime2'];
			$logED2 = $r['endTime2'];
			/*20171218 sean edit*/
			if(!empty($r['dutyID'])) $dutyID = $r['dutyID'];
			$short_bD=date('H:i',strtotime($bD));  	$short_eD=date('H:i',strtotime($eD));
			$short_bD2=date('H:i',strtotime($bD2));	$short_eD2=date('H:i',strtotime($eD2));
			$title = '';
			if($bD2 && $eD2){ $title=$short_bD2."~".$short_eD2;
			}else if($bD2){ $title=$short_bD2."~ 無"; $logED2 = $bD2;
			}else if($eD2) {$title="無 ~".$short_eD2; $bD2=$bD2 ? $bD2 : ""; $logBD2 = $eD2; }
			$title2 = "$short_bD~$short_eD";
			if( $admMd && $bD>date('Y-m-d 23:59:59') ) $title2.=' [刪除]';
			$evtlst[] = "{title:'$title2',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#18F', borderColor :'#18F', id:'".$r['id']."'}";	//排班
			$evtlst[] = "{title:'$title',start:'$logBD2',end:'$logED2',allDay:true,backgroundColor:'#218C03', borderColor :'#218C03'}";	//打卡

			if(isset($abData[$r['id']])) {
				foreach($abData[$r['id']] as $k=>$v){
					$start=$bD2 ? $bD2 : $bD;
					$end=$eD2? $eD2 : $eD;
					if($v->normal==1){//異常已稽核完，轉正常
						$evtlst[] = "{title:'正常',start:'".$start."',end:'".$end."',allDay:false,backgroundColor:'#ff9900', borderColor :'#ff9900' , id:'".$v->id."'}";	
					}else{//異常
						$evtlst[] = "{title:'".$v->abStatus."',start:'".$start."',end:'".$end."',allDay:false,backgroundColor:'#e63900', borderColor :'#e63900' , id:'".$v->id."'}";	
					}
				}
			}

		}
	}
	$sql="select dutyID,title from dutys where dutyID=".$dutyID;
	$rs=db_query($sql);
	$duty_title = '';
	$duty_id = '';
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$duty_id=$r['dutyID'];
		$duty_title=$r['title'];
	}
	if(isset($_SESSION['duty_zoom']))	$zoom=$_SESSION['duty_zoom'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Details</title>
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link rel='stylesheet' type='text/css' href='/scripts/fullcalendar/fullcalendar.css' />
<link rel="stylesheet" href="<?php echo $extfiles?>list.css">
<style type="text/css">
body{	font-family:Verdana, Geneva, sans-serif,"微軟正黑體";	font-size: 14px;  background-repeat:no-repeat;}
.ui-widget {font-size:12px; }
.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
.title{	text-align:center;	background-color:#E1E1E1;}
.sTable{margin:10px;}
.colLabel{	width:120px;}
table.grid{  margin:5px;	border: 1px solid #ccc; border-collapse: collapse; }
table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; text-align:center;}
.btn{cursor:pointer;}
.divBorder2 { /* 內容區 第二層外框 */
border:1px solid #CCC;	background:#FFFFFF;	font-size:12px;}
.divTitle{	color:#fff;	background-color:#900;	border: 0px;	padding: 5px;	margin: 0px;	font-size:12px;}
.title-green{	background-color: #218C03;}
#d_setting_time{	position:fixed;	left:20%;	top:30%;	width:400px;	display:none;}
.dBottom{	background:#dedede; text-align:center;padding:2px;border:1px solid #ccc;}
#tb_setting_time{	width:98%;}
#d_setting_date{ display:none;position:fixed;top:40px;left:300px;	z-index:9999;	box-shadow: 12px 12px 10px rgba(0, 0, 0, 0.6);}
#d_dutylog{	display:none;position:fixed;top:40px;left:300px;	z-index:9999; }
#d_newduty{	display:none;position:fixed;top:40px;left:300px;	z-index:9999; }
#dutylog{max-height: 120px;	overflow:auto; }
/*.fc-event{    cursor: pointer;}*/
#calendar{	background-color:#fff;border:1px solid #888;}
.color_tip{	width:18px; height:18px;float:left;}
.text_tip{	float:left;}
#color_box{	margin:0 8px 4px 8px;}
.fc-event-time{   display : none;}
.tip{ color:#cc0000; }
.title-red{ color:#cc0000; font-weight: bold;}
.fc-header { margin-bottom: -17px }
table.fc-header { background-color:#f8f8f8; }
/* Calender */
.newDuty { background-image:url('/images/new.png'); background-repeat:no-repeat; background-position:center }
.title-sky{ background-color: #168FF9; font-weight: bold; font-size:larger;}
</style>
<script type="text/javascript" src="/Scripts/jquery.js"></script>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src='/scripts/fullcalendar/fullcalendar.min.js'></script>
<script type="text/javascript">
  var zoom='<?php echo isset($zoom) ? $zoom : '';?>';
  var ab_id=0;
  $(function() {
		$('#d_setting_date').draggable();	
		$("#d_dutylog").draggable({scroll: true });		
		$("#d_newduty").draggable({scroll: true });		
		$('#calendar').fullCalendar({
			header: {left:'prev,next today', center:'title', right:'month,agendaWeek,agendaDay'},
			editable: false,
			buttonText : { today: '今日',month: '月',agendaWeek: '周',	agendaDay: '日'	},
			monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			dayNames : ['周日','週一','週二','週三','週四','週五','週六'],
			dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],
			height: 500,
			dayRender: function(date,cell) {
				var aDate = date.toLocaleDateString();
				var flag1 = false;
				var now = new Date();
				if(date<now) return;
				$('#calendar').fullCalendar('clientEvents', function(event){ 
					if(!event.allDay) {
						var bDate = event.start.toLocaleDateString();
						if(bDate==aDate) flag1 = true;
					}
				});
				if(!flag1 && <?php echo  $admMd?'true':'false'?>) {
					if(now.getDate()>25) { 
						var eDate = now.getMonth() == 11 ? new Date(now.getFullYear()+1,0,1) : new Date(now.getFullYear(),now.getMonth()+1,1);
					} else {
						var days = new Date(now.getFullYear(),now.getMonth()+1,0).getDate();
						var eDate = new Date(now.getFullYear(),now.getMonth(),days);
					}
					if(date<eDate) cell.addClass('newDuty');
				}
			},
			eventRender: function(event,element) {
				var d = event.start;
				var mm = d.getMonth()+1; if(mm<10) mm = '0'+mm;
				var dd = d.getDate(); if(dd<10) dd = '0'+dd;
				var aDate = d.getFullYear()+'-'+mm+'-'+dd;
				//console.log(aDate);
				$('.fc-day[data-date="'+aDate+'"]').removeClass('newDuty');
			},
			dayClick: function(date,allDay,jsEvent,view) {
				var aDate = date.toLocaleDateString();
				var flag1 = false;
				var now = new Date();
				if(date<now) return;
				$('#calendar').fullCalendar('clientEvents', function(event){ 
					if(!event.allDay) {
						if (!event.start) return; 
						var bDate = event.start.toLocaleDateString();
						if(bDate==aDate) flag1 = true;
					}
				});
				//過去及下個月的日子不能新增
				if(!flag1 && <?php echo  $admMd?'true':'false'?>) {
					if(now.getDate()>25) { 
						var eDate = now.getMonth() == 11 ? new Date(now.getFullYear()+1,0,1) : new Date(now.getFullYear(),now.getMonth()+1,1);
					} else {
						var days = new Date(now.getFullYear(),now.getMonth()+1,0).getDate();
						var eDate = new Date(now.getFullYear(),now.getMonth(),days);
					}
					if(date<=eDate) {	
						$('#dutyDay').val(aDate);
						$("#d_newduty").show();
					}
				}
			},
			eventClick: function(event) {
				var obj=event;
				var date = obj['start'];
				if(obj['title']!="異常" && obj['title']!="遲到" && obj['title']!="早退" && obj['title']!="未打卡" && obj['title']!="上班或下班未打卡" && obj['title']!="未打卡-請假異常" && obj['title']!="正常"){//點擊排班
					if(new Date() < date && <?php echo  $admMd?'true':'false'?>) {
						if( confirm('你是否確定刪除這個班表?') ) {
							$.ajax({
								url: "api.php",
								method: "POST",
								dataType:"text",
								data: { act: "delDuty", id: obj['id'] }
							}).done(function(rs) {
								alert(rs);
								if(rs==1) location.href='list.php?eid=<?php echo $empID?>';
								else alert('刪除作業失敗!');
							});
						} 
					}
					//return;
				}else{//點擊異常 TODO:: 應檢查是否已經關帳，目前結構下，不好檢查
					ab_id=obj['id'];
					$("#tr_notes").show();
					$('#enter').show();
					$('#d_setting_date').show();
				}
				var y,M,d,h,sd,ed;
				if(date){
					y=date.getFullYear();	M=(date.getMonth()+1); d=date.getDate();
					sd=y+"/"+M+"/"+d;
				}else $('#m_startTime').html("<span class='tip'>沒有資料</span>");

				date = obj['end'];
				if(date){
					y=date.getFullYear();	M=(date.getMonth()+1); d=date.getDate();
					ed=y+"/"+M+"/"+d;
				}else $('#m_endTime').html("<span class='tip'>沒有資料</span>");

				if(obj['backgroundColor']=="#218C03"){//點擊打卡
					getDutylog(sd,ed);
				}else{//點擊異常
					$.ajax({
					  url: "api.php",
					  method: "POST",
					  dataType:"text",
					  data: { act: "get_abNotes", id: obj['id'] }
					}).done(function(rs) {
						var notes=rs.split("(by");
						$("#notes").val(notes[0]);
					});
				}
			}, 
			events: [ <?php echo join(',',$evtlst)?>	]
		});
	});

	function doCancel(id){	$('#'+id).hide();	}
	
	function chgPanel(){
		if(parent.parent.document.getElementsByTagName('frameset')[0].rows=="*,520"){//要縮小
			zoom='zoomOut';
		}else{
			zoom='zoomIn';
		}
	}
	function set_abNotes(){
		if(ab_id==0)	return;
		$.ajax({
		  url: "api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act: "set_abNotes", id: ab_id, notes:$("#notes").val()}
		}).done(function() {
			location.replace(location.href);
		});
	}
	function getDutylog(sd,ed){
  		var empID="<?php echo $empID?>";
  		$.ajax({
		  url: "api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act: "getDutylog", empID: empID, sd:sd , ed:ed }
		}).done(function(rs) {
			if(rs){
				var data=rs.split("#");
				var row;
				var tb="<table class='grid' style='width:98%'><tr><td width='100'>狀態</td><td>地點</td><td>打卡時間</td></tr>";
				for(var i=0;i<data.length;i++){
					row=data[i].split(",");
					tb+="<tr><td>"+row[0]+"</td><td>"+row[1]+"</td><td>"+row[2]+"</td></tr>";
				}
				tb+="</table>";
				$("#dutylog").html(tb);
			}else{
				$("#dutylog").html("NO DATA");
			}
			$("#d_dutylog").show();
		});
	}
</script>
</head>
<body bgcolor="#EEEEEE">
<div id='d_setting_date'>
	<div id='dTitle' class='divTitle' style="font-size:16px">異常原因</div>
	<div class='divBorder2' style='width:400px; background-color:#f0f0f0'>
		<table style='width:98%'>
			<tr><td style="color:#666">[說明]：<br>&nbsp;可直接輸入文字或在 <b>空白內容</b> 時 按下 [↓]鍵，選取常用打卡異常說明</td></tr>
			<tr id='tr_notes'><td style="padding:4px">
				<input type='text' id='notes' list='reason' style="width:100%"/>
				<datalist id="reason"><?php foreach($abNormalText as $v) echo "<option value='$v'/>";?></datalist>
			</td></tr>
		</table>
	</div>
	<div class='dBottom'>
		<input type="button" value='確定' id='enter' onclick='set_abNotes()'/>&nbsp;
		<input type="button" value='關閉' onclick='doCancel("d_setting_date")'/>
	</div>
</div>
<div id='color_box'>
	<span class='color_tip' style='background:#18F'></span><span class='text_tip'>排班&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<span class='color_tip' style='background:#00A800'></span><span class='text_tip'>打卡&nbsp;&nbsp;&nbsp;&nbsp;</span>
	使用中班表名稱：<span class='title-red'><?php echo $duty_title."(".$duty_id.")"?></span>
	<?php if($admMd) { ?>
	<span style="float: right;color:#18F"><img src="/images/info.png" style="vertical-align: text-bottom;"/>單擊日期有加號處可新增班表</span>
	<?php } ?>
</div>

<div id='calendar'></div>

<div id='d_dutylog'>
	<div class='divTitle title-green'>打卡紀錄</div>
	<div class='divBorder2' style='width:400px;' id="dutylog"></div>
	<div class='dBottom'><input type="button" value='關閉' onclick='doCancel("d_dutylog")'/></div>
</div>

<div id='d_newduty'>
<form id="form1" action="doNewDuty.php" method="POST">
	<div class='divTitle title-sky'>新增班表</div>
	<div class='divBorder2' style='width:200px;' id="dutylog">
		<table style='width:98%' align="center">
			<tr><td align="center">日期</td></tr>
			<tr><td align="center"><input id="dutyDay" name="dutyDay" type="text" value="" readonly size="16" style="background-color:#f0f0f0"/></td></tr>
			<tr><td align="center">請選擇時段</td></tr>
			<tr><td align="center"><select name="dutyPeriod"><?php echo $periodOptions?></select></td></tr>	
		</table>
		<br/>
	</div>
	<div class='dBottom'>
		<input type="hidden" name="empID" value="<?php echo $empID?>"/>
		<input type="submit" value='確定' id='enter' onclick=''/>&nbsp;
		<input type="button" value='關閉' onclick='doCancel("d_newduty")'/>
	</div>
</form>	
</div>

</body>
</html>