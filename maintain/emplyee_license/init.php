<?php 
	// for Access Permission Control
	if(!session_id()) session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = $_SESSION['privilege']>10?true:(strpos($_SERVER['HTTP_REFERER'],'community.php')?true:false);	//異動權限
	if((isset($_SESSION["canEditEmp"]) && $_SESSION['privilege']<=10) || $_SESSION['privilege']>10){
		$bE2=true;
	}else{
		$bE2=false;
	}
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	include '../inc_vars.php';
	$pageTitle = "證照/著作";
	$tableName = "emplyee_license";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee';
	$delField = 'path';
	$delFlag = false;
	
	// 需要記住的值
	if(isset($_REQUEST['eid'])) {
		$eid = $_REQUEST['eid'];
		$_SESSION['EE_EID'] = $eid;
	} else $eid = $_SESSION['EE_EID'];
	$filter = "empID='$eid'";
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "type";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,adate,vdate,type,notes,rdate");
	$ftAry = explode(',',"ID,名稱,取得日期,有效日期,形式,備註,登錄日期");
	$flAry = explode(',',"100,200,120,120,100,,200,");
	$ftyAy = explode(',',"id,text,date,date,text,text,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"empID,title,adate,vdate,type,notes,attach");
	$newftA = explode(',',"員工編號,名稱,取得日期,有效日期,形式,備註,相關文件上傳");
	$newflA = explode(',',",,,,,30,");
	$newfhA = explode(',',",,,,,3,");
	$newfvA = array('','','','','','');
	$newetA = explode(',',"empID,text,date,date,select,textbox,multiPhoto");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,adate,vdate,type,notes,attach");
	$editftA = explode(',',"ID,名稱,取得日期,有效日期,形式,備註,相關文件上傳");
	$editflA = explode(',',",,,,,,");
	$editfhA = explode(',',",,,,,,");
	$editfvA = array('','','','','','','');
	$editetA = explode(',',"id,text,date,date,select,textbox,multiPhoto");
?>