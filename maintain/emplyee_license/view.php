<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?php echo $extfiles?>list.css">
 <title><?php echo $pageTitle?></title>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?php echo $pages?>;		//目前所在頁次
	var selid = '<?php echo $selid?>';	
	var keyword = '<?php echo $keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?php echo $pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?php echo $pages?>+1; if(pp><?php echo $pageCount?>) pp=<?php echo $pageCount?>; break;
      case 3 : pp=<?php echo $pageCount?>;
		}
		location.href="<?php echo $PHP_SELF?>?pages="+pp+"&keyword=<?php echo $keyword?>&fieldname=<?php echo $fieldname?>&sortDirection=<?php echo $sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?php echo $keyword?>&fieldname=<?php echo $fieldname?>&sortDirection=<?php echo $sortDirection?>"; 
	}
	function clickEdit(aid) {	
		selid = aid;	
		<?php if($bE || $bE2){ ?> DoEdit();	<?php } ?>
	}	
 </script>
 <script Language="JavaScript" src="<?php echo $extfiles?>list.js"></script> 
</Head>

<body bgcolor="#EEEEEE">
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?php echo $pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?php echo $pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?php echo $bE2?'sBtn':'sxBtn'?>" onclick='location.href="append.php";'><input type="button" name="edit" value="－修改" class="<?php echo $bE2?'sBtn':'sxBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?php echo $bE?'sBtn':'sxBtn'?>" onClick="MsgBox()">
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><?php $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?php echo $ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr bgcolor="#336699" height="25" valign="bottom" align="center" bordercolordark="#336699">
	<?php for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?php echo $fnAry[$i]?>","<?php echo $sortFG?>","<?php echo $keyword?>");' class="head">
		<?php echo $ftAry[$i]?>&nbsp;&nbsp;<?php if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><?php if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><?php } ?>
		</a>
	</th>
	<?php } ?>
  </tr>
<?php
	//過濾條件
	if(isset($filter)) $sql="select * from $tableName where ($filter)"; else $sql="select * from $tableName where 1";
  // 搜尋處理
  if ($keyword!="") $sql.=" and ($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')";
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize"; 
  //echo $sql;
  $rs = db_query($sql);
  if($rs){
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?php echo $id?>" onMouseOver="SelRowIn('<?php echo $id?>',this)" onMouseOut="SelRowOut('<?php echo $id?>',this)" onClick="SelRow('<?php echo $id?>',this)"<?php if($id==$selid) echo ' class="onSel"'?>>
    <td <?php if ($flAry[0]>"") echo "width='".$flAry[0]."'"?> ><input type="checkbox" value="<?php echo $id?>" name="ID[]" <?php if (!empty($nextfg)) echo "checked" ?>>
			<a href="javascript: clickEdit('<?php echo $id?>')"><?php echo $id?></a>
    </td>
		<?php for($i=1; $i<count($fnAry); $i++) { ?>
    	<td  <?php if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<?php $fldValue = $r[$fnAry[$i]] ?>
			<?php if ($fldValue>"") switch ($ftyAy[$i]) {
						case "select": $tm=$$fnAry[$i]; echo $tm[$fldValue]; break;
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : 
							$fldValue = ($fldValue != '' && $fldValue != '0000-00-00') ? date('Y/m/d',strtotime($fldValue)) : '';
							echo $fldValue; 
							break;
						case "custom_date" : echo date('Y/m/d',strtotime($r['bdate'])).'-'.date('Y/m/d',strtotime($r['edate'])); break;
						default : 
							if($fnAry[$i]=="peoples"){
								echo $peoples[$fldValue];	
							}else{
								echo $fldValue;	
							}
			    } else echo "&nbsp";
			?>
			</td>
		<?php } ?>	
  </tr>    
<?php } 
  }?>   
 </table>
</form>
</body>
</Html>