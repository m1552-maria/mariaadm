<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <style>
  .popupPane {
    position:absolute; 
    left: 25%; top:5%;
    background-color:#888;
    padding: 8px;
    display:none;
  }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script> 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
	
	function stateform(id,name) {
		statefm.id.value = id;
		statefm.name.value = name;
		document.getElementById('div1').style.display = 'block';
	}
	
	function checkAll(elm) {
		var chk = $(elm).attr('checked');
		$('form input:checkbox').each(function(index, element) {
      $(element).attr('checked',chk);
    });
		
	}
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body bgcolor="#EEEEEE">
<div id="div1" class="popupPane">
  <form id="statefm" action="stateform.php">
  	<table bgcolor="#FFFFFF" cellpadding="2" cellspacing="0" style="font-size:12px">
    	<tr><th colspan="2" hight="16" bgcolor="#888" style="color:#FFF">保單名冊-異動作業</th></tr>
      <tr><td align="right">編號：</td><td><input type="text" name="id" /></td></tr>
      <tr><td align="right">姓名：</td><td><input type="text" name="name" id="name" /></td></tr>
      <tr><td align="right">退保時間：</td><td><input type="text" name="offDate" value="<?=date('Y/m/d')?>" /></td></tr>
      <tr><td align="right">退保人員：</td><td><input type="text" name="offMen" value="<?=$_SESSION[empName]?>" /></td></tr>
      <tr><td align="right">異動說明：</td><td><textarea name="notes" rows="2" cols="30"></textarea></td></tr>
      <tr><td colspan="2" align="center">
        <input type="submit" name="Submit" value="確定" />
        <button type="button" onClick="document.getElementById('div1').style.display='none'">取消</button>
      </td></tr>
    </table>  
  </form>
</div>

<div id="popup" class="popupPane">
<form id="fm" action="importExcel.php" method="post" enctype="multipart/form-data" style="margin:0">
  <input type="hidden" name="fid" value="<?=$fid?>"/>
  <table bgcolor="#F0F0F0" style="font-size:12px">
  <tr><td>選擇要匯入的Excel檔：<br/><span style="color:#666666; font-size:12px">請選擇格式為 xls 的 excel 檔案，內容必須遵守範本的排法。</span><br/><a href="templet.xls" target="new">下載範本</a></td></tr>
  <tr><td><input type="file" name="file1" size="30" /></td></tr>
  <tr height="40" valign="bottom"><td>
  	<input type="submit" name="Submit" value="匯入">
    <button type="button" onClick="document.getElementById('popup').style.display='none'">取消</button>
  </td></tr>
  </table>
</form></div>

<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='location.href="append.php";'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
		<input type="button" name="toExcel" value="Excel報表" onClick="excelfm.submit()">
    <input type="button" name="ExcelIn" value="Excel匯入" onClick="document.getElementById('popup').style.display='block'">
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
  	<? if($i==0) echo "<input type='checkbox' onClick='checkAll(this)'>"; ?>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  <th width="30">&nbsp;</th>
  </tr>
<?
	//過濾條件
	if(isset($filter)) $sql="select * from $tableName where ($filter)"; else $sql="select * from $tableName where 1";
  // 搜尋處理
  if ($keyword!="") $sql.=" and ($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')";
	// for excel output use.
	$outsql = $sql; //echo $outsql;
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>><a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a></td>

		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						default : echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
    
    <td><? if($r[isOff]==1) { ?>已退保<br><?=$r[notes]?><? } else { ?>[<a href="#" onClick="stateform(<?=$id?>,'<?=$r[name]?>')">退保</a>]<? } ?></td>
  </tr>    
<? } ?>   
 </table>
</form>

<form name="excelfm" action="excel.php" method="post" target="excel">
<input type="hidden" name="title" value="<?=$_REQUEST[title]?>">
<input type="hidden" name="sqltx" value="<?=$outsql?>">
</form>
</body>
</Html>