<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
		
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "平旅險 投保名單";
	$tableName = "insure_travel_list";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath   = '../../data/insurances/';
	$delField = 'ahead';
	$delFlag  = false;
	$imgpath  = '../../data/insurances/';
	
	// Here for List.asp ======================================= //
	if($_REQUEST[fid]) {
		$fid = $_REQUEST[fid];
		$_SESSION['insure_travel_list_fid'] = $fid;
	} else $fid = $_SESSION['insure_travel_list_fid'];
	$filter = "fid=$fid";
	
	$defaultOrder = "id";
	$searchField1 = "name";
	$searchField2 = "pid";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,name,pid,birthday,country,insfee,addMedical,ptype");
	$ftAry = explode(',',"編號,姓名,身分字號,生日,國籍,投保金額,附加醫療險,被保險人身分");
	$flAry = explode(',',"80,100,100,100,100,60,60,");
	$ftyAy = explode(',',"ID,text,text,date,text,text,text,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"name,pid,birthday,country,insfee,addMedical,ptype,fid");
	$newftA = explode(',',"姓名,身分字號,生日,國籍,投保金額,附加醫療險,被保險人身分,主檔編號");
	$newflA = explode(',',",,,,,,,");
	$newfhA = explode(',',",,,,,,,");
	$newfvA = array('','','','','','','',$fid);
	$newetA = explode(',',"text,text,date,text,text,text,text,readonly");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,name,pid,birthday,country,insfee,addMedical,ptype,isOff,notes");
	$editftA = explode(',',"編號,姓名,身分字號,生日,國籍,投保金額,附加醫療險,被保險人身分,已退保,退保原因");
	$editflA = explode(',',",,,,,,,,,50");
	$editfhA = explode(',',",,,,,,,,,");
	$editetA = explode(',',"id,text,text,date,text,text,text,text,checkbox,text");
?>