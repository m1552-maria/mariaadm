<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
  	$sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 </style>
<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 編輯</title>
 <script type="text/javascript">
	function formvalid(frm){
		if($("#title").val()==""){	alert("請輸入時段名稱");return false;}
		var start_hr=$('#start_hr').val();
		var start_minute=$('#start_minute').val();
		var end_hr=$('#end_hr').val();
		var end_minute=$('#end_minute').val();
		if(start_hr==end_hr && parseInt(start_minute)>=parseInt(end_minute)){	alert('開始時間與結束時間有誤，請重新選擇');	return false;}
		$("#startTime").val(start_hr+":"+start_minute);
		$("#endTime").val(end_hr+":"+end_minute);
		return true;
	}
</script>
</Head>
<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<? for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? 
		switch($editetA[$i]) { 
  	  		case "text" :echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  			case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : 
				$timeAry=explode(":",$r[$editfnA[$i]]);
				if($editfnA[$i]=="startTime"){				
					echo "<select name='start_hr' id='start_hr' size='$editflA[$i]' class='input'>"; 
					foreach($hr as $k=> $v) {
						if($v==$timeAry[0]){
							echo "<option value='$v' selected>$v</option>"; 
						}else{
							echo "<option value='$v'>$v</option>"; 
						}
					}
					echo "</select>&emsp;時&emsp;"; 
					echo "<select name='start_minute' id='start_minute' size='$editflA[$i]' class='input'>"; 
					foreach($minute as $k=> $v){
						if($v==$timeAry[1]){
							echo "<option value='$v' selected>$v</option>"; 
						}else{
							echo "<option value='$v'>$v</option>"; 
						}
					}
					echo "</select>&emsp;分"; 
				}else if($editfnA[$i]=="endTime"){
					echo "<select name='end_hr' id='end_hr' size='$editflA[$i]' class='input'>"; 
					foreach($hr as $k=> $v) {
						if($v==$timeAry[0]){
							echo "<option value='$v' selected>$v</option>"; 
						}else{
							echo "<option value='$v'>$v</option>"; 
						}
					}
					echo "</select>&emsp;時&emsp;"; 
					echo "<select name='end_minute' id='end_minute' size='$editflA[$i]' class='input'>"; 
					foreach($minute as $k=> $v){
						echo $v.",".$timeary[1]."<br>";
						if($v==$timeAry[1]){
							echo "<option value='$v' selected>$v</option>"; 
						}else{
							echo "<option value='$v'>$v</option>"; 
						}
					}
					echo "</select>&emsp;分"; 
				}
				break;
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				break;
			default:	echo $r[$editfnA[$i]];	break;
			
  	} ?></td>
  </tr>
  <? 
  } 
  
  ?>

<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
    <input type="hidden" name="startTime" id="startTime"/>
    <input type="hidden" name="endTime" id="endTime"/>
    <input type="hidden" name='week_day_v' id='week_day_v'>
    <input type="hidden" name='tData' id='tData'>
	<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <input type="button" value="取消編輯" class="btn" onClick="history.back()">&nbsp;
  </td>
</tr>
</table>

</form>
</body>
</html>
