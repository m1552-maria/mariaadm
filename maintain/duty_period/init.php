<?	
	session_start();
    header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	$bE = true;	//異動權限
	$pageTitle = "排班時段";
	$tableName = "duty_period";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "id";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key should be first one
	$fnAry = explode(',',"id,title,startTime,endTime");
	$ftAry = explode(',',"編號,時段名稱,開始時間,結束時間");
	$flAry = explode(',',"50,,,");
	$ftyAy = explode(',',"id,text,text,text");
	
	$hr=array();
	for($j=0;$j<24;$j++){	
		if($j<10) array_push($hr,'0'.$j);	else	array_push($hr,$j);
	}	
	$minute=array();
	for($j=0;$j<60;$j++){	
		if($j<10) array_push($minute,'0'.$j);	else	array_push($minute,$j);
	}	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,startTime,endTime");
	$newftA = explode(',',"時段名稱,開始時間,結束時間");
	$newflA = explode(',',"20,,");
	$newfhA = explode(',',",,");
	$newfvA = array('','','');
	$newetA = explode(',',"text,select,select");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,startTime,endTime");
	$editftA = explode(',',"編號,時段名稱,開始時間,結束時間");
	$editflA = explode(',',",,,");
	$editfhA = explode(',',",,,");
	$editfvA = array('','','','','');
	$editetA = explode(',',"text,text,select,select");
?>