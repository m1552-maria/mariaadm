<?php
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 新增</title>
 <script type="text/javascript">
 function formvalid(frm){
	if($("#title").val()==""){	alert("請輸入時段名稱");return false;}
	var start_hr=$('#start_hr').val();
	var start_minute=$('#start_minute').val();
	var end_hr=$('#end_hr').val();
	var end_minute=$('#end_minute').val();
	if(start_hr==end_hr && parseInt(start_minute)>=parseInt(end_minute)){	alert('開始時間與結束時間有誤，請重新選擇');	return false;}
	$("#startTime").val(start_hr+":"+start_minute);
	$("#endTime").val(end_hr+":"+end_minute);
	return true;
 }
 </script>
</Head>

<body class="page">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>

	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><? switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' name='$newfnA[$i]' id='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  		case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : 
				if($newfnA[$i]=="startTime"){				
					echo "<select name='start_hr' id='start_hr' size='$newflA[$i]' class='input'>"; 
					foreach($hr as $k=> $v) {
						if($v=="08") $selected='selected'; else $selected='';
						echo "<option value='$v' $selected>$v</option>"; 
					}
					echo "</select>&emsp;時&emsp;"; 
					echo "<select name='start_minute' id='start_minute' size='$newflA[$i]' class='input'>"; 
					foreach($minute as $k=> $v) echo "<option value='$v'>$v</option>"; 
					echo "</select>&emsp;分"; 
				}else{
					echo "<select name='end_hr' id='end_hr' size='$newflA[$i]' class='input'>"; 
					foreach($hr as $k=> $v) {
						if($v=="16") $selected='selected'; else $selected='';
						echo "<option value='$v' $selected>$v</option>"; 
					}
					echo "</select>&emsp;時&emsp;"; 
					echo "<select name='end_minute' id='end_minute' size='$newflA[$i]' class='input'>"; 
					foreach($minute as $k=> $v) echo "<option value='$v'>$v</option>"; 
					echo "</select>&emsp;分"; 
				}
				break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<input type="submit" value="確定新增" class="btn">&nbsp;
            <input type="reset" value="重新輸入" class="btn">&nbsp;
            <button type="button" class="btn" onClick="history.back()">取消新增</button>
		</td>
	</tr>
    <input type="hidden" name="startTime" id="startTime"/><input type="hidden" name="endTime" id="endTime"/>
</table>
</form>
</body>
</html>
