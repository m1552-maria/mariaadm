<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else{ 			
			location.href="edit.php?ID="+selid;
		}
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
	function doAppend(){
		location.href="append.php"
	}
	function setClose(visitId){
		location.href="booking_close.php?visitId="+visitId;
		
	}
 </script>
 <script Language="JavaScript" src="list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="95%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='doAppend()'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?><? } ?>
		</a>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)) $sql="select * from $tableName where ($filter)"; else $sql="select * from $tableName where 1";
  // 搜尋處理
  if ($keyword!="") $sql.=" and ($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')";
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize"; 
  //echo $sql;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]]; 
?>
  <tr valign="top" id="tr_<?=$id?>" style='cursor:pointer;' onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this,'<?=$r['uType']?>')"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>>
            <a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a>
    </td>

		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? 
			if ($fldValue>"") {
				switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						default :	if ($fnAry[$i]=="uType"){	echo $uType[$fldValue];	}else{	echo $fldValue;	}
							break;
				}
			}
			?>
		</td>
		<? } ?>	
  </tr>    
<? } ?>   
 </table>
</form>
<div align="right"><a href="#top">Top ↑</a></div>
</body>
</Html>