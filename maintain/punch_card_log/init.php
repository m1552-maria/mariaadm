<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../getEmplyeeInfo.php');
	
	//抓setting的資料
	$ip_list = array();
	$sql = ' select * from `punch_card_setting`';
	$rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) {
		$ip_list[$r['id']] = $r['place'];
	}


	$bE = $_SESSION['privilege']>10?true:false;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "打卡異動資料";
	$tableName = "punch_card_log";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/punch_card_log/';
	$delField = '';
	$delFlag = true;
	$imgpath = '../../data/punch_card_log/';
	
	$status = array('lock'=>'鎖定螢幕','unlock'=>'解開螢幕','repunch'=>'補傳打卡資料','ip_setting'=>'綁定IP');
	
	// Here for List.asp ======================================= //
	$defaultOrder = "rdate";
	$searchField1 = "id";
	$searchField2 = "operactor";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,fid,operactor,status,rdate");
	$ftAry = explode(',',"編號,區域,異動人員,異動狀態,異動時間");
	$flAry = explode(',',"80,,,,,");
	$ftyAy = explode(',',"ID,text,text,text,date");
	
?>