<?
include('../../getEmplyeeInfo.php');	
header("Content-Type:text/html; charset=utf-8");

$sType = 'depID';
if(isset($_REQUEST['sType'])) $sType = $_REQUEST['sType'];
//:: to 執行部門過濾功能 ------------------------------------------------------------- >
if($sType == 'depID') {
	if($_REQUEST[depFilter]) $temp[] = "b.depID like '$_REQUEST[depFilter]%'";
	else if ($_SESSION['privilege'] <= 10) {	
		if($_SESSION['user_classdef'][2]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="b.depID like '$v%'";
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "b.depID=''";
	}

} elseif ($sType == 'projID') {	
	if($_REQUEST['projIDFilter']) {
		$projList = explode(',', $_REQUEST['projIDFilter']);
		$aa = array();
		foreach($projList as $v) if($v) $aa[]="b.projID like '$v%'";
		$temp[] = "(".join(' or ',$aa).")"; 
		//$temp[] = "b.projID like '$_REQUEST[projIDFilter]%'";
	} elseif ($_SESSION['privilege'] <= 10) {
		if($_SESSION['user_classdef'][7]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][7] as $v){
				if($v)	$aa[]="b.projID like '$v%'";	
			}
			$temp[] = "(".join(' or ',$aa).")"; 	
		} else {
			$temp[] = "b.projID='-1'";
		}
	}
}

if(count($temp)>0) $filter = join(' and ',$temp);

$nowYr = date('Y');
$curYr = $_REQUEST['year']?$_REQUEST['year']:$curYr=$nowYr;
if($_REQUEST['month']) $curMh = $_REQUEST['month']; else $curMh = date('m');
$curMh=str_pad($curMh,2,"0",STR_PAD_LEFT);


$sql = "select count(*) as con from (select e.empID,e.empName, i.title as awcTitle, r.wkMonth, i.hours as qty, i.money as allowance, i.addsub,
	IFNULL ((select h.projID from emplyee_history h where h.empID = e.empID and r.wkMonth >= date_format(h.rdate, '%Y%m') order by h.rdate desc limit 1) ,  (select min1.projID from emplyee_history min1 where min1.empID = e.empID order by min1.rdate asc limit 1)) as projID,
	IFNULL ((select h2.depID from emplyee_history h2 where h2.empID = e.empID and r.wkMonth >= date_format(h2.rdate, '%Y%m') order by h2.rdate desc limit 1), (select min2.depID from emplyee_history min2 where min2.empID = e.empID order by min2.rdate asc limit 1)) as depID
	from emplyee_salary_report r inner join emplyee_salary_items i on r.id = i.fid inner join emplyee e on r.empID=e.empID where r.wkMonth='".$curYr.$curMh."' and iType='allowance') b where 1 ";
	if($filter != '') $sql .= " and ".$filter;
$rs=db_query($sql);
$r=db_fetch_array($rs);
$canExcel = ($r['con'] == 0) ? 0 : 1;

/*$sql = "select count(*) as con from emplyee_salary_report where wkMonth='".$curYr.$curMh."'";
$rs=db_query($sql);
$r=db_fetch_array($rs);
$canExcel = ($r['con'] == 0) ? 0 : 1;*/

$empIds=array();
if($filter)	{ //前台 或 有選取部門
	$sql="select * from emplyee b where $filter "; 
	$rs=db_query($sql);
	while($r=db_fetch_array($rs)) array_push($empIds,"'".$r[empID]."'");
	$sql="select a.*,b.empName, b.depID,b.projID from emplyee_allowance as a left join emplyee as b on a.empID=b.empID";
	$sql.=" where a.empID in (".join(",",$empIds).") ";
	$sql.=" and a.wkMonth='".$curYr.$curMh."'";
} else { //全撈
	$sql="select a.*,b.empName, b.depID,b.projID from emplyee_allowance as a left join emplyee as b on a.empID=b.empID";
	$sql.=" where a.wkMonth='".$curYr.$curMh."'";
}

if ($keyword!="") {
	$kList = array(
		"($searchField1 like '%".$keyword."%')",
		"($searchField2 like '%".$keyword."%')",
		"($searchField3 like '%".$keyword."%')",
	);
	/*if ($_SESSION['privilege'] > 10) {
		$kList[] = "(b.projID like '".$keyword."%')";
	}*/
	$sql.=" and (".implode(' or ', $kList).") ";
}

//echo $sql;
$rs=db_query($sql);
$totals=db_num_rows($rs);
if (!isset($_REQUEST['pages'])) $pages = 0; else $pages = $_REQUEST['pages'];
$pageCount=(int)($totals / $pageSize)-1; if($pageCount<0) $pageCount=0;
if(($totals % $pageSize) and ($totals>$pageSize)) $pageCount++; 

//$sqlExcel = str_replace('b.empName','b.empName,b.depID',$sql).' order by b.depID,b.empID ASC';
$sqlExcel = $sql.' order by b.projID,b.empID ASC';
$sql .= " limit ".$pageSize*$pages.",$pageSize"; 
$rs=db_query($sql);
$bExp = db_eof($rs)?'false':'true';	//沒資料不能匯出
if($canExcel == 0) $bExp = 'false';
$bE=true;
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="list.css">
 <style type="text/css">
 	table.grid{  margin:10px;	border: 1px solid #ccc; border-collapse: collapse; width:800px;}
	table.grid td {   border: 1px solid #999;	text-align:center;font-family:Verdana, Geneva, sans-serif;padding:3px;	font-size:12px; width:110px;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
 <script Language="JavaScript" src="list.js"></script> 
 <title></title>
 <script type="text/javascript">
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	function clickEdit(id) { 
		location.href="edit.php?id="+id;
	}
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}
	function del(){
		var ids=document.getElementsByName("ID[]");
		var ary=[];
		for(var i=0;i<ids.length;i++){
			if(ids[i].checked)	ary.push($(ids[i]).val());
		}
		if(ary.length==0){	alert('請先勾選項目');return;}
		if(!confirm("確定刪除嗎?"))	return;
		$.ajax({ 
		  type: "POST", 
		  url: "func.php", 
		  data: "act=delEmplyeeAllowance&ids="+ary.join(",")
		}).done(function( rs ) {
			location.reload();
		}).fail(function( msg ) { 
		});
	}
	function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
			case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
			case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
			case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var year = $('select#year option:selected').val();
		var month = $('select#month option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(year) url = url+'&year='+year;
		if(month) url = url+'&month='+month;
		location.href = url;
	}
	
	function excelfmCheck() {
		var bExp = <?=$bExp?>;
		if(bExp) excelfm.submit();
		else alert('沒有資料或該月份薪資尚未試算!');
	}
 </script> 
</Head>

<body>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="<?=$keyword;?>">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="DoEdit()">
    <input type="button" name="delete" value="Ｘ刪除" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="del()">
    <select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    <select name="month" id="month" onChange="doSubmit()"><? for($i=1;$i<=12;$i++) { $sel=($curMh==$i?'selected':''); echo "<option $sel value='$i'>$i 月</option>"; } ?></select>
  <select name="sType" onChange="filterSubmit(this)">
    	<option value="depID" <?php if($sType=='depID') echo 'selected'?>>依部門</option>
    	<option value="projID" <?php if($sType=='projID') echo 'selected'?>>依計劃</option>
    </select>
<?php if($sType == 'depID') { ?>    
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>全部部門</option>
		<? 
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
		foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>
<?php } elseif($sType == 'projID') { ?>

    <?php if($_SESSION['privilege'] <= 10) { ?>
		<select name="projIDFilter" id="projIDFilter" onChange="filterSubmit(this)"><option value=''>-全部計畫-</option>
			<? 
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo "<option value='$v'".($_REQUEST[projIDFilter]==$v?' selected ':'').">$v</option>";
	    	?>
	    </select>
	<?php } else { ?>
		<input type="text" name="projIDFilter" value="<?=$_REQUEST['projIDFilter']?>" placeholder="ex:30,31">
		<input type="button" value="確定" onclick="filterSubmit(this)">
	<?php } ?>
<?php } ?>
	
    
    <input type="button" name="" value="匯出津貼清冊" onClick="excelfmCheck()">
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$totals)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
</form>
<table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<tr class="colHD"><td>序號</td><td>員工編號</td><td>津貼名稱</td><td>津貼單位</td><td>津貼價格</td><td>津貼數量</td><td>津貼金額</td><td>執行月份</td><td>計劃編號</td></tr>

<?
if(!db_eof($rs)){	
	$i=1;
	//var_dump($emplyeepjID);
	while($r=db_fetch_array($rs)){
		$prjID = $emplyeepjID[$r['empID']];
		$str="<tr><td><input type='checkbox' value='$r[id]' name='ID[]'><a href='javascript: clickEdit($r[id])'>".$i."</a></td>";
		$str.="<td>".$r[empID].'-'.$emplyeeinfo[$r[empID]]."</td><td>".$r[awcTitle]."</td><td>".$r[unitName]."</td>";
		$str.="<td>".$r[unitPrice]."</td><td>".$r[qty]."</td><td>".($r[unitPrice]*$r[qty])."</td><td>".$r[wkMonth]."</td><td>".$prjID."</td></tr>";
		echo $str;
		$i++;
	}
}
?>
</table>
<form name="excelfm" action="excel.php" method="post" target="excel">
	<input type="hidden" name="filter" value="<?=$filter?>">
	<input type="hidden" name="sType" value="<?=$sType?>">
	<input type="hidden" name="year" value="<?=$curYr?>">
	<input type="hidden" name="month" value="<?=$curMh?>">
	<input type="hidden" name="sql" value="<?=$sqlExcel?>">
</form>
</body>
</Html>