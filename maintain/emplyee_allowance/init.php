<?	
	session_start();
  header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	$bE = true;	//異動權限
	$pageTitle = "津貼設定";
	$tableName = "emplyee_allowance";
	$extfiles = '';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$defaultOrder = "a.id";
	$searchField1 = "a.awcTitle";
	$searchField2 = "b.empID";
	$searchField3 = "b.empName";
	$pageSize = 20;	//每頁的清單數量

	$state=array("0"=>"否","1"=>"是");
	// 注意 primary key should be first one
	$fnAry = explode(',',"id,title,awcID,action,state,wkMonth");
	$ftAry = explode(',',"編號,名稱,津貼項目,運作,是否套用,執行月份");
	$flAry = explode(',',"50,,,,,");
	$ftyAy = explode(',',"id,text,text,text,text,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,awcTitle,unitName,unitPrice,qty,subTotal,wkMonth,prjID");
	$editftA = explode(',',"編號,津貼名稱,津貼單位,津貼單價,數量,金額,執行月份,計劃編號");
	$editflA = explode(',',",,,,,,,");
	$editfhA = explode(',',",,,,,,,");
	$editfvA = array('','','','','','','','');
	$editetA = explode(',',"label,label,label,label,text,label,label,text");

?>