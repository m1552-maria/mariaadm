<?php
	header("Content-type: text/html; charset=utf-8");
	session_start();
	//include class
	include '../../config.php';
	include '../inc_vars.php';
	include('../../getEmplyeeInfo.php');
	require_once '../Classes/PHPExcel.php';
	require_once '../Classes/PHPExcel/IOFactory.php';

	//設定變數
	$objPHPExcel = new PHPExcel();
	$title = array(
		'empID'=>array('val'=>'編號'),
		'empName'=>array('val'=>'姓名'),
		'awcTitle'=>array('val'=>'津貼名稱'),
		'wkMonth'=>array('val'=>'執行月份'),
		'qty'=>array('val'=>'次數'),
		'unitPrice'=>array('val'=>'單次金額'),
		'allowance'=>array('val'=>'津貼金額'),
		'depID'=>array('val'=>'部門')
		
	);


	//抓部門
	$sql = "select depID,depName,depTitle from department";
	$rs = db_query($sql,$conn);
	$depName = array();
	$depTitle = array();
	while ($r = db_fetch_array($rs)){
		$depName[$r['depID']] = $r['depName'];
		$depTitle[$r['depID']] = $r['depTitle'];
	}
	
	$wkMonth = $_REQUEST['year'].str_pad($_REQUEST['month'],2,'0',STR_PAD_LEFT);

	$sql = "select * from (select e.empID,e.empName, i.title as awcTitle, r.wkMonth, i.hours as qty, i.money as allowance, i.addsub,
	IFNULL ((select h.projID from emplyee_history h where h.empID = e.empID and r.wkMonth >= date_format(h.rdate, '%Y%m') order by h.rdate desc limit 1) ,  (select min1.projID from emplyee_history min1 where min1.empID = e.empID order by min1.rdate asc limit 1)) as projID,
	IFNULL ((select h2.depID from emplyee_history h2 where h2.empID = e.empID and r.wkMonth >= date_format(h2.rdate, '%Y%m') order by h2.rdate desc limit 1), (select min2.depID from emplyee_history min2 where min2.empID = e.empID order by min2.rdate asc limit 1)) as depID
	from emplyee_salary_report r inner join emplyee_salary_items i on r.id = i.fid inner join emplyee e on r.empID=e.empID where r.wkMonth='".$wkMonth."' and iType='allowance') b where 1 ";
	if($_REQUEST['filter'] != '') $sql .= " and ".$_REQUEST['filter'];
	$sql .= " order by b.projID,b.depID, b.empID";


	//抓資料
	//$rs = db_query($_POST['sql'],$conn);
	$rs = db_query($sql,$conn);
	$row = array();
	while ($r = db_fetch_array($rs)){
		if($r['addsub'] == '-') $r['allowance'] *= -1;
		$row[$r['projID']][] = $r;
		$filename = $r['wkMonth'];
	}

	//創建資料工作表
	$objPHPExcel->setActiveSheetIndex(0);
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->setTitle("津貼清冊$filename");

	//基本資料內容
	$objPHPExcel->setActiveSheetIndex(0);
	$sumAll = 0; //總計
	$i = 1;  // 列編號  $a.$i = A1 ;
	foreach ($row as $key => $value) {
		$sum = 0;	//單位合計
		$a = 'A'; // excel $a欄位  => A欄位 => A1
		$merStart = $a.$i;
		$merEnd = 'H'.$i;
		$sheet->mergeCells("$merStart:$merEnd"); 
		/*if($depName[$key]=='') $sheet->setCellValue($a.$i, 'r：'.$depTitle[$key]);
		else $sheet->setCellValue($a.$i, '單位名稱：'.$depName[$key]);*/
		$sheet->setCellValue($a.$i, '計畫編號：'.$key);
		$i++;
		//標題
		foreach ($title as $k => $v) {
			$sheet->setCellValue($a.$i, $v['val']);
			$a++;
		}
		$sheet->getStyle("A$i:H$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFF0F0F0');
		
		$i++;
		foreach ($value as $k => $v) {
			$a = 'A';
			foreach ($title as $k1 => $v1) {
				switch ($k1) {
					case 'empID':
						$sheet->setCellValueExplicit($a.$i, $v[$k1]);
						break;
					/*case 'allowance':
						$mm = $v['qty']*$v['unitPrice'];
						$sheet->setCellValue($a.$i, $mm);
						$sum += $mm;
						$sumAll += $mm;
						break;*/
					case 'allowance':
						$sum += $v['allowance'];
						$sumAll += $v['allowance'];
						$sheet->setCellValue($a.$i, $v['allowance']);
						break;
					case 'unitPrice': //單次金額
						$mm = $v['allowance']/$v['qty'];
						$sheet->setCellValue($a.$i, $mm);
						break;
					case 'prjID':
						$sheet->setCellValue($a.$i, $emplyeepjID[$v[empID]]);
						break;	
					case 'depID':
						if($depName[$v[$k1]]=='') $sheet->setCellValue($a.$i, 'r：'.$depTitle[$v[$k1]]);
						else $sheet->setCellValue($a.$i, $depName[$v[$k1]]);
						break;
					default:
						$sheet->setCellValue($a.$i, $v[$k1]);
						break;
				}
				$a++;
			}
			$i++;
		}
		//本單位合計
		$sheet->getStyle("F$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFF0F0F0');
		$sheet->setCellValue("F$i","合計金額：");

		$sheet->getStyle("G$i")->getFont()->getColor()->setARGB('FF0000FF');
		$sheet->setCellValue("G$i",$sum);
		$i++;
		//跟上個部門單位的間隔
		$sheet->mergeCells("A$i:H$i"); 
		$i++;
	}
	
	$sheet->getStyle("F$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFF0F0F0');
	$sheet->setCellValue("F$i","總計金額：");
	$sheet->getStyle("G$i")->getFont()->getColor()->setARGB('FF0000FF'); 
	$sheet->setCellValue("G$i",$sumAll);
		
	//$sheet->getColumnDimension("C")->setAutoSize(true); 
	$sheet->getColumnDimension("C")->setWidth(20); 
	$sheet->getColumnDimension("F")->setWidth(10); 
		
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=".$filename.".xlsx");
	header("Pragma:no-cache");
	header("Expires:0");

	$objPHPExcel->setActiveSheetIndex(0);
	//Save Excel 2007 file 保存
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');
?>