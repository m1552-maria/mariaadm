<?	
	session_start();
  header('Content-type: text/html; charset=utf-8');
  header("Cache-control:private");
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');
	include('../../getEmplyeeInfo.php');
	include('../../Utilities/authority.php');
	$bE = true;	//異動權限
	$pageTitle = "雜項費用設定";
	$tableName = "otherfee";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/bullets/';
	$delField = 'Ahead,filepath';
	$delFlag = true;
	$thumbW = 150;
	

	

	// Here for List.asp ======================================= //
	$defaultOrder = "ofID";
	$searchField1 = "of.title";
	$searchField2 = "ofc.title";
	$searchField3 = "of.wkMonth";
	$pageSize = 20;	//每頁的清單數量

	$state=array("0"=>"否","1"=>"是");
	// 注意 primary key should be first one
	$fnAry = explode(',',"ofID,title,ofcID,action,wkMonth,copy");
	$ftAry = explode(',',"編號,雜項說明,雜項類別,運作,執行月份,複製");
	$flAry = explode(',',"50,,,,,,");
	$ftyAy = explode(',',"id,text,text,text,text,copy");
	
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,ofcID");
	$newftA = explode(',',"雜項說明,雜項類別");
	$newflA = explode(',',",,");
	$newfhA = explode(',',",,");
	$newfvA = array('','');
	$newetA = explode(',',"text,checkbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ofID,title,ofcID");
	$editftA = explode(',',"編號,雜項說明,雜項類別");
	$editflA = explode(',',",,,");
	$editfhA = explode(',',",,,");
	$editfvA = array('','','');
	$editetA = explode(',',"text,text,checkbox");
?>