<?php
	include "../../config.php";
	

	//處理上傳檔案
	$ulpath = $root.'/data/excel_tmp/123.xls';
	if(count($_FILES['excel'])!=0){
		if(is_file($_FILES['excel']['tmp_name'][0])){
			move_uploaded_file($_FILES['excel']['tmp_name'][0],$ulpath);
		}
	}


	//讀取類別
	$sql = 'select `ofcID`,`unitName`,`unitPrice`,`title` from `otherfee_class` where 1';
	$rs = db_query($sql);
	if (!db_eof($rs)) {
        while($r=db_fetch_array($rs)) {
        	$ofcTitle[$r['title']] = array(
        		'title'=>$r['title'],
        		'ofcID'=>$r['ofcID'],
        		'unitName'=>$r['unitName'],
        		'unitPrice'=>$r['unitPrice'],
        		);
        }
    }


	//處理excel
	require_once('../Classes/PHPExcel/IOFactory.php'); 
	$reader = PHPExcel_IOFactory::createReader('Excel5'); // 讀取舊版 excel 檔案 
	$PHPExcel = $reader->load($ulpath); // 檔案名稱 
	$sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從0開始) 
	$highestRow = $sheet->getHighestRow(); // 取得總列數 //橫的

	$list = array();
	// 一次讀取一列 
	$msg = '';
	for ($row = 2; $row <= $highestRow; $row++) {//從第二列開始
	    for ($column = 0; $column < 6; $column++) { //行數 共5行
	        $val = $sheet->getCellByColumnAndRow($column, $row)->getValue();
	        if($val !=''){
	        	$list[$row][$column] = $val;	
	        }else{
	        	$msg .='第'.$row.'行資料有誤，內容不得為空白。\n';
	        	//將有問題資料過濾
	        	if(isset($list[$row])) 	unset($list[$row]);
	        	break;
	        }
	        
	    }
	}


	foreach($list as $key1=>$val1){
		$employee_ids[$list[$key1][0]] = array(
			'empID'=>$list[$key1][0],
			'ofcTitle'=>$list[$key1][1],
			'wkMonth'=>$list[$key1][5],
			'qty'=>$list[$key1][4],
			'unitPrice'=>$list[$key1][3],
			'unitName'=>$list[$key1][2]
			);
		
	}

	
	//存到employee_fee
	//有就update 沒有就insert
	// 類別 月份 id
	foreach($employee_ids as $key1=>$val1){
		$sql = 'select `id` from `emplyee_otherfee` where `empID`='."'".$employee_ids[$key1]['empID']."'".' AND `ofcTitle`='."'".$employee_ids[$key1]['ofcTitle']."'".' AND `wkMonth`='."'".$employee_ids[$key1]['wkMonth']."'";
		$rs = db_query($sql);
		if (!db_eof($rs)) {
			//update
			$sql1='update `emplyee_otherfee` set `qty`='."'".$employee_ids[$key1]['qty']."'".',`unitPrice`='."'".$employee_ids[$key1]['unitPrice']."'".' where `empID`='."'".$employee_ids[$key1]['empID']."'".' AND `ofcTitle`='."'".$employee_ids[$key1]['ofcTitle']."'".' AND `wkMonth`='."'".$employee_ids[$key1]['wkMonth']."'";
			
			db_query($sql1);
		}else{
			//insert
			$sql2='insert into `emplyee_otherfee` (`empID`,`ofcID`,`ofcTitle`,`unitName`,`unitPrice`,`qty`,`wkMonth`) ';
			$ofcID = '';
			if(isset($ofcTitle[$employee_ids[$key1]['ofcTitle']]['ofcID']) && $ofcTitle[$employee_ids[$key1]['ofcTitle']]['ofcID']!=''){
				$ofcID = $ofcTitle[$employee_ids[$key1]['ofcTitle']]['ofcID'];
			}else{
				$ofcID = "NULL";
			}
			$sql2 .= ' values ('."'".$employee_ids[$key1]['empID']."','".$ofcID."','".$employee_ids[$key1]['ofcTitle']."','".$employee_ids[$key1]['unitName']."','".$employee_ids[$key1]['unitPrice']."','".$employee_ids[$key1]['qty']."','".$employee_ids[$key1]['wkMonth']."'".')';
			
			db_query($sql2);
		}
	}

	echo $msg;
	exit;

?>