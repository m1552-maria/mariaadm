<?php
session_start();
include "../../config.php";
$act=$_POST[act];
switch($act){
	case "getEmplyee":
		$empID=$_POST[empID];
		$depID=$_POST[depID];
		$ofcID=$_POST[ofcID];
		$wkMonth = $_POST[wkMonth];
		getEmplyee($empID,$depID,$ofcID,$wkMonth);
		break;
	case "doAllowance":
		$data=$_POST[data];
		$awID=$_POST[awID];
		doAllowance($data,$awID);
		break;
	case "doContinue":
		$data=$_POST[data];
		doContinue($data);
		break;
	case "delEmplyeeAllowance":
		$ids=$_POST[ids];
		delEmplyeeAllowance($ids);
		break;
}
function delEmplyeeAllowance($ids){
	$sql="delete from emplyee_otherfee where id in (".$ids.")";
	echo $sql;
	db_query($sql);
}
function doContinue($data){
	$ary=explode(",",$data);
	$sql="select title,depID,empID,ofcID,wkMonth from otherfee where ofID in (".$data.") ";
	$rs=db_query($sql);
	$awcID=array();
	$depID=array();
	$empID=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			array_push($depID,$r[depID]);
			array_push($empID,$r[empID]);
			array_push($awcID,$r[ofcID]);
		}
	}
	$msg='';
	$ary2=array();
	foreach($ary as $k=>$v){
		$sql="select * from otherfee where wkMonth='".date("ym")."' and ofcID=".$awcID[$k]." ";
		$sql.="and (depID!='' or empID!='') ";
		$sql.="and (";
		if($depID[$k]!="")	{
			$dep=explode(",",$depID[$k]);
			array_push($ary2,"(depID like '".join("%' or  depID like '",$dep)."%')");
		}
		if($empID[$k]!="")	{
			$emp=explode(",",$empID[$k]);
			array_push($ary2,"empID in ('".join("','",$emp)."')");
		}
		$sql.=join(" or ",$ary2);
		$sql.=")";
		$rs=db_query($sql);
		if(db_eof($rs)){
			$sql="select title,depID,empID,ofcID from otherfee where ofID=".$v;
			$rs2=db_query($sql);
			if(!db_eof($rs2)){
				$r2=db_fetch_array($rs2);
				$sql="insert into otherfee (title,depID,empID,ofcID,wkMonth) ";
				$sql.="values('".$r2[title]."','".$r2[depID]."','".$r2[empID]."',".$r2[ofcID].",'".date("ym")."')";
				db_query($sql);	
			}
		}else{	$msg='此部門或員工已有津貼設定';}
	}
	echo $msg;
}
function doAllowance($data,$awID){

	$sql="select * from otherfee where ofID=".$awID;
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$wkMonth=$r[wkMonth];
	}
	$sql="update otherfee set state=1 where ofID=".$awID;
	db_query($sql);
	$ary=explode("#",$data);
	// $c=count($ary);
	//處理資料
	foreach($ary as $key1=>$val1){
		$ary[$key1] = explode(',', $ary[$key1]);
	}

	//判斷資料
	foreach($ary as $key1=>$val1){
		$sql = 'select `id` from `emplyee_otherfee` where `empID`='."'".$ary[$key1]['0']."'".' AND `ofcTitle`='."'".$ary[$key1]['2']."'".' AND `wkMonth`='."'".$wkMonth."'";
		$rs = db_query($sql);
		if (!db_eof($rs)) {
			//update
			$sql1='update `emplyee_otherfee` set `qty`='."'".$ary[$key1]['5']."'".',`unitPrice`='."'".$ary[$key1]['4']."'".' where `empID`='."'".$ary[$key1]['0']."'".' AND `ofcTitle`='."'".$ary[$key1]['2']."'".' AND `wkMonth`='."'".$wkMonth."'";
			db_query($sql1);
		}else{
			//insert
			$sql2='insert into `emplyee_otherfee` (`empID`,`ofcID`,`ofcTitle`,`unitName`,`unitPrice`,`qty`,`wkMonth`) ';
			$sql2 .= ' values ('."'".$ary[$key1]['0']."','".$ary[$key1]['1']."','".$ary[$key1]['2']."','".$ary[$key1]['3']."','".$ary[$key1]['4']."','".$ary[$key1]['5']."','".$wkMonth."'".')';
			db_query($sql2);
		}
	}

}
function getEmplyee($empID,$depID,$ofcID,$wkMonth){
	if ($_SESSION['privilege']<=10) {
		$depAry=array();
	    foreach($_SESSION['user_classdef'][2] as $v){
	    	array_push($depAry,"depID like '".$v."%'");
	    } 
	    $sql="select empID from emplyee where ".join(" or ",$depAry)." ";
	    $rs=db_query($sql);
		$allowEmpAry=array();
	    if(!db_eof($rs)){
	    	while($r=db_fetch_array($rs)){
	    		array_push($allowEmpAry, $r[empID]);
	    	}
	    }
	 }
	//抓目前單價
	$default_price = 0;
	$sql = ' select `unitPrice` from `otherfee_class` where `ofcID`='.$ofcID;
	$rs=db_query($sql);
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			$default_price = $r['unitPrice'];
		}
	}

	$emp=explode(",",$empID);
	$sql="select a.empID,a.empName,b.id,b.ofcID,b.qty,b.unitPrice from emplyee as a LEFT JOIN emplyee_otherfee AS b ON b.empID=a.empID AND b.wkMonth="."'".$wkMonth."'".' AND b.ofcID='."'".$ofcID."'";
	$ary=array();
	if($empID!="")	array_push($ary,"a.empID in ('".join("','",$emp)."')");
	if($depID!=""){
		$depID=explode(",",$depID);
		array_push($ary,"a.depID like '".join("%' or  a.depID like '",$depID)."%'");
	}
	$data=array();
	if($empID!="" || $depID!=""){
		$sql.=" where (".join(" or ",$ary).")";
		$sql.=" and a.isOnduty=1";
		$rs=db_query($sql);
		if(!db_eof($rs)){
			while($r=db_fetch_array($rs)){
				if($allowEmpAry){
					if(!in_array($r[empID],$allowEmpAry))	continue;
				}
				if($r[qty]=='') $r[qty]=1;//數量預設1
				if($r[unitPrice]=='') $r[unitPrice] = $default_price;
				array_push($data,$r[empID].",".$r[empName].",".$r[qty].",".$r[unitPrice]);
			}
		}
	}
	echo join("#",$data);

	

}
?>