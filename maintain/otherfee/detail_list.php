<?
include "../../config.php";
include('../../getEmplyeeInfo.php');	
header("Content-Type:text/html; charset=utf-8");

//::秀出清單
$nowYr = date('Y');
$curYr = $_REQUEST['year']?$_REQUEST['year']:$curYr=$nowYr;
$curYr=str_pad($curYr,2,"0",STR_PAD_LEFT);
if($_REQUEST['month']) $curMh = $_REQUEST['month']; else $curMh = date('m');
$curMh=str_pad($curMh,2,"0",STR_PAD_LEFT);
$empIds=array();
if(isset($_POST[depFilter])){
	$sql="select * from emplyee where depID like '".$_POST[depFilter]."%'";
	$rs=db_query($sql);
	while($r=db_fetch_array($rs)){
		array_push($empIds,"'".$r[empID]."'");
	}
}
$sql="select * from emplyee_allowance ";
if(isset($_POST[depFilter])){
	$sql.=" where empID in (".join(",",$empIds).") ";
	if(isset($_REQUEST['year'])){
		$sql.="and wkMonth='".$curYr.$curMh."'";
	}
}
$rs=db_query($sql);
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="list.css">
 <style type="text/css">
 	table.grid{  margin:10px;	border: 1px solid #ccc; border-collapse: collapse; width:800px;}
	table.grid td {   border: 1px solid #999;	text-align:center;font-family:Verdana, Geneva, sans-serif;padding:3px;	font-size:12px; width:110px;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script Language="JavaScript" src="list.js"></script> 
 <title></title>
 <script type="text/javascript">

	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}
 </script> 
</Head>

<body>
<form name="form1" method="post" action="detail_list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'sxBtn'?>" onclick='location.href="append.php";'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'sxBtn'?>" onClick="MsgBox()">
    
    <select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>-全部部門-</option>
		<? 
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
     
    ?>
    </select>
	
    <select name="year" id="year" onChange="doSubmit()"><? for($i=$nowYr-1;$i<=$nowYr;$i++) { $sel=($curYr==$i?'selected':''); echo "<option $sel value='$i'>$i 年</option>"; } ?></select>
    <select name="month" id="month" onChange="doSubmit()"><? for($i=1;$i<=12;$i++) { $sel=($curMh==$i?'selected':''); echo "<option $sel value='$i'>$i 月</option>"; } ?></select>
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
</form>
<table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<tr class="colHD"><td>序號</td><td>員工編號</td><td>津貼名稱</td><td>津貼單位</td><td>津貼價格</td><td>津貼數量</td><td>執行月份</td><td>計劃編號</td></tr>
<?
if(!db_eof($rs)){	
	$i=1;
	while($r=db_fetch_array($rs)){
		echo "<tr><td>".$i."</td><td>".$r[empID].'-'.$emplyeeinfo[$r[empID]]."</td><td>".$r[awcTitle]."</td><td>".$r[unitName]."</td><td>".$r[unitPrice]."</td><td>".$r[qty]."</td><td>".$r[wkMonth]."</td><td>".$r[prjID]."</td></tr>";
		$i++;
	}
}
?>
</table>
</body>
</Html>