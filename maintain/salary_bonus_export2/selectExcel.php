<?php 
	session_start();
	include("../../config.php");
	
	if($_SESSION['privilege'] <= 10 && count($_SESSION['user_classdef'][7]) == 0) {
		echo "請設定管理的計畫編號";
		exit;
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<form name="excelfm2" action="exportExcel.php" method="post" target="excel">
計畫編號:
<?php  if($_SESSION['privilege'] > 10) { ?>
	<input type="text" name="projIDFilter" id="projIDFilter" value="" placeholder="請以逗號區隔 ex: 30,31">

<?php  }else{?>
	<div>
		<label><input type="checkbox"  value="全選" onclick="allCheck(this)">全選</label>
		<?php  
			foreach($_SESSION['user_classdef'][7] as $v) if($v) echo '<label><input type="checkbox" name="projIDFilter" value="'.$v.'">'.$v.'</label>';
	    ?>
	</div>


<?php  } ?>


<input type="button" onclick="excelfm2Smi()" value="確定">
<script src="../../Scripts/jquery-1.12.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
	function excelfm2Smi() {
<?php  if($_SESSION['privilege'] > 10) { ?>
		window.opener.projIDFilter.value=$('#projIDFilter').val();
<?php  } else { ?> 
		var projID = [];
		$('[name="projIDFilter"]').each(function(){
			if(this.checked) projID.push($(this).val());
		});
		window.opener.projIDFilter.value=projID.join(',');
<?php  } ?>
		window.opener.doSubmit();
		window.close();
	}

	function allCheck(obj) {
		var pList = document.getElementsByName('projIDFilter');
		for(var i=0;i<pList.length;i++) {
			pList[i].checked = obj.checked;
		} 
	}
</script>
</form>
</body>
</html>