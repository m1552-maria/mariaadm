<?php 
  session_start();
  if(empty($_REQUEST['year'])) exit;
 
  $filename = $_REQUEST['year'].'年薪資、獎金統計(91)'.date("Y-m-d H:i:s",time()).'.xlsx';
  

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');
  include ('../inc_vars.php');
  include("../salary_bonus_export/getData.php");
  include("getData.php");
  $result = getOtherBonus($_REQUEST['year'], $_REQUEST['search_list'], $_REQUEST['projIDList'], '91');
  $baseList = $result['baseList'];
  $dataList = $result['dataList'];
  $monthList = $result['monthList'];


 
  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);

  $tNum = 15;
  $letter = 65;
  $year = $_REQUEST['year'];

  $numList = array(
    ($year-1).'12'=>'十二月',
    $year.'01'=>'一月',
    $year.'02'=>'二月',
    $year.'03'=>'三月',
    $year.'04'=>'四月',
    $year.'05'=>'五月',
    $year.'06'=>'六月',
    $year.'07'=>'七月',
    $year.'08'=>'八月',
    $year.'09'=>'九月',
    $year.'10'=>'十月',
    $year.'11'=> '十一月'
  );

  foreach ($numList as $key => $value) {
    $colspan = (isset($monthList[$key])) ? count($monthList[$key]) : 1;
    $tNum = $tNum + $colspan - 1;
  }


  $tLetter = chr($letter+$tNum);

  $i = 1;
  $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
  $sheet->setCellValueExplicit('A'.$i,$_REQUEST['year'].'年薪資、獎金統計(91)清冊');
 // $sheet->getStyle('A1:R1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A'.$i.':'.$tLetter.$i)->getFont()->setSize(16);

  $i++;
  $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
  $sheet->setCellValueExplicit('A'.$i,'計劃:'.$_REQUEST['projIDList']);
  $sheet->getStyle('A'.$i.':'.$tLetter.$i)->getFont()->setSize(14);

  $i++;
  $letter = 65;
  $sheet->setCellValueExplicit(chr($letter++).$i,'獎金');
  $sheet->setCellValueExplicit(chr($letter++).$i,'姓名');
  $sheet->setCellValueExplicit(chr($letter++).$i,'員編');

  foreach ($numList as $key => $value) {
    $colspan = (isset($monthList[$key])) ? count($monthList[$key])-1 : 0;
    if($colspan > 0) {
       $sheet->mergeCells(chr($letter).$i.':'.chr($letter+$colspan).$i);//合併
       $sheet->setCellValueExplicit(chr($letter).$i, $value);
       $letter += $colspan+1;
    } else {
      $sheet->setCellValueExplicit(chr($letter).$i, $value);
      $letter++;
    }
  }

  $sheet->setCellValueExplicit(chr($letter++).$i,'獎金合計');

  $i++;
  $letter = 67;
  foreach ($numList as $key => $value) {
    if(isset($monthList[$key])) {
        foreach ($monthList[$key] as $k => $v) {
          $letter++;
          $sheet->setCellValueExplicit(chr($letter).$i, $v);
          $objPHPExcel->getActiveSheet()->getStyle(chr($letter).$i)->getAlignment()->setWrapText(true);
          $objPHPExcel->getActiveSheet()->getColumnDimension(chr($letter))->setWidth(13);
        }
    } else {
      $letter++;
    }
  }


  $allSum = array('money'=>array(), 'taxPay'=>array());
  foreach($dataList as $key => $emplyee) {
    $i++;
    $showList = explode('=*=', $key);
    $sum = array('money'=>array(), 'taxPay'=>array());
    $depID = $showList[0];
    $projID = $showList[1];
    $sheet->mergeCells('A'.$i.':'.$tLetter.$i);//合併
    $sheet->setCellValueExplicit('A'.$i,'部門:'.(isset($departmentinfo[$depID]) ? $departmentinfo[$depID] : $depID).',  計劃編號:'.$projID);
    foreach($emplyee as $ek => $r) { 
      $letter = 65;
      $i++;

      $sheet->setCellValueExplicit(chr($letter++).$i,'獎金小計');
      $sheet->setCellValueExplicit(chr($letter++).$i,$baseList[$key][$ek]['empName']);
      $sheet->setCellValueExplicit(chr($letter++).$i,' '.$ek);

      $mTotal = 0;
      foreach ($numList as $k => $v) {
        if(!isset($sum['money'][$k])) $sum['money'][$k] = array();
        if(isset($monthList[$k])) {
          foreach ($monthList[$k] as $monthK => $monthV) {
            if(!isset($sum['money'][$k][$monthK])) $sum['money'][$k][$monthK] = 0;
            if(isset($r[$k][$monthK])) {
                $sum['money'][$k][$monthK] += $r[$k][$monthK]['money'];
                $mTotal+=$r[$k][$monthK]['money'];
                $sheet->setCellValueExplicit(chr($letter++).$i, number_format($r[$k][$monthK]['money']));
              
            } else {
              $letter++;
            }

          }

        } else {
           $letter++;
        }

      }
      $sheet->setCellValueExplicit(chr($letter).$i, number_format($mTotal));

      $i++;
      $mTotal = 0;
      $letter = 65;
      $sheet->setCellValueExplicit(chr($letter).$i,'代扣所得稅');
      $letter += 2;
      foreach ($numList as $k => $v) {
        if(!isset($sum['taxPay'][$k])) $sum['taxPay'][$k] = array();
        if(isset($monthList[$k])) {
          foreach ($monthList[$k] as $monthK => $monthV) {
            if(!isset($sum['taxPay'][$k][$monthK])) $sum['taxPay'][$k][$monthK] = 0;
            if(isset($r[$k][$monthK])) {
              $sum['taxPay'][$k][$monthK] += $r[$k][$monthK]['taxMoney'];
              $letter++;
              $mTotal+=$r[$k][$monthK]['taxMoney'];
              $sheet->setCellValueExplicit(chr($letter).$i, number_format($r[$k][$monthK]['taxMoney']));
              
            } else {
              $letter++;
            }
          }

        } else {
          $letter++;
        }

      }

      $letter++;
      $sheet->setCellValueExplicit(chr($letter).$i, number_format($mTotal));
    }

    $letter = 65;
    $i++;
    $sheet->setCellValueExplicit(chr($letter++).$i,'獎金小計');
    $sheet->mergeCells(chr($letter).$i.':'.chr($letter).($i+1));//合併
    $sheet->setCellValueExplicit(chr($letter++).$i,'小計：');
    $letter++;
    $alltotal = 0;
    foreach ($sum['money'] as $sk => $sv) {
      if(!isset($allSum['money'][$sk])) $allSum['money'][$sk] = array();
      if(count($sv) == 0) {
        $sheet->setCellValueExplicit(chr($letter++).$i, 0);
        continue;
      }
      foreach ($sv as $ssk => $ssv) {
        if(!isset($allSum['money'][$sk][$ssk])) $allSum['money'][$sk][$ssk] = 0;
        $allSum['money'][$sk][$ssk] += $ssv;
        $alltotal += $ssv;
        $sheet->setCellValueExplicit(chr($letter++).$i,number_format($ssv));
      }

    }
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));

    $letter = 65;
    $i++;
    $sheet->setCellValueExplicit(chr($letter++).$i,'代扣所得稅');
    $letter+=2;
    $alltotal = 0;
    foreach ($sum['taxPay'] as $sk => $sv) {
      if(!isset($allSum['taxPay'][$sk])) $allSum['taxPay'][$sk] = array();
      if(count($sv) == 0) {
        $sheet->setCellValueExplicit(chr($letter++).$i, 0);
        continue;
      }
      foreach ($sv as $ssk => $ssv) {
        if(!isset($allSum['taxPay'][$sk][$ssk])) $allSum['taxPay'][$sk][$ssk] = 0;
        $allSum['taxPay'][$sk][$ssk] += $ssv;
        $alltotal += $ssv;
        $sheet->setCellValueExplicit(chr($letter++).$i,number_format($ssv));
      }
      
    }
    $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));
    $i++;
  }

  $letter = 65;
  $i++;
  $sheet->setCellValueExplicit(chr($letter++).$i,'獎金小計');
  $sheet->mergeCells(chr($letter).$i.':'.chr($letter).($i+1));//合併
  $sheet->setCellValueExplicit(chr($letter++).$i,'總計：');
  $letter++;
  $alltotal = 0;
  foreach ($allSum['money'] as $sk => $sv) {
    if(count($sv) == 0) {
      $sheet->setCellValueExplicit(chr($letter++).$i, 0);
      continue;
    }
    foreach ($sv as $ssk => $ssv) {
      $alltotal += $ssv;
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($ssv));
    }

  }
  $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));

  $letter = 65;
  $i++;
  $sheet->setCellValueExplicit(chr($letter++).$i,'代扣所得稅');
  $letter+=2;
  $alltotal = 0;
  foreach ($allSum['taxPay'] as $sk => $sv) {
    if(count($sv) == 0) {
      $sheet->setCellValueExplicit(chr($letter++).$i, 0);
      continue;
    }
    foreach ($sv as $ssk => $ssv) {
      $alltotal += $ssv;
      $sheet->setCellValueExplicit(chr($letter++).$i,number_format($ssv));
    }

  }
  $sheet->setCellValueExplicit(chr($letter++).$i,number_format($alltotal));


  $objPHPExcel->getActiveSheet()->getStyle('B4:'.$tLetter.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $objPHPExcel->getActiveSheet()->getStyle('B4:'.$tLetter.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');


?>