<?php 
  include("../salary_bonus_export/getData.php");
  include("getData.php");
  $result = getOtherBonus($year, implode(' and ', $search_list), implode(',', $projIDList), '91');
  $baseList = isset($result['baseList']) ? $result['baseList'] : array();
  $dataList = isset($result['dataList']) ? $result['dataList'] : array();
  $monthList = isset($result['monthList']) ? $result['monthList'] : array();
  $con = $result['con'];
?> 
<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?php echo $pageTitle?></title>
 <link rel="stylesheet" href="<?php echo $extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link href="tuning.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <script type="text/javascript" src="tuning.js" ></script> 
 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?php echo $pages?>;		//目前所在頁次
	var selid = '<?php echo $selid?>';	
	var keyword = '<?php echo $keyword?>';
	var cancelFG = true;	
	
  /*function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?php echo $pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?php echo $pages?>+1; if(pp><?php echo $pageCount?>) pp=<?php echo $pageCount?>; break;
      case 3 : pp=<?php echo $pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#depFilter option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var url = "<?php echo $PHP_SELF?>?pages="+pp+"&keyword=<?php echo $keyword?>&fieldname=<?php echo $fieldname?>&sortDirection=<?php echo $sortDirection?>";
		if(depflt) url = url+'&depFilter='+depflt;
		if(yrflt) url = url+'&year='+yrflt;

		location.href = url;		
	}*/

	/*$(function() {	//點選清單第一項 或 被異動的項目
		if( $('table.sTable').get(0).rows.length==1 ) return;				 		
		<?php  if ($_REQUEST[empID]) { ?>
			var obj = $('a[href*=<?php echo $_REQUEST[empID]?>]')[0];
		<?php  } else { ?>
			var obj = $('table.sTable tr:eq(1) td:eq(0) a')[0];
		<?php  } ?>
		if(document.all) obj.click();	else {
			var evt = document.createEvent("MouseEvents");  
      evt.initEvent("click", true, true);  
			obj.dispatchEvent(evt);
		}
		
		
	});	*/
	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}


 </script>
 <script Language="JavaScript" src="<?php echo $extfiles?>list.js"></script> 
</Head>

<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<!--<input type="button" value="第一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?php echo $pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?php echo $pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?php echo $pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">-->
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <!--<select name="depFilter" id="depFilter" onChange="filterSubmit(this)"><option value=''>全部部門</option>
		<?php  
      if ($_SESSION['privilege'] > 10) {
        foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
      } else {
		foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
      }
    ?>
    </select>-->
    
    <select name="year" id="year" onChange="doSubmit()">
    	<?php  foreach ($yearList as $key => $value) {
    		$sel = ($year == $key) ? 'selected':'';
    	 	 echo "<option $sel value='".$key."'>".$value." 年</option>"; 
    	} ?>
    	
    </select>
    <input type="text" name="projIDFilter" id="projIDFilter" value="<?php echo $projIDFilter?>" readonly style="background-color: #eae8e8;"> 
    <input type="button" value="點我選擇計畫" onClick="getExcelV()">
    <input type="button" value="匯出清冊" onclick="excelfm2.submit()">
    
  	</td>
  	<td align="right">總筆數：<?php echo $con?><?php  if ($keyword!="") echo " / 搜尋：$keyword "; ?></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
<tr class="colHD">
	<th>獎金</th>
	<th>姓名</th>
	<th>員編</th>
  <?php  
    $tNum = 16;
    $numList = array(
      ($year-1).'12'=>'十二月',
      $year.'01'=>'一月',
      $year.'02'=>'二月',
      $year.'03'=>'三月',
      $year.'04'=>'四月',
      $year.'05'=>'五月',
      $year.'06'=>'六月',
      $year.'07'=>'七月',
      $year.'08'=>'八月',
      $year.'09'=>'九月',
      $year.'10'=>'十月',
      $year.'11'=> '十一月'
    );

    foreach ($numList as $key => $value) {
      $colspan = (isset($monthList[$key])) ? count($monthList[$key]) : 1;

      $tNum = $tNum + $colspan - 1;
      echo '<th colspan="'.$colspan.'">'.$value.'</th>';
    }

  ?>
	<th>獎金合計</th>

<tr class="colHD">
  <th></th>
  <th></th>
  <th></th>
  <?php 
    foreach ($numList as $key => $value) {
      if(isset($monthList[$key])) {
        foreach ($monthList[$key] as $k => $v) {
          echo '<th>'.$v.'</th>';
        }
      } else {
         echo '<th></th>';
      }
    }

  ?>
  <th></th>
</tr>

</tr>
<?php 
  $allSum = array('money'=>array(), 'taxPay'=>array());
  foreach($dataList as $key => $emplyee) {
    $showList = explode('=*=', $key);
    $sum = array('money'=>array(), 'taxPay'=>array());
    $depID = $showList[0];
    $projID = $showList[1];
    echo '<tr valign="top"><td colspan="'.$tNum.'" style="font-weight:bold;font-size: 13px;">部門:'.(isset($departmentinfo[$depID]) ? $departmentinfo[$depID] : $depID).',  計劃編號:'.$projID.'</td><tr>';
  	foreach($emplyee as $ek => $r) { 
  
?>
  <tr valign="top">
    	<td>獎金小計</td><td rowspan="2"><?php echo $baseList[$key][$ek]['empName'];?></td><td rowspan="2"><?php echo $ek;?></td>
  <?php 
    	$mTotal = 0;
    	foreach ($numList as $k => $v) {
        if(!isset($sum['money'][$k])) $sum['money'][$k] = array();
        if(isset($monthList[$k])) {
            foreach ($monthList[$k] as $monthK => $monthV) {
              if(!isset($sum['money'][$k][$monthK])) $sum['money'][$k][$monthK] = 0;
              if(isset($r[$k][$monthK])) {
                //if(!isset($sum['money'][$k][$monthK])) $sum['money'][$k][$monthK] = 0;
                $sum['money'][$k][$monthK] += $r[$k][$monthK]['money'];
                $mTotal+=$r[$k][$monthK]['money'];
                echo '<td>'.number_format($r[$k][$monthK]['money']).'</td>';

              } else {
                 echo '<td></td>';
              }
            }
        } else {
          echo '<td></td>';
        }

       /* if(isset($r[$k])) {
          foreach ($r[$k] as $k2 => $v2) {
            if(!isset($sum['money'][$k][$k2])) $sum['money'][$k][$k2] = 0;
            $sum['money'][$k][$k2] += $v2['money'];
            $mTotal+=$v2['money'];
            echo '<td>'.number_format($v2['money']).'</td>';
          }
        } else{
           echo '<td></td>';
        }*/

      }
      echo '<td>'.number_format($mTotal).'</td>';
  ?>
  </tr>   
 <tr valign="top">
    	<td>代扣所得稅</td>
   <?php 
  	$mTotal = 0;
  	foreach ($numList as $k => $v) {
      if(!isset($sum['taxPay'][$k])) $sum['taxPay'][$k] = array();
      if(isset($monthList[$k])) {
        foreach ($monthList[$k] as $monthK => $monthV) {
          if(!isset($sum['taxPay'][$k][$monthK])) $sum['taxPay'][$k][$monthK] = 0;
          if(isset($r[$k][$monthK])) {
            //if(!isset($sum['taxPay'][$k][$monthK])) $sum['taxPay'][$k][$monthK] = 0;
            $sum['taxPay'][$k][$monthK] += $r[$k][$monthK]['taxMoney'];
            $mTotal+=$r[$k][$monthK]['taxMoney'];
            echo '<td>'.number_format($r[$k][$monthK]['taxMoney']).'</td>';
          } else {
             echo '<td></td>';
          }
        }
      } else {
          echo '<td></td>';
      }

     /*if(isset($r[$k])) {
        foreach ($r[$k] as $k2 => $v2) {
          if(!isset($sum['taxPay'][$k][$k2])) $sum['taxPay'][$k][$k2] = 0;
          $sum['taxPay'][$k][$k2] += $v2['taxMoney'];
          $mTotal+=$v2['taxMoney'];
          echo '<td>'.number_format($v2['taxMoney']).'</td>';
        }
      } else{
         echo '<td></td>';
      }*/


    }
    echo '<td>'.number_format($mTotal).'</td>';
  ?>
  </tr>  
  <?php  } 
    echo '<tr valign="top">';
    echo '<td>獎金小計</td><td colspan="2" rowspan="2">小計：</td>';
    $alltotal = 0;
    foreach ($sum['money'] as $sk => $sv) {
      if(!isset($allSum['money'][$sk])) $allSum['money'][$sk] = array();
      if(count($sv) == 0) {
        echo '<td>0</td>';
        continue;
      }
      foreach ($sv as $ssk => $ssv) {
        if(!isset($allSum['money'][$sk][$ssk])) $allSum['money'][$sk][$ssk] = 0;
        $allSum['money'][$sk][$ssk] += $ssv;
        $alltotal += $ssv;
        echo '<td>'.number_format($ssv).'</td>';
      }

    }
    echo '<td>'.number_format($alltotal).'</td>';
    echo '</tr>';

    echo '<tr valign="top">';
    echo '<td>代扣所得稅</td>';
    $alltotal = 0;
    foreach ($sum['taxPay'] as $sk => $sv) {
      if(!isset($allSum['taxPay'][$sk])) $allSum['taxPay'][$sk] = array();
      if(count($sv) == 0) {
        echo '<td>0</td>';
        continue;
      }
      foreach ($sv as $ssk => $ssv) {
        if(!isset($allSum['taxPay'][$sk][$ssk])) $allSum['taxPay'][$sk][$ssk] = 0;
        $allSum['taxPay'][$sk][$ssk] += $ssv;
        $alltotal += $ssv;
        echo '<td>'.number_format($ssv).'</td>';
      }

    }
    echo '<td>'.number_format($alltotal).'</td>';
    echo '</tr>';
  }
  echo '<tr valign="top"><td colspan="'.$tNum.'">-</td></tr>';
  echo '<tr valign="top">';
  echo '<td>獎金小計</td><td colspan="2" rowspan="2">總計：</td>';
  $alltotal = 0;
  foreach ($allSum['money'] as $sk => $sv) {
    if(count($sv) == 0) {
      echo '<td>0</td>';
      continue;
    }
    foreach ($sv as $ssk => $ssv) {
      $alltotal+=$ssv;
      echo '<td>'.number_format($ssv).'</td>';
    }

  }
  echo '<td>'.number_format($alltotal).'</td>';
  echo '</tr>';

  echo '<tr valign="top">';
  echo '<td>代扣所得稅</td>';
  $alltotal = 0;
  foreach ($allSum['taxPay'] as $sk => $sv) {
    if(count($sv) == 0) {
      echo '<td>0</td>';
      continue;
    }
    foreach ($sv as $ssk => $ssv) {
      $alltotal+=$ssv;
      echo '<td>'.number_format($ssv).'</td>';
    }

  }
  echo '<td>'.number_format($alltotal).'</td>';
  echo '</tr>';
 ?> 

 </table>
</form>


<form name="excelfm2" action="excelDetail.php" method="post" target="excel">
	<input type="hidden" name="year" value="<?php echo $year;?>">
	<input type="hidden" name="projIDList" value="<?php echo implode(',', $projIDList);?>">
  <input type="hidden" name="search_list" value="<?php echo implode(' and ', $search_list);?>">
</form>

<script type="text/javascript">
	var rztExcel = false;
	function getExcelV(){
		var url = 'selectExcel.php?'; 
        if(rztExcel){
        	rztExcel.close();
        }
        if(rztExcel == false){
        	rztExcel = window.open(url, "web",'width=600,height=750');
        }else{
        	rztExcel = window.open(url, "web",'width=600,height=750');
        	rztExcel.focus();
        }
	}
</script>


</body>
</Html>