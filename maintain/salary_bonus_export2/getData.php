<?php 
	function getOtherBonus($year, $search_list = '', $projIDList = '', $type = '') {
		$result = array();
		$dataList = array();
		$sql = "select b.money, b.empID, b.id, b.bcTitle, b.wkMonth, b.taxMoney, e.empName, c.invNo as checkInvNo  from emplyee_bonus b inner join bonus_class c on b.bcID = c.bcID inner join emplyee as e on b.empID = e.empID where b.wkMonth >= '".($year-1)."12' and b.wkMonth <= '".$year."11' ";
		if($type != '') $sql.= " and c.type='".$type."'";
    	if(trim($search_list) != '') $sql.= " and ".$search_list;
    	$sql .= " order by e.empID, b.wkMonth, b.bcTitle ";

    	$rs = db_query($sql);
    	$empIDList = array();
    	while ($r=db_fetch_array($rs)) {
      		$empIDList[$r['empID']] = "'".$r['empID']."'";
      		$dataList[$r['empID']][$r['wkMonth'].'_'.$r['id']] = $r;
    	}

    	//取員工每月的計劃ID&部門ID
    	$empHistory = getEmpHistory($empIDList);
    	//塞入計劃ID&部門ID
    	$dataList = setProjIDAndDepID($dataList, $empHistory);


    	foreach ($dataList as $empID => $v1) {
	     	foreach ($v1 as $wkMonth => $v2) {
	     		$wm = explode('_', $wkMonth);
	     		$wm = $wm[0];
	     		$result['baseList'][$v2['depID'].'=*='.$v2['projID']][$empID] = $v2;
	        	$result['dataList'][$v2['depID'].'=*='.$v2['projID']][$empID][$wm][$v2['bcTitle']] = $v2;
	        	$result['monthList'][$wm][$v2['bcTitle']] = $v2['bcTitle'];
	      	}
	    }

	     //整理資料
    	$result = organisingData($result, $projIDList);

    	return $result;
	}
?>