<?
function getClassTree($nID = 0) {
	global $ID, $Title, $tableName, $conn, $nTreeRecursiveLimit;
	$nTreeRecursiveLimit++;
	if ($nTreeRecursiveLimit > 100) return false;	
	
	$rs = db_query("select $ID as ClassID,$Title as Title from $tableName where parent='$nID'",$conn);
	if(db_eof($rs)) return false;
	$aTree = array();
	while($r=db_fetch_array($rs)) {
		$aChild = getClassTree($r["ClassID"]);
		if ($aChild) $aTree[$r["ClassID"]] = array("name"=>$r["Title"], "child"=>$aChild);
		else $aTree[$r["ClassID"]] = array("name"=>$r["Title"]);
	}
	$nTreeRecursiveLimit--;
	return $aTree;
}

//遞迴限制，避免無限迴圈
$nTreeRecursiveLimit = 0;
$tree = getClassTree($RootClass);
//print_r($tree); exit;

/*
  $rs = db_query("select $ID as ClassID,$Title as Title from $tableName where $PID='$RootClass'",$conn);
	while($r=db_fetch_array($rs)) {
		$rs2 = db_query("select $ID as ClassID,$Title as Title from $tableName where $PID='$r[ClassID]'",$conn);
		$id[$r[ClassID]] = $r[Title]; 
		if(!db_eof($rs2)){
			while($r2=db_fetch_array($rs2)) {
				$rs3 = db_query("select $ID as ClassID,$Title as Title from $tableName where $PID='$r2[ClassID]'",$conn);
				if(!db_eof($rs3)) {
					$id[$r2[ClassID]] = $r2[Title];
					while($r3=db_fetch_array($rs3)) {
						$rs4 = db_query("select $ID as ClassID,$Title as Title from $tableName where $PID='$r3[ClassID]'",$conn);
						if(!db_eof($rs4)) {
							$id[$r3[ClassID]] = $r3[Title];
							while($r4=db_fetch_array($rs4)) $tree[$r[ClassID]][$r2[ClassID]][$r3[ClassID]][$r4[ClassID]]=$r4[Title];
						} else $tree[$r[ClassID]][$r2[ClassID]][$r3[ClassID]]=$r3[Title];
					}
				} else $tree[$r[ClassID]][$r2[ClassID]] = $r2[Title];
			} 
		} else $tree[$r[ClassID]] = $r[Title];
	}		
	*/
  //print_r($tree); exit;
?>	
<style type="text/css">
.whitebg {background-color:white; font-size:12px;}
.whitebg a:link { text-decoration:none }
.whitebg a:visited { text-decoration: none;}
.whitebg a:hover {	color: #FF0000;	text-decoration: none;}
.whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
</style>
<link rel="stylesheet" href="/Scripts/jquery.treeview.css">
<script type="text/javascript" src="/Scripts/jquery.js"></script>
<script type="text/javascript" src="/Scripts/jquery.cookie.js"></script>
<script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
<script type="text/javascript">$(document).ready(function(){	$("#browser").treeview(	{	persist:"location", collapsed: true, unique: true} );});</script>

<table border="0" cellpadding="4" cellspacing="1" bgcolor="#333333" width="100%">
<tr><th bgcolor="#CCCCCC">::: 類別代碼表 :::</th></tr>
<tr><td bgcolor="#FFFFFF">
<a href='list.php?filter=0' style="text-decoration:none"><?=$ROOT_NAME?></a>
<div id="markup" class="whitebg"><ul id="browser" class="filetree">
<?
if($tree) {
	//遞迴限制，避免無限迴圈
	$nTreeRecursiveLimit = 0;
	echo showClassTree($tree);
}

function showClassTree($tree) {
	global $nTreeRecursiveLimit;
	$nTreeRecursiveLimit++;
	if ($nTreeRecursiveLimit > 100) return false;	
	
	if (!$tree) return "";
	$sHTML = "";
	foreach($tree as $k=>$v) {
		if (is_array($v["child"])) $sChild = "<ul>".showClassTree($v["child"])."</ul>";
		else $sChild = "";
		$sHTML .= "<li><span class='folder'><a href='list.php?filter=$k&cnm=".$v["name"]."'>".$v["name"]."</a></span>".$sChild."</li>\n";
	}
	$nTreeRecursiveLimit--;
	return $sHTML;
}
?> 
</ul></div>
</td></tr>
</table>