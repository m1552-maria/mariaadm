<?	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "事件類別管理";
	$tableName = "event_class";
	$extfiles = '../';				//js及css檔的路徑位置
	
	$PID = 'parent';
	$CLASS_PID = 'event_class_PID';
	// for classid.php
	$ID  = 'ID';
	$Title = 'Title';
	
	// Here for List.asp ======================================= //
	//if($_SESSION['domain_Host']) {
	//	$RootClass = $_SESSION['user_classdef'][0];		
	//	$rsT = db_query("select Title from event_class where ID='$RootClass'"); 
	//	$rT = db_fetch_array($rsT); 
	//	$ROOT_NAME = $rT[0];
	//} else { 不管制源頭(root)
		$RootClass = 0;
		$ROOT_NAME = '事件類別';
	//}

	if (isset($_REQUEST[filter])) $_SESSION[$CLASS_PID]=$_REQUEST[filter]; 
	if (!$_SESSION[$CLASS_PID]) $_SESSION[$CLASS_PID]=$RootClass;
	$filter = "$PID='$_SESSION[$CLASS_PID]'"; 
	if ($_REQUEST[cnm]) $_SESSION[cnm]=$_REQUEST[cnm]; else {	
		if(count($_REQUEST)<2 || $_SESSION[$CLASS_PID]==0) $_SESSION[cnm]=$ROOT_NAME;
	}
	
	// for Upload files and Delete Record use
	$ulpath = '../map_icon/';
	$delField = 'path';
	$delFlag = false;
	
	// Here for List.asp ======================================= //
	
	$defaultOrder = "Seque";
	$searchField1 = "Title";
	$searchField2 = "Title";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,Title,rdate,Seque");
	$ftAry = explode(',',"代碼,名稱,建立日期,次序");
	$flAry = explode(',',"70,100,100,");
	$ftyAy = explode(',',"ID,text,date,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"parent,Title,description,Seque");
	$newftA = explode(',',"上層代碼,名稱,敘述,次序");
	$newflA = explode(',',"20,80,80,");
	$newfhA = explode(',',",,6,");
	$newfvA = array($_SESSION[$CLASS_PID],'','','','');
	$newetA = explode(',',"readonly,text,textbox,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,Title,description,Seque");
	$editftA = explode(',',"代碼,名稱,敘述,次序");
	$editflA = explode(',',"20,80,80,");
	$editfhA = explode(',',",,6,");
	$editfvA = array('','','','','');
	$editetA = explode(',',"readonly,text,textbox,text");
?>