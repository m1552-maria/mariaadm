<?	
	session_start();
  	header('Content-type: text/html; charset=utf-8');
	if ($_SESSION['privilege']<10) {
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include('../../miclib.php');	
	include('../inc_vars.php');

	
	$bE = false;	//異動權限	
	$pageTitle = "銀行媒體轉帳清冊";
	$tableName = "emplyee_transfer as a left join emplyee as b";
	//$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/emplyee_transfer/';
	$delField = '';
	$delFlag = false;

	$outAccount = $TransferList[key($TransferList)];
	$unitList = array();
	foreach ($TransferList as $key => $value) {
		$unitList[$key] = $key;
	}

	$sqlAcc = 'select fid,actno from psi_banks order by fid asc';
	$rsAcc = db_query($sqlAcc);
	$rowAcc = array();
	while ($rAcc = db_fetch_array($rsAcc)) {
		if(!$rowAcc[$rAcc['fid']]) $rowAcc[$rAcc['fid']] = array();
		$rowAcc[$rAcc['fid']][]=$rAcc['actno'];
	}

	/*print_r($unitList);
	exit;*/

	$yearList = array();
	$sql = "select year from transfer where 1 order by year desc";
	$rs = db_query($sql, $conn);
	while($r=db_fetch_array($rs)) {
		$yearList[$r['year']] = $r['year'];
	}

	$year = (count($yearList) > 0) ? key($yearList) : '';

	//:: to 執行過濾功能 --------------------------------------------------------------------- >
	if($_REQUEST[projFilter]){
		$projTemp = explode(',', $_REQUEST[projFilter]);
		foreach ($projTemp as $key => $value) {
			$aa[] = "b.projID like '$value%'";	
		}
		$temp[] = "(" . join(' or ',$aa) . ")"; 
		$projid = $_REQUEST[projFilter];
	}else if ($_SESSION['privilege'] <= 10) {
		if($_SESSION['user_classdef'][6]) {
			$aa = array();
			foreach($_SESSION['user_classdef'][6] as $v){
				if($v){
					$aa[]="b.projID like '$v%'";$projid = $v;
				}
			}
			$temp[] = "(".join(' or ',$aa).")"; 	//"depID='$_SESSION[depID]'"
		} else $temp[] = "b.projID=''";
	}

	if(!empty($_REQUEST['year'])){
		$temp[] = "a.year='$_REQUEST[year]'";	
		$year = $_REQUEST['year'];
	} else {
		$temp[] = "a.year='".$year."'";	
	}


	$titleList = array();
	$sql = "select id, title from transfer where year='".$year."' order by id desc";
	$rs = db_query($sql, $conn); 
	while($r=db_fetch_array($rs)) {
		$titleList[$r['id']] = $r['title'];
	}

	$title = (count($titleList) > 0) ? key($titleList) : '';

	if(!empty($_REQUEST['title']) && !empty($titleList[$_REQUEST['title']])){
		$temp[] = "a.tID ='$_REQUEST[title]'";	
		$tID = $_REQUEST[title];
		$title = $_REQUEST['title'];
	} else {
		$temp[] = "a.tID='".$title."'";
		$tID = $title;	
	}

	if(!empty($_REQUEST['payType'])){
		$temp[] = "b.payType ='$_REQUEST[payType]'";
		$payType = $_REQUEST['payType'];
	}

	if(count($temp)>0) $filter = join(' and ',$temp);
	// Here for List.asp ======================================= //

	//left join 條件
	$defWh = "b.empID=a.empID";


	$defaultOrder = "b.empID";
	$searchField1 = "b.empID";
	$searchField2 = "b.empName";
	$sortDirection = 'ASC';

	$pageSize = 20;	//每頁的清單數量

	$fnAry = explode(',',"empID,empName,title,depID,unit,bNum,identifier,class,loan,inDate,inAccount,con,money,outAccount,payType,status");
	$ftAry = explode(',',"員工編號,員工姓名,標題,部門,委託單位,批號,識別碼,類別,借貸,轉帳日期,轉入帳號,筆數,金額,轉出帳號,發放類別,已轉帳");
	$flAry = explode(',',",,,,,,,,,,,,,,,");
	$ftyAy = explode(',',"text,text,text,text,text,text,text,text,text,text,text,text,text,text,text,text");

?>