<?php
  if(!session_id()) session_start();
 
  $filename = '媒體轉帳-詳細資料'.date("Y-m-d H:i:s",time()).'.xlsx';
  

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include_once("init.php");
  include_once('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');

  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();

 // $title = array('轉入帳號','員工姓名','金額','發放方式','備註');
  $title = array('轉入帳號','轉入金額','轉入帳號統編','戶名','發放方式','備註');
  //$fnAry = array('inAccount','empName','money','payType','notes');
  $fnAry = array('inAccount','money','perID','empName','payType','notes');

  $ftAryCount = count($title);
  for($i=0;$i<$ftAryCount;$i++) {
    $sheet->setCellValue(chr(65+$i).'3', $title[$i]);
  }

  $sheet->mergeCells("A1:E1");
  $sheet->setCellValue("A1","瑪利亞基金會");
  $sheet->mergeCells("A2:E2");
  $sheet->setCellValue("A2","員工薪資帳號清冊");
  $sheet->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

  $sql = $_REQUEST['sql'];
  $tranSum = 0;
  $tranPeo = 0;
  $cashSum = 0;
  $cashPeo = 0;
  $moneyTSum = 0;
  $moneyTPeo = 0;
  $arrearsSum = 0; //負數總額
  $rs = db_query($sql);
  $j=3;
  while ($r = db_fetch_array($rs)) {
      if($j == 3) {
        $sheet->mergeCells("A2:E2");
        $sheet->setCellValue("A2",$r['title']."清冊");
        $sheet->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      }

      $Date = $r['inDate'];
      $j++;
      for($i=0;$i<count($fnAry);$i++) {
        $val = isset($r[$fnAry[$i]]) ? $r[$fnAry[$i]] : '';
        switch ($fnAry[$i]) {
            case 'inAccount':
            case 'empName':
            case 'perID':
                $sheet->getColumnDimension(chr(65+$i))->setWidth(18);
                $sheet->setCellValueExplicit(chr(65+$i).$j, (string)$val, PHPExcel_Cell_DataType::TYPE_STRING);
                break;
            case 'money':
                $sheet->getColumnDimension(chr(65+$i))->setWidth(18);
                $sheet->setCellValueExplicit(chr(65+$i).$j, number_format($val), PHPExcel_Cell_DataType::TYPE_STRING);
                break;
            case 'payType':
                if($r['money'] < 0) {
                  $arrearsSum += $r['money'];
                  break;
                }
                switch ($val) {
                    case '轉入帳戶':
                        $tranSum += $r['money'];
                        $tranPeo++;
                        break;
                    case '領現':
                        $cashSum += $r['money'];
                        $cashPeo++;
                        break;
                    case '匯款':
                        $moneyTSum += $r['money'];
                        $moneyTPeo++; 
                        break;
                }
                $sheet->getColumnDimension(chr(65+$i))->setWidth(15);
                $sheet->setCellValue(chr(65+$i).$j, $val);
            break;
        }
      }
  }
  $j+=6;

  $sum = $moneyTSum+$tranSum+$cashSum+$arrearsSum;
  $Psum = $moneyTPeo+$cashPeo+$tranPeo;

  $sheet->mergeCells("A".$j.":E".$j);
  $sheet->getStyle("A".$j.":E".$j)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
  $sheet->setCellValue("A".$j++,'資料內容');

  $sheet->mergeCells("A".$j.":E".$j);
  $sheet->setCellValue("A".$j++,'預定轉帳日：'.$Date );

  $sheet->mergeCells("A".$j.":E".$j);
  $sheet->setCellValue("A".$j,'轉入員工帳：'.number_format($tranSum));
  $sheet->setCellValue("E".$j++,$tranPeo.'筆');

  $sheet->mergeCells("A".$j.":E".$j);
  $sheet->setCellValue("A".$j,'領現金：'.number_format($cashSum));
  $sheet->setCellValue("E".$j++,$cashPeo.'筆');

  $sheet->mergeCells("A".$j.":E".$j);
  $sheet->setCellValue("A".$j,'匯款：'.number_format($moneyTSum));
  $sheet->setCellValue("E".$j++,$moneyTPeo.'筆');

  $sheet->mergeCells("A".$j.":E".$j);
  $sheet->setCellValue("A".$j,'金額總計：'.number_format($sum));
  $sheet->setCellValue("E".$j++,$Psum.'筆');
  if($arrearsSum != 0) {
    $sheet->setCellValue("A".$j,'正數總計：'.number_format(($sum-$arrearsSum)));
    $j++;
    $sheet->setCellValue("A".$j,'負數總計：'.number_format($arrearsSum));
  }  
  $j++;

  $k = $j+3;
  $sheet->mergeCells("A".++$j.":A".$k);
  $sheet->getStyle("A".$j)->getAlignment()->setWrapText(true)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $sheet->setCellValue("A".$j,"單位\n名稱");

  $sheet->mergeCells("B".$j.":B".$k);
  $sheet->getStyle("B".$j)->getAlignment()->setWrapText(true)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $sheet->setCellValue("B".$j,"印\n章");
  
  $sheet->mergeCells("D".$j.":E".$k);
  $sheet->getStyle("D".$j)->getAlignment()->setWrapText(true)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $sheet->setCellValue("D".$j,"負\n責\n人");

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');


?>