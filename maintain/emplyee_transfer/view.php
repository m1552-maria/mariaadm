<!DOCTYPE HTML>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?=$pageTitle?></title>
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css"> 
 <link href="../../Scripts/form.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">

 <style type="text/css">
	#u_setting{display:none;overflow: auto;height:250px; width:400px; position: absolute;top:80px;left:120px;}
	.tip{color:#900;}
	.divTitle{
		color:#fff;
		background-color:#900;
		border: 0px;
		padding: 8px;
		font-family:'微軟正黑體'; 
		font-size:14px;
		width:364px;
		margin-top:10px;
		margin-left:10px;
	}
	.divBorder1 { /* 內容區 第一層外框 */
		background-color:#dedede;
		border:1px solid #666;
		padding:0px;
		margin:0px;
		font-size:16px;
	}

	.divBorder2 { /* 內容區 第二層外框 */
		padding:8px; 
		background:#FFFFFF;
		margin:0px 10px 0px 10px;
		
		height:175px;
		overflow-x:hidden;
		overflow-y:auto;
	}
	#d_bottom{	padding-top:8px;}


 </style>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="../../Scripts/form.js" type="text/javascript"></script> 

 
 <script>
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		//::過濾條件要跟
		var depflt = $('select#projFilter option:selected').val();
		var yrflt = $('select#year option:selected').val();
		var url = "<?=$PHP_SELF?>?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		if(depflt) url = url+'&projFilter='+depflt;
		if(yrflt) url = url+'&year='+yrflt;

		location.href = url;		
	}

	
	function doSubmit() {
		form1.submit();
	}

	function filterSubmit(tSel) {
		tSel.form.submit();
	}


 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>
<body>
<a name="top"/>
<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
 	<tr><td colspan="2">
		<input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
    <select name="projFilter" id="projFilter" onChange="filterSubmit(this)"><option value=''>全部帳號</option>
	<? 
		if ($_SESSION['privilege'] > 10) {
			$sqlBank = 'select * from psi_banks b left join psi_company c on(b.fid=c.id)';
			$rsBank = db_query($sqlBank,$conn);
			while ($rBank=db_fetch_array($rsBank)) {
				$privUnit[$rBank['projID']] = $rBank['unit'];
				$optionBank[$rBank['projID']] = $rBank['fid'].' - '.$rBank['actno'].' ('.$rBank['title'].' - '.$rBank['projID'].')';
			}
			foreach($optionBank as $k=>$v) echo "<option value='$k'".($_REQUEST[projFilter]==$k?' selected ':'').">$v</option>";
		}else{
			if($_SESSION['user_classdef'][6]){
				foreach ($_SESSION['user_classdef'][6] as $key => $value) {
					$user6[] = "b.projID ='".$value."'";
					$user[] = $value;
				}
				$priv6 = 'where '.join(' or ',$user6);
				$priv = join(',',$user);

				$sqlBank = 'select * from psi_banks b left join psi_company c on(b.fid=c.id)'.$priv6;
				$rsBank = db_query($sqlBank,$conn);
				while ($rBank=db_fetch_array($rsBank)) {
					$privBank[] = $rBank['unit'];
					$privUnit[$rBank['projID']] = $rBank['unit'];
					$optionBank[$rBank['projID']] = $rBank['fid'].' - '.$rBank['actno'].' ('.$rBank['title'].' - '.$rBank['projID'].')';
				}
				foreach($optionBank as $k=>$v) echo "<option value='$k'".($_REQUEST[projFilter]==$k?' selected ':'').">$v</option>";
			}
		}

    ?>
    </select>
    <select name="year" id="year" onChange="doSubmit()">
    	<? foreach ($yearList as $key => $value) {
    		$sel = ($year == $key) ? 'selected':'';
    	 	 echo "<option $sel value='".$key."'>".$value." 年</option>"; 
    	} ?>
    	
    </select>
    <select name="title" id="title" onChange="doSubmit()">
    	<? foreach ($titleList as $key => $value) {
    		$sel = ($title == $key) ? 'selected':'';
    	 	 echo "<option $sel value='".$key."'>".$value."</option>"; 
    	} ?>
    	
    </select>
    <select name="payType" onChange="doSubmit()">
    	<option value="">全部發放類別</option>
    <?php foreach($payTypeList as $key => $value) {
      $selected = ($key == $payType) ? 'selected' : '';
    ?>
        <option value="<?=$key;?>" <?=$selected;?> ><?=$value;?></option>
    <?php } ?>
    </select>
   	<input type="button" value="匯入轉帳資料" onclick="u_set()">
    <input type="button" value="匯出轉帳清冊" id="downloadTxt">
    <input type="button" value="匯出詳細資料" onclick="excelDetail();">
	<input type="button" value="關帳" onclick="closeAll();">
	<input type="button" value="重載資料" onclick="reloadData()">
  	</td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>

		<?=$ftAry[$i]?>
	</th>
	<? } ?>
  </tr>
<?
	//過濾條件
	if(isset($filter)) {
		$sql = "select a.*, b.empID,b.empName,b.depID,b.projID,b.perID from $tableName on $defWh where $filter";
	} else {
		$sql = "select a.*, b.empID,b.empName,b.depID,b.projID,b.perID from $tableName on $defWh where 1";
	}

	// if ($_SESSION['privilege'] <= 10){  
	// 	if($_SESSION['user_classdef'][2]){
	// 		foreach($_SESSION['user_classdef'][2] as $v) $depIDList[]="b.depID like '$v%'";
	// 	}else $depIDList[] = "b.depID=''";
	// }
	// if($depIDList) $sql.= " and (".join(' or ',$depIDList).")";

	// 搜尋處理
	if ($keyword!="") {
		$sql.=" and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%'))";
	}
  	// 排序處理
  	if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
  	$excelSql = $sql;

  	//進清單預先將Unit寫入 避免關帳unit不能對應20191111
  	$rs = db_query($sql,$conn);
  	while ($r=db_fetch_array($rs)) {
  		if($r['status'] == '1') continue;
  		$project = preg_split('//', $r['projID'], -1, PREG_SPLIT_NO_EMPTY);
		$projID = $project[0].$project[1];
  		$sqlP = "select * from psi_banks b left join psi_company c on(b.fid=c.id) where projID like '%$projID%'";
		$rsP = db_query($sqlP,$conn);
		$rP = db_fetch_array($rsP);
  		if($r['inAccount']){
			$sqla = "update emplyee_transfer set unit='".$rP['unit']."',outAccount='".$rP['actno']."' where empID='".$r['empID']."' and tID='".$r['tID']."' and money>='0'";
			$rsa = db_query($sqla,$conn);
		}
  	}
	//分頁
  	$sql .= " limit ".$pageSize*$pages.",$pageSize";  
	//echo $sql; //exit;
  	$rs = db_query($sql,$conn);
  	$j = $pages*10;
	while ($r=db_fetch_array($rs)) {
		$project = preg_split('//', $r['projID'], -1, PREG_SPLIT_NO_EMPTY);
		$projID = $project[0].$project[1];
		$sqlP = "select * from psi_banks b left join psi_company c on(b.fid=c.id) where projID like '%$projID%'";
		$rsP = db_query($sqlP,$conn);
		$rP = db_fetch_array($rsP);
		$j++;
		$id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><?=$id?></td>
		<? for($i=1; $i<count($fnAry); $i++) {?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? 	switch ($ftyAy[$i]) {
					case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
					case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
					case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
					case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
					case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
					case "datetime" : echo date('Y/m/d H:i',strtotime($fldValue)); break;
					case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
					case "empID" : echo $emplyeeinfo[$fldValue]; break;
					default : 
						switch ($fnAry[$i]) {
							case 'depID':
								echo $departmentinfo[$fldValue];	
								break;
							case 'con':
								echo str_pad('1',7,'0',STR_PAD_LEFT);
								break;
							case 'status':
								$ch = ($fldValue == 1) ? 'checked':'';
								echo '<div style="text-align:center;"><input disabled data-checkbox="'.$r['empID'].'" type="checkbox" value="1" '.$ch.'></div>';
								break;
							case 'unit':
								if($r['status'] == '1'){
								 	echo $rP['fid'].' - '.$rP['actno'].' ('.$rP['title'].')';
								}else{
									// $project = preg_split('//', $r['projID'], -1, PREG_SPLIT_NO_EMPTY);
									// $projID = $project[0].$project[1];
									// $sqlP = "select fid,actno from psi_banks where projID='$projID'";
									// $rsP = db_query($sqlP,$conn);
									// $rP = db_fetch_array($rsP);
									echo '<select name="unit'.$r['empID'].'" onchange="bankAccount2('."'".$r['empID']."'".')">';
									echo "<option value='".$rP['unit']."' selected>".$rP['fid'].' - '.$rP['actno'].' ('.$rP['title'].' - '.$rP['projID'].')'."</option>";
									echo '</select>';
								}
								break;
							case 'outAccount':
								if($r['status'] == '1'){
								 	echo $fldValue;
								}else{
									if($r['inAccount']){
										$sqla = "update emplyee_transfer set unit='".$rP['unit']."',outAccount='".$rP['actno']."' where empID='".$r['empID']."' and tID='".$r['tID']."' and money>='0'";
										$rsa = db_query($sqla,$conn);
									}
									echo '<select name="outAccount'.$r['empID'].'">';
									echo "<option value='".$rP['actno']."' selected>".$rP['actno']."</option>";
									echo '</select>';
									echo '<input type="button" value="送出" onclick="accountSub('."'".$r['empID']."'".')" >';
								}
								// if($r['inAccount']){
								// 	$sql = "update emplyee_transfer set unit='".$rP['unit']."',outAccount='".$rP['actno']."'";
								// 	$rs = db_query($sql,$conn);
								// }
								break;
							case 'inAccount':
								echo "<input type='text' value='$fldValue' name='inAcc".$r['empID']."'>";
								echo '<input type="button" value="送出" onclick="inAccSub('."'".$r['empID']."'".')" >';
								break;
							default:
								echo $fldValue;	
								break;
						}
						break;
			    }
			?>
			</td>
		<? } ?>	
	</tr>
<? } ?> 
 </table>
</form>

<form name="updatefm" action="update.php" method="post">
	<input type="hidden" name="tID" value="<?=$title;?>">
	<input type="hidden" name="filter" value="<?=$filter;?>">
	<input type="hidden" name="yearUp">
	<input type="hidden" name="titleUp">
	<input type="hidden" name="projFilterUp">
	<div id='u_setting' class='divBorder1'>
		<div class='divTitle'>匯出條件<input type="button" value="取消" style="float:right;cursor:pointer;margin-left:5px;" onclick="$('#u_setting').hide();"><input id="checkDate" type="button" value="確定" style="float:right;cursor:pointer;"></div>
		<div class='divBorder2'>
			<div style="margin-top: 5px;">
				匯出日期:
				<input type="text" id="outDate" name="date" size="" value="<?php echo date('Y/m/d');?>" class="input">
				<input type="hidden" name="tID" value="<?=$tID?>">
				<input type="hidden" name="pID" value="<?=$projid?>">
			</div>
		</div>
	</div>
</form>
<form name="excelfm" action="excel.php" method="post" target="excel">
	<input type="hidden" name="tID" value="<?=$tID?>">
	<input type="hidden" name="priv" value="<?=$priv?>">
	<input type="hidden" name="filter" value="<?=$filter;?>">
</form>

<form name="excelfm2" action="excelDetail.php" method="post" target="excel">
	<input type="hidden" name="sql" value="<?=$excelSql;?>">
</form>
<script type="text/javascript">
	var privBank = <?=empty($privBank) ? "''" : json_encode($privBank) ?>;
	var privUnit = <?=json_encode($privUnit)?>;
	function closeAll(){
		if(typeof privBank === 'undefined'){
			var privBank = <?=empty($privBank) ? "''" : json_encode($privBank) ?>;
		}
		if(confirm("是否要關帳？")){
			if($('#projFilter').val() != '') var privBank = [privUnit[$('#projFilter').val()]];
			$.ajax({
				url: 'closeAll.php',
				type: 'post',
				data:{
					id: $('select[name="title"]').val(),
					priv: privBank
				},
				dataType: 'json',
				success:function(d){
					location.reload();
				}
			});
		}
	}
	function reloadData(){
		if($('#projFilter').val() != '') var privBank = [privUnit[$('#projFilter').val()]];
		$.ajax({
				url: 'reloadData.php',
				type: 'post',
				data:{
					tID: $('select[name="title"]').val(),
					priv: privBank
				},
				dataType: 'json',
				success:function(d){
					location.reload();
				}
			});
	}
	function excelDetail() {
		$('[name="excelfm2"]').submit();
	}

	function u_set(){
		$('#u_setting').show();
		bankAccount();
	}
	function bankAccount(){
		$('[name="outAccount"]').find("option").remove();
		$.ajax({
			url: 'selectId.php',
			type: 'post',
			data:{
				fid:$('[name="unit"]').val()
			},
			dataType: 'json',
			success:function(d){
				$.each(d,function(i,n){
					$('[name="outAccount"]').append("<option value='"+n['actno']+"'>"+n['actno']+"</option>");
				});	
			}
		});
	}
	function bankAccount2(empID){
		$('[name="outAccount'+empID+'"]').find("option").remove();
		$.ajax({
			url: 'selectId.php',
			type: 'post',
			data:{
				fid:$('[name="unit'+empID+'"]').val()
			},
			dataType: 'json',
			success:function(d){
				$.each(d,function(i,n){
					$('[name="outAccount'+empID+'"]').append("<option value='"+n['actno']+"'>"+n['actno']+"</option>");
				});	
			}
		});
	}
	// function moneySub(empID){
	// 	$.ajax({
	// 		url: 'moneySub.php',
	// 		type: 'post',
	// 		data:{
	// 			money: $('[name="money'+empID+'"]').val(),
	// 			empID:empID,
	// 			tID: $('[name="tID"]').val()
	// 		},
	// 		dataType: 'json',
	// 		success:function(d){
	// 			location.reload();
	// 		}
	// 	});
	// }
	function inAccSub(empID){
		var inAcc = $('[name="inAcc'+empID+'"]').val();
		if(inAcc.length >13){
			alert('帳號格式有誤，帳號長度不可超過13碼');
			return false;
		}
		$.ajax({
			url: 'inAcc.php',
			type: 'post',
			data:{
				inAccount: inAcc,
				empID:empID,
				tID: <?=$tID?>
			},
			dataType: 'json',
			success:function(d){
				alert('銀行帳號修改完成！');
			}
		});
	}
	function accountSub(empID){
		$.ajax({
			url: 'accountSub.php',
			type: 'post',
			data:{
				unit:$('[name="unit'+empID+'"]').val(),
				outAccount: $('[name="outAccount'+empID+'"]').val(),
				empID:empID,
				tID: <?=$tID?>
			},
			dataType: 'json',
			success:function(d){
				if(d == '1'){
					alert('帳號銀行不同，無法修改');
				}
				location.reload();
			}
		});
	}
	var transferList = <?php echo json_encode($TransferList);?>;
	$(function(){
		$('#u_setting').draggable();
		$('#outDate').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});
		$('#outDate').datepicker('option', 'duration', '');
		$('#outDate').datepicker($.datepicker.regional['zh-TW']);

		$('#d_setting').draggable();
		$('#outDate').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});
		$('#outDate').datepicker('option', 'duration', '');
		$('#outDate').datepicker($.datepicker.regional['zh-TW']);
		
		$('#checkDate').click(function(){
			var timeCheck = /^([0-9]{4})[./]{1}([0-9]{1,2})[./]{1}([0-9]{1,2})$/;
			if(!timeCheck.test($('#outDate').val())){
				alert('請輸入時間 ex: 2018/01/01');
				return false;
			}
			$('[name="yearUp"]').val($('#year').val());
			$('[name="titleUp"]').val($('#title').val());
			$('[name="projFilterUp"]').val($('#projFilter').val());
			$('[name="date"]').val($('#outDate').val());
			$('#u_setting').hide();
			updatefm.submit();
		});
		$('#downloadTxt').click(function(){
			$.ajax({
				url: 'checkDate.php',
				type: 'post',
				data:{
					tID: <?=$tID?>,
					sql: $('input[name="sql"]').val()
				},
				async: false,
				dataType: 'json',
				success:function(d){
					if(d == '1') alert('日期未匯入');
					else{
						$('[name="date"]').val($('#outDate').val());
						excelfm.submit();
						//setTimeout(function(){location.reload();}, 1000)
					}
				}
			});
		});
	})

	$('[data-all="checkbox"]').click(function(){
		$('[name="depID[]"]').prop('checked',$(this).prop('checked'));
	});
	
</script>
</body>
</Html>