<?php 
	/* 下載ZIP */
	class download{
		protected $_filename;
		protected $_filepath;
		protected $_filesize;//文檔大小
		public function __construct($filename){
			$this->_filename = $filename;
			$this->_filepath = dirname(__FILE__).'/'.$filename;
			$this->getfiles();
		}
		public function getfilepath(){
			return $this->_filepath;
		}
		public function getfiles(){ //下載文檔的功能
			if (file_exists($this->_filepath)){ //檢查文檔是否存在
				$file = fopen($this->_filepath,"r");//打開文檔
				Header("Content-type: application/octet-stream");
				Header("Accept-Ranges: bytes");
				Header("Accept-Length: ".filesize($this->_filepath));
				Header("Content-Disposition: attachment; filename=".$this->_filename);
				echo fread($file, filesize($this->_filepath));//修改之前，一次性將數據傳輸給客户端
				$buffer=1024;//修改之後，一次只傳輸1024個字節的數據給客户端 向客户端回送數據
				//判斷文檔是否讀完
				while (!feof($file)) {
					$file_data=fread($file,$buffer);//將文檔讀入內存
					echo $file_data;//每次向客户端回送1024個字節的數據
				}
				fclose($file);
			}
		}
	}

	/* 銀行格式 */
	class bankIDType{
		protected $data;
		protected $bNum; // 批號 流水號
		protected $sql;
		public $type;
		public function __construct($data,$bNum){
			$this->data = $data;
			$this->bNum = $bNum;
			$empID = $this->data[empID]; 
			$this->sql = "update emplyee_transfer set bNum='$bNum' where empID='$empID' and tID='$_POST[tID]'";
			$this->_init();
		}
		private function _init(){
			$sql = "select type from transfer where id='$_POST[tID]'";
			$rs = db_query($sql);
			$r = mysql_fetch_assoc($rs);
			$this->type = $r['type'];
		}
		public function echoInfo(){ // 回傳要查詢的值
			return $this->sql;
		}

		public function ID103(){
			$rSta = db_query($this->sql);
			$fileText = '';

			$outText = array();
			$outText[] = $this->data['unit'];
			$outText[] = str_pad($this->bNum,2,'0',STR_PAD_LEFT);
			$outText[] = $this->data['identifier'];
			$outText[] = $this->data['class'];
			$outText[] = $this->data['loan'];
			$outText[] = $this->data['inDate'];
			$outText[] = $this->data['inAccount'];
			$outText[] = str_pad('1',7,'0',STR_PAD_LEFT);
			$money = $this->data['money'].'00';
			$money = str_pad($money,15,'0',STR_PAD_LEFT);
			$outText[] = $money;
			$outText[] = $this->data['outAccount'];
			$fileText .= implode('', $outText);
			$fileText .= "\r\n";

			return $fileText;
		}
		public function ID053(){
			$rSta = db_query($this->sql);
			$fileText = '';
			$outText = array();

			$outText[] = '1';
			$outText[] = '055';
			$outText[] = $this->data['inAccount'];
			if($this->type == '薪資') $outText[] = 'B5';
			else if($this->type == '年終' || $tihs->type == '三節獎金') $outText[] = 'D5';
			$outText[] = '1';
			$money = $this->data['money'].'00';
			$money = str_pad($money,12,'0',STR_PAD_LEFT);
			$outText[] = $money;
			$empID = str_pad($this->data['empID'],9,'0',STR_PAD_LEFT);
			$outText[] = $empID.' ';
			$outText[] = '0000';
			$outText[] = '00000';
			$outText[] = '@00000000000000@              ';
			$outText[] = $this->data['empName'];
			$a = 10 -(mb_strlen($this->data['empName'],"utf-8")*2);
			if($a>0) for($iNull=1;$iNull<=$a;$iNull++) $outText[] = ' ';
			$outText[] = '          ';
			$fileText .= implode('', $outText);
			$fileText .= "\r\n";
			return $fileText;
		}
	}

?>