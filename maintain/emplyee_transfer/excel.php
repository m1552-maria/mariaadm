<?php
// ini_set('display_errors','1');
// error_reporting(E_ALL);
	@session_start();
	header("Content-Type:text/html; charset=utf-8");
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include 'bankClass.php';

	$sql = "select 1 from emplyee_transfer where status<>'1' and tID='".$_POST['tID']."'";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
	if(!$r)	echo "<script>alert('已關帳不能匯出');window.close();</script>";

	$sql = 'select * from emplyee_transfer group by outAccount';
	$rs = db_query($sql);
	//查詢有幾個不同的銀行帳號
	while ($r=db_fetch_array($rs)) {
		$rUnit[$r['outAccount']] = $r;
		if($r['outAccount']){
			//將每個銀行帳號的員工抓出來 用銀行帳號算筆數
			$sqlUnit = "select 1 from emplyee_transfer where outAccount='$r[outAccount]' and money>0 and status<>'1' and tID='".$_POST['tID']."' and payType='轉入帳戶'";
			$rsUnit = db_query($sqlUnit);
			$rUnit[$r['outAccount']]['i'] = db_num_rows($rsUnit);
		}
	}

	$projID = explode(',',$_POST['priv']);
	if($projID){
		foreach ($projID as $key => $value) {
			$ab[] = "projID like '%".$value."%'";
		}
		$whereProj = ' where '.join(' or ',$ab);
	}
	$sql = 'select actno from psi_banks'.$whereProj;
	$rs = db_query($sql);
	while ($r=db_fetch_array($rs)) {
		$actno[] = $r['actno'];
	}

	$depIDList = array();
	$projIDList = array();
	$filter = str_replace('a.','t.',$_POST['filter']);
  	$filter = str_replace('b.','e.',$filter);
	$sql = "select t.*,e.empName from emplyee_transfer t inner join emplyee e on t.empID=e.empID where t.inAccount <>'' and t.payType='轉入帳戶' and ".$filter;
	if ($_SESSION['privilege'] <= 10){  
		if($_SESSION['user_classdef'][6]){
			foreach($actno as $v) $projIDList[]="t.outAccount = '$v'";
		}else $projIDList[] = "";

	}
	if($depIDList) $sql.= " and (".join(' or ',$depIDList).")";
	if($projIDList) $sql.= " and (".join(' or ',$projIDList).")";

	$sqlLog = $sql." and unit<>'' and money>'0' and status<>'1' group by outAccount order by t.unit asc, t.empID asc";
	$sql.= " order by t.outAccount asc, t.empID asc";

	$rs = db_query($sql);
	$rsLog = db_query($sqlLog);
	$bNum = 1;
	while ($r = db_fetch_array($rsLog)) {
		$sql = "select 1 from transfer_log where outDate='".$r['inDate']."' and unit='".$r['unit']."' and outAccount='".$r['outAccount']."'";
		$rs2 = db_query($sql);
		$bNum += (int)db_num_rows($rs2);
		$fields = array(
			"tID='".$_POST['tID']."'",
			"outDate='".$r['inDate']."'",
			"bNum='".$bNum."'",
			"unit='".$r['unit']."'",
			"outAccount='".$r['outAccount']."'"
		);
		$sql = "insert into transfer_log set ".implode(',', $fields);
		db_query($sql);
		$sql = "update transfer set notEdit=1 where id='".$_POST['tID']."'";
		db_query($sql);
	}

	$totalMoney = array(0);
	$totalText = array();
	$outText = array();
	$fileText = array();
	while ($r = mysql_fetch_assoc($rs)){
		if($r['status'] == '1') continue;
		if($r['money']<=0) continue;

		$sqlBank = "select bankID from psi_banks where actno='{$r['outAccount']}'";
		$rsBank = db_query($sqlBank);
		$rBank = mysql_fetch_assoc($rsBank);
		$bankIDType = new bankIDType($r,$bNum);
		$accID = $r['outAccount'];

		if(empty($r['unit']) && $rBank['bankID'] == '103') continue;

		switch ($rBank['bankID']) {
			case '103':
				$totalMoney[$accID] += $r['money'];

				$totalText[$accID]['unit'] = $r['unit'];
				$totalText[$accID]['bNum'] = str_pad($bNum,2,'0',STR_PAD_LEFT);
				$totalText[$accID]['identifier'] = '0';
				$totalText[$accID]['class'] = $r['class'];
				$totalText[$accID]['loan'] = 'D';
				$totalText[$accID]['inDate'] = $r['inDate'];
				$totalText[$accID]['inAccount'] = $r['outAccount'];
				$totalText[$accID]['i'] = str_pad($rUnit[$r['outAccount']]['i'],7,'0',STR_PAD_LEFT);
				$totalText[$accID]['money'] = $totalMoney[$accID];
				$totalText[$accID]['money'] .= '00';
				$totalText[$accID]['money'] = str_pad($totalText[$accID]['money'],15,'0',STR_PAD_LEFT);
				$totalText[$accID]['outAccount'] = '0000000000000';

				$fileText[$accID] .= $bankIDType->ID103();
				$outText[$r['outAccount'].'.txt'] = $fileText[$accID].implode('', $totalText[$accID]);
				break;
			case '053':
				$totalMoney[$accID] += $r['money'];

				$totalText[$accID]['class'] = '8';
				$totalText[$accID]['busNum'] = '055'; //企業編號3碼
				$totalText[$accID]['outAccount'] = $r['outAccount'];
				if($bankIDType->type == '薪資'){
					$totalText[$accID]['type'] = 'B5';
					$outName[$accID] = 'BS0055'.$bNum;
				}else if($bankIDType->type == '年終' || $bankIDType->type == '三節獎金'){
					$totalText[$accID]['type'] = 'D5';
					$outName[$accID] = 'BR0055'.$bNum;
				}
				$totalText[$accID]['loan'] = '0';
				$totalText[$accID]['money'] = $totalMoney[$accID];
				$totalText[$accID]['money'] .= '00';
				$totalText[$accID]['money'] = str_pad($totalText[$accID]['money'],12,'0',STR_PAD_LEFT);
				$totalText[$accID]['inDate'] = $r['inDate'];
				$totalText[$accID]['banksNum'] = '045'; 
				$totalText[$accID]['null'] = '0'; //空白 無使用
				$totalText[$accID]['other'] = '00'; //備註 無使用
				$totalText[$accID]['busNum4'] = '0055';//企業編號4碼
				$totalText[$accID]['null2'] = ' @00000000000000@    '; //空白
				$totalText[$accID]['ID'] = '98758507  '; //轉帳檢核ID
				$totalText[$accID]['name'] = '財團法人瑪'; //最大5個中文字
				$totalText[$accID]['null3'] = '          '; //空白 無使用

				$fileText[$accID] .= $bankIDType->ID053();
				$outText[$outName[$accID].'.txt'] = implode('', $totalText[$accID])."\r\n".$fileText[$accID];
				break;
		}
	}

	foreach ($outText as $key => $value) {
		$fp = fopen("$key", 'a');
		fwrite($fp, $value);
		fclose($fp);
		$filename[] = "$key";
	}

	// if($filename){
	$zipname = date("Y-m-d").'.zip';
	$zip = new ZipArchive;
	$zip->open($zipname, ZipArchive::CREATE);
	foreach ($filename as $key => $value) $zip->addFile($value);
	$zip->close();

	$dw = new download($zipname); //下載文檔
	unlink($zipname); //下載完成後要進行刪除
	foreach ($filename as $key => $value) unlink($value);
	// }else{
		// echo "<script>alert('對不起,您可能重複匯出');window.close();</script>";
	// }

?>