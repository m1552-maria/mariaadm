<?
  if($_REQUEST['sqltx']) {
    
    $filename = date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");

    include '../../config.php';
    include '../../getEmplyeeInfo.php';
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);

    
    
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
   
    $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
    
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:I1");
    $objPHPExcel->getActiveSheet()->mergeCells("A2:B2");
    $objPHPExcel->getActiveSheet()->mergeCells("A3:B3");
    $objPHPExcel->getActiveSheet()->mergeCells("C2:I2");
    $objPHPExcel->getActiveSheet()->mergeCells("C3:I3");
    //對齊方式   
    $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    //框線
    $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
    
    /*
    VERTICAL_CENTER 垂直置中
    VERTICAL_TOP
    HORIZONTAL_CENTER
    HORIZONTAL_RIGHT
    HORIZONTAL_LEFT
    HORIZONTAL_JUSTIFY
    */
    $objPHPExcel->getActiveSheet()->setCellValue('A1','團體保險-保單名冊');
    $objPHPExcel->getActiveSheet()->setCellValue('A2','保單名稱：');
    $objPHPExcel->getActiveSheet()->setCellValue('C2',$_REQUEST[title]);
    $objPHPExcel->getActiveSheet()->setCellValue('A3','製作日期：');
    $objPHPExcel->getActiveSheet()->setCellValue('C3',date("Y/m/d"));

    $objPHPExcel->getActiveSheet()->setCellValue('A4','流水號'); 
    $objPHPExcel->getActiveSheet()->setCellValue('B4','姓名'); 
    $objPHPExcel->getActiveSheet()->setCellValue('C4','身分字號'); 
    $objPHPExcel->getActiveSheet()->setCellValue('D4','生日');
    $objPHPExcel->getActiveSheet()->setCellValue('E4','投保金額');
    $objPHPExcel->getActiveSheet()->setCellValue('F4','附加醫療險');
    $objPHPExcel->getActiveSheet()->setCellValue('G4','被保險人身分');
    $objPHPExcel->getActiveSheet()->setCellValue('H4','已退保');
    $objPHPExcel->getActiveSheet()->setCellValue('I4','退保原因');
    

    $i = 5;

    $sql = $_REQUEST[sqltx];
    $rs = db_query($sql);

    while($rs && $r=db_fetch_array($rs)) {
      
      $eid = $r[empID];

      $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,($i-4));
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$r[name]);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$r[pid]);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,intval($r['birthday'])?date('Y-m-d',strtotime($r['birthday'])):'');
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$r[insfee]);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$r[addMedical]);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$r[ptype]);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$r[isOff]==1?'V':'');
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$r[notes]);


      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
    }

    
    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    //設定欄寬
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);



    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    $objWriter->save('php://output');




  }


  
?>