<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include("../../config.php");
	include '../../getEmplyeeInfo.php';
		
	$bE = $_SESSION['privilege']>10?true:false;	//異動權限	
	$pageTitle = "部門資料維護";
	$tableName = "department";
	$extfiles = '';
	
	$PID = 'PID';
	$CLASS_PID = 'department_PID';
	
	// Here for List.asp ======================================= //
	//if($_SESSION['domain_Host']) {  開啟所有類別功能
	//	$RootClass = $_SESSION['domain_Host'];
	//	$rsT = db_query("select depTitle from department where depID='$RootClass'"); 
	//	$rT = db_fetch_array($rsT); 
	//	$ROOT_NAME = $rT[0];			
	//} else {
		$RootClass = 0;
		$ROOT_NAME = '瑪利亞基金會';			
	//}
	
	if (isset($_REQUEST['filter'])) $_SESSION[$CLASS_PID]=$_REQUEST['filter']; 
	if (!isset($_SESSION[$CLASS_PID])) $_SESSION[$CLASS_PID]=$RootClass;
	$filter = "$PID='$_SESSION[$CLASS_PID]'"; 
	if (isset($_REQUEST['cnm'])) $_SESSION['cnm']=$_REQUEST['cnm']; else {	
		if(count($_REQUEST)<2 || $_SESSION[$CLASS_PID]==0)  $_SESSION['cnm']=$ROOT_NAME;
	}
	
	$defaultOrder = "depID";
	$searchField1 = "depTitle";
	$searchField2 = "depEn";
	$pageSize = 20;	//每頁的清單數量	
	//$channel = array(1=>'門市',2=>'網站');
	
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"depID,PID,depTitle,depName,depEn,description,depKind,leadID,rdate");
	$ftAry = explode(',',"代碼,父類別,全稱,簡稱,英文名稱,部門敘述,類型,部門主管,建立日期");
	$flAry = explode(',',"70,70,100,,,,50,,100");
	$ftyAy = explode(',',"ID,text,text,text,text,text,text,qryemp,date");
	
	// Here for Append.asp =========================================================================== //
	//::pre-con in append.php
	$newfnA = explode(',',"PID,depID,depTitle,depName,depEn,description,depKind");
	$newftA = explode(',',"父類別,代碼,全稱,簡稱,英文名稱,部門敘述,類型");
	$newflA = explode(',',"20,20,80,,,80,");
	$newfhA = explode(',',",,,0,,6,");
	$newfvA = array($_SESSION[$CLASS_PID],'','','','','','');
	$newetA = explode(',',"readonly,text,text,text,text,textbox,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"depID,PID,depTitle,depName,depEn,description,depKind");
	$editftA = explode(',',"代碼,父類別,全稱,簡稱,英文名稱,部門敘述,類型");
	$editflA = explode(',',",,80,,,80,");
	$editfhA = explode(',',",,,,,6,");
	$editfvA = array('','','','','','','');
	$editetA = explode(',',"ID,text,text,text,text,textbox,text");
?>