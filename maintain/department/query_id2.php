<?php
	$self = $_SERVER['PHP_SELF'];
	include 'init.php';
	if(isset($_REQUEST['param'])) {
		$aa = explode(',',$_REQUEST['param']);
		$ba = array();
		foreach($aa as $v) $ba[]="'$v':1";
		$baStr = "{".join(',',$ba)."}";
	} else {
		$baStr = "new Object()";
	}
	
	function getClassTree($nID = 0) {
		global $PID, $Title, $tableName, $conn, $nTreeRecursiveLimit;
		$nTreeRecursiveLimit++;
		if ($nTreeRecursiveLimit > 100) return false;	
		$sql = "select depID as ClassID,depName as Title from $tableName where $PID='$nID'";
		$rs = db_query($sql);
		if(db_eof($rs)) return false;
		$aTree = array();
		while($r=db_fetch_array($rs)) {
			$aChild = getClassTree($r["ClassID"]);
			if ($aChild) $aTree[$r["ClassID"]] = array("name"=>$r["Title"], "child"=>$aChild);
			else $aTree[$r["ClassID"]] = array("name"=>$r["Title"]);
		}
		$nTreeRecursiveLimit--;
		return $aTree;
	}
	
	//遞迴限制，避免無限迴圈
	$nTreeRecursiveLimit = 0;
	$tree = getClassTree($RootClass);
  //print_r($tree); exit;
?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font-size:12px;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {color:#0000ff; font-weight:bold }
	#selItems { background-color:#f0f0f0; width:75%; }
	#toolpan {position:fixed; background-color:#eee; width:100%}
	#content {position:absolute; top:30px; padding-top:0}
	
  </style>
  <link rel="stylesheet" href="/Scripts/jquery.treeview.css">
  <script type="text/javascript" src="/Scripts/jquery.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.cookie.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
  <script type="text/javascript">
		var selid;					//送入的 id
		var selObj = <?php echo $baStr?>;
		var idStr;
		
		$(document).ready(function(){
			$("#browser").treeview({persist:"location", collapsed: true, unique: true});
			$('li span').each(function(idx, elm) {
        if(selObj[elm.title]) $(elm).addClass('selItem');
      });

		});
	  function setID(id,elm) {
			//alert(id);
			if(selObj[id]) {
				$(elm).removeClass('selItem');
				delete selObj[id];
			} else {
				$(elm).addClass('selItem');
				selObj[id] = 1;
			}
			var idA = new Array();
			for(var key in selObj) idA.push(key);
			idStr = idA.join(',');
			$('#selItems').val(idStr);
			//console.log(idStr);
	  }
		function returnSeletions() {
			if(window.opener) {
				window.opener.openWindowReturn(idStr);
			}
			window.close(); 
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">
<div id="toolpan">
<input id="selItems" type="text" value="<?php echo $_REQUEST['param']?>" readonly /> <input id="btnOK" type="button" value="確定" onClick="returnSeletions()" />
</div>
<table id="content" border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
<tr><td bgcolor="#FFFFFF"><?php echo $ROOT_NAME?>
<div id="markup" class="whitebg"><ul id="browser" class="filetree">
<?php
if($tree) {	//遞迴限制，避免無限迴圈
	$nTreeRecursiveLimit = 0;
	echo showClassTree($tree);
}

function showClassTree($tree) {
	global $nTreeRecursiveLimit;
	$nTreeRecursiveLimit++;
	if ($nTreeRecursiveLimit > 100) return false;	
	
	if (!$tree) return "";
	$sHTML = "";
	foreach($tree as $k=>$v) {
		if (is_array($v["child"])) $sChild = "<ul>".showClassTree($v["child"])."</ul>";
		else $sChild = "";
		$sHTML .= "<li><span title='$k' class='folder' onClick='setID(\"$k\",this)'>".$v['name']."</span>".$sChild."</li>\n";
	}
	$nTreeRecursiveLimit--;
	return $sHTML;
}
?> 
</ul></div>
</td></tr>
</table>
</body>
</html>