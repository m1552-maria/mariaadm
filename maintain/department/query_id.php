<?php
	$self = $_SERVER['PHP_SELF'];
	//include 'init.php';
	
	include("../../config.php");
	$pageTitle = "部門資料維護";
	$tableName = "department";	
	$PID = 'PID';
	$CLASS_PID = 'department_PID';
	$ROOT_NAME = '瑪利亞基金會';	

	 $host = '';
  if(isset($_GET['ishost'])) {
  	$host = explode('.', $_SERVER['HTTP_HOST']);
  	array_shift($host);
  	$host = implode('.', $host);
  }

  $rs = db_query("select depID as ClassID,depName as Title from $tableName where $PID='0' order by depID");
	while($r=db_fetch_array($rs)) {
		$rs2 = db_query("select depID as ClassID,depName as Title from $tableName where $PID='".$r['ClassID']."' order by depID");
		//建立代碼對應表
		$id[$r['ClassID']] = $r['Title']; 
		if(!db_eof($rs2)){
			while($r2=db_fetch_array($rs2)) {
				$rs3 = db_query("select depID as ClassID,depName as Title from $tableName where $PID='".$r2['ClassID']."' order by depID");
				if(!db_eof($rs3)) {
					$id[$r2['ClassID']] = $r2['Title'];
					while($r3=db_fetch_array($rs3)) $tree[$r['ClassID']][$r2['ClassID']][$r3['ClassID']]=$r3['Title'];
				} else $tree[$r['ClassID']][$r2['ClassID']] = $r2['Title'];
			} 
		} else $tree[$r['ClassID']] = $r['Title'];
	}	
	///if($_SESSION['user_classdef'][2] && $_GET["allowAll"]!="Y")	$allow_list=$_SESSION['user_classdef'][2];
?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font-size:12px;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	.gray{	color:#999;}
  </style>
  <link rel="stylesheet" href="/Scripts/jquery.treeview.css">
  <script type="text/javascript" src="/Scripts/jquery.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.cookie.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
  <script type="text/javascript">
		var selid;					//送入的 id
		<?php if($host != '') { ?> document.domain="<?php echo $host;?>"; <?php } ?>
		var mulsel = false;	//可複選
		var selObjs = new Array();
		$(document).ready(function(){
			//selid = dialogArguments; for showDialog
			//selid <-- call by caller
			$("#browser").treeview({persist:"location", collapsed: true, unique: true});
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
<tr>
	<th id="tblHead" bgcolor="#CCCCCC">::: 類別代碼表 :::</th>
</tr>
<tr><td bgcolor="#FFFFFF"><?php echo $ROOT_NAME?>
<div id="markup" class="whitebg"><ul id="browser" class="filetree">
<?php if($tree){ 
	if(isset($allow_list)){//from 前台
		foreach($tree as $k=>$v) { 
			 if (is_array($v)) {
			 	if(in_array($k,$allow_list)){
			 		echo "<li><span class='folder'><a onClick=setID('$k','$id[$k]',this)>$id[$k]</a></span><ul>";
			 	}else{
					echo "<li><span class='folder'>$id[$k]</span><ul>";
			 	}
				 foreach($v as $k2=>$v2) {	 
					 if (is_array($v2)) {
					 	if(in_array($k2,$allow_list) || in_array($k,$allow_list)){
						    echo "<li><span class='folder'><a onClick=setID('$k2','$id[$k2]',this)>$id[$k2]</a></span><ul>";
						}else{
							echo "<li><span class='folder gray'>$id[$k2]</span><ul>";
						}
						foreach($v2 as $k3=>$v3) {
						 	if(in_array($k2,$allow_list) || in_array($k2,$allow_list) || in_array($k,$allow_list)){
							 	echo "<li><span class='file'><a onClick=setID('$k3','$v3',this)>$v3</a></span></li>";
							}else{
								echo "<li><span class='file gray'>$v3</span></li>";
							}
						}
						echo "</ul></li>";
					 } else{
					 	if(in_array($k2,$allow_list) || in_array($k,$allow_list)){
						 	echo "<li><span class='file'><a onClick=setID('$k2','$v2',this)>$v2</a></span></li>";
						}else{
						 	echo "<li><span class='file gray'>$v2</span></li>";
						}
					 } 
				 }
				 echo "</ul></li>";	 
			 } else{
			 	if(in_array($k,$allow_list)){
				 	echo "<li><span class='folder'><a onClick=setID('$k','$id[$k]',this)>$id[$k]</a></span></li>";
				 }else{
				 	echo "<li><span class='folder gray'>$id[$k]</span></li>";
				 }
			 } 
		}
	}else{//from 後台
		foreach($tree as $k=>$v) { 
			 if (is_array($v)) {
		 	 	 echo "<li><span class='folder'><a onClick=setID('$k','$id[$k]',this)>$id[$k]</a></span><ul>";
				 foreach($v as $k2=>$v2) {	 
					 if (is_array($v2)) {
					    echo "<li><span class='folder'><a onClick=setID('$k2','$id[$k2]',this)>$id[$k2]</a></span><ul>";
						 foreach($v2 as $k3=>$v3) {
						 	echo "<li><span class='file'><a onClick=setID('$k3','$v3',this)>$v3</a></span></li>";
						 }
						 echo "</ul></li>";
					 } else{
					 	echo "<li><span class='file'><a onClick=setID('$k2','$v2',this)>$v2</a></span></li>";
					 } 
				 }
				 echo "</ul></li>";	 
			 } else echo "<li><span class='folder'><a onClick=setID('$k','$id[$k]',this)>$id[$k]</a></span></li>";
		}
	}
   } ?> 
</ul></div>
</td></tr>
</table>
</body>
</html>