<?php
	$self = $_SERVER['PHP_SELF'];
	include 'init.php';

	/*
	include("../../config.php");
	$pageTitle = "部門資料維護";
	$tableName = "department";	
	$PID = 'PID';
	$CLASS_PID = 'department_PID';
	$ROOT_NAME = '瑪利亞基金會';	*/

	$rs = db_query("select depID as ClassID,depName as Title from $tableName where $PID='0' order by depID");
	while($r=db_fetch_array($rs)) {
		$rs2 = db_query("select depID as ClassID,depName as Title from $tableName where $PID='".$r['ClassID']."' order by depID");
		//建立代碼對應表
		$id[$r['ClassID']] = $r['Title']; 
		if(!db_eof($rs2)){
			while($r2=db_fetch_array($rs2)) {
				$rs3 = db_query("select depID as ClassID,depName as Title from $tableName where $PID='".$r2['ClassID']."' order by depID");
				if(!db_eof($rs3)) {
					$id[$r2['ClassID']] = $r2['Title'];
					while($r3=db_fetch_array($rs3)) $tree[$r['ClassID']][$r2['ClassID']][$r3['ClassID']]=$r3['Title'];
				} else {
					$tree[$r['ClassID']][$r2['ClassID']] = $r2['Title'];
					$id[$r2['ClassID']] = $r2['Title'];
				}
			} 
		} else {
			$tree[$r['ClassID']] = $r['Title'];
			$id[$r['ClassID']] = $r['Title'];
		}
	}	


	$emplyee_list = array();
	$sql = 'select * from `emplyee` where `isOnduty`=1';
	$rs = db_query($sql);
	while($r=db_fetch_array($rs)) {
		if($r['depID']==''){
			if($r['empID']!=''){
				$emplyee_list['other'][$r['empID']] = $r['empName'];
			}
		}else{
			if($r['empID']!=''){
				$emplyee_list[$r['depID']][$r['empID']] = $r['empName'];
			}	
		}
	}

	if($_SESSION['user_classdef'][2] && $_GET["allowAll"]!="Y")	$allow_list=$_SESSION['user_classdef'][2];
	$allow_list_str=join(",",$allow_list);
?>
<!DOCTYPE HTML>
<html>
<head>
  	<meta charset="utf-8">
  	<meta http-equiv="cache-control" content="max-age=0" />
  	<meta http-equiv="expires" content="0">
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-cache">
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font-size:12px;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	.gray{	color:#999;}
	.black{	color:#000000;}
	.title{	background: #BE0A09;	font-family: '微軟正黑體';	color:#ffffff;	text-align: center;	font-size: 14px;}
	.title >td{padding:6px;}
	.top-btn{	
		/*float:right;*/
	    padding: 2px 10px;
	    margin-bottom: 0;
	    font-size: 12px;
	    font-weight: 400;
	    line-height: 1.42857143;
	    text-align: center;
	    white-space: nowrap;
	    vertical-align: middle;
	    -ms-touch-action: manipulation;
	    touch-action: manipulation;
	    cursor: pointer;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    background-image: none;
	    border: 1px solid transparent;
	    border-radius: 4px;
	    color: #fff;
	    background-color: #f0ad4e;
	    border-color: #eea236;
	}
  </style>
  <link rel="stylesheet" href="/Scripts/jquery.treeview.css">
  <script type="text/javascript" src="/Scripts/jquery.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.cookie.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
  <script type="text/javascript">
		var selid;	///				//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		var uiType="<?php echo $_REQUEST['uiType']?$_REQUEST['uiType']:''?>";
		$(document).ready(function(){
			//selid = dialogArguments; for showDialog
			//selid <-- call by caller
			$("#browser").treeview({persist:"location", collapsed: true, unique: true});
			
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});

		$(window).load(function(){
			//處理帶入的值
			if(!selid) selid = window.opener.ppppp;
			var list = selid.split('/');
			var class_list = [];
			depID = list[0].split(',');
			empID = list[1].split(',');
			if(depID!=''){
				$.each(depID,function(k,v){
					$('input[type="checkbox"]').each(function(){
						if($(this).val()==v){
							$(this).attr('checked',true);
							$(this).parent().find('span').css({'color':'red'});

							//抓目前emp的class
							class_list = $(this).attr('class').split(' ');
							$.each(class_list,function(k1,v1){
								$('input[type="checkbox"]').each(function(){
									if($(this).val()==v1){
										$(this).parent().children('span').css({'color':'red'});
									}
								})
							})
						}
					})
					$('.'+v).attr('checked',true);
					$('.'+v).parent().find('span').css({'color':'red'});
				})	
			}
			if(empID!=''){
				$.each(empID,function(k,v){
					$('input[type="checkbox"]').each(function(){
						if($(this).val()==v){
							$(this).attr('checked',true);
							$(this).parent().find('span').css({'color':'red'});
							
							//抓目前emp的class
							class_list = $(this).attr('class').split(' ');
							$.each(class_list,function(k1,v1){
								$('input[type="checkbox"]').each(function(){
									if($(this).val()==v1){
										$(this).parent().children('span').css({'color':'red'});
									}
								})
							})
						}	
					})
				})
			}
		})

	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}

		function checkemp(now){
			var now_class = $(now).attr('class');
			if(now_class!=''){
				now_class = now_class.split(' ');
			}
			
			var now_value = $(now).val();

			// 先判斷這個是否被勾選
			if ($(now).is(':checked')) {
				$('.'+now_value).attr('checked',true);//底下的全部都要勾
				//判斷是不是這層都被勾選  上層也要勾起來
				
				//一層一層判斷
				for(var i=now_class.length-1;i>=0;i--){
					tag = 1;
					this_class = now_class[i];
					$('.'+this_class).each(function(){
						if(!$(this).is(':checked')){//遇到沒勾就跳出
							tag = 0;
							return false;
						}
					})
					if(tag == 1){
						// if(uiType!='onlyEmp'){//onlyEmp：只選擇員工
							$('input[type="checkbox"]').each(function(){
								if($(this).val()==now_class[i]){
									$(this).attr('checked',true);
								}	
							})
						// }
					}
				}
				
			}else{
				$('.'+now_value).attr('checked',false);//底下的全部都不能勾
				//上面的都要拿掉
				$.each(now_class,function(k,v){
					$('input[type="checkbox"]').each(function(){
						if($(this).val()==v){
							$(this).attr('checked',false);
						}	
					})
				})
			}
		}


	
		function emp_submit(){
			//處理顯示畫面的部分處理全部的input
			$('input[type="checkbox"]').each(function(e){
				//過濾
				if($(this).is(':checked')){
					if(uiType=="onlyEmp"){//只選擇員工
					}else{
						$('.'+$(this).val()).remove();
					}
				}
			})

			$('input[name="depID[]"]').each(function(e){
				if($(this).is(':checked')){
					selObjs.push({'type':'depID','id':$(this).val(),'name':$('[name="depIDname['+$(this).val()+']"]').val(),'privilege':$(this).attr('privilege')});
				}
			})

			$('input[name="empID[]"]').each(function(e){
				if($(this).is(':checked')){
					selObjs.push({'type':'empID','id':$(this).val(),'name':$('[name="depIDname['+$(this).val()+']"]').val(),'privilege':$(this).attr('privilege')});
				}
			})

			if(uiType=='batch'){
				window.opener.proBatch(selObjs);
			}else window.opener.returnValue_other(selObjs);
			window.opener.rzt = false;
			window.close();
		}

  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%" style="position: fixed;">
<tr class='title'>
	<td style="text-align: center;width: 80%;"><?php echo $ROOT_NAME?></td>
	<td style="text-align: right;"><input type="button" onclick="emp_submit()" value="送出" class='top-btn'></td>
</tr>

</table>
<div id="markup" class="whitebg"><ul id="browser" class="filetree" style="padding: 38px 0 0 0;">
<?php 

if($tree){ 
	foreach($tree as $k=>$v) {
		 if (is_array($v) or isset($emplyee_list[$k])) {//多一個員工條件
		 	if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list))) {
		 	 	getDom($k,$id[$k],'dep');
		 	}else{
	 	 		getDomByAllowList($k,$id[$k],'dep',getAllowClass($allow_list_str,$k));
	 	 	}
			//分類
			foreach($v as $k2=>$v2) {	 
				if (is_array($v2) or isset($emplyee_list[$k2])) {
					$class = $k;
					if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list)) || 
						(isset($allow_list) && in_array($k2,$allow_list))) {
						getDom($k2,$id[$k2],'dep',$class);
					}else{
	 	 				getDomByAllowList($k2,$id[$k2],'dep',getAllowClass($allow_list_str,$k2),$class);
					}
					//分類
					foreach($v2 as $k3=>$v3) {
						if(isset($emplyee_list[$k3])){
							$class = $k.' '.$k2;
							if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list)) || 
								(isset($allow_list) && in_array($k2,$allow_list)) || 
								(isset($allow_list) && in_array($k3,$allow_list)) ) {
								getDom($k3,$v3,'dep',$class);

								//員工
								foreach($emplyee_list[$k3] as $ek3=>$ev3){
									$class = $k.' '.$k2.' '.$k3;
									getDom($ek3,$ev3,'emp',$class);
								}
							}else{
	 	 						getDomByAllowList($k3,$v3,'dep',getAllowClass($allow_list_str,$k3),$class);
	 	 						//員工
								foreach($emplyee_list[$k3] as $ek3=>$ev3){
									$class = $k.' '.$k2.' '.$k3;
									getDomByAllowList($ek3,$ev3,'emp',getAllowClass($allow_list_str,$k3),$class);
								}
							}
							echo "</ul></li>";
						}else{
							$class = $k.' '.$k2;
							if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list)) || 
								(isset($allow_list) && in_array($k2,$allow_list)) || 
								(isset($allow_list) && in_array($k3,$allow_list)) ) {
								getDom($k3,$v3,'dep_other',$class);
							}else{
								getDomByAllowList($k3,$v3,'dep_other',getAllowClass($allow_list_str,$k3),$class);
							}
						}
					}
					//員工
					if(isset($emplyee_list[$k2])){
						if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list)) || 
							(isset($allow_list) && in_array($k2,$allow_list))) {
							foreach($emplyee_list[$k2] as $ek2=>$ev2){
								$class = $k.' '.$k2;
								getDom($ek2,$ev2,'emp',$class);
							}
						}else{
							foreach($emplyee_list[$k2] as $ek2=>$ev2){
								$class = $k.' '.$k2;
								getDomByAllowList($ek2,$ev2,'emp',getAllowClass($allow_list_str,$k2),$class);
							}
						}
					}
					echo "</ul></li>";
				} else{
					$class = $k;
					if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list)) || 
						(isset($allow_list) && in_array($k2,$allow_list))) {
						getDom($k2,$v2,'dep_other',$class);
					}else{
						getDomByAllowList($k2,$v2,'dep_other',getAllowClass($allow_list_str,$k2),$class);
					}
				} 
			}

			//員工
			if(isset($emplyee_list[$k])){
				if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list))) {
					foreach($emplyee_list[$k] as $ek=>$ev){
						$class = $k;
						getDom($ek,$ev,'emp',$class);
					}
				}else{
					foreach($emplyee_list[$k] as $ek=>$ev){
						$class = $k;
						getDomByAllowList($ek,$ev,'emp',getAllowClass($allow_list_str,$ek),$class);
					}
				}
			}
			 echo "</ul></li>";	 
		 } else {
		 	if(!isset($allow_list) || (isset($allow_list) && in_array($k,$allow_list))) {
			 	getDom($k,$id[$k],'dep_other');
			}else{
				getDomByAllowList($k,$id[$k],'dep_other',getAllowClass($allow_list_str,$k));
			}
		 }
	}

	//假如有沒有部們的員工
	if(isset($emplyee_list['other'])){
		if(!isset($allow_list)){ 
			foreach($emplyee_list['other'] as $ek4=>$ev4){
				getDom($ek4,$ev4,'emp');
			}
		}else{
			foreach($emplyee_list['other'] as $ek4=>$ev4){
				getDomByAllowList($ek4,$ev4,'emp',getAllowClass($allow_list_str,$ek));
			}
		}
	}
} 

    function getDom($k,$v,$type,$class){
	   	if($type=='dep'){
		   	$str="<li><input type='checkbox' onClick='checkemp(this)' value='$k' name='depID[]' class='$class' privilege='Y'><input type='hidden' name='depIDname[$k]' value='$v'><span class='folder'><a>$v</a></span><ul>";
	   	}else if($type=='emp'){
	   		$str = "<li><input type='checkbox' onClick='checkemp(this)' value='$k' name='empID[]' class='$class' privilege='Y'><input type='hidden' name='depIDname[$k]' value='$v'><span class='people'><a>$v</a></span></li>";
	   	}else if($type=='dep_other'){
	   		$str = "<li><input type='checkbox' onClick='checkemp(this)' value='$k' name='depID[]' class='$class' privilege='Y'><input type='hidden' name='depIDname[$k]' value='$v'><span class='folder'><a>$v</a></span></li>";
	   	}

	   	echo $str;
    }
    function getDomByAllowList($k,$v,$type,$class,$class2){
	   	if($type=='dep'){
		   	$str="<li><input type='checkbox' onClick='return false;' value='$k' class='$class2' name='depID[]' disabled privilege='N'><input type='hidden' name='depIDname[$k]' value='$v'><span class='folder $class ' value='$k'>$v</span><ul>";
	   	}else if($type=='emp'){
	   		$str = "<li><input type='checkbox' onClick='return false;' value='$k' class='$class2' name='empID[]' disabled privilege='N'><input type='hidden' name='depIDname[$k]' value='$v'><span class='people $class' value='$k'>$v</span></li>";
	   	}else if($type=='dep_other'){
	   		$str = "<li><input type='checkbox' onClick='return false;' value='$k' class='$class2' name='depID[]' disabled privilege='N'><input type='hidden' name='depIDname[$k]' value='$v'><span class='folder $class' value='$k'>$v</span></li>";
	   	}

	   	echo $str;
    }
    function getAllowClass($allow_list_str,$k){
    	$pos = strpos($allow_list_str, $k);
 		if($pos===false){
 			$allow_class='gray';
 		}else{
 			$allow_class="black";
 		}
 		return $allow_class;
    }
   ?> 
</ul></div>


</body>
</html>