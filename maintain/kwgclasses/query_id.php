<?php
	$self = $_SERVER['PHP_SELF'];
	include 'init.php';
	
  $rs = db_query("select ID as ClassID,Title as Title from $tableName where $PID='0'",$conn);
	while($r=db_fetch_array($rs)) {
		$rs2 = db_query("select ID as ClassID,Title as Title from $tableName where $PID='$r[ClassID]'",$conn);
		//建立代碼對應表
		$id[$r[ClassID]] = $r[Title]; 
		if(!db_eof($rs2)){
			while($r2=db_fetch_array($rs2)) {
				$rs3 = db_query("select ID as ClassID,Title as Title from $tableName where $PID='$r2[ClassID]'",$conn);
				if(!db_eof($rs3)) {
					$id[$r2[ClassID]] = $r2[Title];
					while($r3=db_fetch_array($rs3)) $tree[$r[ClassID]][$r2[ClassID]][$r3[ClassID]]=$r3[Title];
				} else $tree[$r[ClassID]][$r2[ClassID]] = $r2[Title];
			} 
		} else $tree[$r[ClassID]] = $r[Title];
	}		
  //print_r($tree); exit;
?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font-size:12px;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
  </style>
  <link rel="stylesheet" href="/Scripts/jquery.treeview.css">
  <script type="text/javascript" src="/Scripts/jquery.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.cookie.js"></script>
  <script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
  <script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		$(document).ready(function(){
			//selid = dialogArguments;
			$("#browser").treeview({persist:"location", collapsed: true, unique: true});
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
<tr>
	<th id="tblHead" bgcolor="#CCCCCC">::: 類別代碼表 :::</th>
</tr>
<tr><td bgcolor="#FFFFFF"><?=$ROOT_NAME?>
<div id="markup" class="whitebg"><ul id="browser" class="filetree">
<? if($tree) foreach($tree as $k=>$v) { 
		 if (is_array($v)) {
	 	 	 echo "<li><span class='folder'><a onClick=setID('$k','$id[$k]',this)>$id[$k]</a></span><ul>";
			 foreach($v as $k2=>$v2) {	 
				 if (is_array($v2)) {
				   echo "<li><span class='folder'><a onClick=setID('$k2','$id[$k2]',this)>$id[$k2]</a></span><ul>";
					 foreach($v2 as $k3=>$v3) echo "<li><span class='file'><a onClick=setID('$k3','$v3',this)>$v3</a></span></li>";
					 echo "</ul></li>";
				 } else echo "<li><span class='file'><a onClick=setID('$k2','$v2',this)>$v2</a></span></li>";
			 }
			 echo "</ul></li>";	 
		 } else echo "<li><span class='folder'><a onClick=setID('$k','$id[$k]',this)>$id[$k]</a></span></li>";
   } ?> 
</ul></div>
</td></tr>
</table>
</body>
</html>