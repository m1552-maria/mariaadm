<?  
  session_start();
  $filename = '其他獎金清冊'.date("Y-m-d H:i:s",time()).'.xlsx';
  
  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");

  include '../../config.php';
  include('../../getEmplyeeInfo.php');  
  require_once('../Classes/PHPExcel.php');
  require_once('../Classes/PHPExcel/IOFactory.php');
    
  $list = array();
  $sql = $_REQUEST['sqlEx'];
  // var_dump($sql);
  // exit;
  $sql .=" order by a.projID ASC, b.empID ASC";

  /*print_r($sql);
  exit;*/
  $bcTitle = '';
  $wkYear;
  $rs=db_query($sql);
  if(!db_eof($rs)){
    while($r=db_fetch_array($rs)){//還要分類別
      $bcTitle = $r['bcTitle'];
      $wkYear = date('Y',strtotime($r['wkMonth']));
      if($r['projID']!=''){
        $projID = explode(',', $r['projID']);
        foreach($projID as $k=>$v){
          $list[$v][$r['depID']]['wkYear'] = $wkYear;
          $list[$v][$r['depID']]['sub_list'][$r['empID']] = $r;
        }
      }else{
        $list['no_proj'][$r['depID']]['wkYear'] = $wkYear;
        $list['no_proj'][$r['depID']]['sub_list'][$r['empID']] = $r;
      }
    }
  }

  $sql = "select empID,actDays from emplyee_annual where year='".$wkYear."'";
  /*print_r($sql);
  exit;*/
  $yearList = array();
  $rs=db_query($sql);
  while ($r = db_fetch_array($rs)) {
      $yearList[$r['empID']] = intval($r['actDays']/365);
  }


  $now_date_time = date('Y-m-d H:i:s');
  $lastYear=intval(date('Y')-1);
  

  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);
  $sheet = $objPHPExcel->getActiveSheet();

  //跑header 迴圈用
  $header = array('A','B','C','D','E','F','G','H','I');
  $headervalue = array('流水號','員工編號','姓名','到職日','類型','實際年資','獎金','實領獎金','扣稅金額');

  $i=1;
  $number = 0;
  $sum = 0;//合計
  $actual_bonus = 0;
  $sum_tax = 0;

  $final = 0; //為了最後一筆
  $final1 = 0;
  foreach($list as $k=>$v){
    $final++;
    $final1=0;
    foreach($v as $k1=>$v1){
      $final1++;
      //頭
      if($final == 1 && $final1 == 1) {
        $sheet->mergeCells('A'.$i.':I'.$i);//合併
        $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
        $sheet->getStyle('A'.$i.':I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
        $i++;
        $sheet->mergeCells('A'.$i.':I'.$i);
        $sheet->setCellValue('A'.$i,($v1['wkYear']-1911).'年〔'.$bcTitle.'〕清冊');
        $sheet->getStyle('A'.$i.':I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
        $i++;
        $sheet->mergeCells('A'.$i.':B'.$i);//合併
        
        $sheet->setCellValue('I'.$i,'印製日期：'.$now_date_time);
        $sheet->getStyle('I'.$i)->getAlignment()->setWrapText(true);//要有這個才能換行
      }
  
      if($k == 'no_proj') $sheet->setCellValue('A'.$i,'計畫編號:無');
      else $sheet->setCellValue('A'.$i,'計畫編號:'.$k);

      $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
      $i++;
      $sheet->mergeCells('A'.$i.':B'.$i);//合併
      $sheet->setCellValue('A'.$i,'組別名稱:'.$departmentinfo[$k1]);
      //$sheet->setCellValue('H'.$i,'年資結算止日:'.($v1['wkYear']-1).'-12-31');
      $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
      $i++;
      //表頭
      foreach($header as $k3=>$v3){
        $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
        $sheet->getStyle($v3.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue($v3.$i,$headervalue[$k3]);
      }
      $sheet->getStyle('A'.$i.':I'.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
      $i++;

      //需要用到的變數
      $sub_sum   = 0;//小計 原始獎金
      $sub_bonus = 0;//小計 實領獎金
      $subtax    = 0;//小計 扣稅金額
      $people    = 0;//人數

      //員工列表
      foreach($v1['sub_list'] as $k2=>$v2){
       // if($v2['bonus'] == -1) $v2['bonus'] = 0;
        $number++;//跟總人數一樣
        $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
        $sheet->setCellValue('A'.$i,$number);
        $sheet->setCellValueExplicit('B'.$i, $v2['empID'], PHPExcel_Cell_DataType::TYPE_STRING);//設定格式
        $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('C'.$i,$v2['empName']);
        $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('D'.$i,date("Y-m-d",strtotime($v2['hireDate'])));
        $sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('E'.$i,$v2['jobClass']);
        $sheet->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('F'.$i,$yearList[$v2['empID']]);
        $sheet->setCellValue('G'.$i,$v2['money']);
        $sheet->setCellValue('H'.$i,($v2['money'] - $v2['taxMoney']));
        $sheet->setCellValue('I'.$i,$v2['taxMoney']);

        //需要加總的
        $sub_sum   += $v2['money'];
        $sub_bonus += ($v2['money'] - $v2['taxMoney']);
        $subtax    += ($v2['taxMoney']);
        $people++;
        $i++;
      }

      $sum += $sub_sum;            //總計->原始金額
      $actual_bonus += $sub_bonus; //總計->實領金額
      $sum_tax += $subtax;         //總計->扣除的稅金

      $sheet->setCellValue('B'.$i,'小計');
      $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->setCellValue('C'.$i,$people.'人');
      $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $sheet->setCellValue('D'.$i,'');
      $sheet->setCellValue('E'.$i,'');
      $sheet->setCellValue('F'.$i,'');
      $sheet->setCellValue('G'.$i,$sub_sum);
      $sheet->setCellValue('H'.$i,$sub_bonus);
      $sheet->setCellValue('I'.$i,$subtax);

      $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
      $sheet->getStyle('A'.$i.':I'.$i)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
      if($final == count($list) && $final1==count($v)){
        $i++;
        //最後
        $sheet->setCellValue('B'.$i,'總計');
        $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('C'.$i,$number.'人');
        $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('D'.$i,'');
        $sheet->setCellValue('E'.$i,'');
        $sheet->setCellValue('F'.$i,'');
        $sheet->setCellValue('G'.$i,$sum);
        $sheet->setCellValue('H'.$i,$actual_bonus);
        $sheet->setCellValue('I'.$i,$sum_tax);
        $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);
      }
  
      $i = $i+4;
      
    }

  }

  $sheet->setCellValue('A'.$i,'經辦:');
  $sheet->setCellValue('D'.$i,'主管:');
 // $sheet->setCellValue('G'.$i,'執行長:');
  $sheet->getStyle('A'.$i.':I'.$i)->getFont()->setSize(12);


  //設定欄寬
  $width = array(10,15,15,15,10,10,10,15,20);
  foreach($header as $k=>$v){
    $sheet->getColumnDimension($v)->setWidth($width[$k]);  
  }
  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');

?>