<? 
	session_start();
	header("Content-Type:text/html; charset=utf-8");
	include("../../config.php");
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
.photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
.photoPreview .photoDesc { float:left; margin-left:5px; }
.photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
.photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
.photoPreview .photoItem .photoDel { margin:5px 0 0 5px; cursor:pointer; height:30px;}
.photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js" ></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script>
 <title><?=$pageTitle?> - 新增</title>
 <script type="text/javascript">
 $(function() {
 	$(".multiPhoto").change(function(){
		var that = $(this);
		var oTD = that.parents("td").eq(0);
		$(this.files).each(function(){
			var fReader = new FileReader();
			fReader.readAsDataURL(this);
			fReader.onloadend = function(event){
				var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);
				oItem.after(oItem.clone(true)).show();
				var img = oItem.find("img").get(0);
				img.height='150';
				img.style.padding='1';
				img.src=event.target.result;
				oItem.get(0).input = that;
				
				var oNewFile = that.clone(true);
				oNewFile.val(''); //這行
				that.hide().after(oNewFile);
			}
		});
	});
	
	$(".photoDel").click(function(){
		//console.log('do del');
		var oItem = $(this).parents(".photoItem").eq(0);
		if (oItem.get(0).input) oItem.get(0).input.remove();
		oItem.remove();
	});

  });
 
	function formvalid(thfm) {
		if($('[name="jobStatus"]').val() == 3 || $('[name="jobStatus"]').val() == 4) { //留停
			if($('[name="bdate"]').val() == '' || $('[name="edate"]').val() == '') {
				alert('起日與迄日不得為空');
				return false;
			}
		}
		var s1 = document.getElementById("bdate").value; 
	    var s2 = document.getElementById("edate").value; 
	    var sDate = new Date(s1.replace(/\-/g,'/')); 
	    var eDate = new Date(s2.replace(/\-/g,'/')); 
	    if(sDate > eDate) { 
	    	alert("结束日期不能小於開始日期"); 
	    	return false; 
	    } 
		return true;
	}
 </script>
</Head>

<body class="page" style="overflow:auto">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>

	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><?  switch ($newetA[$i]) { 
			case "empID":	echo $_SESSION['EE_EID'];	break;
			case "readonly": echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea id='$newfnA[$i]' name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea> <input type='button' value='編輯' onClick='HTMLEdit($newfnA[$i])'>"; break;
  			case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' id='$newfnA[$i]' value='$newfvA[$i]' class='optBtn'"; if ($newfvA[$i]=="true") echo " checked"; echo '>是'; break;
			case "radio":	echo "<input  type='radio' name='$newfnA[$i]' value='$newfvA[$i]'>是";	break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : 
				if($newfnA[$i]=="bdate" || $newfnA[$i]=="edate"){
					$Y=date("Y");
					$initY=$Y-40;	
					echo "<select id='".$newfnA[$i]."_sel_year' size='$newflA[$i]' class='input'>";
						for($j=$initY;$j<=$Y;$j++){
							if($j==$Y){ $sel='selected';}
							echo "<option value='$j' $sel>$j</option>"; 	
						}
					echo "</select>&nbsp;年"; 
					echo "<select id='".$newfnA[$i]."_sel_month' size='$newflA[$i]' class='input'>";
						for($j=1;$j<=12;$j++){	echo "<option value='".str_pad($j, 2,"0",STR_PAD_LEFT)."'>".str_pad($j, 2,"0",STR_PAD_LEFT)."</option>"; 	}
					echo "</select>&nbsp;月"; 
				}else{
					echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=>$v) echo "<option value='$k'>$v</option>"; echo "</select>"; 
				}
				break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
			case "multiPhoto":
				echo "<input type='file' name='".$newfnA[$i]."[]' size='$newflA[$i]' class='input multiPhoto'>"; 
				echo "<div class='photoPreview' >";
				echo "<div class='photoItem'><img /><img src='../images/del.png' class='photoDel'/><div class='clearfx'></div></div>";
				echo "</div>";
				break;	
			
  	} ?></td>
  </tr><? } ?>
  <tr>
  	<td colspan="2" align="center" class="rowSubmit">
    <input type='hidden' id='id' name='id' value="<?=$_SESSION['EE_EID']?>">
    <input type="submit" value="確定新增" class="btn">&nbsp;
  	<input type="reset" value="重新輸入" class="btn">&nbsp;
  	<input type="button" class="btn" onClick="history.back()" value="取消新增">
   	</td>
  </tr>
</table>
</form>
</body>
</html>
