<?
	session_start();
	include("../../config.php");
	include("init.php");
	$ID = $_REQUEST['ID'];
  	$sql = "select b.*, b.wkYear-1911 as year,e.empName,f.giveDate from $tableName b inner join emplyee e on b.empID=e.empID inner join bonus_festival f on b.fid=f.id where b.fid='$ID' and b.empID='$eid' and b.isGet='1'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
.photoPreview .photoItem { margin:5px 5px 5px 0; display:none; clear:both; }
.photoPreview .photoDesc { float:left; margin-left:5px; }
.photoPreview .photoItem input, .photoPreview .photoItem textarea { width:300px; margin-top:5px; }
.photoPreview .photoItem img { max-width:200px; max-height:200px; float:left; }
.photoPreview .photoItem .photoDel { margin:5px 0 0 5px; cursor:pointer; height:30px;}
.photoPreview .photoItem .classClose { font-size:13px; cursor:pointer; }
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script src="../../Scripts/form.js" type="text/javascript"></script> 
 <title>獎金明細</title>
 <script type="text/javascript">
	
 </script>
 <style type="text/css">
 	.bTable {
 		width:600px;
 	}
 	.bTable td:nth-child(odd) {
 		width:200px;
 	}
 </style>
</Head>

<body class="page">
<?php if($r['money'] > 0) { ?>
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
<div style="text-align:center;font-size:18px;margin-top:20px;">財團法人瑪利亞社會福利基金會<br><?php echo $r['year'];?>年[<?php echo $r['title'];?>獎金]明細<br><br></div>
<table align="center" class="bTable" width="100%" border="1" CellSpacing="0" CellPadding="4">
	<!--<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>
	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><?=$ID?></td>
	</tr>/--->
	<tr>
		<td>員工編號</td><td><?php echo $r['empID'];?></td>
	</tr>
	<tr>
		<td>員工姓名</td><td><?php echo $r['empName'];?></td>
	</tr>
	<tr>
		<td>獎金/代金</td><td><?php echo $r['money'];?>元</td>
	</tr>
	<tr>
		<td>發放年月</td><td><?php echo (date('Y', strtotime($r['giveDate'])) - 1911).'年'.(int)date('m', strtotime($r['giveDate'])).'月';?></td>
	</tr>
	<tr>
		<td>所得稅</td><td>0元</td>
	</tr>
	<tr>
		<td>實領金額</td><td><?php echo $r['money'];?>元</td>
	</tr>
	<tr>
		<td colspan="2" style="height: 50px;">備註:</td>
	</tr>


</table>
<div style="text-align:center;margin-top:20px;">
	<button type="button" id="print_button" class="button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
        <button type="button" id="print_close" class="button" onClick="history.back()">關閉</button>
</div>
	
</form>
<script type="text/javascript">
	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
    }
</script>
<?php } else { ?>
	<div style="text-align:center;font-size:18px;margin-top:50px;">不符合發放資格</div>
<?php } ?>
</body>
</html>
