<?php
	@session_start();
	if(!$_SESSION[empID]) header("Location: login.php");
	include 'inc_sys.php';
	include 'config.php';
	if(isset($_REQUEST['colapseM'])) $_SESSION['colapseM']=$_REQUEST['colapseM'];

	$icons = array('doc'=>'icon_doc.gif','docx'=>'icon_doc.gif','pdf'=>'icon_pdf.gif','ppt'=>'icon_ppt.gif','pptx'=>'icon_ppt.gif',
	  'txt'=>'icon_txt.gif','xls'=>'icon_xls.gif','xlsx'=>'icon_xls.gif','zip'=>'icon_zip.gif','rar'=>'icon_rar.gif');
	$self = $_SERVER['PHP_SELF'];	
	$wkmode = $_REQUEST[wkmode];
	$tbl = 'bullets';
 	$cid = $_REQUEST[cid]?$_REQUEST[cid]:0; 	//類別
	$hdpic = $cid?'btn_bull':'btn_news';
 	
	if(!$wkmode) {   //換頁處理 
		$r = db_fetch_array(db_query("select count(*) from $tbl where classid='$cid' and unValidDate>curdate()"));
		$recs   = $r[0];										//紀錄數
		$pageln = 10;												//每頁幾行
		$page   = $_REQUEST[page];					//第幾頁
		$pages  = ceil($recs / $pageln);  	//總頁數				
		$sql = "select * from $tbl where classid='$cid' and unValidDate>=curdate() order by R_Date Desc limit ".$page*$pageln.",$pageln";
		$rsC = db_query($sql);
	} else {		//單筆
		$sql = "select * from $tbl where ID=".$_REQUEST['ID'];	
		$rsC = db_query($sql);
		$rC = db_fetch_array($rsC);
	} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Keywords" content="網站關鍵字" />
<title><?=$gHTML_Title?></title>
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="css/cms.css" rel="stylesheet" type="text/css">
<style>
.menuBar { background-color:#7B0C13; font: 1.2em '微軟正黑體',Verdana; color:#FFFFE0 }
#mainmenu a { text-decoration:none }
#mainmenu a:link { color:#FFFFE0 }
#mainmenu a:visited { color:#FFFFE0 }
#mainmenu a:hover { color:#FFFFE0 }
#mainmenu a:active { color:#FFFFE0 }
.foot {	font:12px "微軟正黑體",Verdana;	color: #CCC; background-color: #7B0C13;	padding: 8px; }
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mainTable">
  <tr>
    <td class="sideBar">&nbsp;</td>
    <td width="1000" class="main" >
    	<!-- 頁面本體 ↓-->
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td class="headBar"><? include('inc_head.php')?></td></tr>
        <tr><td class="menu"><? $_SESSION['colapseM']?include('inc_menu2.php'):include('inc_menu.php') ?></td></tr> 
        <tr><td class="slideShow"><? include('inc_slideshow.php') ?></td></tr>
        <tr><td class="main">
				<? if(!$wkmode) { //清單模式 ?>
          <table id="news" width="94%" border="0" cellspacing="0" cellpadding="4" class="cmsTable" align="center">
          <tr><td colspan="2"><img src="css/default/<?=$hdpic?>.jpg" width="176" height="39"></td></tr>
          <? while($rsC && $rC=db_fetch_array($rsC)) { ?>
            <tr>
              <td width="10"><img src="images/point01.gif"/></td>
              <td class="cell"><span style="color:#993300"><?= date('Y/m/d',strtotime($rC['R_Date'])) ?></span> <a href="<?=$self?>?ID=<?=$rC[ID]?>&wkmode=1&cid=<?=$cid?>"><?=$rC[SimpleText]?></a></td>
            </tr>
          <? } ?>
          <tr><td colspan="2">
          <? //page row creation
          if($pages>1) {
            if ($page>0) echo "<a href='$self?cid=$cid&page=".($page-1)."'>前一頁</a>";
            $pg = floor($page / 10); 
            $i = 0;
            $p = $i+($pg*10);
            while ( ($p<$pages) && ($i<10) ) {
              echo " | ";
              if ($p==$page) echo ($p+1); else echo "<a href='$self?cid=$cid&page=".$p."'>".($p+1)."</a>";
              $i++;
              $p = $i+($pg*10);
            }
            echo " | ";
            if ($page < $pages-1) echo "<a href='$self?cid=$cid&page=".($page+1)."'>下一頁</a>";
          }	?>
          </td></tr>
          </table>        
        <? } else { //內容模式 ?>
        	<? include 'getEmplyeeInfo.php'; ?>
        	<table width="94%" border="0" cellspacing="1" cellpadding="0" class="cmsTable" align="center">
          	<tr><td colspan="2"><img src="css/default/<?=$hdpic?>.jpg" width="176" height="39"></td></tr>
            <tr><td width="2" bgcolor="#6A3500"></td><td style="padding-left:4px" bgcolor="#F8F8F8">
						<? echo $rC[SimpleText].'&nbsp;&nbsp;&nbsp;('.date('Y/m/d',strtotime($rC['R_Date'])).')'; if($rC[announcer]) echo "<span class='rightCell'>發佈者：".($emplyeeinfo[$rC[announcer]]?$emplyeeinfo[$rC[announcer]]:$rC[announcer])."</span>"; ?>
            </td></tr>
            <tr><td></td><td height="1" bgcolor="#C0C0C0"></td></tr>
            <? if($rC[filepath]) { ?><tr><td></td><td>附件：<? $ext = pathinfo($rC[filepath],PATHINFO_EXTENSION); $fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; echo "<img src='/images/$fn' align='absmiddle'/>";?><a href="/data/bullets/<?=$rC[filepath]?>" target="_new">下載</a></td></tr><? } ?>
            <tr><td></td><td><p><?=$rC[Content]?></p></td></tr>
            <tr><td colspan="2"><p>&gt;&gt; <a href='javascript:history.back()'>返回</a></p></td></tr>
          </table>
				<? } ?>
        </td></tr>
        <tr><td class="foot" align="center"><? include('inc_foot.php')?></td></tr>
      </table>
      <!-- 頁面本體 ↑-->
    </td>  
    <td class="sideBar">&nbsp;</td>
  </tr>
</table>
</body>
</html>
