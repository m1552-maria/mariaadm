<?php
	@session_start();
	date_default_timezone_set('Asia/Taipei');
	if(!$_SESSION[empID]) { header("Location: login.php"); exit; }
	include 'inc_sys.php';
	include 'config.php';
	include 'getEmplyeeInfo.php';
	if(isset($_REQUEST['colapseM'])) $_SESSION['colapseM']=$_REQUEST['colapseM'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Keywords" content="網站關鍵字" />
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<META HTTP-EQUIV="EXPIRES" CONTENT="0">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<title><?=$gHTML_Title?></title>
<link href="css/index.css" rel="stylesheet" type="text/css" />
<style>
.menuBar { background-color:#7B0C13; font: 1.2em '微軟正黑體',Verdana; color:#FFFFE0 }
#mainmenu a { text-decoration:none }
#mainmenu a:link { color:#FFFFE0 }
#mainmenu a:visited { color:#FFFFE0 }
#mainmenu a:hover { color:#FFFFE0 }
#mainmenu a:active { color:#FFFFE0 }

#main a { text-decoration:none }
#main a:link { color:#894822 }
#main a:visited { color:#894822 }
#main a:hover { color:#894822 }
#main a:active { color:#894822 }

.foot {	font:12px "微軟正黑體",Verdana;	color: #CCC; background-color: #7B0C13;	padding: 5px; }
</style>
<script src="cookies.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mainTable">
  <tr>
    <td class="sideBar">&nbsp;</td>
    <td width="1000" class="main" >
    	<!-- 頁面本體 ↓-->
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td class="headBar"><? include('inc_head.php')?></td></tr>
        <tr><td class="menu"><? $_SESSION['colapseM']?include('inc_menu2.php'):include('inc_menu.php') ?></td></tr> 
        <tr><td class="slideShow"><? include('inc_slideshow.php') ?></td></tr>
        <tr><td class="main"><? include('inc_main.php') ?></td></tr>
        <tr><td class="foot" align="center"><? include('inc_foot.php')?></td></tr>
      </table>
      <!-- 頁面本體 ↑-->
    </td>  
    <td class="sideBar">&nbsp;</td>
  </tr>
</table>
</body>
<script>
 var menubar = getCookie('menubar');
 var slidebar = getCookie('slidebar');
 $(function(){
	 if (slidebar=='none') switchPane(false);
 });
</script>
</html>
