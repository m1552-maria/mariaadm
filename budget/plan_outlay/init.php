<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/

	
	if(!isset($_REQUEST['fid'])) exit;
	$fID = $_REQUEST['fid'];
	$sID = $_REQUEST['sid'];
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";

	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";

	include "$root/maintain/inc_vars.php";
	
	$pageTitle = "資本支出";
	$tableName = "plan_outlay";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 1000;
	$NeedFilter = "sid = '".$sID."'";

	$db = new db();
	$sql = "select status from plan where id = '".$fID."'";
	$rs = $db->query($sql);
	$r=$db->fetch_array($rs);
	$status = (int)$r['status'];
	
	$canShow = 1;
	if($_SESSION['privilege'] <= 10 && $status >= 20) $canShow = 0;
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,false,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,false,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,false,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,false,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,false,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => (!$canShow) ? array("＋新增",true,true,'') : array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => (!$canShow) ? array("Ｘ刪除",true,true,'') : array("Ｘ刪除",false,true,'doDel') 
	);

	if(empty($sID)) {
		$opPage["searchBtn"] = array("◎搜尋",true,true,'SearchKeyword');
		$opPage["newBtn"] = array("＋新增",true,true,'');
		$opPage["editBtn"] = array("－修改",true,true,'');
		$opPage["delBtn"] = array("Ｘ刪除",true,true,'');
	}
	
	//:ListControl  ---------------------------------------------------------------

	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('name'),	//要搜尋的欄位	
		"filterOption"=>array('autochange'=>true,'goBtn'=>array("確定",false,true,'')),
		"ckBoxAll" =>true,
		"filters"=>array( //過濾器

		),
		/*"extraButton"=>array( //新增的button
			array("進階搜尋",false,true,'Button1Click'),
			array("Button2",false,true,'')
		)*/
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/	 
	

	$sql = "select * from plan_items where pid='".$fID."'";
	$rs = $db->query($sql);
	$sList = array();
	while($r=$db->fetch_array($rs)) $sList[$r['id']] = $r['title'];

	unset($classList['']);
	unset($vTypeList['']);
	$fieldsList = array(
		"id"=>array("編號","50px",true,array('Id',"gorec(%d)")),
		"sid"=>array("計畫名稱","",true,array('Select', $sList)),
		"title"=>array("八萬以上設備/工程","",true,array('Text')),
		"price"=>array("單價","",true,array('Text')),
		"quantity"=>array("數量","",true,array('Text')),
		"sourceMoney"=>array("補助金額","",true,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$fieldA = array(
		"fid[]"=>array("主檔編號","hidden",20,'','',''),
		"_sid[]"=>array("子計畫","readonly",20,'',$sList,''),
		"sid[]"=>array("子計畫","hidden",20,'',$sList,''),
		"title[]"=>array("八萬以上設備/工程","text","","","",array(true,'','','')),
		"price[]"=>array("單價","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"quantity[]"=>array("數量","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"unit[]"=>array("單位","text","","","",array(true,'','','')),
		"source[]"=>array("補捐助單位","text","","","",array(true,'','','')),
		"sourceMoney[]"=>array("補助金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"buyDate[]"=>array("購入日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"donate[]"=>array("捐款金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
	);
	/*新增時的預設值*/
	$fieldA_Data = array("fid[]"=>$fID, "sid[]"=>$sID, "_sid[]"=>$sID);
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"fid"=>array("主檔編號","hidden",20,'','',''),
		"sid"=>array("計畫名稱","readonly",20,'',$sList,''),
		"title"=>array("八萬以上設備/工程","text","","","",array(true,'','','')),
		"price"=>array("單價","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"quantity"=>array("數量","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"unit"=>array("單位","text","","","",array(true,'','','')),
		"source"=>array("補捐助單位","text","","","",array(true,'','','')),
		"sourceMoney"=>array("補助金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"buyDate"=>array("購入日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"donate"=>array("捐款金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
	);
?>