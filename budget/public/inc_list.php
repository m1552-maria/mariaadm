<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	
	if($act=='del') { //:: Delete record -----------------------------------------
		$aa = array();
		foreach($_POST['ID'] as $v) $aa[] = "'$v'";
		$lstss = join(',',$aa);
		$aa =array_keys($fieldsList);		
		$db = new db();	
		//檢查是否要順便刪除檔案
		if(isset($delFlag)) { 
			$sql = "select * from $tableName where $aa[0] in ($lstss)";
			$rs = $db->query($sql);
			while($r=$db->fetch_array($rs)) {
				$fns = $r[$delField];
				if($fns) {
					$ba = explode(',',$fns);
					foreach($ba as $fn) {
						$fn = $root.$ulpath.$fn;
						//echo $fn;
						if(file_exists($fn)) @unlink($fn);
					}
				}
			}
		}
		$sql = "delete from $tableName where $aa[0] in ($lstss)";
		//echo $sql; exit;
		$db->query($sql);
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------
?>	


<?php include($root."/budget/common/header.php");?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
<?php include($root."/budget/common/menu.php");?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- Modal -->
        <div class="modal fade hide" id="messageBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">訊息</h4>
              </div>
              <div class="modal-body">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn blue" data-dismiss="modal">確認</button>
              </div>
            </div>
          </div>
        </div>


            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	echo "<h1 align='center'>$pageTitle ";
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div><hr>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	/*echo $pinf->curPage;
	echo $sql;*/
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	

	//:ViewControl Object ------------------------------------------------------------------------
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			$op3 = array(
				"type"=>"edit",
				"form"=>array('form1','doedit.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定修改",false,''),
				"cancilBtn"=>array("取消修改",false,'')	);
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"type"=>"append",
				"form"=>array('form1','doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			//if(count($pdata) == 0) break;
			$obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	}
?>



                    </div>
                </div>
     
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->