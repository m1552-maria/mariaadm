<script>

var page = <?=$pinf->curPage?>;
var recno = <?=$pinf->curRecord+1?>;

function nextBtnClick(evt){
	page++;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function prevBtnClick(evt){
	page--;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function firstBtnClick(evt){
	page=1;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function lastBtnClick(evt){
	page=<?=$pinf->pages?>; 
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function goBtnClick(evt) {
	var n=$('#PageControl .numEdit').val();
	if(n<1) page=1;
	else if(n>=<?=$pinf->pages?>) page=<?=$pinf->pages?>;
	else page=n;
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function gorec(recno) { //go edit ViewControl
	ListControlForm.recno.value = recno;
	if(window.event.target.tagName=='A') ListControlForm.act.value = 'edit';
	ListControlForm.submit();
}
function viewrec(recno) { //go Only view ViewControl
	ListControlForm.recno.value = recno;
	if(window.event.target.tagName=='A') ListControlForm.act.value = 'view';
	ListControlForm.submit();
}

function SearchKeyword() {
	rzlt = prompt("請輸入關鍵字",'');
	if (rzlt!=null) {
		ListControlForm.keyword.value = rzlt;
		ListControlForm.submit();		
	}	 
}

function doEdit() {
	ListControlForm.recno.value = recno;	//:by recno
	ListControlForm.act.value = 'edit';
	ListControlForm.submit();	
}
function doAppend() {
	page=<?=$pinf->pages?>;
	ListControlForm.page.value = page;
	ListControlForm.act.value = 'new';
	ListControlForm.submit();
}
function doDel() {
	delMsgBox();
	if(delCheckItem()) {
		ListControlForm.act.value = 'del';
		ListControlForm.page.value = page;
		ListControlForm.submit();
	}
}

function form1Valid(evt) {

	var pAry = [];
	if(typeof ListControlForm !== "undefined") {
		<? if($act=='edit') { ?>
		ListControlForm.recno.value = recno;
		<? } else { ?>
		ListControlForm.page.value = page;
		<? } ?>
		$(ListControlForm).find('input[type="hidden"]').each(function(){
			if(this.value) pAry.push(this.name+'='+this.value);
		});
	}

	<? if(!$listObj) foreach($_REQUEST as $k=>$v) {
			if($k=='act') continue;
			if($v!='') echo "if(pAry.indexOf('$k=$v') == -1) pAry.push('$k=$v');"; 
		 }
	?>
	console.log(pAry);
	if(pAry.length>0) {
		var aForm = evt.currentTarget;
		if(!aForm.ListFmParams) {
			var inp = document.createElement('input');
			inp.type = 'hidden';
			inp.name = 'ListFmParams';
			aForm.appendChild(inp);
		}
		aForm.ListFmParams.value = pAry.join('&');
	}
	return true;
}
</script>