<?php

	class dataSearch {

		public function getDataList($tableName, $data = array(), $keyWordList = array()) {
			$result = array();
			$defaultDataField = "*";
			if(isset($data['defaultDataField'])) $defaultDataField = $data['defaultDataField'];
			$sql = "select ".$defaultDataField." from ".$tableName." where 1 ";
			if(isset($data['NeedFilter'])) $sql .= " and ".$data['NeedFilter'];

			/*echo "<pre>";
			print_r($data);
			exit;*/

			$sqlFieldList = array();
			//一般搜尋or 進階搜尋
			if(isset($data['searchList'])) {
				if(count($data['searchList']) == 1 && isset($data['searchList']['keyWordField']) && $data['searchList']['keyWordField'] != '') {
					$sql .= " and (";
					foreach ($keyWordList as $key => $value) {
						$vList = explode('.', $value);
						if(count($vList) == 2) $allKeyList[] = $vList[0].".`".$vList[1]. "` like ? ";
						else $allKeyList[] = "`".end($vList). "` like ? ";
						$sqlFieldList[] = "%".$data['searchList']['keyWordField']."%";
					}

					$sql .= implode(' or ', $allKeyList)." )";
				} else {
					foreach ($data['searchList'] as $key => $value) {
						if($key == 'keyWordField') continue;
						if($value == '') continue;

						if(preg_match("/range/i", $key)) { //判別時間範圍
							$v = str_replace('~', '', $value);
							if($v != '') {
								$fieldList = explode('-', $key);
								$dateValList = explode('~', $value);
							
								$kList = explode('.', $fieldList[1]);
								if(count($kList) == 2) $sql .= " and (".$kList[0].".`" . $kList[1] . "` > ?  and ".$kList[0].".`" . $kList[1] . "` <= ?) ";
								else $allKeyList[] = $sql .= " and (`" . end($kList) . "` > ? and `" . end($kList) . "` <= ?) ";
								$sqlFieldList[] = $dateValList[0];
								$sqlFieldList[] = $dateValList[1];
							}
							
						} else {
							$kList = explode('.', $key);
							if(count($kList) == 2) $sql .= " and ".$kList[0].".`" . $kList[1] . "` like ? ";
							else $allKeyList[] = $sql .= " and `" . end($kList) . "` like ? ";
							//$sql .= " and `" . $key . "` like ? ";
							$sqlFieldList[] = "%".$value."%";
						}
					}
				}
			}

			//過濾搜尋 (count($valList) 大於1 代表 有加入SQL語法 exit "> 0") 
			if(isset($data['filterList'])) {
				foreach ($data['filterList'] as $key => $value) {
					if($value != '') {
						$valList = array();
						if($value != 'is null' && $value != 'is not null') $valList = explode(' ', $value);
						else $valList[] = $value;
						
						
						$kList = explode('.', $key);
						if(count($valList) == 1) {

							if(count($kList) == 2) {
								if($valList[0] != 'is null' && $valList[0] != 'is not null') {
									$sql .= " and ".$kList[0].".`" . $kList[1] . "` = ? ";
									$sqlFieldList[] = $valList[0];
								} else {
									$sql .= " and ".$kList[0].".`" . $kList[1] . "` ".$valList[0]." ";
								}	

						
							} else {
								if($valList[0] != 'is null' && $valList[0] != 'is not null') {
									$sql .= " and `" . end($kList) . "` = ? ";
									$sqlFieldList[] = $valList[0];
								} else {
									$sql .= " and `" . end($kList) . "` ".$valList[0]." ";
								}
							}
						} elseif(count($valList) > 1) {
							if(count($kList) == 2) $sql .= " and ".$kList[0].".`" . $kList[1] . "` ".$valList[0]." ? ";
							else $sql .= " and `" . end($kList) . "` ".$valList[0]." ? ";
							$sqlFieldList[] = $valList[1];
						}
					}
				}

			}

			if(isset($data['groupBy'])) {
				$gList = explode('.', $data['groupBy']);
				if(count($gList) == 2) $sql .= " group by ".$gList[0].".`" . $gList[1]."`";
				else $sql .= " group by `" . end($gList)."`";
			} 

			if(isset($data['order'])) {
				$oList = explode('.', $data['order']);
				if(count($oList) == 2) $sql .= " order by ".$oList[0].".`" . $oList[1]."`";
				else $sql .= " order by `" . end($oList)."`";
			}

			if(isset($data['ascOrDesc']) && isset($data['order']) && $data['ascOrDesc'] == 'desc') $sql .= " desc";
			elseif(isset($data['ascOrDesc']) && isset($data['order']) && $data['ascOrDesc'] == 'asc') $sql .= " asc";
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " limit " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			/*print_r($sql.'<br>');
			echo "<pre>";
			print_r($sqlFieldList);
			exit;*/
			

			if(count($sqlFieldList) > 0) $rs = db_query($sql, $sqlFieldList);
			else $rs = db_query($sql);
	
			while ($r=db_fetch_array($rs)) {
				$result[] = $r;
			}

			/*echo "<pre>";
			print_r($result);
			exit;*/

			return $result;
		}

		public function getDataTotal($tableName, $data = array(), $keyWordList = array()) {
			$sql = "select count(*) from ".$tableName." where 1 ";
			if(isset($data['groupBy'])) $sql = "select * from ".$tableName." where 1 ";
			if(isset($data['NeedFilter'])) $sql .= " and ".$data['NeedFilter'];

			$sqlFieldList = array();
			//一般搜尋or 進階搜尋
			if(isset($data['searchList'])) {
				if(count($data['searchList']) == 1 && isset($data['searchList']['keyWordField']) && $data['searchList']['keyWordField'] != '') {
					$sql .= " and (";
					foreach ($keyWordList as $key => $value) {
						$vList = explode('.', $value);
						if(count($vList) == 2) $allKeyList[] = $vList[0].".`".$vList[1]. "` like ? ";
						else $allKeyList[] = "`".end($vList). "` like ? ";
						$sqlFieldList[] = "%".$data['searchList']['keyWordField']."%";
					}

					$sql .= implode(' or ', $allKeyList)." )";
				} else {
					foreach ($data['searchList'] as $key => $value) {
						if($key == 'keyWordField') continue;
						if($value == '') continue;

						if(preg_match("/range/i", $key)) { //判別時間範圍
							$v = str_replace('~', '', $value);
							if($v != '') {
								$fieldList = explode('-', $key);
								$dateValList = explode('~', $value);
							
								$kList = explode('.', $fieldList[1]);
								if(count($kList) == 2) $sql .= " and (".$kList[0].".`" . $kList[1] . "` > ?  and ".$kList[0].".`" . $kList[1] . "` <= ?) ";
								else $allKeyList[] = $sql .= " and (`" . end($kList) . "` > ? and `" . end($kList) . "` <= ?) ";
								$sqlFieldList[] = $dateValList[0];
								$sqlFieldList[] = $dateValList[1];
							}
							
						} else {
							$kList = explode('.', $key);
							if(count($kList) == 2) $sql .= " and ".$kList[0].".`" . $kList[1] . "` like ? ";
							else $allKeyList[] = $sql .= " and `" . end($kList) . "` like ? ";
							//$sql .= " and `" . $key . "` like ? ";
							$sqlFieldList[] = "%".$value."%";
						}
					}
				}
			}

			//過濾搜尋 (count($valList) 大於1 代表 有加入SQL語法 exit "> 0") 
			if(isset($data['filterList'])) {
				foreach ($data['filterList'] as $key => $value) {
					if($value != '') {
						$valList = array();
						if($value != 'is null' && $value != 'is not null') $valList = explode(' ', $value);
						else $valList[] = $value;
						$kList = explode('.', $key);
						if(count($valList) == 1) {
							if(count($kList) == 2) {
								if($valList[0] != 'is null' && $valList[0] != 'is not null') {
									$sql .= " and ".$kList[0].".`" . $kList[1] . "` = ? ";
									$sqlFieldList[] = $valList[0];
								} else {
									$sql .= " and ".$kList[0].".`" . $kList[1] . "` ".$valList[0]." ";
								}	

							} else {
								if($valList[0] != 'is null' && $valList[0] != 'is not null') {
									$sql .= " and `" . end($kList) . "` = ? ";
									$sqlFieldList[] = $valList[0];
								} else {
									$sql .= " and `" . end($kList) . "` ".$valList[0]." ";
								}
							}
						} elseif(count($valList) > 1) {
							if(count($kList) == 2) $sql .= " and ".$kList[0].".`" . $kList[1] . "` ".$valList[0]." ? ";
							else $sql .= " and `" . end($kList) . "` ".$valList[0]." ? ";
							$sqlFieldList[] = $valList[1];
						}
					}
				}
			}

			if(isset($data['groupBy'])) {
				$gList = explode('.', $data['groupBy']);
				if(count($gList) == 2) $sql .= " group by ".$gList[0].".`" . $gList[1]."`";
				else $sql .= " group by `" . end($gList)."`";
			} 

			if(count($sqlFieldList) > 0) $rs = db_query($sql, $sqlFieldList);
			else $rs = db_query($sql);
			$con = 0;
			if(isset($data['groupBy'])) {
				while ($r=db_fetch_array($rs)) $con++;
			} else {
				$r=db_fetch_array($rs);
				return $con = $r[0];
			}

			/*print_r($sql);
			exit;*/

			return $con;

		}

	}

?>