<?php
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	include '../public/inc_list.php';
	include '../public/inc_listjs.php';
?>
    <!-- END CONTAINER -->
<script>
$(function(){
	/*特殊處理 要防止填入相同的ID 所以先判斷*/
	$(document).on('input','[name="id"]', function(){
		var idThis = this;
		$.ajax({
            url:'checkId.php',
            type:'post',
            data:{id:idThis.value},
            dataType:'json',
           // async: false,
            success:function(d){
            	if(d == 0) idThis.setCustomValidity('');
            	else idThis.setCustomValidity('此編號已經存在');
            }
        })  
	})
})
</script>

<?php include($root."/budget/common/footer.php");?>
