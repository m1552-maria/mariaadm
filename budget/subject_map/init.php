<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "科目設定";
	$tableName = "subject_map";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 30;
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);

	$class = array('經常門'=>'經常門','資本門'=>'資本門');
	$vType = array('+'=>'收入','-'=>'支出');
	
	//:ListControl  ---------------------------------------------------------------
	if($_REQUEST['PID']) $opAR=array($_REQUEST['PID']=>$_REQUEST['cname']);
	else $opAR=array('0'=>'全部');
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','title'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array('autochange'=>true,'goBtn'=>array("確定",false,true,'')),
		"filters"=>array('PID'=>$opAR)
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	
	$fieldsList = array(
		"id"=>array("編號","200px",true,array('Id',"gorec(%d)")),
		"title"=>array("預表科目","",true,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$fieldA = array(
		"id"=>array("<span class='need'>*</span>編號","text","","","",array(true,'','','')),
		"title"=>array("<span class='need'>*</span>預表科目","text","","","",array(true,'','','')),
		"PID"=>array("上層編號","readonly","","","")
	);
	/*新增時的預設值*/
	$PID=$_REQUEST['PID']?$_REQUEST['PID']:0;
	$fieldA_Data = array("PID"=>$PID);
	
	$fieldE = array(
		"id"=>array("編號","id",20),
		"title"=>array("<span class='need'>*</span>預表科目","text","","","",array(true,'','','')),
		"levels"=>array("層級","levels",20),
		"pid"=>array("上層編號","pid",20)
	);
?>