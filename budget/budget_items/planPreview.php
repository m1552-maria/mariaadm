<?php
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	if(empty($_REQUEST['levelID'])) {
		header("Location: /budget/error.html");
		exit;
	}
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");

	include "$root/system/db.php";
	include "$root/system/ViewControl.php";

	$db = new db();	
	$sql = "select id, title from plan_stringmap where isReady = 1 and class= 'mtype'";
	$rs = $db->query($sql);
	$mtypeList = array();
	while($r=$db->fetch_array($rs)) $mtypeList[$r['id']] = $r['title'];

	$levelID = explode('_', $_REQUEST['levelID']);
	if(count($levelID) == 1) {
		include($_SERVER['DOCUMENT_ROOT']."/config.php");
		include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
		$tableName = 'plan';
		$id = $levelID[0];
		$statusList = array(''=>'--申請狀態--', 0=>'未審查', 10=>'審查中', 20=>'審查通過', 21=>'審查不過');
		$fieldE = array(
			"id"=>array("編號","id",20),
			"depID"=>array("部門","select",1,"",$departmentinfo),
			"year"=>array("年度","text",1,"",""),
			"title"=>array("<span class='need'>*</span>名稱","text","60","",""),
			"mtype"=>array("服務類型","select",1,"",$mtypeList),
			"bDate"=>array("開始日期","date","","",""),
			"eDate"=>array("結束日期","date","","",""),
			"srvpeople"=>array("服務量","text","100","",""),
			"talents"=>array("人力需求量","text","100","",""),
			"goal"=>array("目標","textbox","100","5",""),
			"content"=>array("內容","textbox","100","15",""),
			"creator"=>array("建單人","select","","",$emplyeeinfo),
			"cLeader"=>array("部門主管","select","","",$emplyeeinfo),
			"status"=>array("狀態","select",1,"",$statusList)
		);
	} else {
		$tableName = 'plan_items';
		$id = $levelID[1];
		$fieldE = array(
			"id"=>array("編號","id",20),
			"pid"=>array("主檔編號","readonly",20,'','',''),
			"title"=>array("<span class='need'>*</span>名稱","text","","",""),
			"mtype"=>array("服務類型","select",1,"",$mtypeList),
			"bDate"=>array("開始日期","date","","",""),
			"eDate"=>array("結束日期","date","","",""),
			"srvpeople"=>array("服務量","text","","",""),
			"talents"=>array("人力需求量","text","","",""),
			"goal"=>array("目標","textbox","100","5",""),
			"srveffect"=>array("預期效益","textbox","100","5",""),
			"content"=>array("內容","textbox","100","10","")
		);
	}
	
	$db = new db();
	$sql = "select * from ".$tableName." where id = '".$id."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 


<script src="/system/utilities/system.js"></script>
<script src="/system/ViewControl.js"></script>
</head>
<body>
<?php
	$op3 = array(
		"type"=>"view",
		"submitBtn"=>array("確定變更",true,''),
		"resetBtn"=>array("重新輸入",true,''),
		"cancilBtn"=>array("關閉",true,''));
	$ctx = new BaseViewControl($data, $fieldE, $op3);	
?>
</body>
</html>