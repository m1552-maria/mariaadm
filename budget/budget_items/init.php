<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL & ~E_NOTICE);;
	if(!isset($_REQUEST['pid'])) exit;
	$pID = $_REQUEST['pid'];
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	include "$root/system/MultipleListControl.php";
	
	$pageTitle = "選取計劃";
	$tableName = "plan as p inner join plan_items as pi on p.id=pi.pid inner join budget_items as bi on pi.id = bi.pItemID";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 5;
	$NeedFilter = "bi.pid = '".$pID."'";
	/*echo "<pre>";
	print_r($_SESSION);
	exit;*/
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋選取計劃",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$nowYear = date('Y')-1911;
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'p.id',	//主排序的欄位
		"sortMark"=>'DESC',			//升降序 (ASC | DES)
		"searchField"=>array('p.title'),	//要搜尋的欄位	
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,'')
		),
		"filters"=>array( //過濾器
		),
		"extraButton"=>array( //新增的button
			array("選取部門",false,true,'depTreeShow')
		),
		"isCooked" => false,
		"cookName" => ''
	);


	$db = new db();	
	$sqlYear = "select year from plan group by year order by year";
	$yearList = array(''=>'--年度選取--');
	$rs = $db->query($sqlYear);
	while($r=$db->fetch_array($rs)) $yearList[$r['year']] = $r['year'];
	$yearList[$nowYear] = $nowYear; //讓今年度一定出現在下拉選單
	$opList['filters']['year'] = $yearList;

	$depIDList = array_merge(array(''=>'--部門--'), $departmentinfo);
	if(!empty($_REQUEST['depID'])) $opAR = array($_REQUEST['depID']=>$_REQUEST['cname']); 
	else $opAR = array(''=>'--全部--');
	$opList['filters']['depID'] = $opAR;


	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	
	$minorFields = array(
		"id"=>array("項目編號","120px",true,array('Id',"gorec(this, %d)")),
		"percent"=>array("計劃使用百分比","",false,array('Define', 'percentMake')),
		"title"=>array("標題","",true,array('Text')),
		"bDate"=>array("開始日期","",true,array('Date')),
		"eDate" =>array("結束日期","",true,array('Date')),
		"srvpeople"=>array("服務量","",false,array('Text')),
		"talents"=>array("人力需求量","",false,array('Text'))
	);

	unset($depIDList['']);
	$fieldsList = array(
		"id"=>array("主檔編號","120px",true,array('Id',"gorec(this, %d)"), $minorFields),
		"year"=>array("年度","",true,array('Text')),
		"title"=>array("標題","",true,array('Text')),
		"bDate"=>array("開始日期","",true,array('Date')),
		"eDate" =>array("結束日期","",true,array('Date')),
		"srvpeople"=>array("服務量","",false,array('Text')),
		"creator"=>array("建單人","",true,array('Select', $emplyeeinfo)),
		"depID"=>array("部門","",false,array('Select', $depIDList))
	);
	
?>