<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init_append.php';	
	//include "../system/utilities/miclib.php";
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/
	/*加入百分比的input*/
	function percentMake($val, $obj) {
		$html = '<div>可用百分比:'.$val.'%</div><input style="width:50px;" type="number" name="percent[]" value="'.$val.'" required="" max="'.$val.'" min="1" class="input" onChange="changePercent(this)">';
		return $html;
	}

	//:Defined PHP Function -------------------------------------------------------------
?>	

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
    <link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/system/MultipleListControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 
<script src="/budget/media/js/jquery.cookie.js"></script>
<script src="/budget/media/script/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script src="/system/MultipleListControl.js"></script>


</head>
<body>
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	echo "<h1 align='center'>$pageTitle ";
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div><hr>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
		/*echo "<pre>";
	print_r($_REQUEST);*/
	/*echo $pinf->curPage;
	echo $sql;*/
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);

	foreach ($pdata as $key => $value) {
		$idList = array();
		$sql = "select pi.id as pItemID from plan_items as pi inner join budget_items as bi on pi.id = bi.pItemID where bi.pid = '".$pID."' and pi.pid='".$value['id']."'";
		$rs = $db->query($sql);
		while($r = $db->fetch_array($rs)) $idList[] = "'".$r['pItemID']."'";

		$sql = "select (100-IFNULL(pp.total, 0)) as percent, pi.* from plan_items as pi inner join (select pi.id, (select sum(bi.percent) from budget_items as bi where bi.pItemID=pi.id) as total from plan_items as pi where pi.pid = '".$value['id']."') as pp on pi.id = pp.id where (pp.total is null or pp.total <> '100') ";

		if(count($idList) > 0) $sql.= " and pi.id not in (".implode(',', $idList).")";
		$sql.= " order by pi.id asc";
		//$sql = "select * from plan_items where pid = '".$value['id']."' order by id asc";
		/*echo $sql;
		exit;*/
		$rs = $db->query($sql);
		if($db->num_rows($rs) > 0) $pdata[$key]['minorData'] = $db->fetch_all($rs);
	}
	/*echo "<pre>";
	print_r($pdata);
	exit;*/

	//$cookName = 'addPlanItem';
	$obj = new MultipleListControl($pdata, $fieldsList, $opList, $pinf);	

	//:TreeControl -----------------------------------------------------------------------
	$option = array('dbSource'=>DB_SOURCE2);
	$db2 = new db($option);
	$op = array(
		'ID'		=> 'depID',
		'Title'		=> 'depTitle',
		'TableName' => 'department',
		'RootClass' => 0,
		'RootTitle' => '瑪利亞基金會',
		'DB' 		=> $db2,
		'Modal' 	=> true,
		'URLFmt'	=> "javascript:detIDfilter(\"%s\",\"%s\")"
	 );
	$tc=new BaseTreeControl($op);
?>


<script>
var page = <?=$pinf->curPage?>;
//Override Object Event
$(function () {
	percentInit();
	//在次新增data-level-val點擊時的動作
	$('[data-level-val]').click(function(){
		if($(this).prop('checked')) {
			var obj = $(this).parent('td').next('td').children('[name="percent[]"]');
			if(obj.length == 1) changePercent(obj);

			if($(this).attr('data-minor-all') == 1) {
				$('[data-level-val]').each(function(){
					var obj = $(this).parent('td').next('td').children('[name="percent[]"]');
					if(obj.length == 1) changePercent(obj);
				});
			} else if(obj.length == 1) {
				var obj = $(this).parent('td').next('td').children('[name="percent[]"]');
				changePercent(obj);
			}
		}
	});
}); 

function nextBtnClick(evt){
	page++;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function prevBtnClick(evt){
	page--;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function firstBtnClick(evt){
	page=1;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function lastBtnClick(evt){
	page=<?=$pinf->pages?>; 
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function goBtnClick(evt) {
	var n=$('#PageControl .numEdit').val();
	if(n<1) page=1;
	else if(n>=<?=$pinf->pages?>) page=<?=$pinf->pages?>;
	else page=n;
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function gorec(obj, recno) {
	var levelID = $(obj).prev('[data-level-val]').attr('data-level-val');
	window.open('planPreview.php?levelID='+levelID,'','width=800,height=800');
}

function SearchKeyword() {
	rzlt = prompt("請輸入關鍵字",'');
	if (rzlt!=null) {
		ListControlForm.keyword.value = rzlt;
		ListControlForm.submit();		
	}	 
}

function depTreeShow(evt) {
	showTreeControl();
}

function detIDfilter(key,cname) {
	while(ListControlForm.depID.options[0]) $(ListControlForm.depID.options[0]).remove();
	var op = document.createElement('option');
	op.value = key;
	op.text = cname;
	ListControlForm.depID.add(op);
	ListControlForm.cname.value = cname;
	ListControlForm.submit();		
}

function percentInit(){
	var cookName = 'cookPercent';
	var cookItem = {};
	if($.cookie(cookName) != null && $.cookie(cookName) != '') {
		cookItem = JSON.parse($.cookie(cookName));
		for(var i in cookItem) {
			var obj = $('[data-level-val="'+i+'"]').parent('td').next('td').children('[name="percent[]"]');
			var oVal = parseInt(cookItem[i]);
			$(obj).val(oVal);
			if(parseInt(obj.attr('max')) < oVal) {
				oVal = obj.attr('max');
				$(obj).val(oVal);
				changePercent(obj);
			} else if(parseInt(obj.attr('min')) > oVal) {
				oVal = obj.attr('min');
				$(obj).val(oVal);
				changePercent(obj);
			}
		}
	}
}

//更改百分比
function changePercent(obj) {
	var objVal = parseInt($(obj).val());
	var cookName = 'cookPercent';
	var key = $(obj).parent('td').prev('td').children('[data-level-val]').attr('data-level-val');
	var cookItem = {};
	if($.cookie(cookName) != null && $.cookie(cookName) != '') {
		cookItem = JSON.parse($.cookie(cookName));
	}

	var max = parseInt($(obj).attr('max'));
	if(max < objVal) {
		alert('輸入值不得大於: '+max);
		$(obj).val(max);
	}
	var min = parseInt($(obj).attr('min'));
	if(min > objVal) {
		alert('輸入值不得小於: '+min);
		$(obj).val(min);
	}

	cookItem[key] = $(obj).val();
	$.cookie(cookName, JSON.stringify(cookItem));
	//console.log($.cookie(cookName));
}

//新增預算計畫
function addPlanItem(evt) {
	var cookName = $('[data-cook-name]').val();
	if($.cookie(cookName) == null || $.cookie(cookName) == '') {
		alert('請勾選計畫');
		return false;
	}
	if(!confirm('確定新增勾選的計畫?')) return false;
	
	var cookItem = $.cookie(cookName).split(',');
	var percentObj = JSON.parse($.cookie('cookPercent'));	
	var jsonObj = {};

	for(var i in cookItem) {
		var item = cookItem[i].split('_');
		if(item.length == 2) {
			jsonObj[item[1]] = percentObj[cookItem[i]];
		}
 	}

	$.cookie(cookName,null);
	$.cookie('cookPercent',null);
	window.opener.addPlanItem(jsonObj);
    window.close();
}
</script>
</body>
</html>

