<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	//include "../system/utilities/miclib.php";

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	if($act=='new' || $act=='edit') $_SESSION['plan_tab'] = 0;
	$tabidx = (isset($_SESSION['plan_tab'])) ? $_SESSION['plan_tab'] : 0;
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/
	/*加入百分比的input*/
	function percentMake($val, $obj) {
		//傳入val值ex: 100_0 =>前者為此項所占百分比 後者為剩餘可用百分比
		$vList= explode('_', $val);
		$max = (int)$vList[0] + (int)$vList[1];
		$html = '<div>可用百分比:'.$max.'%</div><input style="width:50px;" type="number" name="percent[]" value="'.$vList[0].'" class="input" max="'.$max.'" min="1" onChange="changePercent(this)">';
		return $html;
	}

	//:Defined PHP Function -------------------------------------------------------------
	
?>	
<?php 
    header("Cache-control: private");

    $_SESSION['maintain']['name'] = (isset($_SESSION['Name'])) ? $_SESSION['Name'] : $_SESSION['empName'];

    $message = '';
    if(isset($_SESSION['maintain']['MESSAGE']) && gettype($_SESSION['maintain']['MESSAGE']) == 'string') {
        $message = $_SESSION['maintain']['MESSAGE'];
        unset($_SESSION['maintain']['MESSAGE']);
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
    <link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/system/MultipleListControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 
<script src="/budget/media/js/jquery.cookie.js"></script>
<script src="/budget/media/script/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script src="/system/MultipleListControl.js"></script>


</head>
<body>
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();

	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	/*echo "<pre>";
	print_r($_REQUEST);
	exit;*/

	$db = new db();
	$sql = sprintf('select 1 from %s %s group by p.id',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo "<h3> 搜尋:".$opList['keyword'];
	echo "</h3>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select p.* from %s %s group by p.id order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);

	/*echo '<br>'.$pinf->curPage;
	echo $sql;*/
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	
	//$cookName = 'editPlanItem';
	foreach ($pdata as $key => $value) {
		$sql = "select pi.*, concat(bi.percent,'_',(select (100-sum(bbi.percent)) from budget_items as bbi where bbi.pItemID = pi.id)) as percent from $tableName where $NeedFilter and pi.pid = '".$value['id']."' order by pi.id asc";
		$rs = $db->query($sql);
		if($db->num_rows($rs) > 0) $pdata[$key]['minorData'] = $db->fetch_all($rs);
	}
	$obj = new MultipleListControl($pdata, $fieldsList, $opList, $pinf);	

	//:TreeControl -----------------------------------------------------------------------
	$option = array('dbSource'=>DB_SOURCE2);
	$db2 = new db($option);
	$op = array(
		'ID'		=> 'depID',
		'Title'		=> 'depTitle',
		'TableName' => 'department',
		'RootClass' => 0,
		'RootTitle' => '瑪利亞基金會',
		'DB' 		=> $db2,
		'Modal' 	=> true,
		'URLFmt'	=> "javascript:detIDfilter(\"%s\",\"%s\")"
	 );
	$tc=new BaseTreeControl($op);
?>
<script>
var page = <?=$pinf->curPage?>;
var recno = <?=$pinf->curRecord+1?>;

function setnail(n) {
	$('.tabimg').attr('src','/images/unnail.png');
	$('#tabg'+n).attr('src','/images/nail.png');
	$.get('setnail.php',{'idx':n});
}

function nextBtnClick(evt){
	page++;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function prevBtnClick(evt){
	page--;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function firstBtnClick(evt){
	page=1;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function lastBtnClick(evt){
	page=<?=$pinf->pages?>; 
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function goBtnClick(evt) {
	var n=$('#PageControl .numEdit').val();
	if(n<1) page=1;
	else if(n>=<?=$pinf->pages?>) page=<?=$pinf->pages?>;
	else page=n;
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function gorec(obj, recno) {
	/*ListControlForm.recno.value = recno;
	ListControlForm.submit();*/
	var levelID = $(obj).prev('[data-level-val]').attr('data-level-val');
	window.open('planPreview.php?levelID='+levelID,'','width=800,height=800');

}


function SearchKeyword() {
	rzlt = prompt("請輸入關鍵字",'');
	if (rzlt!=null) {
		ListControlForm.keyword.value = rzlt;
		ListControlForm.submit();		
	}	 
}

function doEdit() {
	if($('[data-level-val]:checked').length == 0) {
		alert('請勾選要修改的項目');
		return false;
	}
	if(!confirm('確定修改勾選的項目?')) return false;
	var jsonObj = {};
	$('[data-level-val]:checked').each(function(){
		var item = $(this).attr('data-level-val').split('_');
		if(item.length == 2) jsonObj[item[1]] = $(this).parent('td').next('td').children('[name="percent[]"]').val();
	});

	ajaxAction('edit', jsonObj);
}
var newOpen = false;
function doAppend() {
	if(newOpen) {
		newOpen.close();
		newOpen = false;
	}
	newOpen = window.open('append.php?pid=<?php echo $pID;?>','','width=1200,height=800');
}
function doDel() {
	delMsgBox();
	if(delCheckItem()) {
		var jsonObj = {};
		/*var cookItem = $.cookie($('[data-cook-name]').val()).split(',');
		for(var i in cookItem) {
			var item = cookItem[i].split('_');
			if(item.length == 2) jsonObj[item[1]] = item[1];
		}*/

		$('[data-level-val]:checked').each(function(){
			var item = $(this).attr('data-level-val').split('_');
			if(item.length == 2) jsonObj[item[1]] = item[1];
		});

		ajaxAction('del', jsonObj);
	}
}

function form1Valid(evt) {
	var pAry = [];
	if(typeof ListControlForm !== "undefined") {
		<? if($act=='edit') { ?>
		ListControlForm.recno.value = recno;
		<? } else { ?>
		ListControlForm.page.value = page;
		<? } ?>
		$(ListControlForm).find('input[type="hidden"]').each(function(){
			if(this.value) pAry.push(this.name+'='+this.value);
		});
	}
	<? if(!$listObj) foreach($_REQUEST as $k=>$v) {
			if($k=='act') continue;
			if($v!='') echo "if(pAry.indexOf('$k=$v') == -1) pAry.push('$k=$v');"; 
		 }
	?>
	if(pAry.length>0) {
		var aForm = evt.currentTarget;
		if(!aForm.ListFmParams) {
			var inp = document.createElement('input');
			inp.type = 'hidden';
			inp.name = 'ListFmParams';
			aForm.appendChild(inp);
		}
		aForm.ListFmParams.value = pAry.join('&');
	}
	return true;
}

function depTreeShow(evt) {
	showTreeControl();
}

function detIDfilter(key,cname) {
	//$('[name="ListControlForm"] [name="p.depID"]').html('');
	while(ListControlForm.depID.options[0]) $(ListControlForm.depID.options[0]).remove();
	var op = document.createElement('option');
	op.value = key;
	op.text = cname;

	//$('[name="ListControlForm"] [name="p.depID"]').append(op);

	ListControlForm.depID.add(op);
	ListControlForm.cname.value = cname;
	ListControlForm.submit();	
}

//更改百分比
function changePercent(obj) {
	var objVal = parseInt($(obj).val());
	var key = $(obj).parent('td').prev('td').children('[data-level-val]').attr('data-level-val');
	var max = parseInt($(obj).attr('max'));
	if(max < objVal) {
		alert('輸入值不得大於: '+max);
		$(obj).val(max);
		return false;
	}
	var min = parseInt($(obj).attr('min'));
	if(min > objVal) {
		alert('輸入值不得小於: '+min);
		$(obj).val(min);
		return false;
	}
}

//新增計畫項目
function addPlanItem(jsonObj) {
	ajaxAction('add', jsonObj);
}


function ajaxAction(act, jsonObj) {
	$.ajax({ 
	  type: "POST", 
	  url: "doAction.php", 
	  data: {
	  	act: act,
	  	bid: '<?php echo $pID;?>',
	  	item: jsonObj
	  },
	  dataType:'json',
	  success: function(d){
	  	if(d['result'] == 1) {
	  		location.reload();
	  	} else {
	  		alert(d['message']);
	  	}
	  }
	})
}

</script>

</body>
</html>
