<?

	/*echo "<pre>";
	print_r($_REQUEST);
	exit;*/

	if(empty($_REQUEST['act']) || empty($_REQUEST['item']) || empty($_REQUEST['bid'])) exit;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";
	
	$result = array('result'=>1);

	$db = new db('budget_items');
	$sql = "select 1 from budget where id = '".$_REQUEST['bid']."'";
	$rs = $db->query($sql);
	if($db->num_rows($rs) != 1) {
		$result['result'] = 0;
		$result['message'] = '查無預算';
		echo json_encode($result);
		exit;
	}

	$idList = array();
	foreach ($_REQUEST['item'] as $key => $value) $idList[$key] = "'".$key."'";
	
	switch ($_REQUEST['act']) {
		case 'add':
			//防止重複新增到同一個預算
			$sql = "select pItemID from budget_items where pid='".$_REQUEST['bid']."' and pItemID in (".implode(',', $idList).")";
			$rs = $db->query($sql);
			if($db->num_rows($rs) > 0) {
				while($r=$db->fetch_array($rs)) {
					if(in_array("'".$r['pItemID']."'", $idList)) {
						unset($idList[$r['pItemID']]);
					}
				}
			}

			//判斷百分比 並且新增
			foreach ($idList as $key => $value) {
				$percent = getPercent($key);
				if($percent['pItemID'] > 0 && ((int)$_REQUEST['item'][$key] + (int)$percent['total']) <= 100) {
					$db->row = array();
					$db->row['pid'] = "'".$_REQUEST['bid']."'";
					$db->row['pItemID'] = "'".$key."'";
					$db->row['percent'] = "'".(int)$_REQUEST['item'][$key]."'";
					$db->insert();
				}
			}
			break;

		case 'edit':
			foreach ($_REQUEST['item'] as $key => $value) {
				$where = " bi.pid <> '".$_REQUEST['bid']."'";
				$percent = getPercent($key, $where);
				if($percent['pItemID'] > 0 && ((int)$_REQUEST['item'][$key] + (int)$percent['total']) <= 100) {
					$condition = "pid='".$_REQUEST['bid']."' and pItemID ='".$key."'";
					$db->row = array();
					$db->row['percent'] = "'".(int)$_REQUEST['item'][$key]."'";
					$db->update($condition);
				}
			}
			break;

		case 'del':
			$condition = "pid='".$_REQUEST['bid']."' and pItemID in (".implode(',', $idList).")";
			$db->delete($condition);
			break;
		
		default:
			$result['result'] = 0;
			$result['message'] = '請求錯誤';
			break;
	}
	echo json_encode($result);

	function getPercent($pItemID, $where = '') {
		global $db;
		$sql = "select IFNULL(pi.id,0) as pItemID, IFNULL(sum(bi.percent),0) as total from plan_items as pi left join budget_items as bi on pi.id=bi.pItemID where pi.id = '".$pItemID."'";
		if($where != '') $sql.= ' and '.$where;
		$rs = $db->query($sql);
		$r = $db->fetch_array($rs);
		return $r;
	}


	
?>