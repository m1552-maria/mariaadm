<?php
include_once($_SERVER['DOCUMENT_ROOT']."/urlGetProtect.php");
 if (!function_exists('file_put_contents')) {
    defined('FILE_APPEND') || define('FILE_APPEND','FILE_APPEND');
    defined('FILE_USE_INCLUDE_PATH') || define('FILE_USE_INCLUDE_PATH','FILE_USE_INCLUDE_PATH');
    function file_put_contents($filename, $data, $flag = '') {
        $flag = explode('|', $flag);
        $f_flag = in_array(FILE_APPEND, $flag) ? 'ab+' : 'wb+';
        $f_uip = in_array(FILE_USE_INCLUDE_PATH, $flag) ? true : false;

        if ($handle = fopen($filename, $f_flag, $f_uip))  {
             if (in_array(LOCK_EX, $flag)) {
                flock($handle,LOCK_EX);
             }
             if (is_resource($data)) {
                $contents = '';
                 while (!feof($data)) {
                    $contents .= fread($data, 1024);
                 }
                $data = $contents;
             } elseif (is_array($data)) {
                $data = implode('',$data);
             }
            fwrite($handle,$data);
            fclose($handle);
            return true;
         } else {
            return false;
         }
     }
 }
 $_REQUEST = getVars(decodeparams($_SERVER['QUERY_STRING']));
 $fn = $_REQUEST['fn'];
 if(isset($_POST['editor1'])) {
     //echo str_replace("\\","",$_POST[editor1]); 
     header("Content-type: text/html; charset=utf-8"); 
     file_put_contents($_SERVER['DOCUMENT_ROOT'].$fn,$_POST['editor1']);
     $message = '資料儲存成功';

 }
 $ct = file_get_contents($_SERVER['DOCUMENT_ROOT'].$fn);
?>
<?php include($_SERVER['DOCUMENT_ROOT']."/budget/common/menuList.php");?>
<?php include($_SERVER['DOCUMENT_ROOT']."/budget/common/header.php");?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
<?php include($_SERVER['DOCUMENT_ROOT']."/budget/common/menu.php");?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
             <!-- Modal -->
            <div class="modal fade hide" id="messageBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                    <h4 class="modal-title" id="myModalLabel">訊息</h4>
                  </div>
                  <div class="modal-body">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn blue" data-dismiss="modal">確認</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">

                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
							<?php echo $_REQUEST['title']?> 
						</h3>
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="/budget/index.php">首頁</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="javascript:void(0)"><?php echo $_REQUEST['title']?> </a></li>
                            <li class="pull-right no-text-shadow">
                                <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>
                                    <span></span>
                                    <i class="icon-angle-down"></i>
                                </div>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                    
                        <div class="portlet box grey">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-globe"></i>編輯資料</div>
                            </div>
                            <div class="portlet-body">
                                <form id="form1" method="post" action="" enctype="application/x-www-form-urlencoded">
                                    <textarea cols="80" id="editor1" name="editor1" rows="50"><?=$ct?></textarea>
                                    <div class="form-actions" style="text-align:right;">
                                        <button id="btnArchive" type="submit" class="btn blue"><i class="icon-save"></i> 確認</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
    <script type="text/javascript">
        var editor;
        window.onload = function() {    if(!editor)editor = CKEDITOR.replace('editor1',{customConfig :'/ckeditor/config.js', height:600}); }
        CKEDITOR.on('instanceReady', function( evt ) {  var editor = evt.editor;   });

        <?php echo (isset($message)) ? 'var message = "'.$message.'"' : 'var message = ""'; ?>
        
        $(document).ready(function(){
            if(message != '') {
                $('#messageBox .modal-body').html(message);
                $('#messageBox').modal('show');
            }
        });
    </script>
<?php include($_SERVER['DOCUMENT_ROOT']."/budget/common/footer.php");?>