imgReader = function(cutW,cutH,border){
	//:: Initial Varients 
	this.plus=50;				//每次zoomin,zoomout增減的值
 	this.thumb_zoom_ratio=0;	//圖片縮放比例
 	this.img_orginal_width=0;	//圖片原始寬
 	this.img_orginal_height=0;	//圖片原始高
 	/*this.thumb_orginal_width=0;	//縮圖未縮放時的原始寬
 	this.thumb_orginal_height=0;//縮圖未縮放時的原始高*/
	this.cutWidth=cutW;	//裁切框內框寬
	this.cutHeight=cutH;	//裁切框內框高
 	this.border=border;
 	this.zoomRatio=1;
 	this.maxCutW=0;
 	this.maxCutH=0;

	//::bind events
	/*$(".multiPhoto").bind('change',this.photoChange);
	$(".photoDel").bind('click',this.photoDel);
	$(".photoCut").bind('click',this.photoCut);
	$(".photoZoomin").bind('click',this.photoZoomin);
	$(".photoZoomout").bind('click',this.photoZoomout);*/
	$(document).on('change', '.multiPhoto', this.photoChange);
	$(document).on('click', '.photoDel', this.photoDel);
	$(document).on('click', '.photoCut', this.photoCut);
	$(document).on('click', '.photoZoomin', this.photoZoomin);
	$(document).on('click', '.photoZoomout', this.photoZoomout);
	
	this.html = $('.photoPreview').html();
}

imgReader.prototype.photoChange = function(){
	var oTD = $(this).parents("div").eq(0);
	var fName=$(this.files)[0]['name'];
	var ary=fName.split(".");
	var im,div,div1;
	$(this.files).each(function(){
		if(this.type!="image/jpeg" && this.type!="image/png"){	
			alert("只能選取圖檔：.jpg或.png"); 
			return false;
		}
		var fReader = new FileReader();
		fReader.readAsDataURL(this);
		fReader.onloadend = function(event){
			var oItem = oTD.find(".photoItem").eq(0);
			oItem.show();
			img = oItem.find("img").get(0);
			$(img).unbind("load");
			img.style.padding='1';
			div = oItem.find("div").get(0);
			div1 = oItem.find("div").get(1);
			$(div).addClass("draggable");
			img.src=event.target.result;
			$(img).load(function(){
				//console.log($(this).width()+","+$(this).height()+"   imgReader.cutWidth:"+imgReader.cutWidth+","+imgReader.cutHeight);
				if($(this).width()<imgReader.cutWidth || $(this).height()<imgReader.cutHeight){	//圖檔過小
					alert("圖檔過小，寬高需為"+imgReader.cutWidth+"x"+imgReader.cutHeight+"以上");
					oItem.hide(); 
					return false;
				}
				imgReader.img_orginal_width=$(this).width();
				imgReader.img_orginal_height=$(this).height();
 				if(deviceOS!="none"){//::行動裝置
					//::微調寬度，避免撐破畫面出現捲軸
					$(".photoPreview .photoItem .img").css("width",$(".photoPreview").parents(0).width());
					$(".photoPreview .photoItem").css("width",$(".photoPreview").parents(0).width()-5);
					//取得在螢幕上與原圖的比例(縮放比例)
					imgReader.zoomRatio=($(".photoPreview").parents(0).width())/imgReader.img_orginal_width;
				}else{//::PC
					if(pc_default_width>imgReader.img_orginal_width){	pc_default_width=imgReader.img_orginal_width;	}
					//::微調寬度，避免撐破畫面出現捲軸
					$(".photoPreview .photoItem .img").css("width",pc_default_width);
					$(".photoPreview .photoItem").css("width",pc_default_width);
					//取得在螢幕上與原圖的比例(縮放比例)
					imgReader.zoomRatio=pc_default_width/imgReader.img_orginal_width;
				}

				//::讓裁切框只能在img上移動
				interact('.draggable').draggable({   restrict: {    restriction: this }  });
				/*::調整裁切框大小
				zoomRatio=(window.innerWidth-40)/imgReader.img_orginal_width;
				zoomRatio=window.innerHeight/imgReader.img_orginal_height
				window.innerHeight=imgReader.cutHeight*zoomRatio
				*/
				//計算裁切框在螢幕上最大的寬高
				imgReader.maxCutW=imgReader.img_orginal_width*imgReader.zoomRatio;
				imgReader.maxCutH=imgReader.img_orginal_height*imgReader.zoomRatio;

				//console.log('maxCutW:'+imgReader.maxCutW+",maxCutH:"+imgReader.maxCutH);
				//console.log('imgReader.zoomRatio:'+imgReader.zoomRatio+","+$(img).width());

				$(div).width(imgReader.cutWidth*imgReader.zoomRatio);
				$(div1).width($(div).width());
				$(div).height(imgReader.cutHeight*imgReader.zoomRatio);


				//console.log($(div).width()+","+$(div).height()+",zoomRatio:"+imgReader.zoomRatio);
				
			});
		}
	});
}
imgReader.prototype.photoDel=function(){
	var oItem = $(this).parents(".photoItem").eq(0);
	$('.multiPhoto').val('');
	if (oItem.get(0).input) {
		oItem.get(0).input.remove();
	}
	oItem.remove();
	$('.photoPreview').html(imgReader.html);
}
/*
img[0]:image
img[1]:del icon
img[2]:cut icon
div[0]:裁切框
div[1]:裁切框內的半透明背景
*/
imgReader.prototype.photoCut=function(){
	var oItem = $(this).parents(".photoItem").eq(0);
	var img = oItem.find("img").get(2);
	$(img).attr("src","/budget/images/refresh.png");
	var div = oItem.find("div").get(0);
	var zoomBtn,display;
	if(div.style.display=="block"){//復原原圖
		display='none';
		$(img).attr("src","/budget/images/cut.png");
		$(img).attr("title", '裁切圖檔');
		img = oItem.find("img").get(0);
		$(img).width($(img).width()-50*imgReader.thumb_zoom_ratio);
		imgReader.thumb_zoom_ratio=0;
	}else{//進入cutting模式
		display='block';
		$(img).attr("src","/budget/images/refresh.png");
		$(img).attr("title", '恢復原圖');
	}
	div.style.display=display;	

	for(var i=3;i<=4;i++){//zoomin,zoomout
		zoomBtn= oItem.find("img").get(i);
		zoomBtn.style.display=display;
	}
}
imgReader.prototype.photoZoomin=function(){
	var oItem = $(this).parents(".photoItem").eq(0);
	var div = oItem.find("div").get(0);
	var div1 = oItem.find("div").get(1);
	var addRatio;
	if(($(div).width()+imgReader.plus)<=imgReader.maxCutW){//寬不能超出maxW
		addRatio=$(div).width()/($(div).width()+imgReader.plus);
		if(($(div).height()/addRatio)<=imgReader.maxCutH){//高不能超出maxH
			$(div).width($(div).width()+imgReader.plus);
		}else{
			addRatio=1;
		}
	}else{
		addRatio=$(div).width()/(imgReader.maxCutW);
		$(div).width(imgReader.maxCutW);
	}
	$(div1).width($(div).width());
	$(div).height($(div).height()/addRatio);
		//console.log('new w:'+($(div).width()+imgReader.plus));
		//console.log('maxCutW:'+imgReader.maxCutW);

}

imgReader.prototype.photoZoomout=function(){
	var oItem = $(this).parents(".photoItem").eq(0);
	var div = oItem.find("div").get(0);
	var div1 = oItem.find("div").get(1);
	var addRatio;
	//裁切框在螢幕的原始大小：imgReader.cutWidth*imgReader.zoomRatio
	if(($(div).width()-imgReader.plus)>=(imgReader.cutWidth*imgReader.zoomRatio)){
		addRatio=$(div).width()/($(div).width()-imgReader.plus);
		$(div).width($(div).width()-imgReader.plus);
	}else{
		addRatio=$(div).width()/(imgReader.cutWidth*imgReader.zoomRatio);
		$(div).width(imgReader.cutWidth*imgReader.zoomRatio);
	}
	$(div1).width($(div).width());
	$(div).height($(div).height()/addRatio);
}