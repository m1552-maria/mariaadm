<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	//include "../system/utilities/miclib.php";

	/*echo "<pre>";
	print_r($_REQUEST);
	exit;*/

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	if($act=='new' || $act=='edit') $_SESSION['plan_tab'] = 0;
	$tabidx = (isset($_SESSION['plan_tab'])) ? $_SESSION['plan_tab'] : 0;
	
	if($act=='del') { //:: Delete record -----------------------------------------
		foreach($_POST['ID'] as $k=>$v) $aa[] = "'$v'";
		$lstss = implode(',',$aa);
		$aa =array_keys($fieldsList);	
		$db = new db();	

		$sql = "select * from $tableName where $aa[0] in ($lstss) and status <= 11";
		$rs = $db->query($sql);
		$delIDList = array();
		while($r=$db->fetch_array($rs)) {
			//檢查是否要順便刪除檔案 與是否可以刪除檔案
			if(!empty($delFlag)) { 
				$fn = $r[$delField];
				if($fn) {
					if(checkStrCode($fn,'utf-8')) $fn=iconv("utf-8","big5",$fn); 
					$fn = $ulpath.$fn;
					//echo $fn;
					if(file_exists($fn)) @unlink($fn);
				}
			}
			$delIDList[] = "'".$r[$aa[0]]."'";
		}

		if(count($delIDList) > 0) {
			$sql = "delete from $tableName where $aa[0] in (".implode(',', $delIDList).")";
			//echo $sql; exit;
			$db->query($sql);

			//將子檔也刪除
			$sql = "delete from plan_items where pid in (".implode(',', $delIDList).")";
			$db->query($sql);
			$sql = "delete from plan_currency where fid in (".implode(',', $delIDList).")";
			$db->query($sql);
			$sql = "delete from plan_talent where fid in (".implode(',', $delIDList).")";
			$db->query($sql);
			$sql = "delete from plan_service where fid in (".implode(',', $delIDList).")";
			$db->query($sql);
			$sql = "delete from plan_education where fid in (".implode(',', $delIDList).")";
			$db->query($sql);
			$sql = "delete from plan_budget where fid in (".implode(',', $delIDList).")";
			$db->query($sql);
			$sql = "delete from plan_outlay where fid in (".implode(',', $delIDList).")";
			$db->query($sql);


		}
		
		
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------
	function planCheck($id) {
		global $emplyeeinfo;
		$db = new db();	

		$sql = "select ACCOUNT,NAME from mariaadm.admins where 1";
		$rs = $db->query($sql);
		while($r=$db->fetch_array($rs)) {
			$emplyeeinfo[$r['ACCOUNT']] = $r['NAME'];
		}

		$sql = "select * from plan_check where pID='".$id."' order by rDate asc";
		//print_r($sql);
		$rs = $db->query($sql);
		$html = array();
		$_html = array();
		while($r=$db->fetch_array($rs)) {
			$type = ($r['type'] == 0) ? $emplyeeinfo[$r['creator']].' 審核意見:' : $emplyeeinfo[$r['creator']].' 回覆:';
			$class = ($r['type'] == 0) ? 'class0' : 'class1';
			$content = str_replace(' ','&nbsp;', $r['content']);
			$content = str_replace(chr(13).chr(10), "<br />", $content);
			$_html[] = '<div class="'.$class.'"><span>'.$type.'</span><span style="float: right;color: #717171;font-size: 10px;">'.$r['rDate'].'</span><br><div>'.$content.'</div></div>';
		}
		if(count($_html) > 0) {
			$html[] = implode('<br>', $_html);
			$html[] = '<hr>';
		}
		$html[] = '<div id="checkText"><div>增加意見或回覆</div><textarea name="newNotes" rows="4" cols="50"></textarea></div>';

		return implode('', $html);
	}
?>	


<?php include($root."/budget/common/header.php");?>
<style type="text/css">
	.class0 {
		margin-top: 10px;
		/*height: 70px;*/
	    background-color: #f6ccca;
	    padding: 8px;
	    display: inline-block;
	    width: 500px;
	}

	.class1 {
		margin-top: 10px;
		/*height: 70px;*/
	    background-color: #bddcf8;
	    padding: 8px;
	    display: inline-block;
	    width: 500px;
	}
</style>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
<?php include($root."/budget/common/menu.php");?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- Modal -->
        <div class="modal fade hide" id="messageBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">訊息</h4>
              </div>
              <div class="modal-body">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn blue" data-dismiss="modal">確認</button>
              </div>
            </div>
          </div>
        </div>


            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			/*sean 20171121 edit*/
			$oprator = $opList['filterCondi'][$k]?$opList['filterCondi'][$k]:"%s = '%s'"; 
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]=sprintf($oprator,$k,$_REQUEST[$k]);
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]=sprintf($oprator,$k,$opList['filterOption'][$k][0]);
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	echo "<h1 align='center'>$pageTitle ";
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div><hr>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select *, id as checkID from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	//echo $pinf->curPage;
	//echo $sql;
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	$obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	echo "<hr>";

	//:TreeControl -----------------------------------------------------------------------
	$option = array('dbSource'=>DB_SOURCE2);
	$db2 = new db($option);
	$op = array(
		'ID'		=> 'depID',
		'Title'		=> 'depTitle',
		'TableName' => 'department',
		'RootClass' => 0,
		'RootTitle' => '瑪利亞基金會',
		'DB' 		=> $db2,
		'Modal' 	=> true,
		'URLFmt'	=> "javascript:detIDfilter(\"%s\",\"%s\")",
		'orderBy'	=> 'depID'
	 );
	$tc=new BaseTreeControl($op);
?>

<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom">
			<ul class="nav nav-tabs">
				<li <?=($tabidx==0?"class='active'":'')?>><a href="#tab0" data-toggle="tab">主檔內容<img id="tabg0" class="tabimg" src="/images/<?= $tabidx==0?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(0)"/></a></li>
				<li <?=($tabidx==1?"class='active'":'')?>><a href="#tab1" data-toggle="tab">計劃項目<img id="tabg1" class="tabimg" src="/images/<?= $tabidx==1?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(1)"/></a></li>
				<!--<li <?=($tabidx==2?"class='active'":'')?>><a href="#tab2" data-toggle="tab">人力編制<img id="tabg2" class="tabimg" src="/images/<?= $tabidx==2?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(2)"/></a></li>
				<li <?=($tabidx==3?"class='active'":'')?>><a href="#tab3" data-toggle="tab">經費預算<img id="tabg3" class="tabimg" src="/images/<?= $tabidx==3?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(3)"/></a></li>-->
			</ul>

			<div class="tab-content">
				<div class="tab-pane<?=($tabidx==0?' active':'')?>" id="tab0">
<?
	//:ViewControl Object ------------------------------------------------------------------------
	$formName = 'form1';
	$listPos = $pinf->curRecord % $pinf->pageSize;
	$status = (isset($pdata[$listPos]['status'])) ? $pdata[$listPos]['status'] : 0;
	switch($act) {
		case 'edit':	//修改
			if($_SESSION['privilege'] <= 10 && $pdata[$listPos]['notes'] == '') unset($fieldE['notes']);
			if($_SESSION['privilege'] <= 10 && $status == 11) {
				$statusList = array(11=>'需修改', 12=>'再次送審');
				$fieldE['status'] = array("狀態","select",1,"",$statusList);
			}elseif($_SESSION['privilege'] <= 10 && $status == 0 && $pdata[$listPos]['cLeader'] == $_SESSION['empID']) {
				$statusList = array(0=>'未完成', 1=>'完成並送審');
				$fieldE['status'] = array("狀態","select",1,"",$statusList);
			}
			$edit = ($_SESSION['privilege'] > 10) ? '審查' :'修改';
			$op3 = array(
				"type"=>"edit",
				"form"=>array($formName,'doedit.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定".$edit, false,''),
				"resetBtn"=>array("重新輸入",($_SESSION['privilege'] > 10)?true:false,''),
				"cancilBtn"=>array("取消".$edit ,false,''));

			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"type"=>"append",
				"form"=>array($formName,'doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			//if(count($pdata) == 0) break;
			if($pdata[$listPos]['notes'] == '') unset($fieldE['notes']);
			$op3 = array(
				"type"=>"view",
				"submitBtn"=>array("確定變更",true,''),
				"resetBtn"=>array("重新輸入",true,''),
				"cancilBtn"=>array("關閉",true,''));
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);		
	}
	$planID = (isset($pdata[$listPos]['id'])) ? $pdata[$listPos]['id'] : 0;
?>
				</div>
				<div class="tab-pane<?=($tabidx==1?' active':'')?>" id="tab1">
			    	<iframe width="100%" height="420" frameborder="0" src="../plan_items/list.php?pid=<?=$planID?>"></iframe>
			    </div>
				<!--<div class="tab-pane<?=($tabidx==2?' active':'')?>" id="tab2">
			    	<iframe width="100%" height="420" frameborder="0" src="../plan_talent/list.php?fid=<?=$planID?>"></iframe>
			    </div>
				<div class="tab-pane<?=($tabidx==3?' active':'')?>" id="tab3">
			    	<iframe width="100%" height="420" frameborder="0" src="../plan_currency/list.php?fid=<?=$planID?>"></iframe>
			    </div>-->
			</div>
		</div>

<!--END TABS-->



                    </div>
                </div>
     
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
<?php include '../public/inc_listjs.php';?>
<script>

var formName = "<?php echo $formName;?>";

$(function () {
	checkPlanArrangeId();
	<?php if($status >= 20 && $_SESSION['privilege'] <= 10) echo "$('.editBtn').prop('disabled',true);
	";?>
	
<?php if($_SESSION['privilege'] > 10 && isset($_REQUEST['act']) && $_REQUEST['act'] == 'edit') { ?>

	/*$('#ViewControl table [name]').each(function(){
		var name = $(this).attr('name');
		if(name != 'status' && name != 'Submit' && name != 'id' && name != 'notes' && name != 'ListFmParams') $(this).prop('disabled',true);
	});*/
<?php } ?>

<?php if($_SESSION['privilege'] <= 10 && isset($_REQUEST['act']) && $_REQUEST['act'] == 'edit') {?>
	if($('#ViewControl table [name="notes"]').length == 1) {
		$('#ViewControl table [name="notes"]').prop('readonly',true);
	} 
<?php } ?>



	/*判斷手KEY部門主管編號 是否正確*/
	$(document).on('input','[name="'+formName+'"] #cLeader', function(){
		var cLeaderThis = this;
		$.get('/getdbInfo.php',{'act':'empName','ID':$(cLeaderThis).val()},function(data){ 
			$(cLeaderThis).next().next().text(data); 
			if(data == '無此編號！') cLeaderThis.setCustomValidity(data);
			else cLeaderThis.setCustomValidity('');
		});
	});

	/*部門更換時 讓部門主管也更著更換*/
	$('[name="'+formName+'"] [name="depID"]').change(function(){
		$.ajax({
            url:'getEmpData.php',
            type:'post',
            data:{depID:$(this).val()},
            dataType:'json',
            success:function(d){
            	console.log(d);
            	if(d['result'] == 1) {
            		$('[name="'+formName+'"] #cLeader').val(d['data']['leadID']);
            		$('[name="'+formName+'"] #cLeader').next().next().text(d['data']['empName']); 
            	}
            }
        })  
	});

	$('[name="'+formName+'"] [name="year"]').change(function(){
		$.ajax({
            url:'getProjIDList.php',
            type:'post',
            data:{
            	year : $(this).val(),
            },
            dataType:'json',
            success:function(d){
      			var html = [];
        		for(var i in d) html.push('<option value="'+i+'" >'+d[i]+'</option>');
        		$('[name="'+formName+'"] [name="plan_arrange_id"]').html(html.join(''));
            }
        })  
	});

<?php if($_SESSION['privilege'] <= 10) { ?>

	$('[name="'+formName+'"] [name="bugetChange"]').change(function(){
		if($(this).val() == 1) {
			$('[name="'+formName+'"] [name="plan_arrange_id"]').attr('disabled','disabled');
			$('[name="'+formName+'"] [name="plan_arrange_id"]').val(1);
		} else {
			$('[name="'+formName+'"] [name="plan_arrange_id"]').removeAttr('disabled','disabled');
		}
		checkPlanArrangeId();
	});

	$('[name="'+formName+'"] [name="plan_arrange_id"]').attr('disabled','disabled');
<?php } ?>

	$('[name="plan_arrange_id"]').change(function(){
		checkPlanArrangeId();
	});
}); 
function setnail(n) {
	$('.tabimg').attr('src','/images/unnail.png');
	$('#tabg'+n).attr('src','/images/nail.png');
	$.get('setnail.php',{'idx':n});
}

/*showDialog 用於複寫 原先form.js的showDialog*/
function showDialog(aObj) {
	if(rzt){
		rzt.close();
		rzt = false;
	} 
	param = $(aObj).prev(); 
	var parid = param.attr('id');
	var procP = id2proc[parid];
	var depID;
	var paramsObj=[];
	if(parid!='depID') {
		depID = $('[name="'+formName+'"] [name="depID"]').val();
		if(depID)paramsObj.push('depID='+depID);
	}
	if(paramsObj.length>0)	procP += "?"+paramsObj.join("&");
 	if(rzt == false) {
		rzt = openBrWindow(procP,param.val(),300,450);
		rzt.focus();
 	}
}

var eSelOpen = false;
function excelSelect(evt) {
	if(eSelOpen){
        eSelOpen.close();
    }
    if(eSelOpen == false){
    	eSelOpen = window.open("excelSelect.php", "web",'width=600,height=750');
    }else{
    	eSelOpen = window.open("excelSelect.php", "web",'width=600,height=750');
    	eSelOpen.focus();
    }
}
function depTreeShow(evt) {
	showTreeControl();
}

function detIDfilter(key,cname) {
	if(key == 0) key = '';
	while(ListControlForm.depID.options[0]) $(ListControlForm.depID.options[0]).remove();
	var op = document.createElement('option');
	op.value = key;
	op.text = cname;
	ListControlForm.depID.add(op);
	ListControlForm.cname.value = cname;
	ListControlForm.submit();		
}

function checkPlanArrangeId() {
	var pThis = $('[name="plan_arrange_id"]')[0];
	if($('[name="bugetChange"]').val() == 2 && $('[name="plan_arrange_id"]').val() == 0) {
		pThis.setCustomValidity('請選擇併入計畫');
	} else {
		pThis.setCustomValidity('');
	}
}
</script>

<?php include($root."/budget/common/footer.php"); exit;?>
