<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	if(empty($_POST['depID'])) exit;

	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/system/db.php");

	$result = array('result'=>0);
	$option = array('dbSource'=>DB_SOURCE2);
	$db = new db($option);

	$sql = "select d.depID, d.leadID, e.empName from department as d inner join emplyee as e on d.leadID = e.empID where d.depID = ?";
	$rs = $db->query($sql, array($_POST['depID']));
	if($db->num_rows($rs) > 0) {
		$r = $db->fetch_array($rs);
		$result['result'] = 1;
		$result['data']['leadID'] = $r['leadID'];
		$result['data']['empName'] = $r['empName'];
	}


	echo json_encode($result);
?>