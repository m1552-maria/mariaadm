<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	include_once($root."/budget/inc_vars.php");
	$creator = (isset($_SESSION['empID'])) ? $_SESSION['empID'] : $_SESSION['vid'];

	$pageTitle = "計劃追加減";
	$tableName = "plan";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 5;
	$NeedFilter = ' bugetChange <> 0 ';
	/*echo "<pre>";
	print_r($_REQUEST);
	exit;*/
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => ($_SESSION['privilege'] > 10) ? array("－審查",false,true,'doEdit') : array("－修改",false,true,'doEdit'),
		"delBtn" => ($_SESSION['privilege'] > 10) ? array("Ｘ刪除",true,true,'') : array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$nowYear = date('Y')-1911;
	$statusList = array(''=>'--申請狀態--', 0=>'未完成', 1=>'完成並送審', 10=>'審查中', 11=>'需修改', 12=>'再次送審', 20=>'審查通過');
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'DESC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,''),
			'year'=>array(($nowYear+1), false)
		),
		"filterCondi"=>array(	//比對條件
			'depID'=>"%s like '%s%%'"
		),
		"filters"=>array( //過濾器
			'year'=>array(),
			'status'=>$statusList
		),
		"extraButton"=>array( //新增的button
		)
	);

	$sqlYear = "select year from ".$tableName;
	$depIDList = array_merge(array(''=>'--部門--'), $departmentinfo);
	if($_SESSION['privilege'] > 10) { //總後台過來
		//$opList['filterOption']['status'] = array(0, false);
		//disabled 新增
	//	$opPage['newBtn'] = array("＋新增",true,true,'');
		//TreeControl
		if(!empty($_REQUEST['depID'])) $opAR = array($_REQUEST['depID']=>$_REQUEST['cname']); 
		else $opAR = array(''=>'--全部--');
		$opList['filters']['depID'] = $opAR;
		$opList['extraButton']['depTreeShow'] = array("選取部門",false,true,'depTreeShow');
	} else {
		if(!in_array($_SESSION['jobID'], $editJop)) {
			$opPage['newBtn'] = array("＋新增",true,true,'');
			$opPage['editBtn'] = array("－修改",true,true,'');
			$opPage['delBtn'] = array("Ｘ刪除",true,true,'');
		}

		$_NeedFilter = array();
		foreach ($depIDList as $key => $value) {
			if(!in_array($key, $_SESSION['user_classdef'][2]) && $key != '' && $key != $_SESSION['depID']) unset($depIDList[$key]);
			elseif($key != '') $_NeedFilter[] = " depID like '".$key."%'";
		}
		ksort($depIDList);
		$opList['filters']['depID'] = $depIDList;

		if($_REQUEST['depID'] == '') {
			$_NeedFilter = "(".implode(' or ', $_NeedFilter).")";
			$sqlYear .= " where ".$_NeedFilter;
		} else $_NeedFilter = '';
		if($_NeedFilter != '') $NeedFilter .= ' and '.$_NeedFilter;
	}

	$sqlYear .= " group by year order by year";
	$db = new db();	
	$yearList = array(''=>'--年度選取--');
	$rs = $db->query($sqlYear);
	while($r=$db->fetch_array($rs)) $yearList[$r['year']] = $r['year'];
	$yearList[($nowYear+1)] = $nowYear+1; //讓明年度一定出現在下拉選單
	$opList['filters']['year'] = $yearList;

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/

	$bugetChangeList = array('1'=>'追加', '2'=>'追減');

	//$invNo['']='無統編';
	unset($statusList['']);
	unset($depIDList['']);
	$fieldsList = array(
		"id"=>array("編號","120px",true,array('Id',"gorec(%d)")),
		"year"=>array("年度","",true,array('Text')),
		"title"=>array("標題","",true,array('Text')),
		"invNo"=>array("統編","",true,array('Select', $invNo)),
		/*"bDate"=>array("開始日期","",true,array('Date')),
		"eDate" =>array("結束日期","",true,array('Date')),
		"srvpeople"=>array("服務量","",false,array('Text')),*/
		"creator"=>array("建單人","",true,array('Select', $emplyeeinfo)),
		"status"=>array("狀態","",true,array('Select', $statusList)),
		"depID"=>array("部門","",false,array('Select', $depIDList)),
		"bugetChange"=>array("追加減","",true,array('Select', $bugetChangeList))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$newYear = array();
	for ($i=$nowYear; $i<$nowYear+10; $i++) $newYear[$i] = $i;
	$sql = "select id, title from plan_stringmap where isReady = 1 and class= 'mtype'";
	$rs = $db->query($sql);
	$mtypeList = array();
	while($r=$db->fetch_array($rs)) $mtypeList[$r['id']] = $r['title'];

	$year = (isset($_REQUEST['year'])) ? $_REQUEST['year'] : $nowYear;
	$sql = "select id, title, projID from plan_arrange where year = '".$year."'";
	if($_SESSION['privilege'] <= 10) {
		$depIDListString = array();
		foreach ($depIDList as $key => $value) {
			$depIDListString[] = " depID like '".$key."%'";
		}
		$sql .= " and (".implode(' or ', $depIDListString).")";
	}
	$sql .= " order by projID asc";
	$rs = $db->query($sql);
	$planArrangeList = array('0'=>'新計劃編號');
	while($r=$db->fetch_array($rs)) $planArrangeList[$r['id']] = $r['projID'].'('.$r['title'].')';

	$fieldA = array(
		"depID"=>array("部門","select",1,"",$depIDList),
		"year"=>array("年度","select",1,"",($_SESSION['privilege'] > 10) ? $yearList : $newYear),
		"title"=>array("<span class='need'>*</span>名稱","text","60","","",array(true,'','','')),
		"invNo"=>array("統編","select",1,"",$invNo),
		/*"mtype"=>array("服務類型","select",1,"",$mtypeList),
		"bDate"=>array("開始日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"eDate"=>array("結束日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"srvpeople"=>array("服務量","text","100","",""),
		"talents"=>array("人力需求量","text","100","",""),
		"goal"=>array("目標","textbox","100","5",""),
		"content"=>array("內容","textbox","100","15",""),*/

		"creator"=>array("建單人","readonly","","",""),
		"cLeader"=>array("部門主管","queryID","","","emplyeeinfo"),
		"bugetChange"=>array("追加減","select",1,"",$bugetChangeList),
		"plan_arrange_id"=> array("併入計畫編號","select",1,"",$planArrangeList,array(true,'','','')),
		"notes2"=> array("備註","textbox","50","7","")
	);
	//新增時的預設值
	$fieldA_Data = array();
	$fieldA_Data['year'] = ($_REQUEST['year']) ?  $_REQUEST['year'] : $nowYear+1;
	if($_SESSION['privilege'] == 10) {
		$fieldA_Data['creator'] = $_SESSION['empID'];
		$fieldA_Data['cLeader'] = $departmentlead[key($depIDList)];
	}
	
	/*echo "<pre>";
	print_r($_REQUEST);
	exit;*/


	$fieldE = array(
		"id"=>array("編號","id",20),
		"depID"=>array("部門","select",1,"",$depIDList),
		"year"=>array("年度","select",1,"",($_SESSION['privilege'] > 10) ? $yearList : $newYear),
		"title"=>array("<span class='need'>*</span>名稱","text","60","","",array(true,'','','')),
		"invNo"=>array("統編","select",1,"",$invNo),
		/*"mtype"=>array("服務類型","select",1,"",$mtypeList),
		"bDate"=>array("開始日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"eDate"=>array("結束日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"srvpeople"=>array("服務量","text","100","",""),
		"talents"=>array("人力需求量","text","100","",""),
		"goal"=>array("目標","textbox","100","5",""),
		"content"=>array("內容","textbox","100","15",""),*/
		"creator"=>array("建單人","readonly","","",""),
		"cLeader"=>array("部門主管","queryID","","","emplyeeinfo"),
		"status"=>($_SESSION['privilege'] > 10)	? array("狀態","select",1,"",$statusList) : array("狀態","readonly","","",$statusList),
		"bugetChange"=>array("追加減","select",1,"",$bugetChangeList),
		"plan_arrange_id"=> ($_SESSION['privilege'] > 10) ? array("併入計畫編號","select",1,"",$planArrangeList) : array("併入計畫編號","readonly","","",$planArrangeList),
		"notes2"=> array("備註","textbox","50","7",""),
		"checkID"=> array("審核意見","Define","","", planCheck),
		//"notes"=> array("需修改內容","textbox","50","7","")
	);

	if($_SESSION['privilege'] > 10) { //總後台過來
		$fieldE['depID'] = array("部門","queryID",10,"","departmentinfo");
	}
?>