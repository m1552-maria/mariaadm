<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	//include "../system/utilities/miclib.php";

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	
	if($act=='del') { //:: Delete record -----------------------------------------
		foreach($_POST['ID'] as $k=>$v) $aa[] = "'$v'";
		$lstss = implode(',',$aa);
		$aa =array_keys($fieldsList);	
		$db = new db();	
		//檢查是否要順便刪除檔案 與是否可以刪除檔案
		if(!empty($delFlag)) { 
			$sql = "select * from $tableName where $aa[0] in ($lstss)";
			$rs = $db->query($sql);
			while($r=$db->fetch_array($rs)) {
				$fn = $r[$delField];
				if($fn) {
					if(checkStrCode($fn,'utf-8')) $fn=iconv("utf-8","big5",$fn); 
					$fn = $ulpath.$fn;
					//echo $fn;
					if(file_exists($fn)) @unlink($fn);
				}
			}
		}

		$sql = "delete from $tableName where $aa[0] in ($lstss)";
		//echo $sql; exit;
		$db->query($sql);
		
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------

?>
<?php 
    header("Cache-control: private");

    $_SESSION['maintain']['name'] = (isset($_SESSION['Name'])) ? $_SESSION['Name'] : $_SESSION['empName'];

    $message = '';
    if(isset($_SESSION['maintain']['MESSAGE']) && gettype($_SESSION['maintain']['MESSAGE']) == 'string') {
        $message = $_SESSION['maintain']['MESSAGE'];
        unset($_SESSION['maintain']['MESSAGE']);
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />	
    <link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 

<script src="/budget/media/script/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
</head>
<body>
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo "<p>搜尋:".$opList['keyword']."</p>";
	
	//:PageControl ---------------------------------------------------------------------
 	if(empty($_REQUEST['openWindow'])) {
		$obj = new EditPageControl($opPage, $pinf);	
		echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div>";
	}

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	//echo $pinf->curPage;
	//echo $sql;
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);

	//:ViewControl Object ------------------------------------------------------------------------
	$formName = 'formPlanService';
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			if($status < 20) {
				$op3 = array(
					"templet"=>$root."/budget/".$tableName."/edit.html",
					"type"=>"edit",
					"form"=>array($formName,'doedit.php','post','multipart/form-data','form1Valid'));
			} else {
				$op3 = array(
					"templet"=>$root."/budget/".$tableName."/_edit.html",
					"type"=>"edit");
			}
			
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"templet"=>$root."/budget/".$tableName."/add.html",
				"type"=>"append",
				"form"=>array($formName,'doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			$listObj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	}
?>
<?php include '../public/inc_listjs.php';?>
<script>
var formName = "<?php echo $formName;?>";
var educationClass = <?php echo json_encode($educationClass);?>;
function doAppend() {
	page=<?=$pinf->pages?>;

	var url = "/budget/plan_education/list.php?openWindow=1&act=new&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
}

function doEdit() {
	var url = "/budget/plan_education/list.php?openWindow=1&act=edit&recno="+recno+"&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
}

function gorec(recno) { //go edit ViewControl
	var url = "/budget/plan_education/list.php?openWindow=1&act=edit&recno="+recno+"&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
	tdChange(recno);
}

function form1Valid() {
	var data = [];
	var _data = {};
	var text = '新增';
<?php if($act == 'new') { ?>
		$('#newTable [name]').each(function(){
			var arr = [];
			var n = $(this).attr('name');
			n = n.split('[]');
			n = n[0];
			if(!data[n]) data[n] = [];
			data[n].push($(this).val());
		});

		for(var i in data) {
			if(!_data[i]) _data[i] = {};
			for(var j in data[i]) {
				_data[i][j] = data[i][j];
			}
		}

<?php } else { ?>
	text = '修改';
	$('#newTable [name]').each(function(){
		_data[$(this).attr('name')] = $(this).val();
	});
<?php } ?>
	$.ajax({
		type:'post',
		url:'<?php echo ($act == "new") ? "doappend.php" : "doedit.php" ;?>',
		data: _data,
		success: function(d){
			alert(text+'教養補助完成');
			window.opener.closeItem('plan_education');
<?php if($_SESSION['privilege'] > 10 || in_array($_SESSION['jobID'], $editJop)) { ?>
			window.opener.closeItem('plan_currency');
<?php } ?>
			window.opener.oItem = false;
			window.close(); 
		}
	});
	
	return false;
}


$(function(){
	$(document).on('input','[name="'+formName+'"] [name^="empID"]', function(){
		var thisDom = this;
		param = $(this);
		$.get(
			'getEmp.php',
			{'empID':$(thisDom).val(),'fid':'<?php echo $fID;?>','sid':'<?php echo $sID;?>'},
			function(d){ 
				data = JSON.parse(d);
				returnValue(data);
				if(data['result'] == 0) thisDom.setCustomValidity(data['msg']);
				else thisDom.setCustomValidity('');
				thisDom.reportValidity(); //直接顯示錯誤訊息
				
			}
		);
	});

	$(document).on('change','[name^="class"]', function(){
		var k = $(this).parents('[data-one-tr]').attr('data-one-tr');
		$('[data-two-tr="'+k+'"] [name^="monthPay"]').val(educationClass[$(this).val()]);
		//console.log($(this).parents('[data-one-tr]').attr('data-one-tr'));
	});

	$(document).on('click', '[name="'+formName+'"] [name^="noEmp"]', function(){
		var key = $('[name^="noEmp"]').index(this);
		$('[name^="empID"]').eq(key).val('');
		$(this).prev('.formTxS').html('');
		if($(this).prop('checked')) {
			$('.formTxS').eq(key).html('');
			$('[name^="empID"]').eq(key).attr('readonly','readonly');
			$(this).val(1);
		} else {
			$('[name^="empID"]').eq(key).removeAttr('readonly');
			$(this).val('');
		}

	});

});

/*showDialog 用於複寫 原先form.js的showDialog*/
function showDialog(aObj) {
	if(rzt){
		rzt.close();
		rzt = false;
	} 
	param = $(aObj).prev(); 
	var parid = 'empID';
	var procP = id2proc[parid];
	var depID;
	var paramsObj=[];
<?php if(empty($_SESSION['user_classdef'][2]) or count($_SESSION['user_classdef'][2]) == 0) { ?>
	paramsObj.push('depID=<?php echo $_SESSION['depID'];?>');
<?php } ?>
	if(paramsObj.length>0)	procP += "?"+paramsObj.join("&");
 	if(rzt == false) {
		rzt = openBrWindow(procP,param.val(),300,450);
		rzt.focus();
 	}
}


/*returnValue 用於複寫 原先form.returnValue*/
function returnValue(data) {
	console.log(data);
	if(data) {
		var isSet = 1;
		var k = param.parents('tr').attr('data-one-tr');
		$('[name^="empID"]').each(function(){
			if(data[0] == $(this).val() &&  k != $(this).parent('td').parent('tr').attr('data-one-tr') && $(this).val() != '') {
				isSet = 0;
				alert('人員重複');
				param.val('');
				param.next().next('span').html(''); 
				param.parent('td').next('td').next('td').children('[name^="rate"]').val('');
				$('[data-two-tr="'+k+'"] [name^="amount"]').val('');	
				return false;
			}
		});
		if(!isSet) return false;
		param.val(data[0]);
		param.next().next('span').html(data[1]); 
		
	}
}

function tdChange(recno) {
	$('.ListRow td').removeClass('columnCurrent');
	$('.ListRow:odd td').addClass('columnDataOdd');
	$('.ListRow:even td').addClass('columnDataEven');
	$('.ListRow').eq(recno-1).children('td').removeClass('columnDataOdd');
	$('.ListRow').eq(recno-1).children('td').removeClass('columnDataEven');
	$('.ListRow').eq(recno-1).children('td').addClass('columnCurrent');
}
</script>
</body>
</html>

