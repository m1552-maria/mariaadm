<?php

	class fileOperat {


		function __construct($dir) {
			$this->dir = $dir;
			if(!is_dir($this->dir)) mkdir($this->dir, 0777, true);
			$this->finfo = finfo_open(FILEINFO_MIME_TYPE);
	    }

	    function __destruct(){
	  	}

	  	public function fileDelete($fileName) {
	  		$fnn = iconv("utf-8", "big5", $_SERVER['DOCUMENT_ROOT'].$fileName);
			if(file_exists($fnn)) @unlink($fnn);
	  	}

	  	public function fileUpload($allow_file_type, $file, $filename = '') {
	  		$result = array('result'=>0);
			$mime = finfo_file($this->finfo, $file['tmp_name']);

			if(!in_array($mime, $allow_file_type)) {
				$result['text'] = "上傳的文件格式不支援，請上傳其他類型檔案";
				return $result;
			}
			$finf = pathinfo($file['name']); 
			if($filename != ''){
	        	$result['text'] = $filename.'.'.$finf['extension'];
	        }else{
	        	$result['text'] = md5(strtotime(date("Y-m-d H:i:s")).rand()).'.'.$finf['extension'];
	        }
	        move_uploaded_file($file["tmp_name"],  iconv("utf-8", "big5", $this->dir."/".$result['text']));

	        $result['result'] = 1;
	        $result['text'] = '/data'.(end(explode('data', $this->dir))).'/'.$result['text'];
        	return $result;
	  	}

	  	public function imgUpload($allow_img_type, $imgFile, $filename = '') {
	  		$result = array('result'=>0);
			$mime = finfo_file($this->finfo, $imgFile['tmp_name']);

			if(!in_array($mime, $allow_img_type)) {
				$result['text'] = "圖檔請上傳jpg、png的格式";
				return $result;
			}
			$finf = pathinfo($imgFile['name']); 
			if($filename != ''){
	        	$result['text'] = $filename.'.'.$finf['extension'];
	        }else{
	        	$result['text'] = md5(strtotime(date("Y-m-d H:i:s")).rand()).'.'.$finf['extension'];
	        }
	        move_uploaded_file($imgFile["tmp_name"], $this->dir."/".$result['text']);

	        $result['result'] = 1;
	        $result['text'] = '/data'.(end(explode('data', $this->dir))).'/'.$result['text'];
        	return $result;
	  	}

		public function imgCutUpload($allow_img_type, $imgFile, $val, $filename = '') {

			$result = array('result'=>0);
			$mime = finfo_file($this->finfo, $imgFile['tmp_name']);

			if(!in_array($mime, $allow_img_type)) {
				$result['text'] = "圖檔請上傳jpg、png的格式";
				return $result;
			}
			$finf = pathinfo($imgFile['name']); 
			if($filename != ''){
	        	$result['text'] = $filename.'.'.$finf['extension'];
	        }else{
	        	$result['text'] = md5(strtotime(date("Y-m-d H:i:s")).rand()).'.'.$finf['extension'];
	        }

        	$val = explode(",",$val);

        	if($val[0]=="0" || $val[0]=="") { //沒有做裁切
        		move_uploaded_file($imgFile["tmp_name"], $this->dir."/".$result['text']);
        	} else {
        		//::取得client的圖片相關資訊
				$zoom_ratio=$val[0];
				$thumb_x=$val[1];
				$thumb_y=$val[2];
				$thumb_w=$val[3];
				$thumb_h=$val[4];

				$extend=strtolower($finf['extension']);
				switch ($extend) {
					case 'jpg': $src = imagecreatefromjpeg($imgFile['tmp_name']); break;
					case 'png': $src = imagecreatefrompng($imgFile['tmp_name']); break;
					default: return false; break;
				}
				$src_w = imagesx($src);
				$src_h = imagesy($src);

			  	//若有做裁切框縮放
			  	if($zoom_ratio>1){
					$zoom_w=$src_w/$zoom_ratio;
					$zoom_h=$src_h/$zoom_ratio;
					$zoom=imagecreatetruecolor($zoom_w, $zoom_h);
					imagecopyresampled($zoom, $src, 0, 0, 0,0,$zoom_w,$zoom_h,  $src_w,$src_h);
					$src=$zoom;
				}
				  
				$thumb = imagecreatetruecolor($thumb_w, $thumb_h);
				imagecopy($thumb, $src, 0, 0, $thumb_x,$thumb_y,  $src_w,$src_h);
				//儲存縮圖
				switch ($extend) {
					case 'jpg': imagejpeg($thumb, $this->dir.'/'.$result['text']); break;
					case 'png': imagepng($thumb, $this->dir.'/'.$result['text']); break;
				}

        	}
        	$result['result'] = 1;
        	$result['text'] = '/data'.(end(explode('data', $this->dir))).'/'.$result['text'];

        	return $result;
		}
	}

?>