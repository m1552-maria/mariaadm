  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '' && $('[name="planID"]').val() == '') {
      return false;
    }
    return true;
  }


  function dataClear() {
    $('[data-tr]').remove();
    $('#showData').html('');
    $('[name="Submit"]').attr('disabled','disabled');
  }


  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        planID:$('[name="planID"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
        } else {
          $('#showTitle').html(data['showTitle']);
          var html = '';
          html = setData(data, act);
          $('#showData').html(html);
          $('[name="Submit"]').removeAttr('disabled');
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act) {
    var result = [];
    var j = 0;
    var dList = data['dataList'];
    
    for(var i in dList) {
      result.push('<h4>【'+dList[i]['name']+'】</h4>');
      if(act == 'print') result.push('<table class="tableData" width="1000px;" border="1">'); 
      else result.push('<table>'); 
      result.push('<tr>');
      result.push('<th style="width:10%" class="cLabel">服務名稱</th>');
      result.push('<th style="width:18%"  class="cLabel">服務目標</th>');
      result.push('<th class="cLabel">服務/計畫內容</th>');
      result.push('<th style="width:8%" class="cLabel">服務量<br>(人/人次/場次)</th>');
      result.push('<th style="width:10%" class="cLabel">執行時間</th>');
      result.push('<th style="width:17%" class="cLabel">經費預算</th>');
      result.push('</tr>');

      var items = dList[i]['data'];
      var c = 0;

      for(var j in items) {
        c++;
        result.push('<tr>');
        result.push('<td class="colData">'+items[j]['title']+'</td>');
        result.push('<td class="colData">'+items[j]['goal']+'</td>');
        result.push('<td class="colData">'+items[j]['content']+'</td>');
        result.push('<td class="colData">'+items[j]['srvpeople']+' '+items[j]['srvunit']+'<br>'+items[j]['srvpeopleNotes']+'</td>');
         result.push('<td class="colData">'+items[j]['bDate']+'~<br>'+items[j]['eDate']+'<br>'+items[j]['dateNotes']+'</td>');

        //經費預算部份
        result.push('<td class="colData">');
        result.push('<span>收入合計 : </span><span>'+items[j]['moneyData']['+']['total']+'</span>');
        var moneyAdd = items[j]['moneyData']['+']['data'];
        for(var k in moneyAdd) {
          result.push('<br>');
          result.push('<span>'+k+':</span><span>'+moneyAdd[k]+'</span>');
        }
        result.push('<br><br><span>支出合計 : </span><span>'+items[j]['moneyData']['-']['total']+'</span>');
        var moneyLess = items[j]['moneyData']['-']['data'];
        for(var k in moneyLess) {
          result.push('<br>');
          result.push('<span>'+k+' : </span><span>'+moneyLess[k]+'</span>');
        }
        result.push('<br><br><span>收支餘絀 : </span><span>'+items[j]['moneyData']['total']+'</span>');
        result.push('</td>');
        


        result.push('</tr>');

        result.push('<tr>');
        result.push('<td class="cLabel">績效/稽核<br>/評鑑指標</td>');
        result.push('<td colspan="3" class="colData">'+items[j]['srveffect']+'</td>');
        result.push('<td class="cLabel">備註</td>');
        result.push('<td class="colData">'+items[j]['notes']+'</td>');
        result.push('</tr>');

      }

      result.push('</table>');
    }

    


    return result.join('');
  }

 