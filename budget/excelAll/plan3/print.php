<?php
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";
	include "getData.php";

	$db = new db();
	$sql = "select * from plan3 where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);

	$itemList = getData($_GET['id']);

	$char = array("十","一","二","三","四","五","六","七","八","九");
/*echo "<pre>";
print_r($itemList);
exit;*/
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>瑪利亞預算管理系統</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
<style type="text/css">
  .cLabel {
    text-align: center;
    background-color: #EEE;
    border: 1px solid #333333;
    padding: 4px 8px 4px 4px;
  }
  .tableData {
  	margin: 0 auto;
  	border: 1px solid #00; border-collapse: collapse;
  }
  .tableData td{
  	padding-left: 5px;
  }
  .colData {
  	border-color: #000;
  }

  table {
  	font-size: 16px;
  }
</style>
</head>
<body>
	<input type="hidden" name="year" value="<?php echo $data['year'];?>">
	<input type="hidden" name="depID" value="<?php echo $data['depID'];?>">
  <input type="hidden" name="invNo" value="<?php echo $data['invNo'];?>">
  <input type="hidden" name="planID" value="<?php echo $data['planID'];?>">
  <input type="hidden" name="groupType" value="<?php echo $data['groupType'];?>">
	<h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3>
	<h3 style="text-align: center;">計劃表(三) - <?php echo $data['year'];?><span id="showYear"></span>年度計畫表</h3>
<div style="width: 1000px;margin: 0 auto;">
	<table  width="1000px;" style="margin: 0 auto;">
		<tr><td style="font-weight: bold;" id="showTitle"></td></tr>
	</table>
	
	<div id="showData">
  
	</div>
</div>

	<div style="text-align:center;margin-top:20px;">
		<button type="button" id="print_button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
	    <button type="button" id="print_close" onClick="window.close();">關閉</button>
	</div>
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="dataControl.js" type="text/javascript"></script>
<script type="text/javascript">
  	$(function(){
	    ajaxCheck('print', <?php echo $_GET['id'];?>);
	 })

	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
    }
</script>


</body>
</html>