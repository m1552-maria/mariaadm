<?php

	function getData($data = array()) {
		global $departmentinfo, $invNo;;
		$result = array('error'=>0,'msg'=>'', 'dataList'=>array());

		$db = new db();	
		$sql = "select * from plan where year='".$data['year']."'";
		$rs = $db->query($sql);
		$planTitleList = array(); 
		while($r=$db->fetch_array($rs)) {
			$planTitleList[$r['id']] = $r['title'];
		}

		$where = array();
		if(!empty($data['depID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$depIDList = explode(',', $data['depID']);
			$_where = array();
			foreach ($depIDList as $key => $value) {
				$_where[] = " p.depID like '".$value."%' ";
				$showTitle[] = $departmentinfo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '機構/中心:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['invNo'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$invNoList = explode(',', $data['invNo']);
			$_where = array();
			foreach ($invNoList as $key => $value) {
				$_where[] = " p.invNo like '".$value."%' ";
				$showTitle[] = $invNo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '統一編號:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['planID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$planList = explode(',', $data['planID']);
			$_where = array();
			foreach ($planList as $key => $value) {
				$showTitle[] = $planTitleList[$value];
				$planList[$key] = "'".$value."'";
			}
			$_where = "p.id in (".implode(',', $planList).")";
			$result['showTitle'][] = '計劃主檔:'.implode(',', $showTitle);
			$where[] = $_where;
		}
		
		$result['showTitle'] = implode(' / ', $result['showTitle']);

		$where = implode(' and ', $where);

		$db = new db();	

		$sql = "select * from plan_stringmap where class='mtype' order by id";
		$mList = array();
		$rs = $db->query($sql);
		while ($r=$db->fetch_array($rs)) {
			$mList[$r['id']] = $r['title'];
		}


		//$sql = "select i.* from plan p inner join plan_items i on p.id = i.pid where p.year='".$data['year']."' and p.depID like '".$data['depID']."%' and  p.bugetChange = '0' order by i.mtype, i.id"; //要排除追加減的部份, 只要本文
		$sql = "select i.* from plan p inner join plan_items i on p.id = i.pid where p.year='".$data['year']."' and p.bugetChange = '0' ";
		$sql .= ' and '. $where;
		$sql .= " order by i.mtype, i.pid, i.sort";
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) { 
			$result['error'] = 1;
			$result['msg'] = ($data['year']).'年 所選'.$result['showTitle'].' 未存在年度計畫表';
			return $result;
		}
		
		$dataList = array();
		//$idList = array();


		while ($r=$db->fetch_array($rs)) {
			$ID = $r['id'];
			$r['id'] = '_'.$r['id']; //防止到前台時自動依照ID(數值)排序
			$dataList[$r['mtype']]['name'] = $mList[$r['mtype']];
			$dataList[$r['mtype']]['data'][$r['id']] = $r;
			$dataList[$r['mtype']]['data'][$r['id']]['bDate'] = (empty($r['bDate'])) ? '' : date('Y/m/d', strtotime($r['bDate']));
			$dataList[$r['mtype']]['data'][$r['id']]['eDate'] = (empty($r['eDate'])) ? '' : date('Y/m/d', strtotime($r['eDate']));

			$dataList[$r['mtype']]['data'][$r['id']]['srveffect'] = str_replace(' ','&nbsp;',$r['srveffect']);
			$dataList[$r['mtype']]['data'][$r['id']]['srveffect'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['mtype']]['data'][$r['id']]['srveffect']);

			$dataList[$r['mtype']]['data'][$r['id']]['dateNotes'] = str_replace(' ','&nbsp;',$r['dateNotes']);
			$dataList[$r['mtype']]['data'][$r['id']]['dateNotes'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['mtype']]['data'][$r['id']]['dateNotes']);

			$dataList[$r['mtype']]['data'][$r['id']]['notes'] = str_replace(' ','&nbsp;',$r['notes']);
			$dataList[$r['mtype']]['data'][$r['id']]['notes'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['mtype']]['data'][$r['id']]['notes']);

			$dataList[$r['mtype']]['data'][$r['id']]['srvpeopleNotes'] = str_replace(' ','&nbsp;',$r['srvpeopleNotes']);
			$dataList[$r['mtype']]['data'][$r['id']]['srvpeopleNotes'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['mtype']]['data'][$r['id']]['srvpeopleNotes']);

			//$dataList[$r['mtype']]['dataLength'] += 1;

			$data['sid'] = "'".$ID."'";
			if(isset($data['id'])) unset($data['id']);
			$budget1Data = getBudget1Data($data);
			$dataList[$r['mtype']]['data'][$r['id']]['moneyData'] = array();
			if($budget1Data['error'] == 0) {
				$moneyData = array('+'=>array('total'=>0, 'data'=>array()), '-'=>array('total'=>0, 'data'=>array()));
				foreach ($budget1Data['dataList'] as $k => $v) {
					if($k != '' && $v['total'] != 0) {
						$moneyData[$v['vType']]['total'] += $v['total'];
						$moneyData[$v['vType']]['data'][$v['title']] = number_format($v['total']);
					}
				}
				$moneyData['total'] = number_format($moneyData['+']['total'] - $moneyData['-']['total']);
				$moneyData['+']['total'] = number_format($moneyData['+']['total']);
				$moneyData['-']['total'] = number_format($moneyData['-']['total']);
				$dataList[$r['mtype']]['data'][$r['id']]['moneyData'] = $moneyData;
			}

			//$idList[$r['mtype']][] = "'".$r['id']."'";
		}
		$result['dataList'] = $dataList;
		/*foreach ($idList as $key => $value) {
			$result['sidList'][$key] = implode(',', $value);
		}*/

		return $result;
	}
?>