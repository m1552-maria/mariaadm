<?php
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";

	$db = new db();
	$sql = "select * from plan4 where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>瑪利亞預算管理系統</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
<style type="text/css">
  .cLabel {
    text-align: center;
    background-color: #EEE;
    border: 1px solid #333333;
    padding: 4px 8px 4px 4px;
  }
  .tableData {
  	margin: 0 auto;
  	border: 1px solid #00; border-collapse: collapse;
  }
  .tableData td{
  	text-align: center;
  	padding: 6px 0;
  }

</style>
</head>
<body>
	
	<h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3>
	<h3 style="text-align: center;">計劃表(四) - <?php echo $data['year'];?>年度人力編制表</h3>
	<input type="hidden" name="year" value="<?php echo $data['year'];?>">
	<input type="hidden" name="depID" value="<?php echo $data['depID'];?>">
	<input type="hidden" name="invNo" value="<?php echo $data['invNo'];?>">
	<input type="hidden" name="planID" value="<?php echo $data['planID'];?>">
	<input type="hidden" name="groupType" value="<?php echo $data['groupType'];?>">
	<table  width="1000px;" style="margin: 0 auto;">
		<tr><td style="font-weight: bold;" id="showTitle"></td></tr>
	</table>
	
	<table class="tableData" width="1000px;" border='1'>
	
	  <tr>
	    <td style="width:5%" rowspan="2" class="cLabel">類別</td>
	    <td style="width:10%" rowspan="2" class="cLabel">職稱</td>
	    <td style="width:8%" rowspan="2" class="cLabel">上年度<br>預算人力</td>
	    <td style="width:25%" colspan="3" class="cLabel">年度預算編制</td>
	    <td rowspan="2" class="cLabel">個案量<br>增減</td>
	    <td colspan="2" class="cLabel">預算人力</td>
	    <td style="width:35%" rowspan="2" class="cLabel">說明</td>
	  </tr>
	  <tr id="dataTr">
	    <td class="cLabel">個案量</td>
	    <td class="cLabel">比例分析</td>
	    <td class="cLabel">預算人力</td>
	    <td class="cLabel">擴編</td>
	    <td class="cLabel">縮編</td>
	  </tr>
	  
	  <tr>
	    <td class="cLabel" colspan="2">合計</td>
	    <td class="cLabel" data-sum="lastTotal"></td>
	    <td class="cLabel" colspan="2" data-sum="srvpeople"></td>
	    <td class="cLabel" data-sum="total"></td>
	    <td class="cLabel" data-sum="lastSrv"></td>
	    <td class="cLabel" data-sum="add"></td>
	    <td class="cLabel" data-sum="less"></td>
	    <td class="cLabel"></td>
	  </tr>

	</table>
	<div style="margin: 0 auto; width: 1000px;">
		<span style="float: right; margin: 8px 140px 0 0;">填表人: <?php echo $emplyeeinfo[$data['creator']]?></span>
		*依法編制：1.早療機構:社工1:30、教保1:5、行政人員或其他工作人員1:30
			<br><span style="margin-left: 88px;">2.日托機構:社工1:50、教保1:3至1:7</span>
			<br><span style="margin-left: 88px;">3.住宿機構:社工1:50、護理1:40、日間教保1:3至1:7、日間生活服務員1:3至1:6、夜間教保倂計生活服務員1:6至1:15(夜間</span><br><span style="margin-left: 88px;">值班至少配置護理人員或教保員一人)。</span>
		<br>*機構服務費補助:行政人員1:10(最高5人)、其他專業人員視需要設置。(行政人員包含:機構主管、事務、司機、廚務等)
		<br>*庇護性就業方案補助：1:6
	</div>


	<div style="text-align:center;margin-top:20px;">
		<button type="button" id="print_button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
	    <button type="button" id="print_close" onClick="window.close();">關閉</button>
	</div>
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="dataControl.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		ajaxCheck('print', <?php echo $_GET['id'];?>);
	})
	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
    }
</script>


</body>
</html>