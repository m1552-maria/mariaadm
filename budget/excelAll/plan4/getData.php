<?php

	function getPlan4Data($data = array()) {
		global $departmentinfo,$jobinfo, $invNo;
		$result = array('error'=>0,'msg'=>'','hasLast'=>1, 'notes'=>'');

		$db = new db();	
		$sql = "select * from plan where year='".$data['year']."'";
		$rs = $db->query($sql);
		$planTitleList = array(); 
		while($r=$db->fetch_array($rs)) {
			$planTitleList[$r['id']] = $r['title'];
		}

		$where = array();
		$where2 = '';
		if(!empty($data['depID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$depIDList = explode(',', $data['depID']);
			$_where = array();
			foreach ($depIDList as $key => $value) {
				$_where[] = " p.depID like '".$value."%' ";
				$showTitle[] = $departmentinfo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '機構/中心:'.implode(',', $showTitle);
			$where[] = $_where;
			$where2 = " depID='".$data['depID']."' and groupType='".$_POST['groupType']."'";
		}

		if(!empty($data['invNo'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$invNoList = explode(',', $data['invNo']);
			$_where = array();
			foreach ($invNoList as $key => $value) {
				$_where[] = " p.invNo like '".$value."%' ";
				$showTitle[] = $invNo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '統一編號:'.implode(',', $showTitle);
			$where[] = $_where;
			$where2 = " invNo='".$data['invNo']."' and groupType='".$_POST['groupType']."'";
		}

		if(!empty($data['planID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$planList = explode(',', $data['planID']);
			$_where = array();
			foreach ($planList as $key => $value) {
				$showTitle[] = $planTitleList[$value];
				$planList[$key] = "'".$value."'";
			}
			$_where = "p.id in (".implode(',', $planList).")";
			$result['showTitle'][] = '計劃主檔:'.implode(',', $showTitle);
			$where[] = $_where;
			$where2 = " planID='".$data['planID']."' and groupType='".$_POST['groupType']."'";
		}

		$result['showTitle'] = implode(' / ', $result['showTitle']);

		$where = implode(' and ', $where);


		$db = new db();	
		/*抓已存在的資料*/
		if(!empty($data['id'])) {
			$sql = "select * from plan4 where id = '".$data['id']."'";
			$rs = $db->query($sql);
			$plan4Data=$db->fetch_array($rs);
		} else {
			$sql = "select * from plan4 where year='".$data['year']."' and ".$where2;
			$rs = $db->query($sql);
			if($db->num_rows($rs) > 0) $plan4Data=$db->fetch_array($rs);
		}

		/*抓已存在的資料*/
		$sql = "select * from plan4 where year='".($data['year']-1)."' and ".$where2;
		$rs = $db->query($sql);
		if($db->num_rows($rs) > 0) $plan4LastData = $db->fetch_array($rs);

		/*先取設定年度的人力*/
		$sql = "select t.jobID, sum(t.rate) as total, p.bugetChange from plan p inner join plan_talent t on p.id=t.fid where p.year='".$data['year']."'";
		$sql .= " and ".$where;
		$sql .= " group by t.jobID order by t.jobID ";

		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) { 
			$result['error'] = 1;
			$result['msg'] = ($data['year']).'年 所選'.$result['showTitle'].'  未存在計畫or人力';
			return $result;
		}
			

		$dataList = array(
			'G'=>array('title'=>'主管', 'data'=>array()),
			'A'=>array('title'=>'社工', 'data'=>array()),
			'B'=>array('title'=>'教保生服', 'data'=>array()),
			'C'=>array('title'=>'職業重建', 'data'=>array()),
			'D'=>array('title'=>'醫療', 'data'=>array()),
			'E'=>array('title'=>'行政', 'data'=>array()),
			'F'=>array('title'=>'技術', 'data'=>array())
		);

		while($r=$db->fetch_array($rs)) {
			if($r['bugetChange'] == 2) $r['total'] = -1*$r['total']; //追減
			$k = substr($r['jobID'],0,1);
			if(!isset($dataList[$k])) $k = 'F';
			$jobID = $r['jobID'];
			if($r['jobID'] == 'B001') {
				$dataList[$k]['data'][$r['jobID']]['title'] = $jobinfo[$r['jobID']]. '(早療)';
				$dataList[$k]['data'][$r['jobID']]['total'] = 0;
				$dataList[$k]['data'][$r['jobID']]['lastTotal'] = 0;
				$jobID = 'B001_no';
			}

			$dataList[$k]['data'][$jobID]['title'] = ($r['jobID'] == 'B001') ? $jobinfo[$r['jobID']].'(非早療)' : $jobinfo[$r['jobID']];
			$dataList[$k]['data'][$jobID]['total'] = $r['total']/100;
			$dataList[$k]['data'][$jobID]['lastTotal'] = 0;	
		}

		/*取設定年度前一年的人力*/
		$sql = "select t.jobID, sum(t.rate) as total from plan p inner join plan_talent t on p.id=t.fid where p.year='".($data['year']-1)."'";
		$sql .= " and ".$where;
		$sql .= " group by t.jobID order by t.jobID ";
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) {
			/*foreach ($dataList as $key => $value) {
				foreach ($value['data'] as $k2 => $v2) {
					$dataList[$key]['data'][$k2]['lastTotal'] = -1;
				}
			}*/
			$result['hasLast'] = 0;

		} else{
			while($r=$db->fetch_array($rs)) {
				$k = substr($r['jobID'],0,1);
				$jobID = ($r['jobID'] == 'B001') ? 'B001_no' : $r['jobID'];
				if(!isset($dataList[$k])) $k = 'F';
				if(!isset($dataList[$k]['data'][$r['jobID']])) {
					if($r['jobID'] == 'B001') {
						$dataList[$k]['data'][$r['jobID']]['title'] = $jobinfo[$r['jobID']]. '(早療)';
						$dataList[$k]['data'][$r['jobID']]['total'] = '';
						$dataList[$k]['data'][$r['jobID']]['lastTotal'] = 0;
						//$jobID = 'B001_no';
					}

					$dataList[$k]['data'][$jobID]['title'] = ($r['jobID'] == 'B001') ? $jobinfo[$r['jobID']].'(非早療)' : $jobinfo[$r['jobID']];
					$dataList[$k]['data'][$jobID]['total'] = 0;
				}
				$dataList[$k]['data'][$jobID]['lastTotal'] = $r['total']/100;
			}
		}


		/*取得個案量 (計畫為機構型 確服務單位為"人" 的服務量)*/
		$sql = "select sum(i.srvpeople) as srvpeople from plan p inner join plan_items i on p.id=i.pid where i.isPlan4='1' and i.srvunit='人' and p.year='".$data['year']."' and ".$where;

		$rs = $db->query($sql);
		$r=$db->fetch_array($rs);
		$result['srvpeople'] = ($r['srvpeople']) ? $r['srvpeople'] : 0;

		$sql = "select sum(i.srvpeople) as srvpeople from plan p inner join plan_items i on p.id=i.pid where i.isPlan4='1' and i.srvunit='人' and p.year='".($data['year']-1)."' and ".$where;
		$rs = $db->query($sql);
		$r=$db->fetch_array($rs);
		$result['lastSrv'] = ($r['srvpeople']) ? $result['srvpeople'] - $r['srvpeople'] : '';
		if($result['srvpeople'] == 0) $result['srvpeople'] = '';


		/*若在plan4有存在值要額外處理*/
		if(isset($plan4Data)) {
			if($plan4Data['lastData'] != '' && $result['hasLast'] == 0) {
				$lastList = explode(',', $plan4Data['lastData']);
				foreach ($lastList as $key => $value) {
					$_last = explode(':', $value);
					$k = substr($_last[0],0,1);
					if(!isset($dataList[$k])) $k = 'F';
					$dataList[$k]['data'][$_last[0]]['lastTotal'] = $_last[1];
					$dataList[$k]['data'][$_last[0]]['total'] += 0;
					switch ($_last[0]) {
						case 'B001_no':
							$title = $jobinfo['B001']. '(非早療)';
							break;
						case 'B001':
							$title = $jobinfo['B001']. '(早療)';
							break;
						default:
							$title = $jobinfo[$_last[0]];
							break;
					}
					$dataList[$k]['data'][$_last[0]]['title'] = $title;
				}	
			}
		
			if(isset($dataList['B']['data'])) $bData = $dataList['B']['data'];
			if($plan4Data['B001_num'] != '' && isset($bData['B001']) && ($plan4Data['B001_num']+$plan4Data['B001_no_num']) == $bData['B001_no']['total']) {
				$dataList['B']['data']['B001']['total'] = $plan4Data['B001_num'];
				$dataList['B']['data']['B001']['pre'] = $plan4Data['B001_early'];
				$dataList['B']['data']['B001_no']['total'] = $plan4Data['B001_no_num'];
				$dataList['B']['data']['B001_no']['pre'] = $plan4Data['B001_no_early'];
			}

			if($data['act'] == 'print') {
				$result['notes'] = str_replace(' ','&nbsp;',$plan4Data['notes']);
				$result['notes'] = str_replace(chr(13).chr(10), "<br />",$result['notes']);
			} else {
				$result['notes'] = $plan4Data['notes'];
			}

		}

		if(isset($plan4LastData)) {
			if(isset($dataList['B']['data'])) $bData = $dataList['B']['data'];
			if($plan4LastData['B001_num'] != '' && isset($bData['B001']) && ($plan4LastData['B001_num']+$plan4LastData['B001_no_num']) == $bData['B001_no']['lastTotal']) {
				$dataList['B']['data']['B001']['lastTotal'] = $plan4LastData['B001_num'];
				$dataList['B']['data']['B001']['pre'] = $plan4LastData['B001_early'];
				$dataList['B']['data']['B001_no']['lastTotal'] = $plan4LastData['B001_no_num'];
				$dataList['B']['data']['B001_no']['pre'] = $plan4LastData['B001_no_early'];
			}
		}

		foreach ($dataList as $key => $value) {
			if(count($value['data']) == 0) unset($dataList[$key]);
			else $dataList[$key]['con'] = count($value['data']);
		}

		$result['con'] = 0;
		$result['total'] = 0;
		$result['lastTotal'] = 0;
		$isFirstK = '';
		foreach ($dataList as $key => $value) {
			if($isFirstK == '') $isFirstK = $key;
			if($dataList[$key]['con'] >= 2) $dataList[$key]['con']++;
			$result['con'] += $dataList[$key]['con'];
			foreach ($value['data'] as $k2 => $v2) {
				$result['total'] += $v2['total'];
				$result['lastTotal'] += $v2['lastTotal'];
			}	
		}

		if($dataList[$isFirstK]['con'] > 1) $result['con'] -= 1;
		//if($result['con'] > count($dataList)) $result['con'] -= 1;
		if($data['act'] == 'add') $result['notes'] = '全部工時  人'.chr(13).chr(10).'部分工時  人';
		$result['dataList'] = $dataList;
		return $result;

	}
?>