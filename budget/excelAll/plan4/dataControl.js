
$(function(){
    $(document).on('keyup', '[data-last]', function(){
      var k = $(this).attr('data-last');
      var totalVal = ($('[data-total="'+k+'"]').prop('tagName') == 'TD') ? $('[data-total="'+k+'"]').html()　: $('[data-total="'+k+'"]').val();

      var result = calculation(totalVal, $(this).val());
      $('[data-add="'+k+'"]').html(result['add']);
      $('[data-less="'+k+'"]').html(result['less']);
      calculationSum();
    });

    $(document).on('keyup', '[name="B001_num"]', function(){
      if($(this).val() == '') $(this).val(0);
      var val = parseFloat($(this).val()) * 100;
      var showVal = (B001Total-val)/100;
      $('[data-total-span="B001_no"]').html((showVal));
      $('[name="B001_no_num"]').val((showVal));

      kList = ['B001','B001_no'];
      for(var i in kList) {
        var k = kList[i];
        var lastVal = ($('[data-last="'+k+'"]').prop('tagName') == 'TD') ? $('[data-last="'+k+'"]').html()　: $('[data-last="'+k+'"]').val();
        var result = calculation($('[data-total="'+k+'"]').val(), lastVal);
        $('[data-add="'+k+'"]').html(result['add']);
        $('[data-less="'+k+'"]').html(result['less']);
      }
      calculationSum();
    })

    $(document).on('keyup', '[data-last]', function(){
    	var lastTotal = 0;
    	$('[data-last]').each(function(){
    		lastTotal+= parseFloat($(this).val().toPrecision(12));
    	});
    	$('[data-sum="lastTotal"]').html(lastTotal)
    })
  })


  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '' && $('[name="planID"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    $('[data-tr]').remove();
    $('[data-sum]').html('');
    $('[name="Submit"]').attr('disabled','disabled');
  }

  /*計算擴編縮編*/
  function calculation(total, lastTotal) {
    var result = {'add':0,'less':0};
    var t = parseFloat((parseFloat(total) - parseFloat(lastTotal)).toPrecision(12));
    if(t > 0) result['add'] = t;
    else if(t < 0) result['less'] = Math.abs(t);

    return result
  }

  /*計算總擴編縮編*/
  function calculationSum() {
    var result = {'sumAdd':0,'sumLess':0};
    $('[data-add]').each(function(){
      result['sumAdd'] = floatAdd(result['sumAdd'], parseFloat($(this).html()));

      //result['sumAdd']+= parseFloat((parseFloat($(this).html())).toPrecision(12));
    });
    $('[data-less]').each(function(){
      result['sumLess'] = floatAdd(result['sumLess'], parseFloat($(this).html()));
      //result['sumLess']+= parseFloat((parseFloat($(this).html())).toPrecision(12));
    });

    $('[data-sum="add"]').html(result['sumAdd']);
    $('[data-sum="less"]').html(result['sumLess']);
  }

  //浮點數轉換
  var floatAdd = function (arg1, arg2) {
    var r1, r2, m;
    try {
        r1 = arg1.toString().split(".")[1].length;
    } catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    } catch (e) {
        r2 = 0;
    }
    m=Math.pow(10,Math.max(r1,r2))  
    return Math.round((arg1*m+arg2*m))/m;
};

  var isFirst = 0;
  var html = [];
  var B001Total = 0;
  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        planID:$('[name="planID"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
          $('[name="depID"]').change();
        } else {
          $('#showTitle').html(data['showTitle']);
          isFirst = 0;
          $('[data-tr]').remove();
          html = [];
          if(data['dataList']['G']) {
            setData('G', data, act);
          }
         
          for(var j in data['dataList']) {
            if(j == 'G') continue;
            setData(j, data, act);
          }

          $('#dataTr').after(html.join(''));

          if(act != 'print') {
          	/*for(var instanceName in CKEDITOR.instances) {
	            CKEDITOR.remove(CKEDITOR.instances[instanceName]);   
	            //CKEDITOR.replaceAll(); 
  	        }
  	        CKEDITOR.replace('notes');*/
  	        $('[name="Submit"]').removeAttr('disabled');
          }
 

          calculationSum();
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(key, data, act) {
    var dataList = data['dataList'];
    dataList[key]['con'] = parseInt(dataList[key]['con']);
    html.push('<tr data-tr=""><td class="colData" rowspan="'+dataList[key]['con']+'">'+dataList[key]['title']+'</td>');
    if(dataList[key]['con'] > 1) html.push('</tr>');
    var gData = dataList[key]['data'];

    if(key == 'B' && gData['B001']){
      B001Total = 100 * parseFloat(gData['B001']['total']) + 100 * parseFloat(gData['B001_no']['total']);
    }
    for(var i in gData) {
      if(dataList[key]['con'] > 1)  html.push('<tr data-tr="">');
       //職稱
      html.push('<td class="colData">'+gData[i]['title']+'</td>');

      //上年度
      if(data['hasLast'] == 0 && act != 'print') html.push('<td class="colData"><input data-last="'+i+'" style="width:45px;" type="text" name="last['+i+']" value="'+gData[i]['lastTotal']+'"></td>');
      else html.push('<td data-last="'+i+'" class="colData">'+gData[i]['lastTotal']+'</td>'); 

      //個案量
      if(isFirst == 0) html.push('<td class="colData" rowspan="'+data['con']+'">'+data['srvpeople']+'</td class="colData">'); 
      
      //比例分析
      var pro = '';
      if(data['srvpeople'] != '' && gData[i]['total'] != 0) { //比例分析
        data['srvpeople'] = parseInt(data['srvpeople']);
        pro = Math.ceil(data['srvpeople'] / Math.ceil(gData[i]['total']));
      }
      if(pro != '') pro = '1:'+pro;
      if(gData[i]['pre']) pro = gData[i]['pre'];
      if((i == 'B001' || i == 'B001_no') && act != 'print') {
        pro = '<input style="width:50px;" name="'+i+'_early" value="'+pro+'">'
      }

      html.push('<td class="colData">'+pro+'</td>');

      //預算人力
      var dTotal = '';
      if((i == 'B001' || i == 'B001_no') && act != 'print') {
        if(i == 'B001') {
          dTotal = '<input data-total="'+i+'" style="width:50px;" name="'+i+'_num" value="'+gData[i]['total']+'">';
        } else {
          dTotal = '<span data-total-span="'+i+'">'+gData[i]['total']+'</span>';
          dTotal += '<input data-total="'+i+'" name="'+i+'_num" type="hidden" value="'+gData[i]['total']+'">';
        }
        html.push('<td class="colData">'+dTotal+'</td>');
      } else {
        html.push('<td data-total="'+i+'" class="colData">'+gData[i]['total']+'</td>');
      }


      //個案量增減
      if(isFirst == 0) html.push('<td class="colData" rowspan="'+data['con']+'">'+data['lastSrv']+'</td>'); 
      re = calculation(gData[i]['total'], gData[i]['lastTotal']);
      //擴編
      html.push('<td data-add="'+i+'" class="colData">'+ re['add']+'</td>');

      //縮編
      html.push('<td data-less="'+i+'" class="colData">'+ re['less']+'</td>');

      //說明
      if(isFirst == 0) {
      	var nData = data['notes'];
      	if(act != 'print') nData = '<textarea id="notes" rows="'+(data['con']+2)+'" name="notes" style="width:95%">'+nData+'</textarea></td>';
      	else nData ='<div style="text-align: left;padding-left: 5px;">'+nData+'</div>';
      	html.push('<td class="colData" rowspan="'+data['con']+'">'+nData+'</td>');
	  }
      if(dataList[key]['con'] > 1) html.push('</tr>');
      isFirst = 1;
    }

    if(dataList[key]['con'] > 1) html.push('</tr>');

    $('[data-sum="lastTotal"]').html(data['lastTotal']);
    $('[data-sum="total"]').html(data['total']);
    $('[data-sum="srvpeople"]').html(data['srvpeople']);
    $('[data-sum="lastSrv"]').html(data['lastSrv']);
  }


