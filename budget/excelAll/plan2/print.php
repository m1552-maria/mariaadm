<?php
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";

	$db = new db();
	$sql = "select * from plan2 where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>瑪利亞預算管理系統</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
<style type="text/css">
  .cLabel {
    text-align: center;
    background-color: #EEE;
    border: 1px solid #333333;
    padding: 4px 8px 4px 4px;
  }
  .tableData {
  	margin: 0 auto;
  	border: 1px solid #00; border-collapse: collapse;
  }
  .tableData td{
  	text-align: center;
  	padding: 6px 0;
  }

  .slash{
    position:relative;
    width:90px;
    height:40px;
    box-sizing:border-box;
    line-height:120px;
    background: linear-gradient(45deg, transparent 49.5%, #212121 49.5%, transparent 50.5%, transparent 50.5%);
    left: -20px;
  }

  .slash01{
    position: absolute;
    top: -50px;
    left: 50px;
  }
  .slash02{
    position: absolute;
    top: -25px;
    left: 20px;

  }
  .notes {
  	text-align: left;
  	padding: 5px;
  }
</style>
</head>
<body>
	
	<h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3>
	<h3 style="text-align: center;">計劃表(二) - <?php echo $data['year'];?>年度計畫/預算總表</h3>
	<input type="hidden" name="year" value="<?php echo $data['year'];?>">
	<input type="hidden" name="depID" value="<?php echo $data['depID'];?>">
	<input type="hidden" name="invNo" value="<?php echo $data['invNo'];?>">
	<input type="hidden" name="planID" value="<?php echo $data['planID'];?>">
	<input type="hidden" name="groupType" value="<?php echo $data['groupType'];?>">
	<table  width="1000px;" style="margin: 0 auto;">
		<tr><td style="font-weight: bold;" id="showTitle"></td></tr>
	</table>
	
	<table class="tableData" width="1000px;" border='1'>
	  <tr id="tabSrv">
	    <th rowspan="1" class="cLabel" style="width:22px;">個案服務總量</th>
	    <th class="cLabel" style="width:80px;">
	      <div class="slash">
	        <div class='slash01'>個案</div>
	        <div class='slash02'>類型</div>
	      </div>
	    </th>
	    <th class="cLabel">服務項目/名稱</th>
	    <th style="width: 25%" class="cLabel">上年度服務量</th>
	    <th style="width: 25%" class="cLabel">本年度服務量</th>
	  </tr>
	</table>
	<table class="tableData" width="1000px;" border='1'>
		<tr id="tabPlan4">
		    <th style="width:22px;" rowspan="3" class="cLabel" >人力需求量</th>
		    <th style="width: 15%" class="cLabel">上年度</th>
		    <th style="width: 15%" class="cLabel">年度增減</th>
		    <th class="cLabel">本年度預計</th>
		    <th style="width: 50%" class="cLabel">人力擴編之方案說明</th>
 		</tr>
	</table>
	<table class="tableData" width="1000px;" border='1'>
		<tr id="tabBudget1">
		    <th style="width:22px;" rowspan="4" class="cLabel" >總預算</th>
		    <th style="width: 15%" class="cLabel">預算項目</th>
		    <th style="width: 15%" class="cLabel">上年度(A)</th>
		    <th class="cLabel">本年度(B)</th>
		    <th style="width: 15%" class="cLabel">年度增減<br>(C=B-A)</th>
		    <th style="width: 35%" class="cLabel">預算增減之主要方案說明</th>
		</tr>
	</table>

	<div style="text-align:center;margin-top:20px;">
		<button type="button" id="print_button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
	    <button type="button" id="print_close" onClick="window.close();">關閉</button>
	</div>
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="dataControl.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		ajaxCheck('print', <?php echo $_GET['id'];?>);
	})
	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
    }
</script>


</body>
</html>