
$(function(){
   
  
})


  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '' && $('[name="planID"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    //$('[name="depID"]').val(''); 
    $('[data-tr]').remove();
    $('[name="Submit"]').attr('disabled','disabled');
    $('#tabSrv th:first').attr('rowspan',1);
  }





  var html = [];
  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        planID:$('[name="planID"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        console.log(data);
        dataClear();
        if(data['error'] == 1) {
          alert(data['msg']);
          dataClear();
        } else {
          $('#showTitle').html(data['showTitle']);
          setData(data, act);
          $('[name="Submit"]').removeAttr('disabled');
         /* if(act != 'print') {
          	for(var instanceName in CKEDITOR.instances) {
	            CKEDITOR.remove(CKEDITOR.instances[instanceName]);   
	            //CKEDITOR.replaceAll(); 
  	        }
  	        CKEDITOR.replace('notes');
  	        $('[name="Submit"]').removeAttr('disabled');
          }*/
 
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(dataList, act) {
    var items = dataList['itemList'];
    var html = [];
    var itemsCon = 0;
    var noLast = dataList['noLast'];
    for(var i in items) {
      var z=0;
      var data = items[i]['data'];
      itemsCon+=parseInt(items[i]['con']+1);
      for(var j in data) {
        z++;
        html.push('<tr data-tr>');
        if(z == 1)  html.push('<td class="colData" rowspan="'+(parseInt(items[i]['con'])+1)+'">'+items[i]['name']+'</td>');
          
        html.push('<td class="colData">'+j+'</td>');
        
        /*上年度服務量*/
        var text = [];
        var srvpeople = (data[j]['last']) ? data[j]['last']['srvpeople'] : '';
        var srvunit = (data[j]['last']) ?　data[j]['last']['srvunit']　: '';
        if(noLast == 1 && act != 'print') {
          text.push('<input min="0" style="width:60px;" name="srvpeople['+i+']['+j+']" type="number" value="'+srvpeople+'" />');
          text.push('<select name="srvunit['+i+']['+j+']">');
          for(var n in srvunitList) {
            var selected = (n == srvunit) ? 'selected' : '';
            text.push('<option '+selected+' value="'+n+'">'+n+'</option>');
          }
          text.push('</select>');
        } else {
          text.push(srvpeople);
          text.push(srvunit);
        }
      
        html.push('<td class="colData">'+text.join('')+'</td>');


        var text = '';
        if(data[j]['now']) {
          text = data[j]['now']['srvpeople']+data[j]['now']['srvunit'];
        } 

        html.push('<td class="colData">'+text+'</td>');
        html.push('</tr>');
      }

      var srvunit = items[i]['srvunit'];
      var srvunitLast = [];
      var srvunitNow = [];
      for(var j in srvunit) {
        if(srvunit[j]['last']) srvunitLast.push(srvunit[j]['last']+j);
        if(srvunit[j]['now']) srvunitNow.push(srvunit[j]['now']+j);
      }

      html.push('<tr data-tr>');
      html.push('<td class="colData">小計</td>');
      html.push('<td class="colData">'+srvunitLast.join(' ')+'</td>');
      html.push('<td class="colData">'+srvunitNow.join(' ')+'</td>');
      html.push('</tr>');
    }

    $('#tabSrv').after(html.join(''));
    $('#tabSrv th:first').attr('rowspan',itemsCon+1);

    var plan4Html = [];
    plan4Html.push('<tr data-tr>');
    plan4Html.push('<td rowspan="2" class="colData">'+dataList['plan4Data']['last']+'人力</td>');
    var plan4Total =(parseFloat(dataList['plan4Data']['now']) - parseFloat(dataList['plan4Data']['last'])).toPrecision(12);

    var totalText = (plan4Total >=0) ? '擴編' : '縮編';
    plan4Html.push('<td class="colData">'+totalText+'</td>');
  //  plan4Html.push('<td class="colData">小計</td>');
    plan4Html.push('<td rowspan="2" class="colData">'+dataList['plan4Data']['now']+'人力</td>');

    plan4Html.push('<td rowspan="2" class="colData">');
    if(act != 'print') {
      plan4Html.push('<textarea name="tNotes" rows="4" style="width:95%">'+dataList['tNotes']+'</textarea>')
    } else {
      plan4Html.push('<div class="notes">'+dataList['tNotes']+'</div>');
    }
    plan4Html.push('</td>');

    plan4Html.push('</tr>');

    plan4Html.push('<tr data-tr>');
    plan4Html.push('<td class="colData">'+Math.abs(plan4Total)+'</td>');
   // plan4Html.push('<td class="colData">0</td>');
    plan4Html.push('</tr>');

    $('#tabPlan4').after(plan4Html.join(''));

    var b1Html = [];
    var incomeTotal = parseInt(dataList['budget1Data']['nowIncome'])-parseInt(dataList['budget1Data']['lastIncome']);
    b1Html.push('<tr data-tr>');
    b1Html.push('<td class="colData">總收入</td>');
    b1Html.push('<td class="colData">'+toCurrency(dataList['budget1Data']['lastIncome'])+'</td>');
    b1Html.push('<td class="colData">'+toCurrency(dataList['budget1Data']['nowIncome'])+'</td>');
    b1Html.push('<td class="colData">'+toCurrency(incomeTotal)+'</td>');

    b1Html.push('<td rowspan="3" class="colData">');
    if(act != 'print') {
      b1Html.push('<textarea name="bNotes" rows="6" style="width:95%">'+dataList['bNotes']+'</textarea>')
    } else {
      b1Html.push('<div class="notes">'+dataList['bNotes']+'</div>');
    }
    b1Html.push('</td>');
    b1Html.push('</tr>');

    var expendTotal = parseInt(dataList['budget1Data']['nowExpend'])-parseInt(dataList['budget1Data']['lastExpend']);
    b1Html.push('<tr data-tr>');
    b1Html.push('<td class="colData">總支出</td>');
    b1Html.push('<td class="colData">'+toCurrency(dataList['budget1Data']['lastExpend'])+'</td>');
    b1Html.push('<td class="colData">'+toCurrency(dataList['budget1Data']['nowExpend'])+'</td>');
    b1Html.push('<td class="colData">'+toCurrency(expendTotal)+'</td>');
    b1Html.push('</tr>');

    var bLastTotal = parseInt(dataList['budget1Data']['lastIncome'])-parseInt(dataList['budget1Data']['lastExpend']);
    var bNowTotal = parseInt(dataList['budget1Data']['nowIncome'])-parseInt(dataList['budget1Data']['nowExpend']);
    b1Html.push('<tr data-tr>');
    b1Html.push('<td class="colData">總餘絀(實際)</td>');
    b1Html.push('<td class="colData">'+toCurrency(bLastTotal)+'</td>');
    b1Html.push('<td class="colData">'+toCurrency(bNowTotal)+'</td>');
    b1Html.push('<td class="colData">'+toCurrency(incomeTotal - expendTotal)+'</td>');
    b1Html.push('</tr>');

    $('#tabBudget1').after(b1Html.join(''));
  }


function toCurrency(num){
    var parts = num.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
}