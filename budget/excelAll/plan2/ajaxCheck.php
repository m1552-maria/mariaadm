<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	
	$result = array('error'=>0,'msg'=>'');

	if(empty($_POST['year'])) {
		$result['error'] = 1;
		$result['msg'] = '資料錯誤';
		echo json_encode($result);
	}

	include_once($root."/system/db.php");

	$db = new db();	

	$sql = "select * from plan where year='".$_POST['year']."'";
	$rs = $db->query($sql);
	$planTitleList = array(); 
	while($r=$db->fetch_array($rs)) {
		$planTitleList[$r['id']] = $r['title'];
	}


	$where = array();
	$showTitle = array();

	if($_POST['groupType'] == 'depID' && !empty($_POST['depID'])) {
		$show = array();
		$_where = " depID = '".$_POST['depID']."'";
		$dList = explode(',', $_POST['depID']);
		foreach ($dList as $key => $value) $show[] = $departmentinfo[$value];
		$show = '部門:' .implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	if($_POST['groupType'] == 'invNo' && !empty($_POST['invNo'])) {
		$show = array();
		$_where = " invNo = '".$_POST['invNo']."'";
		$dList = explode(',', $_POST['invNo']);
		foreach ($dList as $key => $value) $show[] = $invNo[$value];
		$show = '統一編號:'.implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	if($_POST['groupType'] == 'planID' && !empty($_POST['planID'])) {
		$show = array();
		$_where = " planID = '".$_POST['planID']."'";
		$dList = explode(',', $_POST['planID']);
		foreach ($dList as $key => $value) $show[] = $planTitleList[$value];
		$show = '計劃主檔:'.implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	$where[] = " groupType = '".$_POST['groupType']."'";
	$showTitle = implode(' / ', $showTitle);
	$where = implode(' and ', $where);


	if($_POST['act'] == 'add') {
		$sql = "select 1 from plan2 where year='".$_POST['year']."' and ".$where;
		$rs = $db->query($sql);
		if($db->num_rows($rs) > 0) {
			$result['error'] = 1;
			$result['msg'] = ($_POST['year']).'年 所選'.$showTitle.' 計劃表(二)已經存在';
			echo json_encode($result);
			exit;
		}
	} else {
		$sql = "select 1 from plan2 where year='".$_POST['year']."' and ".$where;
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) {
			$result['error'] = 1;
			$result['msg'] = ($_POST['year']).'年 所選'.$showTitle.' 計劃表(二)不存在';
			echo json_encode($result);
			exit;
		}
	}
	
	//$data = array('year'=>$_POST['year'],'depID'=>$_POST['depID'], 'act'=>$_POST['act']);
	$data = array_merge(array(), $_POST);
	if(isset($data['id'])) unset($data['id']);
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/plan4/getData.php");
	$plan4Data = getPlan4Data($data);

	if($plan4Data['error'] == 1) {
		$result = $plan4Data;
		echo json_encode($result);
		exit;
	}

	include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/budget1/getData.php");
	$budget1Data = getBudget1Data($data);
	if($budget1Data['error'] == 1) {
		$result = $budget1Data;
		echo json_encode($result);
		exit;
	}


	$bData = array(
		'lastIncome'=>0,
		'lastExpend'=>0,
		'nowIncome'=>0,
		'nowExpend'=>0
	);
	foreach ($budget1Data['dataList'] as $key => $value) {
		if($value['vType'] == '+') {
			$bData['lastIncome'] += $value['lastTotal'];
			$bData['nowIncome'] += $value['total'];

		} elseif($value['vType'] == '-' && $key != 'R60') {
			$bData['lastExpend'] += $value['lastTotal'];
			$bData['nowExpend'] += $value['total'];
		}
	}


	include_once('getData.php');

	//$data['id'] = $_POST['id'];
	$result = getPlan2Data($_POST);

	$result['plan4Data'] = array('last'=>$plan4Data['lastTotal'], 'now'=>$plan4Data['total']);
	$result['budget1Data'] = $bData;
	echo json_encode($result);



?>