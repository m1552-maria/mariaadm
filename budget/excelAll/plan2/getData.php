<?php

	function getPlan2Data($data = array()) {
		global $departmentinfo, $invNo;
		$result = array('error'=>0,'msg'=>'', 'noLast'=>1, 'tNotes'=>'', 'bNotes'=>'', 'itemList'=>array());

		$db = new db();	
		$sql = "select * from plan where year='".$data['year']."'";
		$rs = $db->query($sql);
		$planTitleList = array(); 
		while($r=$db->fetch_array($rs)) {
			$planTitleList[$r['id']] = $r['title'];
		}

		$where = array();
		if(!empty($data['depID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$depIDList = explode(',', $data['depID']);
			$_where = array();
			foreach ($depIDList as $key => $value) {
				$_where[] = " p.depID like '".$value."%' ";
				$showTitle[] = $departmentinfo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '機構/中心:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['invNo'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$invNoList = explode(',', $data['invNo']);
			$_where = array();
			foreach ($invNoList as $key => $value) {
				$_where[] = " p.invNo like '".$value."%' ";
				$showTitle[] = $invNo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '統一編號:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['planID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$planList = explode(',', $data['planID']);
			$_where = array();
			foreach ($planList as $key => $value) {
				$showTitle[] = $planTitleList[$value];
				$planList[$key] = "'".$value."'";
			}
			$_where = "p.id in (".implode(',', $planList).")";
			$result['showTitle'][] = '計劃主檔:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		$result['showTitle'] = implode(' / ', $result['showTitle']);

		$where = implode(' and ', $where);

		/*抓已存在的資料*/
		if(!empty($data['id'])) {
			$sql = "select * from plan2 where id = '".$data['id']."'";
			$rs = $db->query($sql);
			$plan2Data=$db->fetch_array($rs);
		}

		$sql = "select * from plan_stringmap where isReady='1'";
		$rs = $db->query($sql);
		$mTypeList = array();
		while($r=$db->fetch_array($rs)) {
			$mTypeList[$r['id']] = $r['title'];
		}


		//$sql = "select 1 from plan p inner join plan_items i on p.id=i.pid where i.isPlan2=1 and p.year='".$data['year']."' and p.depID like '".$data['depID']."%' ";

		$sql = "select 1 from plan p inner join plan_items i on p.id=i.pid where i.isPlan2=1 and p.year='".$data['year']."'";
		$sql .= ' and '. $where;

		//if(!$data['addChange']) $sql .= " and p.bugetChange = '0' ";//排除追加減的部份, 只要本文
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) { 
			$result['error'] = 1;
			$result['msg'] = ($data['year']).'年 所選'.$result['showTitle'].' 未存在計畫or人力';
			return $result;
		}

		//$sql = "select i.*, p.year from plan p inner join plan_items i on p.id=i.pid where i.isPlan2=1 and (p.year='".$data['year']."' or p.year='".($data['year']-1)."') and p.depID like '".$data['depID']."%' "; 
		$sql = "select i.*, p.year, p.bugetChange from plan p inner join plan_items i on p.id=i.pid where i.isPlan2=1 and (p.year='".$data['year']."' or p.year='".($data['year']-1)."') "; 
		$sql .= ' and '. $where;
		//if(!$data['addChange']) $sql .= " and p.bugetChange = '0' ";//排除追加減的部份, 只要本文
		$sql .= " order by i.mtype, i.pid, i.sort";
		$rs = $db->query($sql);
		$itemList = array();
		while($r=$db->fetch_array($rs)) {
			$key = ($r['year'] == $data['year']) ? 'now' : 'last';
			/*if(!isset($itemList[$r['mtype']]['con'])) $itemList[$r['mtype']]['con'] = 0;
			$itemList[$r['mtype']]['con']++;*/
			$itemList[$r['mtype']]['name'] = $mTypeList[$r['mtype']];
			if($r['bugetChange'] == 2) $r['srvpeople']*-1;
			if(!isset($itemList[$r['mtype']]['data'][$r['title']][$key])) {
				$itemList[$r['mtype']]['data'][$r['title']][$key] = array(
					'srvpeople' => 0,
					'srvunit' => $r['srvunit']
				);
			}
			$itemList[$r['mtype']]['data'][$r['title']][$key]['srvpeople'] += $r['srvpeople'];

			if(!isset($itemList[$r['mtype']]['srvunit'][$r['srvunit']][$key])) $itemList[$r['mtype']]['srvunit'][$r['srvunit']][$key] = 0;
			$itemList[$r['mtype']]['srvunit'][$r['srvunit']][$key] += $r['srvpeople'];
			if($r['year'] < $data['year']) $result['noLast'] = 0;
		}

		foreach ($itemList as $key => $value) {
			$itemList[$key]['con'] = count($itemList[$key]['data']);
		}

		/*存在自填入的去年資料*/
		if(!empty($plan2Data['lastData'])) {
			$lData = explode('&,&', $plan2Data['lastData']);
			foreach ($lData as $key => $value) {
				$d = explode('&:&', $value);
				if(isset($itemList[$d[0]]['data'][$d[1]])) {
					$itemList[$d[0]]['data'][$d[1]]['last'] = array(
						'srvpeople' => $d[2],
						'srvunit' => $d[3]
					);
					$itemList[$d[0]]['srvunit'][$d[3]]['last'] = 0;
				}
			}

			foreach ($itemList as $key => $value) {
				foreach ($value['data'] as $k => $v) {
					$itemList[$key]['srvunit'][$v['last']['srvunit']]['last'] += $v['last']['srvpeople'];
				}
			}
		}

		if(!empty($plan2Data['tNotes'])) {
			if($data['act'] == 'print') {
				$result['tNotes'] = str_replace(' ','&nbsp;',$plan2Data['tNotes']);
				$result['tNotes'] = str_replace(chr(13).chr(10), "<br />",$result['tNotes']);
			} else {
				$result['tNotes'] = $plan2Data['tNotes'];
			}
		}

		if(!empty($plan2Data['bNotes'])) {
			if($data['act'] == 'print') {
				$result['bNotes'] = str_replace(' ','&nbsp;',$plan2Data['bNotes']);
				$result['bNotes'] = str_replace(chr(13).chr(10), "<br />",$result['bNotes']);
			} else {
				$result['bNotes'] = $plan2Data['bNotes'];
			}
		}


		$result['itemList'] = $itemList;
		return $result;

	}
?>