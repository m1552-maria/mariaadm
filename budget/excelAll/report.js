$(function(){
	var html = [];
    var i = 0;
   /* $('[name="depIDList"] option').each(function(){
      if($(this).html() == '') return;
      i++;
      if(i == 1) html.push('<tr>');
      html.push('<td><label style="padding-right:10px;"><input type="checkbox" data-check="depID" name="depIDCheck[]" value="'+$(this).val()+'">'+$(this).html()+'</label></td>');
      if(i == 6) {
            i = 0;
            html.push('</tr>');
        }
    });
    $('#depIDTable').css('width','initial');
    $('#depIDTable').html(html.join(''));*/


    i = 0;
    html = [];
    $('[name="invNoList"] option').each(function(){
      i++;
      if(i == 1) html.push('<tr>');
      html.push('<td style="text-align: left;"><label style="padding-right:10px;"><input type="checkbox" data-check="invNo" name="invNoCheck[]" value="'+$(this).val()+'">'+$(this).html()+'</label></td>');
      if(i == 4) {
            i = 0;
            html.push('</tr>');
        }
    });
    $('#invNoTable').css('width','initial');
    $('#invNoTable').html(html.join(''));

    $('[data-show-type="'+$('[name="groupType"]').val()+'"]').css('display','');

    var multiplePlan = id2proc['multiplePlan'];
    id2proc['multiplePlan'] = multiplePlan+'?year='+$('[name="year"]').val();
    $('[name="year"]').change(function(){
      id2proc['multiplePlan'] = multiplePlan+'?year='+$(this).val();
      $('[name="groupType"]').change();
    });

    $('[name="groupType"]').change(function(){
      $('[data-check]').prop('checked',false);
      $('[data-show-type]').css('display','none');
      $('[data-show-type="'+$(this).val()+'"]').css('display','');
      $('[name="depID"]').val('');
      $('[name="invNo"]').val('');
      $('[name="planID"]').val('');
      dataClear();
      $('#depIDTable').html('');
      $('#planIDTable').html('');
    });

    $('[data-check]').click(function(){
      var key = $(this).attr('data-check');
      var val = [];
      $('[data-check="'+key+'"]:checked').each(function(){
        val.push($(this).val());
      });
      $('[name="'+key+'"]').val(val.join(','));
      if($('[name="'+key+'"]').val() != '') ajaxCheck('add', 0);
      else dataClear();
    });
});

function returnValue_multipleDepID(obj) {
    dataClear();
    $('#depIDTable').html('');
    if(obj.length == 0) return false;

    var i = 0;
    var depIDData = [];
    var html = [];
    for(var j in obj) {
      i++;
      if(i == 1) html.push('<tr>');
      html.push('<td><label style="padding-right:10px;"><input type="checkbox" checked disabled>'+obj[j]['name']+'</label></td>');
      if(i == 6) {
        i = 0;
        html.push('</tr>');
      }
      depIDData.push(obj[j]['id']);
    }
    $('#depIDTable').html(html.join(''));
    $('[name="depID"]').val(depIDData.join(','));

    ajaxCheck('add', 0);
    //console.log(obj);

 }

function returnValue_multiplePlan(obj) {
    dataClear();
    $('#planIDTable').html('');
    if(obj.length == 0) return false;

    var i = 0;
    var planIDData = [];
    var html = [];
    for(var j in obj) {
      i++;
      if(i == 1) html.push('<tr>');
      html.push('<td><label style="padding-right:10px;"><input type="checkbox" checked disabled>'+obj[j]['name']+'</label></td>');
      if(i == 6) {
        i = 0;
        html.push('</tr>');
      }
      planIDData.push(obj[j]['id']);
    }
    $('#planIDTable').html(html.join(''));
    $('[name="planID"]').val(planIDData.join(','));

    ajaxCheck('add', 0);
    //console.log(obj);

 }
 