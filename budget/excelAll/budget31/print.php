<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/maintain/inc_vars.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	include_once("$root/system/db.php");
  	include_once('getData.php');


  	$db = new db();
	$sql = "select * from budget_other where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);

	$d = array('year'=>$data['year'],'depID'=>$data['depID'],'invNo'=>$data['invNo'], 'planID'=>$data['planID'], 'groupType' =>$data['groupType'], 'id'=>$_GET['id']);
	$result = getData($data);

	$t1 = explode(':', $result['showTitle']);
	$t2 = explode(',', $t1[1]);
	$showTitle = "(".$t1[0].")".implode('_', $t2);

	$filename = $data['year'].'年'.$showTitle.'-教養補助.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('key'=>'','title'=>'序號','width'=>6),
		'B' =>array('key'=>'empName','title'=>'姓名','width'=>10),
		'C' =>array('key'=>'class','title'=>'類別','width'=>8),
		'D' =>array('key'=>'monthNum','title'=>'申請補助月數','width'=>15),
		'E' =>array('key'=>'monthPay','title'=>'每月申請補助金額','width'=>15),
		'F' =>array('key'=>'yTotal','title'=>'全年度補助金額','width'=>15),
		'G' =>array('key'=>'notes','title'=>'備註','width'=>20)
	);

	
	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i, $data['year'].'年度教養機構服務補助預算表');
    $sheet->getStyle('A1:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $i++;
    $sheet->setCellValue('A'.$i, $result['showTitle']);
    $sheet->setCellValue('G'.$i,'預算表(三-1)');
   	$sheet->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    $i++;
	foreach ($header as $key => $value) {
		$sheet->setCellValue($key.$i, $value['title']);
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}

	$total = 0;



	foreach ($result['dataList'] as $key => $value) {
		$i++;
		foreach ($header as $k2 => $v2) {
			$showText = $value[$v2['key']];
			switch ($v2['key']) {
				case '':
					$showText = $i-5;
					break;
				case 'monthPay':
					$showText = number_format($showText);
					break;
				case 'yTotal':
					$yTotal = (int)$value['monthPay'] * $value['monthNum'];
					$total += $yTotal;
					$showText = number_format($yTotal);
					break;
			}

			$sheet->setCellValue($k2.$i, $showText);
		}
		$sheet->getStyle('G'.$i)->getAlignment()->setWrapText(true);
	}

	$per = (int)$result['per'] / 100;

	$i++;
	$sheet->setCellValue('B'.$i, '合計');
	$sheet->mergeCells('C'.$i.':E'.$i);//合併
	$sheet->setCellValue('F'.$i, number_format($total));

	$i++;
	$sheet->mergeCells('B'.$i.':C'.$i);//合併
	$sheet->setCellValue('B'.$i, '機構績優'.$result['perName'].'獎金');
	$sheet->setCellValue('D'.$i, '比例');
	$sheet->setCellValue('E'.$i, '='.$per);
	$sheet->setCellValue('F'.$i, number_format(round($total*$per)));

	$i++;
	$sheet->mergeCells('B'.$i.':E'.$i);//合併
	$sheet->setCellValue('B'.$i, '補助款合計(已計績優獎金)');//合併
	$sheet->setCellValue('F'.$i, number_format($total + round($total*$per)));

	$sheet->getStyle('A4:G'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線

	$i=$i+2;
	$sheet->setCellValue('G'.$i, '填表人 : '.$emplyeeinfo[$data['creator']]);

	$sheet->getStyle('A1'.':G'.$i)->getFont()->setSize(12);
	$sheet->getStyle('A4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('A4:G'.$i)->getAlignment()->setWrapText(true); //自動換行

//	$sheet->getStyle('A1:G'.$i)->getFont()->setBold(true); //設定粗體
	$sheet->getStyle('A'.$i.':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	//$sheet->getStyle('A'.$i.':G'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

?>