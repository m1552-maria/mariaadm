<?php

	function getData($data = array()) {
		//print_r($data);
		global $departmentinfo, $emplyeeinfo, $jobinfo;
		$result = array('error'=>0,'msg'=>'', 'dataList'=>array());

		$db = new db();	

		/*$sql = "select * from plan_stringmap where class='mtype' order by id";
		$mList = array();
		$rs = $db->query($sql);
		while ($r=$db->fetch_array($rs)) {
			$mList[$r['id']] = $r['title'];
		}*/


		$sql = "select i.*, p.bugetChange, p.plan_arrange_id from plan p inner join plan_items i on p.id = i.pid where p.year='".$data['year']."' and p.depID ='".$data['depID']."' and  p.bugetChange > 0 and p.id='".$data['pID']."'"; 


		$rs = $db->query($sql);
		$dataList = array();
		$sidList = array();
		$result['bugetChange'] = 0;
		$result['plan_arrange_id'] = 0;
		while ($r=$db->fetch_array($rs)) {
			$result['bugetChange'] = $r['bugetChange'];
			$result['plan_arrange_id'] = $r['plan_arrange_id'];
			$sidList[]= "'".$r['id']."'";
			$dataList[$r['id']] = $r;
			$dataList[$r['id']]['bDate'] = (empty($r['bDate'])) ? '' : date('Y/m/d', strtotime($r['bDate']));
			$dataList[$r['id']]['eDate'] = (empty($r['eDate'])) ? '' : date('Y/m/d', strtotime($r['eDate']));

			$dataList[$r['id']]['srveffect'] = str_replace(' ','&nbsp;',$r['srveffect']);
			$dataList[$r['id']]['srveffect'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['id']]['srveffect']);

			$dataList[$r['id']]['dateNotes'] = str_replace(' ','&nbsp;',$r['dateNotes']);
			$dataList[$r['id']]['dateNotes'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['id']]['dateNotes']);

			$dataList[$r['id']]['notes'] = str_replace(' ','&nbsp;',$r['notes']);
			$dataList[$r['id']]['notes'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['id']]['notes']);

			$dataList[$r['id']]['srvpeopleNotes'] = str_replace(' ','&nbsp;',$r['srvpeopleNotes']);
			$dataList[$r['id']]['srvpeopleNotes'] = str_replace(chr(13).chr(10), "<br />",$dataList[$r['id']]['srvpeopleNotes']);

			//$dataList[$r['mtype']]['dataLength'] += 1;

			$data['sid'] = "'".$r['id']."'";
			if(isset($data['id'])) unset($data['id']);
			$budget1Data = getBudget1Data($data);

			/*echo "<pre>";
			print_r($budget1Data);
			exit;*/

			$dataList[$r['id']]['moneyData'] = array();
			if($budget1Data['error'] == 0) {
				$moneyData = array('+'=>array('total'=>0, 'data'=>array()), '-'=>array('total'=>0, 'data'=>array()));
				foreach ($budget1Data['dataList'] as $k => $v) {
					if($k != '' && $v['total'] != 0) {
						$moneyData[$v['vType']]['total'] += $v['total'];
						$moneyData[$v['vType']]['data'][$v['title']] = number_format($v['total']);
					}
				}
				$moneyData['total'] = number_format($moneyData['+']['total'] - $moneyData['-']['total']);
				$moneyData['+']['total'] = number_format($moneyData['+']['total']);
				$moneyData['-']['total'] = number_format($moneyData['-']['total']);
				$dataList[$r['id']]['moneyData'] = $moneyData;
			}

			//$idList[$r['mtype']][] = "'".$r['id']."'";

			$sql = "select * from plan_talent where sid='".$r['id']."'";
			$rs2 = $db->query($sql);
			while ($r2=$db->fetch_array($rs2)) {
				$r2['empName'] = ($r2['empID'] != '') ? $emplyeeinfo[$r2['empID']].'('.$r2['empID'].')' : '新聘人員';
				$dataList[$r['id']]['talentData'][$r2['id']] = $r2['empName'].' / '.$jobinfo[$r2['jobID']].' / '.$r2['rate'];
				$bugetChangeTitle = ($result['bugetChange'] == 2 && $result['plan_arrange_id'] > 0) ? '追減人力： ' : '追加人力： ';
				if($r['bugetChange']) $dataList[$r['id']]['talentData'][$r2['id']] = $bugetChangeTitle.$dataList[$r['id']]['talentData'][$r2['id']];

			}
		}

		$result['dataList'] = $dataList;

		if($result['bugetChange'] > 0 && $result['plan_arrange_id'] > 0) { //追加或者追減 非新增
			$data = array(
				'year'=>$data['year'],
				'depID'=>$data['depID'],
				'sid'=>implode(',', $sidList)
			);

			$result['changeBudget1Data'] = getBudget1Data($data);

			/*echo "<pre>";
			print_r($result['changeBudget1Data']);
			exit;*/

			$sql = "select i.id from plan_arrange_item a inner join plan p on a.planID=p.id inner join plan_items i on p.id = i.pid where a.pID = '".$result['plan_arrange_id']."'";
			if(count($sidList) > 0) $sql .= " and i.id not in (".implode(',', $sidList).")";


			$rs = $db->query($sql);
			$sidList = array();
			while ($r=$db->fetch_array($rs)) {
				$sidList[] = "'".$r['id']."'";
			}
			
			$data['sid'] = implode(',', $sidList);
			$result['arrangeBudget1Data'] = getBudget1Data($data);

			foreach ($result['arrangeBudget1Data']['dataList'] as $key => $value) {
				$result['changeBudget1Data']['dataList'][$key]['otherNotes'] = array();
				$result['changeBudget1Data']['dataList'][$key]['notes'] = '';
				$changeData = $result['changeBudget1Data']['dataList'][$key];
				$result['arrangeBudget1Data']['dataList'][$key]['changeTotal'] = $changeData['total'];
				$result['arrangeBudget1Data']['dataList'][$key]['changeNotes'] = $changeData['notes'];
				$result['arrangeBudget1Data']['dataList'][$key]['changeOtherNotes'] = array();
				if($key != 'R51') { //人事費otherNotes由計劃表四而來 所以追加減時要排除
					$result['arrangeBudget1Data']['dataList'][$key]['changeOtherNotes'] = $changeData['otherNotes'];
				}
			}

		}
		
		$sql = "select * from plan_arrange where id='".$result['plan_arrange_id']."'";
		$rs = $db->query($sql);
		$r = $r=$db->fetch_array($rs);
		$result['arrangeTitle'] = $r['title'];
		/*foreach ($idList as $key => $value) {
			$result['sidList'][$key] = implode(',', $value);
		}*/

		/*echo '<pre>';
		print_r($result['dataList']);
		exit;*/

		return $result;
	}
?>