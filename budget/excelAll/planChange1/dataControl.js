  function formCheck() {
    if($('[name="depID"]').val() == '') {
      alert('請選擇 機構/中心');
      return false;
    }
    if($('[name="pID"]').val() == '') {
      alert('請選擇計畫');
      return false;
    }
    return true;
  }

  function dataClear() {
    $('#showData').html('');
    $('[name="Submit"]').attr('disabled','disabled');
  }

  function ajaxGetPlanChange(depID, year) {
    $.ajax({
      url:'ajaxGetPlanChange.php',
      type:'post',
      dataType:'json',
      data:{depID:depID, year:year},
      success:function(data){
        dataClear();
        console.log(data);
        var html = ['<option value="">--請選擇--</option>'];
        for(var i in data) {
          html.push('<option value="'+i+'" >'+data[i]+'</option>');
        }
        $('[name="pID"]').html(html.join(''));

      },error:function(){
        alert('in')
      }

    });
  }

  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{act:act,year:$('[name="year"]').val(), depID:$('[name="depID"]').val(), pID:$('[name="pID"]').val(), id:id},
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
        } else {
          var html = '';
          html = setData(data, act);
          $('#showData').html(html);
          $('[name="Submit"]').removeAttr('disabled');
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act) {
    var result = [];
    var j = 0;
    console.log(data);
    if(act == 'print') result.push('<table class="tableData" width="1000px;" border="1">'); 
    else result.push('<table>'); 
    result.push('<tr>');
    result.push('<th style="width:10%" class="cLabel">服務名稱</th>');
    result.push('<th style="width:18%"  class="cLabel">服務目標</th>');
    result.push('<th class="cLabel">服務/計畫內容</th>');
    result.push('<th style="width:8%" class="cLabel">服務量<br>(人/人次/場次)</th>');
    result.push('<th style="width:10%" class="cLabel">執行時間</th>');
    result.push('<th style="width:22%" class="cLabel">經費預算</th>');
    result.push('</tr>');

    var items = data['dataList'];
    var c = 0;

    for(var j in items) {
      c++;
      result.push('<tr>');
      result.push('<td class="colData">'+items[j]['title']+'</td>');
      result.push('<td class="colData">'+items[j]['goal']+'</td>');
      result.push('<td class="colData">'+items[j]['content']+'</td>');
      result.push('<td class="colData">'+items[j]['srvpeople']+' '+items[j]['srvunit']+'<br>'+items[j]['srvpeopleNotes']+'</td>');
       result.push('<td class="colData">'+items[j]['bDate']+'~<br>'+items[j]['eDate']+'<br>'+items[j]['dateNotes']+'</td>');

      //經費預算部份
      result.push('<td class="colData">');
      result.push('<span>收入合計 : </span><span>'+items[j]['moneyData']['+']['total']+'</span>');
      var moneyAdd = items[j]['moneyData']['+']['data'];
      for(var k in moneyAdd) {
        result.push('<br>');
        result.push('<span>'+k+':</span><span>'+moneyAdd[k]+'</span>');
      }
      result.push('<br><br><span>支出合計 : </span><span>'+items[j]['moneyData']['-']['total']+'</span>');
      var moneyLess = items[j]['moneyData']['-']['data'];
      for(var k in moneyLess) {
        result.push('<br>');
        result.push('<span>'+k+' : </span><span>'+moneyLess[k]+'</span>');
      }
      result.push('<br><br><span>收支餘絀 : </span><span>'+items[j]['moneyData']['total']+'</span>');
      result.push('</td>');
      


      result.push('</tr>');


      var talentData = items[j]['talentData'];
      var talentHtml = [];
      for(var k in talentData) {
        talentHtml.push(talentData[k]+'<br>');
      }

      result.push('<tr>');
      result.push('<td class="cLabel">人力異動</td>');
      result.push('<td colspan="5" class="colData">'+talentHtml.join('')+'</td>');
      result.push('</tr>');


      result.push('<tr>');
      result.push('<td class="cLabel">績效/稽核<br>/評鑑指標</td>');
      result.push('<td colspan="3" class="colData">'+items[j]['srveffect']+'</td>');
      result.push('<td class="cLabel">備註</td>');
      result.push('<td class="colData">'+items[j]['notes']+'</td>');
      result.push('</tr>');

    }

    result.push('</table><br>');
    
    if(data['bugetChange'] != 0 && data['plan_arrange_id'] != 0) {
      result.push('<h4>經費預算【預算追加減】</h4>');
      result.push('<table  class="tableData" width="1000px;" border="1">');
      result.push('<tr>');
      result.push('<td class="cLabel tdTitle" width="15%">預算項目</td>');
      result.push('<td class="cLabel tdTitle" width="10%">收入預算</td>');
      result.push('<td class="cLabel tdTitle" width="10%">支出預算</td>');
      result.push('<td class="cLabel tdTitle" width="10%">收入預算<br>追加減</td>');
      result.push('<td class="cLabel tdTitle" width="10%">支出預算<br>追加減</td>');
      result.push('<td class="cLabel tdTitle" width="10%">收入預算<br>(追加減後)</td>');
      result.push('<td class="cLabel tdTitle" width="10%">支出預算<br>(追加減後)</td>');
      result.push('<td class="cLabel tdTitle" width="25%">預算說明</td>');
      result.push('</tr>');

      var tList = {
        '+':{'total':0,'changeTotal':0, 'allTotal':0},
        '-':{'total':0,'changeTotal':0, 'allTotal':0}
      }
      var allTotal = 0;
      console.log(data['arrangeBudget1Data']['dataList']);

      for(var i in data['arrangeBudget1Data']['dataList']) {

        if(i == 'R101') continue;
        var d = data['arrangeBudget1Data']['dataList'][i];
       // console.log(d);

        if(d['total'] == 0 && d['changeTotal'] == 0) continue;
        result.push('<tr data-tr="">');
        //預算項目
        result.push('<td class="colData">'+d['title']+'</td>');
        tList[d['vType']]['total'] += parseInt(d['total']);
        tList[d['vType']]['changeTotal'] += parseInt(d['changeTotal']);
        tList[d['vType']]['allTotal'] += (parseInt(d['total']) + parseInt(d['changeTotal']));

        var inAmount = '';
        var outAmount = '';
        var changeIn = '';
        var changeOut = '';
        var allInAmount = '';
        var allOutAmount = '';

        if(d['vType'] == '+') {
          inAmount = d['total'];
          allInAmount = inAmount + d['changeTotal'];
          changeIn = toCurrency(d['changeTotal']);

        } else {
          outAmount = d['total'];
          allOutAmount = outAmount + d['changeTotal'];
          changeOut = toCurrency(d['changeTotal']);

        }

        inAmount = (inAmount == 0) ? '' : toCurrency(inAmount);
        outAmount = (outAmount == 0) ? '' : toCurrency(outAmount);
        changeIn = (changeIn == 0) ? '' : changeIn;
        changeOut = (changeOut == 0) ? '' : changeOut;
        allInAmount = (allInAmount == 0) ? '' : toCurrency(allInAmount);
        allOutAmount = (allOutAmount == 0) ? '' : toCurrency(allOutAmount);

        //收入預算
        result.push('<td class="colData tdNum" data-in="'+i+'">'+inAmount+'</td>');
        //支出預算
        result.push('<td class="colData tdNum" data-out="'+i+'">'+outAmount+'</td>');
        //收入預算追加減
        result.push('<td class="colData tdNum">'+changeIn+'</td>');
        //支出預算追加減
        result.push('<td class="colData tdNum">'+changeOut+'</td>');
        //收入預算(追加減後)
        result.push('<td class="colData tdNum">'+allInAmount+'</td>');
        //支出預算(追加減後)
        result.push('<td class="colData tdNum">'+allOutAmount+'</td>');

        //年度預算增減數
        var total = parseInt(d['total']) + parseInt(d['changeTotal']);
        allTotal += total;

        //預算說明
        var otherNotes = d['otherNotes'].join('\n');
        var notes = d['notes'];
        var changeOtherNotes = d['changeOtherNotes'].join('\n');
        var changeNotes = d['changeNotes'];
       /* if(act != 'print') {
          if(otherNotes != '') otherNotes = '<textarea disabled style="width:95%" rows="4">' + otherNotes +'</textarea>';
          notes = '<textarea style="width:95%;margin-top:3px;" rows="4" id="notes'+i+'" name="notes['+i+']">' + notes +'</textarea>';
        }*/
        result.push('<td class="colData">'+changeOtherNotes+changeNotes+'</td>');
        result.push('</tr>');
      }

      /*小計*/
      console.log(tList);
      html = setTotalData('小計', tList, allTotal, 0);
      result.push(html);

      /*損益餘絀*/
      html = setBalanceData('損益餘絀', tList, 0);
      result.push(html);

      otherList = ['R60', 'R101'];
      for(var i in otherList) {
        var d = data['arrangeBudget1Data']['dataList'][otherList[i]];
        console.log(d);
        result.push('<tr data-tr="">');
        result.push('<td class="colData">'+d['title']+'</td>');
        result.push('<td class="colData tdNum"></td>');

        var dd = '';
        if(otherList[i] == 'R60') {
          tList['-']['total'] -= parseInt(d['total']);
          tList['-']['changeTotal'] -= parseInt(d['changeTotal']);
          tList['-']['allTotal'] -= parseInt(d['total']+d['changeTotal']);
          dd = 'data-other="R60"';
        } else {
          tList['-']['total'] += parseInt(d['total']);
          tList['-']['changeTotal'] += parseInt(d['changeTotal']);
          tList['-']['allTotal'] += parseInt(d['total']+d['changeTotal']);
        }

        var changeTotal = toCurrency(d['changeTotal']);
        

        var total = (d['total'] == 0) ? '' : d['total'];
        changeTotal = (changeTotal == 0) ? '' : changeTotal;
        var allAmount = toCurrency(d['total']+d['changeTotal']);
        allAmount = (allAmount == 0) ? '' : allAmount;

        result.push('<td class="colData tdNum">'+toCurrency(total)+'</td>');
        result.push('<td class="colData tdNum"></td>');

        result.push('<td class="colData tdNum" >'+changeTotal+'</td>');
        result.push('<td class="colData tdNum"></td>');

        result.push('<td class="colData tdNum" >'+allAmount+'</td>');

        otherNotes = d['otherNotes'];
        notes = d['notes'];
        changeOtherNotes = d['changeOtherNotes'].join('\n');
        changeNotes = d['changeNotes'];
       /* if(act != 'print' && otherList[i] == 'R101') {
          if(otherNotes != '') otherNotes = '<textarea disabled style="width:95%" rows="4">' + otherNotes +'</textarea>';
          notes = '<textarea style="width:95%;margin-top:3px;" rows="4" id="notes'+otherList[i]+'" name="notes['+otherList[i]+']">' + notes +'</textarea>';
        }*/

        result.push('<td class="colData">'+changeOtherNotes+changeNotes+'</td>');
        result.push('</tr>');
      }
       /*合計*/
      html = setTotalData('合計', tList, '', 1);
      result.push(html);

      /*實質餘絀*/
      html = setBalanceData('實質餘絀', tList, 1);
      result.push(html);

      var total = tList['+']['total'] - tList['-']['total'];
      var changeTotal = tList['+']['changeTotal'] - tList['-']['changeTotal'];
      if(total < 0) {
        tList['+']['total'] += Math.abs(total);
      } else {
        tList['-']['total'] += Math.abs(total);
      }

      if(changeTotal < 0) {
        tList['+']['changeTotal'] += Math.abs(changeTotal);
      } else {
        tList['-']['changeTotal'] += Math.abs(changeTotal);
      }
      
    /*  result.push('<tr data-tr="">');
      result.push('<td class="colData">基金會挹注款收入</td>');
      var t = (total < 0) ? Math.abs(total) : '';
      result.push('<td class="colData tdNum">'+toCurrency(t)+'</td>');
      result.push('<td class="colData tdNum"></td>');
      var lt = (changeTotal < 0) ? Math.abs(changeTotal) : '';
      result.push('<td class="colData tdNum">'+toCurrency(lt)+'</td>');
      result.push('<td class="colData tdNum"></td>');
      result.push('<td class="colData tdNum"></td>');
      result.push('<td class="colData"></td>');
      result.push('</tr>');

      result.push('<tr data-tr="">');
      result.push('<td class="colData">發展及危機準備金</td>');
      var t = (total > 0) ? Math.abs(total) : '';
      result.push('<td class="colData tdNum"></td>');
      result.push('<td class="colData tdNum">'+toCurrency(t)+'</td>');
      var lt = (changeTotal > 0) ? Math.abs(changeTotal) : '';
      result.push('<td class="colData tdNum"></td>');
      result.push('<td class="colData tdNum">'+toCurrency(lt)+'</td>');
      result.push('<td class="colData tdNum"></td>');
      result.push('<td class="colData"></td>');
      result.push('</tr>');*/

      /*總計*/
      /*html = setTotalData('總計', tList, '', 2);
      result.push(html);*/
      
      /*總餘絀*/
     /* result.push('<tr data-tr="">');
      result.push('<td class="cLabel tdTitle" style="background-color:inherit">總餘絀</td>');
      result.push('<td class="cLabel" style="background-color:inherit" colspan="2">$0</td>');
      result.push('<td class="cLabel" style="background-color:inherit" colspan="2">$0</td>');
      result.push('<td class="cLabel" style="background-color:inherit"></td>');
      result.push('<td class="cLabel" style="background-color:inherit"></td>');
      result.push('</tr>');*/



      result.push('</table>');
    }
    


    return result.join('');
  }

  function setTotalData(title, tList, allTotal, dTitle) {
    result = [];
    result.push('<tr data-tr="">');
    result.push('<td class="colData tdTitle">'+title+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['+']['total'])+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['-']['total'])+'</td>');
    result.push('<td class="colData tdNum" >'+toCurrency(tList['+']['changeTotal'])+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['-']['changeTotal'])+'</td>');
    result.push('<td class="colData tdNum" >'+toCurrency(tList['+']['allTotal'])+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['-']['allTotal'])+'</td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function setBalanceData(title, tList, dTitle) {
    console.log(tList);
    result = [];
    var total = tList['+']['total'] - tList['-']['total'];
    total = (total >= 0) ? '$'+toCurrency(total) : '($'+toCurrency(Math.abs(total))+')';

    var changeTotal = tList['+']['changeTotal'] - tList['-']['changeTotal'];
    changeTotal = (changeTotal >= 0) ? '$'+toCurrency(changeTotal) : '-$'+toCurrency(Math.abs(changeTotal));

    var allTotal = tList['+']['allTotal'] - tList['-']['allTotal'];
    allTotal = (allTotal >= 0) ? '$'+toCurrency(allTotal) : '-$'+toCurrency(Math.abs(allTotal));

    result.push('<tr data-tr="">');
    result.push('<td class="cLabel tdTitle">'+title+'</td>');
    result.push('<td class="cLabel" colspan="2">'+total+'</td>');
    result.push('<td class="cLabel" colspan="2">'+changeTotal+'</td>');
    result.push('<td class="cLabel" colspan="2">'+allTotal+'</td>');
    result.push('<td class="cLabel"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function toCurrency(num){
    var parts = num.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
  }

 