<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/maintain/inc_vars.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	
	$result = array('error'=>0,'msg'=>'');

	if(empty($_POST['year']) || empty($_POST['depID'])) {
		$result['error'] = 1;
		$result['msg'] = '資料錯誤';
		echo json_encode($result);
	}

	include_once($root."/system/db.php");

	$db = new db();	

	include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/budget1/getData.php");
	include_once('getData.php');
	$data = array('year'=>$_POST['year'],'depID'=>$_POST['depID'], 'id'=>$_POST['id'], 'pID'=>$_POST['pID']);


	$result = getData($data);

	echo json_encode($result);



?>