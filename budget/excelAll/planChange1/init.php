<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
		/*echo "<pre>";
		print_r($_REQUEST);
		exit;*/
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "專案審核表";
	$tableName = "planchange1";
	// for Upload files and Delete Record use
	$ulpath = '../../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 12;
	$NeedFilter = '';
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$yearList = array();
	$nowYear = (int)date('Y') - 1911;
	if(empty($_REQUEST['year'])) $_REQUEST['year'] = $nowYear+1;
	for($i=$nowYear-2; $i<=$nowYear+2; $i++) $yearList[$i] = $i;
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('creator'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,''),
			'year'=>array($nowYear+1, false)
		),
		"filterCondi"=>array(	//比對條件
			'depID'=>"%s like '%s%%'"
		),
		"filters"=>array( //過濾器
			'year'=>$yearList
		),
		"extraButton"=>array( //新增的button
		)
	);

	$db = new db();		 

	$depIDList = array_merge(array(''=>'--部門--'), $departmentinfo);
	if($_SESSION['privilege'] > 10) { //總後台過來
		//TreeControl
		if(!empty($_REQUEST['depID'])) $opAR = array($_REQUEST['depID']=>$_REQUEST['cname']); 
		else $opAR = array(''=>'--全部--');
		$opList['filters']['depID'] = $opAR;
		$opList['extraButton']['depTreeShow'] = array("選取部門",false,true,'depTreeShow');

	} else {
		
		$NeedFilter = array();
		foreach ($depIDList as $key => $value) {
			if(!in_array($key, $_SESSION['user_classdef'][2]) && $key != '' && $key != $_SESSION['depID']) unset($depIDList[$key]);
			elseif($key != '') $NeedFilter[] = " depID like '".$key."%'";
		}
		ksort($depIDList);
		$opList['filters']['depID'] = $depIDList;

		if($_REQUEST['depID'] == '') {
			$NeedFilter = "(".implode(' or ', $NeedFilter).")";
			$sqlYear .= " where ".$NeedFilter;
		} else $NeedFilter = '';
	}
	

	$sql = "select id, title from plan where bugetChange > 0 and year='".$_REQUEST['year']."'";
	$rs = $db->query($sql);
	$cPlanList = array();
	while($r = $db->fetch_array($rs)) $cPlanList[$r['id']] = $r['title'];

	$fieldsList = array(
		"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
		"year"=>array("年度","",true,array('Text')),
		"depID"=>array("部門","",false,array('Select', $depIDList)),
		"pID"=>array("標題","",false,array('Select', $cPlanList)),
		"creator"=>array("建單人","",false,array('Select', $emplyeeinfo)),
		"idNum"=>array("維護","",false,array('Define', getMaintain))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 

	$fieldA = array(
		"year"=>array("年度","select",1,"",$yearList),
		//"depID"=>array("部門","select",1,"",$depIDList),
		"depID"=>array("部門","queryID",4,"depID"),
		"pID"=>array("追加減計畫","select",1,"",array(''=>'--請選擇--')),
		"creator"=>array("creator","hidden","","","")
	);
	//新增時的預設值
	$fieldA_Data = array();
	$fieldA_Data['creator'] = ($_SESSION['privilege'] <= 10) ? $_SESSION['empID'] : $_SESSION['Account'];
	$fieldA_Data['year'] = $_POST['year'];
	if(!empty($_POST['depID'])) {
		$fieldA_Data['depID'] = $_POST['depID'];

		$sql = "select * from planchange1 where year='".$_POST['year']."' and depID='".$_POST['depID']."' order by id";
		$pChangeList = array();
		$rs = $db->query($sql);
		while($r=$db->fetch_array($rs)) $pChangeList[$r['id']] = "'".$r['id']."'";
		$sql = "select * from plan where year='".$_POST['year']."' and depID='".$_POST['depID']."' and bugetChange > 0 order by id";
		$rs = $db->query($sql);
		$addPlanChange = array(''=>'--請選擇--');
		$editPlanChange = array();
		/*print_r($sql);
		exit;*/
		while($r=$db->fetch_array($rs)) {
			if(empty($fieldA_Data['pID'])) $fieldA_Data['pID'] = $r['id']; 
			if(empty($pChangeList[$r['id']])) $addPlanChange[$r['id']] = $r['title'];
			$editPlanChange[$r['id']] = $r['title'];
			$fieldA['pID'] = array("追加減計畫","select",1,"", $addPlanChange);
		}
	}
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"year"=>array("年度","readonly","",""),
		"depID"=>array("部門","readonly","","",$depIDList),
		"pID"=>array("追加減計畫","readonly","","",$cPlanList)
	);
?>