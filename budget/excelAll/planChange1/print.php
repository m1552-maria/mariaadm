<?php
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";
  include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/budget1/getData.php");
	include "getData.php";

	$db = new db();
	$sql = "select c.*, p.id as planID, p.year, p.title, p.invNo, p.bugetChange, p.depID, p.plan_arrange_id from planChange1 c inner join plan p on c.pID=p.id where c.id = '".$_GET['id']."'";

	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);
  $sql = "select * from plan_arrange where year='".$data['year']."'";
  $rs = $db->query($sql);

  $pArrangeList = array(); 
  while ($r = $db->fetch_array($rs)) {
    $pArrangeList[$r['id']] = $r['projID'];
  }

  if($data['bugetChange'] == 1 && $data['plan_arrange_id'] == 0) {
    $sql = "select i.pID as arrangeID from plan p inner join plan_arrange_item i on p.id=i.planID where p.id='".$data['planID']."' limit 1";
    $rs = $db->query($sql);
    $r = $db->fetch_array($rs);
   
    $data['newPlanHtml'] = '<input type="checkbox" checked onClick="return false;">新增計劃（計劃編號：'.$pArrangeList[$r['arrangeID']].'）'; 
    $data['changePlanHtml'] = '<input type="checkbox" onClick="return false;">預算追加減';
  } else {
    $data['newPlanHtml'] = '<input type="checkbox" onClick="return false;">新增計劃'; 
    $data['changePlanHtml'] = '<input type="checkbox" checked onClick="return false;">預算追加減（原計劃編號：'.$pArrangeList[$data['plan_arrange_id']].')';
  }
  
  $now = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>瑪利亞預算管理系統</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
<style type="text/css">
  .cLabel {
    text-align: center;
    background-color: #EEE;
    border: 1px solid #333333;
    padding: 4px 8px 4px 4px;
  }
  .tableData {
  	margin: 0 auto;
  	border: 1px solid #00; border-collapse: collapse;
  }
  .tableData td{
  	padding-left: 5px;
  }
  .colData {
  	border-color: #000;
  }

  table {
  	font-size: 16px;
  }
</style>
</head>
<body>
	<input type="hidden" name="year" value="<?php echo $data['year'];?>">
	<input type="hidden" name="depID" value="<?php echo $data['depID'];?>">
  <input type="hidden" name="pID" value="<?php echo $data['planID'];?>">
	

  <table  width="1000px;" style="margin: 0 auto;">
    <tr>
      <td width="30%"></td>
      <td width="40%" style="line-height: 0;"><h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3></td>
      <td width="30%"></td>
    </tr>
    <tr>
      <td></td>
      <td style="line-height: 0;"><h3 style="text-align: center;"><?php echo $data['year'];?><span id="showYear"></span>年專案審核表</h3></td>
      <td><div style="text-align: right;">系統編號：<?php echo $data['planID'];?></div></td> 
    </tr>
  </table>
  <br>

<div style="width: 1000px;margin: 0 auto;">
	<table  width="1000px;" style="margin: 0 auto;">
		<tr>
      <td style="font-weight: bold;width: 25%;">機構/中心：<?php echo $departmentinfo[$data['depID']]?></td>
      <td style="font-weight: bold;">統一編號：<?php echo $data['invNo'];?></td>
    </tr>
    <tr>
      <td style="font-weight: bold;width: 30%;"><?php echo $data['newPlanHtml'];?></td>
      <td style="font-weight: bold;"><?php echo $data['changePlanHtml'];?></td>
    </tr>
	</table>
	
	<div id="showData">
  
	</div>
  <table  width="1000px;" style="margin: 0 auto;">
    <tr>
      <td style="font-weight: bold;width: 25%;text-align: left;">申請人：</td>
      <td style="font-weight: bold;width: 25%;text-align: left;">單位主管：</td>
      <td style="font-weight: bold;width: 25%;text-align: left;">機構主管：</td>
      <td style="font-weight: bold;width: 25%;text-align: left;">申請日期：<?php echo $now;?></td>
    </tr>
  </table>
  <br>
  <hr >
  <h4>【審核意見】</h4>
  <table  class="tableData" width="1000px;" border="1">
    <tr>
      <td class="cLabel" colspan="2">審核單位</td>
      <td class="cLabel" colspan="3">照會單位</td>
    </tr>
    <tr>
      <td class="cLabel">執行長</td>
      <td class="cLabel">董事長</td>
      <td class="cLabel">財務室</td>
      <td class="cLabel">行管室</td>
      <td class="cLabel">其他</td>
    </tr>
    <tr>
      <td style="height: 120px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>
</div>

	<div style="text-align:center;margin-top:20px;">
		<button type="button" id="print_button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
	    <button type="button" id="print_close" onClick="window.close();">關閉</button>
	</div>
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="dataControl.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
	    ajaxCheck('print', <?php echo $_GET['id'];?>);
	})

	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
  }
</script>


</body>
</html>