  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    $('#showData').html('');
    $('[name="Submit"]').attr('disabled','disabled');
  }

  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
          $('[name="invNo"]').val(''); 
        } else {
          var html = '';
          html = setData(data, act);
          $('#showData').html(html);
          $('#showTitle').html(data['showTitle']);
          $('[name="Submit"]').removeAttr('disabled');
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act) {
    var result = [];
    var j = 0;
    console.log(data);
    var dataList = data['dataList'];


    result.push('<table  class="tableData" width="1000px;" border="1">');
    result.push('<tr>');
    result.push('<td colspan="7" class="" width=""></td>');
    result.push('<td colspan="3" class="cLabel tdTitle" width="">收入</td>');
    result.push('<td colspan="3" class="cLabel tdTitle" width="">支出</td>');
    result.push('</tr>');
    result.push('<tr>');
    result.push('<td class="cLabel tdTitle" width="">序號</td>');
    result.push('<td class="cLabel tdTitle" width="">編號</td>');
    result.push('<td class="cLabel tdTitle" width="">部門</td>');
    result.push('<td class="cLabel tdTitle" width="">計畫編號</td>');
    result.push('<td class="cLabel tdTitle" width="">計畫名稱</td>');
    result.push('<td class="cLabel tdTitle" width="">新增</td>');
    result.push('<td class="cLabel tdTitle" width="">追加減</td>');
    result.push('<td class="cLabel tdTitle" width="">原總預算</td>');
    result.push('<td class="cLabel tdTitle" width="">追加減<br>預算</td>');
    result.push('<td class="cLabel tdTitle" width="">追加減後<br>總預算</td>');
    result.push('<td class="cLabel tdTitle" width="">原總預算</td>');
    result.push('<td class="cLabel tdTitle" width="">追加減<br>預算</td>');
    result.push('<td class="cLabel tdTitle" width="">追加減後<br>總預算</td>');
    result.push('</tr>');

    var j = 0;
    var total = [];
    total['+'] = 0;
    total['-'] = 0;
    for(var i in dataList) {
      j++;
      var d = dataList[i]['data'];
      var m = dataList[i]['moneyData'];
      //序號
      result.push('<td class="colData" >'+j+'</td>');
      //編號
      result.push('<td class="colData" >'+d['pID']+'</td>');
      //部門
      result.push('<td class="colData" >'+d['depIDName']+'</td>');
      //計畫編號
      result.push('<td class="colData" >'+d['projID']+'</td>');
      //計畫名稱
      result.push('<td>'+d['arrangeTitle']+'</td>');

      var showType = '';
      if(d['bugetChange'] == 1 && d['plan_arrange_id'] == 0) showType = '+';
      //新增
      result.push('<td class="colData" >'+showType+'</td>');

      showType = '';
      if(d['bugetChange'] == 1 && d['plan_arrange_id'] > 0) showType = '+';
      else if(d['bugetChange'] == 2) showType = '-'; 
      //追加減
      result.push('<td class="colData" >'+showType+'</td>');

      total['+'] += m['+']['change'];
      total['-'] += m['-']['change'];

      for(var z in m) {
        for(var y in m[z]) {
          if(m[z][y] < 0) {
            m[z][y] = '<span style="color:red;">('+toCurrency(m[z][y]*-1)+')</span>';
          } else {
            m[z][y] = toCurrency(m[z][y]);
          }
        }
      }

      //(收入)原總預算
      result.push('<td class="tdNum" >'+m['+']['original']+'</td>');
      //(收入)追加減預算
      result.push('<td class="tdNum" >'+m['+']['change']+'</td>');
      //(收入)追加減後總預算
      result.push('<td class="tdNum" >'+m['+']['result']+'</td>');

       //(支出)原總預算
      result.push('<td class="tdNum" >'+m['-']['original']+'</td>');
      //(支出)追加減預算
      result.push('<td class="tdNum" >'+m['-']['change']+'</td>');
      //(支出)追加減後總預算
      result.push('<td class="tdNum" >'+m['-']['result']+'</td>');
      
 
      result.push('</tr>');
    }
    result.push('<tr>');

    for(var i in total) {
      if(total[i] < 0) {
         total[i] = '<span style="color:red;">('+toCurrency(total[i]*-1)+')</span>';
      } else {
         total[i] = toCurrency(total[i]);
      }
    }

    result.push('</tr>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel">合計</td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel tdNum">'+total['+']+'</td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel"></td>');
    result.push('<td class="cLabel tdNum">'+total['-']+'</td>');
    result.push('<td class="cLabel"></td>');




    result.push('</table>');
    
    


    return result.join('');
  }

 
  function toCurrency(num){
    var parts = num.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
  }

 