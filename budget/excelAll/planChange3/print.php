<?php
  include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
  include_once("$root/system/db.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/budget1/getData.php");
  include_once('getData.php');
  

  $db = new db();
  $sql = "select * from planChange3 where id = '".$_GET['id']."'";
  $rs = $db->query($sql);
  $data = $db->fetch_array($rs);
  
  $d = array('year'=>$data['year'],'depID'=>$data['depID'],'invNo'=>$data['invNo'], 'groupType' =>$data['groupType']);

  $result = getData($d);

  /*echo "<pre>";
  print_r($result);
  exit;*/

  
  $t1 = explode(':', $result['showTitle']);
  $t2 = explode(',', $t1[1]);
  $showTitle = implode('_', $t2);

  $filename = $data['year'].'年'.$showTitle.'-專案審核一覽表.xlsx';
  
  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");
  require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);

  $header = array(
    'A' =>array('title'=>'序號','width'=>6),
    'B' =>array('title'=>'編號','width'=>10),
    'C' =>array('title'=>'部門','width'=>12),
    'D' =>array('title'=>'計畫編號','width'=>10),
    'E' =>array('title'=>'計畫名稱','width'=>25),
    'F' =>array('title'=>'新增','width'=>6),
    'G' =>array('title'=>'追加減','width'=>6),
    'H' =>array('title'=>'原總預算','width'=>12),
    'I' =>array('title'=>'追加減預算','width'=>12),
    'J' =>array('title'=>'追加減後總預算','width'=>12),
    'K' =>array('title'=>'原總預算','width'=>12),
    'L' =>array('title'=>'追加減預算','width'=>12),
    'M' =>array('title'=>'追加減後總預算','width'=>12)
  );

  $i=1;
  $sheet = $objPHPExcel->getActiveSheet();
  $sheet->mergeCells('A'.$i.':M'.$i);//合併
  $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
  $i++;
  $sheet->mergeCells('A'.$i.':M'.$i);//合併
  $sheet->setCellValue('A'.$i, $data['year'].'年專案審核一覽表');
  $sheet->getStyle('A1:M'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $i++;

  $sheet->mergeCells('A'.$i.':M'.$i);//合併
  $sheet->setCellValue('A'.$i,$result['showTitle']);
  $sheet->getStyle('A1'.':M'.$i)->getFont()->setSize(14);
 
  $i++;
  $sheet->mergeCells('H'.$i.':J'.$i);//合併
  $sheet->setCellValue('H'.$i,'收入');
  $sheet->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('H'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'CA8EFF'))); 
  $sheet->mergeCells('K'.$i.':M'.$i);//合併
  $sheet->setCellValue('K'.$i,'支出');
  $sheet->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('K'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'84C1FF'))); 

  $i++;
  foreach ($header as $key => $value) {
    $sheet->setCellValue($key.$i, $value['title']);
    $sheet->getColumnDimension($key)->setWidth($value['width']);
  }
  $sheet->getStyle('A1:M'.$i)->getFont()->setBold(true); //設定粗體
  $sheet->getStyle('A'.$i.':M'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A'.$i.':M'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 


  $dataList = $result['dataList'];
  $total = array('+'=>0,'-'=>0);
  $num = 0;
  foreach ($dataList as $key => $value) {
    $i++;
    $num++;
    $d = $value['data'];
    $m = $value['moneyData'];

    $sheet->setCellValue('A'.$i, $num); //序號
    $sheet->setCellValue('B'.$i, $d['pID']); //編號
    $sheet->setCellValue('C'.$i, $d['depIDName']); //部門
    $sheet->setCellValue('D'.$i, $d['projID']); //計畫編號
    $sheet->getStyle('A'.$i.':D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet->setCellValue('E'.$i, $d['arrangeTitle']); //計畫名稱

    $showType = '';
    if($d['bugetChange'] == 1 && $d['plan_arrange_id'] == 0) $showType = '+'; 
    $sheet->setCellValue('F'.$i, $showType); //新增

    $showType = '';
    if($d['bugetChange'] == 1 && $d['plan_arrange_id'] > 0) $showType = '+'; 
    elseif($d['bugetChange'] == 2) $showType = '-'; 
    $sheet->setCellValue('G'.$i, $showType); //追加減
    $sheet->getStyle('F'.$i.':G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $total['+'] += $m['+']['change'];
    $total['-'] += $m['-']['change'];

    $n = 71; //G
    foreach ($m as $k1 => $v1) {
      foreach ($v1 as $k2 => $v2) {
        $n++;
        if($v2 < 0) {
          $m[$k1][$k2] = '('.number_format($v2*-1).')';
          $sheet->getStyle(chr($n).$i)->getFont()->getColor()->setARGB('FF0000');
        } else {
           $m[$k1][$k2] = number_format($v2);
        }
        $sheet->setCellValue(chr($n).$i, $m[$k1][$k2]); 
        $sheet->getStyle(chr($n).$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

      }
    }
    

  }

  $i++;
  $sheet->setCellValue('E'.$i, '合計'); 
  if($total['+'] < 0) {
    $sheet->getStyle('I'.$i)->getFont()->getColor()->setARGB('FF0000');
    $sheet->setCellValue('I'.$i, '('.number_format($total['+']*-1).')'); 
  } else {
     $sheet->setCellValue('I'.$i, number_format($total['+'])); 
  }
  $sheet->getStyle('I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

  if($total['-'] < 0) {
    $sheet->getStyle('L'.$i)->getFont()->getColor()->setARGB('FF0000');
    $sheet->setCellValue('L'.$i, '('.number_format($total['-']*-1).')'); 
  } else {
     $sheet->setCellValue('L'.$i, number_format($total['-'])); 
  }
  $sheet->getStyle('L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


  $sheet->getStyle('A'.$i.':M'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 
  $sheet->getStyle('A'.$i.':M'.$i)->getFont()->setBold(true); //設定粗體


  $sheet->getStyle('A4:M'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
  $sheet->getStyle('A4:M'.$i)->getAlignment()->setWrapText(true); //自動換行//畫格線
  //$sheet->getRowDimension(4)->setRowHeight(50);


 


  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');

 
?>
