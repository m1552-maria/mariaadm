<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/maintain/inc_vars.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	
	$result = array('error'=>0,'msg'=>'');

	if(empty($_POST['year'])) {
		$result['error'] = 1;
		$result['msg'] = '資料錯誤';
		echo json_encode($result);
	}

	include_once($root."/system/db.php");

	$db = new db();	

	$where = array();
	$showTitle = array();

	if($_POST['groupType'] == 'depID' && !empty($_POST['depID'])) {
		$show = array();
		$_where = " depID = '".$_POST['depID']."'";
		$dList = explode(',', $_POST['depID']);
		foreach ($dList as $key => $value) $show[] = $departmentinfo[$value];
		$show = '部門:' .implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	if($_POST['groupType'] == 'invNo' && !empty($_POST['invNo'])) {
		$show = array();
		$_where = " invNo = '".$_POST['invNo']."'";
		$dList = explode(',', $_POST['invNo']);
		foreach ($dList as $key => $value) $show[] = $invNo[$value];
		$show = '統一編號:'.implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	$showTitle = implode(' / ', $showTitle);
	$where = implode(' and ', $where);

	if($_POST['act'] == 'add') {
		$sql = "select 1 from planchange3 where year='".$_POST['year']."' and groupType='".$_POST['groupType']."' and ".$where;
		$rs = $db->query($sql);
		if($db->num_rows($rs) > 0) {
			$result['error'] = 1;
			$result['msg'] = ($_POST['year']).'年 所選'.$showTitle.' 專案審核一覽表已經存在';
			echo json_encode($result);
			exit;
		}
	} else {
		$sql = "select 1 from planchange3 where year='".$_POST['year']."' and groupType='".$_POST['groupType']."' and ".$where;
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) {
			$result['error'] = 1;
			$result['msg'] = ($_POST['year']).'年 所選'.$showTitle.' 專案審核一覽表不存在';
			echo json_encode($result);
			exit;
		}
	}


	include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/budget1/getData.php");
	include_once('getData.php');

	$result = getData($_POST);

	echo json_encode($result);



?>