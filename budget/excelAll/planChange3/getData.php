<?php

	function getData($data = array()) {
		global $departmentinfo, $invNo;
		$result = array('error'=>0,'msg'=>'', 'dataList'=>array());

		$db = new db();	

		$where = array();
		if(!empty($data['depID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$depIDList = explode(',', $data['depID']);
			$_where = array();
			foreach ($depIDList as $key => $value) {
				$_where[] = " p.depID like '".$value."%' ";
				$showTitle[] = $departmentinfo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '機構/中心:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['invNo'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$invNoList = explode(',', $data['invNo']);
			$_where = array();
			foreach ($invNoList as $key => $value) {
				$_where[] = " p.invNo like '".$value."%' ";
				$showTitle[] = $invNo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '統一編號:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		$result['showTitle'] = implode(' / ', $result['showTitle']);

		$where = implode(' and ', $where);

		/*取得追加減資料*/
		$sql = "select a.projID, p.id as pID, p.bugetChange,p.plan_arrange_id, i.id as sid, a.depID, a.title as arrangeTitle, ai.pID as arrangeID from plan p inner join plan_items i on p.id = i.pid inner join plan_arrange_item ai on p.id = ai.planID inner join plan_arrange a on ai.pID = a.id where p.year='".$data['year']."' and p.bugetChange > 0 and ".$where; 
		$sql .= " order by a.projID, p.id ";

		$rs = $db->query($sql);
		$dataList = array();
		if(isset($data['id'])) unset($data['id']);

		$aIDList = array();
		while ($r=$db->fetch_array($rs)) {
			$aIDList[$r['arrangeID']] = "'".$r['arrangeID']."'";
			
			$dataList['_'.$r['pID']]['data'] = $r;
			$dataList['_'.$r['pID']]['data']['depIDName'] = $departmentinfo[$r['depID']];
			$dataList['_'.$r['pID']]['sid'][$r['sid']] = "'".$r['sid']."'";
			$dataList['_'.$r['pID']]['moneyData'] = array(
				'+' => array('original' => 0, 'change' => 0, 'result' => 0),
				'-' => array('original' => 0, 'change' => 0, 'result' => 0)
			);
		}

		if(count($dataList) == 0) {
			$result = array('error'=>1,'msg'=>'所選條件沒有資料');
			return $result;
		}

		/*塞入追加減金額*/
		foreach ($dataList as $pID => $v1) {
			$data['sid'] = implode(',', $v1['sid']);
			$budget1Data = getBudget1Data($data);
			if($budget1Data['error'] == 0) {
				foreach ($budget1Data['dataList'] as $k => $v) {
					if($k != '' && $v['total'] != 0) {
						$dataList[$pID]['moneyData'][$v['vType']]['change'] += $v['total'];
						$dataList[$pID]['moneyData'][$v['vType']]['result'] += $v['total'];
					}
				}
			}
		}
		 
		/*取得原始計畫資料& 金額*/
		$sql = "select ai.pID as arrangeID, i.id as sid, p.id as pID from plan p inner join plan_items i on p.id = i.pid inner join plan_arrange_item ai on p.id = ai.planID where p.year='".$data['year']."' and p.bugetChange = 0 and ai.pID in(".implode(',', $aIDList).")"; 
		$rs = $db->query($sql);
		$originalData = array();
		while ($r=$db->fetch_array($rs)) {
			$originalData[$r['arrangeID']]['sid'][$r['sid']] = "'".$r['sid']."'";
			$originalData[$r['arrangeID']]['moneyData'] = array(
				'+' => array('original' => 0),
				'-' => array('original' => 0)
			);
		}
		foreach ($originalData as $arrangeID => $v1) {
			$data['sid'] = implode(',', $v1['sid']);
			$budget1Data = getBudget1Data($data);
			if($budget1Data['error'] == 0) {
				foreach ($budget1Data['dataList'] as $k => $v) {
					if($k != '' && $v['total'] != 0) {
						$originalData[$arrangeID]['moneyData'][$v['vType']]['original'] += $v['total'];
					}
				}
			}
		}

		/*組合原始計畫跟追加減*/
		foreach ($dataList as $pID => $v1) {
			$arrangeID = $v1['data']['arrangeID'];
			if(!isset($originalData[$arrangeID])) continue;
			foreach ($v1['moneyData'] as $vType => $v2) {
				//$dataList[$pID]['moneyData'][$vType]['change'] = number_format($v2['change']);

				$oMoney = $originalData[$arrangeID]['moneyData'][$vType]['original'];
				$dataList[$pID]['moneyData'][$vType]['original'] = $oMoney;
				$dataList[$pID]['moneyData'][$vType]['result'] += $oMoney;

				$resultMoney = $dataList[$pID]['moneyData'][$vType]['result'];
				$dataList[$pID]['moneyData'][$vType]['result'] = $resultMoney;
				$originalData[$arrangeID]['moneyData'][$vType]['original'] = $resultMoney;
			}
			
		}

		$result['dataList'] = $dataList;

		return $result;
	}
?>