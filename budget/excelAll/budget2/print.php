<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/maintain/inc_vars.php");
	include_once("$root/system/db.php");
  	include_once('getData.php');

  	$db = new db();
	$sql = "select * from budget_other where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);

	$d = array('year'=>$data['year'],'depID'=>$data['depID'],'invNo'=>$data['invNo'], 'planID'=>$data['planID'], 'groupType' =>$data['groupType'], 'id'=>$_GET['id']);
	$result = getData($data);

	$t1 = explode(':', $result['showTitle']);
	$t2 = explode(',', $t1[1]);
	$showTitle = "(".$t1[0].")".implode('_', $t2);

	$filename = $data['year'].'年'.$showTitle.'-服務收入.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('key'=>'','title'=>'序號','width'=>6),
		'B' =>array('key'=>'name','title'=>'學生姓名','width'=>12),
		'C' =>array('key'=>'impGrade','title'=>'障礙等級','width'=>6),
		'D' =>array('key'=>'monthNum','title'=>'補助月數','width'=>6),
		'E' =>array('key'=>'monthPay','title'=>'月費全額','width'=>10),
		'F' =>array('key'=>'aidTrainPay','title'=>'補助照顧費','width'=>10),
		'G' =>array('key'=>'selfTrainPay','title'=>'自付照顧費','width'=>10),
		'H' =>array('key'=>'allTrafficPay','title'=>'交通費全額','width'=>10),
		'I' =>array('key'=>'aidTrafficPay','title'=>'補助交通費','width'=>10),
		'J' =>array('key'=>'selfTrafficPay','title'=>'自付交通費','width'=>10)
	);

	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':J'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':J'.$i);//合併
    $sheet->setCellValue('A'.$i, $data['year'].'年度服務費收入預算表');
    $sheet->getStyle('A1:J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $i++;
    $sheet->setCellValue('A'.$i, $result['showTitle']);
    $sheet->setCellValue('J'.$i,'預算表(二)');
   
    $sheet->getStyle('J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    $i++;
	foreach ($header as $key => $value) {
		$sheet->setCellValue($key.$i, $value['title']);
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}

	$totalList = array(
   		'monthNum'=>0,
      	'monthPay'=>0,
      	'aidTrainPay'=>0,
      	'selfTrainPay'=>0,
      	'allTrafficPay'=>0,
      	'aidTrafficPay'=>0,
      	'selfTrafficPay'=>0
   	);

   	$yTotalList = array(
   		'monthNum'=>0,
      	'monthPay'=>0,
      	'aidTrainPay'=>0,
      	'selfTrainPay'=>0,
      	'allTrafficPay'=>0,
      	'aidTrafficPay'=>0,
      	'selfTrafficPay'=>0
   	);

	foreach ($result['dataList'] as $key => $value) {
		$i++;
		foreach ($header as $k2 => $v2) {
			$showText = $value[$v2['key']];
			switch ($v2['key']) {
				case '':
					$showText = $i-5;
					break;
				case 'name':
					if($showText == '') $showTex = '○○○';
					break;
				case 'impGrade':
					break;
				default:
					$totalList[$v2['key']] += (int)$value[$v2['key']];
					if($v2['key'] != 'monthNum') $yTotalList[$v2['key']] += (int)$value[$v2['key']] * (int)$value['monthNum'];
					else $yTotalList[$v2['key']] += (int)$value[$v2['key']];
					$showText = number_format($showText);
					break;
			}

			$sheet->setCellValue($k2.$i, $showText);
		}
	}

	$i++;
	$sheet->mergeCells('A'.$i.':C'.$i);//合併
	$sheet->setCellValue('A'.$i, '合計');

	foreach ($header as $key => $v) {
		if($v['key'] == '' || $v['key'] == 'name' || $v['key'] == 'impGrade') continue;
		$sheet->setCellValue($key.$i, number_format($totalList[$v['key']]));
	}

	$i++;
	$sheet->mergeCells('A'.$i.':C'.$i);//合併
	$sheet->setCellValue('A'.$i, '年度合計');

	foreach ($header as $key => $v) {
		if($v['key'] == '' || $v['key'] == 'name' || $v['key'] == 'impGrade') continue;
		$sheet->setCellValue($key.$i, number_format($yTotalList[$v['key']]));
	}

	$sheet->getStyle('A4:J'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線

	$i=$i+2;
	$sheet->mergeCells('H'.$i.':J'.$i);//合併
	$sheet->setCellValue('H'.$i, '填表人 : '.$emplyeeinfo[$data['creator']]);

	$sheet->getStyle('A1'.':J'.$i)->getFont()->setSize(12);
	$sheet->getStyle('A4:J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('A4:J'.$i)->getAlignment()->setWrapText(true); //自動換行

//	$sheet->getStyle('A1:J'.$i)->getFont()->setBold(true); //設定粗體
	$sheet->getStyle('A'.$i.':J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	//$sheet->getStyle('A'.$i.':J'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

?>