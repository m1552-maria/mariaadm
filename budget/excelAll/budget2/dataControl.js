  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '' && $('[name="planID"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    $('[data-tr]').remove();
    $('[name="Submit"]').attr('disabled','disabled');
  }


  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        planID:$('[name="planID"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
        } else {
          $('#showTitle').html(data['showTitle']);
          var html = '';
          html = setData(data['dataList'], act);
          $('#dataTable').append(html);
          $('[name="Submit"]').removeAttr('disabled');
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act) {
    var result = [];
    var j = 0;
    var totaList = {
      'monthNum':0,
      'monthPay':0,
      'aidTrainPay':0,
      'selfTrainPay':0,
      'allTrafficPay':0,
      'aidTrafficPay':0,
      'selfTrafficPay':0
    };
    var yTotalList = {
      'monthNum':0,
      'monthPay':0,
      'aidTrainPay':0,
      'selfTrainPay':0,
      'allTrafficPay':0,
      'aidTrafficPay':0,
      'selfTrafficPay':0
    };
    for(var i in data) {
      var d = data[i];
      j++;
      for(var k in totaList) {
        totaList[k]+= parseInt(d[k]);
        if(k != 'monthNum') yTotalList[k] += parseInt(d[k])*d['monthNum'];
        else yTotalList[k] += parseInt(d[k]);
      }

      result.push('<tr data-tr>');
      result.push('<td class="colData">'+j+'</td>');
      result.push('<td class="colData">'+d['name']+'</td>');
      result.push('<td class="colData">'+d['impGrade']+'</td>');
      result.push('<td class="colData">'+d['monthNum']+'</td>');
      result.push('<td class="colData">'+d['monthPay']+'</td>');
      result.push('<td class="colData">'+d['aidTrainPay']+'</td>');
      result.push('<td class="colData">'+d['selfTrainPay']+'</td>');
      result.push('<td class="colData">'+d['allTrafficPay']+'</td>');
      result.push('<td class="colData">'+d['aidTrafficPay']+'</td>');
      result.push('<td class="colData">'+d['selfTrafficPay']+'</td>');
      result.push('</tr>');
    }

    result.push('<tr data-tr>');
    result.push('<td colspan="3" class="colData">合計</td>');
    for(var i in totaList) {
      result.push('<td class="colData">'+totaList[i]+'</td>');
    }
    result.push('</tr>');

    result.push('<tr data-tr>');
    result.push('<td colspan="3" class="colData">年度合計</td>');
    for(var i in yTotalList) {
      result.push('<td class="colData">'+yTotalList[i]+'</td>');
    }
    result.push('</tr>');


    return result.join('');
  }

 