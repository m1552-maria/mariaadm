<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
		/*echo "<pre>";
		print_r($_REQUEST);
		exit;*/
	if(empty($_REQUEST['type'])) exit;
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	include "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "預算表(二)";
	$tableName = "budget_other";
	// for Upload files and Delete Record use
	$ulpath = '../../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 12;
	$NeedFilter = '';
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$yearList = array();
	$nowYear = (int)date('Y') - 1911;
	if(empty($_REQUEST['year'])) $_REQUEST['year'] = $nowYear+1;
	for($i=$nowYear-2; $i<=$nowYear+2; $i++) $yearList[$i] = $i;
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('creator'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,''),
			'year'=>array($nowYear+1, false)
		),
		"filterCondi"=>array(	//比對條件
			'depID'=>"%s like '%%%s%%'",
			'invNo'=>"%s like '%%%s%%'",
			'planID'=>"%s like '%%%s%%'",
		),
		"filters"=>array( //過濾器
			'year'=>$yearList
		),
		"extraButton"=>array( //新增的button
		)
	);
	$groupType = (empty($_REQUEST['groupType'])) ? 'depID' : $_REQUEST['groupType'];
	$db = new db();		 

	$depIDList = array_merge(array(''=>'--部門--'), $departmentinfo);
	$invNoList = array(''=>'--全部--');
	foreach ($invNo as $key => $value) $invNoList[$key] = $value;
	if($_SESSION['privilege'] > 10) { //總後台過來
		$opList['filterOption']['groupType'] = array('depID', false);
		$opList['filters']['groupType'] = $groupTypeList;
		//TreeControl
		switch ($groupType) {
			case 'depID':
				if(!empty($_REQUEST['depID'])) $opAR = array($_REQUEST['depID']=>$_REQUEST['cname']); 
				else $opAR = array(''=>'--全部--');
				$opList['filters']['depID'] = $opAR;
				$opList['extraButton']['depTreeShow'] = array("選取部門",false,true,'depTreeShow');
				break;
			
			case 'invNo':
				$opList['filters']['invNo'] = $invNoList;
				break;
		}	

	} else {
		unset($groupTypeList['invNo']);
		$opList['filterOption']['groupType'] = array('depID', false);
		$opList['filters']['groupType'] = $groupTypeList;

		$NeedFilter = array();
		foreach ($depIDList as $key => $value) {
			if(!in_array($key, $_SESSION['user_classdef'][2]) && $key != '' && $key != $_SESSION['depID']) unset($depIDList[$key]);
			elseif($key != '') $NeedFilter[] = " depID like '".$key."%'";
		}
		ksort($depIDList);

		if($_REQUEST['depID'] == '') {
			$NeedFilter = "(".implode(' or ', $NeedFilter).")";
		} else $NeedFilter = array();

		switch ($groupType) {
			case 'depID':
				$opList['filters']['depID'] = $depIDList;
				break;
			case 'planID':
				break;
		}	
	}
	
	$NeedFilter.= ($NeedFilter != '') ? " and type='".$_GET['type']."'" : " type='".$_GET['type']."'";

	switch ($groupType) {
		case 'depID':
			$fieldsList = array(
				"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
				"year"=>array("年度","",true,array('Text')),
				"depID"=>array("部門","",false,array('Define', getDepIDTitle)),
				"creator"=>array("建單人","",false,array('Select', $emplyeeinfo)),
				"idNum"=>array("維護","",false,array('Define', getMaintain))
			);
			break;
		case 'invNo':
			$fieldsList = array(
				"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
				"year"=>array("年度","",true,array('Text')),
				"invNo"=>array("統一編號","",false,array('Define', getInvNoTitle)),
				"creator"=>array("建單人","",false,array('Select', $emplyeeinfo)),
				"idNum"=>array("維護","",false,array('Define', getMaintain))
			);
			break;
		case 'planID':
			$fieldsList = array(
				"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
				"year"=>array("年度","",true,array('Text')),
				"planID"=>array("計劃主檔","",false,array('Define', getPlanIDTitle)),
				"creator"=>array("建單人","",false,array('Select', $emplyeeinfo)),
				"idNum"=>array("維護","",false,array('Define', getMaintain))
			);
			break;
	}
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	//if($_SESSION['privilege'] <= 10) $groupTypeList = array('depID'=>'依照部門');
	$fieldA = array(
		"year"=>array("年度","select",1,"",$yearList),
		"invNoList"=>array("統編","select",1,"",$invNo),
		"groupType"=>array("報表挑選方式","select",1,"",$groupTypeList),
		"creator"=>array("creator","hidden","","",""),
		"type"=>array("type","hidden","","",""),
		"depID"=>array("creator","hidden","","",""),
		"invNo"=>array("creator","hidden","","",""),
		"depIDQuery" => array("選取部門","queryID",'4','multipleDepID'),
		"planID"=>array("creator","hidden","","",""),
		"planIDQuery" => array("選取計劃","queryID",'4','multiplePlan'),
	);
	//新增時的預設值
	$fieldA_Data = array();
	$fieldA_Data['creator'] = ($_SESSION['privilege'] <= 10) ? $_SESSION['empID'] : $_SESSION['Account'];
	$fieldA_Data['year'] = $_POST['year'];
	$fieldA_Data['groupType'] = $groupType;
	$fieldA_Data['type'] = $_GET['type'];
	if(!empty($_POST['depID'])) $fieldA_Data['depID'] = $_POST['depID'];
	if(!empty($_POST['invNo'])) $fieldA_Data['invNo'] = $_POST['invNo'];
	if(!empty($_POST['planID'])) $fieldA_Data['planID'] = $_POST['planID'];
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"year"=>array("年度","readonly","",""),
		"groupType"=>array("groupType","hidden","","",""),
		"depID"=>array("部門","hidden","","",""),
		"invNo"=>array("invNo","hidden","","",""),
		"planID"=>array("planID","hidden","","",""),
		"showTitle"=>array("showTitle","hidden","","","")
	);
?>