<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/maintain/inc_vars.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	
	$result = array('error'=>0,'msg'=>'');

	if(empty($_POST['year'])) {
		$result['error'] = 1;
		$result['msg'] = '資料錯誤';
		echo json_encode($result);
	}

	include_once($root."/system/db.php");

	$db = new db();	

	$sql = "select * from plan where year='".$_POST['year']."'";
	$rs = $db->query($sql);
	$planTitleList = array(); 
	while($r=$db->fetch_array($rs)) {
		$planTitleList[$r['id']] = $r['title'];
	}

	$where = array();
	$showTitle = array();
	if($_POST['groupType'] == 'depID' && !empty($_POST['depID'])) {
		$show = array();
		$_where = " depID = '".$_POST['depID']."'";
		$dList = explode(',', $_POST['depID']);
		foreach ($dList as $key => $value) $show[] = $departmentinfo[$value];
		$show = '部門:' .implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	if($_POST['groupType'] == 'invNo' && !empty($_POST['invNo'])) {
		$show = array();
		$_where = " invNo = '".$_POST['invNo']."'";
		$dList = explode(',', $_POST['invNo']);
		foreach ($dList as $key => $value) $show[] = $invNo[$value];
		$show = '統一編號:'.implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	if($_POST['groupType'] == 'planID' && !empty($_POST['planID'])) {
		$show = array();
		$_where = " planID = '".$_POST['planID']."'";
		$dList = explode(',', $_POST['planID']);
		foreach ($dList as $key => $value) $show[] = $planTitleList[$value];
		$show = '計劃主檔:'.implode(',', $show);
		$showTitle[] = $show;
		$where[] = $_where;
	}

	$where[] = " groupType = '".$_POST['groupType']."'";
	$showTitle = implode(' / ', $showTitle);
	$where = implode(' and ', $where);

	$sql = "select 1 from plan1 where year='".$_POST['year']."' and ".$where;
	//print_r($sql);
	$rs = $db->query($sql);
	if($db->num_rows($rs) > 0) {
		include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
		include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
		$result['error'] = 1;
		$result['msg'] = ($_POST['year']).'年 所選'.$showTitle.' 計劃表(一)已經存在';

		echo json_encode($result);
		exit;
	}

	$sql = "select id from plan1 where year='".((int)$_POST['year']-1)."' and ".$where;
	$rs = $db->query($sql);
	if($db->num_rows($rs) > 0) {
		$r=$db->fetch_array($rs);
		$sql = "select *, (yType-1) as YearType from plan1_items where pID='".$r['id']."' and yType > 1 order by yType, fID, id";

		$rs = $db->query($sql);
		while ($r=$db->fetch_array($rs)) {
			switch ($r['type']) {
				case 'aims':
					$result['data'][$r['YearType']][$r['id']]['aims'] = $r['content'];
					break;
				default:
					$result['data'][$r['YearType']][$r['fID']][$r['type']][] = $r['content'];
					break;
				
			}
		}

	}
	echo json_encode($result);
?>