<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($root."/system/db.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");

	if(isset($_POST['callType'])) {
		$result = getData($_POST['id']);
		echo json_encode($result);
	}

	function getData($id) {
		global $departmentinfo, $invNo;
		$result = array('data'=>array(), 'showTitle'=>array());
		$db = new db();	

		$sql = "select * from plan1 where id='".$id."'";
		$rs = $db->query($sql);
		$plan1Data = $db->fetch_array($rs);

		$sql = "select * from plan where year='".$plan1Data['year']."'";
		$rs = $db->query($sql);
		$planTitleList = array(); 
		while($r=$db->fetch_array($rs)) {
			$planTitleList[$r['id']] = $r['title'];
		}

		switch ($plan1Data['groupType']) {
			case 'depID':
				$dList = explode(',', $plan1Data['depID']);
				foreach ($dList as $key => $value) {
					$result['showTitle'][] = $departmentinfo[$value];
				}
				$result['showTitle'] = '機構/中心:'.implode(',', $result['showTitle']);
				break;
			
			case 'invNo':
				$dList = explode(',', $plan1Data['invNo']);
				foreach ($dList as $key => $value) {
					$result['showTitle'][] = $invNo[$value];
				}
				$result['showTitle'] = '統一編號:'.implode(',', $result['showTitle']);
				break;
			case 'planID':
				$dList = explode(',', $plan1Data['planID']);
				foreach ($dList as $key => $value) {
					$result['showTitle'][] = $planTitleList[$value];
				}
				$result['showTitle'] = '計劃主檔:'.implode(',', $result['showTitle']);
				break;
		}

		$sql = "select * from plan1_items where pID='".$id."'  order by yType, fID, id";
		$rs = $db->query($sql);
		while ($r=$db->fetch_array($rs)) {
			switch ($r['type']) {
				case 'aims':
					$result['data'][$r['yType']][$r['id']]['aims'] = $r['content'];
					break;
				default:
					$result['data'][$r['yType']][$r['fID']][$r['type']][] = $r['content'];
					break;
				
			}
		}

		return $result;
	}

	
?>