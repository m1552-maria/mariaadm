<?php
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	include "$root/system/db.php";
	include "getData.php";

	$db = new db();
	$sql = "select * from plan1 where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);


	$plan1ItemData = getData($_GET['id']);
	$itemList = $plan1ItemData['data'];

	$char = array("十","一","二","三","四","五","六","七","八","九");
/*echo "<pre>";
print_r($itemList);
exit;*/
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>瑪利亞預算管理系統</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
<style type="text/css">
  .cLabel {
    text-align: center;
    background-color: #EEE;
    border: 1px solid #333333;
    padding: 4px 8px 4px 4px;
  }
  .tableData {
  	margin: 0 auto;
  	border: 1px solid #00; border-collapse: collapse;
  }
  .tableData td{
  	padding-left: 5px;
  }
  .colData {
    vertical-align: unset;
  }

</style>
</head>
<body>
	
	<h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3>
	<h3 style="text-align: center;">計劃表(一) - <?php echo $data['year'];?><span id="showYear"></span>年短中長程計劃</h3>

	<table  width="1000px;" style="margin: 0 auto;">
		<tr><td style="font-weight: bold;"><?php echo $plan1ItemData['showTitle'];?></td></tr>
	</table>
	
	<table class="tableData" width="1000px;" border='1'>
	
	  <tr>
	    <th class="cLabel">年度</td>
	    <th style="width:25%" class="cLabel">目標</td>
	    <th style="width:38%" class="cLabel">服務/計畫內容</td>
	    <th style="width:30%" class="cLabel">具體績效/預期效益</td>
	  </tr>

<?php 
	foreach($itemList as $year => $value) { 
		$i = 0;
		foreach ($value as $k => $v) {
			$i++;
			$showNum = array();
			$iList = $i.'';

			for($a=0; $a<strlen($iList); $a++) {
				$showNum[] = $char[$iList[$a]];
			}
			$showNum = implode('', $showNum);
?>

	<tr>
	<?php if($i == 1) { ?>
		<td rowspan="<?php echo count($value);?>" class="colData" style="text-align:center;"><?php echo $data['year']+($year-1);?>年<br>(<?php echo $char[$year];?>年)</td>
	<?php } ?>

		<td class="colData"><?php echo $showNum;?>、 <?php echo $v['aims'];?></td>
		<td class="colData">
		<?php 
			$content = array();
			$z = 0;
			foreach($v['content'] as $k2 => $v2) { 
				if($v2 != '') {
					$z++;
					$content[] = $z.'. '.$v2;
				}
			}
			if(count($content) > 0) {
				echo implode('<br>', $content);
			}
		?>
		
		</td>

		<td class="colData">
		<?php 
			$content = array();
			$z = 0;
			foreach($v['benefit'] as $k2 => $v2) { 
				if($v2 != '') {
					$z++;
					$content[] = $z.'. '.$v2;
				}
			}
			if(count($content) > 0) {
				echo implode('<br>', $content);
			}
		?>
		
		</td>


	</tr>
<?php 
		}
	} 
?>
	  
	</table>


	<div style="text-align:center;margin-top:20px;">
		<button type="button" id="print_button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
	    <button type="button" id="print_close" onClick="window.close();">關閉</button>
	</div>
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script type="text/javascript">
  	/*$(function(){
    	var year = parseInt($('[data-year="0"]').html());
    	$('[data-year]').each(function(){
      		$(this).html(year+parseInt($(this).attr('data-year')));
    	});
  	})*/

	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
    }
</script>


</body>
</html>