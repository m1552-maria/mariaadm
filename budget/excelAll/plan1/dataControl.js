function addAims(key) {
	var num = parseInt($('#tab'+key+' [data-aims]:last').attr('data-aims'))+1;
	var html = [];

	html.push('<div data-aims="'+num+'">');
	html.push('<div>目標'+num+': <input name="plan1['+key+']['+num+'][aims]" type="text" value="" style="width:300px;"><a href="javascript:void(0)" onclick="addAims('+key+')">增加</a> / <a style="color:red;" href="javascript:void(0)" onclick="delAims('+key+', '+num+')">刪除</a></div>');
	html.push('<div data-type="content" style="margin-left:40px;">服務/計畫內容:');
	html.push('<div data-content="1">1.<textarea name="plan1['+key+']['+num+'][content][]" rows="2" cols="100"></textarea>');
	html.push('<a href="javascript:void(0)" onclick="addContent('+"'content'"+' ,'+key+', '+num+')">增加</a>');
	html.push(' / <a style="color:red;" href="javascript:void(0)" onclick="delContent('+"'content'"+', '+key+', '+num+', 1)">刪除</a>')
	html.push('</div></div>');
	html.push('<div data-type="benefit" style="margin-left:40px;">具體績效/預期效益:');
	html.push('<div data-benefit="1">1.<textarea name="plan1['+key+']['+num+'][benefit][]"  rows="2" cols="100"></textarea>');
	html.push('<a href="javascript:void(0)" onclick="addContent('+"'benefit'"+' ,'+key+','+num+')">增加</a>')
	html.push(' / <a style="color:red;" href="javascript:void(0)" onclick="delContent('+"'benefit'"+', '+key+', '+num+', 1)">刪除</a>')
	html.push('</div></div>');
	html.push('</div>');

	$('#tab'+key).append(html.join(''));
}

function delAims(key, num) {
	if($('#tab'+key+' [data-aims]').length == 1) {
		alert('只剩一筆不得刪除');
		return false;
	}
	if(!confirm('確定要刪除第'+key+'年 目標'+num+'嗎?')) return false;
	$('#tab'+key+' [data-aims="'+num+'"]').remove();
}

function addContent(type, key, aKey) {
	var num = parseInt($('#tab'+key+' [data-aims='+aKey+'] [data-'+type+']:last').attr('data-'+type))+1;
	var html = [];

	html.push('<div data-'+type+'="'+num+'">'+num+'.');
	html.push('<textarea name="plan1['+key+']['+aKey+']['+type+'][]" rows="2" cols="100"></textarea>');
	html.push('<a href="javascript:void(0)" onclick="addContent('+"'"+type+"'"+', '+key+', '+aKey+')">增加</a>');
	html.push(' / <a style="color:red;" href="javascript:void(0)" onclick="delContent('+"'"+type+"'"+', '+key+', '+aKey+', '+num+')">刪除</a>');
	html.push('</div>');

	$('#tab'+key+' [data-aims='+aKey+'] [data-type="'+type+'"]').append(html.join(''));
}

function delContent(type, key, aKey, num) {
	if($('#tab'+key+' [data-aims="'+aKey+'"] [data-'+type+']').length == 1) {
		alert('只剩一筆不得刪除');
		return false;
	}
	var typeText = {
		'content':'服務/計畫內容', 
		'benefit':'具體績效/預期效益'
	}
	if(!confirm('確定要刪除第'+key+'年 目標'+aKey+'的 '+typeText[type]+num+' 嗎?')) return false;
	$('#tab'+key+' [data-aims="'+aKey+'"] [data-'+type+'="'+num+'"]').remove();
}

function ajaxCheck(id) {
    $.ajax({
      url:'getData.php',
      type:'post',
      dataType:'json',
      data:{callType:'ajax',id:id},
      success:function(data){
      	$('#showTitle').html(data['showTitle']);
      	console.log(data);
      	for(var i in data['data']) {
      		setData(data['data'][i], i);
		}
   
      },error:function(){
      	alert('in')
      }

    });
}

function setData(data, key) {
	console.log(data);
	
	var html = [];
	var j = 0;
	for(var i in data) {
		j++;	
		html.push('<div data-aims="'+j+'">');
		html.push('<div>目標'+j+':');
		html.push('<input name="plan1['+key+']['+j+'][aims]" type="text" value="'+data[i]['aims']+'" style="width:300px;"><a href="javascript:void(0)" onclick="addAims('+key+')">增加</a>');
		html.push(' / <a style="color:red;" href="javascript:void(0)" onclick="delAims('+key+', '+j+')">刪除</a>');
		html.push('</div>');

		var d2 = data[i]['content'];
		html.push('<div data-type="content" style="margin-left:40px;">服務/計畫內容:');
		for(var d in d2) {
			var z = parseInt(d)+1;
			html.push('<div data-content="'+z+'">'+z+'.<textarea name="plan1['+key+']['+j+'][content][]" rows="2" cols="100">'+d2[d]+'</textarea>');
			html.push('<a href="javascript:void(0)" onclick="addContent('+"'content'"+', '+key+', '+j+')">增加</a>');
			html.push(' / <a style="color:red;" href="javascript:void(0)" onclick="delContent('+"'content'"+', '+key+', '+j+', '+z+')">刪除</a>');
			html.push('</div>');
		}
		html.push('</div>');

		var d3 = data[i]['benefit'];
		html.push('<div data-type="benefit" style="margin-left:40px;">具體績效/預期效益:');
		for(var d in d3) {
			var z = parseInt(d)+1;
			html.push('<div data-benefit="'+z+'">'+z+'.<textarea name="plan1['+key+']['+j+'][benefit][]" rows="2" cols="100">'+d3[d]+'</textarea>');
			html.push('<a href="javascript:void(0)" onclick="addContent('+"'benefit'"+', '+key+', '+j+')">增加</a>');
			html.push(' / <a style="color:red;" href="javascript:void(0)" onclick="delContent('+"'benefit'"+', '+key+', '+j+', '+z+')">刪除</a>');
			html.push('</div>');
		}
		html.push('</div>');
		html.push('</div>');
	}

	$('#tab'+key).html(html.join(''));
}

function dataClear() {
}