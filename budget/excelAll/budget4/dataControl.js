  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '' && $('[name="planID"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    $('[data-tr]').remove();
    $('[name="Submit"]').attr('disabled','disabled');
  }


  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
         planID:$('[name="planID"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id},
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
        } else {
          $('#showTitle').html(data['showTitle']);
          var html = '';
          html = setData(data, act);
          $('#dataTable').append(html);
          $('[name="Submit"]').removeAttr('disabled');
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act) {
    var result = [];
    var j = 0;
    var dList = data['dataList'];
    var total = 0;
    var allPTotal = 0;
    var sourceTotal = 0;
    var donateTotal = 0;
    var per = parseInt(data['per']) / 100;
    for(var i in dList) {
      var d = dList[i];
      var pTotal = parseInt(d['price']) * parseInt(d['quantity'])
      var yTotal = pTotal - parseInt(d['sourceMoney']) - parseInt(d['donate']);
      total+=yTotal;
      allPTotal+=pTotal;
      sourceTotal+=parseInt(d['sourceMoney']);
      donateTotal+=parseInt(d['donate']);
      result.push('<tr data-tr>');
      result.push('<td class="colData">'+d['title']+'</td>');
      result.push('<td class="colData">'+toCurrency(d['price'])+'</td>');
      result.push('<td class="colData">'+d['quantity']+'</td>');
      result.push('<td class="colData">'+d['unit']+'</td>');
      result.push('<td class="colData">'+toCurrency(pTotal)+'</td>');
      result.push('<td class="colData">'+d['source']+'</td>');
      result.push('<td class="colData">'+toCurrency(d['sourceMoney'])+'</td>');
      result.push('<td class="colData">'+toCurrency(d['donate'])+'</td>');
      result.push('<td class="colData">'+toCurrency(yTotal)+'</td>');
      result.push('<td class="colData">'+d['buyDate']+'</td>');
     
      result.push('</tr>');
    }

    result.push('<tr data-tr>');
    result.push('<td class="colData">總計</td>');
    result.push('<td class="colData"></td>');
    result.push('<td class="colData"></td>');
    result.push('<td class="colData"></td>');
    result.push('<td class="colData">'+toCurrency(allPTotal)+'</td>');
    result.push('<td class="colData"></td>');
    result.push('<td class="colData">'+toCurrency(sourceTotal)+'</td>');
    result.push('<td class="colData">'+toCurrency(donateTotal)+'</td>');
    result.push('<td class="colData">'+toCurrency(total)+'</td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function toCurrency(num){
    var parts = num.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
  }