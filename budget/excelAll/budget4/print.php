<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/maintain/inc_vars.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	include_once("$root/system/db.php");
  	include_once('getData.php');


  	$db = new db();
	$sql = "select * from budget_other where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);

	$d = array('year'=>$data['year'],'depID'=>$data['depID'],'invNo'=>$data['invNo'], 'planID'=>$data['planID'], 'groupType' =>$data['groupType'], 'id'=>$_GET['id']);
	$result = getData($data);

	$t1 = explode(':', $result['showTitle']);
	$t2 = explode(',', $t1[1]);
	$showTitle = "(".$t1[0].")".implode('_', $t2);

	$filename = $data['year'].'年'.$showTitle.'-年度資本支出預算表.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('key'=>'title','title'=>'八萬以上設施設備\修繕工程項目','width'=>24),
		'B' =>array('key'=>'price','title'=>'單價','width'=>12),
		'C' =>array('key'=>'quantity','title'=>'數量','width'=>8),
		'D' =>array('key'=>'unit','title'=>'單位','width'=>12),
		'E' =>array('key'=>'pTotal','title'=>'經費概算','width'=>12),
		'F' =>array('key'=>'source','title'=>'補捐助單位','width'=>16),
		'G' =>array('key'=>'sourceMoney','title'=>'補助金額','width'=>12),
		'H' =>array('key'=>'yTotal','title'=>'自籌金額','width'=>12),
		'I' =>array('key'=>'buyDate','title'=>'購置日期','width'=>12)
	);

	
	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':I'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':I'.$i);//合併
    $sheet->setCellValue('A'.$i, $data['year'].'年度資本支出預算表');
    $sheet->getStyle('A1:I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $i++;
    $sheet->setCellValue('A'.$i,$result['showTitle']);
    $sheet->setCellValue('I'.$i,'預算表(四)');
	$sheet->getStyle('I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


    $i++;
	foreach ($header as $key => $value) {
		$sheet->setCellValue($key.$i, $value['title']);
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}

	$total = 0;
	$allPTotal = 0;
	$sourceTotal = 0;
	foreach ($result['dataList'] as $key => $value) {
		$i++;
		$pTotal = 0;
		foreach ($header as $k2 => $v2) {
			$showText = $value[$v2['key']];
			switch ($v2['key']) {
				case 'price':
					$showText = number_format($showText);
					break;
				case 'sourceMoney':
					$sourceTotal += (int)$showText;
					$showText = number_format($showText);
					break;
				case 'pTotal':
					$pTotal = (int)$value['price'] * (int)$value['quantity'];
					$allPTotal += $pTotal;
					$showText = number_format($pTotal);
					break;
				case 'yTotal':
					$yTotal = $pTotal - (int)$value['sourceMoney'];
					$total += $yTotal;
					$showText = number_format($yTotal);
					break;
			}

			$sheet->setCellValue($k2.$i, $showText);
		}
		$sheet->getStyle('I'.$i)->getAlignment()->setWrapText(true);
	}


	$i++;
	$sheet->setCellValue('A'.$i, '總計');
	$sheet->setCellValue('E'.$i, number_format($allPTotal));
	$sheet->setCellValue('G'.$i, number_format($sourceTotal));
	$sheet->setCellValue('H'.$i, number_format($total));


	$sheet->getStyle('A4:I'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線

	$i=$i+2;
	$sheet->mergeCells('H'.$i.':I'.$i);//合併
	$sheet->setCellValue('H'.$i, '填表人 : '.$emplyeeinfo[$data['creator']]);

	$sheet->getStyle('A1'.':I'.$i)->getFont()->setSize(12);
	$sheet->getStyle('A4:I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('A4:I'.$i)->getAlignment()->setWrapText(true); //自動換行

//	$sheet->getStyle('A1:I'.$i)->getFont()->setBold(true); //設定粗體
	//$sheet->getStyle('A'.$i.':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	//$sheet->getStyle('A'.$i.':I'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

?>