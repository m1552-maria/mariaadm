<?php
  include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
  include_once("$root/system/db.php");
  include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/budget1/getData.php");
  include_once('getData.php');
  

  $db = new db();
  $sql = "select * from planChange2 where id = '".$_GET['id']."'";
  $rs = $db->query($sql);
  $data = $db->fetch_array($rs);
  
  $d = array('year'=>$data['year'],'depID'=>$data['depID'],'invNo'=>$data['invNo'], 'groupType' =>$data['groupType']);

  $result = getData($d);
  $isAllNote = (!empty($_GET['isAllNote'])) ? 1 : 0; 


  /*echo "<pre>";
  print_r($result);
  exit;*/

  
  $t1 = explode(':', $result['showTitle']);
  $t2 = explode(',', $t1[1]);
  $showTitle = implode('_', $t2);

  $filename = $data['year'].'年'.$showTitle.'-預算追加減表.xlsx';

  header("Content-type:application/vnd.ms-excel");
  header("Content-Disposition:attachment;filename=$filename");
  header("Pragma:no-cache");
  header("Expires:0");
  
  require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
  $objPHPExcel = new PHPExcel(); 
  $objPHPExcel->setActiveSheetIndex(0);

  $header = array(
    'A' =>array('title'=>'預算項目','width'=>20),
    'B' =>array('title'=>'歲入預算','width'=>10),
    'C' =>array('title'=>'歲出預算','width'=>10),
    'D' =>array('title'=>'歲入追加減','width'=>10),
    'E' =>array('title'=>'歲出追加減','width'=>10),
    'F' =>array('title'=>'歲入追加減後預算','width'=>10),
    'G' =>array('title'=>'歲出追加減後預算','width'=>10),
    'H' =>array('title'=>'預算說明','width'=>40)
  );

  $i=1;
  $sheet = $objPHPExcel->getActiveSheet();
  $sheet->mergeCells('A'.$i.':H'.$i);//合併
  $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
  $i++;
  $sheet->mergeCells('A'.$i.':H'.$i);//合併
  $sheet->setCellValue('A'.$i, $data['year'].'年預算追加減表');
  $sheet->getStyle('A1:H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $i++;

  $sheet->mergeCells('A'.$i.':H'.$i);//合併
  $sheet->setCellValue('A'.$i,$result['showTitle']);
  $sheet->getStyle('A1'.':H'.$i)->getFont()->setSize(14);
 
   
  $i++;
  foreach ($header as $key => $value) {
    $sheet->setCellValue($key.$i, $value['title']);
    $sheet->getColumnDimension($key)->setWidth($value['width']);
  }
  $sheet->getStyle('A1:H'.$i)->getFont()->setBold(true); //設定粗體
  $sheet->getStyle('A'.$i.':H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
  $sheet->getStyle('A'.$i.':H'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 

  $tList = array(
    '+'=>array('total'=>0, 'changeTotal'=>0, 'allTotal'=>0),
    '-'=>array('total'=>0, 'changeTotal'=>0, 'allTotal'=>0)
  );
  $allTotal = 0;
  foreach ($result['arrangeBudget1Data']['dataList'] as $key => $value) {
    if($key == 'R101') continue;
    if($value['total'] == 0 && $value['changeTotal'] == 0) continue;
    $i++;
    $sheet->setCellValue('A'.$i,$value['title']);
    
    $tList[$value['vType']]['total'] += (int)$value['total'];
    $tList[$value['vType']]['changeTotal'] += (int)$value['changeTotal'];
    $tList[$value['vType']]['allTotal'] += (int)$value['total'] + (int)$value['changeTotal'];

    /*$inAmount = '';
    $outAmount = '';
    $changeIn = '';
    $changeOut = '';*/
    $allInAmount = '';
    $allOutAmount = '';


    if($value['vType'] == '+') {
      $sheet->setCellValue('B'.$i,number_format($value['total']));
      $sheet->setCellValue('D'.$i,number_format($value['changeTotal']));
      $allInAmount = $value['total'] + $value['changeTotal'];
    } else {
      $sheet->setCellValue('C'.$i,number_format($value['total']));
      $sheet->setCellValue('E'.$i,number_format($value['changeTotal']));
      $allOutAmount = $value['total'] + $value['changeTotal'];
    }

    if($allInAmount > 0)  $sheet->setCellValue('F'.$i,number_format($allInAmount));
    if($allOutAmount > 0)  $sheet->setCellValue('G'.$i,number_format($allOutAmount));


    $total = (int)$value['total'] + (int)$value['changeTotal'];
    $allTotal += $total;

    $sourceNotes = array();
    if(isset($value['sourceNotes'])) {
      foreach ($value['sourceNotes'] as $t => $tTotal) {
        if($isAllNote == 0 && !strpos($t, "(追加)") && !strpos($t, "(追減)")) continue;
        $sourceNotes[] = $t.'/'.number_format($tTotal);
      }
    }

    $sheet->setCellValue('H'.$i,implode(chr(13).chr(10), $sourceNotes));
    $sheet->getStyle('H'.$i)->getAlignment()->setWrapText(true);
  }
  $sheet->getStyle('A1'.':H'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

  $i++;
  setTotalData('小計',$i, $tList);

  $sheet->getStyle('B5:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

  $i++;
  setBalanceData('損益餘絀',$i, $tList);
  $sheet->getStyle('B'.$i.':G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



  $otherList = array('R60', 'R101');
  foreach ($otherList as $key => $value) {
    $i++;
    $d = $result['arrangeBudget1Data']['dataList'][$value];
    $sheet->setCellValue('A'.$i, $d['title']);
    $allAmount = (int)$d['total']+(int)$d['changeTotal'];
    if($value == 'R60') {
      $tList['-']['total'] -= (int)($d['total']);
      $tList['-']['changeTotal'] -= (int)($d['changeTotal']);
      $tList['-']['allTotal'] -= $allAmount;
    } else {
      $tList['-']['total'] += (int)($d['total']);
      $tList['-']['changeTotal'] += (int)($d['changeTotal']);
      $tList['-']['allTotal'] += $allAmount;
    }

    $sheet->setCellValue('C'.$i, ($d['total'] == 0) ? '' : number_format($d['total']));
    $sheet->setCellValue('E'.$i, ($d['changeTotal'] == 0) ? '' : number_format($d['changeTotal']));
    $sheet->setCellValue('G'.$i, ($allAmount == 0) ? '' : number_format($allAmount));

    $sourceNotes = array();
    if(isset($d['sourceNotes'])) {
      foreach ($d['sourceNotes'] as $t => $tTotal) {
        if($isAllNote == 0 && !strpos($t, "(追加)") && !strpos($t, "(追減)")) continue;
        $sourceNotes[] = $t.'/'.number_format($tTotal);
      }
    }

    $sheet->setCellValue('H'.$i,implode(chr(13).chr(10), $sourceNotes));
    $sheet->getStyle('H'.$i)->getAlignment()->setWrapText(true);
    
  }

  $i++;
  setTotalData('合計',$i, $tList);
  $sheet->getStyle('B'.($i-2).':G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

  //print_r($tList);
  $i++;
  setBalanceData('實質餘絀',$i, $tList);
  $sheet->getStyle('B'.$i.':G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


 /* $total = $tList['+']['total'] - $tList['-']['total'];
  $changeTotal = $tList['+']['changeTotal'] - $tList['-']['changeTotal'];

  if($total < 0) {
      $tList['+']['total'] += abs($total);
    } else {
      $tList['-']['total'] += abs($total);
    }

    if($changeTotal < 0) {
      $tList['+']['changeTotal'] += abs($changeTotal);
    } else {
      $tList['-']['changeTotal'] += abs($changeTotal);
    }

  $i++;
  $sheet->setCellValue('A'.$i, '基金會挹注款收入');
  $sheet->setCellValue('B'.$i, ($total < 0) ? number_format(abs($total)) : '');
  $sheet->setCellValue('D'.$i, ($changeTotal < 0) ? number_format(abs($changeTotal)) : '');

  $i++;
  $sheet->setCellValue('A'.$i, '發展及危機準備金');
  $sheet->setCellValue('B'.$i, ($total > 0) ? number_format(abs($total)) : '');
  $sheet->setCellValue('D'.$i, ($changeTotal > 0) ? number_format(abs($changeTotal)) : '');*/

 /* $i++;
  setTotalData('總計',$i, $tList);
  $sheet->getStyle('B'.($i-2).':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

  $i++;
  $sheet->setCellValue('A'.$i, '總餘絀');
  $sheet->mergeCells('B'.$i.':C'.$i);//合併
  $sheet->setCellValue('B'.$i, '0');
  $sheet->mergeCells('D'.$i.':E'.$i);//合併
  $sheet->setCellValue('D'.$i, '0');*/
 // $sheet->getStyle('B'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $sheet->getStyle('A4:H'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
  //$sheet->getRowDimension(4)->setRowHeight(50);
  $sheet->getStyle('A4:H'.$i)->getFont()->setSize(12);
  $sheet->getStyle('A4:H'.$i)->getAlignment()->setWrapText(true); //自動換行

  $i++;
  $i++;
  $sheet->setCellValue('A'.$i, '製表： '.$emplyeeinfo[$data['creator']]);
 /* $sheet->mergeCells('B'.$i.':C'.$i);//合併
  $sheet->setCellValue('B'.$i, '機構／中心主管：');*/
  $sheet->mergeCells('D'.$i.':E'.$i);//合併 
  $sheet->setCellValue('D'.$i, '財務室主任：');
  $sheet->setCellValue('G'.$i, '執行長：');
  $sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

  //$sheet->getStyle('A4:H'.$i)->getFont()->setName('Candara'); //設定字體


  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  $objWriter->setPreCalculateFormulas(false);
  $objWriter->save('php://output');

  function setTotalData($title, $i, $tList) {
    global $sheet;
    $sheet->setCellValue('A'.$i, $title);
    $sheet->setCellValue('B'.$i, number_format($tList['+']['total']));
    $sheet->setCellValue('C'.$i, number_format($tList['-']['total']));
    $sheet->setCellValue('D'.$i, number_format($tList['+']['changeTotal']));
    $sheet->setCellValue('E'.$i, number_format($tList['-']['changeTotal']));
    $sheet->setCellValue('F'.$i, number_format($tList['+']['allTotal']));
    $sheet->setCellValue('G'.$i, number_format($tList['-']['allTotal']));
    $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setBold(true); //粗體
  }

  function setBalanceData($title, $i, $tList) {
    global $sheet;
    $sheet->setCellValue('A'.$i, $title);

    $total = $tList['+']['total'] -$tList['-']['total'];
    $total = ($total >= 0) ? '$'.number_format($total) : '-$'.number_format(abs($total));
    $sheet->mergeCells('B'.$i.':C'.$i);//合併
    $sheet->setCellValue('B'.$i, $total);

    $changeTotal = $tList['+']['changeTotal'] - $tList['-']['changeTotal'];
    $changeTotal = ($changeTotal >= 0) ? '$'.number_format($changeTotal) : '-$'.number_format(abs($changeTotal));
    $sheet->mergeCells('D'.$i.':E'.$i);//合併
    $sheet->setCellValue('D'.$i, $changeTotal);

    $allTotal = $tList['+']['allTotal'] - $tList['-']['allTotal'];
    $allTotal = ($allTotal >= 0) ? '$'.number_format($allTotal) : '-$'.number_format(abs($allTotal));
    $sheet->mergeCells('F'.$i.':G'.$i);//合併
    $sheet->setCellValue('F'.$i, $allTotal);


    $sheet->getStyle('A'.$i.':H'.$i)->getFont()->setBold(true);
    $sheet->getStyle('A'.$i.':H'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 
  }
?>
