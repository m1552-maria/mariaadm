  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    $('#showData').html('');
    $('[name="Submit"]').attr('disabled','disabled');
  }

  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
          $('[name="invNo"]').val(''); 
        } else {
          var html = '';
          html = setData(data, act);
          $('#showData').html(html);
          $('#showTitle').html(data['showTitle']);
          $('[name="Submit"]').removeAttr('disabled');
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act) {
    var result = [];
    var j = 0;
    console.log(data);

    result.push('<table  class="tableData" width="1000px;" border="1">');
    result.push('<tr>');
    result.push('<td class="cLabel tdTitle" width="15%">預算項目</td>');
    result.push('<td class="cLabel tdTitle" width="10%">歲入預算</td>');
    result.push('<td class="cLabel tdTitle" width="10%">歲出預算</td>');
    result.push('<td class="cLabel tdTitle" width="10%">歲入<br>追加減</td>');
    result.push('<td class="cLabel tdTitle" width="10%">歲出<br>追加減</td>');
    result.push('<td class="cLabel tdTitle" width="10%">歲入<br>追加減後預算</td>');
    result.push('<td class="cLabel tdTitle" width="10%">歲出<br>追加減後預算</td>');
    result.push('<td class="cLabel tdTitle" width="25%">預算說明</td>');
    result.push('</tr>');

    var tList = {
      '+':{'total':0,'changeTotal':0, 'allTotal':0},
      '-':{'total':0,'changeTotal':0, 'allTotal':0}
    }
    var allTotal = 0;
    console.log(data['arrangeBudget1Data']['dataList']);

    for(var i in data['arrangeBudget1Data']['dataList']) {

      if(i == 'R101') continue;
      var d = data['arrangeBudget1Data']['dataList'][i];
     // console.log(d);

      if(d['total'] == 0 && d['changeTotal'] == 0) continue;
      result.push('<tr data-tr="">');
      //預算項目
      result.push('<td class="colData">'+d['title']+'</td>');
      tList[d['vType']]['total'] += parseInt(d['total']);
      tList[d['vType']]['changeTotal'] += parseInt(d['changeTotal']);
      tList[d['vType']]['allTotal'] += (parseInt(d['total']) + parseInt(d['changeTotal']));

      var inAmount = '';
      var outAmount = '';
      var changeIn = '';
      var changeOut = '';
      var allInAmount = '';
      var allOutAmount = '';

      if(d['vType'] == '+') {
        inAmount = d['total'];
        allInAmount = inAmount + d['changeTotal'];
        changeIn = toCurrency(d['changeTotal']);

      } else {
        outAmount = d['total'];
        allOutAmount = outAmount + d['changeTotal'];
        changeOut = toCurrency(d['changeTotal']);

      }

      inAmount = (inAmount == 0) ? '' : toCurrency(inAmount);
      outAmount = (outAmount == 0) ? '' : toCurrency(outAmount);
      changeIn = (changeIn == 0) ? '' : changeIn;
      changeOut = (changeOut == 0) ? '' : changeOut;
      allInAmount = (allInAmount == 0) ? '' : toCurrency(allInAmount);
      allOutAmount = (allOutAmount == 0) ? '' : toCurrency(allOutAmount);

      //收入預算
      result.push('<td class="colData tdNum" data-in="'+i+'">'+inAmount+'</td>');
      //支出預算
      result.push('<td class="colData tdNum" data-out="'+i+'">'+outAmount+'</td>');
      //收入預算追加減
      result.push('<td class="colData tdNum">'+changeIn+'</td>');
      //支出預算追加減
      result.push('<td class="colData tdNum">'+changeOut+'</td>');
      //收入預算(追加減後)
      result.push('<td class="colData tdNum">'+allInAmount+'</td>');
      //支出預算(追加減後)
      result.push('<td class="colData tdNum">'+allOutAmount+'</td>');

      //年度預算增減數
      var total = parseInt(d['total']) + parseInt(d['changeTotal']);
      allTotal += total;

      //預算說明
      var sourceNotes = [];
      if(d['sourceNotes']) {
        for(var j in d['sourceNotes']) {
          sourceNotes.push(j+'/'+toCurrency(d['sourceNotes'][j]));
        }
      }
      sourceNotes = sourceNotes.join('<br>');
      result.push('<td class="colData">'+sourceNotes+'</td>');
      result.push('</tr>');
    }

    /*小計*/
    console.log(tList);
    html = setTotalData('小計', tList, allTotal, 0);
    result.push(html);

    /*損益餘絀*/
    html = setBalanceData('損益餘絀', tList, 0);
    result.push(html);

    otherList = ['R60', 'R101'];
    for(var i in otherList) {
      var d = data['arrangeBudget1Data']['dataList'][otherList[i]];
      console.log(d);
      result.push('<tr data-tr="">');
      result.push('<td class="colData">'+d['title']+'</td>');
      result.push('<td class="colData tdNum"></td>');

      var dd = '';
      if(otherList[i] == 'R60') {
        tList['-']['total'] -= parseInt(d['total']);
        tList['-']['changeTotal'] -= parseInt(d['changeTotal']);
        tList['-']['allTotal'] -= parseInt(d['total']+d['changeTotal']);
        dd = 'data-other="R60"';
      } else {
        tList['-']['total'] += parseInt(d['total']);
        tList['-']['changeTotal'] += parseInt(d['changeTotal']);
        tList['-']['allTotal'] += parseInt(d['total']+d['changeTotal']);
      }

      var changeTotal = toCurrency(d['changeTotal']);
      

      var total = (d['total'] == 0) ? '' : d['total'];
      changeTotal = (changeTotal == 0) ? '' : changeTotal;
      var allAmount = toCurrency(d['total']+d['changeTotal']);
      allAmount = (allAmount == 0) ? '' : allAmount;

      result.push('<td class="colData tdNum">'+toCurrency(total)+'</td>');
      result.push('<td class="colData tdNum"></td>');

      result.push('<td class="colData tdNum" >'+changeTotal+'</td>');
      result.push('<td class="colData tdNum"></td>');

      result.push('<td class="colData tdNum" >'+allAmount+'</td>');

    
      var sourceNotes = [];
      if(d['sourceNotes']) {
        for(var j in d['sourceNotes']) {
          sourceNotes.push(j+'/'+toCurrency(d['sourceNotes'][j]));
        }
      }
      sourceNotes = sourceNotes.join('<br>');
     /* if(act != 'print' && otherList[i] == 'R101') {
        if(otherNotes != '') otherNotes = '<textarea disabled style="width:95%" rows="4">' + otherNotes +'</textarea>';
        notes = '<textarea style="width:95%;margin-top:3px;" rows="4" id="notes'+otherList[i]+'" name="notes['+otherList[i]+']">' + notes +'</textarea>';
      }*/
      result.push('<td class="colData">'+sourceNotes+'</td>');
      result.push('</tr>');
    }
     /*合計*/
    html = setTotalData('合計', tList, '', 1);
    result.push(html);

    /*實質餘絀*/
    html = setBalanceData('實質餘絀', tList, 1);
    result.push(html);

   /* var total = tList['+']['total'] - tList['-']['total'];
    var changeTotal = tList['+']['changeTotal'] - tList['-']['changeTotal'];
    if(total < 0) {
      tList['+']['total'] += Math.abs(total);
    } else {
      tList['-']['total'] += Math.abs(total);
    }

    if(changeTotal < 0) {
      tList['+']['changeTotal'] += Math.abs(changeTotal);
    } else {
      tList['-']['changeTotal'] += Math.abs(changeTotal);
    }
    
    result.push('<tr data-tr="">');
    result.push('<td class="colData">基金會挹注款收入</td>');
    var t = (total < 0) ? Math.abs(total) : '';
    result.push('<td class="colData tdNum">'+toCurrency(t)+'</td>');
    result.push('<td class="colData tdNum"></td>');
    var lt = (changeTotal < 0) ? Math.abs(changeTotal) : '';
    result.push('<td class="colData tdNum">'+toCurrency(lt)+'</td>');
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    result.push('<tr data-tr="">');
    result.push('<td class="colData">發展及危機準備金</td>');
    var t = (total > 0) ? Math.abs(total) : '';
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData tdNum">'+toCurrency(t)+'</td>');
    var lt = (changeTotal > 0) ? Math.abs(changeTotal) : '';
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData tdNum">'+toCurrency(lt)+'</td>');
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');*/

    /*總計*/
    /*html = setTotalData('總計', tList, '', 2);
    result.push(html);*/
    
    /*總餘絀*/
   /* result.push('<tr data-tr="">');
    result.push('<td class="cLabel tdTitle" style="background-color:inherit">總餘絀</td>');
    result.push('<td class="cLabel" style="background-color:inherit" colspan="2">$0</td>');
    result.push('<td class="cLabel" style="background-color:inherit" colspan="2">$0</td>');
    result.push('<td class="cLabel" style="background-color:inherit"></td>');
    result.push('<td class="cLabel" style="background-color:inherit"></td>');
    result.push('</tr>');*/



    result.push('</table>');
    
    


    return result.join('');
  }

  function setTotalData(title, tList, allTotal, dTitle) {
    result = [];
    result.push('<tr data-tr="">');
    result.push('<td class="colData tdTitle">'+title+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['+']['total'])+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['-']['total'])+'</td>');
    result.push('<td class="colData tdNum" >'+toCurrency(tList['+']['changeTotal'])+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['-']['changeTotal'])+'</td>');
    result.push('<td class="colData tdNum" >'+toCurrency(tList['+']['allTotal'])+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['-']['allTotal'])+'</td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function setBalanceData(title, tList, dTitle) {
    console.log(tList);
    result = [];
    var total = tList['+']['total'] - tList['-']['total'];
    total = (total >= 0) ? '$'+toCurrency(total) : '-$'+toCurrency(Math.abs(total));

    var changeTotal = tList['+']['changeTotal'] - tList['-']['changeTotal'];
    changeTotal = (changeTotal >= 0) ? '$'+toCurrency(changeTotal) : '($'+toCurrency(Math.abs(changeTotal))+')';

    var allTotal = tList['+']['allTotal'] - tList['-']['allTotal'];
    allTotal = (allTotal >= 0) ? '$'+toCurrency(allTotal) : '-$'+toCurrency(Math.abs(allTotal));

    result.push('<tr data-tr="">');
    result.push('<td class="cLabel tdTitle">'+title+'</td>');
    result.push('<td class="cLabel" colspan="2">'+total+'</td>');
    result.push('<td class="cLabel" colspan="2">'+changeTotal+'</td>');
    result.push('<td class="cLabel" colspan="2">'+allTotal+'</td>');
    result.push('<td class="cLabel"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function toCurrency(num){
    var parts = num.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
  }

 