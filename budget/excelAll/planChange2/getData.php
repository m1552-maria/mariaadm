<?php

	function getData($data = array()) {
		/*echo "<pre>";
		print_r($data);
		exit;*/
		//print_r($data);
		global $departmentinfo, $invNo;
		$result = array('error'=>0,'msg'=>'', 'dataList'=>array());

		$db = new db();	

		$where = array();
		if(!empty($data['depID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$depIDList = explode(',', $data['depID']);
			$_where = array();
			foreach ($depIDList as $key => $value) {
				$_where[] = " p.depID like '".$value."%' ";
				$showTitle[] = $departmentinfo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '機構/中心:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['invNo'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$invNoList = explode(',', $data['invNo']);
			$_where = array();
			foreach ($invNoList as $key => $value) {
				$_where[] = " p.invNo like '".$value."%' ";
				$showTitle[] = $invNo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '統一編號:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		$result['showTitle'] = implode(' / ', $result['showTitle']);

		$where = implode(' and ', $where);


		$sql = "select i.*, p.bugetChange from plan p inner join plan_items i on p.id = i.pid where p.year='".$data['year']."' and ".$where; 


		$rs = $db->query($sql);
		$dataList = array();
		$sidList1 = array();
		$sidList2 = array();
		if(isset($data['id'])) unset($data['id']);
		$i = 0;
		while ($r=$db->fetch_array($rs)) {
			$i++;
			if($r['bugetChange'] == 0) $sidList1[] = "'".$r['id']."'";
			else $sidList2[] = "'".$r['id']."'";
		}
		if($i == 0) {
			$result = array('error'=>1,'msg'=>'所選條件沒有資料');
			return $result;
		}


		$data['sid'] = implode(',', $sidList1);
		$result['arrangeBudget1Data'] = getBudget1Data($data);

		$data['sid'] = implode(',', $sidList2);
		$result['changeBudget1Data'] = getBudget1Data($data);

		foreach ($result['arrangeBudget1Data']['dataList'] as $key => $value) {
			$result['arrangeBudget1Data']['dataList'][$key]['changeTotal'] = 0;

			$cData = $result['changeBudget1Data']['dataList'];
			if(isset($cData[$key])) {
				$result['arrangeBudget1Data']['dataList'][$key]['changeTotal'] = $cData[$key]['total'];
				foreach ($cData[$key]['sourceNotes'] as $title => $Ttotal) {
					$result['arrangeBudget1Data']['dataList'][$key]['sourceNotes'][$title] += $Ttotal;
				}
				
			}
			
		}

		return $result;
	}
?>