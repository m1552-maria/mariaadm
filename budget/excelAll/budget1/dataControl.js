  $(function(){
    /*$(document).on('change','[data-last-in]', function(){
      var k = $(this).attr('data-last-in');
      var total = parseInt($('[data-in="'+k+'"]').html()) - parseInt($(this).val());
      $('[data-total="'+k+'"]').html(total);
    })*/
  })


  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '' && $('[name="planID"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    $('[data-tr]').remove();
    $('[name="Submit"]').attr('disabled','disabled');
  }


  /**/
 /* function calculationSum() {
    var inSum = 0;
    $('[data-last-in]').each(function(){
      inSum += parseInt($(this).val());
    });
    $('[data-in-sum="0"]').html(inSum);


    var outSum = 0;
    $('[data-last-out]').each(function(){
      outSum += parseInt($(this).val());
    });
    $('[data-out-sum="0"]').html(outSum);

    var allTotal = 0;
    $('[data-total]').each(function(){
      allTotal+=parseInt($(this).html());
    });
    $('data-all-total="0"]').html(allTotal);

    $('[data-balance="0"]').html();
  }*/

  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        planID:$('[name="planID"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        console.log(data);
        dataClear();
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
          $('[name="invNo"]').val(''); 
        } else {
          var html = '';
          html = setData(data['dataList'], act, data['hasLast']);
          $('#dataTable').append(html);
          $('#showTitle').html(data['showTitle']);
          if(act != 'print') {
            /*for(var instanceName in CKEDITOR.instances) {
              CKEDITOR.remove(CKEDITOR.instances[instanceName]);   
              //CKEDITOR.replaceAll(); 
            }
            $('[name^="notes"]').each(function(){
              id = $(this).attr('id');
              CKEDITOR.replace(id);
            });*/
           
            $('[name="Submit"]').removeAttr('disabled');
          }
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act, hasLast) {
    var result = [];
    var tList = {
      '+':{'total':0,'lastTotal':0},
      '-':{'total':0,'lastTotal':0}
    }
    var allTotal = 0;
    for(var i in data) {
      if(i == 'R101') continue;
      var d = data[i];
      if(d['total'] == 0 && d['lastTotal'] == 0) continue;
      result.push('<tr data-tr="">');
      //預算項目
      result.push('<td class="colData">'+d['title']+'</td>');
      tList[d['vType']]['total'] += parseInt(d['total']);
      tList[d['vType']]['lastTotal'] += parseInt(d['lastTotal']);
      
      var inAmount = '';
      var outAmount = '';
      var lastIn = '';
      var lastOut = '';

      if(d['vType'] == '+') {
        inAmount = d['total'];
        lastIn = toCurrency(d['lastTotal']);
        if(hasLast == 0 && act != 'print') {
          lastIn = '<input name="lastData['+i+']" style="width:80px;" data-last-in="'+i+'" type="text" value="'+d['lastTotal']+'" >';
        }
      } else {
        outAmount = d['total'];
        lastOut = toCurrency(d['lastTotal']);
        if(hasLast == 0 && act != 'print') {
          lastOut = '<input name="lastData['+i+']" style="width:80px;" data-last-out="'+i+'" type="text" value="'+d['lastTotal']+'">';
        }
      }

      inAmount = (inAmount == 0) ? '' : toCurrency(inAmount);
      outAmount = (outAmount == 0) ? '' : toCurrency(outAmount);
      lastIn = (lastIn == 0) ? '' : lastIn;
      lastOut = (lastOut == 0) ? '' : lastOut;

      //收入預算
      result.push('<td class="colData tdNum" data-in="'+i+'">'+inAmount+'</td>');
      //支出預算
      result.push('<td class="colData tdNum" data-out="'+i+'">'+outAmount+'</td>');
      //上年度收入預算
      result.push('<td class="colData tdNum">'+lastIn+'</td>');
      //上年度支出預算
      result.push('<td class="colData tdNum">'+lastOut+'</td>');

      //年度預算增減數
      var total = parseInt(d['total']) - parseInt(d['lastTotal']);
      allTotal += total;
      total = (total == 0) ? '' : total;
      result.push('<td class="colData tdNum" data-total="'+i+'">'+toCurrency(total)+'</td>');

      //預算說明
      var otherNotes = d['otherNotes'].join('\n');
      var notes = d['notes'];
      if(act != 'print') {
        if(otherNotes != '') otherNotes = '<textarea disabled style="width:95%" rows="4">' + otherNotes +'</textarea>';
        notes = '<textarea style="width:95%;margin-top:3px;" rows="4" id="notes'+i+'" name="notes['+i+']">' + notes +'</textarea>';
      }
      result.push('<td class="colData">'+otherNotes+notes+'</td>');
      result.push('</tr>');
    }

    /*小計*/
    html = setTotalData('小計', tList, allTotal, 0);
    result.push(html);

    /*損益餘絀*/
    html = setBalanceData('損益餘絀', tList, 0);
    result.push(html);

    otherList = ['R60', 'R101'];
    for(var i in otherList) {
      var d = data[otherList[i]];
      result.push('<tr data-tr="">');
      result.push('<td class="colData">'+d['title']+'</td>');
      result.push('<td class="colData tdNum"></td>');

      var dd = '';
      if(otherList[i] == 'R60') {
        tList['-']['total'] -= parseInt(d['total']);
        tList['-']['lastTotal'] -= parseInt(d['lastTotal']);
        dd = 'data-other="R60"';
      } else {
        tList['-']['total'] += parseInt(d['total']);
        tList['-']['lastTotal'] += parseInt(d['lastTotal']);
      }

      var lastTotal = toCurrency(d['lastTotal']);
      if(hasLast == 0 && act != 'print' && otherList[i] == 'R101') {
        lastTotal = '<input name="lastData[R101]" style="width:80px;" data-out="'+otherList[i]+'" value="'+d['lastTotal']+'">';
      }

      var total = (d['total'] == 0) ? '' : d['total'];
      lastTotal = (lastTotal == 0) ? '' : lastTotal;

      result.push('<td class="colData tdNum">'+toCurrency(total)+'</td>');
      result.push('<td class="colData tdNum"></td>');


      result.push('<td class="colData tdNum" '+dd+'>'+lastTotal+'</td>');

      otherNotes = d['otherNotes'];
      notes = d['notes'];
      if(act != 'print' && otherList[i] == 'R101') {
        if(otherNotes != '') otherNotes = '<textarea disabled style="width:95%" rows="4">' + otherNotes +'</textarea>';
        notes = '<textarea style="width:95%;margin-top:3px;" rows="4" id="notes'+otherList[i]+'" name="notes['+otherList[i]+']">' + notes +'</textarea>';
      }

      result.push('<td class="colData tdNum"></td>');
      result.push('<td class="colData">'+otherNotes+notes+'</td>');
      result.push('</tr>');
    }
     /*合計*/
    html = setTotalData('合計', tList, '', 1);
    result.push(html);

    /*實質餘絀*/
    html = setBalanceData('實質餘絀-', tList, 1);
    result.push(html);

    var total = tList['+']['total'] - tList['-']['total'];
    var lastTotal = tList['+']['lastTotal'] - tList['-']['lastTotal'];
    if(total < 0) {
      tList['+']['total'] += Math.abs(total);
    } else {
      tList['-']['total'] += Math.abs(total);
    }

    if(lastTotal < 0) {
      tList['+']['lastTotal'] += Math.abs(lastTotal);
    } else {
      tList['-']['lastTotal'] += Math.abs(lastTotal);
    }
    
    result.push('<tr data-tr="">');
    result.push('<td class="colData">基金會挹注款收入</td>');
    var t = (total < 0) ? Math.abs(total) : '';
    result.push('<td class="colData tdNum">'+toCurrency(t)+'</td>');
    result.push('<td class="colData tdNum"></td>');
    var lt = (lastTotal < 0) ? Math.abs(lastTotal) : '';
    result.push('<td class="colData tdNum">'+toCurrency(lt)+'</td>');
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    result.push('<tr data-tr="">');
    result.push('<td class="colData">發展及危機準備金</td>');
    var t = (total > 0) ? Math.abs(total) : '';
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData tdNum">'+toCurrency(t)+'</td>');
    var lt = (lastTotal > 0) ? Math.abs(lastTotal) : '';
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData tdNum">'+toCurrency(lt)+'</td>');
    result.push('<td class="colData tdNum"></td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    /*總計*/
    html = setTotalData('總計', tList, '', 2);
    result.push(html);
    
    /*總餘絀*/
    result.push('<tr data-tr="">');
    result.push('<td class="cLabel tdTitle" style="background-color:inherit">總餘絀</td>');
    result.push('<td class="cLabel" style="background-color:inherit" colspan="2">$0</td>');
    result.push('<td class="cLabel" style="background-color:inherit" colspan="2">$0</td>');
    result.push('<td class="cLabel" style="background-color:inherit"></td>');
    result.push('<td class="cLabel" style="background-color:inherit"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function setTotalData(title, tList, allTotal, dTitle) {
    result = [];
    result.push('<tr data-tr="">');
    result.push('<td class="colData tdTitle">'+title+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['+']['total'])+'</td>');
    result.push('<td class="colData tdNum">'+toCurrency(tList['-']['total'])+'</td>');
    result.push('<td class="colData tdNum" data-in-sum="'+dTitle+'">'+toCurrency(tList['+']['lastTotal'])+'</td>');
    result.push('<td class="colData tdNum" data-in-out="'+dTitle+'">'+toCurrency(tList['-']['lastTotal'])+'</td>');
    result.push('<td class="colData tdNum" data-all-total="'+dTitle+'">'+toCurrency(allTotal)+'</td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function setBalanceData(title, tList, dTitle) {
    result = [];
    var total = tList['+']['total'] - tList['-']['total'];
    total = (total >= 0) ? '$'+toCurrency(total) : '-$'+toCurrency(Math.abs(total));
   

    var lastTotal = tList['+']['lastTotal'] - tList['-']['lastTotal'];
    lastTotal = (lastTotal >= 0) ? '$'+toCurrency(lastTotal) : '-$'+toCurrency(Math.abs(lastTotal));
   

    result.push('<tr data-tr="">');
    result.push('<td class="cLabel tdTitle">'+title+'</td>');
    result.push('<td class="cLabel" colspan="2">'+total+'</td>');
    result.push('<td class="cLabel" colspan="3" data-balance="'+dTitle+'">'+lastTotal+'</td>');
    result.push('<td class="cLabel"></td>');
    result.push('</tr>');

    return result.join('');
  }

  function toCurrency(num){
    var parts = num.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
}