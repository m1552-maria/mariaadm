<?php
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";

	$db = new db();
	$sql = "select * from budget1 where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>瑪利亞預算管理系統</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
<style type="text/css">
  .cLabel {
    text-align: center;
    background-color: #EEE;
    border: 1px solid #333333;
    padding: 4px 8px 4px 4px;
  }
  .tableData {
  	margin: 0 auto;
  	border: 1px solid #00; border-collapse: collapse;
  }
  .tableData td{
  	padding: 2px 4px;
  }

  .tdNum {
  text-align: right;
 }
 .tdTitle {
  text-align: center;
  font-size: 14px;
  font-weight: bolder;
 }

</style>
</head>
<body>
	
	<h3 style="text-align: center;">財團法人瑪利亞社會福利基金會</h3>
	<h3 style="text-align: center;"><?php echo $data['year'];?>年度預算表</h3>
	<input type="hidden" name="year" value="<?php echo $data['year'];?>">
	<input type="hidden" name="depID" value="<?php echo $data['depID'];?>">
	<table  width="920px;" style="margin: 0 auto;">
		<tr><td style="font-weight: bold;">機構/中心：<?php echo $departmentinfo[$data['depID']]?></td></tr>
	</table>
	
	<table id="dataTable" class="tableData"  width="920px;" border='1'>
	  <tr>
	    <td class="cLabel tdTitle" width="15%">預算項目</td>
	    <td class="cLabel tdTitle" width="10%">收入預算</td>
	    <td class="cLabel tdTitle" width="10%">支出預算</td>
	    <td class="cLabel tdTitle" width="10%">上年度收入預算</td>
	    <td class="cLabel tdTitle" width="10%">上年度支出預算</td>
	    <td class="cLabel tdTitle" width="10%">年度預算增減數</td>
	    <td class="cLabel tdTitle">預算說明</td>
	  </tr>
	  
	</table>
	<table width="920px;" style="margin: 0 auto;">
		<tr>
			<td width="6%">製表：</td>
			<td width="9%"><?php echo $emplyeeinfo[$data['creator']]?></td>
			<td width="28%">機構／中心主管：</td>
			<td></td>
			<td width="26%">財務室主任：</td>
			<td></td>
			<td>執行長：</td>
			<td></td>
		</tr>
	</table>

	<div style="text-align:center;margin-top:20px;">
		<button type="button" id="print_button" style="margin-left: 12px;" onclick="print_the_page();">下載列印</button>
	    <button type="button" id="print_close" onClick="window.close();">關閉</button>
	</div>
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="dataControl.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
		ajaxCheck('print', <?php echo $_GET['id'];?>);
	})
	function print_the_page(){
        document.getElementById("print_button").style.visibility = "hidden";    //顯示按鈕
         document.getElementById("print_close").style.visibility = "hidden";    //顯示按鈕
        javascript:print();
        document.getElementById("print_button").style.visibility = "visible";   //不顯示按鈕
        document.getElementById("print_close").style.visibility = "visible";   //不顯示按鈕
    }
</script>


</body>
</html>