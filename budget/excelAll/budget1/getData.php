<?php

	function getBudget1Data($data = array(), $groupType='depID') {
		global $departmentinfo, $invNo; 
		$result = array('error'=>0,'msg'=>'','notes'=>'', 'hasLast'=>0, 'showTitle'=>array());

		$db = new db();	
		$sql = "select * from plan where year='".$data['year']."'";
		$rs = $db->query($sql);
		$planTitleList = array(); 
		while($r=$db->fetch_array($rs)) {
			$planTitleList[$r['id']] = $r['title'];
		}

		$where = array();
		if(!empty($data['depID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$depIDList = explode(',', $data['depID']);
			$_where = array();
			foreach ($depIDList as $key => $value) {
				$_where[] = " p.depID like '".$value."%' ";
				$showTitle[] = $departmentinfo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '機構/中心:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['invNo'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$invNoList = explode(',', $data['invNo']);
			$_where = array();
			foreach ($invNoList as $key => $value) {
				$_where[] = " p.invNo like '".$value."%' ";
				$showTitle[] = $invNo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '統一編號:'.implode(',', $showTitle);
			$where[] = $_where;
		}
		
		if(!empty($data['planID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$planList = explode(',', $data['planID']);
			$_where = array();
			foreach ($planList as $key => $value) {
				$showTitle[] = $planTitleList[$value];
				$planList[$key] = "'".$value."'";
			}
			$_where = "p.id in (".implode(',', $planList).")";
			$result['showTitle'][] = '計劃主檔:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		$result['showTitle'] = implode(' / ', $result['showTitle']);

		$where = implode(' and ', $where);
		
		
		$db = new db();	

		$sql = "select 1 from plan p where p.year='".$data['year']."' and ".$where;
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) { 
			$result['error'] = 1;
			$result['msg'] = ($data['year']).'年 所選'.$result['showTitle'].' 未存在計畫';
			return $result;
		}

		$sql = "select * from subject_map where levels = 1 or levels = 2 order by id";
		$rs = $db->query($sql);
		$dataList = array();
		$subList =array();
		while($r=$db->fetch_array($rs)) {
			if($r['levels'] == 1) {
				$dataList[$r['id']] = array('title'=>$r['title'], 'vType'=>$r['vType'], 'total'=>0, 'notes'=>array(), 'otherNotes'=>array(), 'lastTotal'=>0);
			} else {
				$subList[$r['id']] = $r['title'];
			}
		}



		/*取經費預算*/
		$sql = "select p.year, p.bugetChange, p.depID, i.title as sourceTitle, s.pid as subjectPid, s.title as subTitle, c.subject, c.amount, c.notes from plan p inner join plan_currency c on p.id=c.fid inner join subject_map s on c.subject=s.id inner join plan_items i on c.sid = i.id where p.year in ('".$data['year']."','".($data['year']-1)."') and ".$where;
		if(isset($data['sid'])) $sql.= " and c.sid in (".$data['sid'].")";
		$sql .= " order by p.year, c.subject";

		$rs = $db->query($sql);
		while ($r=$db->fetch_array($rs)) {
			if($r['bugetChange'] == 2) $r['amount'] = -1*$r['amount'];
			if($r['year'] == $data['year']) {
				$dataList[$r['subjectPid']]['total'] += $r['amount'];
				$dataList[$r['subjectPid']]['second'][$r['subject']]['total'] += $r['amount'];
				$dataList[$r['subjectPid']]['second'][$r['subject']]['title'] = $r['subTitle'];
				if($r['notes'] != '') {
					$dataList[$r['subjectPid']]['otherNotes'][] = $r['notes'];
					$dataList[$r['subjectPid']]['second'][$r['subject']]['notes'][]= $r['notes'];
				}
				if($r['amount'] != 0) {
					//加入計畫來源
					$sourceNotes = $departmentinfo[$r['depID']].'-'.$r['sourceTitle'];
					if($r['bugetChange'] == 1) $sourceNotes .= '(追加)';
					elseif ($r['bugetChange'] == 2) $sourceNotes .= '(追減)';
					$dataList[$r['subjectPid']]['sourceNotes'][$sourceNotes] += $r['amount'];
				}
			} else {
				$dataList[$r['subjectPid']]['lastTotal'] += $r['amount'];
			}
		}

/*echo "<pre>";
print_r($dataList);
exit;*/
		/*人事費 & 備註*/
		$pTalent = getPlanTalent($data, $where);
		foreach ($pTalent as $key => $value) {
			if($key == '') continue;
			$dataList[$key]['total'] += $value['total'];
			$dataList[$key]['lastTotal'] += $value['lastTotal'];
			if(isset($value['second'])) {
				foreach ($value['second'] as $k2=> $v2) {
					$dataList[$key]['second'][$k2]['total'] += $v2['total'];
					$dataList[$key]['second'][$k2]['title'] = $v2['title'];
					$dataList[$key]['second'][$k2]['notes'][]= "人力編制：各項人事總額";
				}
			}
			if(isset($value['notes']) && count($value['notes']) != 0) {
				$dataList[$key]['otherNotes'][] = implode(chr(13).chr(10), $value['notes']);
				//$dataList[$key]['otherNotes'] .= chr(13).chr(10);
			}
			if(isset($value['sourceNotes'])) {
				foreach ($value['sourceNotes'] as $k2=> $v2) {
					$dataList[$key]['sourceNotes'][$k2] += $v2;
				}
			}

		}

		/*服務收入表 R01(服務費) R05(補助款)*/
		$sql = "select p.year, p.bugetChange, p.depID, i.title as sourceTitle, o.aidTrainPay, o.selfTrainPay, o.allTrafficPay, o.monthNum from plan p inner join plan_service o on p.id=o.fid inner join plan_items i on o.sid = i.id where p.year in ('".$data['year']."','".($data['year']-1)."') and ".$where;
		if(isset($data['sid'])) $sql.= " and o.sid in (".$data['sid'].")";
		$sql.= " order by p.year";

		$rs = $db->query($sql);
		$R01 = array('selfTrainPay'=>0,'allTrafficPay'=>0);
		//$R05 = array('aidTrainPay'=>0);
		$R05 = array();
		$R01['aidTrainPay'] = 0;
		while ($r=$db->fetch_array($rs)) {
			if($r['bugetChange'] == 2) $r['monthNum'] = -1*$r['monthNum'];
			if($r['year'] == $data['year']) {
				$R01['selfTrainPay'] += $r['selfTrainPay']*$r['monthNum'];
				$R01['allTrafficPay'] += $r['allTrafficPay'] * $r['monthNum'];
				//$R05['aidTrainPay'] += $r['aidTrainPay']*$r['monthNum'];
				$R01['aidTrainPay'] += $r['aidTrainPay']*$r['monthNum'];
				$dataList['R01']['total'] += ($r['selfTrainPay']+$r['allTrafficPay'])*$r['monthNum'];
				//$dataList['R05']['total'] += $r['aidTrainPay']*$r['monthNum'];
				$dataList['R01']['total'] += $r['aidTrainPay']*$r['monthNum'];

				$dataList['R01']['second']['B4610']['total'] += ($r['selfTrainPay']+$r['allTrafficPay'])*$r['monthNum'];
				//$dataList['R05']['second']['B4621']['total'] += $r['aidTrainPay']*$r['monthNum'];
				$dataList['R01']['second']['B4610']['total'] += $r['aidTrainPay']*$r['monthNum'];

				$sTotal = ($r['selfTrainPay']+$r['allTrafficPay'])*$r['monthNum'] + ($r['aidTrainPay']*$r['monthNum']);
				if($sTotal != 0) {
					//加入計畫來源
					$sourceNotes = $departmentinfo[$r['depID']].'-'.$r['sourceTitle'];
					if($r['bugetChange'] == 1) $sourceNotes .= '(追加)';
					elseif ($r['bugetChange'] == 2) $sourceNotes .= '(追減)';
					$dataList['R01']['sourceNotes'][$sourceNotes] += $sTotal;
				}

			}else {
				$dataList['R01']['lastTotal'] += ($r['selfTrainPay']+$r['allTrafficPay'])*$r['monthNum'];
				//$dataList['R05']['lastTotal'] += $r['aidTrainPay']*$r['monthNum'];
				$dataList['R01']['lastTotal'] += $r['aidTrainPay']*$r['monthNum'];
			}
		}

		if($R01['selfTrainPay'] > 0 || $R01['allTrafficPay'] > 0) {
			$dataList['R01']['second']['B4610']['title'] = $subList['B4610'];
			//$dataList['R01']['second']['B4610']['notes'][]= '服務收入表:自付教養費、通費全額=>'.number_format($R01['selfTrainPay']+$R01['allTrafficPay']);
			$dataList['R01']['second']['B4610']['notes'][]= '服務收入表:自付照顧費、通費全額=>'.number_format($R01['selfTrainPay']+$R01['allTrafficPay']);
		}

		//加入備註
		if($R01['selfTrainPay'] > 0) {
			$dataList['R01']['otherNotes'][]= '服務費收入: $'.number_format($R01['selfTrainPay']);
			//$dataList['R01']['otherNotes'] .= chr(13).chr(10);
		}
		if($R01['allTrafficPay'] > 0) {
			$dataList['R01']['otherNotes'][]= '交通費收入: $'.number_format($R01['allTrafficPay']);
			//$dataList['R01']['otherNotes'] .= chr(13).chr(10);
		}
		/*if($R05['aidTrainPay'] > 0) {
			$dataList['R05']['otherNotes'][]= '托育養護補助: $'.number_format($R05['aidTrainPay']);
			//$dataList['R05']['otherNotes'] .= chr(13).chr(10);
			$dataList['R05']['second']['B4621']['title'] = $subList['B4621'];
			$dataList['R05']['second']['B4621']['notes'][]= '服務收入表:補助教養費=>'.number_format($R05['aidTrainPay']);
		}*/
		if($R01['aidTrainPay'] > 0) {
			//$dataList['R01']['otherNotes'][]= '托育養護補助: $'.number_format($R01['aidTrainPay']);
			$dataList['R01']['otherNotes'][]= '照顧費補助: $'.number_format($R01['aidTrainPay']);
			$dataList['R01']['second']['B4610']['title'] = $subList['B4610'];
			//$dataList['R01']['second']['B4610']['notes'][]= '服務收入表:補助教養費=>'.number_format($R01['aidTrainPay']);
			$dataList['R01']['second']['B4610']['notes'][]= '服務收入表:補助照顧費=>'.number_format($R01['aidTrainPay']);
		}


		/*8萬以上 R101(8萬) R05(補助) R10(捐贈收入)*/
		$sql = "select p.year, p.bugetChange, p.depID, i.title as sourceTitle, o.* from plan p inner join plan_outlay o on p.id=o.fid inner join plan_items i on o.sid = i.id where p.year in ('".$data['year']."','".($data['year']-1)."') and ".$where;
		if(isset($data['sid'])) $sql.= " and o.sid in (".$data['sid'].")";
		$sql.= " order by p.year";
		$rs = $db->query($sql);
		$R101 = array('price'=>0);
		$R05['sourceMoney'] = 0;
		$R10 = array('donate'=>0);
		while ($r=$db->fetch_array($rs)) {
			if($r['bugetChange'] == 2) {
				$r['price'] = -1*$r['price'];
				$r['sourceMoney'] = -1*$r['sourceMoney'];
				$r['donate'] = -1*$r['donate'];
			}
			if($r['year'] == $data['year']) {
				$R101['price'] += $r['price']*$r['quantity'];
				$R05['sourceMoney'] += $r['sourceMoney'];
				$R10['donate'] += $r['donate'];
				$dataList['R101']['total'] += ($r['price']*$r['quantity']);
				$dataList['R05']['total'] += $r['sourceMoney'];
				$dataList['R10']['total'] += $r['donate'];
				$dataList['R101']['otherNotes'][]= $r['title'].': $'.number_format($r['price']*$r['quantity']);
				//$dataList['R101']['otherNotes'] .= chr(13).chr(10);

				$dataList['R101']['second']['B1400']['total'] += ($r['price']*$r['quantity']);
				$dataList['R05']['second']['B4621']['total'] += $r['sourceMoney'];
				$dataList['R10']['second']['B4631']['total'] += $r['donate'];

				//加入計畫來源
				$sourceNotes = $departmentinfo[$r['depID']].'-'.$r['sourceTitle'];
				if($r['bugetChange'] == 1) $sourceNotes .= '(追加)';
				elseif ($r['bugetChange'] == 2) $sourceNotes .= '(追減)';

				if($r['price']*$r['quantity'] != 0) $dataList['R101']['sourceNotes'][$sourceNotes] += ($r['price']*$r['quantity']);
				if($r['sourceMoney'] != 0) $dataList['R05']['sourceNotes'][$sourceNotes] += $r['sourceMoney'];
				if($r['donate'] != 0) $dataList['R10']['sourceNotes'][$sourceNotes] += $r['donate'];
			} else {
				$dataList['R101']['lastTotal'] += ($r['price']*$r['quantity']);
				$dataList['R05']['lastTotal'] += $r['sourceMoney'];
				$dataList['R10']['lastTotal'] += $r['donate'];
			}
		}
		//加入備註
		if($R101['price'] > 0) {
			/*$dataList['R101']['otherNotes'] .= '資本支出表:經費概算=>$'.number_format($R101['price']);
			$dataList['R101']['otherNotes'] .= chr(13).chr(10);*/
			$dataList['R101']['second']['B1400']['title'] = $subList['B1400'];
			//$dataList['R101']['second']['B1400']['notes'] .= '資本支出表:經費概算'.chr(13).chr(10);
		}
		if($R05['sourceMoney'] > 0) {
			$dataList['R05']['otherNotes'][]= '資本支出表:補助金額=>$'.number_format($R05['sourceMoney']);
			//$dataList['R05']['otherNotes'] .= chr(13).chr(10);
			$dataList['R05']['second']['B4621']['title'] = $subList['B4621'];
			//$dataList['R05']['second']['B4621']['notes'] .= '資本支出表:補助金額=>$'.number_format($R05['sourceMoney']).chr(13).chr(10);
		}
		if($R10['donate'] > 0) {
			$dataList['R10']['otherNotes'][]= '資本支出表:捐款金額=>$'.number_format($R10['donate']);
			$dataList['R10']['second']['B4631']['title'] = $subList['B4631'];
		}


		/*經費來源 R05(補助)*/
		$sql = "select p.year, p.bugetChange, p.depID, i.title as sourceTitle, b.* from plan p inner join plan_budget b on p.id=b.fid inner join plan_items i on b.sid = i.id where p.year in ('".$data['year']."','".($data['year']-1)."') and ".$where;
		if(isset($data['sid'])) $sql.= " and b.sid in (".$data['sid'].")";
		$sql.= " order by p.year";
		$rs = $db->query($sql);
		$R05['monthPay'] = 0;
		while ($r=$db->fetch_array($rs)) {
			if($r['bugetChange'] == 2) $r['monthPay'] = -1*$r['monthPay'];
			if($r['year'] == $data['year']) {
				$R05['monthPay'] += $r['monthPay']*$r['monthNum'];
				$dataList['R05']['total'] += ($r['monthPay']*$r['monthNum']);
				$dataList['R05']['second']['B4621']['total'] += $r['monthPay']*$r['monthNum'];
				//if($r['notes'] != '') $dataList['R05']['notes'].= $r['notes'].chr(13).chr(10);

				if($r['monthPay']*$r['monthNum'] != 0){
					//加入計畫來源
					$sourceNotes = $departmentinfo[$r['depID']].'-'.$r['sourceTitle'];
					if($r['bugetChange'] == 1) $sourceNotes .= '(追加)';
					elseif ($r['bugetChange'] == 2) $sourceNotes .= '(追減)';
					$dataList['R05']['sourceNotes'][$sourceNotes] += $r['monthPay']*$r['monthNum'];
				}
			} else $dataList['R05']['lastTotal'] += ($r['monthPay']*$r['monthNum']);
		}

		//備註
		if($R05['monthPay'] > 0) {
			$dataList['R05']['otherNotes'][]= '經費來源預算表:年度總額=>$'.number_format($R05['monthPay']);
			//$dataList['R05']['otherNotes'] .= chr(13).chr(10);
			$dataList['R05']['second']['B4621']['title'] = $subList['B4621'];
			$dataList['R05']['second']['B4621']['notes'][]= '經費來源預算表:年度總額=>$'.number_format($R05['monthPay']);
		}

		/*教養補助 R05(補助)*/
		$sql = "select p.year, p.bugetChange, p.depID, i.title as sourceTitle, i.id from plan p inner join plan_items i on p.id=i.pid where p.year in ('".$data['year']."','".($data['year']-1)."') and ".$where;
		if(isset($data['sid'])) $sql.= " and i.id in (".$data['sid'].")";
		$sql.= " order by p.year";
		$rs = $db->query($sql);
		$R05['mPay'] = 0;
		while ($r=$db->fetch_array($rs)) {
			$per = 0;
			$sql2 = "select * from plan_education where sid='".$r['id']."'";
			$rs2 = $db->query($sql2);
			$eTotal = 0;
			$notes = '';
			while ($r2=$db->fetch_array($rs2)) {
				if($r['bugetChange'] == 2) $r2['monthPay'] = -1*$r2['monthPay'];
				$per = $r2['per'];
				$eTotal += ($r2['monthPay']*$r2['monthNum']);
				//$notes .= $r2['notes'].chr(13).chr(10);
			}
			$eTotal += (int)($eTotal/100*$per);
			if($r['year'] == $data['year']) {
				$dataList['R05']['total'] += $eTotal;
				$R05['mPay'] += $eTotal;
				/*if($notes != '') {
					$dataList['R05']['notes'].= $notes.chr(13).chr(10);
				}*/
				$dataList['R05']['second']['B4621']['total'] += $eTotal;

				if($eTotal != 0) {
					//加入計畫來源
					$sourceNotes = $departmentinfo[$r['depID']].'-'.$r['sourceTitle'];
					if($r['bugetChange'] == 1) $sourceNotes .= '(追加)';
					elseif ($r['bugetChange'] == 2) $sourceNotes .= '(追減)';
					$dataList['R05']['sourceNotes'][$sourceNotes] += $eTotal;
				}
			} else $dataList['R05']['lastTotal'] += $eTotal;
		}
		//備註
		if($R05['mPay'] > 0) {
			$dataList['R05']['otherNotes'][] = '教養機構補助: $'.number_format($R05['mPay']);
			//$dataList['R05']['otherNotes'] .= chr(13).chr(10);
			$dataList['R05']['second']['B4621']['title'] = $subList['B4621'];
			$dataList['R05']['second']['B4621']['notes'][]= '教養機構補助表:年度總額=> $'.number_format($R05['mPay']);
		}

		/*判斷去年有資料就不能手動KEY IN*/
		foreach ($dataList as $key => $value) {
			if($key == '') continue;
			if($value['lastTotal'] > 0) {
				$result['hasLast'] = 1;
				break;
			}
		}

		/*抓已存在的資料*/ 
		if(!empty($data['id'])) $sql = "select * from budget1 where id = '".$data['id']."'";
		else {
			/*if($groupType == 'depID') $sql = "select * from budget1 where year='".$data['year']."' and depID='".$data['depID']."'";
			elseif($groupType == 'invNo') $sql = "select * from budget1 where year='".$data['year']."' and invNo='".$data['invNo']."'";*/
			$sql = "select * from budget1 where year='".$data['year']."'";
			if(!empty($data['depID'])) $sql .= " and depID='".$data['depID']."'";
			if(!empty($data['invNo'] != '')) $sql .= " and invNo='".$data['invNo']."'";
			if(!empty($data['planID'] != '')) $sql .= " and planID='".$data['planID']."'";
		}
		$rs = $db->query($sql);
		if($db->num_rows($rs) > 0) {
			$bData=$db->fetch_array($rs);
			$notesList = explode('&,&', $bData['notes']);
			foreach ($notesList as $key => $value) {
				$n = explode(':&:', $value);
				$dataList[$n[0]]['notes'] = $n[1];
			}
			/*去年沒資料 手KEY部份*/
			if($result['hasLast'] == 0 && $bData['lastData'] != '') {
				$lastList = explode(',', $bData['lastData']);
				foreach ($lastList as $key => $value) {
					$l =  explode(':', $value);
					$dataList[$l[0]]['lastTotal'] = $l[1];
				}
			}
		}

		/*$sql = "select 1 from budget1 where year='".($data['year']-1)."' and depID='".$data['depID']."'";
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) $result['hasLast'] = 0;*/
		$result['dataList'] = $dataList;

		foreach ($result['dataList'] as $key => $value) {
			$result['dataList'][$key]['total'] = round($value['total']);
			$result['dataList'][$key]['lastTotal'] = round($value['lastTotal']);
			if(isset($value['second'])) {
				foreach ($value['second'] as $k => $v) {
					$result['dataList'][$key]['second'][$k]['total'] = round($v['total']);
				}
			}
		}

		return $result;
	}

	/*取得各項人事費*/
	function getPlanTalent($data, $where) {
		global $db, $departmentinfo;
		$result = array();
		$sql = "select p.year, p.bugetChange, p.depID, i.title as sourceTitle, t.amount, t.overPay, t.subject, s.pid, s.title as subTitle from plan p inner join plan_talent t on p.id=t.fid inner join subject_map s on t.subject=s.id inner join plan_items i on t.sid = i.id where p.year in ('".$data['year']."','".($data['year']-1)."')";

		//if(isset($data['depID'])) $sql .= " and p.depID like '".$data['depID']."%'";
		if(isset($data['sid'])) $sql.= " and t.sid in (".$data['sid'].")";
		$sql.= " and ".$where;
		$sql .= " order by p.year, t.subject";
		/*echo '<pre>';
		print_r($data);
		exit;*/
		$rs = $db->query($sql);
		$pid = '';
		while ($r=$db->fetch_array($rs)) {
			if($r['bugetChange'] == 2) {
				$r['amount'] = -1*$r['amount'];
				$r['overPay'] = -1*$r['overPay'];
			}
			if($r['year'] == $data['year']) {
				$result[$r['pid']]['total'] += ($r['amount'] + $r['overPay']);
				$result[$r['pid']]['lastTotal'] += 0;
				$result[$r['pid']]['second'][$r['subject']]['total'] += ($r['amount'] + $r['overPay']);
				$result[$r['pid']]['second'][$r['subject']]['title'] = $r['subTitle'];

				if($r['amount'] + $r['overPay'] != 0) {
					//加入計畫來源
					$sourceNotes = $departmentinfo[$r['depID']].'-'.$r['sourceTitle'];
					if($r['bugetChange'] == 1) $sourceNotes .= '(追加)';
					elseif ($r['bugetChange'] == 2) $sourceNotes .= '(追減)';
					$result[$r['pid']]['sourceNotes'][$sourceNotes] += ($r['amount'] + $r['overPay']);
				}

				if($pid == '') $pid = $r['pid'];
			} else {
				$result[$r['pid']]['total'] += 0;
				$result[$r['pid']]['lastTotal'] += ($r['amount']+ $r['overPay']);
			}
		}

		/*人事費備註 R51*/
		$sql = "select notes from plan4 as p where p.year='".$data['year']."' and ".$where;
		//if(isset($data['depID'])) $sql .= " and depID='".$data['depID']."'";

		$rs = $db->query($sql);
		if($db->num_rows($rs) > 0) { 
			while ($r=$db->fetch_array($rs)) {
				if(isset($result['R51'])) $result['R51']['notes'][] = $r['notes'].chr(13).chr(10);
				else $result[$pid]['notes'][] = $r['notes'];
			}
		}

		return $result;

	}
?>