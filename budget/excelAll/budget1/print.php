<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	include_once("$root/system/db.php");
  	include_once('getData.php');
	

	$db = new db();
	$sql = "select * from budget1 where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);
	
	$d = array('year'=>$data['year'],'depID'=>$data['depID'],'invNo'=>$data['invNo'], 'planID'=>$data['planID'], 'groupType' =>$data['groupType'], 'id'=>$_GET['id']);
	$result = getBudget1Data($d);

	
	$t1 = explode(':', $result['showTitle']);
	$t2 = explode(',', $t1[1]);
	$showTitle = "(".$t1[0].")".implode('_', $t2);

	$filename = $data['year'].'年'.$showTitle.'-預算主表.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('title'=>'預算項目','width'=>15),
		'B' =>array('title'=>'收入預算','width'=>10),
		'C' =>array('title'=>'支出預算','width'=>10),
		'D' =>array('title'=>'上年度收入預算','width'=>10),
		'E' =>array('title'=>'上年度支出預算','width'=>10),
		'F' =>array('title'=>'年度預算增減數','width'=>10),
		'G' =>array('title'=>'預算說明','width'=>25)
	);

	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i, $data['year'].'年度預算表');
    $sheet->getStyle('A1:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $i++;
    $sheet->setCellValue('A'.$i,$result['showTitle']);
    $sheet->setCellValue('G'.$i,'預算表(一)');
    $sheet->getStyle('A1'.':G'.$i)->getFont()->setSize(14);
    $sheet->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

   
    $i++;
	foreach ($header as $key => $value) {
		$sheet->setCellValue($key.$i, $value['title']);
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}
	$sheet->getStyle('A1:G'.$i)->getFont()->setBold(true); //設定粗體
	$sheet->getStyle('A'.$i.':G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('A'.$i.':G'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 

	$tList = array(
		'+'=>array('total'=>0, 'lastTotal'=>0),
		'-'=>array('total'=>0, 'lastTotal'=>0)
	);
	$allTotal = 0;
	foreach ($result['dataList'] as $key => $value) {
		if($key == 'R101') continue;
		if($value['total'] == 0 && $value['lastTotal'] == 0) continue;
		$i++;
		$sheet->setCellValue('A'.$i,$value['title']);
		
		$tList[$value['vType']]['total'] += (int)$value['total'];
		$tList[$value['vType']]['lastTotal'] += (int)$value['lastTotal'];

		if($value['vType'] == '+') {
			$sheet->setCellValue('B'.$i,number_format($value['total']));
			$sheet->setCellValue('D'.$i,number_format($value['lastTotal']));
		} else {
			$sheet->setCellValue('C'.$i,number_format($value['total']));
			$sheet->setCellValue('E'.$i,number_format($value['lastTotal']));
		}

		$total = (int)$value['total'] - (int)$value['lastTotal'];
		$allTotal += $total;
		$total = ($total == 0) ? '' : number_format($total);
		$sheet->setCellValue('F'.$i,$total);
		$notes = $value['otherNotes'];
		$notes[] = $value['notes'];
		$sheet->setCellValue('G'.$i,implode(chr(13).chr(10), $notes));
		$sheet->getStyle('G'.$i)->getAlignment()->setWrapText(true);
	}
	$sheet->getStyle('A1'.':G'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$i++;
	setTotalData('小計',$i, $tList, $allTotal);

	$sheet->getStyle('B5:F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$i++;
	setBalanceData('損益餘絀',$i, $tList);
	$sheet->getStyle('B'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



	$otherList = array('R60', 'R101');
	foreach ($otherList as $key => $value) {
		$i++;
		$d = $result['dataList'][$value];
		$sheet->setCellValue('A'.$i, $d['title']);
		if($value == 'R60') {
			$tList['-']['total'] -= (int)($d['total']);
        	$tList['-']['lastTotal'] -= (int)($d['lastTotal']);
		} else {
			$tList['-']['total'] += (int)($d['total']);
        	$tList['-']['lastTotal'] += (int)($d['lastTotal']);
		}

		$sheet->setCellValue('C'.$i, ($d['total'] == 0) ? '' : number_format($d['total']));
		$sheet->setCellValue('E'.$i, ($d['lastTotal'] == 0) ? '' : number_format($d['lastTotal']));
		if($value == 'R101') {
			$notes = $d['otherNotes'];
			$notes[] = $d['notes'];
			$sheet->setCellValue('G'.$i, implode(chr(13).chr(10), $notes));
		}
	}

	$i++;
	setTotalData('合計',$i, $tList, '');
	$sheet->getStyle('B'.($i-2).':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	//print_r($tList);
	$i++;
	setBalanceData('實質餘絀',$i, $tList);
	$sheet->getStyle('B'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


	$total = $tList['+']['total'] - $tList['-']['total'];
	$lastTotal = $tList['+']['lastTotal'] - $tList['-']['lastTotal'];

	if($total < 0) {
      $tList['+']['total'] += abs($total);
    } else {
      $tList['-']['total'] += abs($total);
    }

    if($lastTotal < 0) {
      $tList['+']['lastTotal'] += abs($lastTotal);
    } else {
      $tList['-']['lastTotal'] += abs($lastTotal);
    }

	$i++;
	$sheet->setCellValue('A'.$i, '基金會挹注款收入');
	$sheet->setCellValue('B'.$i, ($total < 0) ? number_format(abs($total)) : '');
	$sheet->setCellValue('D'.$i, ($lastTotal < 0) ? number_format(abs($lastTotal)) : '');

	$i++;
	$sheet->setCellValue('A'.$i, '發展及危機準備金');
	$sheet->setCellValue('C'.$i, ($total > 0) ? number_format(abs($total)) : '');
	$sheet->setCellValue('E'.$i, ($lastTotal > 0) ? number_format(abs($lastTotal)) : '');

	$i++;
	setTotalData('總計',$i, $tList, '');
	$sheet->getStyle('B'.($i-2).':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$i++;
	$sheet->setCellValue('A'.$i, '總餘絀');
	$sheet->mergeCells('B'.$i.':C'.$i);//合併
	$sheet->setCellValue('B'.$i, '0');
	$sheet->mergeCells('D'.$i.':E'.$i);//合併
	$sheet->setCellValue('D'.$i, '0');
	$sheet->getStyle('B'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$sheet->getStyle('A4:G'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
	//$sheet->getRowDimension(4)->setRowHeight(50);
	$sheet->getStyle('A4:G'.$i)->getFont()->setSize(12);
	$sheet->getStyle('A4:G'.$i)->getAlignment()->setWrapText(true); //自動換行

	$i++;
	$i++;
	$sheet->setCellValue('A'.$i, '製表： '.$emplyeeinfo[$data['creator']]);
	$sheet->mergeCells('B'.$i.':C'.$i);//合併
	$sheet->setCellValue('B'.$i, '機構／中心主管：');
	$sheet->mergeCells('D'.$i.':E'.$i);//合併	
	$sheet->setCellValue('D'.$i, '財務室主任：');
	$sheet->setCellValue('G'.$i, '執行長：');
	$sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//$sheet->getStyle('A4:G'.$i)->getFont()->setName('Candara'); //設定字體


	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

	function setTotalData($title, $i, $tList, $allTotal) {
		global $sheet;
		$sheet->setCellValue('A'.$i, $title);
		$sheet->setCellValue('B'.$i, number_format($tList['+']['total']));
		$sheet->setCellValue('C'.$i, number_format($tList['-']['total']));
		$sheet->setCellValue('D'.$i, number_format($tList['+']['lastTotal']));
		$sheet->setCellValue('E'.$i, number_format($tList['-']['lastTotal']));
		$sheet->setCellValue('F'.$i, number_format($allTotal));
		$sheet->getStyle('A'.$i.':G'.$i)->getFont()->setBold(true); //粗體
	}

	function setBalanceData($title, $i, $tList) {
		global $sheet;
		$sheet->setCellValue('A'.$i, $title);
		$sheet->mergeCells('B'.$i.':C'.$i);//合併

		$total = $tList['+']['total'] -$tList['-']['total'];
		$lastTotal = $tList['+']['lastTotal'] - $tList['-']['lastTotal'];

		$total = ($total >= 0) ? '$'.number_format($total) : '-$'.number_format(abs($total));
		$lastTotal = ($lastTotal >= 0) ? '$'.number_format($lastTotal) : '-$'.number_format(abs($lastTotal));

		$sheet->setCellValue('B'.$i, $total);

		$sheet->mergeCells('D'.$i.':E'.$i);//合併
		$sheet->setCellValue('D'.$i, $lastTotal);

		$sheet->getStyle('A'.$i.':G'.$i)->getFont()->setBold(true);
		$sheet->getStyle('A'.$i.':G'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 
	}
?>
