<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/maintain/inc_vars.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/inc_vars.php");
	include_once("$root/system/db.php");
  	include_once('getData.php');


  	$db = new db();
	$sql = "select * from budget_other where id = '".$_GET['id']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);

	$d = array('year'=>$data['year'],'depID'=>$data['depID'],'invNo'=>$data['invNo'], 'planID'=>$data['planID'], 'groupType' =>$data['groupType'], 'id'=>$_GET['id']);
	$result = getData($data);

	$t1 = explode(':', $result['showTitle']);
	$t2 = explode(',', $t1[1]);
	$showTitle = "(".$t1[0].")".implode('_', $t2);

	$filename = $data['year'].'年'.$showTitle.'-年度經費來源預算表.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('key'=>'','title'=>'序號','width'=>6),
		'B' =>array('key'=>'empName','title'=>'姓名','width'=>10),
		'C' =>array('key'=>'source','title'=>'經費來源','width'=>12),
		'D' =>array('key'=>'monthNum','title'=>'月數','width'=>12),
		'E' =>array('key'=>'monthPay','title'=>'每月金額','width'=>12),
		'F' =>array('key'=>'yTotal','title'=>'全年度金額','width'=>14),
		'G' =>array('key'=>'notes','title'=>'備註','width'=>25)
	);

	
	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i, $data['year'].'年度經費來源預算表');
    $sheet->getStyle('A1:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $i++;
    $sheet->setCellValue('A'.$i,$result['showTitle']);
    $sheet->setCellValue('G'.$i,'預算表(三-2)');
	$sheet->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


    $i++;
	foreach ($header as $key => $value) {
		if($key == 'A' || $key == 'B' || $key == 'G') {
			$sheet->setCellValue($key.$i, $value['title']);
			$sheet->mergeCells($key.$i.':'.$key.($i+1));//合併
		} else {
			$sheet->setCellValue($key.($i+1), $value['title']);
		}
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}
	$sheet->mergeCells('C'.$i.':F'.$i);//合併
	$sheet->setCellValue('C'.$i, '經費收入');

	$i=$i+1;
	$total = 0;
	foreach ($result['dataList'] as $key => $value) {
		$i++;
		foreach ($header as $k2 => $v2) {
			$showText = $value[$v2['key']];
			switch ($v2['key']) {
				case '':
					$showText = $i-5;
					break;
				case 'monthPay':
					$showText = number_format($showText);
					break;
				case 'yTotal':
					$yTotal = (int)$value['monthPay'] * $value['monthNum'];
					$total += $yTotal;
					$showText = number_format($yTotal);
					break;
			}

			$sheet->setCellValue($k2.$i, $showText);
		}
		$sheet->getStyle('G'.$i)->getAlignment()->setWrapText(true);
	}


	$i++;
	$sheet->setCellValue('B'.$i, '合計');
	$sheet->mergeCells('C'.$i.':E'.$i);//合併
	$sheet->setCellValue('F'.$i, number_format($total));


	$sheet->getStyle('A4:G'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線

	$i=$i+2;
	$sheet->setCellValue('G'.$i, '填表人 : '.$emplyeeinfo[$data['creator']]);

	$sheet->getStyle('A1'.':G'.$i)->getFont()->setSize(12);
	$sheet->getStyle('A4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('A4:G'.$i)->getAlignment()->setWrapText(true); //自動換行

//	$sheet->getStyle('A1:G'.$i)->getFont()->setBold(true); //設定粗體
	$sheet->getStyle('A'.$i.':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	//$sheet->getStyle('A'.$i.':G'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

?>