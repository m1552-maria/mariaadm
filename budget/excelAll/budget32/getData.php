<?php

	function getData($data = array()) {
		global $departmentinfo, $emplyeeinfo, $assessPer, $invNo; 
		$result = array('error'=>0,'msg'=>'', 'dataList'=>array());

		$db = new db();	
		$sql = "select * from plan where year='".$data['year']."'";
		$rs = $db->query($sql);
		$planTitleList = array(); 
		while($r=$db->fetch_array($rs)) {
			$planTitleList[$r['id']] = $r['title'];
		}

		$where = array();
		if(!empty($data['depID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$depIDList = explode(',', $data['depID']);
			$_where = array();
			foreach ($depIDList as $key => $value) {
				$_where[] = " p.depID like '".$value."%' ";
				$showTitle[] = $departmentinfo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '機構/中心:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		if(!empty($data['invNo'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$invNoList = explode(',', $data['invNo']);
			$_where = array();
			foreach ($invNoList as $key => $value) {
				$_where[] = " p.invNo like '".$value."%' ";
				$showTitle[] = $invNo[$value];
			}
			$_where = "(".implode(' or ', $_where).")";
			$result['showTitle'][] = '統一編號:'.implode(',', $showTitle);
			$where[] = $_where;
		}
		
		if(!empty($data['planID'])) {
			$result['showTitle'] = array();
			$showTitle = array();
			$planList = explode(',', $data['planID']);
			$_where = array();
			foreach ($planList as $key => $value) {
				$showTitle[] = $planTitleList[$value];
				$planList[$key] = "'".$value."'";
			}
			$_where = "p.id in (".implode(',', $planList).")";
			$result['showTitle'][] = '計劃主檔:'.implode(',', $showTitle);
			$where[] = $_where;
		}

		$result['showTitle'] = implode(' / ', $result['showTitle']);

		$where = implode(' and ', $where);

		$db = new db();	

		$sql = "select o.*, p.bugetChange from plan p inner join plan_budget o on p.id = o.fid where p.year='".$data['year']."'";
		$sql .= ' and '. $where;
		$rs = $db->query($sql);
		if($db->num_rows($rs) == 0) { 
			$result['error'] = 1;
			$result['msg'] = ($data['year']).'年 所選'.$result['showTitle'].' 未存在經費來源';
			return $result;
		}

		while ($r=$db->fetch_array($rs)) {
			if($r['bugetChange'] == 2) $r['monthPay'] = -1*$r['monthPay']; //追減
			$result['dataList'][$r['id']] = $r;
			$result['dataList'][$r['id']]['empName'] = ($r['noEmp'] == 0) ? $emplyeeinfo[$r['empID']] : '新聘人員';
		}
		return $result;
	}
?>