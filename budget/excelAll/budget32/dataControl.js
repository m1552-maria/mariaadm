  function formCheck() {
    if($('[name="depID"]').val() == '' && $('[name="invNo"]').val() == '' && $('[name="planID"]').val() == '') {
      return false;
    }
    return true;
  }

  function dataClear() {
    $('[data-tr]').remove();
    $('[name="Submit"]').attr('disabled','disabled');
  }


  //判斷是否重複 年度&部份
  function ajaxCheck(act, id) {
    $.ajax({
      url:'ajaxCheck.php',
      type:'post',
      dataType:'json',
      data:{ 
        act:act,
        year:$('[name="year"]').val(), 
        depID:$('[name="depID"]').val(),
        invNo:$('[name="invNo"]').val(),
        planID:$('[name="planID"]').val(),
        groupType:$('[name="groupType"]').val(),
        id:id
      },
      success:function(data){
        dataClear();
        console.log(data);
        if(data['error'] == 1) {
          alert(data['msg']);
          $('[name="depID"]').val(''); 
        } else {
          $('#showTitle').html(data['showTitle']);
          var html = '';
          html = setData(data, act);
          $('#dataTable').append(html);
          $('[name="Submit"]').removeAttr('disabled');
        }
      },error:function(){
      	alert('in')
      }

    });
  }

  function setData(data, act) {
    var result = [];
    var j = 0;
    var dList = data['dataList'];
    var total = 0;
    var per = parseInt(data['per']) / 100;
    for(var i in dList) {
      var d = dList[i];
      j++;
      var yTotal = d['monthNum'] * parseInt(d['monthPay']);
      total+=yTotal;
      result.push('<tr data-tr>');
      result.push('<td class="colData">'+j+'</td>');
      result.push('<td class="colData">'+d['empName']+'</td>');
      result.push('<td class="colData">'+d['source']+'</td>');
      result.push('<td class="colData">'+d['monthNum']+'</td>');
      result.push('<td class="colData">'+d['monthPay']+'</td>');
      result.push('<td class="colData">'+yTotal+'</td>');
      result.push('<td class="colData">'+d['notes']+'</td>');
     
      result.push('</tr>');
    }

    result.push('<tr data-tr>');
    result.push('<td class="colData"></td>');
    result.push('<td class="colData">合計</td>');
    result.push('<td colspan="3" class="colData"></td>');
    result.push('<td class="colData">'+total+'</td>');
    result.push('<td class="colData"></td>');
    result.push('</tr>');

    return result.join('');
  }

 