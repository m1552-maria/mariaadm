<?php
	//[]內規則:["網址", "中文名稱", "icon", "子類別"]
    //子類別Key規則 最上層的Key + '/' + link的資料表名稱 (若是直接link 到跟目錄 那就是 不含附檔名的檔案名稱)
    //EX: members 
    //include_once($_SERVER['DOCUMENT_ROOT']."/urlGetProtect.php");
    if(!isset($_SESSION)) {
        ini_set('session.gc_maxlifetime', 7200);
        session_start();
        setcookie('PHPSESSID', session_id(), time() + 7200, '/');
    }

    if($_SESSION['privilege'] > 10) { //總後台過來
        $MENU_LIST = json_decode('{
            "index" : ["index.php", "首頁", "icon-home"],

            "setting" : ["", "系統設定", "icon-cogs", {  
                "setting/acc_map" : ["acc_map/list.php", "會計科目", ""],
                "setting/subject_map" : ["subject_map/list.php", "科目設定", ""],
                "setting/plan_sources" : ["plan_sources/list.php", "經費預算", ""],
                "setting/plan_stringmap" : ["plan_stringmap/list.php", "字串對應表", ""],
                "setting/varsSet" : ["varsSet.php", "欄位資料對應表", ""]
            }],

            "planAll" : ["", "計劃管理", "icon-file-text", {  
                "planAll/plan" : ["plan/list.php", "部門計劃", ""],
                "planAll/planChange" : ["planChange/list.php", "計劃追加減", ""]
            }],
            "planArrange" : ["", "計畫編號管理", "icon-file-text", {  
                "planArrange/plan_arrange" : ["plan_arrange/list.php", "編列編號", ""]
            }],

            "budgetAll" : ["", "預算管理", "icon-dollar", {  
                "budgetAll/budget" : ["budget/list.php", "預算明細表", ""],
                "budgetAll/summary" : ["summary/list.php", "預算彙總表", ""],
                "budgetAll/analysis" : ["analysis/list.php", "預算分析表", ""] 
            }],

            "excelAll" : ["", "報表管理", "icon-file-text", {  
                "excelAll/planAll" : ["", "計劃表管理", "icon-bar-chart", {
                    "excelAll/planAll/plan1" : ["excelAll/plan1/list.php", "計畫表(一)", ""],
                    "excelAll/planAll/plan2" : ["excelAll/plan2/list.php", "計畫表(二)", ""],
                    "excelAll/planAll/plan3" : ["excelAll/plan3/list.php", "計畫表(三)", ""],
                    "excelAll/planAll/plan4" : ["excelAll/plan4/list.php", "計畫表(四)", ""]
                }],
                "excelAll/budgetAll" : ["", "預算表管理", "icon-bar-chart", {
                    "excelAll/budgetAll/budget1" : ["excelAll/budget1/list.php", "預算表(一)", ""],
                    "excelAll/budgetAll/budget2" : ["excelAll/budget2/list.php?type=plan_service", "預算表(二)", ""],
                    "excelAll/budgetAll/budget31" : ["excelAll/budget31/list.php?type=plan_education", "預算表(三-1)", ""],
                    "excelAll/budgetAll/budget32" : ["excelAll/budget32/list.php?type=plan_budget", "預算表(三-2)", ""],
                    "excelAll/budgetAll/budget4" : ["excelAll/budget4/list.php?type=plan_outlay", "預算表(四)", ""]
                }],
                "excelAll/planChange" : ["", "追加減表管理", "icon-bar-chart", {
                    "excelAll/planChange/planChange1" : ["excelAll/planChange1/list.php", "專案審核表", ""],
                    "excelAll/planChange/planChange2" : ["excelAll/planChange2/list.php", "預算追加減表", ""],
                    "excelAll/planChange/planChange3" : ["excelAll/planChange3/list.php", "專案審核表一覽表", ""]
                }]
            }]



        }');
    } else {
        $MENU_LIST = json_decode('{
            "index" : ["index.php", "首頁", "icon-home"],

            "planAll" : ["", "計劃管理", "icon-file-text", {  
                "planAll/plan" : ["plan/list.php", "部門計劃提案", ""],
                "planAll/planChange" : ["planChange/list.php", "計劃追加減", ""]
            }],

            "budgetAll" : ["", "預算管理", "icon-dollar", {  
               
            }],

            "excelAll" : ["", "報表管理", "icon-file-text", {  
                "excelAll/planAll" : ["", "計劃表管理", "icon-bar-chart", {
                    "excelAll/planAll/plan1" : ["excelAll/plan1/list.php", "計畫表(一)", ""],
                    "excelAll/planAll/plan2" : ["excelAll/plan2/list.php", "計畫表(二)", ""],
                    "excelAll/planAll/plan3" : ["excelAll/plan3/list.php", "計畫表(三)", ""],
                    "excelAll/planAll/plan4" : ["excelAll/plan4/list.php", "計畫表(四)", ""]
                }],
                "excelAll/budgetAll" : ["", "預算表管理", "icon-bar-chart", {
                    "excelAll/budgetAll/budget1" : ["excelAll/budget1/list.php", "預算表(一)", ""],
                    "excelAll/budgetAll/budget2" : ["excelAll/budget2/list.php?type=plan_service", "預算表(二)", ""],
                    "excelAll/budgetAll/budget31" : ["excelAll/budget31/list.php?type=plan_education", "預算表(三-1)", ""],
                    "excelAll/budgetAll/budget32" : ["excelAll/budget32/list.php?type=plan_budget", "預算表(三-2)", ""],
                    "excelAll/budgetAll/budget4" : ["excelAll/budget4/list.php?type=plan_outlay", "預算表(四)", ""]
                }],
                "excelAll/planChange" : ["", "追加減表管理", "icon-bar-chart", {
                    "excelAll/planChange/planChange1" : ["excelAll/planChange1/list.php", "專案審核表", ""],
                    "excelAll/planChange/planChange2" : ["excelAll/planChange2/list.php", "預算追加減表", ""],
                    "excelAll/planChange/planChange3" : ["excelAll/planChange3/list.php", "專案審核表一覽表", ""]     
                }]
            }]


        }');
    }

/*
"budgetAll" : ["", "預算管理", "icon-dollar", {  
                "budgetAll/budget" : ["budget/list.php", "預算明細表", ""],
                "budgetAll/summary" : ["summary/list.php", "預算彙總表", ""],
                "budgetAll/analysis" : ["analysis/list.php", "預算分析表", ""] 
            }]
*/

    function getAllMenu($MenuLest, $result = array()) {
        if($MenuLest == '') {
            echo '<pre>';
            print_r('MENU_LIST 有錯誤');
            exit;
        }
        foreach ($MenuLest as $key => $value) {
            $result[] = array('key'=> $key, 'text' => $value[1]);
            if(isset($value[3])) {
                $result = getAllMenu($value[3], $result);
            } 
        }
        return $result;
    }

?>