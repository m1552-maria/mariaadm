<?php 
    
    include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php");
    header("Cache-control: private");

    $_SESSION['maintain']['name'] = (isset($_SESSION['Name'])) ? $_SESSION['Name'] : $_SESSION['empName'];

    $message = '';
    if(isset($_SESSION['maintain']['MESSAGE']) && gettype($_SESSION['maintain']['MESSAGE']) == 'string') {
        $message = $_SESSION['maintain']['MESSAGE'];
        unset($_SESSION['maintain']['MESSAGE']);
    }
    //ini_set('session.gc_maxlifetime', 7200);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>瑪利亞預算管理系統</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="/budget/media/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/chosen.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/system/MultipleListControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 
<script src="/budget/media/js/jquery.cookie.js"></script>


<script src="/budget/media/script/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script src="/system/MultipleListControl.js"></script>


   

    <script type="text/javascript">
        <?php if($message != '') { ?>
            $(document).ready(function(){
                $('#messageBox .modal-body').html('<?php echo $message;?>');
                $('#messageBox').modal('show');
            });
        <?php } ?>
    </script>


 <style>

#ant {
    display: inline-block;
    height:50px;
    color:red;
    text-align:center;
    line-height:50px;
    margin:0px auto;
}
</style>
<script>
window.onload = function () {
  var odiv = document.getElementById("ant");
  var count = 6900;
  var _count = formatSecond(count);
  odiv.innerHTML=_count;

  var timer = null;
  timer = setInterval(function () {
    if (count > 0) {
      count = count - 1;
      odiv.innerHTML = formatSecond(count);
    }
    else {
      clearInterval(timer);
      odiv.innerHTML='您的系統已被登出';
      alert('您的系統已被登出');
    }
  }, 1000);
}

function formatSecond(secs) {          
    var hr = Math.floor(secs / 3600);
    var min = Math.floor((secs - (hr * 3600)) / 60);
    var sec = parseInt( secs - (hr * 3600) - (min * 60));

    if(min < 10)  min = ''+parseInt(min); 
    if(min == 0) min = '0';
    if(sec < 10) { sec = '0' + sec; }

    if (hr) hr += '時';
    return hr + min + '分' + sec+'秒';

}

</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
    <div class="header navbar navbar-inverse navbar-fixed-top">
        <!-- BEGIN TOP NAVIGATION BAR -->
        <div class="navbar-inner">
            <div class="container-fluid">
                <!-- BEGIN LOGO -->
                <a class="brand" href="/budget/index.php">
                    <img src="/budget/media/image/logo-mini.png" alt="logo" />
                </a>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                    <img src="/budget/media/image/menu-toggler.png" alt="" / height="100%">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav pull-right">
                  
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin-top:5px !important;">
                            <span class="username"><?php echo $_SESSION['maintain']['name'];?></span>
                            <i class="icon-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/" target="_blank"><i class="icon-search"></i> 前台預覽</a></li>
                            <li><a href="<?php echo ($_SESSION['maintain']['ADMIN']['userType'] == 'admin') ? '/budget/login.php' : '/budget/castLogin.php';?>"><i class="icon-key"></i> 登出</a></li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        
        <!-- END TOP NAVIGATION BAR -->
    </div>
    <!-- END HEADER -->
    <div style="position: fixed;right: 19px;font-size: 16px;">倒數登出: <div id="ant"></div></div>