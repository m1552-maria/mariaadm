<?php 


    include_once('menuList.php');
    $PHP_SELF_LIST = explode('/', $_SERVER['PHP_SELF']);
    $MAIN_PAGE = 'index';

    if(count($PHP_SELF_LIST) == 3) {
        $PList = explode('.', $PHP_SELF_LIST[2]);
        $MAIN_PAGE = $PList[0];
    }
    elseif(count($PHP_SELF_LIST) > 3) $MAIN_PAGE = $PHP_SELF_LIST[(count($PHP_SELF_LIST) - 2)];


    /*if($_SERVER['QUERY_STRING'] != '') {
        $_data = getVars(decodeparams($_SERVER['QUERY_STRING']));
        if(isset($_data['fn'])) {
            $fnList = explode('/', $_data['fn']);
            $MAIN_PAGE = str_replace('.html', '', end($fnList));
        }
    }*/

    $purviewList = array(); //權限
    $allMenu = getAllMenu($MENU_LIST, array());  //在menuList.php
    foreach ($allMenu as $key => $value) {
        $purviewList[] = $value['key'];
    }
    
    //權限判斷
    $purviewIsOK = 0;
    foreach ($purviewList as $key => $value) {
        $pList = explode('/', $value);
        if(end($pList) == $MAIN_PAGE) {
            $purviewIsOK = 1;
            break;
        }
    }
   /* echo "<pre>";
    print_r($purviewList);
    print_r($MAIN_PAGE);
    print_r($purviewIsOK);*/
    //exit;
    if($purviewIsOK == 0) header("Location: /budget/error.html");

    $MENU = array();
    $MENU = getMenu($MENU_LIST, $MENU, $purviewList);

    function getMenu($MenuLest, $result = array(), $purviewList) {
        foreach ($MenuLest as $key => $value) {
            if(in_array($key, $purviewList)){
                $result[] = '<li data-li-page="'.$key.'" class="">';
                if($value[0] == '') $result[] = '<a href="javascript:void(0)">';
                else $result[] = '<a href="/budget/'.$value[0].'">';

                if(isset($value[2])) $result[] = '<i class="'.$value[2].'"></i>';
                $result[] = '<span class="title">'.$value[1].'</span>';

                if(isset($value[3])) {
                    $result[] = '<span class="arrow"></span>';
                    $result[] = '</a>';
                    $result[] = '<ul class="sub-menu">';
                    $result = getMenu($value[3], $result, $purviewList);
                    $result[] = '</ul>';
                    $result[] = '</li>';
                } else {
                    $result[] = '<span class="selected"></span>';
                    $result[] = '</a>';
                    $result[] = '</li>';
                }
            }
            
        }
        return $result;
    }

?>

        <div class="page-sidebar nav-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="page-sidebar-menu">
                <li>
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler hidden-phone"></div>
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                </li>
                <li>
                    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                   <!-- <form class="sidebar-search">
                        <div class="input-box">
                            <a href="javascript:;" class="remove"></a>
                            <input type="text" placeholder="Search..." />
                            <input type="button" class="submit" value=" " />
                        </div>
                    </form>-->
                    <!-- END RESPONSIVE QUICK SEARCH FORM -->

                </li>
            <?php echo implode('', $MENU);?>
               
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    <script type="text/javascript">
        var pageVal = $('[data-li-page$="<?php echo $MAIN_PAGE;?>"]').attr('data-li-page')
        var liPage = pageVal.split('/');
        $('[data-li-page="'+liPage[0]+'"]').addClass('start active');
        $('.sub-menu i').each(function(){
            $(this).css({position:'absolute', margin:'2px 0 0 -20px' });
        });

        setSelectMenu(pageVal);
        function setSelectMenu(pageVal) {
            liPage = pageVal.split('/');
            if((liPage.length-1) < 1) {
                return false;
            } else {
                $('[data-li-page="'+pageVal+'"]').parent('.sub-menu').css('display','block');
                if($('[data-li-page="'+pageVal+'"]:has(ul)').length == 0) {
                    $('[data-li-page="'+pageVal+'"]').addClass('open');
                } 
                var newList = [];
                for(var i=0; i<liPage.length; i++) {
                    if((i+1) < liPage.length){
                        newList.push(liPage[i]);
                    }
                }
                newList = newList.join('/');
               // console.log(newList);
               // $('[data-li-page="'+newList+'"]').addClass('open');
                $('[data-li-page="'+newList+'"]').find('.arrow').addClass('open');
                setSelectMenu(newList);
            }
        }

    </script>