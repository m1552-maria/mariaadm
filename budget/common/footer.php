    <!-- BEGIN FOOTER -->
    <div class="footer">
        <div class="footer-inner">
            2016 &copy; 雲碼資訊.
        </div>
        <div class="footer-tools">
            <span class="go-top">

			<i class="icon-angle-up"></i>

			</span>
        </div>
    </div>
    <!-- END FOOTER -->

    <script>
    jQuery(document).ready(function() {
        App.init();
       // Search.init();
       // FormComponents.init();
    });
    </script>

</body>
<!-- END BODY -->

</html>
