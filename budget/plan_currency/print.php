<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once("$root/system/db.php");
  	include_once($root.'/budget/excelAll/budget1/getData.php');


	$db = new db();

	$sql = "select p.year, p.depID, p.creator, i.* from plan p inner join plan_items i on p.id=i.pid where p.id = '".$_GET['fid']."' and i.id='".$_GET['sid']."'";
	$rs = $db->query($sql);
	$data = $db->fetch_array($rs);
	$d = array('year'=>$data['year'],'depID'=>$data['depID'], 'sid'=>$_GET['sid']);
	$result = getBudget1Data($d);

	/*echo "<pre>";
	print_r($result);
	exit;*/

	$filename = $data['year'].'經費預算-計畫項目明細表.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('title'=>'預算項目','width'=>15),
		'B' =>array('title'=>'預編科目','width'=>15),
		'C' =>array('title'=>'收入預算','width'=>15),
		'D' =>array('title'=>'支出預算','width'=>15),
		'E' =>array('title'=>'預算說明','width'=>30)
	);

	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':E'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':E'.$i);//合併
    $sheet->setCellValue('A'.$i, $data['year'].'年度預算表');
    $sheet->getStyle('A1:E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $i++;
    $sheet->setCellValue('A'.$i,'機構/中心：'.$departmentinfo[$data['depID']]);
    $i++;
    $sheet->setCellValue('A'.$i,'計畫項目：'.$data['title']);
    $sheet->getStyle('A1'.':E'.$i)->getFont()->setSize(14);
   // $sheet->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

   
    $i++;
	foreach ($header as $key => $value) {
		$sheet->setCellValue($key.$i, $value['title']);
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}
	$sheet->getStyle('A1:E'.$i)->getFont()->setBold(true); //設定粗體
	$sheet->getStyle('A'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('A'.$i.':E'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 

	$tList = array(
		'+'=>array('total'=>0),
		'-'=>array('total'=>0)
	);
	$allTotal = 0;
	$i++;
	foreach ($result['dataList'] as $key => $value) {
		if($key == 'R101') continue;
		if($value['total'] == 0) continue;
		//$i++;
		$sheet->setCellValue('A'.$i,$value['title']);
		$sheet->mergeCells('A'.$i.':A'.($i+count($value['second'])-1));
		foreach ($value['second'] as $k2 => $v2) {
			$sheet->setCellValue('B'.$i,$v2['title']);
			if($value['vType'] == '+') {
				$sheet->setCellValue('C'.$i,number_format($v2['total']));
			} else {
				$sheet->setCellValue('D'.$i,number_format($v2['total']));
			}
			$sheet->setCellValue('E'.$i,implode(chr(13).chr(10), $v2['notes']));
			$i++;
		}

		$tList[$value['vType']]['total'] += (int)$value['total'];
		$total = (int)$value['total'] - (int)$value['lastTotal'];
		$allTotal += $total;
		$total = ($total == 0) ? '' : number_format($total);
		$sheet->getStyle('E'.$i)->getAlignment()->setWrapText(true);
	}
	$sheet->getStyle('A1'.':E'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	//$i++;
	setTotalData('小計',$i, $tList, $allTotal);

	$sheet->getStyle('C6:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$i++;
	setBalanceData('損益餘絀',$i, $tList);
	$sheet->getStyle('B'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



	$otherList = array('R60', 'R101');
	foreach ($otherList as $key => $value) {
		$i++;
		$d = $result['dataList'][$value];
		$sheet->setCellValue('A'.$i, $d['title']);
		if($value == 'R60') {
			$tList['-']['total'] -= (int)($d['total']);
		} else {
			$tList['-']['total'] += (int)($d['total']);
		}

		$sheet->setCellValue('D'.$i, ($d['total'] == 0) ? '' : number_format($d['total']));
		//$sheet->setCellValue('E'.$i, ($d['lastTotal'] == 0) ? '' : number_format($d['lastTotal']));
		if($value == 'R101') {
			//$sheet->setCellValue('E'.$i, $d['otherNotes'].$d['notes']);
		}
	}

	$i++;
	setTotalData('合計',$i, $tList, '');
	$sheet->getStyle('B'.($i-2).':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	//print_r($tList);
	$i++;
	setBalanceData('實質餘絀',$i, $tList);
	$sheet->getStyle('B'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


	
	$sheet->getStyle('B'.$i.':E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$sheet->getStyle('A5:E'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
	//$sheet->getRowDimension(4)->setRowHeight(50);
	$sheet->getStyle('A5:E'.$i)->getFont()->setSize(12);
	$sheet->getStyle('A5:E'.$i)->getAlignment()->setWrapText(true); //自動換行

	$i++;
	$i++;
	$sheet->setCellValue('A'.$i, '製表： '.$emplyeeinfo[$data['creator']]);
	
	$sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//$sheet->getStyle('A5:E'.$i)->getFont()->setName('Candara'); //設定字體


	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

	function setTotalData($title, $i, $tList, $allTotal) {
		global $sheet;
		$sheet->setCellValue('A'.$i, $title);
		$sheet->mergeCells('A'.$i.':B'.$i);//合併
		$sheet->setCellValue('C'.$i, number_format($tList['+']['total']));
		$sheet->setCellValue('D'.$i, number_format($tList['-']['total']));

		$sheet->getStyle('A'.$i.':E'.$i)->getFont()->setBold(true); //粗體
	}

	function setBalanceData($title, $i, $tList) {
		global $sheet;
		$sheet->setCellValue('A'.$i, $title);
		$sheet->mergeCells('A'.$i.':B'.$i);//合併

		$total = $tList['+']['total'] -$tList['-']['total'];

		$total = ($total >= 0) ? '$'.number_format($total) : '-$'.number_format(abs($total));

		$sheet->setCellValue('C'.$i, $total);

		$sheet->mergeCells('C'.$i.':D'.$i);//合併

		$sheet->getStyle('A'.$i.':E'.$i)->getFont()->setBold(true);
		$sheet->getStyle('A'.$i.':E'.$i)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'C1C1C1'))); 
	}
?>
