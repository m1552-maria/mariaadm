<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/

	
	if(!isset($_REQUEST['fid'])) exit;
	$fID = $_REQUEST['fid'];
	$sID = $_REQUEST['sid'];
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";

	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";

	/*特殊處理vType Get時 無法解析+與-*/
	if(isset($_SERVER['QUERY_STRING'])) {
		$getList = explode('&', $_SERVER['QUERY_STRING']);
		foreach ($getList as $key => $value) {
			if (preg_match("/vType/i", $value)){
				$vType = explode('=', $value);
				$_REQUEST['vType'] = $vType[1];
				break;
			}
		}
	}
	
	$pageTitle = "經費預算";
	$tableName = "plan_currency";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 1000;
	$NeedFilter = "sid = '".$sID."'";

	$db = new db();
	$sql = "select status from plan where id = '".$fID."'";
	$rs = $db->query($sql);
	$r=$db->fetch_array($rs);
	$status = (int)$r['status'];
	
	/*判別按鈕是否可使用*/
	$canShow = 1;
	if($_SESSION['privilege'] <= 10 && $status >= 20) $canShow=0;
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,false,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,false,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,false,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,false,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,false,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => (!$canShow) ? array("＋新增",true,true,'') : array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => (!$canShow) ? array("Ｘ刪除",true,true,'') : array("Ｘ刪除",false,true,'doDel') 
	);

	if(empty($sID)) {
		$opPage["searchBtn"] = array("◎搜尋",true,true,'SearchKeyword');
		$opPage["newBtn"] = array("＋新增",true,true,'');
		$opPage["editBtn"] = array("－修改",true,true,'');
		$opPage["delBtn"] = array("Ｘ刪除",true,true,'');
	}
	
	//:ListControl  ---------------------------------------------------------------
	$classList = array(''=>'--預算類別--', '經常門'=>'經常門', '資本門'=>'資本門');
	$vTypeList = array(''=>'--收入/支出--', '+'=>'收入', '-'=>'支出');

	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"filterOption"=>array('autochange'=>true,'goBtn'=>array("確定",false,true,'')),
		"ckBoxAll" =>true,
		"filters"=>array( //過濾器
			/*'class'=>$classList,
			'vType'=>$vTypeList*/
		),
		"extraButton"=>array( //新增的button
			'printExcel' =>array("EXCEL匯出",false,true,'printExcel')
		)
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/	 
	$sql = "select * from subject_map where levels = 2 order by id asc";
	$rs = $db->query($sql);
	$subjectList = array();
	$subList = array();
	while($r=$db->fetch_array($rs)) {
		$subjectList[$r['id']] = $r['title'];
		$subList[$r['id']] = $r;
	}

	$sql = "select * from plan_items where pid='".$fID."'";
	$rs = $db->query($sql);
	$sList = array();
	while($r=$db->fetch_array($rs)) $sList[$r['id']] = $r['title'];

	unset($classList['']);
	unset($vTypeList['']);
	$fieldsList = array(
		"id"=>array("編號","50px",true,array('Id',"gorec(%d)")),
		//"fid"=>array("計畫編號","",true,array('Text')),
		"sid"=>array("計畫名稱","",true,array('Select', $sList)),
		//"title"=>array("名稱","",true,array('Text')),
		//"class"=>array("預算類別","",true,array('Text')),
		"subject"=>array("預編科目","",true,array('Select',$subjectList)),
		"amount"=>array("金額","",true,array('Text')),
		"notes"=>array("備註","",true,array('Text'))
		//"vType"=>array("支出/收入","100px",true,array('Select', $vTypeList))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$fieldA = array(
		"fid[]"=>array("主檔編號","hidden",20,'','',''),
		"_sid[]"=>array("子計畫","readonly",20,'',$sList,''),
		"sid[]"=>array("子計畫","hidden",20,'',$sList,''),
		//"title[]"=>array("<span class='need'>*</span>名稱","text","","","",array(true,'','','')),
		//"class[]"=>array("<span class='need'>*</span>預算類別","select",1,"",$classList,array(true,'','','')),
		"subject[]"=>array("<span class='need'>*</span>預編科目","select",1,"",$subjectList),
		"amount[]"=>array("金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		//"vType[]"=>array("<span class='need'>*</span>支出/收入","select",1,"",$vTypeList),
		"notes[]"=>array("備註","textbox","","","")
	);
	/*新增時的預設值*/
	$fieldA_Data = array("fid[]"=>$fID, "sid[]"=>$sID, "_sid[]"=>$sID);
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"fid"=>array("主檔編號","hidden",20,'','',''),
		"sid"=>array("計畫名稱","readonly",20,'',$sList,''),
		//"title"=>array("<span class='need'>*</span>名稱","text","","","",array(true,'','','')),
		//"class"=>array("<span class='need'>*</span>預算類別","select",1,"",$classList),
		"subject"=>array("<span class='need'>*</span>預編科目","select",1,"",$subjectList),
		"amount"=>array("金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		//"vType"=>array("<span class='need'>*</span>支出/收入","select",1,"",$vTypeList),
		"notes"=>array("備註","textbox","","","")
	);
?>