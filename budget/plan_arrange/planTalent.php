<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once("$root/system/db.php");
  	

	$filename = $_REQUEST['year'].'年人力編制彙整表.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('title'=>'部門','width'=>15),
		'B' =>array('title'=>'計畫編號','width'=>10),
		'C' =>array('title'=>'主檔名稱','width'=>18),
		'D' =>array('title'=>'子計畫名稱','width'=>18),
		'E' =>array('title'=>'員工編號','width'=>10),
		'F' =>array('title'=>'姓名','width'=>10),
		'G' =>array('title'=>'人員使用百分比','width'=>10)
	);

	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':G'.$i);//合併
    $sheet->setCellValue('A'.$i, '人力編制彙整表-'.$_REQUEST['year'].'年度計畫預算版');
    $sheet->getStyle('A1'.':G'.$i)->getFont()->setSize(14);

    $i++;
	foreach ($header as $key => $value) {
		$sheet->setCellValue($key.$i, $value['title']);
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}
	$sheet->getStyle('A1:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('G'.$i)->getAlignment()->setWrapText(true); //換行

    $sheet->getStyle('A1:G'.$i)->getFont()->setBold(true); //設定粗體

	$db = new db();
	$sql = "select a.projID, a.depID, a.title aTitle, p.Title pTitle, i.title iTitle, t.empID, t.rate, t.noEmp from plan_arrange a inner join plan_arrange_item ai on a.id=ai.pID inner join plan p on ai.planID = p.id inner join plan_items i on p.id = i.pid inner join plan_talent t on i.id=t.sid where a.year='".$_REQUEST['year']."'";
	if(!empty($_REQUEST['depID'])) $sql.=" and a.depID='".$_REQUEST['depID']."'";
	$sql.=" order by a.projID, a.depID, ai.planID, i.id, t.empID";

	$rs = $db->query($sql);
	$dep = '';
	while ($r=$db->fetch_array($rs)) {
		$i++;
		$sheet->setCellValue('A'.$i, $departmentinfo[$r['depID']]);
		$sheet->setCellValue('B'.$i, $r['projID']);
		$sheet->setCellValue('C'.$i, $r['pTitle']);
		$sheet->setCellValue('D'.$i, $r['iTitle']);
		$sheet->setCellValue('E'.$i, ' '.$r['empID']);
		$empName = $emplyeeinfo[$r['empID']];
		if($r['noEmp'] == 1) $empName .= '(新聘或留停)';
		$sheet->setCellValue('F'.$i, $empName);
		$sheet->setCellValue('G'.$i, $r['rate']);
	}
	$sheet->getStyle('A4:G'.$i)->getAlignment()->setWrapText(true); //換行
	if($con > 1) $sheet->mergeCells('A'.($i-$con+1).':A'.($i));

	$sheet->getStyle('A3:G'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
	$sheet->getStyle('A2'.':G'.$i)->getFont()->setSize(12);
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

  	/*echo "<pre>";
  	print_r($_REQUEST);
  	exit;*/
?>