<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	
	if($act=='del') { //:: Delete record -----------------------------------------
		$aa = array();
		foreach($_POST['ID'] as $v) $aa[] = "'$v'";
		$lstss = join(',',$aa);
		$aa =array_keys($fieldsList);		
		$db = new db();	
		//檢查是否要順便刪除檔案
		$sql = "select * from $tableName where $aa[0] in ($lstss) and status=0";
		$rs = $db->query($sql);
		$deleteID = array();
		while($r=$db->fetch_array($rs)) {
			if(isset($delFlag)) { 
				$fns = $r[$delField];
				if($fns) {
					$ba = explode(',',$fns);
					foreach($ba as $fn) {
						$fn = $root.$ulpath.$fn;
						//echo $fn;
						if(file_exists($fn)) @unlink($fn);
					}
				}
			}
			$deleteID[$r[$aa[0]]] = "'".$r[$aa[0]]."'";
		}
		
		if(count($deleteID) > 0) {
			$deleteID = implode(',', $deleteID);
			$sql = "delete from $tableName where $aa[0] in ($deleteID)";
			$db->query($sql);

			$sql = "delete from plan_arrange_item where pID in ($deleteID)";
			$db->query($sql);
		}
		
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------

	function getMaintain($id) {
		$html = array();
		$html[] = '<a href="/budget/excelAll/plan1/print.php?id='.$id.'" target="_blank" >';
		$html[] = '<img src="/images/print.png" title="列印" align="absmiddle" border="0"/>';
		$html[] = '</a>';

		return implode('', $html);
	}


	function showItem($idNum) {
		$result = array();
		$db = new db();
		$sql = "select i.*, p.title, p.invNo, p.bugetChange, p.plan_arrange_id from plan_arrange_item i inner join plan p on i.planID = p.id where i.pID='".$idNum."'";
		$rs = $db->query($sql);
		while($r=$db->fetch_array($rs)) {
			$title = $r['title'].'('.$r['invNo'].')';
			if($r['bugetChange'] == 1 && $r['plan_arrange_id'] > 0) $title='( 追加 ) '.$title;
			elseif($r['bugetChange'] == 1 && $r['plan_arrange_id'] == 0) $title='( 新增 ) '.$title;
			elseif($r['bugetChange'] == 2)  $title='( 追減 ) '.$title;
			$result[] = $title;
			
		}
		$result = implode('<br>', $result);
		return $result;
	}

	function planSelect() {
		global $pdata, $listPos, $act;
		/*print_r($act);
		echo "<pre>";
		print_r($pdata[$listPos]);
		exit;*/
		$html = array();

		$db = new db();	
		
		if($act == 'new') {
			if($_REQUEST['depID'] == '' || $_REQUEST['year'] == '') {
				$html[] = '<select name="item[0][]" ></select>';
				return implode('', $html);
			}
			$sql = "select * from plan where status='20' and year='".$_REQUEST['year']."' and depID like '".$_REQUEST['depID']."%'";
			$rs = $db->query($sql);
			if($db->num_rows($rs) == 0) {
				$html[] = '<select name="item[0][]" ></select>';
				return implode('', $html);
			}
			$html[] = '<select name="item[0][]" data-placeholder="" class="chosen span6" multiple="multiple" tabindex="6"><option value=""></option><optgroup>';

			while($r=$db->fetch_array($rs)) {
				$html[] = '<option value="'.$r['id'].'">'.$r['title'].'('.$r['invNo'].')</option>';
			}
			
		} else {

			$sql2 = "select * from plan_arrange_item where pID='".$pdata[$listPos]['id']."'";
			$rs2 = $db->query($sql2);
			$selectedItem = array();
			while ($r2 = $db->fetch_array($rs2)) $selectedItem[$r2['planID']] = $r2['planID'];

			$html[] = '<select name="item[0][]" data-placeholder="" class="chosen span6" multiple="multiple" tabindex="6"><option value=""></option><optgroup>';

			$sql = "select * from plan where status='20' and year='".$pdata[$listPos]['year']."' and depID like '".$pdata[$listPos]['depID']."%'";
			$rs = $db->query($sql);
			while($r=$db->fetch_array($rs)) {
				$selected = (isset($selectedItem[$r['id']])) ? 'selected' : '';
				$html[] = '<option '.$selected.' value="'.$r['id'].'">'.$r['title'].'('.$r['invNo'].')</option>';
			}


		}
		$html[] = '</optgroup></select>';
		
		return implode('', $html);
	}

?>
<?php include($root."/budget/common/header.php");?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
<?php include($root."/budget/common/menu.php");?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- Modal -->
        <div class="modal fade hide" id="messageBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">訊息</h4>
              </div>
              <div class="modal-body">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn blue" data-dismiss="modal">確認</button>
              </div>
            </div>
          </div>
        </div>


            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	$wherefStr = '';
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			$oprator = $opList['filterCondi'][$k]?$opList['filterCondi'][$k]:"%s = '%s'"; 
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]=sprintf($oprator,$k,$_REQUEST[$k]);
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]=sprintf($oprator,$k,$opList['filterOption'][$k][0]);
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	echo "<h1 align='center'>$pageTitle ";
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div><hr>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select *, id as idNum from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	//echo $pinf->curPage;
	//echo $sql;
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	

	//:TreeControl -----------------------------------------------------------------------
	$option = array('dbSource'=>DB_SOURCE2);
	$db2 = new db($option);
	$op = array(
		'ID'		=> 'depID',
		'Title'		=> 'depTitle',
		'TableName' => 'department',
		'RootClass' => 0,
		'RootTitle' => '瑪利亞基金會',
		'DB' 		=> $db2,
		'Modal' 	=> true,
		'URLFmt'	=> "javascript:detIDfilter(\"%s\",\"%s\")",
		'orderBy'	=> 'depID'
	 );
	$tc=new BaseTreeControl($op);

	//:ViewControl Object ------------------------------------------------------------------------
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			$op3 = array(
				"templet"=>$root."/budget/".$tableName."/edit.html",
				"type"=>"edit",
				"form"=>array('form1','doedit.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定修改",false,''),
				"cancilBtn"=>array("取消修改",false,'')	);
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"templet"=>$root."/budget/".$tableName."/add.html",
				"type"=>"append",
				"form"=>array('form1','doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			//if(count($pdata) == 0) break;
			$obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	}
?>



                    </div>
                </div>
     
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
<?php include($root.'/budget/public/inc_listjs.php');?>
<script type="text/javascript">
	$(function(){
		$('[name^="year"]').change(function(){
			changeItemID();
			$('[name^="projID"]').val('');
		});

		$(document).on('change','[name^="projID"]', function(){
			var projIDThis = this;
			$.ajax({
	            url:'checkProjID.php',
	            type:'post',
	            data:{
	            	projID: projIDThis.value,
	            	year: $('[name^="year"]').val(),
	            	id: $('[name="id"]').val()
	            },
	            dataType:'json',
	           // async: false,
	            success:function(d){
	            	if(d == 0) {
	            		projIDThis.setCustomValidity('');
	            		var con = 0;
	            		$('[name^="projID"]').each(function(){
	            			if($(this).val() == projIDThis.value) con++;
	            		});
	            		if(con > 1) projIDThis.setCustomValidity('新增編號重複');
	            	} else projIDThis.setCustomValidity('此編號已經存在');

	            }
	        })  
		});

		$(document).on('change', '[name^="item"]', function(){
			var k = $(this).parent('td').parent('[data-one-tr]').attr('data-one-tr');
			if(!$('[data-checkbox="'+k+'"]').prop('checked')) return;
			$('[data-two-tr="'+k+'"] [name^="title"]').val($(this).find('option:selected').text());
		});

		$(document).on('click', '[data-checkbox]', function(){
			if(!$(this).prop('checked')) return;
			var k = $(this).attr('data-checkbox');
			$('[data-two-tr="'+k+'"] [name^="title"]').val($('[data-one-tr="'+k+'"] [name^="item"]').find('option:selected').text());
		});

	})
	function depTreeShow(evt) {
		showTreeControl();
	}

	function detIDfilter(key,cname) {
		if(key == 0) key = '';
		while(ListControlForm.depID.options[0]) $(ListControlForm.depID.options[0]).remove();
		var op = document.createElement('option');
		op.value = key;
		op.text = cname;
		ListControlForm.depID.add(op);
		ListControlForm.cname.value = cname;
		ListControlForm.submit();		
	}

	function returnValue(data) {
		if(data) {
			param.val(data[0]);
			param[0].setCustomValidity(''); //讓錯誤訊息清空
			param.next().next('span').html(data[1]); 
			changeItemID();
		}
	}

	var itemIDSelect = ''; //增加時要替換用的
	function changeItemID() {
		$.ajax({
            url:'getPlanID.php',
            type:'post',
            data:{
            	year:  $('[name^="year"]').val(),
            	depID: $('[name^="depID"]').val()
            },
            dataType:'json',
           // async: false,
            success:function(d){
            	var html = '';
            	var _html = [];
            	if(d['con'] == 0) _html.push('<select name="itemIDName"></select>');
            	else {
            		_html.push('<select name="itemIDName" data-placeholder="" class="chosen span6" multiple="multiple" tabindex="6"><option value=""></option><optgroup>');
            		for(var i in d['data']) _html.push('<option value="'+i+'">'+d['data'][i]+'</option>');
            		_html.push('</optgroup></select>');
            	}
            	itemIDSelect = _html.join('');

            	$('[name^="item"]').each(function(){
            		var name = $(this).attr('name');
            		html = itemIDSelect.replace('itemIDName',name);
            		$(this).parent('td').html(html);
            	});

            	$(".chosen").each(function () {
		            $(this).chosen({
		                allow_single_deselect: $(this).attr("data-with-diselect") === "1" ? true : false
		            });
		        });
            
            }
        })  
	}

	function excel1Export() {
		if($('#year').val() == '') {
			alert('請選擇年度');
			return false;
		}
		//$('[name="printForm"]').submit();
		window.open('excel1.php?year='+$('#year').val()+'&depID='+$('#depID').val());
	}

	function excel2Export() {
		if($('#year').val() == '') {
			alert('請選擇年度');
			return false;
		}
		//$('[name="printForm"]').submit();
		window.open('excel2.php?year='+$('#year').val()+'&depID='+$('#depID').val());
	}

	function planTalentExport() {
		if($('#year').val() == '') {
			alert('請選擇年度');
			return false;
		}
		//$('[name="printForm"]').submit();
		window.open('planTalent.php?year='+$('#year').val()+'&depID='+$('#depID').val());
	}

</script>
<?php include($root."/budget/common/footer.php");?>
