<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "編列編號";
	$tableName = "plan_arrange";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 20;
	$NeedFilter = '';
	/*echo "<pre>";
	print_r($_SESSION);
	exit;*/
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$nowYear = date('Y')-1911;
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'projID',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,''),
			'year'=>array($nowYear+1, false)
		),
		"filterCondi"=>array(	//比對條件
			'depID'=>"%s like '%s%%'"
		),
		"filters"=>array( //過濾器
			'year'=>array()
		),
		"extraButton"=>array( //新增的button
			'depTree'=>array("選取部門",false,true,'depTreeShow'),
			'excel1' =>array("匯出編號表",false,true,'excel1Export'),
			'excel2' =>array("匯出編號表(帶金額)",false,true,'excel2Export'),
			'planTalent' =>array("匯出人力編制表",false,true,'planTalentExport')
		)
	);

	if(!empty($_REQUEST['depID'])) $opAR = array($_REQUEST['depID']=>$_REQUEST['cname']); 
	else $opAR = array(''=>'--全部--');
	$opList['filters']['depID'] = $opAR;

	$db = new db();	
	$sqlYear = "select year from ".$tableName." group by year order by year";
	$yearList = array(''=>'--年度選取--');
	$rs = $db->query($sqlYear);
	while($r=$db->fetch_array($rs)) $yearList[$r['year']] = $r['year'];
	$yearList[$nowYear] = $nowYear; //讓明年&今年年度一定出現在下拉選單
	$yearList[$nowYear+1] = $nowYear+1; 
	$opList['filters']['year'] = $yearList;


	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	
	$fieldsList = array(
		"id"=>array("編號","120px",true,array('Id',"gorec(%d)")),
		"projID"=>array("計畫編號","",true,array('Text')),
		"title"=>array("標題","",true,array('Text')),
		"idNum"=>array("計畫主檔","", false, array('Define',showItem)),
		"year"=>array("年度","",true,array('Text')),
		"depID"=>array("部門","",true,array('Select', $departmentinfo)),
		"isUse"=>array("使用","",true,array('Select', array(0=>'否',1=>'是')))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$newYear = array();
	for ($i=$nowYear; $i<$nowYear+2; $i++) $newYear[$i] = $i;

	$fieldA = array(
		"year"=>array("年度","select",1,"",$newYear),
		"depID"=>array("部門","queryID","","depID","departmentinfo",array(true,'','','')),
		"item[0]"=>array("主檔ID","Define","",'', planSelect),
		"title[0]"=>array("名稱","text","40","","",array(true,'','','')),
		"projID[0]"=>array("計畫編號","text","","","",array(true,'','','')),
		"notes[0]"=>array("備註","textbox","","",""),
		"isUse[0]"=>array("是否使用","checkbox","","")
	);
	//新增時的預設值
	$fieldA_Data = array(
		'year'=>($_REQUEST['year']) ? $_REQUEST['year'] : $nowYear+1, 
		'depID'=>($_REQUEST['depID']) ? $_REQUEST['depID'] : '', 
		'isUse[0]'=>1
	);
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"year"=>array("年度","readonly","","",""),
		"depID"=>array("部門","queryID","","depID","departmentinfo",array(true,'','','')),
		"item"=>array("主檔ID","Define","",'', planSelect),
		"title"=>array("名稱","text","40","","",array(true,'','','')),
		"projID"=>array("計畫編號","text","","","",array(true,'','','')),
		"notes"=>array("備註","textbox","","",""),
		"status"=>array("狀態","hidden","","",""),
		"isUse"=>array("是否使用","checkbox","","")
	);
?>