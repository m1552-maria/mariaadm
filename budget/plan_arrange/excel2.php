<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/config.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/budget/excelAll/budget1/getData.php");
	include_once("$root/system/db.php");
  	

	$filename = $_REQUEST['year'].'年計畫編號表-同正版.xlsx';
  
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");
	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel.php');
  	require_once($_SERVER['DOCUMENT_ROOT'].'/maintain/Classes/PHPExcel/IOFactory.php');
	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);

	$header = array(
		'A' =>array('title'=>'單位','width'=>18),
		'B' =>array('title'=>'部門代號','width'=>6),
		'C' =>array('title'=>'計劃編號','width'=>10),
		'D' =>array('title'=>'計劃名稱','width'=>35),
		'E' =>array('title'=>'補充資訊','width'=>20),
		'F' =>array('title'=>'歲入預算','width'=>12),
		'G' =>array('title'=>'歲出預算','width'=>12),
		'H' =>array('title'=>'刪*','width'=>5),
	);

	$i=1;
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->mergeCells('A'.$i.':H'.$i);//合併
    $sheet->setCellValue('A'.$i,'財團法人瑪利亞社會福利基金會');
    $i++;
    $sheet->mergeCells('A'.$i.':H'.$i);//合併
    $sheet->setCellValue('A'.$i, $_REQUEST['year'].'年計畫編號表-同正版');
    $sheet->getStyle('A1'.':H'.$i)->getFont()->setSize(14);

    $i++;
	foreach ($header as $key => $value) {
		$sheet->setCellValue($key.$i, $value['title']);
		$sheet->getColumnDimension($key)->setWidth($value['width']);
	}
	$sheet->getStyle('A1:H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('B'.$i)->getAlignment()->setWrapText(true); //換行

    $sheet->getStyle('A1:H'.$i)->getFont()->setBold(true); //設定粗體

	$db = new db();
	$sql = "select * from plan_arrange where year='".$_REQUEST['year']."'";
	if(!empty($_REQUEST['depID'])) $sql.=" and depID='".$_REQUEST['depID']."'";
	$sql.= " order by projID asc, depID asc";
	$rs = $db->query($sql);
	$con = 0;
	$dep = '';
	while ($r=$db->fetch_array($rs)) {
		$i++;
		if($dep != $r['depID']) {
			if($con > 0) $sheet->mergeCells('A'.($i-$con).':A'.($i-1));
			$sheet->setCellValue('A'.$i, $departmentinfo[$r['depID']]);
			$sheet->getStyle('A'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			$dep = $r['depID'];
			$con = 0;
		}
		$con++;
		$sheet->setCellValue('B'.$i, '');
		$sheet->setCellValue('C'.$i, $r['projID']);
		$sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$sheet->getStyle('C'.$i)->getFont()->getColor()->setARGB('E3170D');
		$sheet->setCellValue('D'.$i, $r['title']);
		$sheet->setCellValue('E'.$i, $r['notes']);
		$color = ($r['isUse']) ? '1E90FF' : 'C0C0C0';
		$sheet->getStyle('D'.$i.':E'.$i)->getFont()->getColor()->setARGB($color);

		$money = getMoney($r['id']);
		$sheet->setCellValue('F'.$i, number_format($money['+']['total']));
		$sheet->setCellValue('G'.$i, number_format($money['-']['total']));
		$sheet->getStyle('F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		$sheet->getStyle('F'.$i)->getFont()->getColor()->setARGB('E3170D');
		$sheet->getStyle('G'.$i)->getFont()->getColor()->setARGB('E3170D');
		if($color == 'C0C0C0') $sheet->setCellValue('H'.$i, '*');

	}
	$sheet->getStyle('A4:H'.$i)->getAlignment()->setWrapText(true); //換行
	if($con > 1) $sheet->mergeCells('A'.($i-$con+1).':A'.($i));

	$sheet->getStyle('A3:G'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //畫格線
	$sheet->getStyle('A2'.':H'.$i)->getFont()->setSize(12);
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');

  	function getMoney($pID) {
  		$db = new db();
  		$tList = array(
			'+'=>array('total'=>0),
			'-'=>array('total'=>0)
		);
		$sql = "select * from plan_arrange_item where pID ='".$pID."'";
		$rs = $db->query($sql);
		$data = array('year'=>$_REQUEST['year']);
		$planIDList = array();
		if($db->num_rows($rs) == 0) {
			$tList = array(
				'+'=>array('total'=>''),
				'-'=>array('total'=>'')
			);
			return $tList;
		}

		while ($r=$db->fetch_array($rs)) $planIDList[] = $r['planID'];
		$data['planID'] = implode(',', $planIDList);
		$result = getBudget1Data($data);

		foreach ($result['dataList'] as $key => $value) {
			$tList[$value['vType']]['total'] += (int)$value['total'];
		}
	
		return $tList;
    }
?>