<?php
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";

	$db = new db();
	$fields = array(
		'`projID`= ?',
		'`year`= ?'
	);
	$data = array(
		$_POST['projID'],
		$_POST['year']
	);

	if(!empty($_POST['id'])) {
		$fields[] = '`id` <> ?';
		$data[] = $_POST['id'];
	}

	$sql = "select id from plan_arrange where ".implode(' and ', $fields);
	$rs = $db->query($sql, $data);
	$rCount = $db->num_rows($rs);

	echo $rCount;
	exit;
	
?>