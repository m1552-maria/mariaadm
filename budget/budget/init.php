<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "預算明細";
	$tableName = "budget";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 5;
	$NeedFilter = '';
	/*echo "<pre>";
	print_r($_SESSION);
	exit;*/
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$nowYear = date('Y')-1911;
	$statusList = array(''=>'--申請狀態--', 0=>'未通過', 1=>'通過');
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'DESC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,''),
			//'year'=>array($nowYear, false)
		),
		"filters"=>array( //過濾器
			'year'=>array(),
			'status'=>$statusList
		),
		"extraButton"=>array( //新增的button
		)
	);

	$db = new db();	
	$sqlYear = "select year from ".$tableName." group by year order by year";
	$yearList = array(''=>'--年度選取--');
	$rs = $db->query($sqlYear);
	while($r=$db->fetch_array($rs)) $yearList[$r['year']] = $r['year'];
	$yearList[$nowYear] = $nowYear; //讓今年度一定出現在下拉選單
	$opList['filters']['year'] = $yearList;


	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	
	unset($statusList['']);
	$fieldsList = array(
		"id"=>array("編號","120px",true,array('Id',"gorec(%d)")),
		"year"=>array("年度","",true,array('Text')),
		"title"=>array("標題","",true,array('Text')),
		"creator"=>array("建單人","",true,array('select', $emplyeeinfo)),
		"status"=>array("狀態","",true,array('Select', $statusList))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$newYear = array();
	for ($i=$nowYear; $i<$nowYear+10; $i++) $newYear[$i] = $i;
	$sql = "select id, title from plan_stringmap where isReady = 1 and class= 'mtype'";
	$rs = $db->query($sql);
	$mtypeList = array();
	while($r=$db->fetch_array($rs)) $mtypeList[$r['id']] = $r['title'];

	$fieldA = array(
		"id"=>array("<span class='need'>*</span>預算編號","text","","","",array(true,'^.[A-Za-z0-9]+$','只能填英數混合','')),
		"year"=>array("年度","select",1,"",$newYear),
		"title"=>array("<span class='need'>*</span>名稱","text","60","","",array(true,'','','')),
		"creator"=>array("建單人","readonly","","",""),
		"status"=>array("狀態","select",1,"",$statusList)
	);
	//新增時的預設值
	$fieldA_Data = array('creator'=>$_SESSION['Name']);
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"year"=>array("年度","select",1,"",$newYear),
		"title"=>array("<span class='need'>*</span>名稱","text","60","","",array(true,'','','')),
		"creator"=>array("建單人","readonly","","",""),
		"status"=>array("狀態","select",1,"",$statusList)
	);
?>