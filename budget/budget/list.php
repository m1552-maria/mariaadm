<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	//include "../system/utilities/miclib.php";

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	if($act=='new' || $act=='edit') $_SESSION['plan_tab'] = 0;
	$tabidx = (isset($_SESSION['plan_tab'])) ? $_SESSION['plan_tab'] : 0;
	
	if($act=='del') { //:: Delete record -----------------------------------------
		foreach($_POST['ID'] as $k=>$v) $aa[] = "'$v'";
		$lstss = implode(',',$aa);
		$aa =array_keys($fieldsList);	
		$db = new db();	

		$sql = "select * from $tableName where $aa[0] in ($lstss) and status = 0";
		$rs = $db->query($sql);
		$delIDList = array();
		while($r=$db->fetch_array($rs)) {
			//檢查是否要順便刪除檔案 與是否可以刪除檔案
			if(!empty($delFlag)) { 
				$fn = $r[$delField];
				if($fn) {
					if(checkStrCode($fn,'utf-8')) $fn=iconv("utf-8","big5",$fn); 
					$fn = $ulpath.$fn;
					//echo $fn;
					if(file_exists($fn)) @unlink($fn);
				}
			}
			$delIDList[] = "'".$r[$aa[0]]."'";
		}

		if(count($delIDList) > 0) {
			$sql = "delete from $tableName where $aa[0] in (".implode(',', $delIDList).")";
			//echo $sql; exit;
			$db->query($sql);

			//將子檔也刪除
			$sql = "delete from budget_items where pid in (".implode(',', $delIDList).")";
			$db->query($sql);
			$sql = "delete from budget_talent where pid in (".implode(',', $delIDList).")";
			$db->query($sql);
	
		}
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------
	
?>	


<?php include($root."/budget/common/header.php");?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
<?php include($root."/budget/common/menu.php");?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- Modal -->
        <div class="modal fade hide" id="messageBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">訊息</h4>
              </div>
              <div class="modal-body">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn blue" data-dismiss="modal">確認</button>
              </div>
            </div>
          </div>
        </div>


            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	echo "<h1 align='center'>$pageTitle ";
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div><hr>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	/*echo $pinf->curPage;
	echo $sql;*/
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	$obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	echo "<hr>";
	
?>

<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom">
			<ul class="nav nav-tabs">
				<li <?=($tabidx==0?"class='active'":'')?>><a href="#tab0" data-toggle="tab">預算內容<img id="tabg0" class="tabimg" src="/images/<?= $tabidx==0?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(0)"/></a></li>
				<li <?=($tabidx==1?"class='active'":'')?>><a href="#tab1" data-toggle="tab">計劃項目<img id="tabg1" class="tabimg" src="/images/<?= $tabidx==1?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(1)"/></a></li>
				<li <?=($tabidx==2?"class='active'":'')?>><a href="#tab2" data-toggle="tab">人力編制<img id="tabg2" class="tabimg" src="/images/<?= $tabidx==2?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(2)"/></a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane<?=($tabidx==0?' active':'')?>" id="tab0">
<?
	//:ViewControl Object ------------------------------------------------------------------------
	$formName = 'form1';
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			$op3 = array(
				"type"=>"edit",
				"form"=>array($formName,'doedit.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定修改",false,''),
				"cancilBtn"=>array("取消修改",false,'')	);
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"type"=>"append",
				"form"=>array($formName,'doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			//if(count($pdata) == 0) break;
			$op3 = array(
				"type"=>"view",
				"submitBtn"=>array("確定變更",true,''),
				"resetBtn"=>array("重新輸入",true,''),
				"cancilBtn"=>array("關閉",true,''));
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);		
	}
	$budgetID = (isset($pdata[$listPos]['id'])) ? $pdata[$listPos]['id'] : 0;

?>
				</div>
				<div class="tab-pane<?=($tabidx==1?' active':'')?>" id="tab1">
			    	<iframe width="100%" height="420" frameborder="0" src="../budget_items/list.php?pid=<?=$budgetID?>"></iframe>
			    </div>
				<div class="tab-pane<?=($tabidx==2?' active':'')?>" id="tab2">
			    	<iframe width="100%" height="420" frameborder="0" src="../budget_talent/list.php?pid=<?=$budgetID?>"></iframe>
			    </div>
			</div>
		</div>

<!--END TABS-->



                    </div>
                </div>
     
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
<?php include '../public/inc_listjs.php';?>
<script>

var formName = "<?php echo $formName;?>";
$(function () {
	$(document).on('input','[name="id"]', function(){
		var idThis = this;
		$.ajax({
            url:'checkId.php',
            type:'post',
            data:{id:idThis.value},
            dataType:'json',
           // async: false,
            success:function(d){
            	if(d == 0) idThis.setCustomValidity('');
            	else idThis.setCustomValidity('此編號已經存在');
            }
        })  
	})
}); 
function setnail(n) {
	$('.tabimg').attr('src','/images/unnail.png');
	$('#tabg'+n).attr('src','/images/nail.png');
	$.get('setnail.php',{'idx':n});
}

</script>

<?php include($root."/budget/common/footer.php");?>
