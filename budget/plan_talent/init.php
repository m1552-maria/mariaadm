<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	if(!isset($_REQUEST['fid'])) exit;
	$fID = $_REQUEST['fid'];
	$sID = $_REQUEST['sid'];
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";

	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/budget/inc_vars.php";
	
	
	$pageTitle = "人力編制";
	$tableName = "plan_talent";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 1000;
	$NeedFilter = "sid = '".$sID."'";
	
	$db = new db();
	$sql = "select status, year from plan where id = '".$fID."'";
	$rs = $db->query($sql);
	$r=$db->fetch_array($rs);
	$status = (int)$r['status'];
	$year = (int)$r['year'] + 1911;

	//人力編製權限為 總後台 副主任以上 可使用薪資作業的帳號
	unset($editJop['G007']);
	
	/*判別按鈕是否可使用*/
	$canShow = 1;
	//if($_SESSION['privilege'] > 10 || $status >= 20 || (!in_array($_SESSION['jobID'], $editJop) && !in_array('Q', $_SESSION['user_priviege']) && $_SESSION['empID'] != '00433')) $canShow = 0;
	if($_SESSION['privilege'] <=10 && $status >= 20) $canShow = 0;
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,false,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,false,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,false,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,false,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,false,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => (!$canShow) ? array("＋新增",true,true,'') : array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => (!$canShow) ? array("Ｘ刪除",true,true,'') : array("Ｘ刪除",false,true,'doDel') 
	);

	if(empty($sID)) {
		$opPage["searchBtn"] = array("◎搜尋",true,true,'SearchKeyword');
		$opPage["newBtn"] = array("＋新增",true,true,'');
		$opPage["editBtn"] = array("－修改",true,true,'');
		$opPage["delBtn"] = array("Ｘ刪除",true,true,'');
	}

	//:ListControl  ---------------------------------------------------------------

	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array('autochange'=>true,'goBtn'=>array("確定",false,true,'')),
		"filters"=>array( //過濾器
		),
		/*"extraButton"=>array( //新增的button
			array("進階搜尋",false,true,'Button1Click'),
			array("Button2",false,true,'')
		)*/
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/

	$sql = "select * from subject_map where levels = 2 order by id asc";
	$rs = $db->query($sql);
	$subjectList = array();
	while($r=$db->fetch_array($rs)) {
		$subjectList[$r['id']] = $r['title'];
	}

	$sql = "select empID,jobID from emplyee where jobID<>''";
 	$rsX = db_query($sql);
	while($rx = db_fetch_array($rsX) ) {
		 $jobIDList[$rx['empID']] = $rx['jobID'];
	}

	$jobinfo = array_merge($jobinfo,$classOther);
	ksort($jobinfo);

	$db = new db();
	$sql = "select * from plan_items where pid='".$fID."'";
	$rs = $db->query($sql);
	$sList = array();
	while($r=$db->fetch_array($rs)) $sList[$r['id']] = $r['title'];
	$emplyeeinfo[''] = '新聘或留停';

	$fieldsList = array(
		"id"=>array("編號","50px",true,array('Id',"gorec(%d)")),
		//"fid"=>array("計畫編號","",true,array('Text')),
		"sid"=>array("計畫名稱","",true,array('Select',$sList)),
		"empID"=>array("人員名稱","60px",true,array('Select', $emplyeeinfo)),
		"jobID"=>array("職銜","",true,array('Select', $jobinfo)),
		"rate"=>array("人員使用百分比","50px",false,array('Define', 'rateChange')),
		"subject"=>array("預編科目","",true,array('Select', $subjectList))
	);
	$emplyeeinfo[''] = '';
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	
	/*echo "<pre>";
	print_r($jobinfo);
	exit;*/
	$fieldA = array(
		"fid[]"=>array("主檔編號","hidden",20,'','',''),
		"_sid[]"=>array("子計畫","readonly",5,'',$sList,''),
		"sid[]"=>array("子計畫","hidden",20,'',$sList,''),
		"empID[]"=>array("員工編號","queryID",10,"","emplyeeinfo",array(true,'','','')),
		"jobID[]"=>array("職銜","select",1,'',$jobinfo,''),
		"rate[]"=>array("人員使用百分比","text",5,"","",array(true,PTN_NUMBER,'只能填數字','')),
		"bDate[]"=>array("開始日期","date",10,"","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"eDate[]"=>array("結束日期","date",10,"","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"amount[]"=>array("金額","text",10,"","",array(true,PTN_NUMBER,'只能填數字','')),
		"noEmp[]"=>array("非編制", "checkbox","","",""),
		"subject[]"=>array("預編科目","select",1,'',$subjectList,''),
		"overPay[]"=>array("加班費","text",5,"","",array(true,PTN_NUMBER,'只能填數字','')),
		"isLinkage[]"=>array("聯動百分比", "checkbox","","","")
	);
	/*新增時的預設值*/
	$fieldA_Data = array("fid[]"=>$fID, "sid[]"=>$sID, "_sid[]"=>$sID, "subject[]"=>'B5601', 'bDate[]'=>$year.'/01/01', 'eDate[]'=>$year.'/12/31', 'overPay[]'=>0, 'isLinkage[]'=>1);

	$fieldE = array(
		"id"=>array("編號","id",20),
		"fid"=>array("主檔編號","hidden",20,'','',''),
		"sid"=>array("計畫名稱","readonly",5,"",$sList),
		"empID"=>array("員工編號","readonly",10,"",$emplyeeinfo),
		"jobID"=>array("職銜","select",1,'',$jobinfo,''),
		"rate"=>array("人員使用百分比","Define",5,"","rateChangeEdit",array(true,PTN_NUMBER,'只能填數字','')),
		"bDate"=>array("開始日期","date",10,"","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"eDate"=>array("結束日期","date",10,"","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"amount"=>array("金額","text",10,"","",array(true,PTN_NUMBER,'只能填數字','')),
		"_noEmp"=>array("非編制", "checkbox","","",""),
		"noEmp"=>array("非編制", "hidden","","",""),
		"subject"=>array("預編科目","select",1,'',$subjectList,''),
		"overPay"=>array("加班費","text",5,"","",array(true,PTN_NUMBER,'只能填數字','')),
		"isLinkage"=>array("聯動百分比", "checkbox","","","")
	);
?>