<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	//include "../system/utilities/miclib.php";

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	
	if($act=='del') { //:: Delete record -----------------------------------------
		$aa = array();
		foreach($_POST['ID'] as $v) $aa[] = "'$v'";
		$lstss = join(',',$aa);
		$aa =array_keys($fieldsList);		
		$db = new db();	
		
		$sql = "select * from $tableName where $aa[0] in ($lstss)";
		$rs = $db->query($sql);
		while($r=$db->fetch_array($rs)) {
			//檢查是否要順便刪除檔案
			if(isset($delFlag)) { 
				$fns = $r[$delField];
				if($fns) {
					$ba = explode(',',$fns);
					foreach($ba as $fn) {
						$fn = $root.$ulpath.$fn;
						//echo $fn;
						if(file_exists($fn)) @unlink($fn);
					}
				}
			}

			//刪除教育機構補助表 & 經費來源表的人
			/*$sql = "delete from plan_education where empID='".$r['empID']."' and sid='".$r['sid']."'";
			$db->query($sql);
			$sql = "delete from plan_budget where empID='".$r['empID']."' and sid='".$r['sid']."'";
			$db->query($sql);*/
		}




		
		$sql = "delete from $tableName where $aa[0] in ($lstss)";
		//echo $sql; exit;
		$db->query($sql);
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------

	function rateChange($val, $obj) {
		//顯示為正數
		return abs($val);
	}

	function rateChangeEdit($val, $obj) {
		//顯示為正數
		$html = '<input type="text" name="rate" size="5" value="'.abs($val).'" required pattern="[0-9]*" title="只能填數字" class="input">';
		return $html;
	}

?>	
<?php 
    header("Cache-control: private");

    $_SESSION['maintain']['name'] = (isset($_SESSION['Name'])) ? $_SESSION['Name'] : $_SESSION['empName'];

    $message = '';
    if(isset($_SESSION['maintain']['MESSAGE']) && gettype($_SESSION['maintain']['MESSAGE']) == 'string') {
        $message = $_SESSION['maintain']['MESSAGE'];
        unset($_SESSION['maintain']['MESSAGE']);
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
    <link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 

<script src="/budget/media/script/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
</head>
<body>
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	
	//: Header -------------------------------------------------------------------------
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo "<p>搜尋:".$opList['keyword']."</p>";
	
	//:PageControl ---------------------------------------------------------------------
	if(empty($_REQUEST['openWindow'])) {	
		$obj = new EditPageControl($opPage, $pinf);	
		echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div>";
	}

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	//echo $pinf->curPage;
	//echo $sql;
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);

	//:ViewControl Object ------------------------------------------------------------------------
	$formName = 'formPlanTalent';
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			if(($_SESSION['privilege'] > 10 || in_array($_SESSION['jobID'], $editJop) || $_SESSION['empID'] == '00433' || in_array('Q', $_SESSION['user_priviege'])) && $status < 20) {
				$op3 = array(
					"templet"=>$root."/budget/".$tableName."/edit.html",
					"type"=>"edit",
					"form"=>array($formName,'doedit.php','post','multipart/form-data','form1Valid'));
			} else {
				$op3 = array(
					"templet"=>$root."/budget/".$tableName."/_edit.html",
					"type"=>"edit");
			}

			

			$sql = "select depID from mariaadm.emplyee where empID = ? and isOnduty = '1'";
			$rs = $db->query($sql, array($pdata[$listPos]['empID']));
			$r=$db->fetch_array($rs);
			if($_SESSION['privilege'] <= 10 && $_SESSION['depID'] != $r['depID']) $isPass = 1;

			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);
			break;
		case 'new':	//新增
			$op3 = array(
				"templet"=>$root."/budget/".$tableName."/add.html",
				"type"=>"append",
				"form"=>array($formName,'doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			$listObj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	}
?>
<?php include '../public/inc_listjs.php';?>
<script>
var formName = "<?php echo $formName;?>";
var isPass = '<?php echo ($isPass) ? $isPass : 0 ;?>';
var jobIDList = <?php echo json_encode($jobIDList);?>;
$(function () {
	/*判斷手KEY員工 是否正確*/
	$(document).on('input','[name="'+formName+'"] [name^="empID"]', function(){
		var thisDom = this;
		param = $(this);
		$.get(
			'getEmp.php',
			{'empID':$(thisDom).val(),'fid':'<?php echo $fID;?>','sid':'<?php echo $sID;?>'},
			function(d){ 
				data = JSON.parse(d);
				returnValue(data['data']);
				if(data['result'] == 0) thisDom.setCustomValidity(data['msg']);
				else thisDom.setCustomValidity('');
				thisDom.reportValidity(); //直接顯示錯誤訊息
				
			}
		);
	});

	$(document).on('input','[name="'+formName+'"] [name^="rate"]', function(){
		
		var thisDom = this;
		var k = $(this).parents('tr').attr('data-two-tr');

		if($('[data-one-tr="'+k+'"] [name^="noEmp"]').val() == 1 || $('[data-one-tr="'+k+'"] [name^="empID"]').val() == '') {
			
			return false;
		}

		var id = ($('[data-one-tr="'+k+'"] [name^="id"]').length) ? $('[data-one-tr="'+k+'"] [name^="id"]').val() : 0;
		$.get(
			'getRate.php',
			{'empID':$('[data-one-tr="'+k+'"] [name^="empID"]').val(),
			'fid':'<?php echo $fID;?>', 
			'rate':$(thisDom).val(), 'id':id},
			function(d){ 

				data = JSON.parse(d);
				console.log(data);
				$(thisDom).val(data['rate']);
				if(data['result'] == 0) thisDom.setCustomValidity(data['msg']);
				else thisDom.setCustomValidity('');

				//alert(0);
				//勾選連動狀態
				/*if($('[data-three-tr="'+k+'"] [name^="isLinkage"]').prop('checked')) {
					$('[data-three-tr="'+k+'"] [name^="amount"]').val(Math.round(data['money'] / 100 * data['rate']));
				}*/
				/*if($('[data-check-money="'+k+'"]').prop('checked')) {
					$('[data-three-tr="'+k+'"] [name^="amount"]').val(Math.round(data['money'] / 100 * data['rate']));
				}*/

				thisDom.reportValidity(); //直接顯示錯誤訊息
			}
		);
	});

	$(document).on('click', '[name="'+formName+'"] [name^="noEmp"]', function(){
		var key = $('[name^="noEmp"]').index(this);
		$('[name^="empID"]').eq(key).val('');
		$('[name^="rate"]').eq(key).val('');
		$(this).prev('.formTxS').html('');
		if($('[name^="amount"]').eq(key).attr('type') == 'password') $('[name^="amount"]').eq(key).val('');
		$('[name^="amount"]').eq(key).attr('type','text');
		if($(this).prop('checked')) {
			$('.formTxS').eq(key).html('');
			$('[name^="empID"]').eq(key).attr('readonly','readonly');
			$(this).val(1);
			/*$('[name^="rate"]').eq(key).attr('readonly','readonly');
			$('[name^="rate"]').eq(key).val('100');*/
		} else {
			$('[name^="empID"]').eq(key).removeAttr('readonly');
			$('[name^="rate"]').eq(key).removeAttr('readonly');
			$(this).val('');
		}


	});

	$(document).on('click', '[name="'+formName+'"] [name^="isLinkage"]', function(){
		if($(this).prop('checked')) $(this).val(1);	
		else $(this).val('');
	});
	
	//if(isPass == 1) $('[name="amount"]').attr('type', 'password');


	/*刪除檔案後要重整*/
	/*parent.document.getElementById('plan_education').src = parent.document.getElementById('plan_education').src
	parent.document.getElementById('plan_budget').src = parent.document.getElementById('plan_budget').src
*/
}); 


/*showDialog 用於複寫 原先form.js的showDialog*/
function showDialog(aObj) {
	if(rzt){
		rzt.close();
		rzt = false;
	} 
	param = $(aObj).prev();
	var parid = 'empID';
	var procP = '/budget/plan_talent/query_id.php';
	procP += "?fid=<?php echo $fID;?>&sid="+$('[name="'+formName+'"] [name^="sid"]').val();
 	rzt = window.open(procP,'','width=300,height=450');
}

/*returnValue 用於複寫 原先form.returnValue*/
function returnValue(data) {
	console.log(data);
	if(data) {
		var isSet = 1;
		var k = param.parents('tr').attr('data-one-tr');
		$('[name^="empID"]').each(function(){
			if(data['empID'] == $(this).val() &&  k != $(this).parent('td').parent('tr').attr('data-one-tr') && $(this).val() != '') {
				isSet = 0;
				alert('人員重複');
				param.val('');
				param.next().next('span').html(''); 
				param.parent('td').next('td').next('td').children('[name^="rate"]').val('');
				$('[data-three-tr="'+k+'"] [name^="amount"]').val('');	
				return false;
			}
		});
		if(!isSet) return false;
		param.val(data['empID']);
		param.next().next('span').html(data['empName']); 
		param.parent('td').next('td').next('td').children('[name^="rate"]').val(data['rate']);
		if($('[data-three-tr="'+k+'"] [name^="isLinkage"]').prop('checked')) {
			data['money'] = Math.round(data['money'] / 100 * data['rate']);
		}
		/*if($('[data-check-money="'+k+'"]').prop('checked')) {
			data['money'] = Math.round(data['money'] / 100 * data['rate']);
		}*/
		var money = (data['money'] > 0) ? data['money'] : '';
		
		//$('[data-three-tr="'+k+'"] [name^="amount"]').val(money);	
		$('[data-one-tr="'+k+'"] [name^="jobID"]').val(jobIDList[data['empID']]);	
		/*if(!data['unsameDep']) $('[data-three-tr="'+k+'"] [name^="amount"]').attr('type', 'text');
		else $('[data-three-tr="'+k+'"] [name^="amount"]').attr('type', 'password');*/
		//$('[data-three-tr="'+k+'"] [name^="amount"]').attr('type', 'password');
		$('[data-two-tr="'+k+'"] [name^="rate"]').val(data['rate']);	
	}
}


function doAppend() {
	page=<?=$pinf->pages?>;
	var url = "/budget/plan_talent/list.php?openWindow=1&act=new&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
}

function doEdit() {
	var url = "/budget/plan_talent/list.php?openWindow=1&act=edit&recno="+recno+"&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
}

function gorec(recno) { //go edit ViewControl
	var url = "/budget/plan_talent/list.php?openWindow=1&act=edit&recno="+recno+"&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
	tdChange(recno);
}

function form1Valid() {
	var data = [];
	var _data = {};
	var text = '新增';
<?php if($act == 'new') { ?>
		$('#newTable [name]').each(function(){
			var arr = [];
			var n = $(this).attr('name');
			n = n.split('[]');
			n = n[0];
			if(!data[n]) data[n] = [];
			data[n].push($(this).val());
		});

		for(var i in data) {
			if(!_data[i]) _data[i] = {};
			for(var j in data[i]) {
				_data[i][j] = data[i][j];
			}
		}

<?php } else { ?>
	text = '修改';
	$('#newTable [name]').each(function(){
		_data[$(this).attr('name')] = $(this).val();
	});
<?php } ?>
	$.ajax({
		type:'post',
		url:'<?php echo ($act == "new") ? "doappend.php" : "doedit.php" ;?>',
		data: _data,
		success: function(d){
			alert(text+'人力編制完成');
			window.opener.closeItem('plan_talent');
			window.opener.closeItem('plan_currency');
			window.opener.oItem = false;
			window.close();
		}
	});
	
	return false;
}


function tdChange(recno) {
	$('.ListRow td').removeClass('columnCurrent');
	$('.ListRow:odd td').addClass('columnDataOdd');
	$('.ListRow:even td').addClass('columnDataEven');
	$('.ListRow').eq(recno-1).children('td').removeClass('columnDataOdd');
	$('.ListRow').eq(recno-1).children('td').removeClass('columnDataEven');
	$('.ListRow').eq(recno-1).children('td').addClass('columnCurrent');
}
</script>
</body>
</html>
