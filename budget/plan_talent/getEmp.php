<?php
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include "$root/system/db.php";
	include("checkEmp.php");


	$result = array('result'=>1,'data'=>array('empID'=>$_REQUEST['empID'],'empName'=>'','rate'=>'','unsameDep'=>0));
	$db = new db();
	
	$ch = checkEmp('plan_talent');
	if(!$ch['result']) {
		$result['result'] = 0;
		$result['msg'] = $ch['msg'];
		echo json_encode($result);
		exit;
	}

	if($_SESSION['depID'] != $ch['depID']) $result['data']['unsameDep'] = 1;

	$data = array(
		'empID' => $_REQUEST['empID'],
		'year' => $ch['year']
	);

	$db = new db();
	$sql = "select bugetChange from plan where id = ?";
	$rs = $db->query($sql, array($_REQUEST['fid']));
	if($db->num_rows($rs) == 0) exit;
	$r=$db->fetch_array($rs);
	$bugetChange =$r['bugetChange'];

	if($bugetChange < 2) { //原計畫或者追加
		$rate = 100-getRate($data);
		if($rate <= 0) {
			$result['result'] = 0;
			$result['msg'] = '此員工使用率為0';
			echo json_encode($result);
			exit;
		} 
	} else { //追減
		$rate = getRate($data);
		if($rate == 0 || empty($rate)) {
			$result['result'] = 0;
			$result['msg'] = '此員工尚未被選入計畫';
			echo json_encode($result);
			exit;
		} 
	}
	

	$result['data']['empName'] = $ch['empName'];
	$result['data']['rate'] = $rate;

	$sql = "select * from emplyee_salary where empID='".$_REQUEST['empID']."' order by changeDate desc limit 1";
	$rs2 = db_query($sql);
	$r2 = db_fetch_array($rs2);

	if($r2 == '') $result['data']['money'] = 0;
	else {
		switch ($r2['salaryType']) {
			case 0: //月薪
			 	//20180423 sean 年資加點 若日後有改規則 這邊也要改
				if($r['salary'] < 45800) {
					$result['data']['money'] = $r2['salary']*(1+0.0453+0.06)*12+$r2['salary']*1.5+$r2['salary']*0.074*12;
				} else {
					$result['data']['money'] = $r2['salary']*(1+0.06)*12+45800*0.0453*12 +$r2['salary']*1.5 +$r2['salary']*0.074*12;
				}
				break;
			case 1: //時薪
				$result['data']['money'] = 0;
				break;
			case 2: //產能敘薪
				$result['data']['money'] = 0;
				break;
			case 3: //固定薪
				$result['data']['money'] = 0;
				break;
			
		}
		$result['data']['money'] = round($result['data']['money']);
	}
	
	echo json_encode($result);

?>