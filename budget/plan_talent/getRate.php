<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include "$root/system/db.php";
	include("checkEmp.php");


	$result = array('result'=>1);
	$result['money'] = 0;
	if((int)$_REQUEST['rate'] < 1 || (int)$_REQUEST['rate'] > 100) {
		$result['result'] = 0;
		$result['msg'] = '請輸入1~100整數';
		$result['rate'] = '';
		echo json_encode($result);
		exit;
	}

	$db = new db();
	$result = checkEmp('plan_talent');
	if(!$result['result']) {
		$result['msg'] = '員工資料錯誤';
		$result['rate'] = $_REQUEST['rate'];
		echo json_encode($result);
		exit;
	}

	$data = array(
		'empID' => $_REQUEST['empID'],
		'year' => $result['year']
	);
	if(!empty($_REQUEST['id'])) $data['btID'] = $_REQUEST['id'];

	$db = new db();
	$sql = "select bugetChange from plan where id = ?";
	$rs = $db->query($sql, array($_REQUEST['fid']));
	if($db->num_rows($rs) == 0) exit;
	$r=$db->fetch_array($rs);
	$bugetChange =$r['bugetChange'];

	$rate = getRate($data);
	$result['rate'] = $_REQUEST['rate'];
	if($bugetChange < 2) { //原計畫或者追加
		if($rate + (int)$_REQUEST['rate'] > 100) {
			$result['result'] = 0;
			$result['msg'] = '剩餘使用率為:'.(100 - $rate);
			$result['rate'] = 100 - $rate;
		} 
	} else { //追減
		if((int)$_REQUEST['rate'] > $rate) {
			$result['result'] = 0;
			$result['msg'] = '可扣使用率為:'.$rate;
			$result['rate'] = $rate;
		}
	}

	$sql = "select * from emplyee_salary where empID='".$_REQUEST['empID']."' order by changeDate desc limit 1";
	$rs2 = db_query($sql);
	$r2 = db_fetch_array($rs2);

	if($r2 == '') $result['money'] = 0;
	else {
		switch ($r2['salaryType']) {
			case 0: //月薪
			 	//20180423 sean 年資加點 若日後有改規則 這邊也要改
				if($r['salary'] < 45800) {
					$result['money'] = $r2['salary']*(1+0.0453+0.06)*12+$r2['salary']*1.5+$r2['salary']*0.074*12;
				} else {
					$result['money'] = $r2['salary']*(1+0.06)*12+45800*0.0453*12 +$r2['salary']*1.5 +$r2['salary']*0.074*12;
				}
				break;
			case 1: //時薪
				$result['money'] = 0;
				break;
			case 2: //產能敘薪
				$result['money'] = 0;
				break;
			case 3: //固定薪
				$result['money'] = 0;
				break;
			
		}
		$result['money'] = round($result['money']);
	}
	

	echo json_encode($result);

?>