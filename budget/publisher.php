<?	

if(!isset($cut_width))  $cut_width=300;
if(!isset($cut_height))  $cut_height=300;
if(!isset($pc_default_width))  $pc_default_width=600;
if(!isset($pc_default_height))  $pc_default_height=400;
?>
<link rel="stylesheet" href="/budget/publisher.css">
<script type="text/javascript" src="/Scripts/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="/Scripts/interactjs.js"></script>
<script type="text/javascript" src="/budget/publisher.js"></script>
<script type="text/javascript">
 var imgReader;
 //::裝置識別 -----------------------------------------
  var deviceOS = '';
  if(/Android/i.test(navigator.userAgent)) {
    deviceOS = 'android';
  }else if(/iPhone/i.test(navigator.userAgent)) {
    deviceOS = 'iphone'; 
  }else if(/iPad/i.test(navigator.userAgent)) {
    deviceOS = 'ipad';
  }else if(/IEMobile/i.test(navigator.userAgent)) { 
    deviceOS = 'IEMobile';
  }else deviceOS = 'none';
  
 var pc_default_width='<?=$pc_default_width?>';
 var pc_default_height='<?=$pc_default_height?>';
 //console.log('@publisher deviceOS:'+deviceOS);
 $(function(){
  	imgReader = new imgReader('<?=$cut_width?>','<?=$cut_height?>',1);
 });
 //::----------------- 裁切框 draggable start-----------------------
interact('.draggable')
  .draggable({
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    restrict: {
      restriction: "parent",
      endOnly: true,
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
    // enable autoScroll
    autoScroll: true,

    // call this function on every dragmove event
    onmove: dragMoveListener,
    // call this function on every dragend event
    onend: function (event) {
    }
  });

  function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }

  // this is used later in the resizing and gesture demos
  window.dragMoveListener = dragMoveListener;
//::----------------- 裁切框 draggable end-----------------------
 
function go(act){
  switch(act){
    case "menu": location.href="../menu.php";  break;
    case "mainmenu": location.href="../mainmenu/publisher.php";  break;
    case "stickers": location.href="../stickers/publisher.php";  break;
    case "logout":   location.href="../index.php?act=logout";    break;
  }
}
function ckFile(){
	/*
	img[0]:image
	img[1]:del icon
	img[2]:cut icon
	div[0]:裁切框
	div[1]:裁切框內的半透明背景
	*/

	var photoItems=document.getElementsByClassName("photoItem");
	var oItem,img,ratio,cut,cutL,cutT;
	oItem = $(photoItems[0]).eq(0);
	img=oItem.find("img").get(0);
	cut=oItem.find("div").get(0);
  //裁切框在螢幕的原始大小：imgReader.cutWidth*imgReader.zoomRatio
	ratio=($(cut).width()/(imgReader.cutWidth*imgReader.zoomRatio)).toFixed(2);
	cutT=$(cut).offset().top-$(img).offset().top;
	cutL=$(cut).offset().left-$(img).offset().left;

  var ary=[];
	if($(cut).css("display")=="none"){//沒有做裁切
    if($(img).width()==imgReader.cutWidth && $(img).height()==imgReader.cutHeight){
      ary.push(0);
    }else{
      if($(oItem).css("display")!="none"){
      alert("圖片限定為："+imgReader.cutWidth+"x"+imgReader.cutHeight+"，請先進行圖片裁切");
      return false;
      }
    }
	}else{
		ary.push(ratio);
    //框的Left:cutL/imgReader.zoomRatio, 再除以縮放比例ratio：cutL/imgReader.zoomRatio/ratio 
		ary.push(cutL/imgReader.zoomRatio/ratio);
		ary.push(cutT/imgReader.zoomRatio/ratio);
    ary.push(imgReader.cutWidth);
    ary.push(imgReader.cutHeight);
	}
  $("#val").val(ary.join(","));
	//console.log($("#val").val());
  return true;
}
</script>
