<?php
	function checkEmp() {
		global $db;
		$result = array('result'=>1, 'year'=>0, 'empName'=>'');
		$sql = "select empName from mariaadm.emplyee where empID = ? and isOnduty = '1'";
		$rs = $db->query($sql, array($_REQUEST['empID']));
		if($db->num_rows($rs) == 0) {
			$result['result'] = 0;
			$result['msg'] = '查無此員工';
			return $result;
		}
		$r=$db->fetch_array($rs);
		$result['empName'] = $r['empName'];

		$sql = "select year from budget where id = ?";
		$rs = $db->query($sql, array($_REQUEST['pid']));
		if($db->num_rows($rs) == 0) {
			$result['result'] = 0;
			$result['msg'] = '預算主檔錯誤';
			return $result;
		}
		$r=$db->fetch_array($rs);
		$result['year'] = $r['year'];

		if(empty($_REQUEST['id'])) {
			$sql = "select 1 from budget_talent where pid = ? and empID = ?";
			$rs = $db->query($sql, array($_REQUEST['pid'], $_REQUEST['empID']));
			if($db->num_rows($rs) > 0) {
				$result['result'] = 0;
				$result['msg'] = '員工已經存在';
			}
		}
		
		return $result;
	}

	function getRate($data = array()) {
		global $db;
		$sql = "select IFNULL(sum(bt.rate),0) as rate from budget_talent as bt inner join budget as b on b.id=bt.pid where bt.empID = '".$data['empID']."'";
		if(!empty($data['year'])) $sql.= " and b.year = '".$data['year']."'";
		if(!empty($data['btID'])) $sql.= " and bt.id <> '".$data['btID']."'";
		$rs = $db->query($sql);
		$r = $db->fetch_array($rs);
		return (int)$r['rate'];
	}

?>