<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include "$root/system/db.php";

	$db = new db();
	$sql = "select year from budget where id = ?";
	$rs = $db->query($sql, array($_REQUEST['pid']));
	if($db->num_rows($rs) == 0) exit;
	$r=$db->fetch_array($rs);
	$year = $r['year'];

	$sql = "select empID from budget_talent where pid =?";
	$rs = $db->query($sql, array($_REQUEST['pid']));
	$hasEmpList = array();
	if($db->num_rows($rs) > 0) {
		while ($r = $db->fetch_array($rs)) {
			$hasEmpList[$r['empID']] = "'".$r['empID']."'";
		}
	}

	$sql = "select (100-IFNULL(ee.total, 0)) as rate, e.* from mariaadm.emplyee as e inner join (select e.empID, (select sum(bt.rate) from mariaadm_budget.budget_talent as bt inner join mariaadm_budget.budget as b on b.id=bt.pid where bt.empID=e.empID and b.year='".$year."') as total from mariaadm.emplyee as e where e.isOnduty=1) as ee on e.empID = ee.empID where (ee.total is null or ee.total <> '100')";

	if(count($hasEmpList) > 0) $sql.=" and e.empID not in(".implode(',', $hasEmpList).")";
	if(!empty($_REQUEST['depID'])) $sql.=" and e.depID like '".$_REQUEST['depID']."'";


	$rs = $db->query($sql);

?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font:15px "微軟正黑體",Verdana;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	#browser li {
		list-style-image: url(../../images/man.png);
		list-style-position: outside;
		padding: 0 4px 0 4px; 
		cursor:pointer;
	}
  </style>
  <script type="text/javascript" src="/Scripts/jquery.js"></script>  
  <script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		$(document).ready(function(){
			//selid = dialogArguments;
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
			
	  function setID(obj,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue(obj);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}		
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
	<tr>
   	<th id="tblHead" bgcolor="#CCCCCC">::: 員工代碼表 :::</th>
  </tr>
	<tr><td bgcolor="#FFFFFF">
    <div id="markup" class="whitebg"><ul id="browser">
    <? while($r=$db->fetch_array($rs)) {
    	$data = array();
    	$data['empID'] = $r['empID'];
    	$data['empName'] = $r['empName'];
    	$data['rate'] = $r['rate'];
    	echo "<li><a onClick=setID(".json_encode($data).",this)>".$r['empName']."(".$r['empID'].") - ".$r['rate']."%</a></li>";}
    	 ?> 
    </ul></div>
	</td></tr>
</table>
</body>
</html>