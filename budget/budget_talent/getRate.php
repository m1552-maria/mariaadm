<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include "$root/system/db.php";
	include("checkEmp.php");

	/*print_r($_REQUEST);
	exit;*/


	$result = array('result'=>1);
	if((int)$_REQUEST['rate'] < 1 || (int)$_REQUEST['rate'] > 100) {
		$result['result'] = 0;
		$result['msg'] = '請輸入1~100整數';
		$result['rate'] = '';
		echo json_encode($result);
		exit;
	}

	$db = new db();
	$result = checkEmp();
	if(!$result['result']) {
		$result['msg'] = '員工資料錯誤';
		$result['rate'] = $_REQUEST['rate'];
		echo json_encode($result);
		exit;
	}

	$data = array(
		'empID' => $_REQUEST['empID'],
		'year' => $result['year']
	);
	if(!empty($_REQUEST['id'])) $data['btID'] = $_REQUEST['id'];

	$rate = getRate($data);
	$result['rate'] = $_REQUEST['rate'];
	if($rate + (int)$_REQUEST['rate'] > 100) {
		$result['result'] = 0;
		$result['msg'] = '剩餘使用率為:'.(100 - $rate);
		$result['rate'] = 100 - $rate;
	} 

	echo json_encode($result);

?>