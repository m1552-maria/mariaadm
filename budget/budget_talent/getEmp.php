<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";
	include("checkEmp.php");

	$result = array('result'=>1,'data'=>array('empID'=>$_REQUEST['empID'],'empName'=>'','rate'=>''));
	$db = new db();
	
	$ch = checkEmp();
	if(!$ch['result']) {
		$result['result'] = 0;
		$result['msg'] = $ch['msg'];
		echo json_encode($result);
		exit;
	}

	$data = array(
		'empID' => $_REQUEST['empID'],
		'year' => $ch['year']
	);
	$rate = 100-getRate($data);
	if($rate <= 0) {
		$result['result'] = 0;
		$result['msg'] = '此員工使用率為0';
		echo json_encode($result);
		exit;
	} 

	$result['data']['empName'] = $ch['empName'];
	$result['data']['rate'] = $rate;
	echo json_encode($result);

?>