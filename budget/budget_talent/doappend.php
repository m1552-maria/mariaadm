<?
	
	include("init.php");	
	//include "../system/utilities/miclib.php";	
	include_once("checkEmp.php");
	$_REQUEST['rate'] = (int)$_REQUEST['rate'];
	if($_REQUEST['rate'] < 1 || $_REQUEST['rate'] > 100) exit;

	$db = new db();
	$ch = checkEmp();
	if(!$ch['result']) exit;

	$data = array('empID' => $_REQUEST['empID'], 'year' => $ch['year']);
	$rate = getRate($data);
	if($_REQUEST['rate'] + $rate > 100) exit;

	include("../public/inc_doappend.php");

?>