<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
		/*echo "<pre>";
		print_r($_REQUEST);
		exit;*/
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";

	/*特殊處理vType Get時 無法解析+與-*/
	if(isset($_SERVER['QUERY_STRING'])) {
		$getList = explode('&', $_SERVER['QUERY_STRING']);
		foreach ($getList as $key => $value) {
			if (preg_match("/vType/i", $value)){
				$vType = explode('=', $value);
				$_REQUEST['vType'] = $vType[1];
				break;
			}
		}
	}

	
	$pageTitle = "經費預算";
	$tableName = "plan_sources";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 5;
	$NeedFilter = '';
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$classList = array(''=>'--預算類別--', '經常門'=>'經常門', '資本門'=>'資本門');
	$vTypeList = array(''=>'--收入/支出--', '+'=>'收入', '-'=>'支出');

	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"filterOption"=>array('autochange'=>true,'goBtn'=>array("確定",false,true,'')),
		"ckBoxAll" =>true,
		"filters"=>array( //過濾器
			'class'=>$classList,
			'vType'=>$vTypeList
		),
		/*"extraButton"=>array( //新增的button
			array("進階搜尋",false,true,'Button1Click'),
			array("Button2",false,true,'')
		)*/
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	$db = new db();		 
	$sql = "select * from budget_map";
	$rs = $db->query($sql);
	$accList = array(''=>'無');
	while($r=$db->fetch_array($rs)) $accList[$r['id']] = $r['id'].' '.$r['title'];
	unset($classList['']);
	unset($vTypeList['']);
	$fieldsList = array(
		"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
		"class"=>array("預算類別","100px",true,array('Text')),
		"subject"=>array("預算科目","150px",true,array('Select',$accList)),
		"title"=>array("名稱","200px",true,array('Text')),
		"amount"=>array("金額","100px",true,array('Text')),
		"vType"=>array("支出/收入","100px",true,array('Select', $vTypeList)),
		"notes"=>array("備註","",false,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$fieldA = array(

		"id"=>array("<span class='need'>*</span>編號","text","","","",array(true,'','','')),
		"class"=>array("<span class='need'>*</span>預算類別","select",1,"",$classList),
		"subject"=>array("<span class='need'>*</span>預算科目","select",1,"",$accList),
		"title"=>array("<span class='need'>*</span>名稱","text","","","",array(true,'','','')),
		"amount"=>array("金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"vType"=>array("<span class='need'>*</span>支出/收入","select",1,"",$vTypeList),
		"notes"=>array("備註","textbox","","","")
	);
	/*新增時的預設值*/
	$fieldA_Data = array();
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"class"=>array("<span class='need'>*</span>預算類別","select",1,"",$classList),
		"subject"=>array("<span class='need'>*</span>預算科目","select",1,"",$accList),
		"title"=>array("<span class='need'>*</span>名稱","text","","","",array(true,'','','')),
		"amount"=>array("金額","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"vType"=>array("<span class='need'>*</span>支出/收入","select",1,"",$vTypeList),
		"notes"=>array("備註","textbox","","","")
	);
?>