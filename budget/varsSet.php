<?php
    //header("Content-type: text/html; charset=utf-8");
    include "inc_vars.php";
    
    $fp=fopen("inc_vars.php","r");
    $c=-1;
    $data=array();
    $var = '';
    while (!feof($fp)){
        $str=trim(fgets($fp));
        if(strlen($str)==0) continue;//跳過空行
        if(substr($str,0,2)=="/*"){//group name
            $c++;
            $end=strpos($str,"*/");
            //echo strlen($str).','.$end."<br>";
            $groupName=substr($str,2,strlen($str)-2-2);
            if(!isset($data[$c]) || !is_array($data[$c]))   $data[$c]=array();
            $data[$c]['groupName']=$groupName;
            //echo $groupName.'<br>';
            continue;
        }
        if(substr($str,0,2)=="//"){//var name
            $varName=substr($str,2,strlen($str)-2);
            if(!isset($data[$c]['varName']) || !is_array($data[$c]['varName'])) $data[$c]['varName']=array();
            array_push($data[$c]['varName'],$varName);
            
            continue;
        }
        if(substr($str,0,1)=="$"){//處理陣列
            $index=strpos($str,"=");
            $ary=trim(substr($str,1,$index-1));
            $cnt=trim(substr($str,$index+1,strlen($str)-$index));
            $cnt=substr($cnt,6,strlen($cnt)-6-2);
            $a1=explode(",",$cnt);
            $cnt=array();
            
            $var.="var ".$ary."=[];\n";
            for($i=0;$i<count($a1);$i++){
                $s=explode("=>",$a1[$i]);
                
                //print_r($s);
                if(isset($s[0]) && isset($s[1])) {
                    $cnt[$s[0]]=$s[1];
                
                    $var.=$ary."[".$s[0]."]=".$s[1].";\n";
                }
                
            }
            //exit;
            ///print_r( $cnt)."<br>";
            $$ary=$cnt;
            if(!isset($data[$c]['aryName']) || !is_array($data[$c]['aryName'])) $data[$c]['aryName']=array();
            if(!isset($data[$c]['aryData']) || !is_array($data[$c]['aryData'])) $data[$c]['aryData']=array();
            //if(!is_array($data[$c]['aryDataJson']))   $data[$c]['aryDataJson']=array();
            array_push($data[$c]['aryName'],$ary);
            array_push($data[$c]['aryData'],$$ary);
            //echo $$ary."<br>";
            //print_r($cnt);
            //$$ary=json_encode($$ary);
            //array_push($data[$c]['aryDataJson'],$$ary);
        }
    }
    fclose($fp);
    //print_r($data);
    //$data=json_encode($data);
    
?>
<?php include("common/header.php");?>
<style type="text/css">


</style>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
<?php include("common/menu.php");?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE -->
        <div class="page-content">


            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            欄位資料對應表
                        </h3>
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">首頁</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="javascript:void(0)">欄位資料對應表</a></li>
                           
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                    
                        <div class="portlet box grey">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-globe"></i>欄位資料對應表清單</div>
                            </div>
                            <div class="portlet-body">
                                <?php
                                    $tb="";
                                    $cols=5;
                                    foreach($data as $k=>$v){
                                        $tb.="<table class='table table-striped table-bordered table-hover'>";
                                        $tb.="<tr><td colspan='".$cols."' class='title2'>".$v['groupName']."</td></tr>";
                                        $trC=0;
                                        //$tdC
                                        $varC=count($v["varName"]);
                                        foreach($v["varName"] as $k1=>$v1){
                                            if($trC%$cols==$cols){
                                                $tb.="<tr>";
                                            }
                                            if($trC==($varC-1)){
                                                $tb.="<td colspan='".abs($cols-$trC)."'>".$v1."&nbsp;&nbsp;<input type='button' onClick='openPnl(\"d_container\",\"Y\",".$v['aryName'][$k1].",\"".$v['aryName'][$k1]."\",\"".$v["varName"][$k1]."\")' class='sBtn' value='維護'></td>";
                                            }else{
                                                $tb.="<td class='keyWidth'>".$v1."&nbsp;&nbsp;<input type='button' onClick='openPnl(\"d_container\",\"Y\",".$v['aryName'][$k1].",\"".$v['aryName'][$k1]."\",\"".$v["varName"][$k1]."\")' class='sBtn' value='維護'></td>";
                                            }
                                            if($trC%$cols==$cols-1){
                                                $tb.="</tr>";
                                            }
                                            $trC++;
                                        }
                                        
                                        $tb.="</table><br>";
                                    }
                                    echo $tb;
                                ?>
                            </div>
                        </div> 

                    </div>
                </div>
                <!-- END PAGE HEADER-->

            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
<!--<div id='d_container' > 
    <div id='dTitle' class='divTitle divTitle2'></div>
    <div  class='divBorder2' >
    <div class='dTop'><input type="button" id='btnAdd' onClick="addItem();" class='sBtn' value="新增"></div>
    <div id='d_list' align="center"></div>
    <div class='dBottom'><input type="button" id='btnSave' class='sBtn' onClick="saveItem()" value="儲存">&nbsp;
    <input type="button" class='sBtn' id='btnCancel' onClick="openPnl('d_container','N')" value="取消"></div>
    </div>
</div>-->

<div class="modal fade hide" id="d_container" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
        <h4 class="modal-title" id="dTitle"></h4>
      </div>
      <div class="modal-body" id="d_list">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue" id="btnAdd" onclick="addItem();">新增</button>
        <button type="button" class="btn blue" id='btnSave' onClick="saveItem()" >儲存</button>
        <button type="button" class="btn" data-dismiss="modal" id='btnCancel' onClick="openPnl('d_container','N')" >取消</button>

      </div>
    </div>
  </div>
</div>

<?php include("common/footer.php");?>
<script type="text/javascript">
var ACT_TYPE='';//目前正處理哪一個類別

<?= $var ?>;
$(function() {
});
function openPnl(id,act,type,typeStr,title){
    /*if(act=="Y"){
        $('#'+id).show();
        $('#'+id).draggable();
    }else{
        $('#'+id).hide();
        return;
    }*/
    if(act=="Y") {
        $('#'+id).modal('show');
    } else {
        $('#'+id).modal('hide');
        return false;
    }
    ACT_TYPE=typeStr;
    var str="<table class='table table-striped table-bordered table-hover' id='tb_"+ACT_TYPE+"'>";
    str+="<tr align='center' class='tbTitle'><td>代碼</td><td >名稱</td><td style='text-align:right'>刪除</td></tr>";
    var i=0;
    
    for(var k in type){
        str+="<tr align='center'>";
        str+='<td ><input readonly class="m-wrap small" type="text" size="10" value="'+k+'" name="'+ACT_TYPE+'_key">';
        str+='<td ><input class="m-wrap small" type="text" size="10" value="'+type[k]+'" name="'+ACT_TYPE+'_val"></td>';
        str+='<td><img src="/budget/images/del.png" style="width:30px;cursor:pointer;float:right;" onClick="delItem(this,event)"></td>';
        str+="</tr>";
        i++;
    }
    title+="維護";
    $('#d_list').html(str);
    
    $('#dTitle').html(title);
}

function addItem(){//新增
    var tb=document.getElementById("tb_"+ACT_TYPE);
    var tbObj=tb.tBodies[0];
    var newRow=document.createElement('tr');
    var newcell1=document.createElement('td');
    var newcell2=document.createElement('td');
    var newcell3=document.createElement('td');
    newRow.style.height='30px';
    newRow.bgColor=''; 
    newcell1.align='center'; 
    newcell1.innerHTML="<input class='m-wrap small' type='text' size='10' name='"+ACT_TYPE+"_key'/>"; 
    newcell2.align='center'; 
    newcell2.innerHTML="<input class='m-wrap small' type='text' size='10' name='"+ACT_TYPE+"_val'/>";
    newcell3.align='center'; 
    newcell3.innerHTML='<img src="/budget/images/del.png" style="width:30px;cursor:pointer;float:right;" onClick="delItem(this,event)">'; 
    newRow.appendChild(newcell1);
    newRow.appendChild(newcell2);
    newRow.appendChild(newcell3);
    tbObj.appendChild(newRow);
    //document.all.tbContext.focus(); 
    var objDiv = document.getElementById("d_list");
    objDiv.scrollTop = objDiv.scrollHeight;
}
function delItem(obj,e){//刪除
    var evt = e || window.event;
    var current = evt.target || evt.srcElement;
    var currRowIndex=current.parentNode.parentNode.rowIndex; 
    var tb = document.getElementById("tb_"+ACT_TYPE);
    if (tb.rows.length > 0) {  tb.deleteRow (currRowIndex);}
} 
function saveItem(){
    var keyAry=new Array();
    var valAry=new Array();
    var objKey=document.getElementsByName(ACT_TYPE+"_key");
    var valKey=document.getElementsByName(ACT_TYPE+"_val");
    var bNull=false;
    for(var i=0;i<objKey.length;i++){
        if(objKey[i].value==""){    
            alert("代碼不能空白");    
            objKey[i].focus();  
            bNull=true; 
            break;  
        }
        if(valKey[i].value==""){    
            alert("名稱不能空白");    
            valKey[i].focus();  
            bNull=true; 
            break;  
        }
        if(jQuery.inArray(objKey[i].value,keyAry)>-1){
            alert("代碼不能重覆");    
            objKey[i].focus();  
            bNull=true; 
            break;  
        }
       /* if(jQuery.inArray(valKey[i].value,valAry)>-1){
            alert("名稱不能重覆");    
            valKey[i].focus();  
            bNull=true; 
            break;  
        }*/
        keyAry.push(objKey[i].value);
        valAry.push(valKey[i].value);
    }
    if(bNull){  return; }
    var keyStr=keyAry.join(",");
    var valStr=valAry.join(",");
    //return;
    $.ajax({ 
      type: "POST", 
      url: "varsSet_lib.php", 
      data: "act=saveItem&type="+ACT_TYPE+"&keyStr="+keyStr+"&valStr="+valStr
    }).done(function( rs ) {
        location.reload();

    });
}
</script>