var Search = function () {

    return {
        //main function to initiate the module
        init: function () {
            if (jQuery().datepicker) {
                $('.date-picker').datepicker();
            }
            if(jQuery().datetimepicker) {
            	$(".date-time-picker").datetimepicker({
		            format: "yyyy/mm/dd hh:ii"
		            //pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
		        });
            }
            $('.select2').select2({
                //placeholder: "Select an Option",
                allowClear: true
            });

            App.initFancybox();
        }

    };

}();