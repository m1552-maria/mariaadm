$(document).ready(function(){
    if($('.error').length > 0) {
        $('html, body').animate({
            scrollTop: $('.error').eq(0).offset().top-55
        }, 1000);
    }
    $('#btnArchive').click(function(){   
        $(this).attr('disabled','disabled');
        var isOk = 1;
        $('.error').removeClass('error');
        $('.help-block').remove();
        /*必填欄位*/
        $('.required').each(function(){
            var input = $(this).find('.controls').children().eq(0);  
            if(input.hasClass('date')) input = input.children().eq(0);
            if(input.hasClass('select2')) return;
            if(!input.hasClass('radio') && !input.hasClass('checkbox') && !input.hasClass('ckeditor')){
                if(input.val().trim() == '' || input.val() == null) {
                    $(this).addClass('error');
                    $(this).find('.controls').append('<span class="help-block">此欄位為必填</span>');
                    isOk = 0;
                }
            } else if(input.hasClass('ckeditor') && CKEDITOR.instances[input.attr('name')].getData() == '') {
                $(this).addClass('error');
                $(this).find('.controls').append('<span class="help-block">此欄位為必填</span>');
                isOk = 0;
            }
        });

        if(isOk == 0) {
            $('html, body').animate({
                scrollTop: $('.error').eq(0).offset().top-55
            }, 1000);
            $(this).removeAttr('disabled');
            return false;
        }

        if($('.multiPhoto').length > 0 && $('.multiPhoto').val() != '') {
            if(!ckFile()) {
                $('.multiPhoto').parent('.controls').parent('.control-group').addClass('error');
               // $('.multiPhoto').parent('.controls').append('<span class="help-block">只能選取圖檔：.jpg或.png</span>');
                $(this).removeAttr('disabled');
                return false;
            }
        }  

        $('#dataForm').submit();
    });

    //一般圖片預覽
    $('[data-img-input]').change(function(){
        var obj = this;
        var name = $(this).attr('data-img-input');
        var checkImg = inArray(this.files[0].type.toLowerCase(), allow_img_type);

        if(!checkImg['result']){  
            $('#messageBox .modal-body').html('只能選取圖檔：'+checkImg['text']);
            $('#messageBox').modal('show');
            $(this).val('');
            $('[data-img-preview="'+name+'"]').attr('src','');
            $('[data-img-del="'+name+'"]').css('display','none');
            return false;
        }


        var name = $(this).attr('data-img-input');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('[data-img-preview="'+name+'"]').attr('src', e.target.result);
                $('[data-img-del="'+name+'"]').show();
            }
            reader.readAsDataURL(this.files[0]);
        }

         if($(this).attr('data-size') != undefined && $(this).attr('data-size') != '') {
            var size = $(this).attr('data-size').split('*');
            var w = size[0];
            var h = size[1];
            $(obj.files).each(function(){
                var fReader = new FileReader();
                fReader.readAsDataURL(this);
                fReader.onloadend = function(event){
                    if($(obj).next('div').find('[data-img-preview]').prop("naturalWidth") != w || $(obj).next('div').find('[data-img-preview]').prop("naturalHeight") != h) {
                        alert("請上傳，寬高為 "+ w +'px * '+ h +'px圖片');
                        $(obj).next('div').find('[data-img-preview]').attr('src','');
                        $(obj).next('div').find('[data-img-del]').css('display', 'none');
                        $(obj).val('');
                    }
                }
            });

        }
    });
    //一般圖片刪除
    $('[data-img-del]').click(function(){
        var name = $(this).attr('data-img-del');
        $('[data-img-input="'+name+'"]').val('');
        $('[data-img-preview="'+name+'"]').attr('src','');
        $(this).css('display','none');
    });

    //上傳文件
    $('[data-file-input]').change(function(){
        var checkFile = inArray(this.files[0].type, allow_file_type);
        if(!checkFile['result']){  
            $('#messageBox .modal-body').html('上傳的文件格式不支援，請上傳其他類型檔案');
            $('#messageBox').modal('show');
            $(this).val('');
            return false;
        }
    });

    //checkbox 全選
    $('[data-checkbox-all]').click(function(){
        var key = $(this).attr('data-checkbox-all');
        if($(this).prop('checked')) {
            $('[name="'+key+'[]"]').each(function(){
                $(this).prop('checked', true);
                $(this).parent('span').addClass('checked');
            });
        } else {
            $('[name="'+key+'[]"]').each(function(){
                $(this).prop('checked', false);
                $(this).parent('span').removeClass('checked');
            });
        }
    });

    /*縣市*/
    $("select[name='county']").change(function(){
        var city = $('[name="city"]').val();    //鄉鎮市要緊接縣市
        var cd2 = this.options[this.selectedIndex].value;
        $.getJSON('/getptycd3.php',{'cd2':cd2},function(data){
            var str = '';
            $.each(data,function(idx,itm){ if(itm==city) str += '<option selected>'+itm+'</option>'; else str += '<option>'+itm+'</option>'; });
            $("select[name='city']").html(str);
        });
    });
    $("select[name='county']").change();

});

function inArray(text, list) {
    var result = [];
    result['result'] = false;
    result['text'] = [];
    for(var i in list) {
        if(list[i] == text) {
            result['result'] = true;
        }
        var type = list[i].split('/');
        result['text'].push('.'+type[1]);
    }
    result['text'] = result['text'].join(' , ');
    return result;
}
