$(document).ready(function(){
        tableNarrow();
        $(window).resize(function() {
            $('[data-table-change]').css('display','');
            tableNarrow();
        })

        //一般搜尋或進階搜尋 
        $('[data-search-button]').click(function(){
            var link = '/budget/'+ folderName + '/list.php?';
            var data = haveSortUrl;
            if($(this).attr('data-search-button') == 'advanced') $('[name="keyWordField"]').val('');
            else $('[data-search-div="advanced"] [name]').val('');

            $('.well [name]').each(function(){
                if($(this).val() !='') data += '&'+$(this).attr('name')+'='+$(this).val();
            });

            $('[data-search-filte]').each(function(){
                data += '&'+$(this).attr('name')+'='+$(this).val();
            });

            $.when(ajaxEncode(data)).then(function (d) {
                location.href = link + d;
            });

        });
        //一般搜尋或進階搜尋 Enter
        $('[data-search-div] input').keydown(function(e){
            var key;
            if(window.event) {
                key = window.event.keyCode;
                if(key == 13 ) {
                    var type = $(this).parents('[data-search-div]').attr('data-search-div');
                    var link = '/budget/'+ folderName + '/list.php?';
                    var data = haveSortUrl;
                    if(type == 'advanced') $('[name="keyWordField"]').val('');
                    else $('[data-search-div="advanced"] [name]').val('');

                    $('.well [name]').each(function(){
                        if($(this).val() !='') data += '&'+$(this).attr('name')+'='+$(this).val();
                    });

                    $('[data-search-filte]').each(function(){
                        data += '&'+$(this).attr('name')+'='+$(this).val();
                    });

                    $.when(ajaxEncode(data)).then(function (d) {
                        location.href = link + d;
                    });
                }
            } else {
                key = e.which;  
            }
            return (key != 13);
        });


        $('#checkboxID').click(function(){
            if($(this).prop('checked') == false) {
                $('[name="ID[]"]').parent('span').removeClass('checked');
                $('[name="ID[]"]').prop('checked', false);
            } else {
                $('[name="ID[]"]').parent('span').addClass('checked');
                $('[name="ID[]"]').prop('checked', true);
            }
        });

        $('#delete').click(function(){
            if($('[name="ID[]"]:checked').length == 0) {
                $('#messageBox .modal-body').html('請勾選須刪除的資料');
                $('#messageBox').modal('show');
            } else {
                if(confirm('確定要刪除勾選的資料嗎?')){
                    $('[name="action"]').val('delete');
                    $('#dataForm').submit();
                }
            }
        });

        //進階搜尋 一般搜尋 交換
        $('[data-search-change]').click(function(){
            var key = $(this).attr('data-search-change');
            $(this).css('display','none');
            if(key == 'advanced') $('[data-search-change="blurry"]').css('display','');
            else $('[data-search-change="advanced"]').css('display','');
            $('[data-search-div]').css('display','none');
            $('[data-search-div="'+key+'"]').css('display','');
        });


        //過濾搜尋
        $('[data-search-filte]').change(function(){
            var link = '/budget/'+ folderName + '/list.php?';
            var data = haveSortUrl;
    
            $('[data-search-filte]').each(function(){
                data += '&'+$(this).attr('name')+'='+$(this).val();
            });

            if($('[data-search-div="blurry"]').length > 0 && $('[data-search-div="blurry"]').css('display') != 'none') {
                $('[data-search-div="advanced"] [name]').val('');
            } else if($('[data-search-div="advanced"]').length > 0 && $('[data-search-div="advanced"]').css('display') != 'none') {
                $('[data-search-div="blurry"] [name]').val('');
            }

            $('.well [name]').each(function(){
                if($(this).val() !='') data += '&'+$(this).attr('name')+'='+$(this).val();
            });
            
            $.when(ajaxEncode(data)).then(function (d) {
                location.href = link + d;
            });
        });
          
    });

    function ajaxEncode(data) {
        var dfd = $.Deferred();
        $.ajax({
            url:'/urlGetProtectJs.php',
            type:'POST',
            data : {data:data},
            success :function(d) {
                dfd.resolve(d);
            }
        });
        return dfd.promise();
    }

    function tableNarrow() {
        $('#sample_1').wrap('<div style="overflow-x:auto;"></div>');
        $('.hidden-480').removeClass('hidden-480');
        /*if($('#sample_1').width() > $('#sample_1').parents('.box').width() && $('#sample_1 th a:visible:visible').length > 1) {
            $('#sample_1 th a:visible:last').parent('th').css('display','none').attr('data-table-change','');
            $('#sample_1 tbody tr').each(function() {
                $(this).find('.hidden-480:visible:last').css('display','none').attr('data-table-change','');;
            });
            tableNarrow();
        } else if($('#sample_1').width() > $('#sample_1').parents('.box').width() && $('#sample_1 th a:visible:visible').length == 1) {
            $('#sample_1').wrap('<div style="overflow-x:auto;"></div>');
        } else if($('#sample_1').width() <= $('#sample_1').parents('.box').width()) {
            return;
        }*/
    }
