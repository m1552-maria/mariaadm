<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	//include "../system/utilities/miclib.php";

	/*20171002 sean*/
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];

	if($act=='del') { //:: Delete record -----------------------------------------
		foreach($_POST['ID'] as $k=>$v) $aa[] = "'$v'";
		$lstss = implode(',',$aa);
		$aa =array_keys($fieldsList);	
		$db = new db();	
		//檢查是否要順便刪除檔案 與是否可以刪除檔案
		if(!empty($delFlag)) { 
			$sql = "select * from $tableName where $aa[0] in ($lstss)";
			$rs = $db->query($sql);
			while($r=$db->fetch_array($rs)) {
				$fn = $r[$delField];
				if($fn) {
					if(checkStrCode($fn,'utf-8')) $fn=iconv("utf-8","big5",$fn); 
					$fn = $ulpath.$fn;
					//echo $fn;
					if(file_exists($fn)) @unlink($fn);
				}
			}
		}

		$sql = "delete from $tableName where $aa[0] in ($lstss)";
		//echo $sql; exit;
		$db->query($sql);
		
	} elseif($act == 'import') {
		include 'importExcel.php';
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------

?>
<?php 
    header("Cache-control: private");

    $_SESSION['maintain']['name'] = (isset($_SESSION['Name'])) ? $_SESSION['Name'] : $_SESSION['empName'];

    $message = '';
    if(isset($_SESSION['maintain']['MESSAGE']) && gettype($_SESSION['maintain']['MESSAGE']) == 'string') {
        $message = $_SESSION['maintain']['MESSAGE'];
        unset($_SESSION['maintain']['MESSAGE']);
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />	
    <link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 

<script src="/budget/media/script/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
</head>
<body>
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo "<p>搜尋:".$opList['keyword']."</p>";
	
	//:PageControl ---------------------------------------------------------------------
 	if(empty($_REQUEST['openWindow'])) {
		$obj = new EditPageControl($opPage, $pinf);	
		echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div>";
	}

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	//echo $pinf->curPage;
	//echo $sql;
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);

	//:ViewControl Object ------------------------------------------------------------------------
	$formName = 'formPlanService';
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			if($status < 20) {
				$op3 = array(
					"templet"=>$root."/budget/".$tableName."/edit.html",
					"type"=>"edit",
					"form"=>array($formName,'doedit.php','post','multipart/form-data','form1Valid'));
			} else {
				$op3 = array(
					"templet"=>$root."/budget/".$tableName."/_edit.html",
					"type"=>"edit");
			}
			
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"templet"=>$root."/budget/".$tableName."/add.html",
				"type"=>"append",
				"form"=>array($formName,'doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			$listObj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	}
?>

<!-- Modal -->
        <div class="modal fade hide" id="importBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">服務收入EXCEL匯入</h4>
              </div>
              <div class="modal-body">
              	請選擇格式為 xls 的 excel 檔案，內容必須遵守範本的排法。
              	<br>
              	<a href="templet.xls" target="new">下載範本</a>
              	<form name="implodeForm" method="POST" enctype="multipart/form-data"  action="">
              		<input type="file" name="file1" size="30">
              	</form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn blue" onclick="importCheck()">確認</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
              </div>
            </div>
          </div>
        </div>

<?php include '../public/inc_listjs.php';?>
<script>
function doAppend() {
	page=<?=$pinf->pages?>;

	var url = "/budget/plan_service/list.php?openWindow=1&act=new&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
}

function doEdit() {
	var url = "/budget/plan_service/list.php?openWindow=1&act=edit&recno="+recno+"&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
}

function gorec(recno) { //go edit ViewControl
	var url = "/budget/plan_service/list.php?openWindow=1&act=edit&recno="+recno+"&sid=<?=$_GET['sid']?>&fid=<?=$_GET['fid']?>";
	window.parent.openItem(url);
	parent.tdChange(recno);
}

function form1Valid() {
	var data = [];
	var _data = {};
	var text = '新增';
<?php if($act == 'new') { ?>
		$('#newTable [name]').each(function(){
			var arr = [];
			var n = $(this).attr('name');
			n = n.split('[]');
			n = n[0];
			if(!data[n]) data[n] = [];
			data[n].push($(this).val());
		});

		for(var i in data) {
			if(!_data[i]) _data[i] = {};
			for(var j in data[i]) {
				_data[i][j] = data[i][j];
			}
		}

<?php } else { ?>
	text = '修改';
	$('#newTable [name]').each(function(){
		_data[$(this).attr('name')] = $(this).val();
	});
<?php } ?>
	$.ajax({
		type:'post',
		url:'<?php echo ($act == "new") ? "doappend.php" : "doedit.php" ;?>',
		data: _data,
		success: function(d){
			alert(text+'服務收入完成');
			window.opener.closeItem('plan_service');
<?php if($_SESSION['privilege'] > 10 || in_array($_SESSION['jobID'], $editJop)) { ?>
			window.opener.closeItem('plan_currency');
<?php } ?>
			window.opener.oItem = false;
			window.close(); 
		}
	});
	
	return false;
}

$(function(){
	$(document).on('change', '[name^="monthPay"]', function(){
		var k = $(this).parent('td').attr('data-month');
		monthPayCount(k);
	})
	
	$(document).on('change', '[name^="aidTrainPay"]', function(){
		var k = $(this).parent('td').attr('data-aid');
		monthPayCount(k);
	})

	$(document).on('change', '[name^="allTrafficPay"]', function(){
		var k = $(this).parents('[data-three-tr]').attr('data-three-tr');
		trafficPayCount(k);
	})
	
	$(document).on('change', '[name^="aidTrafficPay"]', function(){
		var k = $(this).parents('[data-three-tr]').attr('data-three-tr');
		trafficPayCount(k);
	})
})

function monthPayCount(key) {
	var total =  parseInt($('[data-month="'+key+'"]').children('input').val()) - parseInt($('[data-aid="'+key+'"]').children('input').val());
	$('[data-self="'+key+'"]').children('input').val(total);
}

function trafficPayCount(key) {
	var total = parseInt($('[data-three-tr="'+key+'"] [name^="allTrafficPay"]').val()) - parseInt($('[data-three-tr="'+key+'"] [name^="aidTrafficPay"]').val());
	total = (total > 0) ? total : 0;

	$('[data-three-tr="'+key+'"] [name^="selfTrafficPay"]').val(total);
}

function importExcel() {
	$('#importBox').modal('show');
}

function importCheck() {
	if($('#importBox [name="implodeForm"] [name="file1"]').val() == '') {
		alert('請選擇要匯入的檔案');
		return false;
	}

	var filePath = $('#importBox [name="implodeForm"] [name="file1"]').val().toLowerCase().split(".");
	var fileType =  filePath[filePath.length - 1];

	if(fileType != 'xls') {
		alert('檔案格試錯誤');
		return false;
	}

	$('[name="ListControlForm"] input[type="hidden"]').each(function(){
		$('#importBox [name="implodeForm"]').append(this);
	});

	$('#importBox [name="implodeForm"] [name="act"]').val('import');

	$('#importBox [name="implodeForm"]').submit();

}

function tdChange(recno) {
	$('.ListRow td').removeClass('columnCurrent');
	$('.ListRow:odd td').addClass('columnDataOdd');
	$('.ListRow:even td').addClass('columnDataEven');
	$('.ListRow').eq(recno-1).children('td').removeClass('columnDataOdd');
	$('.ListRow').eq(recno-1).children('td').removeClass('columnDataEven');
	$('.ListRow').eq(recno-1).children('td').addClass('columnCurrent');
}
</script>
</body>
</html>

