<?php

	if ($_FILES[file1][tmp_name] != "") {
		$fsrc = $ulpath.'import.xls';
		if (!copy($_FILES[file1][tmp_name],$fsrc)) { //上傳失敗
			echo '附件上傳作業失敗 !'; exit;
		}
	}		
	
	include($_SERVER['DOCUMENT_ROOT']."/maintain/Excel/reader.php");

	
	//建立excel檔的物件
	$data = new Spreadsheet_Excel_Reader();  
	//設定輸出編碼，指的是從excel讀取後再進行編碼
	$data->setOutputEncoding('UTF-8');  
	//載入要讀取的檔案
	$data->read($fsrc);  

	$heard = array(
		'序號',
		'學生姓名',
		'障礙等級',
		'月費全額',
		'補助教養費',
		'自付教養費',
		'交通費全額',
		'補助交通費',
		'自付交通費',
	);

	foreach ($data->sheets[0]['cells'][5] as $key => $value) {
		if($value != $heard[$key-1]) {
			echo "格式錯誤";
			exit;
		}
	}

	$i = 6;
	while ($i <= $data->sheets[0]['numRows']) {
		if(count($data->sheets[0]['cells'][$i]) != count($heard)) break;
		$d = $data->sheets[0]['cells'][$i];
		$grade = array_search( preg_replace('/\s(?=)/', '', $d[3]), $impGrade);
		$imDb = new db($tableName);
		$name = ($d[2] == '○○○') ? '' : $d[2];
		$imDb->row['fid'] = "'".$fID."'";
		$imDb->row['sid'] = "'".$sID."'";
		$imDb->row['name'] = "'".$name."'";
		$imDb->row['impGrade'] = "'".$grade."'";
		$imDb->row['monthPay'] = "'".$d[4]."'";
		$imDb->row['aidTrainPay'] = "'".$d[5]."'";
		$imDb->row['selfTrainPay'] = "'".$d[6]."'";
		$imDb->row['allTrafficPay'] = "'".$d[7]."'";
		$imDb->row['aidTrafficPay'] = "'".$d[8]."'";
		$imDb->row['selfTrafficPay'] = "'".$d[9]."'";
		$imDb->row['monthNum'] = "'12'";
		$imDb->insert();	
		$i++;
	}

?>