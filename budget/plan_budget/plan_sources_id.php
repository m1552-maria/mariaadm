<?php
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";
	$db = new db();
	$sql = "select * from plan_sources where 1";
	$rs = $db->query($sql);
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>代碼選擇</title>
	<style type="text/css">
  .whitebg {background-color:white; font:15px "微軟正黑體",Verdana;}
  .whitebg a:link { text-decoration:none }
  .whitebg a:visited { text-decoration: none;}
  .whitebg a:hover {	color: #FF0000;	text-decoration: none;}
  .whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	#browser li {
		list-style-image: url(../../images/man.png);
		list-style-position: outside;
		padding: 0 4px 0 4px; 
		cursor:pointer;
	}
  </style>
  <script type="text/javascript" src="/Scripts/jquery.js"></script>  
  <script type="text/javascript">
		
	
	  function setID(data,elm) {
		//opener.source.postMessage([id,title], opener.origin);
		//window.returnValue = [id,title]; 
		window.opener.returnPlanSources(data);
		window.opener.rzt = false;
		//opener.source.postMessage('OK');
		window.close(); 
			
	  }
		
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
	<tr>
   	<th id="tblHead" bgcolor="#CCCCCC">::: 經費來員表 :::</th>
  </tr>
	<tr><td bgcolor="#FFFFFF">
    <div id="markup" class="whitebg"><ul id="browser">
    <? 
    	while($r=$db->fetch_array($rs)) {
    		$data = array();
    		foreach ($r as $key => $value) {
    			if(gettype($key) != 'integer') $data[$key] = (!empty($value)) ? $value : '';
    		}
    		echo "<li><a onClick=setID(".json_encode($data).",this)>".$r['title']."(".$r['vType'].") - ".$r['class']."</a></li>";
    	}
    ?> 
    
    </ul></div>
	</td></tr>
</table>
</body>
</html>