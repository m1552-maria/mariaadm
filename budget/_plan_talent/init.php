<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	if(!isset($_REQUEST['fid'])) exit;
	$fID = $_REQUEST['fid'];
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include($_SERVER['DOCUMENT_ROOT']."/config.php");
	include($_SERVER['DOCUMENT_ROOT']."/getEmplyeeInfo.php");
	include "$root/system/db.php";

	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	
	$pageTitle = "人力編制";
	$tableName = "plan_talent";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 5;
	$NeedFilter = "fid = '".$fID."'";
	
	$db = new db();
	$sql = "select status from plan where id = '".$fID."'";
	$rs = $db->query($sql);
	$r=$db->fetch_array($rs);
	$status = (int)$r['status'];

	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,false,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => ($_SESSION['privilege'] > 10 || $status >= 20) ? array("＋新增",true,true,'') : array("＋新增",false,true,'doAppend'),
		"editBtn" => ($_SESSION['privilege'] > 10 || $status >= 20) ? array("－查看",false,true,'doEdit') : array("－修改",false,true,'doEdit'),
		"delBtn" => ($_SESSION['privilege'] > 10 || $status >= 20) ? array("Ｘ刪除",true,true,'') : array("Ｘ刪除",false,true,'doDel') 
	);
	
	//:ListControl  ---------------------------------------------------------------

	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"filterOption"=>array('autochange'=>true,'goBtn'=>array("確定",false,true,'')),
		"filters"=>array( //過濾器
		),
		/*"extraButton"=>array( //新增的button
			array("進階搜尋",false,true,'Button1Click'),
			array("Button2",false,true,'')
		)*/
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	
	$fieldsList = array(
		"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
		"fid"=>array("主檔編號","",true,array('Text')),
		"jobID"=>array("職稱代碼","",true,array('Text')),
		"jobtitle"=>array("職稱","",true,array('Text')),
		"qty"=>array("人數","",true,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 
	$fieldA = array(
		"fid"=>array("主檔編號","readonly",20,'','',''),
		"jobID"=>array("<span class='need'>*</span>職稱代碼","queryID","","","jobinfo",array(true,'','','')),
		"qty"=>array("人數","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"notes"=>array("備註","textbox","50","5","")
	);
	/*新增時的預設值*/
	$fieldA_Data = array("fid"=>$fID);
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"fid"=>array("主檔編號","readonly",20,'','',''),
		"jobID"=>array("<span class='need'>*</span>職稱代碼","queryID","","","jobinfo",array(true,'','','')),
		"qty"=>array("人數","text","","","",array(false,PTN_NUMBER,'只能填數字','')),
		"notes"=>array("備註","textbox","50","5","")
	);
?>