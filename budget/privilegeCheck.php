<?php
	if(!isset($_SESSION)) {
        ini_set('session.gc_maxlifetime', 7200);
        session_start();
        setcookie('PHPSESSID', session_id(), time() + 7200, '/');
    }
	if(empty($_SESSION['privilege'])) header("Location: /budget/error.html");
    elseif($_SESSION['privilege'] <= 10 && (empty($_SESSION['user_priviege']) || !in_array('Z', $_SESSION['user_priviege']))) header("Location: /budget/error.html");
    elseif($_SESSION['privilege'] > 10) {
        $str = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/maintain/priviege.txt');
        $priviege = json_decode($str,true);
        if(!in_array('Z',$priviege[$_SESSION["privilege"]])) header("Location: /budget/error.html");
    }

    $editJop = array();
    for($i=1; $i<=8; $i++) {
    	$key = 'G00'.$i;
    	$editJop[$key] = $key;
    }
    
?>