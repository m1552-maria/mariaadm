<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	include '../public/inc_list.php';
	include '../public/inc_listjs.php';
?>
<script type="text/javascript">
function gorec(recno) { //go edit ViewControl
	ListControlForm.recno.value = recno;
	if(window.event.target.tagName=='A') {
		if($('[onclick="gorec('+recno+')"]').parents('tr').find('td:last').html() == '否'){
			alert('此項目不能修改');
			return false;
		}
		ListControlForm.act.value = 'edit';
	}
	ListControlForm.submit();
}
function doEdit() {
	<?php if($pdata[$listPos]['bModify'] == 0) { ?>
		alert('此項目不能修改');
	<?php } else { ?>
		ListControlForm.recno.value = <?=$pinf->curRecord+1?>;
		ListControlForm.act.value = 'edit';
		ListControlForm.submit();	
	<?php } ?>
}
</script>

<?php include($root."/budget/common/footer.php");?>
