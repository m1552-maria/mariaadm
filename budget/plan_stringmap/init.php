<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	
	$pageTitle = "字串對應表";
	$tableName = "plan_stringmap";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 10;
	$NeedFilter = 'isReady=1';
	
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,true,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,true,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,true,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,true,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
		"newBtn" => array("＋新增",false,true,'doAppend'),
		"editBtn" => array("－修改",false,true,'doEdit'),
		"delBtn" => array("Ｘ刪除",false,true,'doDel')
	);
	
	//:ListControl  ---------------------------------------------------------------
	$classList = array(''=>'--選擇欄位名稱--', 'mtype'=>'服務類型');

	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'class',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,''),
			//'isReady'=>array(true,1) /*---- 必須過濾的欄位值---*/
		),
		"filters"=>array( //過濾器
			'class'=>$classList,
			//'isReady'=>array(1=>1)  /*---- 必須過濾的欄位值---*/
		),
		/*"extraButton"=>array( //新增的button
			array("進階搜尋",false,true,'Button1Click'),
			array("Button2",false,true,'')
		)*/
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	unset($classList['']);
	$fieldsList = array(
		"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
		"class"=>array("欄位名稱","",true,array('Select',$classList)),
		"title"=>array("字串名","",true,array('Text')),
		"bModify"=>array("可否異動","100px",true,array('Select', array(0=>'否','可')))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','pattern','title','maxlength')
	)*/ 
	$fieldA = array(
		"class"=>array("<span class='need'>*</span>欄位名稱", "select",1,"",$classList),
		"title"=>array("<span class='need'>*</span>字串名","text",'','','',array(true,'','',20))
	);
	/*新增時的預設值*/
	$fieldA_Data = array();
	

	$fieldE = array(
		"id"=>array("編號","id",20),
		"class"=>array("<span class='need'>*</span>欄位名稱","select",1,'',$classList),
		"title"=>array("<span class='need'>*</span>字串名","text",60,'','',array(true,'','',20))
	);
?>