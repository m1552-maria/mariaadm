<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	if(!isset($_REQUEST['pid'])) exit;
	$pID = $_REQUEST['pid'];
	error_reporting(E_ALL & ~E_NOTICE);;
	include($_SERVER['DOCUMENT_ROOT']."/budget/privilegeCheck.php"); //權限判斷
	include($_SERVER['DOCUMENT_ROOT']."/config_budget.php");
	include "$root/system/db.php";

	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	include "$root/budget/inc_vars.php";
	
	$pageTitle = "計劃項目";
	$tableName = "plan_items";
	// for Upload files and Delete Record use
	$ulpath = '../data/';
	$delField = '';
	$delFlag = false;
	$listSize = 1000;
	$NeedFilter = " pid = '".$pID."'";
	

	$db = new db();
	$sql = "select status, year from plan where id = '".$pID."'";
	$rs = $db->query($sql);
	$r=$db->fetch_array($rs);
	$status = (int)$r['status'];
	$year = (int)$r['year'] + 1911;
	//:PageControl ------------------------------------------------------------------
	/* array("顯示名稱","是否disabled","是否顯示","js function name")*/
	$opPage = array(
		"firstBtn"=>array("第一頁",false,false,'firstBtnClick'),
		"prevBtn"=>array("前一頁",false,false,'prevBtnClick'),
		"nextBtn"=>array("下一頁",false,false,'nextBtnClick'),
		"lastBtn"=>array("最末頁",false,false,'lastBtnClick'),
		"goBtn"=>array("跳頁",false,false,'goBtnClick'),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,'SearchKeyword'),
	);
	

	if($_SESSION['privilege'] > 10 || $status < 20) {
		//$opPage['newBtn'] = array("＋新增",true,true,'');
		$opPage['newBtn'] = array("＋新增",false,true,'doAppend');
		$opPage['editBtn'] = array("－修改",false,true,'doEdit');
		//$opPage['delBtn'] = array("Ｘ刪除",true,true,'');
		$opPage['delBtn'] = array("Ｘ刪除",false,true,'doDel');
	} elseif($status >= 20) {
		$opPage['newBtn'] = array("＋新增",true,true,'');
		$opPage['editBtn'] = array("－查看",false,true,'doEdit');
		$opPage['delBtn'] = array("Ｘ刪除",true,true,'');
	} 

	if(empty($pID)) {
		$opPage["searchBtn"] = array("◎搜尋",true,true,'SearchKeyword');
		$opPage['newBtn'] = array("＋新增",true,true,'');
		$opPage['editBtn'] = array("－修改",true,true,'');
		$opPage['delBtn'] = array("Ｘ刪除",true,true,'');
	}

	//:ListControl  ---------------------------------------------------------------

	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'sort',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('title'),	//要搜尋的欄位	
		"ckBoxAll" =>true,
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定",false,true,'')
		),
		"filters"=>array( //過濾器
		),
		"extraButton"=>array( //新增的button
		)
	);

	/*欄位型別 Id, Link, Date, DateTime, Image, Select, Define */
	/*array("顯示名稱","寬度","可否排序","array('欄位型別', '應用')")
	  應用=> Id:     function (%d) , ex: gorec(%d)
	  		 Link:   放連結前段文字, ex: 'http://'
	  		 Image:  寬度, ex: 20px;
	  		 Select: 陣列, ex: array('0'=>'下架', '1'=>'下架')
	  		 Define: function: 'testAlert'
	*/
	
	$fieldsList = array(
		"id"=>array("編號","50px",true,array('Id',"gorec(%d)")),
		"pid"=>array("主檔編號","60px",true,array('Text')),
		"sort"=>array("排序","20px",true,array('Text')),
		"title"=>array("名稱","",true,array('Text')),
		"bDate"=>array("開始日期","",true,array('Date')),
		"eDate" =>array("結束日期","",true,array('Date')),
		"srvpeople"=>array("服務量","",false,array('Text')),
		"srvunit"=>array("服務量單位","40px",false,array('Text')),
		"talents"=>array("人力需求量","30px",false,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*欄位型別 readonly, text, textbox, textarea, checkbox, date, datetime, select, file, id, hidden, queryID, Define */
	/*array("顯示名稱","欄位型別","size","rows", "select的陣列 or Define的function name", 
		array('是否必填','正規表示驗證','錯誤顯示文字','maxlength')
	)*/ 

	$sql = "select id, title from plan_stringmap where isReady = 1 and class= 'mtype'";
	$rs = $db->query($sql);
	$mtypeList = array();
	while($r=$db->fetch_array($rs)) $mtypeList[$r['id']] = $r['title'];
	$fieldA = array(
		"pid"=>array("主檔編號","readonly",20,'','',''),
		"title"=>array("<span class='need'>*</span>名稱","text","60","","",array(true,'','','')),
		"mtype"=>array("服務類型","select",1,"",$mtypeList),
		"sort"=>array("排序","text","","","",array(false,PTN_DECIMAL,'請輸入數字')),
		"bDate"=>array("開始日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"eDate"=>array("結束日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"dateNotes"=>array("日期備註","textbox","50","2",""),
		"srvpeople"=>array("服務量","text","","","",array(false,PTN_NUMBER,'請輸入數字')),
		"srvunit"=>array("服務量單位","select",1,"",$srvunitList),
		"srvpeopleNotes"=>array("服務量備註","textbox","50","2",""),
		"talents"=>array("人力需求量","text","","","",array(false,PTN_DECIMAL,'請輸入數字')),
		"isPlan2"=>array("計劃表二統計","checkbox","","",""),
		"isPlan4"=>array("統計個案量","checkbox","","",""),
		"goal"=>array("目標","textarea","100","5",""),
		"content"=>array("內容","textarea","100","10",""),
		"srveffect"=>array("績效/稽核/評鑑指標","textbox","100","5",""),
		"notes"=>array("備註","textbox","50","2","")
	);
	//新增時的預設值
	$fieldA_Data = array('pid'=>$pID, 'bDate'=>$year.'/01/01', 'eDate'=>$year.'/12/31', 'isPlan2'=>1,'sort'=>1);

	$fieldE = array(
		"id"=>array("編號","id",20),
		"pid"=>array("主檔編號","readonly",20,'','',''),
		"title"=>array("<span class='need'>*</span>名稱","text","60","","",array(true,'','','')),
		"mtype"=>array("服務類型","select",1,"",$mtypeList),
		"sort"=>array("排序","text","","","",array(false,PTN_DECIMAL,'請輸入數字')),
		"bDate"=>array("開始日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"eDate"=>array("結束日期","date","","","",array(false,PTN_DATE,'ex:2017/01/01','')),
		"dateNotes"=>array("日期備註","textbox","50","2",""),
		"srvpeople"=>array("服務量","text","","","",array(false,PTN_NUMBER,'請輸入數字')),
		"srvunit"=>array("服務量單位","select",1,"",$srvunitList),
		"srvpeopleNotes"=>array("服務量備註","textbox","50","2",""),
		"talents"=>array("人力需求量","text","","","",array(false,PTN_DECIMAL,'請輸入數字')),
		"isPlan2"=>array("計劃表二統計","checkbox","","",""),
		"isPlan4"=>array("統計個案量","checkbox","","",""),
		"goal"=>array("目標","textarea","100","5",""),
		"content"=>array("內容","textarea","100","10",""),
		"srveffect"=>array("績效/稽核/評鑑指標","textbox","100","5",""),
		"notes"=>array("備註","textbox","50","2","")
	);
?>