<?
	/*
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	//include "../system/utilities/miclib.php";

	//組長G007 以下不能看到人力編制
	/*20171002 sean*/
	$isFrameShow = 1;
	$act = (isset($_REQUEST['act'])) ? $_REQUEST['act'] : '';
	//if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	if($act == 'new' || $act == 'edit') $isFrameShow = 0;

	$tabidx = (isset($_SESSION['plan_tab2'])) ? $_SESSION['plan_tab2'] : 0;
	if($tabidx == 0 && $_SESSION['privilege'] <= 10 && !in_array('Q', $_SESSION['user_priviege'])) {
		if($_SESSION['jobID'] == 'G007') $tabidx = 1;
		elseif(!in_array($_SESSION['jobID'], $editJop)) $tabidx = 2;
	}

	/*
		0 => 總後台,
		1 => 人事(有薪資管理權限),
		2 => 副主任以上
		3 => 組長
		4 => 其他組員
	*/
	if($_SESSION['privilege'] > 10) $privilege = 0;
	elseif(in_array('Q', $_SESSION['user_priviege'])) $privilege = 1;
	elseif($_SESSION['jobID'] == 'G007') $privilege = 3;
	elseif(in_array($_SESSION['jobID'], $editJop)) $privilege = 2;
	else $privilege = 4;

	if($act=='del') { //:: Delete record -----------------------------------------
		foreach($_POST['ID'] as $k=>$v) $aa[] = "'$v'";
		$lstss = implode(',',$aa);
		$aa =array_keys($fieldsList);	
		$db = new db();	
		//檢查是否要順便刪除檔案 與是否可以刪除檔案
		if(!empty($delFlag)) { 
			$sql = "select * from $tableName where $aa[0] in ($lstss)";
			$rs = $db->query($sql);
			while($r=$db->fetch_array($rs)) {
				$fn = $r[$delField];
				if($fn) {
					if(checkStrCode($fn,'utf-8')) $fn=iconv("utf-8","big5",$fn); 
					$fn = $ulpath.$fn;
					//echo $fn;
					if(file_exists($fn)) @unlink($fn);
				}
			}
		}

		$sql = "delete from $tableName where $aa[0] in ($lstss)";
		$db->query($sql);

		$sql = "delete from plan_currency where sid in ($lstss)";
		$db->query($sql);
		$sql = "delete from plan_talent where sid in ($lstss)";
		$db->query($sql);
		$sql = "delete from plan_service where sid in ($lstss)";
		$db->query($sql);
		$sql = "delete from plan_education where sid in ($lstss)";
		$db->query($sql);
		$sql = "delete from plan_budget where sid in ($lstss)";
		$db->query($sql);
		$sql = "delete from plan_outlay where sid in ($lstss)";
		$db->query($sql);
		//echo $sql; exit;
		
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	/*20171012 sean edit*/
	$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1; 
	$recno = (isset($_REQUEST['recno']))?$_REQUEST['recno']:1;
		
	/*print_r('page:'.$page.'<br>'.$recno);
	exit;*/


	//:Defined PHP Function -------------------------------------------------------------

?>	

<?php 
    header("Cache-control: private");

    $_SESSION['maintain']['name'] = (isset($_SESSION['Name'])) ? $_SESSION['Name'] : $_SESSION['empName'];

    $message = '';
    if(isset($_SESSION['maintain']['MESSAGE']) && gettype($_SESSION['maintain']['MESSAGE']) == 'string') {
        $message = $_SESSION['maintain']['MESSAGE'];
        unset($_SESSION['maintain']['MESSAGE']);
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
<link href="/budget/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/budget/media/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="/Scripts/jquery-1.12.3.min.js"></script>
    <script src="/budget/media/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/budget/media/js/jquery-ui-1.10.1.custom.min.js"></script>    
    <script src="/budget/media/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="/budget/media/js/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/budget/media/js/jquery.uniform.min.js" type="text/javascript" ></script>  
    <script src="/budget/media/js/app.js" type="text/javascript"></script>


<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/system/TreeControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/budget/media/js/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/budget/media/js/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 

<script src="/budget/media/script/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
</head>
<body>
<div>
<div style="<?php if($isFrameShow == 1){ echo 'display:inline-block;width:54%;'; } ?>">
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if(!empty($NeedFilter)) $whereAry[] = $NeedFilter;
	if(isset($opList['filters'])) {
		foreach($opList['filters'] as $k=>$v) {
			if(isset($_REQUEST[$k]) && $_REQUEST[$k] != '') $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if(!empty($opList['filterOption'][$k]) && !isset($_REQUEST[$k])) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if(!empty($opList['keyword'])) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	$pinf = new PageInfo($rCount,$listSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	/*20171002 sean*/
	if(!empty($opList['keyword'])) echo "<p>搜尋:".$opList['keyword']."</p>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	/*echo $pinf->curPage;
	echo $sql;*/
	
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);

	//:ViewControl Object ------------------------------------------------------------------------
	$formName = 'formPlanCurrency';
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			if($_SESSION['privilege'] > 10 || $status < 20) {
				$op3 = array(
					"type"=>"edit",
					"form"=>array($formName,'doedit.php','post','multipart/form-data','form1Valid'),
					"submitBtn"=>array("確定修改",false,''),
					"cancilBtn"=>array("取消修改",false,'')	);

			} else {
				$op3 = array(
					"type"=>"edit",
					"form"=>array($formName,'doedit.php','post','multipart/form-data','form1Valid'),
					"submitBtn"=>array("確定修改",true,''),
					"resetBtn"=>array('重新輸入',true,''),
					"cancilBtn"=>array("返回列表",false,'')	);
			}
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"type"=>"append",
				"form"=>array($formName,'doappend.php','post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			$isFrameShow = 1;
			$listObj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);	
	}
	$planID = (isset($pdata[$listPos]['id'])) ? $pdata[$listPos]['id'] : 0;
?>
	
</div>
<?php if($isFrameShow == 1) { ?>
<div class="tabbable tabbable-custom" style="display:inline-block; width:45%;float:right;">
	<ul id="lastUl" class="nav nav-tabs">
	<?php if($privilege <= 2 || $_SESSION['empID'] == '00433') { ?>
		<li <?=($tabidx==0?"class='active'":'')?>><a href="#tab0" data-toggle="tab">人力編制<img id="tabg0" class="tabimg" src="/images/<?= $tabidx==0?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(0)"/></a></li>
	<?php 
		}
		//if($privilege <= 3) { 
	?>
		<li <?=($tabidx==1?"class='active'":'')?>><a href="#tab1" data-toggle="tab">經費預算<img id="tabg1" class="tabimg" src="/images/<?= $tabidx==1?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(1)"/></a></li>
	<?php// } ?>
		<li <?=($tabidx==2?"class='active'":'')?>><a href="#tab2" data-toggle="tab">服務收入<img id="tabg2" class="tabimg" src="/images/<?= $tabidx==2?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(2)"/></a></li>


		<li <?=($tabidx==3?"class='active'":'')?>><a href="#tab3" data-toggle="tab">教養補助<img id="tabg3" class="tabimg" src="/images/<?= $tabidx==3?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(3)"/></a></li>
		<li <?=($tabidx==4?"class='active'":'')?>><a href="#tab4" data-toggle="tab">經費來源<img id="tabg4" class="tabimg" src="/images/<?= $tabidx==4?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(4)"/></a></li>
		<li <?=($tabidx==5?"class='active'":'')?>><a href="#tab5" data-toggle="tab">資本支出<img id="tabg5" class="tabimg" src="/images/<?= $tabidx==5?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(5)"/></a></li>
	</ul>
	<div class="tab-content">
	<?php if($privilege <= 2 || $_SESSION['empID'] == '00433') { ?>	
		<div class="tab-pane<?=($tabidx==0?' active':'')?>" id="tab0">
	    	<iframe  id="plan_talent" name="plan_talent" width="100%" height="420" frameborder="0" src="../plan_talent/list.php?fid=<?=$_REQUEST['pid']?>&sid=<?=$planID?>"></iframe>
	    </div>
	<?php 
		}
		//if($privilege <= 3) { 
	?>
		<div class="tab-pane<?=($tabidx==1?' active':'')?>" id="tab1">
	    	<iframe id="plan_currency" name="plan_currency" width="100%" height="420" frameborder="0" src="../plan_currency/list.php?fid=<?=$_REQUEST['pid']?>&sid=<?=$planID?>"></iframe>
	    </div>
	<?php //} ?>
	    <div class="tab-pane<?=($tabidx==2?' active':'')?>" id="tab2">
	    	<iframe id="plan_service" name="plan_service" width="100%" height="420" frameborder="0" src="../plan_service/list.php?fid=<?=$_REQUEST['pid']?>&sid=<?=$planID?>"></iframe>
	    </div>
	    <div class="tab-pane<?=($tabidx==3?' active':'')?>" id="tab3">
	    	<iframe id="plan_education" name="plan_education" width="100%" height="420" frameborder="0" src="../plan_education/list.php?fid=<?=$_REQUEST['pid']?>&sid=<?=$planID?>"></iframe>
	    </div>
	    <div class="tab-pane<?=($tabidx==4?' active':'')?>" id="tab4">
	    	<iframe id="plan_budget" name="plan_budget" width="100%" height="420" frameborder="0" src="../plan_budget/list.php?fid=<?=$_REQUEST['pid']?>&sid=<?=$planID?>"></iframe>
	    </div>
	    <div class="tab-pane<?=($tabidx==5?' active':'')?>" id="tab5">
	    	<iframe id="plan_outlay" name="plan_outlay" width="100%" height="420" frameborder="0" src="../plan_outlay/list.php?fid=<?=$_REQUEST['pid']?>&sid=<?=$planID?>"></iframe>
	    </div>
	</div>
</div>
<?php } ?>
</div>
<?php include '../public/inc_listjs.php';?>
<script type="text/javascript">
	var tabidx = <?php echo $tabidx;?>;
	$(function(){
		var liLength = $('#lastUl li').length;
		/*顯示頁籤的程式 大於3個要插入製換標籤 & 顯示(隱藏)某些頁籤*/
		if(liLength > 3) {
			var changeLi = Math.ceil(liLength/2);
			var cIndex = changeLi-1;
			var active = 1;
			$('#lastUl li').each(function(){
				if($(this).hasClass('active')) {
					active = $(this).index()+1;
					return false;
				}
			});

			var cHtml = ['<li><a id="changeLi" href="javascript:void(0)"><i class="'];
			if(active <= changeLi) {
				cHtml.push('icon-forward');
				$('#lastUl li:gt('+cIndex+')').css('display','none');
			} else { 
				cHtml.push('icon-backward');
				$('#lastUl li:lt('+changeLi+')').css('display','none');
			}
			cHtml.push('"></i></a></li>');
			$('#lastUl li:eq('+cIndex+')').after(cHtml.join(''));

			$(document).on('click','#changeLi', function(){
				if($(this).children('i').hasClass('icon-forward')) {
					$(this).html('<i class="icon-backward"></i>');
					$('#lastUl li').css('display','');
					$('#lastUl li:lt('+changeLi+')').css('display','none');
				} else {
					$(this).html('<i class="icon-forward"></i>');
					$('#lastUl li').css('display','');
					$('#lastUl li:gt('+changeLi+')').css('display','none');
				}
			});
		}


		
	});
	function setnail(n) {
		$('.tabimg').attr('src','/images/unnail.png');
		$('#tabg'+n).attr('src','/images/nail.png');
		$.get('/budget/plan/setnail.php',{'idx':n, 'type':'2'});
	}

	var oItem = false;
	function openItem(url) {
		if(oItem){
			oItem.close();
			oItem = false;
		} 
		oItem = window.open(url,'','width=1000,height=800');

	}

	function closeItem(name) {
		window.frames[name].location.reload();
	}
</script>
</body>
</html>
