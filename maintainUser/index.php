<?php
	@session_start();
	if(!$_SESSION[empID]) header("Location: /login.php");
	include '../inc_sys.php';
	$incfn = $_REQUEST[incfn];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="Keywords" content="網站關鍵字" />
  <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
  <META HTTP-EQUIV="EXPIRES" CONTENT="0">
  <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<title>系統管理</title>
	<link href="/css/index.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="/Scripts/jquery.js"></script>
  <? include 'style.php' ?>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="sideBar">&nbsp;</td>
    <td width="1000" class="main" >
    	<!-- 頁面本體 ↓-->
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td class="headBar"><? include('../inc_head.php')?></td></tr>
        <tr><td class="menu"><? include('../inc_menu2.php') ?></td></tr>        
        <tr><td class="main" align="center" bgcolor="#FFFFFF"><iframe width="100%" height="630" src="main.htm" frameborder="0" scrolling="no"></iframe></td></tr>
        <tr><td class="foot" align="center"><? include('../inc_foot.php')?></td></tr>
      </table>
      <!-- 頁面本體 ↑-->
    </td>  
    <td class="sideBar">&nbsp;</td>
  </tr>
</table>
</body>
</html>
