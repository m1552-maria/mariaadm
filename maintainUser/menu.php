<?php 
	@session_start();
	include_once('../miclib.php');	
	//::取的權限設定
	$priviege = $_SESSION['user_priviege'];
	$classdef = $_SESSION['user_classdef'];
		
	//::帶参數傳入(整頁方式)
  $cpPage = "http://".$_SERVER['SERVER_NAME'];  		//首頁 網址
  $cpRoot = str_replace("\\","/",realpath('../'));  //檔案 真實路徑		
	//::直接開(編輯按鈕)的圖檔路徑
	$_SESSION[heroot] = $cpRoot.'/data/sys/';
	$_SESSION[heurl]  = $cpPage.'/data/sys/';	

	// Type, Link Path, Target, bullet picture
	$Menu['基本資料維護'] = array();
	$Menu['基本資料維護']['部門 資料查詢'] =	array(0,'../maintain/department/list.php','main','');
	if(in_array('A',$priviege)) {
		if(in_array('A2',$priviege)) $Menu['基本資料維護']['職銜 資料查詢'] =	array(0,'../maintain/jobs/list.php','main','');
		if(in_array('A3',$priviege)) {
			if($_SESSION["canEditEmp"])	$Menu['基本資料維護']['員工 資料維護'] =	array(0,'../maintain/emplyee/index.php','main','');
			else $Menu['基本資料維護']['員工 資料查詢'] =	array(0,'../maintain/emplyee/index.php','main','');
		}
		if(in_array('A4',$priviege)) $Menu['基本資料維護']['欄位資料對應表'] =	array(0,'../maintain/varsSet.php','main','');
		if(in_array('A5',$priviege)) $Menu['基本資料維護']['年資/特休'] =	array(0,'../maintain/emplyee_annual/list.php','main','');
		if(in_array('A6',$priviege)) $Menu['基本資料維護']['特休信件'] =	array(0,'../maintain/schedule_send_mail/list.php','main','');
	}
	if(in_array('B',$priviege)) {
		$Menu['制度規章維護'] = array();
		if(in_array('B1',$priviege)) $Menu['制度規章維護']['制度規章 分類表'] =	array(0,'../maintain/docclasses/list.php','main','');
		if(in_array('B2',$priviege)) $Menu['制度規章維護']['發佈 制度規章'] =	array(0,'../maintain/documents/list.php','main','');
	}
	
	if( in_array('D',$priviege) ) {
		$Menu['後台帳號'] = array();
		if(in_array('D1',$priviege)) $Menu['後台帳號']['後台帳號維護'] = array(0,'../maintain/admins/list.php','main','');
		if(in_array('D2',$priviege)) $Menu['後台帳號']['等級功能設定'] = array(0,'../maintain/priviege.php','main','');
	}		
	
	if( in_array('F',$priviege) ) $Menu['最新消息'] = array(0,'../maintain/bullets/list.php','main','');
	$Menu['保險系統'] = array(0,'../maintain/insurances/index.php','main','');
	if( in_array('K',$priviege) )	$Menu['請假系統'] = array(0,'../maintain/absence/index.php','main','');
	if( in_array('K1',$priviege) ) $Menu['綜合申請單'] = array(0,'../maintain/integratform/index.php','main','');
	if( in_array('K2',$priviege) ) $Menu['外出單'] = array(0,'../maintain/outoffice/index.php','main','');
	if( in_array('K',$priviege) ) $Menu['加班單'] = array(0,'../maintain/overtime/index.php','main','');
	
	
	$Menu['資訊查閱'] = array(0,'../information/?incfn=main.html','_top','');
	if($_SESSION["jobID"]=='G001' || $_SESSION["jobID"]=='G002') $Menu['執行長專區'] = array(0,'../information/index2.php?incfn=main.html','_top','');
	$Menu['文件管理'] = array();
	if( in_array('C',$priviege) ) {
		$Menu['文件管理']['知識平台維護'] = array();
		if(in_array('C1',$priviege)) $Menu['文件管理']['知識平台維護']['知識平台 分類表'] =	array(0,'../maintain/kwgclasses/list.php','main','');
		if(in_array('C2',$priviege)) $Menu['文件管理']['知識平台維護']['發佈 知識平台'] =	array(0,'../maintain/knowledges/list.php','main','');
	}
	if( in_array('L',$priviege) ) {
		$Menu['文件管理']['資訊查閱維護'] = array();
		if(in_array('L1',$priviege)) $Menu['文件管理']['資訊查閱維護']['資訊查閱 分類表'] =	array(0,'../maintain/infoclasses/list.php','main','');
		if(in_array('L2',$priviege)) $Menu['文件管理']['資訊查閱維護']['發佈 資訊查閱'] =	array(0,'../maintain/information/list.php','main','');
	}
	if( in_array('M',$priviege) ) {
		$Menu['文件管理']['資源管理維護'] = array();
		if(in_array('M1',$priviege)) $Menu['文件管理']['資源管理維護']['資源管理 分類表'] = array(0,"../maintain/resource_class/list.php",'main','');
		if(in_array('M2',$priviege)) $Menu['文件管理']['資源管理維護']['發佈 資源內容'] = array(0,"../maintain/resources/index.php",'main','');	
	}
	$Menu['文件管理']['資源管理維護']['資源管理查閱'] = array(0,'../maintain/map.php','main','');
	if( in_array('N',$priviege) ) {
		$Menu['文件管理']['影像事件'] = array();
		if(in_array('N1',$priviege)) $Menu['文件管理']['影像事件']['影像事件分類表'] = array(0,"../maintain/event_class/list.php",'main','');
		if(in_array('N2',$priviege)) {
			$Menu['文件管理']['影像事件']['發佈影像事件'] = array(0,"../maintain/event/index.php",'main','');	
			$Menu['文件管理']['影像事件']['影像事件瀏覽'] = array(0,'../maintain/event/browser.php','main','');	
		}
	}
	
	
	if( in_array('O',$priviege) ) {	
		$Menu['員工排班作業'] = array();
		$Menu['員工排班作業']['輪班班表'] = array(0,"../maintain/duty/list.php?uType=2",'main','');
		$Menu['員工排班作業']['週班表'] = array(0,"../maintain/duty/index.php?uType=0",'main','');
		$Menu['員工排班作業']['特殊班表']= array(0,"../maintain/duty/index.php?uType=1",'main','');
		$Menu['員工排班作業']['時段設定'] = array(0,"../maintain/duty_period/list.php",'main','');
		$Menu['員工排班作業']['代換班紀錄'] = array(0,"../maintain/emplyee_duty_change/list.php",'main','');
		$Menu['員工排班作業']['班表維護作業'] =	array(0,'../maintain/emplyee/index2.php','main','');
	}
	
	if( in_array('P',$priviege) ) {	
		$Menu['雜項費用'] = array();
		$Menu['雜項費用']['雜項費用 設定作業'] = array(0,"../maintain/otherfee/list.php",'main','');
		$Menu['雜項費用']['雜項費用 清冊作業'] = array(0,"../maintain/emplyee_otherfee/list.php",'main','');
	}
	
	if( in_array('Q',$priviege) ) {	
		// $Menu['薪資作業'] = array();
		// $Menu['薪資作業']['津貼 設定作業'] = array(0,"../maintain/allowance/list.php",'main','');
		// $Menu['薪資作業']['津貼 清冊作業'] = array(0,"../maintain/emplyee_allowance/list.php",'main','');
		// $Menu['薪資作業']['出缺勤異常作業'] = array(0,"../maintain/duty/abnormal.php",'main','');
		// $Menu['薪資作業']['薪資試算'] = array(0,"../maintain/emplyee_salary_report/index.php",'main','');

		$Menu['薪資作業'] = array();
		$Menu['薪資作業']['津貼'] = array();
		$Menu['薪資作業']['津貼']['設定作業'] = array(0,"../maintain/allowance/list.php",'main','');
		$Menu['薪資作業']['津貼']['清冊作業'] = array(0,"../maintain/emplyee_allowance/list.php",'main','');
		$Menu['薪資作業']['年終獎金'] = array();
		$Menu['薪資作業']['年終獎金']['設定作業'] = array(0,"../maintain/bonus_festival/list.php?type=year_end",'main','');
		$Menu['薪資作業']['年終獎金']['清冊作業'] = array(0,"../maintain/emplyee_bonus_year_end/list.php",'main','');
		
		$Menu['薪資作業']['三節獎金'] = array();
		// $Menu['薪資作業']['三節獎金']['設定作業'] = array(0,"../maintain/bonus_festival/list.php?type=festival",'main','');
		$Menu['薪資作業']['三節獎金']['清冊作業'] = array(0,"../maintain/emplyee_bonus_festival/list.php?type=festival",'main','');
		$Menu['薪資作業']['其他獎金'] = array();
		$Menu['薪資作業']['其他獎金']['設定作業'] = array(0,"../maintain/bonus/list.php",'main','');
		$Menu['薪資作業']['其他獎金']['清冊作業'] = array(0,"../maintain/emplyee_bonus/list.php",'main','');
		$Menu['薪資作業']['未補休加班單'] = array(0,"../maintain/emplyee_salary_report/overdue.php",'main','');	
    $Menu['薪資作業']['異常補修單'] = array(0,"../maintain/emplyee_salary_report/fixAbsent.php",'main','');	
		
		
		$Menu['薪資作業']['薪資']['試算'] = array(0,"../maintain/emplyee_salary_report/index.php",'main','');
		$Menu['薪資作業']['薪資']['加班費'] = array(0,"../maintain/emplyee_salary_items/list.php?type=overtime",'main','');
		$Menu['薪資作業']['薪資']['出缺勤'] = array(0,"../maintain/emplyee_salary_items/list.php?type=absence",'main','');
		$Menu['薪資作業']['薪資']['勞健退'] = array(0,"../maintain/emplyee_salary_insurence/list.php",'main','');
		$Menu['薪資作業']['薪資']['離職專區'] = array(0,"../maintain/emplyee_salary_report/index.php?fieldname=1",'main','');

	}


	if( in_array('R',$priviege) ) {
		$Menu['打卡系統'] = array();
		if(in_array('R1',$priviege)) $Menu['打卡系統']['打卡資料'] = array(0,"../maintain/emplyee_dutylog/list.php",'main','');
		if(in_array('R4',$priviege)) $Menu['打卡系統']['出勤資料'] = array(0,"../maintain/emplyee_duty_list/list.php",'main','');
		if(in_array('R2',$priviege)) $Menu['打卡系統']['員工識別證清單'] = array(0,"../maintain/emplyee_card_log/list.php",'main','');
		if(in_array('R3',$priviege)) $Menu['打卡系統']['匯入打卡資料'] = array(0,"../maintain/punch/api.php",'main','');
		$Menu['打卡系統']['出缺勤異常作業'] = array(0,"../maintain/duty/abnormal.php",'main','');
		$Menu['打卡系統']['月餅打卡檢核表'] = array(0,"../maintain/emplyee_dutylog/list_cake.php",'main','');
		$Menu['打卡系統']['月餅打卡檢核表<br/>(浩棟加速版)'] = array(0,"../maintain/emplyee_dutylog/list_cake-haotung.php",'main','');
	}

	if( in_array('S',$priviege) ) {	
		$Menu['出納作業'] = array();
		$Menu['出納作業']['人員基本資料'] = array(0,"../maintain/emplyee_cashier/index.php",'main','');
		$Menu['出納作業']['銀行媒體轉帳'] = array();
		//$Menu['出納作業']['銀行媒體轉帳']['設定作業'] = array(0,"../maintain/transfer/list.php",'main','');
		$Menu['出納作業']['銀行媒體轉帳']['清冊作業'] = array(0,"../maintain/emplyee_transfer/list.php",'main','');
	}


	$ecStr = json_encode($_SESSION);
	$ecStrF = mic_crypt($_SESSION['empID'],$ecStr,'e');	
	if( in_array('Z',$priviege) ) $Menu['預算管理系統'] = array(0,'../budget/index.php','_blank','');
    
    //add by Tina 20190304 定義各環境之公文系統網址
    $edisUrl='';
	if($_SERVER['HTTP_HOST']==='mariaadm.cloudcode.com.tw'){
        $edisUrl ='http://edis.cloudcode.com.tw/login.php?act='.$_SESSION[empID].'&cryp='.$ecStrF;
	}else if($_SERVER['HTTP_HOST']==='mariaadm'){
        $edisUrl ='http://edis/login.php?act='.$_SESSION[empID].'&cryp='.$ecStrF;
	}else if($_SERVER['HTTP_HOST']==='adm.maria.org.tw'){
        $edisUrl ='http://edis.maria.org.tw/login.php?act='.$_SESSION[empID].'&cryp='.$ecStrF;
	}

	$Menu['公文系統'] = array(0,$edisUrl,'_blank','');
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>主選單</title>
<script type="text/javascript">
var ahref;
function swtichDiv(ly,img) {
	ly.style.display = (ly.style.display=='none'?'':'none');
	img.src = (ly.style.display=='none'?'images/fold.gif':'images/open.gif');
}
function setfcn(pt) { top.topfrm.setfuncname('<< '+pt+' >>'); }

</script>
<style type="text/css">
<!--
a:link { color: #224B59; text-decoration:none }
a:visited {	color: #224B59;	text-decoration: none;}
a:hover {	color: #FF0000;	text-decoration: none;}
a:active { color: #FFFF00; text-decoration: none;	font-weight: bold;}
.itemTxt { font-family: "微軟正黑體", Verdana; }
.itemTxt2 {	font-size: 13px; }
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>

<body leftmargin="0" topmargin="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f8f8f8">
  <tr>
    <td width="4" class="top"></td>
    <td valign="top" class="map-c">
			<table width="100%" border="0">
			<? $seq =1;?>
			<? foreach($Menu as $k => $v) 
				if ($v[0]===0) { //項目
			   	 $img = $v[3]=='' ? 'table.gif' : $v[3]; 
				   echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt'><a href='$v[1]'";
					 if($v[2]>'') echo "target='$v[2]'";
					 echo ">$k</a></td></tr>";
				} else {	//目錄
					 $lyName = "ly$seq";
					 $imName = "im$seq";
					 $img = 'fold.gif'; 
				   echo "<tr><td width='16'><img name='im$seq' src='images/$img' onclick='swtichDiv($lyName,this)' style='cursor:hand'></td>";
					 echo "<td class='itemTxt'><a href='javascript:swtichDiv($lyName,$imName)'>$k</a></td></tr>";
 					 $seq++;
					 //目錄清單
					 echo "<tr><td width='16'></td><td>";
					 echo "<div id='$lyName' style='display:none'><table width='100%'>";
					 foreach($v as $k2 => $v2)  {
					 	if ($v2[0]===0) {//項目
					 		$img = $v2[3]=='' ? 'table.gif' : $v2[3];
							echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt2'><a href='$v2[1]'";
					 		if($v2[2]>'') echo "target='$v2[2]'";
					 		echo ">$k2</a></td></tr>";
					 	}else{//目錄
					 		$lyName = "ly2$seq";
							 $imName = "im$seq";
							 $img = 'fold.gif'; 
						   echo "<tr><td width='16'><img name='im$seq' src='images/$img' onclick='swtichDiv($lyName,this)' style='cursor:hand'></td>";
							 echo "<td class='itemTxt'><a href='javascript:swtichDiv($lyName,$imName)'>$k2</a></td></tr>";
		 					 $seq++;
							 //目錄清單
							 echo "<tr><td width='16'></td><td>";
							 echo "<div id='$lyName' style='display:none'><table width='100%'>";
							 foreach($v2 as $k3 => $v3)  {
							 	if ($v3[0]===0) {
							 		$img = $v3[3]=='' ? 'table.gif' : $v3[3];
									echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt2'><a href='$v3[1]'";
							 		if($v3[2]>'') echo "target='$v3[2]'";
							 		echo ">$k3</a></td></tr>";
							 	}else{
							 		
							 	}
							 }
							 echo "</table></div>";
							 echo "</td></tr>";
					 	}
					 }
					 echo "</table></div>";
					 echo "</td></tr>";
				} 
			?>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
