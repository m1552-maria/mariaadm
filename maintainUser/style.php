<style>
/*  主選單 */
.menuBar { background-color:#5C0A10;	font: 19px '微軟正黑體',Verdana;	color:#FFFFE0 }
#mainmenu a { text-decoration:none }
#mainmenu a:link { color:#FFFFE0 }
#mainmenu a:visited { color:#FFFFE0 }
#mainmenu a:hover { color:#FFFFE0 }
#mainmenu a:active { color:#FFFFE0 }
.foot {	font:12px "微軟正黑體",Verdana;	color: #CCC; background-color: #7B0C13;	padding: 5px; }

/*  次選單 */
.subpageTitle {
	font-family: "微軟正黑體", Verdana;
	font-size: 20px;
	font-weight: bold;
	background-color:#308F01;
	color:#FFFFFF;
	padding: 4px 0 4px 0;
}
.subpageTitle a { text-decoration:none }
.subpageTitle a:link { color:#FFF }
.subpageTitle a:visited { color:#FFF }
.subpageTitle a:hover { color:#FF0 }
.subpageTitle a:active { color:#FFF }

.mainDiv a {text-decoration:none }
.BannerDiv {
	font:bold 32px "微軟正黑體";
	background-color:#88FF6A;
	color:#0D4200;
	padding: 10px;
}

.mainList { 
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px; 
	border-radius: 20px;
	border:dashed 1px #308F01;
}
.mainDiv {
	 padding: 0px 12px 16px 12px;
	 font:13px "微軟正黑體"; 
}
.maintable_Head {
	background-color:#E1FFDD;
}
</style>
<script type="text/javascript" src="../Scripts/jquery.corner.js"></script>
<script>$(function(){ $('.BannerDiv').corner("sc:#308F01 8px");});</script>