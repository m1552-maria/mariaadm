<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once '../../config_psi.php';
	include_once '../../maintain/inc_vars.php';
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php"; 
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "帳務資料";
	$pageSize=3; //使用預設
	$tableName = "psi_invoice";
	// for Upload files and Delete Record use
	$ulpath = "/data/";
	$delField = '';
	$delFlag = false;
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,false,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);
	
	//:ListControl  ---------------------------------------------------------------
	if($_REQUEST['fid']) $opAR=array($_REQUEST['fid']=>$_REQUEST['fid']); else $opAR=array();
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','title'),	//要搜尋的欄位	
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定GO",false,true,''),
			'fid'=>array($_REQUEST['fid'],true,true)		//filter預設值,是否隱藏
		),
		"filters"=>array('fid'=>$opAR)
	);
		
	$fieldsList = array(
		"id"=>array("編號","60px",true,array('Id',"gorec(%d)")),
		"payterm"=>array("付款方式","100px",true,array('Define',getPayterm)),
		"conditions"=>array("付款條件","100px",true,array('Define',getConditions)),
		"mType"=>array("貨幣類別","100px",true,array('Define',getMType)),
		"invType"=>array("發票類別","100px",true,array('Define',getInvType)),
		"invRate"=>array("發票稅率","100px",true,array('Text','%')),
		"invCount"=>array("發票稅別","100px",true,array('Define',getInvCount)),
		"invNo"=>array("統一編號","100px",true,array('Text')),
		"invTitle"=>array("統編抬頭","100px",true,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"fid"=>array("機構編號","readonly",20,'',),
		"payterm"=>array("付款方式","select",1,'',$payterm),
		"conditions"=>array("付款條件","select",1,'',$conditions),
		"mType"=>array("貨幣類別","select",1,'',$moneyType),
		"invType"=>array("發票類別","select",1,'',$invType),
		"invRate"=>array("發票稅率","text",10,'',' %',array(true,PTN_DECIMAL,'',60)),
		"invCount"=>array("發票稅別","select",1,'',$invCount),
		"invNo"=>array("統一編號","text",8,'','',array(false,PTN_NUMBER,'',60)),
		"invTitle"=>array("統編抬頭","text",60,'','')
	);

	$fieldA_Data = array("fid"=>$_REQUEST['fid'],"invRate"=>'5',"invCount"=>'S');
	
	$fieldE = array(
		"id"=>array("編號","id",20,),
		"payterm"=>array("付款方式","select",1,'',$payterm),
		"conditions"=>array("付款條件","select",1,'',$conditions),
		"mType"=>array("貨幣類別","select",1,'',$moneyType),
		"invType"=>array("發票類別","select",1,'',$invType),
		"invRate"=>array("發票稅率","text",10,'',' %',array(false,PTN_DECIMAL,'',60)),
		"invCount"=>array("發票稅別","select",1,'',$invCount),
		"invNo"=>array("統一編號","text",8,'','',array(false,PTN_NUMBER,'',60)),
		"invTitle"=>array("統編抬頭","text",60,'','')
	);
?>