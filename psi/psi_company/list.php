<?
	/* =======		TAB Page Mode		=======
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	if($act=='new' || $act=='edit') $_SESSION['psi_tab']=0;
	$tabidx = $_SESSION['psi_tab'] ? $_SESSION['psi_tab'] : 0;
	if($act=='del') { //:: Delete record -----------------------------------------
		$aa = array();
		foreach($_POST['ID'] as $v) $aa[] = "'$v'";
		$lstss = join(',',$aa);
		$aa =array_keys($fieldsList);		
		$db = new db();	
		//檢查是否要順便刪除檔案
		if(isset($delFlag)) { 
			$sql = "select * from $tableName where $aa[0] in ($lstss)";
			$rs = $db->query($sql);
			while($r=$db->fetch_array($rs)) {
				$fns = $r[$delField];
				if($fns) {
					$ba = explode(',',$fns);
					foreach($ba as $fn) {
						$fn = $root.$ulpath.$fn;
						//echo $fn;
						if(file_exists($fn)) @unlink($fn);
					}
				}
			}
		}
		$sql = "delete from $tableName where $aa[0] in ($lstss)";
		//echo $sql; exit;
		$db->query($sql);
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	$page = $_REQUEST['page']?$_REQUEST['page']:1; 
	$recno = $_REQUEST['recno']?$_REQUEST['recno']:1;

	$db = new db();
	$sqlAddress = "select id,address,city from $tableName";
	$rsAddress = $db->query($sqlAddress);
	while ($rAddress = $db->fetch_array($rsAddress)) {
		$rowAddress[$rAddress['id']] = $rAddress;
	}

	//:Defined PHP Function -------------------------------------------------------------
	function getBType($id) { global $businessType; return $businessType[$id]; }
	function getCounty($id) { global $county; return $county[$id].$rowAddress[$id]['city'].$rowAddress[$id]['address']; }

	function func_addrE() {
		global $county,$fieldE_Data;
	    $rtn = "<select name='county'>";
			foreach($county as $k=>$v) {
				$ck=($fieldE_Data[county]==$k?'selected':'');
				$rtn .= "<option value='$k' $ck>$v</option>";
			}
	    $rtn .= "</select>";
	    $rtn .= "<select name='city' id='city' style='width:80'>".($fieldE_Data[city]?"<option>$fieldE_Data[city]</option>":'')."</select>";
	    $rtn .= "<input name='address' type='text' id='address' size='30' value='$fieldE_Data[address]' />";
		
		return $rtn;
	}
	
	function func_addr() {
		global $county;
	    $rtn = "<select name='county'>";
			foreach($county as $k=>$v) $rtn .= "<option value='$k'>$v</option>";
	    $rtn .= "</select>";
	    $rtn .= "<select name='city' id='city' style='width:80'></select>";
	    $rtn .= "<input name='address' type='text' id='address' size='30' />";
			return $rtn;
	}
		
	function view_addr($value,$obj) {
		global $county;
		return $county[$obj->curRow[county]].$obj->curRow[city].$obj->curRow[address];
	}
?>	
<title><?=$CompanyName.'-'.$pageTitle?></title>
<link href="/system/PageControl.css" rel="stylesheet">
<link href="/system/ListControl.css" rel="stylesheet">
<link href="/system/ViewControl.css" rel="stylesheet">
<link href="/system/TreeControl.css" rel="stylesheet">

<link href="/media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/media/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">
<link href="/css/formRwd.css" rel="stylesheet">
<style>
	.tabimg { margin-left: 1px; }
</style>
<script src="../../Scripts/jquery-1.12.3.min.js"></script>
<script src="../../media/js/jquery-ui-1.10.1.custom.min.js"></script>
<script src="../../Scripts/bootstrap.min.js"></script>

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<!-- <script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script> -->
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 
<script src="/Scripts/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if($opList['filters']) {
		foreach($opList['filters'] as $k=>$v) {
			if($_REQUEST[$k]) $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if($opList['filterOption'][$k]) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if($opList['keyword']) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = "where";
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	if(!$pageSize) $pageSize=10;
	$pinf = new PageInfo($rCount,$pageSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	echo "<h1 align='center'>$pageTitle ";
	if($opList['keyword']) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ---------------------------------------------------------------------
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div><hr style='margin:3px'>";

	//:ListControl Object --------------------------------------------------------------------
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf("select * from %s %s order by %s %s limit %d,%d",
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	//echo $sql;
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	$obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);
?>	
	<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
		<li <?=($tabidx==0?"class='active'":'')?>><a href="#tab0" data-toggle="tab">基本資料<img id="tabg0" class="tabimg" src="/images/<?= $tabidx==0?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(0)"/></a></li>
		<? if($act!='new'){ ?>
		<li <?=($tabidx==1?"class='active'":'')?>><a href="#tab1" data-toggle="tab">通訊資料<img id="tabg1" class="tabimg" src="/images/<?= $tabidx==1?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(1)"/></a></li>
		<li <?=($tabidx==2?"class='active'":'')?>><a href="#tab2" data-toggle="tab">連絡人<img id="tabg2" class="tabimg" src="/images/<?= $tabidx==2?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(2)"/></a></li>
		<li <?=($tabidx==3?"class='active'":'')?>><a href="#tab3" data-toggle="tab">銀行帳戶<img id="tabg3" class="tabimg" src="/images/<?= $tabidx==3?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(3)"/></a></li>
    	<li <?=($tabidx==4?"class='active'":'')?>><a href="#tab4" data-toggle="tab">帳務資料<img id="tabg4" class="tabimg" src="/images/<?= $tabidx==4?'nail.png':'unnail.png'?>" align="absmiddle" onclick="setnail(4)"/></a></li>
		<? } ?>
	</ul>
  
	<div class="tab-content">
		<div class="tab-pane<?=($tabidx==0?' active':'')?>" id="tab0">
<?	
	//:ViewControl Object ------------------------------------------------------------------------
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			$op3 = array(
				"templet"=>"$root/psi/$tableName/edit.html",
				"type"=>"edit",
				"form"=>array('form1',"../../psi/$tableName/doedit.php",'post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定修改",false,''),
				"cancilBtn"=>array("取消修改",false,'')	);
			$fieldE_Data = $pdata[$listPos];
			$ctx = new BaseViewControl($fieldE_Data, $fieldE, $op3);
			break;
		case 'new':	//新增
			$op3 = array(
				"templet"=>"$root/psi/$tableName/edit.html",
				"type"=>"append",
				"form"=>array('form1',"../../psi/$tableName/doappend.php",'post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,''));
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
		default :	//View 
			if(count($pdata) == 0) break;
			$op3 = array(
				"templet"=>"$root/psi/$tableName/view.html",
				"type"=>"view",
				"submitBtn"=>array("確定變更",true,''),
				"resetBtn"=>array("重新輸入",true,''),
				"cancilBtn"=>array("關閉",true,''));
			$fieldE_Data=$pdata[$listPos];
			$ctx = new BaseViewControl($fieldE_Data, $fieldE, $op3);	
	}
	$id = $pdata[$listPos][id];
?>
    </div>
    <div class="tab-pane<?=($tabidx==1?' active':'')?>" id="tab1">
    	<iframe width="100%" height="500" frameborder="0" src="../psi_community/list.php?fid=<?=$id?>"></iframe>
    </div>
    <div class="tab-pane<?=($tabidx==2?' active':'')?>" id="tab2">
	    <iframe width="100%" height="500" frameborder="0" src="../psi_contact/list.php?fid=<?=$id?>"></iframe>
    </div>
    <div class="tab-pane<?=($tabidx==3?' active':'')?>" id="tab3">
    	<iframe width="100%" height="500" frameborder="0" src="../psi_banks/list.php?fid=<?=$id?>"></iframe>
    </div>
    <div class="tab-pane<?=($tabidx==4?' active':'')?>" id="tab4">
		<iframe width="100%" height="500" frameborder="0" src="../psi_invoice/list.php?fid=<?=$id?>"></iframe>
    </div>
  </div>
<? include '../../public/inc_listjs.php'; ?>
<script>
	function setnail(n) {
		$('.tabimg').attr('src','../../images/unnail.png');
		$('#tabg'+n).attr('src','../../images/nail.png');
		$.get('setnail.php',{'idx':n});
	}
		
	function ninwindows(btn) {
		var elm = document.getElementById('frameUp');
		if(elm.style.display=='none') {
			$(elm).slideDown(500);	
			document.getElementById('frameDw').height = 0;
		} else {
			if(btn.name=='btnDepd')	frameDw.src = "emplyee_dependents/list.php?empID=<?=$empID?>";
			else if(btn.name=='btnHist') frameDw.src = "emplyee_insurence/list.php?empID=<?=$empID?>";
			$(elm).slideUp(500);
			document.getElementById('frameDw').height = 295;
		}
	}

	$(function(){
		/*特殊處理 要防止填入相同的ID 所以先判斷*/
		$(document).on('input','[name="id"]', function(){
			var idThis = this;
			$.ajax({
				url:'/psi/psi_company/checkId.php',
				type:'post',
				data:{id:idThis.value},
				dataType:'json',
				// async: false,
				success:function(d){
					if(d == 0) idThis.setCustomValidity('');
					else idThis.setCustomValidity('此編號已經存在');
				}
			})  
		})
		$("select[name='county']").change(function(){
			var city = $(this).next().val();	//鄉鎮市要緊接縣市
			var cd2 = this.options[this.selectedIndex].value;
			$.getJSON('../getptycd3.php',{'cd2':cd2},function(data){
				var str = '';
				$.each(data,function(idx,itm){ if(itm==city) str += '<option selected>'+itm+'</option>'; else str += '<option>'+itm+'</option>'; });
				$("select[name='city']").html(str);
			});
		});
		$("select[name='county']").change();
	})
	
</script>