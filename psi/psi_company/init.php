<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	@session_start();
	include_once '../../config_psi.php';
	include_once '../../maintain/inc_vars.php';
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php";

	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "機構資料維護";
	//$pageSize=3; 使用預設
	$tableName = "psi_company";
	// for Upload files and Delete Record use
	$ulpath = "/data/";
	$delField = '';
	$delFlag = false;
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);

	//:ListControl  ---------------------------------------------------------------
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','title'),	//要搜尋的欄位	
	);
		
	$fieldsList = array(
		"id"=>array("機構編號","80px",true,array('Id',"gorec(%d)")),
		"title"=>array("機構簡稱","90px",true,array('Text')),
		"titlefull"=>array("機構名稱","100px",true,array('Text')),
		"email"=>array("電子郵件","100px",true,array('Text')),
		"tel"=>array("電話","100px",true,array('Text')),
		"fax"=>array("傳真","100px",true,array('Text')),
		"county"=>array("縣市","150px",true,array('Define',view_addr)),
		"leader"=>array("負責人","100px",true,array('Text')),
		"invno"=>array("統一編號","100px",true,array('Text')),
		"url"=>array("機構網址","100px",true,array('Text')),
		// "bType"=>array("營業別","50px",true,array('Define',getBType)),
		// "states"=>array("往來況狀","80px",true,array('Text')),
		// "levels"=>array("機構等級","50px",true,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"id"=>array("機構編號","text",20,'','',array(true,'','',16)),
		"title"=>array("機構簡稱","text",20,'','',array(true,'','',50)),
		"titlefull"=>array("機構名稱","text",20,),
		"email"=>array("電子郵件","text",20,'','',array(false,PTN_EMAIL,'請輸入電子郵件格式')),
		"tel"=>array("聯絡電話","text",20,'','',array(false,PTN_TEL,'請輸入電話格式')),
		"fax"=>array("傳真號碼","text",20,'','',array(false,PTN_TEL,'請輸入電話格式')),
		"county"=>array("縣市","text",20,'',),
		"city"=>array("鄉鎮區","text",20,'',),
		"address"=>array("地址","Define",60,'',func_addr),
		"PosCode"=>array("郵遞區號","text",10,0,),
		"leader"=>array("負責人","text",20,0,),
		"invno"=>array("統一編號","text",8,'','',array(false,PTN_NUMBER,'請輸入統編格式')),
		"url"=>array("機構網址","text",20,'',),
		// "bType"=>array("營業別","select",1,'',$businessType),
		// "states"=>array("往來況狀","select",1,'',$contactState),
		// "levels"=>array("客戶等級","text",20,),
		"notes"=>array("備註","textbox",50,2,)
	);
	$fieldA_Data = array();
	
	$fieldE = array(
		"id"=>array("機構編號","readonly",20,'','',array(true,'','',16)),
		"title"=>array("機構簡稱","text",20,'','',array(true,'','',50)),
		"titlefull"=>array("機構名稱","text",20,),
		"email"=>array("電子郵件","text",20,'','',array(false,PTN_EMAIL,'請輸入電子郵件格式')),
		"tel"=>array("聯絡電話","text",20,'','',array(false,PTN_TEL,'請輸入電話格式')),
		"fax"=>array("傳真號碼","text",20,'','',array(false,PTN_TEL,'請輸入電話格式')),
		"county"=>array("縣市","text",20,'',),
		"city"=>array("鄉鎮區","text",20,'',),
		"address"=>array("地址","Define",20,'',func_addrE),
		"PosCode"=>array("郵遞區號","text",10,0,),
		"leader"=>array("負責人","text",20,0,),
		"invno"=>array("統一編號","text",8,'','',array(false,PTN_NUMBER,'請輸入統編格式')),
		"url"=>array("機構網址","text",20,'',),
		// "bType"=>array("營業別","select",1,'',$businessType),
		// "states"=>array("往來況狀","select",1,'',$contactState),
		// "levels"=>array("客戶等級","text",20,),
		"notes"=>array("備註","textbox",50,2,),
	);
?>