<?
	include 'init.php';
	include '../../public/inc_frlist.php';
	include '../../public/inc_listjs.php';
?>
<script type="text/javascript">
	$(function(){
		$("select[name='county']").change(function(){
			var city = $(this).next().val();	//鄉鎮市要緊接縣市
			var cd2 = this.options[this.selectedIndex].value;
			$.getJSON('../getptycd3.php',{'cd2':cd2},function(data){
				var str = '';
				$.each(data,function(idx,itm){ if(itm==city) str += '<option selected>'+itm+'</option>'; else str += '<option>'+itm+'</option>'; });
				$("select[name='city']").html(str);
			});
		});
		$("select[name='county']").change();
	})
</script>