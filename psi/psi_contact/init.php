<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once '../../config_psi.php';
	include_once '../../maintain/inc_vars.php';
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php"; 
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "連絡人";
	$pageSize=3; //使用預設
	$tableName = "psi_contact";
	// for Upload files and Delete Record use
	$ulpath = "/data/";
	$delField = '';
	$delFlag = false;
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,false,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);
	
	//:ListControl  ---------------------------------------------------------------
	if($_REQUEST['fid']) $opAR=array($_REQUEST['fid']=>$_REQUEST['fid']); else $opAR=array();
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','title'),	//要搜尋的欄位	
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定GO",false,true,''),
			'fid'=>array($_REQUEST['fid'],true,true)		//filter預設值,是否隱藏
		),
		"filters"=>array('fid'=>$opAR)
	);

	$fieldsList = array(
		"id"=>array("編號","50px",true,array('Id',"gorec(%d)")),
		"title"=>array("連絡人名稱","50px",true,array('Text')),
		"job"=>array("職位","50px",true,array('Text')),
		"tel"=>array("電話","100px",true,array('Text')),
		"email"=>array("電子信箱","150px",true,array('Text')),
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"fid"=>array("機構編號","readonly",20,'',),
		"title"=>array("名稱","text",60,'','',array(true,'','',50)),
		"job"=>array("職位","text",60,'','',array(true,'','',50)),
		"tel"=>array("電話","text",60,'','',array(false,PTN_TEL,'',20)),
		"email"=>array("電子信箱","text",60,'','',array(false,PTN_EMAIL,'',60))
	);
	$fieldA_Data = array("fid"=>$_REQUEST['fid']);

	$fieldE = array(
		"id"=>array("編號","readonly",20,'',),
		"title"=>array("名稱","text",60,'','',array(true,'','',50)),
		"job"=>array("職位","text",60,'','',array(true,'','',50)),
		"tel"=>array("電話","text",60,'','',array(false,PTN_TEL,'',20)),
		"email"=>array("電子信箱","text",60,'','',array(false,PTN_EMAIL,'',60))
	);
?>