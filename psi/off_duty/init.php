<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	// ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	include_once('../../config_psi.php');
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php"; 
		
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "離職專區";
	//$pageSize=3; 使用預設
	$tableName = "emplyee";
	// for Upload files and Delete Record use
	$ulpath = "/data/emplyee/";
	$delField = '';
	$delFlag = false;
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",true,false,doAppend),
		"editBtn" => array("－修改",true,false,doEdit),
		"delBtn" => array("Ｘ刪除",true,false,doDel)
	);
	
	//:ListControl  ---------------------------------------------------------------
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'empID',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('empID','empName'),	//要搜尋的欄位	
		"filterOption"=>array(
			'autochange'=>true,
			'bDate'=>array(date('Y/m'),false),	//預設Key值,是否隱藏
			'goBtn'=>array("確定GO",false,true,'')
		),
		"filterType"=>array(
			'bDate'=>'Month'	//不定義就是 <select>, Month:月選取， DateRange:日期範圍
		),
		"filters"=>array(
			'bDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
		),
		"extraButton"=>array(
			array("月明細表",false,true,''),
			array("年明細表",false,true,'')
		)
	);
		
	$fieldsList = array(
		"empID"=>array("單號","50px",true,array('Id',"gorec(%d)")),
		"empName"=>array("請假員工","100px",true,array('Text')),
		"hireDate"=>array("起始日期","100px",true,array('DateTime2')),
		"leaveDate"=>array("結束日期","100px",true,array('DateTime2')),
		"hours"=>array("時數","50px",true,array('Text')),
		"content"=>array("事由","",false,array('Text')),
		"isOnduty"=>array("主管簽名","",false,array('Bool')),
		"rspContent"=>array("辦理情形","",false,array('Text')),
		"isOK"=>array("准假","",false,array('Bool')),
		"isDel"=>array("銷假","",false,array('Bool'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldE = array(
		"empID"=>array("請假員工","Define",20,'',IDtoName),
		'leadmen'=>array("主管","Define",20,'',IDtoName),
		'agent'=>array("主管","Define",20,'',IDstoName),
		"aType"=>array("假別","label",20,),
		"rdate"=>array("請假日期","datetime",12,),
		"bDate"=>array("起始日期","datetime",12,),
		"eDate"=>array("結束日期","datetime",15,),
		"hours"=>array("時數","text",20,),
		"isOK"=>array("准假","checkbox","",),
		"isReqD"=>array("申請銷假","checkbox","",),
		"isDel"=>array("銷假","checkbox","",),
		'content'=>array("事由","textbox",80,2,''),
		'notes'=>array("交辦事項","textbox",80,2,''),
		'rspContent'=>array("辦理情形","textbox",80,2,'')
	);
?>