<?
	include 'init.php';
	function moneyType($id) { global $moneyType; return $moneyType[$id]; }
	include '../../public/inc_frlist.php';
	include '../../public/inc_listjs.php';
?>
<script type="text/javascript">
	$(function(){
		$('#fid').attr('hidden',true);
		/*特殊處理 要防止填入相同的ID 所以先判斷*/
		$(document).on('input','[name="id"]', function(){
			var idThis = this;
			$.ajax({
				url:'/psi/psi_banks/checkId.php',
				type:'post',
				data:{id:idThis.value},
				dataType:'json',
				// async: false,
				success:function(d){
					if(d == 0) idThis.setCustomValidity('');
					else idThis.setCustomValidity('此編號已經存在');
				}
			})  
		})
	})
</script>