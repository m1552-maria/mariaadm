<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once '../../config_psi.php';
	include_once '../../maintain/inc_vars.php';
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "銀行帳戶";
	$pageSize=3; //使用預設
	$tableName = "psi_banks";
	// for Upload files and Delete Record use
	$ulpath = "/data/";
	$delField = '';
	$delFlag = false;
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,false,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);
	
	//:ListControl  ---------------------------------------------------------------
	if($_REQUEST['fid']) $opAR=array($_REQUEST['fid']=>$_REQUEST['fid']); else $opAR=array();
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','title'),	//要搜尋的欄位	
		"filterOption"=>array(
			'autochange'=>true,
			'goBtn'=>array("確定GO",false,true,''),
			'fid'=>array($_REQUEST['fid'],true)		//filter預設值,是否隱藏
		),
		"filters"=>array('fid'=>$opAR)
	);
		
	$fieldsList = array(
		"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
		"bankID"=>array("銀行代碼","100px",true,array('Text')),
		"title"=>array("銀行簡稱","100px",true,array('Text')),
		"titlefull"=>array("銀行名稱","",true,array('Text')),
		"account"=>array("帳號名稱","100px",true,array('Text')),
		"actno"=>array("帳號","",true,array('Text')),
		"projID"=>array("計畫識別碼","100px",true,array('Text')),
		"dType"=>array("帳戶類別","100px",true,array('Text')),
		"mType"=>array("貨幣類別","100px",true,array('Define',moneyType)),
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"fid"=>array("機構編號","readonly",20,''),
		"id"=>array("銀行編號","text",20,'','',array(true,'','',50)),
		"bankID"=>array("銀行代碼","select",1,'',$bankID),
		"title"=>array("銀行簡稱","text",60,'',''),
		"titlefull"=>array("銀行名稱","text",60,'',''),
		"account"=>array("帳戶名稱","text",60,'','',array(true,'','',50)),
		"actno"=>array("帳號","text",60,'','',array(true,'','',50)),
		"dType"=>array("帳戶類別","select",1,'',$accountType),
		"mType"=>array("貨幣類別","select",1,'',$moneyType),
		"money"=>array("帳戶餘額","text",60,'',''),
		"projID"=>array("計畫識別碼","text",60,'','如需多個識別碼，中間請用半形逗號區隔 例： 30,31,32',array(true,'','')),
		"unit"=>array("委託單位","text",7,'','',array(false,'[0-9]{7}','請輸入編號',7)),
		"notes"=>array("備註","text",60,'','')
	);
	$fieldA_Data = array("fid"=>$_REQUEST['fid']);
	
	$fieldE = array(
		"id"=>array("銀行編號","id",20,),
		"bankID"=>array("銀行代碼","select",1,'',$bankID),
		"title"=>array("銀行簡稱","text",60,'',''),
		"titlefull"=>array("銀行名稱","text",60,'','',array(true,'','',50)),
		"account"=>array("帳戶名稱","text",60,'','',array(true,'','',50)),
		"actno"=>array("帳號","text",60,'','',array(true,'','',50)),
		"dType"=>array("帳戶類別","select",1,'',$accountType),
		"mType"=>array("貨幣類別","select",1,'',$moneyType),
		"money"=>array("帳戶餘額","text",60,'',''),
		"projID"=>array("計畫識別碼","text",60,'','如需多個識別碼，中間請用半形逗號區隔 例： 30,31,32',array(true,'','')),
		"unit"=>array("委託單位","text",7,'','',array(false,'[0-9]{7}','請輸入編號',7)),
		"notes"=>array("備註","text",60,'','')
	);
?>