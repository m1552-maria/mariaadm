// JavaScript Document
var id2proc = { 
		depID:'/maintain/department/query_id.php',
		depID_other:'/maintain/department/query_id_other.php',
		jobID:'/maintain/jobs/query_id.php',
		pmtID:'/maintain/permits/query_id.php',
		empID:'/maintain/emplyee/query_id.php',
		sId:  '/maintain/emplyee/query_idT.php',
		docID:'/maintain/docclasses/query_id.php',
		kwgID:'/maintain/kwgclasses/query_id.php',
		infoID:'/maintain/infoclasses/query_id.php',
		pmpID:'/emplyee/permits_emplyee/query_id.php',
		rspDep:'/maintain/department/query_id.php'
	};

//var obj;
var param
var rzt = false;
function showDialog(aObj) {
	if(rzt){ rzt.close();	rzt = false; } 
	param = $(aObj).prev(); 
	var parid = param.attr('id');
	var procP = id2proc[parid];
	var depID;
	var paramsObj=[];
	if(parid!='depID') {
		if(typeof qryField != 'undefined') {
			depID = $('#'+qryField).val();			
		} else {
			depID = $('#depID').val();
		}
		if(depID)	paramsObj.push('depID='+depID);
	}

	if(param.attr("lead"))	paramsObj.push('lead=1');	//判別是否主管
	if(param.attr("allowAll"))	paramsObj.push('allowAll='+param.attr("allowAll"));
	var element  = document.getElementById("depID_other");
	if(element && element.hasAttribute("data-ui-type"))	paramsObj.push('uiType='+element.dataset.uiType);
	if(paramsObj.length>0)	procP += "?"+paramsObj.join("&");

 	if (rzt == false) {
   	if(parid=='pmtID' || parid=='pmpID') rzt = openBrWindow(procP,param.val(),650,600);
   	else if(parid=='depID_other'){
   		str = '';
   		$('[name="depID[]"]').each(function(){ paramsObj.push($(this).val());	})
			str += paramsObj.join(",");
			str +='/';
			paramsObj=[];
			$('[name="empID[]"]').each(function(){ paramsObj.push($(this).val());	})
			str += paramsObj.join(",");
			rzt = openBrWindow(procP,str,300,450);
   	}	else rzt = openBrWindow(procP,param.val(),300,450);
		rzt.focus();
 	}

	//rzt.postMessage('message',domain);
	/*setTimeout(function(){
		rzt.postMessage('in', procP);
	},500);*/

	/*if(rzt) { 
		alert(rzt);
		param.val(rzt[0]); 
		$(aObj).next().text(rzt[1]); 
		//:: Handler for callback
		var cbFunc = param.attr('callback');
		if(cbFunc) setTimeout(cbFunc+'("'+rzt[0]+'")',0);
	}*/
	
}

function returnValue(data) {
	if(data) {
		param.val(data[0]);
		param.trigger("focus");
		param.next().next('span').html(data[1]); 
		var cbFunc = param.attr('callback');
		if(cbFunc) setTimeout(cbFunc+'("'+data[0]+'")',0);
	}
}

function returnValue_other(data) {
	if(data) {
		detail = '';
		detail_emp = '';
		$.each(data,function(e,v){
			if(v['type']=='depID'){
				if(v['privilege']=='Y'){
					detail = detail + '<li><input type="hidden" name="depID[]" value="'+v['id']+'">'+v['name']+'<img src="/images/remove.png" width="18" align="absmiddle" onclick="d_remove(this)"></li>';
				}else{
					detail = detail + '<li style="display:none"><input type="hidden" name="depID[]" value="'+v['id']+'">'+v['name']+'</li>';
				}
				
			}
			if(v['type']=='empID'){
				if(v['privilege']=='Y'){
					detail_emp = detail_emp + '<li><input type="hidden" name="empID[]" value="'+v['id']+'">'+v['name']+'<img src="/images/remove.png" align="absmiddle" width="18" onclick="d_remove(this)"></li>';
				}else{
					detail_emp = detail_emp + '<li style="display:none"><input type="hidden" name="empID[]" value="'+v['id']+'">'+v['name']+'</li>';	
				}
			}
		})
		$('.depID_list').html('<ul>'+detail+'</ul>');
		$('.empID_list').html('<ul>'+detail_emp+'</ul>');
	}
}

/*** 防止Enter直接送出表單 ***/
function checkFormEnter(evt) {
	if(evt.which == 13 && evt.target.nodeName=='INPUT') return false;
}

/*** 對話框keyin的資料作檢查  ***/
function checkInput(evt,act,elm) {
	var v = elm.value;
	function handleRezult(data) { //Closure
		if(data) {
			if(data=='無此編號！') {
				$(elm).val('');
				$(elm).next().next().text(v+' '+data); 
			} else $(elm).next().next().text(data); 
		}		
	}
	
	if( typeof evt === 'undefined' ) { //with ViewControl
		$.get('/getdbInfo.php',{'act':act,'ID':v},handleRezult);
		return false;
	} else {  //without ViewControl
		if(evt.keyCode==13) {
			$.get('/getdbInfo.php',{'act':act,'ID':v},handleRezult);
			return false;
		}
	}
}

function checkEmpID(value) {
	if ( !NumValidate(value) ) return false;
	if ( value.length!=5 ) { alert('請輸入完整的5位數員工編號'); return false; }
	return true;
} 

function d_remove(now){
	$(now).parent('li').remove();
}


var ppppp;
function openBrWindow(theURL,inParam,win_width,win_height) { 
  var PosX = (document.body.clientWidth-win_width)/2; 
  var PosY = (document.body.clientHeight-win_height)/2; 
  features = "width="+win_width+",height="+win_height+",top="+PosY+",left=-400,scrollbars=yes"; 
  var newwin = window.open(theURL,'',features);
	ppppp = inParam;	
	newwin.selid = inParam;	//send to callee
	
  return newwin;
} 

function genQueryIcon(idx,elm) {
	var str = $(elm).attr('title');
	if(str) {	
		$(elm).after("<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)' align='absmiddle'/><span class='formTxS'>"+str+"</span>");
	} else {
		$(elm).after("<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)' align='absmiddle'/><span class='formTxS'></span>");
	}	
}

$(function() {
	var idstr='';
	$(".queryID").each(genQueryIcon);

	//$('.queryDate').datepicker(	{showOn: 'button', buttonImage: '/images/calendar.jpg', buttonImageOnly: true, dateFormat:'R/mm/dd'});
	$('.queryDate').datepicker(	{showOn: 'button', buttonImage: '/images/calendar.jpg', buttonImageOnly: true});
	$('.queryDate').datepicker('option', 'duration', '');
	$('.queryDate').datepicker($.datepicker.regional['zh-TW']);

	if($('.queryDateTime').length>0) $('.queryDateTime').datetimepicker();
	
  $("select[name='county']").change(function(){
		var city = $(this).next().val();	//鄉鎮市要緊接縣市
		var cd2 = this.options[this.selectedIndex].value;
		$.getJSON('/getptycd3.php',{'cd2':cd2},function(data){
			var str = '';
			$.each(data,function(idx,itm){ if(itm==city) str += '<option selected>'+itm+'</option>'; else str += '<option>'+itm+'</option>'; });
			$("select[name='city']").html(str);
		});
	});
	$("select[name='county']").change();
});
