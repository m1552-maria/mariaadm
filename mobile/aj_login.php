<?php
  @session_start();
  include("..\config_pdo.php");
  if($_REQUEST['empID']) {
    $act = $_REQUEST['empID'];
    $sql = "SELECT empName,A.depID,jobID,jobLevel,canProject,canSchedule,B.depName,canEditEmp FROM emplyee A,department B where A.depID=B.depID and empID=? and isOnduty=1";
    $rs = db_query($sql,array($act));
		if (!db_eof($rs)) {
		  $r = db_fetch_array($rs);
			if ($r) {
				session_unset();										//防止由後台直接過來
				$_SESSION["empName"] = $r[0];
	      $_SESSION["empID"] = $act;
	      $_SESSION["depID"] = $r[1];
				$_SESSION["depTL"] = $r['depName'];
				$_SESSION["jobID"] = $r[2];
				$_SESSION["loginTm"] = date("H:i:s");
				$_SESSION["canProject"] = $r['canProject'];
				$_SESSION["canSchedule"] = $r['canSchedule'];
				$_SESSION["canEditEmp"] = $r['canEditEmp'];

				$_SESSION['domain_Host']=$r[1];		//主管
				$_SESSION['privilege']=10;				//主管後臺權限
				echo "OK";
			}	else echo "Your Account is not open yet !";
		} else echo "帳號 或 密碼錯誤, 請確認後重新輸入！";
  }
?>