<?php
  $self = $_SERVER['PHP_SELF']."?incfn=cmsView.php";
	$wkmode = $_REQUEST['wkmode'];
	$tbl = 'bullets';
 	$cid = $_REQUEST['cid']?$_REQUEST['cid']:0; 	//類別
	$hdpic = $cid?'最新公告':'最新訊息';
 	
	if(!$wkmode) {   //換頁處理 
		$r = db_fetch_array(db_query("select count(*) from $tbl where classid='$cid' and unValidDate>curdate()"));
		$recs   = $r[0];										//紀錄數
		$pageln = 10;												//每頁幾行
		$page   = $_REQUEST[page];					//第幾頁
		$pages  = ceil($recs / $pageln);  	//總頁數				
		$sql = "select * from $tbl where classid='$cid' and unValidDate>=curdate() order by R_Date Desc limit ".$page*$pageln.",$pageln";
		$rsC = db_query($sql);
	} else {		//單筆
		$sql = "select * from $tbl where ID=".$_REQUEST['ID'];	
		$rsC = db_query($sql);
		$rC = db_fetch_array($rsC);
	} 
?>
<div id="inc_main">
  <div class="elipseDiv"><i class="icon-list"></i> <?=$hdpic?></div>
  <? if(!$wkmode) { //清單模式 ?>
    <table id="news" width="100%" border="0" cellspacing="0" cellpadding="4" class="cmsTable">
    <? while($rsC && $rC=db_fetch_array($rsC)) { ?>
      <tr>
        <td class="cell"><span style="color:#993300"><?= date('Y/m/d',strtotime($rC['R_Date'])) ?></span></td>
        <td class="cell"><a href="<?=$self?>&ID=<?=$rC[ID]?>&wkmode=1&cid=<?=$cid?>"><?=$rC[SimpleText]?></a></td>
      </tr>
    <? } ?>
    <tr><td colspan="2">
    <? //page row creation
    if($pages>1) {
      if ($page>0) echo "<a href='$self&cid=$cid&page=".($page-1)."'>前一頁</a>";
      $pg = floor($page / 10); 
      $i = 0;
      $p = $i+($pg*10);
      while ( ($p<$pages) && ($i<10) ) {
        echo " | ";
        if ($p==$page) echo ($p+1); else echo "<a href='$self&cid=$cid&page=".$p."'>".($p+1)."</a>";
        $i++;
        $p = $i+($pg*10);
      }
      echo " | ";
      if ($page < $pages-1) echo "<a href='$self&cid=$cid&page=".($page+1)."'>下一頁</a>";
    }	?>
    </td></tr>
    </table>        
  <? } else { //內容模式 ?>
    <? include '../getEmplyeeInfo.php'; ?>
    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="cmsTable">
    <tr><td style="padding-left:4px" bgcolor="#F8F8F8">
      <? echo $rC['SimpleText'].'&nbsp;&nbsp;&nbsp;('.date('Y/m/d',strtotime($rC['R_Date'])).')'; if($rC['announcer']) echo "<span class='rightCell'>發佈者：".($emplyeeinfo[$rC['announcer']]?$emplyeeinfo[$rC['announcer']]:$rC['announcer'])."</span>"; ?>
      </td></tr>
      <tr><td height="1" bgcolor="#C0C0C0"></td>
    </tr>

      <? if($rC['filepath']) { ?><tr><td>附件：<? $ext = pathinfo($rC['filepath'],PATHINFO_EXTENSION); $fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; echo "<img src='/images/$fn' align='absmiddle'/>";?><a href="/data/bullets/<?=$rC['filepath']?>" target="_new">下載</a></td></tr><? } ?>
      <tr><td><p><?=$rC['Content']?></p></td></tr>
      <tr><td><p>&gt;&gt; <a href='javascript:history.back()'>返回</a></p></td></tr>
    </table>
  <? } ?>
</div>
