<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once('../configN.php');
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/getEmplyeeInfo.php";
	include_once "$root/system/db.php"; 
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "代換班清單";
	//$pageSize=3; 使用預設
	$tableName = "emplyee_duty_change";
	// for Upload files and Delete Record use
	$ulpath = "/data/";
	$delField = '';
	$delFlag = false;
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn"=>array("◎搜尋",false,true,SearchKeyword),
		"newBtn"=>array("＋新增",false,false,doAppend),
		"editBtn"=>array("－修改",true,false,doEdit),
		"delBtn"=>array("Ｘ刪除",false,false,doDel)
	);

	//:ListControl  ---------------------------------------------------------------
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','agent'),	//要搜尋的欄位	
		"checkbox"=>false,
		"filterOption"=>array(
			'autochange'=>true,
			'empID'=>array($_SESSION['empID'], false,true),
			'goBtn'=>array("確定GO",false,true,'')
		),
		"filters"=>array( //過濾器
			'empID'=>array()
		)
	);
		
	$fieldsList = array(
		"id"=>array("編號","50px",true,array('Id',"gorec(%d)")),
		"agent"=>array("換班對象","60px",true,array('Define',IDtoName)),
		"dutyID"=>array("要換班的時段","80px",true,array('Define',getDutyPeriod)),
		"dutyID2"=>array("被換班的時段","80px",true,array('Define',getDutyPeriod)),
		"leadmen"=>array("需簽名者","",false,array('Define',IDtoNames))
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"jobID"=>array("編號","text",20,'','',array(true,'','',16)),
		"jobTitle"=>array("全稱","text",60,'','',array(true,'','',50)),
		"jobName"=>array("簡稱","text",60,'','',array(true,'','',20)),
		"jobEn"=>array("英文名稱","text",60,'','',array()),
		"description"=>array("部門敘述","text",60,'','',array())
	);
	$fieldA_Data = array();
	
	$fieldE = array(
		"jobID"=>array("編號","id",20,),
		"jobTitle"=>array("全稱","text",60,),
		"jobName"=>array("簡稱","text",60,),
		"jobEn"=>array("英文名稱","text",60,),
		'description'=>array("部門敘述","textbox",60,5,'')
	);
?>