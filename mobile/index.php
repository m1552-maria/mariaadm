<?php
  @session_start();
  include "..\config_pdo.php";
  include '..\inc_sys.php';
  $incfn = $_REQUEST['incfn']?$_REQUEST['incfn']:'inc_main.php'
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  <title>老爹網Mobile</title>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">

  <script src="/Scripts/jquery-3.3.1.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    var act,actname,depTL;
    window.onload = function(){
      var st = window.localStorage;
      act = st.getItem('account');
      actname = st.getItem('actname');
      depTL = st.getItem('depTL');
      if(!act) window.location.href = "login.php";
      else {
        $('.userInfo').text(actname+' ('+depTL+')');
        $('#headpic').attr('src','/data/emplyee/'+act+'.jpg');
        <? if(!$_SESSION['empID']) { ?>
        $.ajax({
          url: 'aj_login.php',type:"POST",data: {'empID': act },dataType:'text',
          success:function(respone){ console.log(respone); },
          error:function(xhr, ajaxOptions, thrownError){ alert(xhr.status+','+thrownError); }
        });
        <? } ?>
      }
    }

    function openNav() {
      var elm = document.getElementById("mySidenav");
      elm.style.width = "250px";
      $(elm).addClass('shadowD');
      //document.getElementById("main").style.marginLeft = "250px"; //push content 
    }

    function closeNav() {
      var elm = document.getElementById("mySidenav");
      elm.style.width = "0";
      $(elm).removeClass('shadowD');
      //document.getElementById("main").style.marginLeft= "0"; //push content
    }
  </script>
</head>

<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 headDiv">
      <div class="headTxt">
        <span class="menubtn" onclick="openNav()">&#9776;</span>
        <div class="lgInfo userInfo"><?=$_SESSION["empName"]." ($_SESSION[depTL])"?></div>
        <? include $incfn ?>
        <p>&nbsp;</p>
        <div class="footDiv"><?=$gHTML_Title?></div>
      </div>
    </div>
  </div>
</div>

  
  
<div id="mySidenav" class="sidenav">
  <div style='text-align: center'><img id="headpic" src='/data/emplyee/99998.jpg' style="padding: 4px; background-color: #ffffff"></div>
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="userInfo" style="text-align: center"><?=$_SESSION["empName"]." ($_SESSION[depTL])"?></div>
  <hr/>
  <a href="/mobile"><i class='icon-home'></i> 回首頁</a>
  <a href="/mobile/index.php?incfn=emplyee_duty\list.php"><i class="icon-calendar"></i> 出勤表</a>
  <a href="/mobile/index.php?incfn=emplyee_duty_change\list.php"><i class="icon-calendar"></i> 代換班清單</a>
  <a href="/mobile/index.php?incfn=emplyee_duty_change_unsign\list.php"><i class="icon-calendar"></i> 代換班簽名</a>
  <a href="logout.php"><i class="icon-off"></i> 登出</a>
</div>
  
</body>
</html> 