<?php
  @session_start();
  include("..\config_pdo.php");
  if($_REQUEST['empID']) {
    header("Content-Type:text/html; charset=utf-8");
    $act = $_REQUEST['empID'];
		$pwd = $_REQUEST['password'];
    $sql = "SELECT empName,A.depID,jobID,jobLevel,canProject,canSchedule,B.depName,canEditEmp FROM emplyee A,department B where A.depID=B.depID and empID=? and password=? and isOnduty=1";
    $rs = db_query($sql,array($act,$pwd));
		if (!db_eof($rs)) {
		  $r = db_fetch_array($rs);
			if ($r) {
				session_unset();										//防止由後台直接過來
				$_SESSION["empName"] = $r[0];
	      $_SESSION["empID"] = $act;
	      $_SESSION["depID"] = $r[1];
				$_SESSION["depTL"] = $r['depName'];
				$_SESSION["jobID"] = $r[2];
				$_SESSION["loginTm"] = date("H:i:s");
				$_SESSION["canProject"] = $r['canProject'];
				$_SESSION["canSchedule"] = $r['canSchedule'];
				$_SESSION["canEditEmp"] = $r['canEditEmp'];

				$_SESSION['domain_Host']=$r[1];		//主管
				$_SESSION['privilege']=10;				//主管後臺權限
				echo "<script>window.onload=function(){ 
         var st=window.localStorage; 
         st.setItem('account','".$act."'); 
         st.setItem('actname','".$r[0]."'); 
         st.setItem('depTL','".$r['depName']."'); 
         location.href='index.php'; }</script>";
        exit;
			}	else $ErrMsg = "Your Account is not open yet !";
		} else $ErrMsg = "帳號 或 密碼錯誤, 請確認後重新輸入！";
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  <title>老爹網Mobile-登入</title>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">

  <script src="/Scripts/jquery-3.3.1.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
      <div id="main">
        <? if($ErrMsg) { ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <i class="icon-warning-sign"></i> <?=$ErrMsg?>
        </div>
        <? } ?>
        <img id="logo" src="../pan/loginfm/pic1.jpg">
        <form role="form">
          <div class="input-group form-group">
            <div class="input-group-prepend">
							<span class="input-group-text pinkBG"><i class="icon-user"></i></span>
						</div>
            <input type="text" class="form-control" id="empID" name="empID" placeholder="請輸入5位數員工編號" />
          </div>
          
          <div class="input-group form-group">
            <span class="input-group-text pinkBG"><i class="icon-lock"></i></span>
            <input type="password" class="form-control" id="password" name="password" placeholder="請輸入密碼" />
          </div>
          
          <div class="form-group checkBox">
            <input type="checkbox" class="bigCB" /> 
            <label> 記住帳號及密碼 (下次不用登入)</label>
          </div> 
          
          <div class="form-group">
            <button type="submit" class="btn btn-block pinkBtn">送 出</button>
          </div>
          
        </form>
      </div>
		</div>
		<div class="col-md-1"></div>
	</div>
</div>  
</body>
</html>  