<br/>
<?
	$empID=$_SESSION['empID'];
	//::for OLD WAY Uesed
	$VenderDB = 'mariaadm';
	$conn = mysql_connect("localhost","root","16657143") or die("資料庫連接失敗!");		
  mysql_select_db($VenderDB,$conn) or die("資料庫選擇失敗!");
	mysql_query("SET NAMES 'utf8'");

	function IDtoName($v) {
		global $emplyeeinfo;
		return $emplyeeinfo[$v];
	}
	function IDtoName2($v,$row) {
		global $emplyeeinfo;
		$signed = $row->curRow['is_agent']?'<i class="icon-ok-sign"></i>':($_SESSION['empID']==$v?'<i class="icon-pencil"></i>':'');
		return $emplyeeinfo[$v].$signed;
	}
	function IDtoNames($v,$row) {
		global $emplyeeinfo;
		$aa = explode(',',$v);
		$ba = array();

		$signV = $row->curRow['is_leadmen'];
		$pos = 0;
		foreach($aa as $id) {
			$signed = (pow(2,$pos) & $signV)?'<i class="icon-ok-sign"></i>':($_SESSION['empID']==$aa[$pos]?'<i class="icon-pencil"></i>':'');	
			$ba[] = $emplyeeinfo[$id].$signed;
			$pos++;
		}
		return join(',',$ba);
	}
	function getDutyPeriod($v,$row) {
		if(!$v) {
			if($row->curRow['wkType']=='調班') return "<span class='notice'>尚未選取時段</span>";  
			else return "代班";
		}
		$sqltx = "select startTime,endTime from emplyee_duty where id=$v";
		$rs = mysql_query($sqltx);
		$r = mysql_fetch_array($rs);
		return date('Y-m-d H:i',strtotime($r[0])).'~'.date('H:i',strtotime($r[1]));
	}

	include 'emplyee_duty_change_unsign/init.php';	
	include '../public/inc_list.php';
	include '../public/inc_listjs.php';	
?>
<style>
	.headTxt { background-image : url("../../css/default/pic3.jpg"); }
	.headTxt .pageinfoDiv { color: #FFFFFF}
	#ListControl { margin:-10px }
	h1 { font-size: 18px;	float:right; font-weight: bold }
	#d_setting_change{ display:none;position:fixed;top:30%;left:5%; width: 90%;	z-index:9990;	box-shadow: 12px 12px 10px rgba(0, 0, 0, 0.6);}
	.divBorder2 { border:1px solid #CCC;	background:#F0F0F0;	font-size:12px; padding: 5px; }
	.divTitle2{	color:#fff;	background-color:#168FF9;	border: 0px;	padding: 5px;	margin: 0px;	font-size:12px; }
	.dBottom{	background:#dedede; text-align:center;padding:2px;border:1px solid #ccc;}
	.icon-pencil { font-size:18px; color: blue}
</style>
<script>
	$(function() {
		$('.icon-pencil').on('click',openSignDiv);
		<? if($_REQUEST['msg']) echo "alert('$_REQUEST[msg]');"; ?>
	});

	function doCancel(id){ $('#bgCover').hide(); $('#'+id).hide(); }
	function openSignDiv(){
		var aNode = this.parentNode.parentNode;
		if(aNode) {
			$(aNode).find('td').each(function(k,v){
				switch(k) {
					case 0 : $('#id').val(v.innerText); break;
					case 1 : $('#frEmpName').text(v.innerText); break;
					case 2 : $('#toEmpName').text(v.innerText); break;
					case 3 : $('#frPeriod').text(v.innerText); break;
					case 4 : 
						if(v.innerText=='尚未選取時段') {
							$('#toPeriod').html('<select id="dutyList" name="dutyID2"></select>');
							getDutyID();
						}	else $('#toPeriod').text(v.innerText); 
						break;
					case 5 : $('#siegnEmpName').text(v.innerText); break;																				
				}
			});
			$('#d_setting_change').show();
		}
	}

	function getDutyID(){	//取的換班對象的 班表 資料
		var nowlUrl = "getDutyID.php?empID=<?=$empID?>";
		$.ajax({
			url: nowlUrl,type:"GET",dataType:'json',
			success: function(data){
				$("#dutyList option").remove();
				for(var k in data) $('#dutyList').append('<option value="'+k+'">'+data[k][0]+'~'+data[k][1]+'</option>');
			},
			error:function(xhr, ajaxOptions, thrownError){
				alert(xhr.status+','+thrownError);
			}
		});
	}

	function formValid(tfm) {
		if(tfm.dutyList) {
			if(tfm.dutyList.selectedIndex<0) { 
				alert('沒有可用的時段可換!'); 
				return false; 
			} 
		}
		return true;
	}
</script>

<div id='d_setting_change'>
	<form id="form1" action="emplyee_duty_change_unsign/sign.php" method="POST" onsubmit="return formValid(this)">
	<div id='dTitle' class='divTitle2' style="font-size:16px"><i class="icon-pencil"></i> 代換班簽名</div>
	<div class='divBorder2'>
		<table style='width:98%' cellpadding="8">
			<tr><td align="right">調換班的人員：</td><td id="frEmpName"></td></tr>
			<tr><td align="right">要調整的時段：</td><td id="frPeriod"></td></tr>
			<tr><td align="right">調換班的對象：</td><td id="toEmpName"></td></tr>
			<tr><td align="right">被調整的時段：</td><td id="toPeriod"></td></tr>
			<tr><td align="right">簽核人員：</td><td id="siegnEmpName"></td></tr>
		</table>
	</div>
	<div class='dBottom'>
		<input type="hidden" name="id" id="id"/>
		<input type="hidden" name="empID" value="<?=$empID?>"/>
		<input type="submit" value='確定簽名' id='enter' onclick=''/>&nbsp;
		<input type="button" value='關閉離開' onclick='doCancel("d_setting_change")'/>
	</div>
	</form>
</div>