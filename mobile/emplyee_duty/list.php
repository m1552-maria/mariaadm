<?php
	include '../maintain/inc_vars.php';
	$empID=$_SESSION['empID'];

	//取得異常表資料
	class dutyAbnormal { public $bDate; public $eDate; public $abStatus; public $id; public $notes;	public $normal;	}
	$sql="select * from emplyee_duty_abnormal ";
	$sql.="where empID='$empID' ";
	/*20180111 sean 增加狀態類別 "上班或下班未打卡"*/
	$sql.="and abStatus in ('異常','遲到','早退','未打卡','上班或下班未打卡','未打卡-請假異常') ";
	$rs=db_query($sql);
	$abData=array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			if(!$abData[$r['fid']])	$abData[$r['fid']]=array();
			$obj=new dutyAbnormal();
			$obj->bDate=$r['bDate'];
			$obj->eDate=$r['eDate'];
			$obj->abStatus=$r['abStatus'];
			$obj->id=$r['id'];
			$obj->notes=$r['notes'];
			$obj->normal=$r['normal'];
			array_push($abData[$r['fid']],$obj);
		}
	}
	
	$dateFrom = date('Y-m-d', strtotime('-6 months'));
	$sql="select * from emplyee_duty where empID='$empID' and startTime>'$dateFrom'";
	$rs=db_query($sql);
	$evtlst = array();
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)) {
			$bD = $r[startTime];
			$eD = $r[endTime];
			$bD2= $r[startTime2];
			$eD2 = $r[endTime2];
			/*20171218 sean 新增logBD2,logED2 用於顯示綠色打卡時間*/
			$logBD2 = $r[startTime2];
			$logED2 = $r[endTime2];
			/*20171218 sean edit*/
			if(!empty($r[dutyID])) $dutyID = $r[dutyID];
			$short_bD=date('H:i',strtotime($bD));
			$short_eD=date('H:i',strtotime($eD));
			$short_bD2=date('H:i',strtotime($bD2));
			$short_eD2=date('H:i',strtotime($eD2));
			if($bD2 && $eD2){ $title=$short_bD2."~".$short_eD2;
			}else if($bD2){ $title=$short_bD2."~ 無"; $logED2 = $bD2;
			}else if($eD2){ $title="無 ~".$short_eD2;	$bD2=$bD2 ? $bD2 : "";	$logBD2 = $eD2; }
			$evtlst[] = "{title:'$short_bD~$short_eD',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#18F',  borderColor :'#18F', id:'".$r['id']."'}";	//排班
			$evtlst[] = "{title:'$title',start:'$logBD2',end:'$logED2',allDay:true,backgroundColor:'#218C03',  borderColor :'#218C03'}";			//打卡
			foreach($abData[$r[id]] as $k=>$v){
				$start=$bD2 ? $bD2 : $bD;
				$end=$eD2? $eD2 : $eD;
				if($v->normal==1){//異常已稽核完，轉正常
					$evtlst[] = "{title:'正常',start:'".$start."',end:'".$end."',allDay:false,backgroundColor:'#ff9900',  borderColor :'#ff9900' , id:'".$v->id."'}";	
				}else{//異常
					$evtlst[] = "{title:'".$v->abStatus."',start:'".$start."',end:'".$end."',allDay:false,backgroundColor:'#e63900',  borderColor :'#e63900' , id:'".$v->id."'}";	
				}
			}
		}
	}
	$sql="select dutyID,title from dutys where dutyID=".$dutyID;
	$rs=db_query($sql);
	if(!db_eof($rs)){
		$r=db_fetch_array($rs);
		$duty_id=$r[dutyID];
		$duty_title=$r[title];
	}
	if(isset($_SESSION['duty_zoom']))	$zoom=$_SESSION['duty_zoom'];
?>

<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<link rel='stylesheet' type='text/css' href='/scripts/fullcalendar1.6/fullcalendar.css' />
<style type="text/css">
	.ui-widget {font-size:12px; }
	.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
	.title{	text-align:center;	background-color:#E1E1E1;}
	table.grid{  margin:5px;	border: 1px solid #ccc; border-collapse: collapse; }
	table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; text-align:center;}
	.divBorder2 { border:1px solid #CCC;	background:#F0F0F0;	font-size:12px; padding: 5px; }
	.divTitle{	color:#fff;	background-color:#900;	border: 0px;	padding: 5px;	margin: 0px;	font-size:12px; }
	.divTitle2{	color:#fff;	background-color:#168FF9;	border: 0px;	padding: 5px;	margin: 0px;	font-size:12px; }
	.title-green{	background-color: #218C03;}
	.dBottom{	background:#dedede; text-align:center;padding:2px;border:1px solid #ccc;}
	#d_setting_date{ display:none;position:fixed;top:40%;left:5%; width: 90%;	z-index:9990;	box-shadow: 12px 12px 10px rgba(0, 0, 0, 0.6);}
	#d_setting_change{ display:none;position:fixed;top:30%;left:5%; width: 90%;	z-index:9990;	box-shadow: 12px 12px 10px rgba(0, 0, 0, 0.6);}
	#bgCover{ position:fixed; width: 100%; height: 120%; background-color: rgba(0, 0, 0, 0.6); top: 0; left: 0;z-index: 9; display: none }
	#d_dutylog{	display:none;position:fixed;top:40px;left:300px;	z-index:9999; }
	#dutylog{max-height: 100px;	overflow:auto; }
	#calendar{	background-color:#fff; border:1px solid #888; margin: 0 -20px; }
	#calendar h2 {font-size: 1em; padding-top: 5px }
	.fc .fc-header-space {	padding-left: 0;}
	.color_tip{	width:18px; height:18px;float:left;}
	.text_tip{	float:left;}
	#color_box{	 color: #ffffff}
	.fc-event-time{   display : none;}
	.tip{ color:#cc0000; }
	.title-red{ color:#F2C8C5; font-weight: bold;}
	.fc-header{ margin-bottom: -17px }
	.fc-first, .fc-last {width: 12%}
</style>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.16.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src='/scripts/fullcalendar1.6/fullcalendar.min.js'></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script type="text/javascript">
  var zoom='<?=$zoom?>';
  var ab_id=0;
  $(function() {
		$('#d_setting_date').draggable();	
		$("#d_dutylog").draggable({scroll: true });		
		$('#calendar').fullCalendar({
			header: {left:'prev,next today', center:'title', right:'month,agendaWeek,agendaDay'},
			editable: false,
			buttonText : { today: '今日',month: '月',agendaWeek: '周',	agendaDay: '日'	},
			monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			dayNames : ['周日','週一','週二','週三','週四','週五','週六'],
			dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],
			height: 500,
			eventClick: function(event) {
				var obj=event;
				var date = obj['start'];
				var y,M,d,h,sd,ed;
				if(obj['title']!="異常" && obj['title']!="遲到" && obj['title']!="早退" && obj['title']!="未打卡" && obj['title']!="上班或下班未打卡" && obj['title']!="未打卡-請假異常" && obj['title']!="正常"){//點擊排班
					if(obj['backgroundColor']=="#218C03"){ //點擊打卡
						y=date.getFullYear();	M=(date.getMonth()+1); d=date.getDate();	sd=y+"/"+M+"/"+d;		ed=y+"/"+M+"/"+d;
						getDutylog(sd,ed);
					}	else if(new Date() > date) {
							alert('日期已過，無法代換班!'); 
							return false; 
						} else {
							ab_id=obj['id'];
							$('#dutyID').val(ab_id);
							$('#frPeriod').text( obj["start"].toLocaleDateString()+' '+obj['title'] );
							$('#bgCover').show();
							$('#d_setting_change').show();
						}
				}else{ //點擊異常 TODO:: 應檢查是否已經關帳，目前結構下，不好檢查
					ab_id=obj['id'];
					$('#bgCover').show();
					$('#d_setting_date').show();
					$.ajax({
					  url: "../maintain/emplyee_duty/api.php",  method: "POST",  dataType:"text",
					  data: { act: "get_abNotes", id: obj['id'] }
					}).done(function(rs) {
						var notes=rs.split("(by");
						$("#notes").val(notes[0]);
					});
				}
			}, 
			events: [ <? echo join(',',$evtlst)?>	]
		});
		$("input[name='wkType']").click(function(){
			if(this.value=='代班') {
				$("#dutyList option").remove();
			} 
		});
	});

	function doCancel(id){ $('#bgCover').hide(); $('#'+id).hide(); }
	function set_abNotes(){
		if(ab_id==0)	return;
		$.ajax({
		  url: "../maintain/emplyee_duty/api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act: "set_abNotes", id: ab_id, notes:$("#notes").val()}
		}).done(function() {
			location.replace(location.href);
		});
	}
	function getDutylog(sd,ed){
  		var empID="<?=$empID?>";
  		$.ajax({
		  url: "../maintain/emplyee_duty/api.php",
		  method: "POST",
		  dataType:"text",
		  data: { act: "getDutylog", empID: empID, sd:sd , ed:ed }
		}).done(function(rs) {
			if(rs){
				var data=rs.split("#");
				var row;
				var tb="<table class='grid' style='width:98%'><tr><td width='100'>狀態</td><td>地點</td><td>打卡時間</td></tr>";
				for(var i=0;i<data.length;i++){
					row=data[i].split(",");
					tb+="<tr><td>"+row[0]+"</td><td>"+row[1]+"</td><td>"+row[2]+"</td></tr>";
				}
				tb+="</table>";
				$("#dutylog").html(tb);
			}else{
				$("#dutylog").html("NO DATA");
			}
			$("#d_dutylog").show();
		});
	}
	function getDutyID(eid){	//取的換班對象的 班表 資料
		var nowlUrl = "getDutyID.php?empID="+eid;
		$.ajax({
			url: nowlUrl,type:"GET",dataType:'json',
			success: function(data){
				$("#dutyList option").remove();
				if ($("input[name='wkType']:checked").val()=='調班') 	for(var k in data) $('#dutyList').append('<option value="'+k+'">'+data[k][0]+'~'+data[k][1]+'</option>');
			},
			error:function(xhr, ajaxOptions, thrownError){
				alert(xhr.status+','+thrownError);
			}
		});
	}
	function addItem(tbtn,fld,qid) {
		var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
			+ "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode;
		$(elm).remove();
	}
	function formValid(tfm) {
		for(var i=0; i<sId.length; i++) {
			if(!sId[i].value) { alert('請選擇對象!'); return false; }
		}
	}
</script>

<div id="inc_main">
	<div id='d_setting_date'>
    <div id='dTitle' class='divTitle' style="font-size:16px"><i class="icon-bug"></i>異常原因</div>
    <div class='divBorder2'>
    	<table style='width:98%'>
				<tr><td style="color:#666">[說明]：<br>可直接輸入文字或在 <b>空白內容</b> 時 按下 [↓]鍵，選取常用打卡異常說明</td></tr>
    		<tr id='tr_notes'><td style="padding:4px">
        	<input type='text' id='notes' list='reason' style="width:100%"/>
          <datalist id="reason"><? foreach($abNormalText as $v) echo "<option value='$v'/>";?></datalist>
        </td></tr>
    	</table>
    </div>
    <div class='dBottom'>
    	<input type="button" value='確定' id='enter' onclick='set_abNotes()'/>&nbsp;
    	<input type="button" value='關閉' onclick='doCancel("d_setting_date")'/>
    </div>
	</div>

	<div id='d_setting_change'>
		<form id="form1" action="emplyee_duty/doDutyChange.php" method="POST" onsubmit="return formValid(this)">
    <div id='dTitle' class='divTitle2' style="font-size:16px"><i class="icon-share"></i> 調整排班申請</div>
    <div class='divBorder2'>
    	<table style='width:98%' cellpadding="8">
				<tr><td align="right">自己要調整的班別：</td><td id="frPeriod"></td></tr>
				<tr><td align="right">調整的方式：</td><td><input type="radio" name="wkType" value="調班" checked />調班 &nbsp;<input type="radio" name="wkType" value="代班"/>代班</td></tr>
				<tr><td align="right">要調換班的對象：</td><td>
					<input name="agent" type="text" class="queryID" id="sId" size="6" callback="getDutyID" onkeypress="return checkInput(event,'empName',this)"/>
					<span class='formTxS'></span>
				</td></tr>
    		<tr><td align="right">對方要調整的班別：</td><td><select id="dutyList" name="dutyID2"></select>
				</td></tr>
				<tr><td align="right">簽核主管：</td><td>
					<input name="leadmen[]" type="text" class="queryID" id="sId" lead="1" size="6" onkeypress="return checkInput(event,'empName',this)"/>
					<span class='formTxS'></span>
					<button type="button" class="sBtn" onclick="addItem(this,'leadmen','sId')"> +新增</button>
				</td></tr>
    	</table>
    </div>
    <div class='dBottom'>
			<input type="hidden" name="empID" value="<?=$empID?>"/>
			<input type="hidden" name="dutyID" id="dutyID"/>
    	<input type="submit" value='確定' id='enter'/>&nbsp;
    	<input type="button" value='關閉' onclick='doCancel("d_setting_change")'/>
		</div>
		</form>
	</div>

	<div id='color_box'>
		<span class='color_tip' style='background:#18F'></span><span class='text_tip'>排班&nbsp;&nbsp;&nbsp;&nbsp;</span><span class='color_tip' style='background:#00A800'></span><span class='text_tip'>打卡&nbsp;&nbsp;&nbsp;&nbsp;</span>
		&nbsp;使用中班表名稱：<span class='title-red'><?=$duty_title."(".$duty_id.")"?></span>
	</div>

	<? if($_REQUEST['code']) { ?>
		<div class="alert <?=$_REQUEST['code']?> alert-dismissible fade show" role="alert">
			<?=$_REQUEST['msg']?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
	<? } ?>

	<div id='calendar'></div>

	<div id='d_dutylog'>
   	<div class='divTitle title-green'>打卡紀錄</div>
   	<div class='divBorder2' style='width:400px;' id="dutylog"></div>
   	<div class='dBottom'><input type="button" value='關閉' onclick='doCancel("d_dutylog")'/></div>
	</div>

	<div id="bgCover"></div>
</div>