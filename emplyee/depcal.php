<?
	$depID = $_SESSION[depID];
	include '../config.php';
	include '../getEmplyeeInfo.php';
	$self = $_SERVER['PHP_SELF']."?incfn=depcal.php";

	$dateB = date("Y/m/01",strtotime("-1 month"));	//今日
	$dateE = date("Y/m/01",strtotime("+5 month"));

	$sql = "select * from schedule where PID is not  NULL and ( (beginDate between '$dateB' and '$dateE') or (endDate between '$dateB' and '$dateE') ) and ClassID<>'活動/旅遊'";	//選部門 並排除 活動
	$rs = db_query($sql);
?>
<style>
.calDay {
	background-image: url(/images/activity.png) !important;
  background-repeat: no-repeat !important;
  background-position: 2px 2px !important;
}
</style>
<script>
	Accordion1.openPanel(4);
	
	Date.prototype.ymdhi = function() {
		var mm = this.getMonth() + 1; 
  	var dd = this.getDate();
		var hh = this.getHours();
		var ii = this.getMinutes();
	  return [this.getFullYear(),(mm>9?'':'0')+mm,(dd>9?'':'0')+dd].join('-')+' '+(hh>9?'':'0')+hh+':'+(ii>9?'':'0')+ii;
	}
	
	function showTooltip(data,event,view) {
		tooltip = '<div class="tooltiptopicevent" style="background:#feb811;position:absolute;z-index:10001;padding:10px 10px 10px 10px;border-radius:10px">'
		 + data.title + '</br>' + data.start.ymdhi() + '<br>' + data.end.ymdhi() + '</div>';
		$("body").append(tooltip);
		$(this).mouseover(function (e) {
			$(this).css('z-index', 10000);
			$('.tooltiptopicevent').fadeIn('500');
			$('.tooltiptopicevent').fadeTo('10', 1.9);
		}).mousemove(function (e) {
			$('.tooltiptopicevent').css('top', e.pageY + 10);
			$('.tooltiptopicevent').css('left', e.pageX + 20);
		});
  }
	function hideTooltip(data, event, view) {
    $(this).css('z-index', 8);
    $('.tooltiptopicevent').remove();
  }
				
	$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {left:'prev,next today', center:'title', right:'month,agendaWeek,agendaDay'},
			editable: false,
			buttonText : { today: '今日',month: '月',agendaWeek: '周',	agendaDay: '日'	},
			monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			dayNames : ['周日','週一','週二','週三','週四','週五','週六'],
			dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],
			timeFormat: 'H:mm',	//'h(:mm)t' 
			eventMouseover: showTooltip,
			eventMouseout: hideTooltip,
			// dayClick: function(date,allDay,jsEvent,view) {
			// 	var aDay = date.toLocaleDateString();
			// 	var r = confirm('是否確定新增'+aDay+'日的活動行事曆!');
			// 	if(r)	window.open('index.php?incfn=schedule/append.php&act=activity&day='+aDay);
			// },
			events: [ <? 
			$evtlst = array();
			if(!$_POST['bType']) while($r=db_fetch_array($rs)) { //部門行事曆
				$aa = explode(',',$r[PID]);
				foreach($aa as $v) if( strpos($depID,$v) !== false ) {
				  $bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				 	$eD = date('Y/m/d H:i',strtotime($r[endDate]));
				 	$evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#B08080',borderColor:'#B08080'}";
					break;
				}
			} 
			//部門請假紀錄
			$aa = array();
			array_push($aa,"depID like '$depID%'");
			if(!empty($_POST['depFilter'])){
				$sql="select A.*,B.empName,B.depID from absence A,emplyee B where A.empID=B.empID and ((bDate between '$dateB' and '$dateE') or (eDate between '$dateB' and '$dateE')) and depID like '{$_POST['depFilter']}%' and isOK=1 and isDel=0 order by bDate";
				$sql2 = "select o.* from outoffice o inner join emplyee e on o.empID = e.empID where (o.bDate between '$dateB' and '$dateE') and e.depID like '{$_POST['depFilter']}%' order by o.bDate";
				$sql3 = "select d.* from emplyee_duty d inner join emplyee e on d.empID = e.empID where (d.startTime between '$dateB' and '$dateE') and e.depID like '{$_POST['depFilter']}%' and (dutyID in (SELECT dutyID FROM `dutys` where uType=2)) order by d.startTime";
			}else{
				foreach($_SESSION['user_classdef'][2] as $v) if($v) $aa[]="depID like '$v%'";
				$likeStr = join(' or ',$aa);
				$sql="select A.*,B.empName,B.depID from absence A,emplyee B where A.empID=B.empID and ((bDate between '$dateB' and '$dateE') or (eDate between '$dateB' and '$dateE')) and ($likeStr) and isOK=1 and isDel=0 order by bDate";
				$sql2 = "select o.* from outoffice o inner join emplyee e on o.empID = e.empID where (o.bDate between '$dateB' and '$dateE') and ($likeStr) order by o.bDate";
				$sql3 = "select d.* from emplyee_duty d inner join emplyee e on d.empID = e.empID where (d.startTime between '$dateB' and '$dateE') and ($likeStr) and (dutyID in (SELECT dutyID FROM `dutys` where uType=2)) order by d.startTime";
			}
			if($_POST['bType']==1){
				$rs = db_query($sql);			
				while($r=db_fetch_array($rs)) { 
					$bD = date('Y/m/d H:i',strtotime($r[bDate]));
					$eD = date('Y/m/d H:i',strtotime($r[eDate]));
					$evtlst[] = "{title:'".$emplyeeinfo[$r[empID]]." - $r[aType]',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#80B0B0',borderColor:'#80B0B0'}";
				} 
				$rs2 = db_query($sql2);			
				while($r2=db_fetch_array($rs2)) { 
					$bD = date('Y/m/d H:i',strtotime($r2[bDate]));
					$eD = date('Y/m/d H:i',strtotime($r2[eDate]));
					$evtlst[] = "{title:'".$emplyeeinfo[$r2[empID]]." - 外出',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#80B0B0',borderColor:'#80B0B0'}";
				} 
			} else if($_POST['bType']==2) {
				$rs3 = db_query($sql3);			
				while($r3=db_fetch_array($rs3)) { 
					$bD = date('Y/m/d H:i',strtotime($r3[startTime]));
					$eD = date('Y/m/d H:i',strtotime($r3[endTime]));
					$evtlst[] = "{title:'".$emplyeeinfo[$r3[empID]]."',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#80B0B0',borderColor:'#80B0B0'}";
				} 
			}	
			echo join(',',$evtlst);	?>
			]
		});
		// $('td[class*="fc-day"]').addClass('calDay');
	});	
</script>
<link rel='stylesheet' type='text/css' href='/scripts/fullcalendar/fullcalendar.css' />
<script type='text/javascript' src='/scripts/jquery-ui-1.7.2.custom.min.js'></script>
<script type='text/javascript' src='/scripts/fullcalendar/fullcalendar.min.js'></script>
<div class="pageTitle" style="display: inline;">部門行事曆</div>
<form name="form1" method="post" onSubmit="return Form1_Validator(this);" style="display: inline;margin:0">
	<select name="depFilter" id="depFilter" onChange="filterSubmit(this)">
		<option value=''>-全部部門-</option>
		<?
			if ($_SESSION['privilege'] > 10) {
				foreach($departmentinfo as $k=>$v) echo "<option value='$k'".($_REQUEST[depFilter]==$k?' selected ':'').">$v</option>";
			}else{
				foreach($_SESSION['user_classdef'][2] as $v) if($v) echo "<option value='$v'".($_REQUEST[depFilter]==$v?' selected ':'').">$departmentinfo[$v]</option>";
			}
		?>
	</select>
	<input type="radio" name="bType" value="0" checked onChange="filterSubmit(this)">部門
	<input type="radio" name="bType" value="1" <?= ($_POST['bType']==1?'checked':'')?> onChange="filterSubmit(this)">請假、外出
	<input type="radio" name="bType" value="2" <?= ($_POST['bType']==2?'checked':'')?> onChange="filterSubmit(this)">排班
</form>
<div id='calendar' style=""></div>

<script type="text/javascript">
	function filterSubmit(tSel) {
		tSel.form.submit();
	}
</script>