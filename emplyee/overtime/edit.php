<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	include '../maintain/inc_vars.php';	
	$empID = $_SESSION[empID];
	$depID = $_SESSION["depID"];
	$sql = "select * from overtime where id=$_REQUEST[id]";
	$rs = db_query($sql);
	if($rs) $r=db_fetch_array($rs);
?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
Accordion1.openPanel(12);
function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	var bDate = tfm.bDate.value;
	var eDate = tfm.eDate.value;
	if(!tfm.bDate.value) { alert('起始日期不能為空白！'); tfm.bDate.focus(); return false; }
	if(!tfm.eDate.value) { alert('結束日期不能為空白！'); tfm.eDate.focus(); return false; }
	var bDateA = bDate.split('/'); 
	var eDateA = eDate.split('/');
	if( (bDateA[0]!=eDateA[0]) || (bDateA[1]!=eDateA[1]) ) {alert('請勿跨月請假，若有此需要請分單'); return false; }
	if( bDateA[2]>eDateA[2] ) {alert('結束日期 不能早於 起始日期'); return false; }
	if(!tfm.content.value) {alert('請填寫加班事由！'); tfm.content.focus(); return false;}
  var rzt = true;
  var manager = '';
  var manager_up = '';
	$(tfm['leadmen[]']).each(function(){if(!this.value) {alert('請選擇主管'); rzt = false;}else{manager = this.value;}});
  $(tfm['agent[]']).each(function(){manager_up = this.value;});
  
  if(manager_up == manager){
    alert('直屬主管和主管不能是同一個人');
    rzt=false;  return false;
  }

  /*if($("[name='aType']").val()==1){ 
    if($("[name='hours']").val()==4 || $("[name='hours']").val()==8 || $("[name='hours']").val()==12){
    }else{
      rzt = false;
      alert("休息日加班時數有誤");return false;  
    }
  }*/

	return rzt;
}
$(function(){
	$("#prjform").keypress(checkFormEnter); 
	$.get('/getdbInfo.php',{'act':'dep','depID':'<?=$depID?>'},function(data){ 
		if(data) {
			var aa = data.split(',');
			var elm = document.getElementsByName('leadmen')[0];
			elm.value = aa[2];
			$(elm).next().next().text(aa[3]); 
		}
	});

  //判斷一開始的加班別
  if($("[name='aType']").val()==1){
    $('.aTypeerror').show();
  }
});


function change_aType(now){
  if($(now).val()==1){
    $('.aTypeerror').show();
  }else{
    $('.aTypeerror').hide();
  }
}

</script>

<style type="text/css">
  .aTypeerror{
    color:red;
    display: none;
  }
</style>

<form id="prjform" action="overtime/editDo.php" method="post" onsubmit="return formCheck(this)">
<table width="100%" border="0" cellpadding="8" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td align="right">加班人：</td>
    <td>
		<?=$_SESSION["empName"]?><input name="empID" type="hidden" id="empID" value="<?=$empID?>"/>
    <input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
    <input name="id" type="hidden" id="id" value="<?=$r[id]?>"/>
    </td>
    <td align="right">選擇：</td>
    <td><input name="fmAct" type="radio" value="0" <?=$r[fmAct]==0?'checked':''?> />
      補休
        <input name="fmAct" type="radio" value="1" <?=$r[fmAct]==1?'checked':''?>/>
      加班費 </td>
  </tr>
  <tr>
    <td align="right">加班別：</td>
    <td><select name="aType" id="aType" onchange="/*change_aType(this)*/">
      <? foreach($overtimeType as $k=>$v) {$ck=($r[aType]==$k)?'selected':''; echo "<option $ck value='$k'>$v</option>"; } ?>
    </select></td>
    <td align="right">加班時數：</td>
    <td><input name="hours" type="text" id="hours" size="6" value="<?=$r[hours]?>"/>
   <!-- <div class="aTypeerror">*時數只能是 4H、8H、12H</div>-->
    </td>
  </tr>
  <tr>
    <td align="right">起始日期：</td>
    <td><input type="text" name="bDate" id="bDate" value="<?=date('Y/m/d H:i',strtotime($r[bDate]))?>"/><script>$(function(){$('#bDate').datetimepicker({hour:<?=date('H',strtotime($r[bDate]))?>,minute:<?=date('i',strtotime($r[bDate]))?>})})</script></td>
    <td align="right">結束日期：</td>
    <td><input type="text" name="eDate" id="eDate" value="<?=date('Y/m/d H:i',strtotime($r[eDate]))?>"/><script>$(function(){$('#eDate').datetimepicker({hour:<?=date('H',strtotime($r[eDate]))?>,minute:<?=date('i',strtotime($r[eDate]))?>})})</script></td>
  </tr>
	  
  <tr>
    <td align="right">加班事由</td>
    <td colspan="3"><textarea name="content" id="content" cols="60" rows="3"><?=$r[content]?></textarea></td>
  </tr>
  
  <tr>
    <!--
    <td align="right">組長：</td>
    <td>
    <? $aa = explode(',',$r[agent]); ?>
  	<input name="agent[]" type="text" class="queryID" id="empID" size="6" value="<?=$aa[0]?>" title="<?=$emplyeeinfo[$aa[0]]?>"/> 
    <span class="btn" onclick="addItem(this,'agent','empID')"> +新增組長)</span>
    <? for($i=1; $i<count($aa); $i++) {
			 echo "<div><input name='agent[]' id='empID' type='text' size='6' value='$aa[$i]'>"
			 . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$emplyeeinfo[$aa[$i]]."</span>"
			 . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		} ?>
    </td>
    -->
    <td align="right">主管</td>
    <td colspan="3">
    <input name="leadmen[]" type="text" class="queryID" id="empID" size="6" value="<?=$r[leadmen]?>" title="<?=$emplyeeinfo[$r[leadmen]]?>" onkeypress="return checkInput(event,'empName',this)"/> </td>

    

  </tr>

  <tr>
    <td align="right">直屬主管</td>
    <td colspan="3">
    <input name="agent[]" type="text" class="queryID" id="empID" size="6" value="<?=$r[agent]?>" title="<?=$emplyeeinfo[$r[agent]]?>" onkeypress="return checkInput(event,'empName',this)"/> </td>
  </tr>
  
  <tr>
    <td colspan="4" align="center">
    	<input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
</form>