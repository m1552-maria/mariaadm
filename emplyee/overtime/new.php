<?
	include '../config.php';
	include '../maintain/inc_vars.php';	
	include '../getEmplyeeInfo.php';	
	$empID = $_SESSION[empID];
	$depID = $_SESSION["depID"];
	$hoursOfMonth=46;
	$sql="select * from overtime ";
	$sql.="where empID='".$empID."' and ((isOK=0 and is_leadmen=0) or isOK=1) ";
	$sql.="and year(bDate)=".date("Y")." and month(bDate)=".date("m")." ";
	$sql.="and (aType=0 or aType=1) ";
	//echo $sql;
	$rs=db_query($sql);
	$hours=0;
	$hours1=0;//補休時數
	$hours2=0;//加班費時數
	if(!db_eof($rs)){
		while($r=db_fetch_array($rs)){
			/*$bDate=getdate(strtotime($r['bDate']));
			$eDate=getdate(strtotime($r['eDate']));*/
			$hours+=$r['hours'];
			if($r[fmAct]==0){//補休
				$hours1+=$r[hours];
			}else if($r[fmAct]==1){//加班費
				$hours2+=$r[hours];
			}
		}
	}
?><head>

<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<link href="/css/mapping.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#d_tip{
	font-family: "微軟正黑體";
	background:#FFDFDF;
	border:1px solid #FFA2A2;
	padding:8px;
	width:420px;
	border-radius: 5px;
	cursor:pointer;
	
	position: absolute;
	left:50%;
	top:50%;
	margin-top:-50px;
	margin-left:-50px;
	display: none;
}
</style>
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
Accordion1.openPanel(12);
function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
		+ "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck() {
	var bDate = $("[name='bDate']").val();
	var eDate = $("[name='eDate']").val();
	var hours='<?=$hours?>';
	if(!($("[name='hours']").val()>0)) { alert('預計時數錯誤'); $("[name='hours']").focus(); return; }
	if($("[name='aType']").val()==0 && $("[name='hours']").val()>4){	alert("上班日加班時數不得大於4小時");return;}
	/*if($("[name='aType']").val()==1){	
		if($("[name='hours']").val()==4 || $("[name='hours']").val()==8 || $("[name='hours']").val()==12){
		}else{
			alert("休息日加班時數有誤");return;	
		}
	}*/
	if(('<?=$hoursOfMonth?>'-hours-$("[name='hours']").val())<0){	alert('時數錯誤：本月可申請加班的剩餘時數為'+('<?=$hoursOfMonth?>'-hours)+'小時');return;}
	if(!bDate) { alert('起始日期 不能為空白！'); $("[name='bDate']").focus(); return; }
	if(!eDate) { alert('結束日期 不能為空白！'); $("[name='eDate']").focus(); return; }
	var bDateA = bDate.split('/'); if(bDateA.length<3) {alert('起始日期 格式錯誤！'); $("[name='bDate']").focus(); return ; }
	var eDateA = eDate.split('/'); if(eDateA.length<3) {alert('結束日期 格式錯誤！'); $("[name='eDate']").focus(); return; }
	
	if( bDate.split(' ')[0]!=eDate.split(' ')[0] ) {alert('請勿跨天，若有此需要請分單'); return; }
	if( bDateA[2]>eDateA[2] ) {alert('結束日期 不能早於 起始日期'); return; }
	if(!$("[name='content']").val()) {alert('請填寫加班事由'); $("[name='content']").focus(); return;}
  	var rzt = true;
  	var manager = '';
  	var manager_up = '';
	$("[name='leadmen']").each(function(){if(!this.value) {	
			alert('請選擇主管'); rzt=false;	return false;
		} else {
			if(checkEmpID(this.value)) {
				manager = this.value;
				if(this.value=='<?=$empID?>') {
					alert('核准主管不能為請假人'); 
					rzt=false;	return false;
				}
			}
		}
	});
	$("[name='agent']").each(function(){
		if(this.value) {
			if(checkEmpID(this.value)) {
				manager_up = this.value;
				if(this.value=='<?=$empID?>') {
					alert('核准直屬主管不能為請假人'); 
					rzt=false;	return false;
				}
			}
		}
	});



	//判斷直屬主管和主管是不是同一個人
	if(manager_up == manager){
		alert('直屬主管和主管不能是同一個人');
		rzt=false;	return false;
	}

	if(!rzt)	return;
	// var agent=[];
	// $("[name='agent[]']").each(function(){
	// 	if(this.value!="") 	agent.push(this.value);
	// });

	var year=new Date($("[name='bDate']").val()).getFullYear();
	var month=new Date($("[name='bDate']").val()).getMonth()+1;
	$.get('overtime/func.php',{'act':'get_overtimeHours','empID':'<?=$empID?>','year':year,'month':month},function(hr){ 
		if(('<?=$hoursOfMonth?>'-hr-$("[name='hours']").val())>=0){
			$.post( "overtime/newDo.php", { 'act':'add_overtimeHours',empID: "<?=$empID?>", hours: $("[name='hours']").val(),content:$("[name='content']").val() , bDate: $("[name='bDate']").val() ,eDate:$("[name='eDate']").val(), leadmen: $("[name='leadmen']").val(),aType:$("[name='aType']").val(),fmAct:$('input[name=fmAct]:checked').val(), agent: $("[name='agent']").val() },function(data){
				var obj = JSON.parse(data);
				if(obj.result == 1){
					alert('時間區間有誤，此時段已經有加班紀錄，請重新填寫時間');
				} else if(obj.result == 2){
					alert('本月份已經關帳，無法請假');
				} else if(obj.result == 3) {
					alert('主管編號有誤');
				} else{
					location.href="index.php?incfn=overtime/my.php&pjid="+obj.id;
				}
			});
		}else{
			alert("時數已超過"+month+'月可申請的加班時數');
		}
	});
}

$(function(){
	$.get('/getdbInfo.php',{'act':'dep','depID':'<?=$depID?>'},function(data){ 
		if(data) {
			var aa = data.split(',');
			var elm = document.getElementsByName('leadmen')[0];
			elm.value = aa[2];
			$(elm).next().next().text(aa[3]); 
		}
	});
	$("#d_tip").draggable();
	$("#detail").draggable();
});
function showTip(bol){
	//2017-4-14 拿掉
	// if(bol){
	// 	document.getElementById('d_tip').style.display='block';	
	// }else {
	// 	document.getElementById('d_tip').style.display='none';
	// }
}

function showMapping(empID){
	var dateB,dateE;
	if($("#dateB").val()){
		dateB=$("#dateB").val();
		dateE=$("#dateE").val();
	}else{	dateB='';	dateE='';}

	$.ajax({
		url: '../maintain/absence/tuning.php?act=getMapping&dateB='+dateB+'&dateE='+dateE+'&empID='+empID,
		type:"POST",
		dataType:'text',
		success: function(data){
			$('#detail').html(data);
			showDetail();
		},
		error:function(xhr, ajaxOptions, thrownError){  }
	});
	
}
function showDetail(){
	detail.style.display='block';
}
function closePnl(){
	detail.style.display='none';
}

function change_aType(now){
	if($(now).val()==1){
		$('.aTypeerror').show();
	}else{
		$('.aTypeerror').hide();
	}
}

$(function(){
	var firstDate = '<?php echo date("Y/m/1 00:00:00")?>';
	$('#bDate').change(function(){
		$('#dateLable').css("display","none");
		if(Date.parse(firstDate).valueOf() > Date.parse($(this).val()).valueOf()){
			$('#dateLable').css("display","");
			$('[data-date="1"]').css("display","none");
			$('[data-date="0"]').children("input").attr("checked",true);
		} else {
			$('[data-date="1"]').css("display","");
		}
	});
});

</script>
<style type="text/css">
	.aTypeerror{
		color:red;
		display: none;
	}
</style>
</head>

<table width="170" border="1" bordercolor="#f0f0f0" bordercolordark="#FFFFFF" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana; color:#999" align="right">
	<tr><th colspan="2" bgcolor="#f0f0f0"><?=date('Y')?>年<?=date("m")?>月-加班時數統計</th></tr>
  <tr><td align="right">加班時數</td><td>&nbsp;<?=$hours?> 小時</td></tr>
  <!--
  <tr><td align="right">已請補休時數</td><td>&nbsp;<?=$hours1?> 小時</td></tr>
  <tr><td align="right">已請加班費時數</td><td>&nbsp;<?=$hours2?> 小時</td></tr>
  <tr><td align="right">剩餘加班時數</td><td>&nbsp;<?=($hoursOfMonth-$hours)?> 小時</td></tr>
  -->
</table>
<table width="600" cellpadding="3" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
<tr><td bgcolor="#e0e0e0" align="center">加班申請須知</td></tr>
<tr><td ><div style="overflow:auto; height:100px; width:100%;"><?= nl2br(file_get_contents('../data/overtime.txt'))?></div></td></tr>
</table>
<div style="height:10px"></div>
<div style="border-top:1px #999999 dashed; height=1px;"></div>
<div style="height:5px"></div>
<table width="600" border="0" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td width="130" align="right" class='gridtd'>加班人：</td>
    <td>
		<?=$_SESSION["empName"]?><input name="empID" type="hidden" id="empID" value="<?=$empID?>"/>
    <input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
    </td>
    <td width="90" align="right">選擇：</td>
    <td width="150" align="left">
    	<label data-date="0"><input name="fmAct" type="radio" value="0" class="various" checked="checked" onclick="showTip(false)"/>補休</label>
      <label data-date="1"><input name="fmAct" type="radio" value="1" class="various" onclick="showTip(true)"/>加班費</label>
        <br>   
      <label id="dateLable" style="color: red; display: none;">※跨前月加班只能請補休</label>
    </td>
  </tr>
  <tr>
    <td align="right">加班別：</td>
    <td><select name="aType" id="aType" onchange="/*change_aType(this)*/">
      <? foreach($overtimeType as $k=>$v) { if($_REQUEST['aType']==$k) echo "<option selected='selected' value='$k'>$v</option>"; else echo "<option value='$k'>$v</option>"; } ?>
    </select></td>
    <td align="right">預計時數：</td>
    <td align="left"><input name="hours" type="text" id="hours" size="6" />&nbsp;<input type='button' value='補休明細'  onclick='showMapping("<?=$empID?>")'/><div class="aTypeerror">*時數只能是 4H、8H、12H</div>
    </td>
  </tr>
  <tr>
    <td align="right">起始日期：</td>
    <td><input type="text" name="bDate" id="bDate"/><script>$(function(){$('#bDate').datetimepicker()})</script></td>
    <td align="right">結束日期：</td>
    <td align="left"><input type="text" name="eDate" id="eDate"/>
    	<script>
    		$(function(){
    			$('#eDate').click(function(){
    				if($('#eDate').val()==''){
    					$('#eDate').val($('#bDate').val());//先塞入值//因為datetimepicker的預設值太難設也沒辦法塞之後要給的值
    					$('#eDate').blur();//解綁
    				}
    				$('#eDate').datetimepicker();
    			}) 
    		})
    	</script>
    </td>
  </tr>
	  
  <tr>
    <td align="right">加班事由：</td>
    <td colspan="3"><textarea name="content" id="content" cols="60" rows="3"></textarea></td>
  </tr>
  <tr>
  	<td align="right" valign="top">主管：</td>
    <td align="left" valign="top" colspan='3'><input name="leadmen" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)"/><span class='formTxS'></span></td>
  </tr>

  <tr>
  	<td align="right" valign="top">直屬主管：</td>
    <td align="left" valign="top" colspan='3'><input name="agent" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)"/></td>
  </tr>

  <tr>
    <td colspan="4" align="center">
    	<input type="button" name="Submit" id="Submit" value="送出" onclick="formCheck()"/>
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
<div id='d_tip'><?=$overtimeTip[0]?>&nbsp;&nbsp;<img src="../../images/del.png" width='25' align="absmiddle" onclick="showTip(false)" /></div>

<div id='detail'></div>
