<?
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../../maintain/inc_vars.php';	
	$id = $_REQUEST[id];
	$sql = "select * from overtime where id=$id";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);

  $sql1 = "select * from overtime_del where fkey=$id";
  $rs1 = db_query($sql1);
  $r1 = db_fetch_array($rs1);

  $fmActAry=array("0"=>"補休","1"=>"加班費");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/css/index.css" rel="stylesheet" type="text/css">
<title>表單列印</title>
<style>
body{
	margin:10px;
	background:#fff;
}
.pageTitle {
	font-family: "微軟正黑體", Verdana;
	font-size: 18px;
	font-weight: bold;
	padding-bottom: 12px;
	text-align:center;
}

</style>
</head>

<body>
<table width="92%" border="1" align="center" cellpadding="10" cellspacing="0" style="font:normal 15px '微軟正黑體',Verdana"> 
	<tr>
    <td colspan='4' class="pageTitle">加班單</td>
  </tr>
  <tr>
    <td width="100" align="right">加班人</td>
    <td><?=$emplyeeinfo[$r[empID]]?></td>
    <td align="right">申請時間</td>
    <td><?= date('Y-m-d H:i',strtotime($r[rdate]))?></td>
  </tr>
  <tr>
      <td align="right">加班別</td>
      <td colspan="3"><?=$overtimeType[$r['aType']]?></td>
    </tr>
  <tr>
    <td align="right">加班日期</td><td><?= date('Y-m-d H:i',strtotime($r[bDate]))?> ~ <?=date('Y-m-d H:i',strtotime($r[eDate]))?></td>
    <td align="right">加班時數</td>
    <td><?=$r["hours"]?> 小時</td>
  </tr>
	  
  <tr>
    <td align="right">類型</td>
    <td><?=$fmActAry[$r[fmAct]]?></td>
    <td align="right">加班事由</td>
    <td><?=$r[content]?></td>
  </tr>
	<tr>
    <td align="right">主管</td>
    <td><?=$emplyeeinfo[$r[leadmen]]?></td>
    <td align="right">是否准加班</td>
    <td>
      <? 
        if($r['is_leadmen']==1) echo '已准加班 '.date('Y-m-d',strtotime($r[is_leadmen_date]));
        elseif($r['is_leadmen']==-1) echo '不准加班 '.date('Y-m-d',strtotime($r[is_leadmen_date]));
      ?>
    </td>

  </tr>

  <?if($r['agent']!=''){?>
  <tr>
    <td align="right">直屬主管</td>
    <td><?=$emplyeeinfo[$r[agent]]?></td>
    <td align="right">是否准加班</td>
    <td>
      <? 
        if($r['is_agent']==1) echo '已准加班 '.date('Y-m-d',strtotime($r[is_agent_date]));
        elseif($r['is_agent']==-1) echo '不准加班 '.date('Y-m-d',strtotime($r[is_agent_date]));
      ?>
    </td>
  </tr>
  <tr>
    <td align="right">是否准加班</td>
    <td colspan="3"><? if($r['isOK']) echo '已准加班 ';?></td>
  </tr>
  <?}?>

	<tr>
	  <td align="right">辦理情形</td>
	  <td colspan="3"><?=$r['rspContent']?></td>
  </tr>

  <?if($r['isReqD']==1){?>
    <tr>
      <td colspan='4' class="pageTitle">銷單紀錄</td>
    </tr>

    <tr>
      <td align="right">是否申請銷班</td>
      <td colspan="3"><?=$r['isReqD']?'是':'否';?></td>
    </tr>
    
    <tr>
      <td align="right">主管</td>
      <td><?=$emplyeeinfo[$r1[leadmen]]?></td>
      <td align="right">是否准銷班</td>
      <td>
        <? 
          if($r1['is_leadmen']==1) echo '已准銷班 '.date('Y-m-d',strtotime($r1[is_leadmen_date]));
          elseif($r1['is_leadmen']==-1) echo '不准銷班 '.date('Y-m-d',strtotime($r1[is_leadmen_date]));
        ?>
      </td>

    </tr>

    <?if($r1['agent']!=''){?>
    <tr>
      <td align="right">直屬主管</td>
      <td><?=$emplyeeinfo[$r1[agent]]?></td>
      <td align="right">是否准銷班</td>
      <td>
        <? 
          if($r1['is_agent']==1) echo '已准銷班 '.date('Y-m-d',strtotime($r1[is_agent_date]));
          elseif($r1['is_agent']==-1) echo '不准銷班 '.date('Y-m-d',strtotime($r1[is_agent_date]));
        ?>
      </td>
    </tr>
    <?}?>

    <?if($r1['is_agent']!=0 && $r1['is_leadmen']!=0){?>
    <tr>
      <td align="right">是否准銷班</td>
      <td colspan="3"><?=$r['isDel']?'已准銷班':'不准銷班';?></td>
    </tr>
    <?}?>

    <tr>
      <td align="right">辦理情形</td>
      <td colspan="3"><?=$r1['rspContent']?></td>
    </tr>
  <?}?>

</table>
</body>
</html>