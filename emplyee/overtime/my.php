<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
  include '../maintain/inc_vars.php'; 
	$self = $_SERVER['PHP_SELF']."?incfn=overtime/my.php";
	$sql = "select *,o.id as id,o.isOK as isOK,o.is_leadmen as is_leadmen,o.content as content ,o.agent as agent,o.is_agent as is_agent,o.rspContent as rspContent,";
  $sql.="od.is_leadmen as is_leadmen2,od.is_agent as is_agent2,o.leadmen as leadmen,o.rdate as rdate ";
	$sql.="from overtime as o ";
	$sql.="left join overtime_del as od ";
	$sql.="on o.id=od.fkey ";
	$sql.="where o.empID='$_SESSION[empID]'";
	$sqc = "select count(ID) from overtime where empID='$_SESSION[empID]'";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 10;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數
	$sql .= " order by o.rdate Desc limit ".$page*$pageln.",$pageln";
	// echo $sql;
	$rs = db_query($sql);	
?>
<script>
	Accordion1.openPanel(12);
	function stateitgform(id,men,agent) {
		statefm.id.value = id;
		$("#td_id").html(id);
		statefm.leadmen.value = men;
    statefm.agent.value = agent;
		$('#div1').show();
	}
	function doDel(id){
		if(!confirm("確定刪除?")){ return;}
		location.href='overtime/del.php?id='+id;
	}
	
</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<style>
#div1 {
	position:absolute; 
	left: 40%; top:30%;
	background-color:#888;
	padding: 20px;
	display:none;
}
.date{width:50px;}
</style>
<div id="div1">
  <form id="statefm" action="overtime/req.php">
  	<table bgcolor="#FFFFFF" cellpadding="4">
    	<tr><td align="right">加班單號：</td><td id='td_id' ></td></tr>
        <tr><td align="right">說明：</td><td><textarea name="content" rows="5" cols="30"></textarea></td></tr>
        <tr><td colspan="2" align="center">
        <input type='hidden' name='id'/>
      	<input type="hidden" name="leadmen" />
        <input type="hidden" name="agent" />
        <input type="submit" name="Submit" value="確定" />
        <button type="button" onclick="$('#div1').hide()">取消</button>
      </td></tr>
    </table>  
  </form>
</div>

<div class="pageTitle">我的加班單</div>	
<table class="sTable" width="100%" cellpadding="4" cellspacing="0">
<tr class="hdCell">
	<td >申請日期</td>
  <td >起始日期</td>
  <td >結束日期</td>
  <td >加班別</td>
  <td width="40">時數</td>
  <td>事由</td>
  <td bgcolor="#FFEEFF" width="60">主管</td>
  <td width="40" bgcolor="#FFEEFF">簽核</td>
  <td width="40" bgcolor="#FFEEFF">准假</td>
  <td bgcolor="#FFFFDF" width="70">辦理情形</td>
  <td width="90" align="center" bgcolor="#E8E8D0">維護</td>
</tr>
<? while($rs and $r=db_fetch_array($rs)) { ?>
  <tr valign="top">
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['rdate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['bDate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['eDate'])) ?></td>
    <td class="cell" align="center"><?=$overtimeType[$r['aType']]?></td>
    <td class="cell" align="center"><?=$r['hours']?></td>
    <td class="cell"><?=$r['content']?></td>
    <td class="cell">
      <?
        echo $emplyeeinfo[$r['leadmen']];
        if($r['agent']!=''){
          echo '</br>'.$emplyeeinfo[$r['agent']];
        }
      ?>
      
    </td>
    <td class="cell">
      <? 
        $checked = $r[is_leadmen]?'checked.gif':'uncheck.gif';
        echo '<img src="'.$checked.'">';
        if($r['agent']!=''){
          $checked = $r[is_agent]?'checked.gif':'uncheck.gif';
          echo '</br><img src="'.$checked.'">';
        }
      ?>

      
    </td>
    <td class="cell"><img src="<?=$r[isOK]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><?
	 	  if($r['isDel'])	echo "<span class='noticeA'>已銷班</span><br>";   
			else if($r[isDel]==0 && ($r[is_leadmen2]==-1 || $r[is_agent2]==-1))	echo "<span class='noticeB'>不准銷單</span><br>";
		 	else if($r['isReqD'])	echo "<span class='noticeC'>已申請銷班</span><br>";
			else if($r['isOK']==1) echo "<span class='noticeA'>已准單</span><br>";
     
			/* if($r['fmAct']==0) { //only check 補休,※會不准 暫時註解
				//$dateB = date("Y-m-d H:i:s",strtotime("-6 month")); 
				//$dateE = date("Y-m-d H:i:s",strtotime("-5 month"));
				//20180701 變更規則
				$dateB = date("Y-12-01");
				$dateE = date("Y-12-31");	
				$dateD = date('Y-m-d H:i:s',strtotime($r['bDate']));
				$sqlx = "select * from absence_overtime where otId='{$r['id']}'";
				$rsX2=db_query($sqlx);
				$v = 0;
				while($rX2=db_fetch_array($rsX2)) $v += $rX2['hours'];
				$hours2 = $r['hours']-$v;    //未沖銷時數
				if($hours2 != 0){
					if($dateD > $dateB){
						if($dateD < $dateE)	echo '<p style="color:#ffc107;">未補休時數'.$hours2.'小時</p>';
					} //else echo '<p style="color:red;">已逾期未補休時數'.$hours2.'小時</p>';
				}
			} */
		?></td>
    <td class="cellsmall">
     	<a href="printform.php?overtime/view.php?id=<?=$r[id]?>" target="_blank"><img src="print.png" title="列印" align="absmiddle" border="0"/></a>
			<? if($r['is_leadmen'] || $r['is_agent']) {
					if( (date('Y/m',strtotime($r['bDate']))>=date('Y/m')) && $r['isOK']==1 && $r[isReqD]=='0') {//尚未使用於補休，才可以銷單
					 	echo "<span class='sBtn' onclick=stateitgform($r[id],'$r[leadmen]','$r[agent]')>我要銷班</span>";
					}
				 }else if($r[isOK]==1){
					 //批次匯入的單據
         }else echo "<a class='sBtn' href='index.php?incfn=overtime/edit.php&id=$r[id]'>修改</a> <a class='sBtn' onclick='doDel($r[id])' >刪除</a>";
			?>
    </td>    
  </tr>
<? } ?>
<? //::page row creation
if($pages>1) {
	?>
    
<tr><td colspan="11" class="topline">
<?
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	
?>
</td></tr>
</table>
