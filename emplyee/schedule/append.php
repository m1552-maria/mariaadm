<?
	include("../config.php");
	include('../miclib.php');	
	include('../getEmplyeeInfo.php');
	include('../maintain/inc_vars.php');
	$extfiles = 'schedule/';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../data/jobs/';
	$imgpath = '../data/jobs/';
	include("init.php"); 
	if($_REQUEST['act']=='activity') {
		$act=true;
		$aDate = $_REQUEST['day'].' 08:00'; 
		$newfvA = array('','',$aDate,$aDate,array('1'=>'活動/旅遊'),"$_SESSION[empID]");
	}
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px; vertical-align: bottom;}
 .pmClasses option[checked] {background-color:#027EF9; color:#fff}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
 <script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
 <script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
<script>var act = <?=$act?'true':'false'?>;</script>
 <title><?=$pageTitle?> - 新增</title>
</Head>

<body class="page">
<form action="schedule/doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>
	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><? switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input list='reason' type='search' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  		case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "datetime": echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; 
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datetimepicker();})</script>";
				break;	
			case "select"  : echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $v) echo "<option>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>
	
  <tr valign="top">
  	<td align="right" class="colLabel">對象</td>
  	<td>
		<? $nn = count($_SESSION[user_classdef][2]);
			if($act) { ?>
			<input type="radio" name="evtobj" value="dep" id="evtobj" checked/>部門
			<? if ($nn>0) {
				echo '<select name="pmClasses[]" id="pmClasses" style="vertical-align:top" title="按住CTRL鍵可多選" multiple>';
				foreach($_SESSION[user_classdef][2] as $v) echo "<option value='$v' selected>".$departmentinfo[$v]."</option>";
			} else {
				echo '<select name="pmClasses[]" id="pmClasses">';
				echo "<option value='$_SESSION[depID]' checked>".$departmentinfo[$_SESSION['depID']]."</option>";
			} ?>
			</select>
		<? } else { 
    	echo '<input type="radio" name="evtobj" value="self" id="evtobj" checked />只有自己';
    	if($nn>0) echo '<input type="radio" name="evtobj" value="dep" id="evtobj" />部門視野';
		} ?>	
		
    <? if($_SESSION["canSchedule"] && $classdef[2][0]) { ?>
      <input type="radio" name="evtobj" value="center" id="evtobj" />機構
      <select id="depCenter" name="depCenter">
			<? 
				$rsX = db_query("select * from permits where isCenter=1");
				while($rX = db_fetch_array($rsX)) {
					$aa = explode(';',$rX[depPM]);
					$bb = array();
					foreach($aa as $v) { $cc = explode(':',$v);	$bb[] = $cc[0];	}
					$bbstr = join(',',$bb);
					echo "<option value='$bbstr'>$rX[title]</option>"; 
				}
			?>
      </select>
    <? } ?>
    </td>
  </tr>
    
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<datalist id="reason"><? foreach($scheduleTitle as $v) echo "<option value='$v'/>";?></datalist>
			<input type="submit" value="確定新增" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
      <input type="button" value="取消新增" class="btn" onClick="<?=($act?'window.close()':'history.back()')?>">
		</td>
	</tr>
</table>
</form>
</body>
</html>