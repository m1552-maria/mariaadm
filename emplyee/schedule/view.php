<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>list.css">
 <title><?=$pageTitle?></title>
 <style>
  .popupPane {
    position:absolute; 
    left: 35%; top:25%;
    background-color:#888;
    padding: 8px;
    display:none;
  }
 </style> 
 <script>
  Accordion1.openPanel(4);
 	/*** forward php variants to javascript ***/
 	var cp = <?=$pages?>;		//目前所在頁次
	var selid = '<?=$selid?>';	
	var keyword = '<?=$keyword?>';
	var cancelFG = true;	
	
  function goPage(p) {
		var pp;
		switch(p) {
			case 0 : pp=0; break;
      case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
      case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
      case 3 : pp=<?=$pageCount?>;
		}
		location.href="<?=$self?>&pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
	}
	function DoEdit() { 
		if(selid==-1) alert('請選擇一個項目！'); 
		else location.href='index.php?incfn=schedule/edit.php&ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
	}
	function clickEdit(aid) {
		selid = aid;
		DoEdit();
	}	
 </script>
 <script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</Head>

<body>
<div id="popup" class="popupPane">
	<form id="fm" action="/maintain/schedule/importExcel.php" method="post" enctype="multipart/form-data" style="margin:0">
		<input type="hidden" name="announcer" value="<?=$_SESSION[empID]?>"/>
		<table bgcolor="#F0F0F0" style="font-size:12px">
		<tr><td>選擇要匯入的Excel檔：<br/><span style="color:#666666; font-size:12px">請選擇格式為 xls 的 excel 檔案，內容必須遵守範本的排法。</span><br/><a href="templet.xls" target="new">下載範本</a></td></tr>
		<tr><td><input type="file" name="file1" size="30" /></td></tr>
		<tr height="40" valign="bottom"><td>
				<input type="radio" name="evtobj" value="self" id="evtobj" checked />只有自己
			<? if($_SESSION["canSchedule"] && $classdef[2][0]) { ?>
				<input type="radio" name="evtobj" value="dep" id="evtobj" />部門
			<? } ?>
			<? if($classdef[5]) { ?>  
				<input type="radio" name="evtobj" value="center" id="evtobj" />機構
				<select id="depCenter" name="depCenter">
				<? 
				$rsX = db_query("select * from permits where id=$classdef[5]");
				while($rX = db_fetch_array($rsX)) {
					$aa = explode(';',$rX[depPM]);
					$bb = array();
					foreach($aa as $v) { $cc = explode(':',$v);	$bb[] = $cc[0];	}
					$bbstr = join(',',$bb);
					echo "<option value='$bbstr'>$rX[title]</option>"; 
				}
				?>
				</select>
			<? } ?>
			<br>
			<input type="submit" name="Submit" value="匯入">
			<button type="button" onClick="document.getElementById('popup').style.display='none'">取消</button>
		</td></tr>
		</table>
	</form>
</div>

<a name="top"/>
<form name="form1" method="post" action="schedule/dodelete.php" onSubmit="return Form1_Validator(this);" style="margin:0">
 <table width="100%">
  <tr><td><font class="headTX"><?=$pageTitle?></font></td></tr>
 	<tr><td colspan="2">
    <input type="hidden" name="keyword" value="">
		<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)"><input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)"><input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)"><input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
    <input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();"><input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='location.href="index.php?incfn=schedule/append.php";'><input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()"><input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
		<input type="button" name="toExcel" value="Excel報表" onClick="excelfm.submit()">
    <input type="button" name="ExcelIn" value="Excel匯入" onClick="document.getElementById('popup').style.display='block'">    
  </td></tr>
  <tr>
  	<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
  	<td align="right"><? $ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; if ($keyword!="") $ss = "搜尋：$keyword ".$ss ?><label class="sText"><?=$ss?></label></td>
  </tr>
 </table>
  
 <table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
	<!--## 表頭  ##-->
  <tr class="colHD">
	<? for ($i=0; $i<count($fnAry); $i++) { ?>
	<th>
		<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=$sortFG?>","<?=$keyword?>");' class="head">
		<?=$ftAry[$i]?>&nbsp;&nbsp;<? if ($fieldname==$fnAry[$i]) { ?><font class="bulDir"><? if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?></font><? } ?>
		</a>
	</th>
	<? } ?>
  <th>&nbsp;</th>
  </tr>
<?
	$sql="select * from $tableName where announcer='$_SESSION[empID]'";
  // 搜尋處理
  if ($keyword!="") $sql.=" and ($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%')";
	// for excel output use.
	$outsql = $sql; //echo $outsql;
  // 排序處理
  if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
	//分頁
  $sql .= " limit ".$pageSize*$pages.",$pageSize";  
	//echo $sql; //exit;
  $rs = db_query($sql,$conn);
	while ($r=db_fetch_array($rs)) { $id = $r[$fnAry[0]];
?>
  <tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<? if($id==$selid) echo ' class="onSel"'?>>
    <td <? if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <? if ($nextfg) echo "checked" ?>><a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a></td>
		<? for($i=1; $i<count($fnAry); $i++) { ?>
    	<td <? if ($flAry[$i]>"") echo "width='$flAry[$i]'" ?>>
			<? $fldValue = $r[$fnAry[$i]] ?>
			<? if ($fldValue>"") switch ($ftyAy[$i]) {
						case "image" : echo "<img src='$imgpath$fldValue' width='$flAry[$i]'/>"; break;
						case "link" : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
						case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
						case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
						case "bool" : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
						case "datetime" : echo date('Y/m/d H:i',strtotime($fldValue)); break;
						case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
						default : echo $fldValue;	
			    } else echo "&nbsp";
			?>
			</td>
		<? } ?>	
		<td><?
			if( strpos($r[ClassID],'活動') !== false ) {
				//check 投保數量
				$rsT = db_query("select count(*) from insure_travel where sid=$id");
				$rT = db_fetch_array($rsT);
				echo	"<a href='/maintain/insurances/index.php?aType=insure_travel&sid=$id' target='new'>申請投保旅平險 (".$rT[0]."保單)</a>"; 
			} else echo "&nbsp;"; 
		?></tr>    
<? } ?>   
 </table>
</form>
	<div align="right"><a href="#top">Top ↑</a></div>
	<form name="excelfm" action="/maintain/schedule/excel.php" method="post" target="excel">
	<input type="hidden" name="title" value="<?=$_REQUEST[title]?>">
	<input type="hidden" name="sqltx" value="<?=$outsql?>">
</form>
</body>
</Html>