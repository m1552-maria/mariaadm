<?	
	$bE = true;	//異動權限
	$pageTitle = "行事曆維護";
	$tableName = "schedule";

	// for Upload files and Delete Record use
	$ulpath = '../../data/schedule/';
	$delField = 'ahead';
	$delFlag = true;
	$imgpath = '../../data/schedule/';
	$ClassID = $scheduleType;
	
	// Here for List.asp ======================================= //
	$defaultOrder = "ID";
	$searchField1 = "ID";
	$searchField2 = "title";
	$pageSize = 5;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,title,Content,beginDate,endDate,ClassID");
	$ftAry = explode(',',"編號,簡述,內容,時間起,時間迄,類別");
	$flAry = explode(',',"70,150,,65,65,");
	$ftyAy = explode(',',"ID,text,text,datetime,datetime,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"title,Content,beginDate,endDate,ClassID,announcer");
	$newftA = explode(',',"簡述,內容,時間起,時間迄,類別,發布者");
	$newflA = explode(',',"60,60,,,,");
	$newfhA = explode(',',",6,,,");
	$newfvA = array('','','','',$ClassID,"$_SESSION[empID]");
	$newetA = explode(',',"text,textarea,datetime,datetime,select,hidden");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,title,Content,beginDate,endDate,ClassID");
	$editftA = explode(',',"編號,簡述,內容,時間起,時間迄,類別");
	$editflA = explode(',',",60,60,,,");
	$editfhA = explode(',',",,6,,,");
	$editfvA = array('','','','','',$ClassID);
	$editetA = explode(',',"text,text,textarea,datetime,datetime,select");
?>