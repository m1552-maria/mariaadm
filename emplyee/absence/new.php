<?
	include '../config.php';
	include '../maintain/inc_vars.php';	
	include '../getEmplyeeInfo.php';	
	include 'func.php';	
	$empID = $_SESSION['empID'];
	$depID = $_SESSION["depID"];

	$cDate = strtotime(date('Y-m-d'));
	if(!empty($_POST['bDate'])) {
		$cDate = strtotime($_POST['bDate']);
		/*echo "<pre>";
		print_r($_POST);
		exit;*/
	}


	//計算特休時數
	//$r=db_fetch_array(db_query("select spHolidays from emplyee where empID='$empID'"));
	/*20180410 修改特修取得時數 改抓 emplyee_annual資料表的值*/
	$r=db_fetch_array(db_query("select hours, userYear, lastHours,nHours from emplyee_annual where year='".date('Y', $cDate)."' and empID='$empID'"));

	$spHolidays = $r[0]?$r[0]:0;
	$canSpHolidaysDate = date('Y', $cDate).'/01/01';

	$userYear = $r['userYear'];
	$yList = array('0.5','1','0.5&1');
	if(in_array($userYear, $yList)) {
		include_once($root.'/maintain/emplyee_annual/emplyeeAnnualApi.php');
		$r2=db_fetch_array(db_query("select hireDate from emplyee where empID='$empID'"));
		$now_date = date('Y-m-d', $cDate);
		$s = (strtotime($now_date) - strtotime($r2['hireDate'])+24*60*60);//會相差一天

		$addDay = 0;
		$s = $s / 31536000;
		switch ($userYear) {
			case '0.5':
				$addDay += 182;
				$canSpHolidaysDate = date('Y/m/d', strtotime("+".$addDay." day",strtotime($r2['hireDate'])));
				break;
			case '1':
				$addDay += 365;
				$canSpHolidaysDate = date('Y/m/d', strtotime("+".$addDay." day",strtotime($r2['hireDate'])));
				break;
			case '0.5&1':
				$addDay += 182;
				$canSpHolidaysDate = date('Y/m/d', strtotime("+".$addDay." day",strtotime($r2['hireDate'])));
				if($s <= 1) {
					$sixM = $spHolidays/($holidayList['0.5']+$holidayList['1'])*$holidayList['0.5']; //滿半年可休時數
					$spHolidays = floor($sixM); 
				}			
				break;
		}

		$data = array('wkDayE'=>$canSpHolidaysDate,'empID'=>$empID);
		$stopJop =  calculateStopJop($data);
		$addDay = 0;
		if(!empty($stopJop[$empID]['stopDay'])) {
			$s = ($s - ($stopJop[$empID]['stopDay']*24*60*60));
			$addDay = $stopJop[$empID]['stopDay'];
		}

		if($addDay > 0) {
			$canSpHolidaysDate = date('Y/m/d', strtotime("+".$addDay." day",strtotime($canSpHolidaysDate)));
		}

	}

	$useSpHolidays = $spHolidays;

	$lastHours = $r['lastHours'];
	if($lastHours > 0 && $r['nHours'] < $r['hours']) {
		$sql = "SELECT sum(hours) as thours FROM `absence` ";
		$sql.="where empID='$empID' and ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and YEAR(bDate)=".date('Y', $cDate);
		$sql.=" and aType='特休' ";
		$rs = db_query($sql);
		$r = db_fetch_array($rs);
		if(!$r['thours'] || $r['thours'] < $lastHours) {
			$useSpHolidays = $lastHours;
			$canSpHolidaysDate = date('Y', $cDate).'/01/01';
		}
	}


	$sql = "SELECT sum(hours) as thours,aType FROM `absence` ";
	$sql.="where empID='$empID' and ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and YEAR(bDate)=".date('Y', $cDate);
	$sql.=" and aType!='補休' ";
	$sql.="group by aType";
	/*echo $sql;
	exit;*/
	$rs = db_query($sql);
	$avHours = get_rest_Hours($cDate,$empID);

	//計算已請補休時數
	$dateB = date("Y-01-01", $cDate);
	$dateE = date("Y-12-31 23:59:59", $cDate);
	$sql = "SELECT sum(hours) as thours,aType FROM `absence` ";
	$sql.="where empID='$empID' and ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and bDate between '$dateB' and '$dateE' ";
	$sql.="and aType='補休'";
	$av_rs = db_query($sql);
	$av_r=db_fetch_array($av_rs);
?><head>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<link href="/css/mapping.css" rel="stylesheet" type="text/css" />
<style type='text/css'>
.opt{	width:16px;	height:16px;}

#d_Content{
	max-height: 300px;
	overflow: auto;
	background:#FFEFD7;
}
</style>
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script>
var useSpHolidays = <?=($useSpHolidays?$useSpHolidays:0)?>;
var usedHour = 0;			//預設已請特休時數
Accordion1.openPanel(6);
var avHours=<?=$avHours?>;	//可補休時數
$(document).ready(function() {
	$("#detail").draggable();
	$("#prjform").keypress(checkFormEnter);
});
function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
		+ "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	var bDate = tfm.bDate.value;
	var eDate = tfm.eDate.value;
	if(!bDate) { alert('起始日期 不能為空白！'); tfm.bDate.focus(); return false; }
	if(!eDate) { alert('結束日期 不能為空白！'); tfm.eDate.focus(); return false; }	
	if(!DateTimeValidate(bDate)) {alert('起始日期 格式錯誤！'); tfm.bDate.focus(); return false; }
	if(!DateTimeValidate(eDate)) {alert('結束日期 格式錯誤！'); tfm.eDate.focus(); return false; }
	
	var bDateA = bDate.split('/');
	var eDateA = eDate.split('/');
	if( (bDateA[0]!=eDateA[0]) || (bDateA[1]!=eDateA[1]) ) {alert('請勿跨月請假，若有此需要請分單'); return false; }
	if( bDateA[2]>eDateA[2] ) {alert('結束日期 不能早於 起始日期'); return false; }
	if(!tfm.content.value) {alert('事由 不能為空白！'); tfm.content.focus(); return false;}
	if(!NumValidate(tfm.hours.value)) { tfm.hours.focus(); return false; }
	if(tfm.hours.value>0){
		var r =  /^[0-9]*[1-9][0-9]*$/　　//正整數    
		if(!r.test((tfm.hours.value/0.5).toString())){	
			alert("時數請以0.5為單位");
			return false;
		}
	}
	if(tfm.aType[tfm.aType.selectedIndex].value=='特休') {
		/*20180411 sean add*/
		if((Date.parse("<?=$canSpHolidaysDate;?>")).valueOf() > (Date.parse(bDate)).valueOf() || (Date.parse("<?=$canSpHolidaysDate;?>")).valueOf() > (Date.parse(eDate)).valueOf() ) {
			alert('請假日期要大於等於特休開始日期');
			return false;
		}
		 var sum = usedHour+ parseFloat(tfm.hours.value);
		 if(sum>useSpHolidays) {
<?php if($useSpHolidays == $spHolidays) { ?>
		 	alert('特休時數已超過'); return false;
<?php } else { ?>
			alert('超過去年特休延休時數,請起兩單'); return false;
<?php } ?> 
		 }
	 }
	if(tfm.aType[tfm.aType.selectedIndex].value=='補休') {
		 var sum = parseFloat(tfm.hours.value);
		 if(sum>parseFloat(avHours)) { 
		 	alert("補休時數剩餘"+avHours+"小時，請調整請假時數"); 
			document.getElementById('hours').value='';
			document.getElementById('hours').focus();
		 	return false; 
		 }
	 }	 
	 
  	var rzt = false;
	$(tfm['agent[]']).each(function(){
		if(!this.value) {	
			alert('請選擇代理人'); rzt = false;	
		} if(this.value==prjform.empID[0].value) {
			alert('自己不能為代理人');
			rzt = false;
		} else rzt = checkEmpID(this.value);
	});
	if(!rzt) return false;
	$(tfm['leadmen']).each(function(){if(!this.value) {	
			alert('請選擇主管'); 
			rzt = false;
		} else {
			if(checkEmpID(this.value)) {
				if(this.value=='<?=$empID?>') {
					alert('核准主管不能為請假人'); 
					rzt = false;
				} else rzt = true;
			} else rzt = false;
		}
	});

	//判斷檔名
	if($('#file').val()!=''){
		var ext = $('#file').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['doc','docx','png','jpg','jpeg','pdf']) == -1) {
		    alert('請上傳正確的檔案!');
		    rzt = false;
		}	
	}

	if(rzt) {
		$('#prjform').attr('action','absence/newDo.php');
	}

	return rzt;
}

$(function(){
	$.get('/getdbInfo.php',{'act':'dep','depID':'<?=$depID?>'},function(data){ 
		if(data) {
			var aa = data.split(',');
			var elm = document.getElementsByName('leadmen')[0];
			elm.value = aa[2];
			$(elm).next().next().text(aa[3]); 
		}
	});

});

function setOverTime(){//加班單自動沖銷
	if(prjform.aType.value!='補休')	return;
	var hours=parseFloat(document.getElementById('hours').value);
	if(hours=='' || hours==0)	return;
	if(hours>0){
		var r =  /^[0-9]*[1-9][0-9]*$/　　//正整數    
		if(!r.test((hours/0.5).toString())){	
			alert("時數請以0.5為單位");
			document.getElementById('hours').value='';
			document.getElementById('hours').focus();
			return;
		}
	}
	if(hours>parseFloat(avHours)){
		alert("補休時數剩餘"+avHours+"小時，請調整請假時數");
		document.getElementById('hours').value='';
		document.getElementById('hours').focus();
		return;
	}
}
function getHours(){//取得可補休時數
	//if($('#aType :selected').text()!='補休') return;
	if($("[name='bDate']").val()=="")	return;
	$.ajax({
		url: 'absence/api.php?act=getHours&empID=<?=$empID?>&bDate='+$("[name='bDate']").val(),
		type:"POST",
		dataType:'text',
		success: function(data){
			avHours=parseFloat(data);
			$("#s_avHours").html(avHours);
		},
		error:function(xhr, ajaxOptions, thrownError){  }
	});
}
function showMapping(empID){
	var dateB,dateE;
	if($("#dateB").val()){
		dateB=$("#dateB").val();
		dateE=$("#dateE").val();
	}else{	dateB='';	dateE='';}

	$.ajax({
		url: '../maintain/absence/tuning.php?act=getMapping&dateB='+dateB+'&dateE='+dateE+'&empID='+empID,
		type:"POST",
		dataType:'text',
		success: function(data){
			$('#detail').html(data);
			showDetail();
		},
		error:function(xhr, ajaxOptions, thrownError){  }
	});
	
}
function showDetail(){
	detail.style.display='block';
}
function closePnl(){
	detail.style.display='none';
}
</script>
</head>

<table width="170" border="1" bordercolor="#f0f0f0" bordercolordark="#FFFFFF" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana; color:#999" align="right">
	<tr><th colspan="2" bgcolor="#f0f0f0"><?=date('Y', $cDate)?>年-請假時數統計</th></tr>
  <tr><td align="right">特休總時數</td><td>&nbsp;<?=$spHolidays?> 小時</td></tr>
<?php if($useSpHolidays != $spHolidays) {?>
	<tr><td align="right">特休延休時數</td><td>&nbsp;<?=$useSpHolidays?> 小時</td></tr>
<?php } ?>
  <tr><td align="right">特休開始日期</td><td>&nbsp;<?=$canSpHolidaysDate;?></td></tr>
<? 
	if(!db_eof($rs)){
		while ($r=db_fetch_array($rs)) { 
			if($r['aType']=='特休') {	echo "<script>usedHour=$r[thours]</script>"; $color = ($spHolidays-$r['thours']<0 ? '#f00':'#999'); } 
   		echo "<tr><td align='right'>已請 $r[aType]</td><td style='color:$color'>&nbsp;$r[thours] 小時</td></tr>";
		}
	} else echo "<tr><td align='right'>已請休</td><td>&nbsp;0 小時</td></tr>";
?>
	<tr><td align="right">已請補休</td><td>&nbsp;<?=$av_r['thours']?$av_r['thours']:0 ?> 小時</td></tr>
	<tr><td align="right">可補休時數</td><td >&nbsp;<span id='s_avHours'><?=$avHours?></span>小時</td></tr>
</table>
<table width="600" cellpadding="0" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
<tr><th bgcolor="#e0e0e0">請假須知</th></tr>
<tr><td bgcolor="#f8f8f8"><div style="overflow:auto; height:100px; width:100%;"><?= nl2br(file_get_contents('../data/absence.txt'))?></div></td></tr>
</table>
<div style="height:10px"></div>
<div style="border-top:1px #999999 dashed; height=1px;"></div>
<div style="height:5px"></div>
<form id="prjform" action="/emplyee/index.php?incfn=absence/new.php" method="post" onsubmit="return formCheck(this)" enctype="multipart/form-data">
<table width="100%" border="0" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td width="110" align="right">請假人：</td>
    <td colspan="1">
		<?=$_SESSION["empName"]?><input name="empID" type="hidden" id="empID" value="<?=$empID?>"/>
    <input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
    </td>
    <td colspan="2" style="color: red;"> </td>
  </tr>
  <tr>
    <td align="right">假別：</td>
    <td><select name="aType" id="aType" onchange="getHours()">
		<? foreach($absType as $v) { if($_REQUEST['aType']==$v) 
			echo "<option selected='selected'>$v</option>"; 
			else echo "<option>$v</option>"; } ?>
    	</select>
    <?if($avHours > 0) {	?>
    	<span style="color:red;">尚有補休時數: <?=$avHours;?>小時</span>
    <? } ?>
    </td>
    <td width="70" align="right">請假時數：</td>
    <td width="150" align="left"><input name="hours" type="text" id="hours" size="6" value="<?=$_REQUEST['hours']?>" onblur="setOverTime()"/>&nbsp;<input type='button' value='補休明細'  onclick='showMapping("<?=$empID?>")'/></td>
  </tr>
  <tr>
    <td align="right">起始日期：</td>
    <td><input type="text" name="bDate" id="bDate" value="<?=$_REQUEST['bDate']?>" onchange="getHours()"/>
    <script>
    	$(function(){
    		var bDate = '<?=$_REQUEST['bDate']?>';
    		$('#bDate').datetimepicker({
    			onClose: function(dateText, inst) {
    				if(bDate != $(this).val()) $('#prjform').submit();
			    }

    		});
    	})

	</script></td>
    <td align="right">結束日期：</td>
    <td align="left"><input type="text" name="eDate" id="eDate" value="<?=$_REQUEST['eDate']?>"/>
    	<script>
    		$(function(){
    			$('#eDate').click(function(){
    				if($('#eDate').val()==''){
    					$('#eDate').val($('#bDate').val());//先塞入值//因為datetimepicker的預設值太難設也沒辦法塞之後要給的值
    					$('#eDate').blur();//解綁
    				}
    				$('#eDate').datetimepicker();
    			}) 
    		})
    	</script>
    </td>
  </tr>
	  
  <tr>
    <td align="right">事由：</td>
    <td colspan="3"><textarea name="content" id="content" cols="55" rows="3"><?=$_REQUEST['content']?></textarea></td>
  </tr>
  <tr>
    <td align="right">交辦事項：</td>
    <td colspan="3"><textarea name="notes" id="notes" cols="55" rows="3"><?=$_REQUEST['notes']?></textarea></td>
  </tr>
  <tr>
    <td align="right">文件上傳：</td>
    <td colspan="3"><input type="file" name="file" id="file" accept="image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf"></td>
  </tr>
  <tr>
  	<td></td>
  	<td colspan="3" style="color:red;">請上傳副檔名格式為jpg、jpeg、png、doc、docx、pdf的檔案</td>
  </tr>
  
	<tr>
    <td align="right">代理人/小組長：</td>
    <td><input name="agent[]" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)"/><span class="sBtn" onclick="addItem(this,'agent','empID')"> +新增其它人員</span></td>
    <td align="right" valign="top">主管：</td>
    <td align="left" valign="top"><input name="leadmen" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)"/><span class='formTxS'></span></td>
  </tr>

  <tr>
    <td align="right">&nbsp;</td>
    <td colspan="3">
    	<input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>

</form>

<div id='detail'></div>