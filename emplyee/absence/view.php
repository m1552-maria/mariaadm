<?
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../../maintain/inc_vars.php';	
	$id = $_REQUEST[id];
	$sql = "select * from absence where id=$id";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>表單列印</title>
<style>
.pageTitle {
	font-family: "微軟正黑體", Verdana;
	font-size: 18px;
	font-weight: bold;
	padding-bottom: 12px;
}

</style>
<script type="text/javascript" src="/Scripts/jquery-1.12.3.min.js"></script>
<script type="text/javascript">
  
function formCheck(tfm) {
  var rzt = true;
  //判斷檔名
  if($('#file').val()!=''){
    var ext = $('#file').val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['doc','docx','png','jpg','jpeg','pdf']) == -1) {
        alert('請上傳正確的檔案!');
        rzt = false;
    } 
  }
  return rzt;
}

</script>
</head>

<body>
<form id="prjform" action="viewDo.php" method="post" onsubmit="return formCheck(this)" enctype="multipart/form-data">
<p class="pageTitle" align="center">請假單</p>	
<table width="92%" border="1" align="center" cellpadding="10" cellspacing="0" style="font:normal 15px '微軟正黑體',Verdana">
  <tr>
    <td width="100" align="right">請假人</td>
    <td><?=$emplyeeinfo[$r[empID]]?></td>
    <td width="100" align="right">假別</td>
    <td><?=$r["aType"]?></td>
  </tr>
  <tr>
    <td align="right">請假期間</td><td><?= date('Y-m-d H:i',strtotime($r[bDate]))?> ~ <?=date('Y-m-d H:i',strtotime($r[eDate]))?></td>
    <td align="right">請假時數</td>
    <td><?=$r["hours"]?> 小時</td>
  </tr>
	  
  <tr>
    <td align="right">事由</td>
    <td colspan="3"><?=$r[content]?></td>
  </tr>
  <tr>
    <td align="right">交辦事項</td>
    <td colspan="3"><?=$r[notes]?></td>
  </tr>
  
	<tr>
	  <td align="right">代理人</td>
	  <td><? $aa=explode(',',$r[agent]);foreach($aa as $v) echo $emplyeeinfo[$v].' &nbsp; ';?></td>
	  <td align="right">申請時間</td><td><?= date('Y-m-d H:i',strtotime($r[rdate]))?></td>
  </tr>
	<tr>
    <td align="right">主管</td>
    <td><?=$emplyeeinfo[$r[leadmen]]?></td>
    <td align="right">是否准假</td>
    <td>
      <? 
        if($r['isOK']) echo '已准假 '.date('Y-m-d',strtotime($r[is_leadmen_date]));
        elseif($r['isOK']==0 && $r['is_leadmen']!=0) echo '不准假 ';
      ?>
        
    </td>
  </tr>
	<tr>
	  <td align="right">辦理情形</td>
	  <td colspan="3"><?=$r['rspContent']?></td>
  </tr>
  <? if($r['isReqD']) { ?>
  <tr>
	  <td align="right">是否銷假</td>
	  <td colspan="3"><? if(intval($r['isDel'])==1) echo "已銷假"; else echo "不准銷假" ?></td>
  </tr>
  <? } ?>

  <?
    echo '<tr>
              <input type="hidden" name="id" value="'.$id.'">
              <td align="right">文件</td><td colspan="3"><input type="file" name="file" id="file" accept="image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf">
              <p style="color:red;margin:0;">*請上傳副檔名格式為jpg、jpeg、png、doc、docx、pdf的檔案</p>
              ';
    $ulpath = '../../data/absence/'.$id.'/';
    if(file_exists($ulpath)){
      $dh  = opendir($ulpath);
      while (false !== ($filename = readdir($dh))) {
        if ($filename=='.') continue;
        if ($filename=='..') continue; 
        $file_path = '../../data/absence/'.$id.'/'.$filename;  
        echo '
              <a target="_blank" href="'.$file_path.'">'.$filename.'</a>
            ';
      }
    }
    
    echo '</td></tr>';
  ?>
  
  <tr><td colspan="4" style="text-align: center;"><input type="submit" name="" value="送出" ></td></tr>
</table>

</form>
</body>
</html>