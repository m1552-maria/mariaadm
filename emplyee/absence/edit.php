<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	include '../maintain/inc_vars.php';	
	include 'func.php';	

	$empID = $_SESSION['empID'];
	$depID = $_SESSION["depID"];

	//計算特休時數
	//$r=db_fetch_array(db_query("select spHolidays from emplyee where empID='$empID'"));
	/*20180410 修改特修取得時數 改抓 emplyee_annual資料表的值*/
	$r=db_fetch_array(db_query("select hours, userYear from emplyee_annual where year='".date('Y')."' and empID='$empID'"));

	$spHolidays = $r[0]?$r[0]:0;
	$canSpHolidays = 1; //是否可請特休
	$canSpHolidaysDate = date('Y').'/01/01';
	$userYear = $r['userYear'];
	$yList = array('0.5','1','0.5&1');

	if(in_array($userYear, $yList)) {
		include_once($root.'/maintain/emplyee_annual/emplyeeAnnualApi.php');
		$data = array('wkDayE'=>date('Y').'-12-31','empID'=>$empID);
		$stopJop =  calculateStopJop($data);

		$r2=db_fetch_array(db_query("select hireDate from emplyee where empID='$empID'"));
		$now_date = date('Y-m-d');
		$s = (strtotime($now_date) - strtotime($r2['hireDate'])+24*60*60);//會相差一天

		$addDay = 0;
		if(!empty($stopJop[$empID]['stopDay'])) {
			$s = ($s - ($stopJop[$empID]['stopDay']*24*60*60));
			$addDay = $stopJop[$empID]['stopDay'];
		}
		$s = $s / 31536000;
		switch ($userYear) {
			case '0.5':
				$addDay += 182;
				$canSpHolidaysDate = date('Y/m/d', strtotime("+".$addDay." day",strtotime($r2['hireDate'])));
				if($s <= 0.5) $canSpHolidays = 0;
				break;
			case '1':
				$addDay += 365;
				$canSpHolidaysDate = date('Y/m/d', strtotime("+".$addDay." day",strtotime($r2['hireDate'])));
				if($s <= 1) $canSpHolidays = 0;
				break;
			case '0.5&1':
				$addDay += 182;
				$canSpHolidaysDate = date('Y/m/d', strtotime("+".$addDay." day",strtotime($r2['hireDate'])));
				if($s <= 1) {
					$sixM = $spHolidays/($holidayList['0.5']+$holidayList['1'])*$holidayList['0.5']; //滿半年可休時數
					$spHolidays = floor($sixM); 
				}
				if($s <= 0.5) $canSpHolidays = 0;
				break;
		}
		
	}



	$sql = "SELECT sum(hours) as thours,aType FROM `absence` where empID='$empID' and ((isOK=0 and is_leadmen=0) or isOK=1) and isDel<>1 and YEAR(bDate)=YEAR(NOW()) group by aType";

	$rs = db_query($sql);
	$usedHour=0;
	while ($r=db_fetch_array($rs)) {
	  if($r['aType']=='特休') $usedHour=$r[thours];
	}

	$sql = "select * from absence where id=$_REQUEST[id]";
	$rs = db_query($sql);
	if($rs) $r=db_fetch_array($rs);
	$thisHours=0;
	if($r[aType]=="補休")	$thisHours=$r[hours];
	$avHours = get_rest_Hours(strtotime($r[bDate]),$empID);
	$avHours+=floatval($thisHours);
?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<style type='text/css'>

</style>
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
var canSpHolidays = <?=$canSpHolidays;?>;
var spHolidays = <?=($spHolidays?$spHolidays:0)?>;
var avHours = <?=$avHours?>;
var usedHour = <?=$usedHour?>;	//已請特休時數
var thisHours=<?=$thisHours?>;	//此筆請假單時數
Accordion1.openPanel(6);

function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}

function formCheck(tfm) {
	var bDate = tfm.bDate.value;
	var eDate = tfm.eDate.value;
	if(!bDate) { alert('起始日期 不能為空白！'); tfm.bDate.focus(); return false; }
	if(!eDate) { alert('結束日期 不能為空白！'); tfm.eDate.focus(); return false; }

	if(!DateTimeValidate(bDate)) {alert('起始日期 格式錯誤！'); tfm.bDate.focus(); return false; }
	if(!DateTimeValidate(eDate)) {alert('結束日期 格式錯誤！'); tfm.eDate.focus(); return false; }

	var bDateA = bDate.split('/');
	var eDateA = eDate.split('/');
	if( (bDateA[0]!=eDateA[0]) || (bDateA[1]!=eDateA[1]) ) {alert('請勿跨月請假，若有此需要請分單'); return false; }
	if( bDateA[2]>eDateA[2] ) {alert('結束日期 不能早於 起始日期'); return false; }
	if(!tfm.content.value) {alert('事由 不能為空白！'); tfm.content.focus(); return false;}

	if(!NumValidate(tfm.hours.value)) { tfm.hours.focus(); return false; }

	if(tfm.hours.value>0){
		var r =  /^[0-9]*[1-9][0-9]*$/　　//正整數    
		if(!r.test((tfm.hours.value/0.5).toString())){	
			alert("時數請以0.5為單位");
			return false;
		}
	}

	if(tfm.aType[tfm.aType.selectedIndex].value=='特休') {
		/*20180411 sean add*/
		/*if(canSpHolidays == 0) {
			alert('還未到特休開始日期');
			return false;
		}*/

		if((Date.parse("<?=$canSpHolidaysDate;?>")).valueOf() > (Date.parse(bDate)).valueOf() || (Date.parse("<?=$canSpHolidaysDate;?>")).valueOf() > (Date.parse(eDate)).valueOf() ) {
			alert('請假日期要大於等於特休開始日期');
			return false;
		}

		 var sum = usedHour+ parseFloat(tfm.hours.value);

		 if(sum>spHolidays) {alert('特休時數已超過'); return false; }
	}

	if(tfm.aType[tfm.aType.selectedIndex].value=='補休') {
		var sum = parseFloat(tfm.hours.value);
		if(sum>parseFloat(avHours)) {
			alert('補休時數剩餘'+avHours+'小時，請調整請假時數'); 
			document.getElementById('hours').value='';
			document.getElementById('hours').focus();
			return false; 
		}
	}	
	///setOverTime();

  var rzt = true;
	$(tfm['agent[]']).each(function(){
		if(!this.value) {alert('請選擇代理人'); rzt = false;}
		if(this.value==prjform.empID[0].value) {alert('自己不能為代理人'); rzt = false;}
	});
	$(tfm['leadmen']).each(function(){if(!this.value) {alert('請選擇主管'); rzt = false;}});

	//判斷檔名
	if($('#file').val()!=''){
		var ext = $('#file').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['doc','docx','png','jpg','jpeg','pdf']) == -1) {
		    alert('請上傳正確的檔案!');
		    rzt = false;
		}	
	}
	
	return rzt;
}

function setOverTime(){//加班單自動沖銷
	if(prjform.aType.value!='補休')	return;
	var hours=parseFloat(document.getElementById('hours').value);
	if(hours=='' || hours==0)	return;
	if(hours>0){
		var r =  /^[0-9]*[1-9][0-9]*$/　　//正整數    
		if(!r.test((hours/0.5).toString())){	
			alert("時數請以0.5為單位");
			document.getElementById('hours').value='';
			document.getElementById('hours').focus();
			return;
		}
	}
	if(parseFloat(avHours)<hours){
		alert('補休時數剩餘'+avHours+'小時，請調整請假時數');
		document.getElementById('hours').value='';
		document.getElementById('hours').focus();
		return;
	}
}

function getHours(){//取得可補休時數
	if($("[name='bDate']").val()=="")	return;
	$.ajax({
		url: 'absence/api.php?act=getHours&empID=<?=$empID?>&bDate='+$("[name='bDate']").val(),
		type:"POST",
		dataType:'text',
		success: function(data){
			avHours=parseFloat(data)+parseFloat(thisHours);
		},
		error:function(xhr, ajaxOptions, thrownError){  }
	});
}

$(function(){ $("#prjform").keypress(checkFormEnter); });
</script>
<form id="prjform" action="absence/editDo.php" method="post" onsubmit="return formCheck(this)" enctype="multipart/form-data">
<table width="100%" border="0" cellpadding="8" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td align="right">請假人：</td>
    <td colspan="3">
		<?=$_SESSION["empName"]?><input name="empID" type="hidden" id="empID" value="<?=$empID?>"/>
    <input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
    <input name="id" type="hidden" id="id" value="<?=$r[id]?>"/>
    </td>
  </tr>
  <tr>
    <td align="right">假別：</td>
    <td><select name="aType" id="aType" onchange="getHours()"><? foreach($absType as $v) {$ck=($r[aType]==$v)?'selected':''; echo "<option $ck>$v</option>"; } ?></select>
    </td>
    <td align="right">請假時數：</td>
    <td><input name="hours" type="text" id="hours" size="6" value="<?=$r[hours]?>" onblur="setOverTime()"/></td>
  </tr>
  <tr>
    <td align="right">起始日期：</td>
    <td><input type="text" name="bDate" id="bDate" onchange="getHours()" value="<?=date('Y/m/d H:i',strtotime($r[bDate]))?>"/><script>$(function(){$('#bDate').datetimepicker({hour:<?=date('H',strtotime($r[bDate]))?>,minute:<?=date('i',strtotime($r[bDate]))?>})})</script></td>
    <td align="right">結束日期：</td>
    <td><input type="text" name="eDate" id="eDate" value="<?=date('Y/m/d H:i',strtotime($r[eDate]))?>"/><script>$(function(){$('#eDate').datetimepicker({hour:<?=date('H',strtotime($r[eDate]))?>,minute:<?=date('i',strtotime($r[eDate]))?>})})</script></td>
  </tr>
	  
  <tr>
    <td align="right">是由：</td>
    <td colspan="3"><textarea name="content" id="content" cols="60" rows="3"><?=$r[content]?></textarea></td>
  </tr>
  <tr>
    <td align="right">交辦事項：</td>
    <td colspan="3"><textarea name="notes" id="notes" cols="60" rows="3"><?=$r[notes]?></textarea></td>
  </tr>
  
	<tr>
		<td align="right">文件上傳：</td>
		<td colspan="3"><input type="file" name="file" id="file" accept="image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf"></td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3" style="color:red;">請上傳副檔名格式為jpg、jpeg、png、doc、docx、pdf的檔案</td>
	</tr>
	
	<tr>
    <td align="right">代理人：</td>
    <td>
    <? $aa = explode(',',$r[agent]); ?>
  	<input name="agent[]" type="text" class="queryID" id="empID" size="6" value="<?=$aa[0]?>" title="<?=$emplyeeinfo[$aa[0]]?>" onkeypress="return checkInput(event,'empName',this)"/> 
    <span class="sBtn" onclick="addItem(this,'agent','empID')"> +新增其它人員(如第二代理人或小組長)</span>
    <? for($i=1; $i<count($aa); $i++) {
			 echo "<div><input name='agent[]' id='empID' type='text' size='6' value='$aa[$i]'>"
			 . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$emplyeeinfo[$aa[$i]]."</span>"
			 . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		} ?> 
    
    </td>
    <td align="right">主管</td>
    <td><input name="leadmen" type="text" class="queryID" id="empID" size="6" value="<?=$r[leadmen]?>" title="<?=$emplyeeinfo[$r[leadmen]]?>" onkeypress="return checkInput(event,'empName',this)"/></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td colspan="3">
      <input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" />
    </td>
  </tr>
</table>
</form>
<script type="application/javascript">
var avHours = <?=$avHours?>;
</script>