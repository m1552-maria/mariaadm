<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	$self = $_SERVER['PHP_SELF']."?incfn=absence/my.php";
	$sql = "select *,a.is_leadmen as is_leadmen,a.rdate as rdate,a.id as id,a.leadmen as leadmen,a.content as content, ";
  $sql.="ad.is_leadmen as is_leadmen2 ";
  $sql.="from absence as a left join absence_del as ad ";
  $sql.="on a.id=ad.fkey ";
  $sql.="where a.empID='$_SESSION[empID]' ";
	$sqc = "select count(ID) from absence where empID='$_SESSION[empID]'";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 10;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數
	$sql .= " order by a.rdate Desc limit ".$page*$pageln.",$pageln";
  //echo $sql;
	$rs = db_query($sql);	
?>
<script>
	Accordion1.openPanel(6);
	function stateitgform(id,men) {
		statefm.id.value = id;
		statefm.leadmen.value = men;
		$('#div1').show();
	}
</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<style>
#div1 {
	position:absolute; 
	left: 40%; top:30%;
	background-color:#888;
	padding: 20px;
	display:none;
}
td>img { vertical-align:middle; padding-top:1px; padding-bottom:3px; }
</style>
<div id="div1">
  <form id="statefm" action="absence/req.php">
  	<table bgcolor="#FFFFFF" cellpadding="4">
    	<tr><td align="right">單號：</td><td><input type="text" name="id" /></td></tr>
      <tr><td align="right">說明：</td><td><textarea name="content" rows="5" cols="30"></textarea></td></tr>
      <tr><td colspan="2" align="center">
      	<input type="hidden" name="leadmen" />
        <input type="submit" name="Submit" value="確定" />
        <button type="button" onclick="$('#div1').hide()">取消</button>
      </td></tr>
    </table>  
  </form>
</div>

<div class="pageTitle">我的請假單</div>	
<table class="sTable" width="100%" cellpadding="4" cellspacing="0">
<tr class="hdCell">
	<td width="70">請假日期</td><td width="70">起始日期</td><td width="70">結束日期</td><td width="35">時數</td><td>事由</td>
  <td bgcolor="#E6E6FF" width="43">代理</td><td width="30" bgcolor="#E6E6FF">簽核</td>
  <td bgcolor="#FFEEFF" width="42">主管</td><td width="30" bgcolor="#FFEEFF">簽核</td><td width="30" bgcolor="#FFEEFF">准假</td>
  <td bgcolor="#FFFFDF" width="80">辦理情形</td><td width="85" align="center" bgcolor="#E8E8D0">維護</td>
</tr>
<? while($rs and $r=db_fetch_array($rs)) { ?>
  <tr valign="top">
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['rdate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['bDate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['eDate'])) ?></td>
    <td class="cell" align="center"><?=$r['hours']?></td>
    <td class="cell"><? echo "(".$r['aType'].")".$r['content']; ?></td>
    <td class="cell"><?	$ct=0; foreach(split(',',$r[agent]) as $v) {echo $emplyeeinfo[$v].'<br>'; $ct++;} ?></td>
    <td class="cell"><? for($i=0; $i<$ct; $i++) { ?><img src="<?= pow(2,$i)&$r[is_agent]?'checked.gif':'uncheck.gif'?>"><? } ?></td>
    <td class="cell"><?=$emplyeeinfo[$r['leadmen']]?></td>
    <td class="cell"><img src="<?=$r[is_leadmen]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><img src="<?=$r[isOK]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><? 
	 	  if($r['isDel']) echo "<span class='noticeB'>已銷假</span><br>";
      else if($r['isReqD']==1 && $r[isDel]==0 && $r['is_leadmen2']==1) echo "<span class='noticeC'>不准銷假</span><br>";
			else if($r['isReqD']==1 && $r[isDel]==0) echo "<span class='noticeC'>已申請銷假</span><br>";
			else if($r['isOK']) echo "<span class='noticeA'>已准假</span><br>";
			echo "<span class='formTx'>".$r['rspContent']."</span>";
		?></td>
    <td class="cellsmall">
     	<a href="printform.php?absence/view.php?id=<?=$r[id]?>" target="_blank"><img src="print.png" title="列印" align="absmiddle" border="0"/></a>
			<? if($r['is_agent'] || $r['is_leadmen']) {
					 if( (date('Y/m',strtotime($r['bDate']))>=date('Y/m')) && $r['isOK'] && $r[isReqD]=='0') echo "<span class='sBtn' onclick=stateitgform($r[id],'$r[leadmen]')>銷假</span>";
				 } else echo "<a class='sBtn' href='index.php?incfn=absence/edit.php&id=$r[id]'>修改</a> <a class='sBtn' href='absence/del.php?id=$r[id]'>刪除</a>";
			?>
    </td>    
  </tr>
<? } ?>
<tr><td colspan="12" class="topline">
<? //::page row creation
if($pages>1) {
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	?>
</td></tr>
</table>
