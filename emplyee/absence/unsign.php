<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	$empID = $_SESSION[empID];
	$self = $_SERVER['PHP_SELF']."?incfn=absence/unsign.php";
	//$where= "(leadmen='$empID' and is_leadmen<>1) or ( instr(agent,'$empID') and is_leadmen<>1 )";
	$where= "(leadmen='$empID' and is_leadmen<>1) or ( instr(agent,'$empID') and (is_agent&power(2,Floor(instr(agent,'$empID')/6)))=0)";	
	$sql  = "select * from absence where $where";
	$sqc  = "select count(ID) from absence where $where";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 10;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數
	$sql .= " order by rdate Desc limit ".$page*$pageln.",$pageln"; 
	$rs = db_query($sql);	
?>
<script>
	Accordion1.openPanel(6);
	function signAbsence(id,field,bv) {
		bv = bv?bv:1;
		if(confirm('您是否確定要簽名?')) {
			var tfm = document.getElementById('form1');
			tfm.id.value = id;
			tfm.signfield.value = field;
			tfm.bitV.value = bv;
			tfm.submit();
		}
	}
	
	function stateitgform(id,ckHour,eid,aType, year) {
		statefm.id.value = id;
		$("#td_aType").html(aType);
		if(ckHour) {
			//Ajax check Hours
			$.ajax({
				url: 'chkHours.php?empID='+eid+'&hours='+ckHour+'&year='+year,
				type:"GET",
				dataType:'text',
				success: function(data){
					if(data!='OK') {
						document.getElementsByName('isOK')[0].checked = false;
						document.getElementsByName('isOK')[0].disabled = true
						document.getElementsByName('isOK')[1].checked = true;						
						alert(data);
					} 
				},
				error:function(xhr, ajaxOptions, thrownError){ alert(xhr.status+','+thrownError); }
			});
		}
		$('#div1').show();
	}
	
	function showList() {
		window.open('/maintain/absence/index.php','',"dialogWidth:820px; dialogHeight:500px; center:yes; status:no");
	}
	function viewDetail(aid) {
		window.open('printform.php?absence/view.php?id='+aid,'viewDetail',"dialogWidth:820px; dialogHeight:500px; center:yes; status:no");
	}
	function valid(frm){
		$("#Submit").hide();
		///$("#Submit").attr('disabled','disabled');
		return true;
	}
</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<style>
#div1 {
	position:absolute; 
	left: 40%; top:30%;
	background-color:#888;
	padding: 20px;
	display:none;
}
.spanbtn {
	font-size:smaller;
	float:right;
	cursor: pointer;
}
td>img { vertical-align:middle; }

@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
	td>img { vertical-align:middle; padding-bottom:4px; }
}
</style>

<div id="div1">
  <form id="statefm" action="absence/state.php" onSubmit="return valid(this);">
  	<table bgcolor="#FFFFFF" cellpadding="4">
    	<tr><td align="right">單號：</td><td><input type="text" name="id" /></td></tr>
    	<tr><td align="right">假別：</td><td id='td_aType'></td></tr>
      <tr><td align="right">核准：</td><td><input type="radio" name="isOK" value="1" checked="checked" />准假<input type="radio" name="isOK" value="0" />不准假</td></tr>
      <tr><td align="right">說明：</td><td><textarea name="rspContent" rows="5" cols="30"></textarea></td></tr>
      <tr><td colspan="2" align="center">
        <input type="submit" id="Submit" name="Submit" value="確定" />
        <input type="button" value="取消" onclick="$('#div1').hide()" />
      </td></tr>
    </table>  
  </form>
</div>

<div class="pageTitle">應簽名的請假單 <span onclick="showList()" class="spanbtn">[查詢部門請假紀錄]</span></div>	
<form id="form1" action="absence/sign.php" method="post">
<input type="hidden" name="id" /><input type="hidden" name="signfield" /><input type="hidden" name="bitV" />
<input type="hidden" name="empID" value="<?=$empID?>" />
<table class="sTable" width="100%" cellpadding="4" cellspacing="0">
<tr class="hdCell">
	<td width="50">申請人</td><td width="80">請假日期</td><td width="80">起始日期</td><td width="80">結束日期</td>
  <td width="35">時數</td><td width="45">假別</td><td width="45">&nbsp;</td>
  <td bgcolor="#E6E6FF" width="45">代理</td><td width="50" bgcolor="#E6E6FF">簽核</td>
  <td bgcolor="#FFEEFF" width="45">主管</td><td width="45" bgcolor="#FFEEFF">簽核</td><td width="35" bgcolor="#FFEEFF">准假</td>
</tr>
<? 
while($rs and $r=db_fetch_array($rs)) { 
		 $aa = split(',',$r[agent]);	
		 $ct = count($aa);
		 $bSignedLeadm = !$r['is_leadmen'] && $empID==$r[leadmen];
		 $CkHour = $r['aType']=='特休'?$r['hours']:0;
		 $r["empID"]=trim($r["empID"]);

		 $wkYear = date('Y',strtotime($r['bDate']));
?>
  <tr>
  	<td class="cell"><?=$emplyeeinfo[$r[empID]]?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['rdate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['bDate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['eDate'])) ?></td>
    <td class="cell" align="center"><?= $r['hours'] ?></td>
    <td class="cell" align="center">
    	<?
			$ulpath = '../data/absence/'.$r['id'];
			if(file_exists($ulpath)){
			  echo '<img src="/images/paper.png" width="15" border="0"/ style="float: left;padding-top: 2px;">';
			}
		?>
    	
    	<?= $r['aType'] ?></td>
    <td class="cell" align="center">

    	<a href="javascript:viewDetail(<?=$r[id]?>)"><img src="/images/detail.gif" title="列印" align="absmiddle" border="0"/></a></td>
    <td class="cell"><?	foreach($aa as $v) echo $emplyeeinfo[$v].'<br>'; ?></td>
    <td class="cell">
    	<? for($i=0; $i<$ct; $i++) { $bitV=pow(2,$i); $bSignedAgent = !($bitV&$r[is_agent]) && ($empID==$aa[$i]); ?>
      	<img src="<?= $bitV&$r[is_agent]?'checked.gif':'uncheck.gif'?>"><? if($bSignedAgent) echo "<img style='padding-top:5px' title='簽名' src='sign.png' onclick=signAbsence($r[id],'agent',$bitV)>"?>
        <br />
			<? } ?>
    </td>
    <td class="cell"><?=$emplyeeinfo[$r['leadmen']]?></td>
    <td class="cell"><img src="<?=$r[is_leadmen]?'checked.gif':'uncheck.gif'?>"><? if($bSignedLeadm) echo "<img title='簽名' src='sign.png' onclick=stateitgform($r[id],$CkHour,'$r[empID]','$r[aType]','$wkYear')>"?></td>
		<td class="cell"><img src="<?=$r[isOK]?'checked.gif':'uncheck.gif'?>"></td>    
  </tr>
<? } ?>
 
<tr><td colspan="12" class="topline">
<? //::page row creation
if($pages>1) {
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	?>
</td></tr>
</table>
</form>