<?
function get_rest_Hours($time,$empID){//取得補休時數
	//當季已核准的加班時數
	//$time=max($time,time());
	//$dateB = date("Y-m-d H:i:s",strtotime("-6 month",$time));
	//$dateE = date("Y-m-d H:i:s",$time);
	//20180701 變更規則
	$dateB = date("Y-01-01", $time);
	$dateE = date("Y-12-31 23:59:59", $time); // sean 修改	
	$sql="select * from overtime where empID='$empID' and isOK=1 and isDel=0 and fmAct=0 and bDate between '$dateB' and '$dateE'";
	$rsX=db_query($sql);
	$otHours=0;
	$otAry=array();
	if(!db_eof($rsX)){	
		while($rX=db_fetch_array($rsX)){ 	
			$otHours+=$rX['hours']?$rX['hours']:0;	
			array_push($otAry,$rX['id']);
		}
	}
	//echo $sql."<br>";
	//取得已請補休總時數
	//absence_overtime 作廢
	//$sql="select * from absence_overtime where otId in (".join(",",$otAry).") ";
	$sql="select * from absence where empID='$empID' and aType='補休' and isDel=0 and (isOK=1 or (isOK=0 and is_leadmen=0)) and bDate between '$dateB' and '$dateE'";
	//var_dump($sql);
	$rsX=db_query($sql);
	$abAry=array();
	$usedHour=0;
	if(!db_eof($rsX)){while($rX=db_fetch_array($rsX)){ $usedHour += $rX['hours'];	array_push($abAry,$rX['abId']);	}}

	//取得已請補休但未審核的時數
/* 	$sql="select sum(hours) as hours from absence ";
	$sql.="where empID='$empID' and isDel=0 and (is_leadmen=0 and isOK=0) ";
	$sql.="and aType='補休' ";
	//echo $sql."<br>";
	$rsX=db_query($sql);
	$abHours=0;
	if(!db_eof($rsX)){ $rX=db_fetch_array($rsX); $abHours=$rX[hours]?$rX[hours]:0; }
	$avHours = $otHours-$usedHour-$abHours; */
	$avHours = $otHours-$usedHour;
	return $avHours;
}

//::補休沖銷加班單 - 常規
function do_absence_overtime($id){
	$sql="select * from absence where id=".$id;
	$rs=db_query($sql);
	$ab=db_fetch_array($rs);
	//var_dump($sql);
	if($ab['aType']=="補休"){
		$time=max(strtotime($ab['bDate']),strtotime($ab['rdate']));
		$overtime=array();
		//找出所有加班單及可用時數
		//$dateB = date("Y-m-d H:i:s",strtotime("-6 month",$time));
		//20180701 變更規則
		//$dateB = date("Y-01-01");
    //20190925 變更規則
		$dateB = date("Y-01-01",$time);
		$dateE = date("Y-m-d H:i:s",$time);
	
		$sql = "SELECT *,o.hours as hours,sum(ao.hours) as use_hour,o.id as id ";
		$sql.=" FROM `overtime` as o left join absence_overtime as ao on o.id=ao.otId ";
		$sql.=" where o.empID='$ab[empID]' and o.isOK=1 and o.isDel=0 and o.fmAct=0 and o.bDate between '$dateB' and '$dateE' ";
		$sql.="group by o.bDate ";
    //var_dump($sql);
		$rs=db_query($sql);
		if(!db_eof($rs)){
			while($r=db_fetch_array($rs)){
				if(!$overtime[$r["id"]])	$overtime[$r["id"]]=array();
				$avHours=($r[hours]-$r[use_hour])>0?($r[hours]-$r[use_hour]):0;
				$overtime[$r["id"]]["empID"]=$r["empID"];
				$overtime[$r["id"]]["bDate"]=$r["bDate"];
				$overtime[$r["id"]]["avHours"]=$avHours;	//剩餘加班單的時數
			}
		}

		$ab['avHours']=$ab['hours'];	//請假時數
		foreach($overtime as $overId=>$v1){
			if($overtime[$overId]["avHours"]==0)	continue;
			if($ab["avHours"]<=$overtime[$overId]["avHours"]){//加班單時數夠沖
				$sql="insert into absence_overtime (abId,otId,hours) ";
				$sql.="values(".$id.",".$overId.",".$ab["avHours"].")";
				//echo "加班單時數夠沖:".$sql."<br>";
				db_query($sql);
				$overtime[$overId]["avHours"]-=$ab["avHours"];	
				$ab["avHours"]=0;
				break;	//沖完結束
			}else{//加班單時數不夠沖
				$sql="insert into absence_overtime (abId,otId,hours) ";
				$sql.="values(".$id.",".$overId.",".$overtime[$overId]["avHours"].")";
				//echo "加班單時數不夠沖:".$sql."<br>"; 下一筆繼續沖
				db_query($sql);
				$ab["avHours"]-=$overtime[$overId]["avHours"];	
				$overtime[$overId]["avHours"]=0;	
			}
		}
	}
}

//::補休沖銷加班單 - 補救
function do_absence_overtime2($id){
	$sql="select * from absence where id=".$id;
	$rs=db_query($sql);
	$ab=db_fetch_array($rs);
	if($ab['aType']=="補休"){
		$time=max(strtotime($ab['bDate']),strtotime($ab['rdate']));
		$overtime=array();
		//找出所有加班單及可用時數
		$dateB = date("Y-01-01",$time);
		$dateE = date("Y-12-31 23:59:59",$time);
		$sql = "SELECT *,o.hours as hours,sum(ao.hours) as use_hour,o.id as id ";
		$sql.=" FROM `overtime` as o left join absence_overtime as ao on o.id=ao.otId ";
		$sql.=" where o.empID='$ab[empID]' and o.isOK=1 and o.isDel=0 and o.fmAct=0 and o.bDate between '$dateB' and '$dateE' ";
		$sql.="group by o.bDate ";
		$rs=db_query($sql);
		if(!db_eof($rs)){
			while($r=db_fetch_array($rs)){
				if(!$overtime[$r["id"]])	$overtime[$r["id"]]=array();
				$avHours=($r[hours]-$r[use_hour])>0?($r[hours]-$r[use_hour]):0;
				$overtime[$r["id"]]["empID"]=$r["empID"];
				$overtime[$r["id"]]["bDate"]=$r["bDate"];
				$overtime[$r["id"]]["avHours"]=$avHours;	//剩餘加班單的時數
			}
		}
		$ab['avHours']=$ab['hours'];	//請假時數
		foreach($overtime as $overId=>$v1){
			if($overtime[$overId]["avHours"]==0)	continue;
			if($ab["avHours"]<=$overtime[$overId]["avHours"]){//加班單時數夠沖
				$sql="insert into absence_overtime (abId,otId,hours) ";
				$sql.="values(".$id.",".$overId.",".$ab["avHours"].")";
				//echo "加班單時數夠沖:".$sql."<br>";
				db_query($sql);
				$overtime[$overId]["avHours"]-=$ab["avHours"];	
				$ab["avHours"]=0;
				break;	//沖完結束
			}else{//加班單時數不夠沖
				$sql="insert into absence_overtime (abId,otId,hours) ";
				$sql.="values(".$id.",".$overId.",".$overtime[$overId]["avHours"].")";
				//echo "加班單時數不夠沖:".$sql."<br>"; 下一筆繼續沖
				db_query($sql);
				$ab["avHours"]-=$overtime[$overId]["avHours"];	
				$overtime[$overId]["avHours"]=0;	
			}
		}
	}
}

function get_overdue_Hours($time,$empID){ //取得逾期加班未補休時數, $time = 傳入的時間點
	//$dateB = date("Y-m-1 H:i:s",strtotime("-7 month",$time));
	//$dateE = date("Y-m-1 H:i:s",strtotime("-6 month",$time));
	//20180701 變更規則
	$dateB = date("Y-12-01");
	$dateE = date("Y-12-31");		
	$sql="select * from overtime where empID='$empID' and isOK=1 and isDel=0 and fmAct=0 and bDate between '$dateB' and '$dateE'";
	$rsX=db_query($sql);
	
	$tHours = 0;	//合計時數
	if(!db_eof($rsX)){
		$aa = array();	//一維
		while($rX=db_fetch_array($rsX)){ //加班單迴圈
			$id = $rX['id'];	//單號
			$ba = array();
			$ba['bDate'] = $rX['bDate'];
			$ba['eDate'] = $rX['eDate'];
			$ba['aType'] = $rX['aType'];
			$ba['hours'] = $rX['hours'];	//加班時數
			
			$sql = "select * from absence_overtime where otId=$id";
			$rsX2=db_query($sql);
			if(!db_eof($rsX)){ //有沖銷，檢查沖銷餘額
				$v = 0;
				while($rX2=db_fetch_array($rsX2))	$v += $rX2['hours'];
				$ba['hours2'] = $rX['hours']-$v;		//未沖銷時數
			} else { //未沖銷
				$otHours += $rX['hours'];
				$ba['hours2'] = $rX['hours'];
			}
			$aa[$id] = $ba;
			$otHours+=$rX[hours]?$rX[hours]:0;
			//$aa[$id]['fee'] 計算加班費金額
		}
	} return false;	//無資料
}
?>