<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	$empID = $_SESSION[empID];
	$self = $_SERVER['PHP_SELF']."?incfn=absence/unsign3.php";
	$where= "A.leadmen='$empID' and A.is_leadmen<>1";
	$sql  = "select A.*,B.empID,B.bDate,B.eDate,B.aType from absence_del A,absence B where A.fkey=B.id and $where";
	$sqc  = "select count(A.id) from absence_del A,absence B where A.fkey=B.id and $where";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 20;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數
	$sql .= " order by rdate Desc limit ".$page*$pageln.",$pageln";
	$rs = db_query($sql);	
?>
<script>
	Accordion1.openPanel(6);
	function signAbsence(id,fkey,aType) { 
    $("#td_aType").html(aType);
		statefm.id.value = id;
		statefm.fkey.value = fkey;
		$('#div1').show();
	}
</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<style>
#div1 {
	position:absolute; 
	left: 40%; top:30%;
	background-color:#888;
	padding: 20px;
	display:none;
}
</style>
<div id="div1">
  <form id="statefm" action="absence/sign_del.php">
  	<table bgcolor="#FFFFFF" cellpadding="4">
    	<tr><td align="right">單號：</td><td><input type="text" name="id" /></td></tr>
      <tr><td align="right">假別：</td><td id='td_aType'></td></tr>
      <tr><td align="right">核准：</td><td><input type="radio" name="isOK" value="1" checked="checked" />准許銷假<input type="radio" name="isOK" value="0" />不准銷假</td></tr>
      <tr><td align="right">說明：</td><td><textarea name="rspContent" rows="5" cols="30"></textarea></td></tr>
      <tr><td colspan="2" align="center">
      	<input type="hidden" name="fkey" />
        <input type="submit" name="Submit" value="確定" />
        <button type="button" onclick="$('#div1').hide()">取消</button>
      </td></tr>
    </table>  
  </form>
</div>

<div class="pageTitle">應簽名的銷假單</div>	
<table class="sTable" width="100%" cellpadding="4" cellspacing="0">
<tr class="hdCell">
	<td width="45">申請人</td><td width="80">請假日期</td><td width="80">起始日期</td><td width="80">結束日期</td><td>銷假緣故</td>
  <td bgcolor="#FFEEFF">主管</td><td width="50" bgcolor="#FFEEFF">簽核</td>
</tr>
<? while($rs and $r=db_fetch_array($rs)) { 
		 $bSignedLeadm = !$r['is_leadmen'] && $empID==$r[leadmen];
     $r["empID"]=trim($r["empID"]);
?>
  <tr>
  	<td class="cell"><?=$emplyeeinfo[$r[empID]]?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['rdate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['bDate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['eDate'])) ?></td>
    <td class="cell"><?=$r['content']?></td>
    <td class="cell"><?=$emplyeeinfo[$r['leadmen']]?></td>
    <td class="cell"><img src="<?=$r[is_leadmen]?'checked.gif':'uncheck.gif'?>">
			<? if($bSignedLeadm) echo "<img title='簽名' src='sign.png' onclick=signAbsence($r[id],$r[fkey],'$r[aType]')>" ?>
    </td>
  </tr>
<? } ?>
 
<tr><td colspan="10" class="topline">
<? //::page row creation
if($pages>1) {
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	?>
</td></tr>
</table>
