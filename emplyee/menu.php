<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../Scripts/jquery.corner.js"></script>
<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script> 
<style>
#Accordion1 { background-color:#F8F8F8}
#Accordion1 a {text-decoration:none }
</style>
<div id="Accordion1" class="Accordion" tabindex="0">
  <div class="AccordionPanel">
    <div class="AccordionPanelTab">個人資訊</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=person.php">個人資料維護</a></li>
        <li><a href="index.php?incfn=password.php">變更密碼</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=community">通訊資料</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=emplyee_experience">經歷</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=emplyee_education">學歷</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=emplyee_license">證照/著作</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=emplyee_training">教育訓練</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=emplyee_duty">出勤表</a></li>
      </ul>
    </div>
  </div>
  <div class="spera"></div>
  
  <div class="AccordionPanel">
    <div class="AccordionPanelTab">訊息中心</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=unreadmsg.php">尚未讀取的訊息</a></li>
        <li><a href="index.php?incfn=readedmsg.php">已經讀取的訊息</a></li>
        <li><a href="index.php?incfn=mymsg.php">我發佈的訊息</a></li>
        <li><a href="index.php?incfn=newmsg.php">發佈新的訊息</a></li>
      </ul>
    </div>
  </div>
  <div class="spera"></div>
  
  <div class="AccordionPanel">
    <div class="AccordionPanelTab">行事曆</div>
    <div class="AccordionPanelContent">
    	<ul>
      	<li><a href="index.php?incfn=all2cal.php">行事曆(不分類)</a></li>
        <li><a href="index.php?incfn=mycal.php">我的行事曆</a></li>
        <li><a href="index.php?incfn=depcal.php">部門行事曆</a></li>
        <li><a href="index.php?incfn=allcal.php">全員行事曆</a></li>
        <li><a href="index.php?incfn=schedule/list.php">行事曆維護</a></li>
    	</ul>
    </div>
  </div>
  <div class="spera"></div>  
  
  <div class="AccordionPanel">
    <div class="AccordionPanelTab">請假系統</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=absence/unsign.php">應簽名的請假單</a></li>
        <li><a href="index.php?incfn=absence/unsign2.php">已簽名的請假單</a></li>
        <li><a href="index.php?incfn=absence/unsign3.php">應簽名的銷假單</a></li>
        <li><a href="index.php?incfn=absence/unsign4.php">已簽名的銷假單</a></li>        
        <li><a href="index.php?incfn=absence/my.php">我的請假單</a></li>
        <li><a href="index.php?incfn=absence/new.php">新的請假單</a></li>
      </ul>
    </div>
  </div>
  <div class="spera"></div>
  
  <div class="AccordionPanel">
    <div class="AccordionPanelTab">外出系統</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=outoffice/unsign.php">應簽名的外出單</a></li>
        <li><a href="index.php?incfn=outoffice/unsign2.php">已簽名的外出單</a></li>        
        <li><a href="index.php?incfn=outoffice/my.php">我的外出單</a></li>
        <li><a href="index.php?incfn=outoffice/new.php">新的外出單</a></li>
      </ul>
    </div>
  </div>
  <div class="spera"></div>

  <div class="AccordionPanel">
    <div class="AccordionPanelTab">綜合申請單</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=integratform/unsign.php">應簽名的綜合單</a></li>
        <li><a href="index.php?incfn=integratform/unsign2.php">已簽名的綜合單</a></li>
        <li><a href="index.php?incfn=integratform/my.php">我的綜合申請單</a></li>
        <li><a href="index.php?incfn=integratform/new.php">新的綜合申請單</a></li>
      </ul>
    </div>
  </div>
  <div class="spera"></div>
  
  <div class="AccordionPanel">
    <div class="AccordionPanelTab">加班申請單</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=overtime/unsign.php">應簽名的加班單</a></li>
        <li><a href="index.php?incfn=overtime/unsign2.php">已簽名的加班單</a></li>
        <li><a href="index.php?incfn=overtime/unsign3.php">應簽名的銷班單</a></li>
        <li><a href="index.php?incfn=overtime/unsign4.php">已簽名的銷班單</a></li>    
        <li><a href="index.php?incfn=overtime/my.php">我的加班申請單</a></li>
        <li><a href="index.php?incfn=overtime/new.php">新的加班申請單</a></li>
      </ul>
    </div>
  </div>
  <div class="spera"></div>

  <div class="AccordionPanel">
    <div class="AccordionPanelTab">預算計劃申請訊息</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=planMsg/unreadmsg.php">未讀的訊息通知</a></li>
        <li><a href="index.php?incfn=planMsg/readedmsg.php">已讀的訊息通知</a></li>
      </ul>
    </div>
  </div>
  <div class="spera"></div>

  <div class="AccordionPanel">
    <div class="AccordionPanelTab">薪資/獎金查詢</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=setIframe.php&act=salaryReportPersonal">薪資查詢</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=festivalPersonal">三節獎金查詢</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=yearEndPersonal">年終獎金查詢</a></li>
        <li><a href="index.php?incfn=setIframe.php&act=bonusPersonal">特休代金查詢</a></li>
      </ul>
    </div>
  </div>


  <!-- 暫時隱藏
  <div class="spera"></div>  
  <div class="AccordionPanel">
    <div class="AccordionPanelTab">派外訓練申請單</div>
    <div class="AccordionPanelContent">
      <ul>
        <li><a href="index.php?incfn=apply_train/unsign.php">應簽名派外訓練單</a></li>
        <li><a href="index.php?incfn=apply_train/unsign2.php">已簽名派外訓練單</a></li>
        <li><a href="index.php?incfn=apply_train/my.php">我的派外訓練單</a></li>
        <li><a href="index.php?incfn=apply_train/new.php">新的派外訓練單</a></li>
      </ul>
    </div>
  </div>  
  -->
</div>
<script type="text/javascript">
	var Accordion1 = new Spry.Widget.Accordion("Accordion1",{useFixedPanelHeights: false, defaultPanel: 0, duration: 250});
	$(function(){
		$('.AccordionPanelTab').each(function(index, element) {
    	$(element).corner("5px"); //dog tr
  	});
	});
</script>