<?
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../../maintain/inc_vars.php';	
	$id = $_REQUEST[id];
	$sql = "select * from integratform where id=$id";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>表單列印</title>
<style>
.pageTitle {
	font-family: "微軟正黑體", Verdana;
	font-size: 18px;
	font-weight: bold;
	padding-bottom: 12px;
}

</style>
</head>

<body>
<p class="pageTitle" align="center">綜合申請單<br /><span style="font:normal 15px '微軟正黑體',Verdana">單號：<?=$r[fmNo]?></span></p>	
<table width="92%" border="1" align="center" cellpadding="10" cellspacing="0" style="font:normal 15px '微軟正黑體',Verdana">
  <tr>
    <td width="100" align="right">申請人</td>
    <td><?=$emplyeeinfo[$r[empID]]?></td>
    <td width="100" align="right">申請單位</td>
    <td><?=$departmentinfo[$r["depID"]]?></td>
  </tr>
  <tr>
    <td align="right">統編</td><td><?=$r[invNo]?></td>
    <td align="right">專案編號</td><td><?=$r[prjNo]?></td>
  </tr>
  <tr>
    <td align="right">申請類別</td><td><?=$r[aType]?></td>
    <td align="right">申請單位主管</td><td><?=$emplyeeinfo[$r[depLead]]?></td>
  </tr>
  <tr>
    <td align="right">需求期間</td><td colspan="3"><?=$r[bDate]?> ~ <?=$r[eDate]?></td>
  </tr>
	  
  <tr>
    <td align="right">需求內容</td>
    <td colspan="3"><?=$r[content]?></td>
  </tr>
  <tr>
    <td align="right">需求說明</td>
    <td colspan="3"><?=$r[description]?></td>
  </tr>
  
	<tr>
	  <td align="right">承辦單位</td>
	  <td><?=$departmentinfo[$r[rspDep]]?></td>
	  <td align="right">申請時間</td><td><?=$r[rDate]?></td>
  </tr>
	<tr>
    <td align="right">承辦人</td>
    <td><?=$emplyeeinfo[$r[rspMan]]?></td>
    <td align="right">承辦單位主管</td>
    <td><?=$emplyeeinfo[$r[rspLead]]?></td>
  </tr>
	<tr>
	  <td align="right">辦理情形</td>
	  <td colspan="3"><?= $r['rspState'].($r['rspContent']?'<br>[說明]：<br>'.$r['rspContent']:'')?></td>
  </tr>
</table>
</body>
</html>