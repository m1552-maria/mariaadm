<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	include '../maintain/inc_vars.php';	
	$empID = $_SESSION[empID];
	$sql = "select * from integratform where id=$_REQUEST[id]";
	$rs = db_query($sql);
	if($rs) $r=db_fetch_array($rs);
?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
Accordion1.openPanel(10);
function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	if(!tfm.bDate.value) { alert('起始日期不能為空白！'); tfm.bDate.focus(); return false; }
	if(!tfm.eDate.value) { alert('結束日期不能為空白！'); tfm.eDate.focus(); return false; }
  var rzt = true;
	$(tfm['rspDep']).each(function(){if(!this.value) {alert('承辦單位'); rzt = false;}});
	$(tfm['rspMan']).each(function(){if(!this.value) {alert('承辦人'); rzt = false;}});
	$(tfm['rspLead']).each(function(){if(!this.value) {alert('承辦單位主管'); rzt = false;}});
	return rzt;
}
$(function(){ $("#prjform").keypress(checkFormEnter); });
//:: callback functions
var qryField = 'rspDep';	//Query	EmpID filter Field's name
function rspDep_Callback(id) {
	//alert(id);
}
</script>
<form id="prjform" action="integratform/editDo.php" method="post" onsubmit="return formCheck(this)">
<table width="100%" border="0" cellpadding="6" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td align="right">申請人：</td><td><?=$emplyeeinfo[$r['empID']]?></td>
    <td align="right">申請單位：</td><td><?=$departmentinfo[$r["depID"]]?></td>
  </tr>
  <tr>
    <td align="right">統編：</td>
    <td><select name="invNo" id="invNo"><? foreach($invNo as $v) echo "<option ".($r[invNo]==$v?'selected':'').">$v</option>" ?></select></td>
    <td align="right">專案編號：</td>
    <td><input type="text" name="prjNo" id="prjNo" value="<?=$r[prjNo]?>" /></td>
  </tr>
  <tr>
    <td align="right">申請類別：</td>
    <td><select name="aType" id="aType"><? foreach($itgFmType as $v) echo "<option ".($r[aType]==$v?'selected':'').">$v</option>" ?></select>
    </td>
    <td align="right">申請單位主管：</td>
    <td><input name="depLead" type="text" class="queryID" id="empID" size="6" value="<?=$r[depLead]?>" title="<?=$emplyeeinfo[$r[depLead]]?>" /></td>
  </tr>
  <tr>
    <td align="right">需求期間：</td>
    <td colspan="3">起始日期
      <input type="text" name="bDate" id="bDate" value="<?=$r[bDate]?>"/><script>$(function(){$('#bDate').datetimepicker()})</script> 
      ~ 結束日期
      <input type="text" name="eDate" id="eDate" value="<?=$r[eDate]?>"/><script>$(function(){$('#eDate').datetimepicker()})</script></td>
    </tr>
	  
  <tr>
    <td align="right">需求內容：</td>
    <td colspan="3"><textarea name="content" id="content" cols="60" rows="5"><?=$r[content]?></textarea></td>
  </tr>
  <tr>
    <td align="right">需求說明：</td>
    <td colspan="3"><textarea name="description" id="description" cols="60" rows="5"><?=$r[description]?></textarea></td>
  </tr>
  
	<tr>
	  <td align="right">承辦單位：</td>
	  <td><input name="rspDep" type="text" class="queryID" id="rspDep" size="6" callback="rspDep_Callback" onkeypress="return checkInput(event,'depName',this)" value="<?=$r[rspDep]?>" title="<?=$departmentinfo[$r[rspDep]]?>"/></td>
	  <td align="right">&nbsp;</td>
	  <td>&nbsp;</td>
	  </tr>
	<tr>
    <td align="right">承辦人：</td>
    <td><input name="rspMan" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)" value="<?=$r[rspMan]?>" title="<?=$emplyeeinfo[$r[rspMan]]?>"/></td>
    <td align="right">承辦單位主管：</td>
    <td><input name="rspLead" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)" value="<?=$r[rspLead]?>" title="<?=$emplyeeinfo[$r[rspLead]]?>"/></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td colspan="3">
	    <input name="id" type="hidden" id="id" value="<?=$r[id]?>"/>
      <input name="empID" type="hidden" value="<?=$r[empID]?>"/>
    	<input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
</form>