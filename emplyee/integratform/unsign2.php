<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	include '../maintain/inc_vars.php';		
	$empID = $_SESSION[empID];
	$self = $_SERVER['PHP_SELF']."?incfn=integratform/unsign2.php";
	$where= "(rspMan='$empID' and is_rspMan=1) or (rspLead='$empID' and is_rspLead=1) or (depLead='$empID' and is_depLead=1)";
	$sql = "select * from integratform where $where";
	$sqc = "select count(ID) from integratform where $where";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 10;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數
	$sql .= " order by rdate Desc limit ".$page*$pageln.",$pageln";
	$rs = db_query($sql);	

?>
<script>
	Accordion1.openPanel(10);
	function signForm(id,field) {
		if(confirm('您是否確定要簽名?')) {
			var tfm = document.getElementById('form1');
			tfm.id.value = id;
			tfm.signfield.value = field;
			tfm.submit();
		}
	}
	function stateitgform(id) {
		statefm.id.value = id;
		$('#div1').show();
	}
</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<style>
#div1 {
	position:absolute; 
	left: 40%; top:30%;
	background-color:#888;
	padding: 20px;
	display:none;
}
</style>
<div id="div1">
  <form id="statefm" action="state.php">
  	<table bgcolor="#FFFFFF" cellpadding="4">
    	<tr><td align="right">單號：</td><td><input type="text" name="id" /></td></tr>
      <tr><td align="right">辦理情形：</td><td><select name="rspState"><? foreach($itgFmStatus as $v) echo "<option>$v</option>" ?></select></td></tr>
      <tr><td align="right">說明：</td><td><textarea name="rspContent" rows="5" cols="30"></textarea></td></tr>
      <tr><td colspan="2" align="center">
        <input type="submit" name="Submit" value="確定" />
        <button type="button" onclick="$('#div1').hide()">取消</button>
      </td></tr>
    </table>  
  </form>
</div>

<div class="pageTitle">已簽名的綜合單</div>	
<form id="form1" action="sign.php" method="post">
<input type="hidden" name="id" /><input type="hidden" name="signfield" />
<input type="hidden" name="empID" value="<?=$empID?>" />
<table class="sTable" width="100%" cellpadding="4" cellspacing="0">
<tr class="hdCell">
	<td width="45">申請人</td><td>類別</td><td colspan="2">單位主管</td><td>需求期間</td><td>需求內容</td><td>需求說明</td><td>承辦單位</td>
  <td colspan="2" bgcolor="#E6E6FF">承辦人</td>
  <td colspan="2" bgcolor="#FFEEFF">承辦主管</td>
  <td colspan="3" width="30" bgcolor="#FFFFDF">辦理情形</td>
</tr>
<? while($rs and $r=db_fetch_array($rs)) { ?>
  <tr>
	  <td class="cell"><?=$emplyeeinfo[$r[empID]]?></td>
    <td class="cell"><?=$r['aType']?></td>
    <td class="cell"><?=$emplyeeinfo[$r['depLead']]?></td>
    <td class="cell"><img src="<?=$r[is_depLead]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['bDate'])) ?> ~ <?= date('Y/m/d H:i',strtotime($r['eDate'])) ?></td>
    <td class="cell"><?=$r['content']?></td>
    <td class="cell"><?=$r['description']?></td>
    <td class="cell"><?=$departmentinfo[$r['rspDep']]?></td>
    <td class="cell"><?=$emplyeeinfo[$r['rspMan']]?></td>
    <td class="cell"><img src="<?=$r[is_rspMan]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><?=$emplyeeinfo[$r['rspLead']]?></td>
    <td class="cell"><img src="<?=$r[is_rspLead]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><?= $r['rspState'].($r['rspContent']?'<br>[說明]：<br>'.$r['rspContent']:'')?></td>
    <td class="cell"><? if($bSignedMan || $bSignedLead) echo "<img title='辦理情形' src='sign.png' onclick=stateitgform($r[id],'rspLead')>"?></td>
    <td class="cell"><a href="printform.php?integratform/view.php?id=<?=$r[id]?>" target="_blank"><img src="print.png" title="列印" align="absmiddle" border="0"/></a></td>    
  </tr>
<? } ?>
 
<tr><td colspan="15" class="topline">
<? //::page row creation
if($pages>1) {
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	?>
</td></tr>
</table>
</form>