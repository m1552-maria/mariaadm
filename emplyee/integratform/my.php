<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	$self = $_SERVER['PHP_SELF']."?incfn=integratform/my.php";
	$sql = "select * from integratform where empID='$_SESSION[empID]'";
	$sqc = "select count(ID) from integratform where empID='$_SESSION[empID]'";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 10;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數
	$sql .= " order by rdate Desc limit ".$page*$pageln.",$pageln";
	$rs = db_query($sql);	

?>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<script>Accordion1.openPanel(10);</script>
<div class="pageTitle">我的綜合申請單</div>	

<table class="sTable" width="100%" cellpadding="4" cellspacing="0">
<tr class="hdCell">
	<td>申請類別</td><td>單位主管</td><td>需求期間</td><td>需求內容</td><td>需求說明</td><td>承辦單位</td>
  <td colspan="2" bgcolor="#E6E6FF">承辦人</td>
  <td colspan="2" bgcolor="#FFEEFF">承辦單位主管</td>
  <td width="75" align="center" bgcolor="#E8E8D0">維護</td>
  <td bgcolor="#FFFFDF">辦理情形</td>
  <td bgcolor="#FFFFDF">&nbsp;</td>
</tr>
<? while($rs and $r=db_fetch_array($rs)) { ?>
  <tr>
    <td class="cell"><?=$r['aType']?></td>
    <td class="cell"><?=$emplyeeinfo[$r['depLead']]?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['bDate'])) ?> ~ <?= date('Y/m/d H:i',strtotime($r['eDate'])) ?></td>
    <td class="cell"><?=$r['content']?></td>
    <td class="cell"><?=$r['description']?></td>
    <td class="cell"><?=$departmentinfo[$r['rspDep']]?></td>
    <td class="cell"><?=$emplyeeinfo[$r['rspMan']]?></td>
    <td class="cell"><img src="<?=$r[is_rspMan]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><?=$emplyeeinfo[$r['rspLead']]?></td>
    <td class="cell"><img src="<?=$r[is_rspLead]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cellsmall"><?= ($r['is_rspMan'] || $r['is_rspLead'] || $r['is_depLead']) ? "&nbsp;" : "[<a href='index.php?incfn=integratform/edit.php&id=$r[id]'>修改</a>] [<a href='integratform/del.php?id=$r[id]'>刪除</a>]" ?></td>
    <td class="cell"><?= $r['rspState'].($r['rspContent']?'<br>[說明]：<br>'.$r['rspContent']:'')?></td>
    <td class="cell"><a href="printform.php?integratform/view.php?id=<?=$r[id]?>" target="_blank"><img src="print.png" title="列印" align="absmiddle" border="0"/></a>
    </td>
  </tr>
<? } ?>
 
<tr><td colspan="13" class="topline">
<? //::page row creation
if($pages>1) {
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	?>
</td></tr>

</table>
