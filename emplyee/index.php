<?php
	@session_start();
	if(!$_SESSION[empID]) header("Location: /login.php");
	//::取的權限設定
	$priviege = $_SESSION['user_priviege'];
	$classdef = $_SESSION['user_classdef'];	
	
	include '../inc_sys.php';
	$incfn = $_REQUEST[incfn];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Keywords" content="網站關鍵字" />
  <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
  <META HTTP-EQUIV="EXPIRES" CONTENT="0">
  <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<title>個人資訊</title>
	<link href="/css/index.css" rel="stylesheet" type="text/css" />
  <link href="../Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
	<style>
  .menuBar { background-color:#5C0A10;	font: 19px '微軟正黑體',Verdana;	color:#FFFFE0 }
  #mainmenu a { text-decoration:none }
  #mainmenu a:link { color:#FFFFE0 }
  #mainmenu a:visited { color:#FFFFE0 }
  #mainmenu a:hover { color:#FFFFE0 }
  #mainmenu a:active { color:#FFFFE0 }
	.foot {	font:12px "微軟正黑體",Verdana;	color: #CCC; background-color: #7B0C13;	padding: 5px; }
  </style>  
	<script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
  <script type="text/javascript" src="../Scripts/showModalDialog.js"></script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mainTable">
  <tr>
    <td class="sideBar">&nbsp;</td>
    <td width="1000" class="main" >
    	<!-- 頁面本體 ↓-->
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td class="headBar"><? include('../inc_head.php')?></td></tr>
        <tr><td class="menu"><? include('../inc_menu2.php') ?></td></tr>        
        <tr><td class="main">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr height="8"><td width="5"></td><td width="170"></td><td colspan="2"></td><td width="5"></td></tr>
            <!-- 目錄頁本體 ↓-->
            <tr valign="top">
              <td>&nbsp;</td>
              <td><? include 'menu.php' ?></td>
              <!--td width="24" background="/images/line.gif" style="background-repeat:repeat-y; background-position:center"></td-->
              <td width="5"></td>
              <td valign="top"><? include $incfn; ?></td>
              <td width="5"></td>
            </tr>
            <!-- 目錄頁本體 ↓-->
            <tr><td colspan="5" height="4"></td></tr>
          </table>
        </td></tr>
        <tr><td class="foot" align="center"><? include('../inc_foot.php');?></td></tr>
      </table>
      <!-- 頁面本體 ↑-->
    </td>  
    <td class="sideBar">&nbsp;</td>
  </tr>
</table>
</body>
</html>
