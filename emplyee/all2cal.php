<? //:: 全體 + 部門 + 個人
	@session_start();
	$depID = $_SESSION[depID];
	include '../config.php';
	$self = $_SERVER['PHP_SELF']."?incfn=all2cal.php";

	$dateB = date("Y/m/01",strtotime("-1 month"));	//今日
	$dateE = date("Y/m/01",strtotime("+5 month"));
	$sql = "select * from schedule where announcer='$_SESSION[empID]' and PID is null and ( (beginDate between '$dateB' and '$dateE') or (endDate between '$dateB' and '$dateE') )";	
	$rs = db_query($sql);
?>
<script>
	Accordion1.openPanel(4);
	
	$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {left:'prev,next today', center:'title', right:'month,agendaWeek,agendaDay'},
			editable: false,
			buttonText : { today: '今日',month: '月',agendaWeek: '周',	agendaDay: '日'	},
			monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			dayNames : ['周日','週一','週二','週三','週四','週五','週六'],
			dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],
			events: [ <? 
			$evtlst = array();
			//個人
			while($r=db_fetch_array($rs)) {
				 $bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				 $eD = date('Y/m/d H:i',strtotime($r[endDate]));
				 $evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false}";
			} 
			//部門行事曆
			$sql = "select * from schedule where PID is not  NULL and ( (beginDate between '$dateB' and '$dateE') or (endDate between '$dateB' and '$dateE') )";	
			$rs = db_query($sql);
			while($r=db_fetch_array($rs)) {
				$aa = explode(',',$r[PID]);
				foreach($aa as $v) if( strpos($depID,$v) !== false ) {
				  $bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				 	$eD = date('Y/m/d H:i',strtotime($r[endDate]));
				 	$evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#B08080',borderColor:'#B08080'}";
					break;
				}
			} 
			//全員行事曆
			$dateB2 = date("Y/1/01");
			$dateE2 = date("Y/1/01",strtotime("+1 year"));
			$sql = "select * from schedule_all where (beginDate between '$dateB2' and '$dateE2') or (endDate between '$dateB2' and '$dateE2')";
			$rs = db_query($sql);
			while($r=db_fetch_array($rs)) { 
				$bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				$eD = date('Y/m/d H:i',strtotime($r[endDate]));
				$evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#80B080',borderColor:'#80B080'}";
			} 
			
			//專案行事曆
			$sql = "SELECT A.* FROM `schedule_project` A left join projects B on A.ClassID=B.id where instr(B.PID,'$_SESSION[empID]')";
			$rs = db_query($sql);
			while($r=db_fetch_array($rs)) { 
				$bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				$eD = date('Y/m/d H:i',strtotime($r[endDate]));
				$evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#336699',borderColor:'#80B080'}";
			} 
					
			echo join(',',$evtlst);
			?>
			]
		});
	});	
</script>
<link rel='stylesheet' type='text/css' href='/scripts/fullcalendar/fullcalendar.css' />
<script type='text/javascript' src='/scripts/jquery-ui-1.7.2.custom.min.js'></script>
<script type='text/javascript' src='/scripts/fullcalendar/fullcalendar.min.js'></script>

<div class="pageTitle">行事曆(不分類)</div>	
<div id='calendar'></div>

