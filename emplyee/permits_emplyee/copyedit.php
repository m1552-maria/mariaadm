<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
  $sql = "select * from $tableName where $editfnA[0]='$ID'";
	$rs = db_query($sql, $conn);
	$r = db_fetch_array($rs);
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <script src="/Scripts/miclib.js" type="text/javascript"></script>
 <script>
 	function addItem(tbtn,fld) {
		var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6'>"
			+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode;
		$(elm).remove();
	}
	
	function cancel() {
		if (isIE()) window.close();
		else location.href = 'query_id.php';
	}
 </script> 
 <title><?=$pageTitle?> - 複製編輯</title>
</Head>
<body class="page">
<form method="post" action="docopyedit.php" name="form1" enctype="multipart/form-data">
<table align="center" class="sTable" width="96%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<? for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><? switch($editetA[$i]) { 
  	  case "text" : echo "<input type='text' name='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  		case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $v) { $vv=$r[$editfnA[$i]]==$v?"<option selected>$v</option>":"<option>$v</option>"; echo $vv;} echo "</select>"; break;				 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";				
  	} ?></td>
  </tr><? } ?>

  <tr valign="top"><td align="right" class="colLabel">部門</td>	<td>
  	<? $a = explode(';',$r[depPM]); $aa=explode(':',$a[0]); ?>
    <input name="depID[]" type="text" class="queryID" id="depID" size="6" value="<?=$aa[0]?>" title="<?=$departmentinfo[$aa[0]]?>"/> <span class="btn" onClick="addItem(this,'depID')"> +新增</span>
    <? for($i=1; $i<count($a); $i++) {
			 $aa=explode(':',$a[$i]);
			 echo "<div><input name='depID[]' id='depID' type='text' size='6' value='$aa[0]'>"
			 . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$departmentinfo[$aa[0]]."</span>"
			 . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		} ?>
  </td></tr>
  <tr valign="top"><td align="right" class="colLabel">員工</td><td>
	  <? $a = explode(';',$r[empPM]); $aa=explode(':',$a[0]); ?>
  	<input name="empID[]" type="text" class="queryID" id="empID" size="6" value="<?=$aa[0]?>" title="<?=$emplyeeinfo[$aa[0]]?>"/> <span class="btn" onClick="addItem(this,'empID')"> +新增</span>
    <? for($i=1; $i<count($a); $i++) {
			 $aa=explode(':',$a[$i]);
			 echo "<div><input name='empID[]' id='empID' type='text' size='6' value='$aa[0]'>"
			 . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$emplyeeinfo[$aa[0]]."</span>"
			 . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		} ?>    
  </td></tr>
  <tr valign="top"><td align="right" class="colLabel">職掌</td><td>
	  <? $a = explode(';',$r[jobPM]); $aa=explode(':',$a[0]); ?>
  	<input name="jobID[]" type="text" class="queryID" id="jobID" size="6" value="<?=$aa[0]?>" title="<?=$jobinfo[$aa[0]]?>"/> <span class="btn" onClick="addItem(this,'jobID')"> +新增</span>
    <? for($i=1; $i<count($a); $i++) {
			 $aa=explode(':',$a[$i]);
			 echo "<div><input name='jobID[]' id='jobID' type='text' size='6' value='$aa[0]'>"
			 . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$jobinfo[$aa[0]]."</span>"
			 . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		} ?>     
  </td></tr>
  
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="crman" value="<?=$_SESSION[empID]?>">
		<input type="submit" value="確定" class="btn">&nbsp;	
    <a href="#" onClick="cancel()"><span class="btn" style="padding:3px; text-decoration:none">取消</span></a>
  </td>
</tr>
</table>
</form>
</body>
</html>
