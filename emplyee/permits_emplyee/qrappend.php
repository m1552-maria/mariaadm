<?php
	include("init.php"); 
?>
<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>append.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
 <script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
 <script type="text/javascript" src="/Scripts/validation.js"></script>
 <script src="/Scripts/form.js" type="text/javascript"></script>
 <script>
 	function addItem(tbtn,fld) {
		var newone = "<div><input name='"+fld+"[]' id='"+fld+"' type='text' size='6'>"
			+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
			+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		$(tbtn).after(newone);
	}
	function removeItem(tbtn) {
		var elm = tbtn.parentNode; 
		$(elm).remove();
	}
 </script>
 
 <title><?=$pageTitle?> - 新增</title>
</Head>

<body class="page">
<form action="qrdoappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>

	<? for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><? switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  		case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $v) echo "<option>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
  	} ?></td>
  </tr><? } ?>
  
  <tr valign="top"><td align="right" class="colLabel">部門授權</td><td><input name="depID[]" type="text" class="queryID" id="depID" size="6" /> <span class="btn" onClick="addItem(this,'depID')"> +新增</span></td></tr>
  <tr valign="top"><td align="right" class="colLabel">員工授權</td><td><input name="empID[]" type="text" class="queryID" id="empID" size="6" /> <span class="btn" onClick="addItem(this,'empID')"> +新增</span></td></tr>
  <tr valign="top"><td align="right" class="colLabel">職掌授權</td><td><input name="jobID[]" type="text" class="queryID" id="jobID" size="6" /> <span class="btn" onClick="addItem(this,'jobID')"> +新增</span></td></tr>
	
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<input type="submit" value="確定新增" class="btn">&nbsp;
      <input type="reset" value="重新輸入" class="btn">&nbsp;
		</td>
	</tr>
</table>
</form>
</body>
</html>
