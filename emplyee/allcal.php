<?
	include '../config.php';
	$self = $_SERVER['PHP_SELF']."?incfn=allcal.php";
	$dateB2 = date("Y/1/01");
	$dateE2 = date("Y/1/01",strtotime("+1 year"));
	$sql = "select * from schedule_all where (beginDate between '$dateB2' and '$dateE2') or (endDate between '$dateB2' and '$dateE2')";
	$rs = db_query($sql);
?>
<style>
.calDay {
	background-image: url(/images/activity.png) !important;
  background-repeat: no-repeat !important;
  background-position: 2px 2px !important;
}
</style>
<script>
	Accordion1.openPanel(4);
	
	Date.prototype.ymdhi = function() {
		var mm = this.getMonth() + 1; 
  	var dd = this.getDate();
		var hh = this.getHours();
		var ii = this.getMinutes();
	  return [this.getFullYear(),(mm>9?'':'0')+mm,(dd>9?'':'0')+dd].join('-')+' '+(hh>9?'':'0')+hh+':'+(ii>9?'':'0')+ii;
	}
	
	function showTooltip(data,event,view) {
		tooltip = '<div class="tooltiptopicevent" style="background:#feb811;position:absolute;z-index:10001;padding:10px 10px 10px 10px;border-radius:10px">'
		 + data.title + '</br>' + data.start.ymdhi() + '<br>' + data.end.ymdhi() + '</div>';
		$("body").append(tooltip);
		$(this).mouseover(function (e) {
			$(this).css('z-index', 10000);
			$('.tooltiptopicevent').fadeIn('500');
			$('.tooltiptopicevent').fadeTo('10', 1.9);
		}).mousemove(function (e) {
			$('.tooltiptopicevent').css('top', e.pageY + 10);
			$('.tooltiptopicevent').css('left', e.pageX + 20);
		});
  }
	function hideTooltip(data, event, view) {
    $(this).css('z-index', 8);
    $('.tooltiptopicevent').remove();
  }

	$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			header: {left:'prev,next today', center:'title', right:'month,agendaWeek,agendaDay'},
			editable: false,
			buttonText : { today: '今日',month: '月',agendaWeek: '周',	agendaDay: '日'	},
			monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
			dayNames : ['周日','週一','週二','週三','週四','週五','週六'],
			dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],
			//eventClick: function(e){ alert(e); },
			//dayClick:function(dayDate, allDay, jsEvent, view){      //点击单元格事件   
      //  alert(dayDate.getFullYear()+"-"+dayDate.getMonth()+"-"+dayDate.getDate()+"-星期:"+dayDate.getDay()+ "视图:"+view.name);   
        //$('#calendar').fullCalendar("incrementDate", -1, 0, 0); //移动日历 后三个参数为 年 月 日   
        //$('#calendar').fullCalendar("prev"); // 跳转到前一月/周/天, 根据当前的view决定   
        //$('#calendar').fullCalendar("next"); // 跳转到下一月/周/天, 根据当前的view决定   
        //$('#calendar').fullCalendar("today"); // 跳转今天   
        //$('#calendar').fullCalendar("gotoDate", year[, month[, date]]); //跳转到指定的日期   
        //$('#calendar').fullCalendar("changeView","viewName"); //切换日历的view, viewName必须是允许的views   
    	//}, //	eventColor: '#336699', //變更預設顏色
			eventMouseover: showTooltip,
			eventMouseout: hideTooltip,
			dayClick: function(date,allDay,jsEvent,view) {
				var aDay = date.toLocaleDateString();
				var r = confirm('是否確定新增'+aDay+'日的活動行事曆!');
				if(r)	window.open('index.php?incfn=schedule/append.php&act=activity&day='+aDay);
			},
			events: [ <? 
			$evtlst = array();
			while($r=db_fetch_array($rs)) {
				 $bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				 $eD = date('Y/m/d H:i',strtotime($r[endDate]));
				 $evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#80B080',borderColor:'#80B080'}";
			}
			//選部門 活動
			$sql = "select * from schedule where PID is not  NULL and ( (beginDate between '$dateB2' and '$dateE2') or (endDate between '$dateB2' and '$dateE2') ) and ClassID='活動/旅遊'";	
			$rs = db_query($sql);
			while($r=db_fetch_array($rs)) { //部門活動行事曆
				$aa = explode(',',$r[PID]);
				foreach($aa as $v) {
				  $bD = date('Y/m/d H:i',strtotime($r[beginDate]));
				 	$eD = date('Y/m/d H:i',strtotime($r[endDate]));
				 	$evtlst[] = "{title:'$r[title]',start:'$bD',end:'$eD',allDay:false,backgroundColor:'#B08080',borderColor:'#B08080'}";
					break;
				}
			} 
			echo join(',',$evtlst);
			?>
			]
		});
		$('td[class*="fc-day"]').addClass('calDay');
	});	
</script>
<link rel='stylesheet' type='text/css' href='/scripts/fullcalendar/fullcalendar.css' />
<script type='text/javascript' src='/scripts/jquery-ui-1.7.2.custom.min.js'></script>
<script type='text/javascript' src='/scripts/fullcalendar/fullcalendar.min.js'></script>

<div class="pageTitle">全員行事曆</div>	
<div id='calendar'></div>

