<?
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	$id = $_REQUEST[id];
	$sql = "select * from outoffice where id=$id";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>表單列印</title>
<style>
.pageTitle {
	font-family: "微軟正黑體", Verdana;
	font-size: 18px;
	font-weight: bold;
	padding-bottom: 12px;
}

</style>
</head>

<body>
<p class="pageTitle" align="center">外出單</p>	
<table width="92%" border="1" align="center" cellpadding="10" cellspacing="0" style="font:normal 15px '微軟正黑體',Verdana">
  <tr>
    <td width="100" align="right">外出人</td>
    <td><?=$emplyeeinfo[$r[empID]]?></td>
    <td width="100" align="right">外出地點</td>
    <td><?=$r["place"]?></td>
  </tr>
  <tr>
    <td align="right">外出時間</td><td><?= date("Y-m-d H:i",strtotime($r[bDate]))?></td>
    <td align="right">直接下班</td><td><?=($r[offwork]?'√':'')?></td>
  </tr>
  <tr>
    <td align="right">預定返回時間</td><td><?= intval($r[eDate])?date("Y-m-d H:i",strtotime($r[eDate])):''?></td>
    <td align="right">實際返回時間</td><td><?= intval($r[rtnDate])?date("Y-m-d H:i",strtotime($r[rtnDate])):''?></td>
  </tr>
  <tr>
    <td align="right">經辦事項</td><td colspan="3"><?=$r[content]?></td>
  </tr>
	<tr>
    <td align="right">主管</td>
    <td><?=$emplyeeinfo[$r[leadmen]]?></td>
    <td align="right">登錄時間</td>
    <td><?= date("Y-m-d H:i",strtotime($r[rdate]))?></td>
  </tr>

</table>
</body>
</html>