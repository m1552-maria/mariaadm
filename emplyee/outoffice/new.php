<?
	include '../config.php';
	include '../maintain/inc_vars.php';	
	$empID = $_SESSION[empID];
	$depID = $_SESSION["depID"];
	$rs = db_query("select * from emplyee where empID='$empID'");
	if($rs) $r=db_fetch_array($rs);
?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/validation.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
Accordion1.openPanel(8);
function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) { 
	var timeCheck = /^([0-9]{4})[./]{1}([0-9]{1,2})[./]{1}([0-9]{1,2})\s([0-1][0-9]|2[0-3])\:[0-5][0-9]$/;
	if(!tfm.place.value) { alert('外出地點不能為空白！'); tfm.place.focus(); return false; }
	if(!tfm.bDate.value) { alert('外出時間不能為空白！'); tfm.bDate.focus(); return false; }
	if(!tfm.eDate.value && !tfm.offwork.checked) { alert('預定返回時間不能為空白！'); tfm.eDate.focus(); return false; }
	/*20171223 sean 新增 外出返回時間驗證*/
	if(!timeCheck.test(tfm.bDate.value) || (!tfm.offwork.checked && !timeCheck.test(tfm.eDate.value))){
		alert('時間格式錯誤 ex: 2017/01/01 08:00');
		return false;
	}
	if(!tfm.offwork.checked && (Date.parse(tfm.bDate.value)).valueOf() >= (Date.parse(tfm.eDate.value)).valueOf()) {
		alert('外出時間不得大於等於返回時間');
		return false;
	}

	if(!tfm.offwork.checked) {
		$bD = tfm.bDate.value.split(" ");
		$bD = $bD[0].split("/");
		$eD = tfm.eDate.value.split(" ");
		$eD = $eD[0].split("/");

		var re = 0;
		for(var i in $bD) {
			if($bD[i] != $eD[i]) {
				re = 1;
				break;
			}
		}
		if(re) {
			alert('外出返回要在同一天完成');
			return false;
		}
	}
	

  	var rzt = true;
	$(tfm['leadmen']).each(function(){
		if(!this.value) {
			alert('請選擇主管'); rzt = false;
		} else rzt = checkEmpID(this.value);
	});
	return rzt;
}

$(function(){
	$.get('/getdbInfo.php',{'act':'dep','depID':'<?=$depID?>'},function(data){ 
		if(data) {
			var aa = data.split(',');
			var elm = document.getElementsByName('leadmen')[0];
			elm.value = aa[2];
			$(elm).next().next().text(aa[3]); 
		}
	});
});
</script>
<form id="prjform" action="outoffice/newDo.php" method="post" onsubmit="return formCheck(this)">
<table width="100%" border="0" cellpadding="8" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td align="right">外出人：</td>
    <td colspan="3"><?=$r[empName]?><input name="empID" type="hidden" id="empID" value="<?=$empID?>"/></td>
  </tr>
  <tr>
    <td align="right">外出地點：</td>
    <td colspan="2"><input name="place" type="text" id="place" size="60" /></td>
    <td><input type="checkbox" name="offwork" id="offwork" value="1" />直接下班</td>
    </tr>
  <tr>
    <td align="right">外出時間：</td>
    <td><input type="text" name="bDate" id="bDate"/><script>$(function(){$('#bDate').datetimepicker()})</script></td>
    <td align="right">預定返回時間：</td>
    <td><input type="text" name="eDate" id="eDate"/><script>$(function(){$('#eDate').datetimepicker()})</script></td>
  </tr>
	  
  <tr>
    <td align="right">經辦事項：</td>
    <td colspan="3"><textarea name="content" id="content" cols="60" rows="5"></textarea></td>
  </tr>
  
	<tr>
    <td align="right">主管：</td>
    <td><input name="leadmen" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)"/><span class='formTxS'></span></td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td colspan="3">
    	<input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
      <input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
</form>