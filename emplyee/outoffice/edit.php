<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	include '../maintain/inc_vars.php';	
	$empID = $_SESSION[empID];
	$sql = "select * from outoffice where id=$_REQUEST[id]";
	$rs = db_query($sql);
	if($rs) $r=db_fetch_array($rs);

?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
Accordion1.openPanel(8);
$(function(){ $("#prjform").keypress(checkFormEnter); });

function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	if(!tfm.bDate.value) { alert('外出時間不能為空白！'); tfm.bDate.focus(); return false; }
	if(!tfm.eDate.value && !$('#offwork').attr('checked')) { alert('預定返回時間不能為空白！'); tfm.eDate.focus(); return false; }
  var rzt = true;
	$(tfm['leadmen']).each(function(){if(!this.value) {alert('請選擇主管'); rzt = false;}});
	return rzt;
}
</script>
<form id="prjform" action="outoffice/editDo.php" method="post" onsubmit="return formCheck(this)">
<table width="100%" border="0" cellpadding="8" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td align="right">外出人：</td>
    <td colspan="3">
		<?=$_SESSION["empName"]?><input name="empID" type="hidden" id="empID" value="<?=$empID?>"/>
    <input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
    <input name="id" type="hidden" id="id" value="<?=$r[id]?>"/>
    </td>
  </tr>
  <tr>
    <td align="right">外出地點：</td>
    <td><input type='text' name='place' value="<?=$r['place']?>"/></td>
    <td colspan='2'><input type="checkbox" name="offwork" id="offwork" <?= $r[offwork]?'checked':'';?>/>直接下班</td>
  </tr>
  <tr>
    <td align="right">外出時間：</td>
    <td><input type="text" name="bDate" id="bDate" value="<?=date('Y/m/d H:i',strtotime($r[bDate]))?>"/><script>$(function(){$('#bDate').datetimepicker({hour:<?=date('H',strtotime($r[bDate]))?>,minute:<?=date('i',strtotime($r[bDate]))?>})})</script></td>
    <td align="right">預定返回時間：</td>
    <td><input type="text" name="eDate" id="eDate" value="<?=($r[eDate] !='0000-00-00 00:00:00') ? date('Y/m/d H:i',strtotime($r[eDate])) : '';?>"/><script>$(function(){$('#eDate').datetimepicker({hour:<?=date('H',strtotime($r[eDate]))?>,minute:<?=date('i',strtotime($r[eDate]))?>})})</script></td>
  </tr>
	  
  <tr>
    <td align="right">經辦事項：</td>
    <td colspan="3"><textarea name="content" id="content" cols="60" rows="3"><?=$r[content]?></textarea></td>
  </tr>
	<tr>
    <td align="right">主管</td>
    <td colspan="3"><input name="leadmen" type="text" class="queryID" id="empID" size="6" value="<?=$r[leadmen]?>" title="<?=$emplyeeinfo[$r[leadmen]]?>" onkeypress="return checkInput(event,'empName',this)"/></td>
  </tr>

  <tr>
    <td colspan="4" align='center'>
    	<input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
</form>