<?
	include '../config.php';
	if($_REQUEST[id]) {
		if($_REQUEST[act]==1) {
			$sql = "update outoffice set offwork=1 where id=$_REQUEST[id]";
		} else {
			$sql = "update outoffice set rtnDate='$_REQUEST[rtnDate]' where id=$_REQUEST[id]";
		}
		db_query($sql);
	}
	
	include '../getEmplyeeInfo.php';
	$self = $_SERVER['PHP_SELF']."?incfn=outoffice/my.php";
	$sql = "select * from outoffice where empID='$_SESSION[empID]'";
	$sqc = "select count(ID) from outoffice where empID='$_SESSION[empID]'";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 10;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數
	$sql .= " order by rdate Desc limit ".$page*$pageln.",$pageln";
	$rs = db_query($sql);	

?>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script>
	Accordion1.openPanel(8);
	function dooffwork(id) {
		var url = 'index.php?incfn=outoffice/my.php&act=1&id='+id;
		window.location.href= url;
	}
	function dortnDate(id,ds) {
		var bDate = new Date(ds);
		var aDate = $('#rtnDate'+id).val();
		if(!aDate) {alert('時間不能為空白'); return; }
		if(new Date(aDate) < bDate) {alert('實際返回時間不能早於外出時間'); return; }
		var url = 'index.php?incfn=outoffice/my.php&act=2&id='+id+'&rtnDate='+aDate;
		window.location.href= url;
	}
	function doDel(id){
		if(!confirm("確定要刪除嗎?")) return;
		location.href='outoffice/del.php?id='+id;
	}
</script>
<div class="pageTitle">我的外出單</div>	

<table class="sTable" width="100%" cellpadding="4" cellspacing="0">
<tr class="hdCell">
	<td width="70">外出時間</td><td width="90">預定返回<br>時間</td><td>經辦事項</td><td>實際返回 / <br>下班時間</td><td>直接下班</td><td bgcolor="#FFEEFF">主管</td><td bgcolor="#FFEEFF">簽核</td><td width="16" bgcolor="#FFEEFF">&nbsp;</td>
</tr>
<? while($rs and $r=db_fetch_array($rs)) { ?>
  <tr>
    <td class="date"><? $bDate=date('Y/m/d H:i',strtotime($r['bDate'])); echo $bDate; ?></td>
    <td class="date"><?= intval($r['eDate'])?date('Y/m/d H:i',strtotime($r['eDate'])):'&nbsp;' ?></td>
    <td class="cell"><?=$r['content']?></td>
    <td class="date"><? if($r['rtnDate']) echo date('Y/m/d H:i',strtotime($r['rtnDate'])); else echo '<input size="12" type="text" name="rtnDate" id="rtnDate'.$r[id].'"/><button onclick="dortnDate('."$r[id],'$bDate'".')">送出</button><script>$(function(){$("#rtnDate'.$r[id].'").datetimepicker()})</script>' ?></td>
    <td class="cell"><? if($r['offwork']) echo "<img src='checked.gif'>"; else echo '<input type="checkbox" name="offwork" onclick="dooffwork('.$r[id].')"/>' ?></td>
    <td class="cell"><?=$emplyeeinfo[$r['leadmen']]?></td>
    <td class="cell">
    	<img src="<?=$r[is_leadmen]?'checked.gif':'uncheck.gif'?>">
      <? //$r['is_leadmen'] ? "&nbsp;" : "[<a href='deloutoffice.php?id=$r[id]'>刪除</a>]" ?>
      <? if($r['is_leadmen']==0) {	echo "<a class='sBtn' href='index.php?incfn=outoffice/edit.php&id=$r[id]'>修改</a> <a class='sBtn' onclick='doDel($r[id])'>刪除</a>";}
		?>
    </td>
    <td class="cell"><a href="printform.php?outoffice/view.php?id=<?=$r[id]?>" target="_blank"><img src="print.png" title="列印" align="absmiddle" border="0"/></a></td>
  </tr>
<? } ?>
 
<tr><td colspan="8" class="topline">
<? //::page row creation
if($pages>1) {
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	?>
</td></tr>

</table>
