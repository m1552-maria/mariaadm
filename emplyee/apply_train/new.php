<?
	include '../config.php';
	include '../maintain/inc_vars.php';	
	$empID = $_SESSION[empID];
	$depID = $_SESSION["depID"];
?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<style type="text/css">
</style>
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
Accordion1.openPanel(14);

function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
		+ "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	var bDate = tfm.bDate.value;
	var eDate = tfm.eDate.value;
	if(!tfm.title.value) {alert('請填寫課程名稱'); tfm.title.focus(); return false;}
	if(!bDate) { alert('請選擇受訓起日'); tfm.bDate.focus(); return false; }
	if(!eDate) { alert('請選擇受訓迄日'); tfm.eDate.focus(); return false; }
	var bDateA = bDate.split('/'); if(bDateA.length<3) {alert('受訓起日錯誤'); tfm.bDate.focus(); return false; }
	var eDateA = eDate.split('/'); if(eDateA.length<3) {alert('受訓迄日錯誤'); tfm.eDate.focus(); return false; }
	if( (bDateA[0]!=eDateA[0]) || (bDateA[1]!=eDateA[1]) ) {alert('請勿跨月申請，若有此需要請分單'); return false; }
	if( bDateA[2]>eDateA[2] ) {alert('受訓起迄日錯誤，請注意起迄時間'); return false; }
	if(!(tfm.hours.value>0)) { alert('時數錯誤，請填寫數字'); tfm.hours.focus(); return false; }
	if(!tfm.trainUnit.value) {alert('請填寫主辦單位'); tfm.trainUnit.focus(); return false;}
	if(isNaN(tfm.fee.value)) { alert('費用錯誤，請填寫數字'); tfm.fee.focus(); return false; }
	
  var rzt = false;
	
	$(tfm['leadmen']).each(function(){if(!this.value) {	
			alert('請選擇主管'); 
			rzt = false;
		} else {
			if(checkEmpID(this.value)) {
				if(this.value=='<?=$empID?>') {
					alert('核准主管不能為申請人'); 
					rzt = false;
				} else rzt = true;
			} else rzt = false;
		}
	});
	
	/* Reserved 2015/11/11
	var applyType=document.getElementsByName("applyType");
	var applyTypeV='';
	for(var i=0;i<applyType.length;i++){
		if(applyType[i].checked==true){//委請報名
			applyTypeV=applyType[i].value;
		}
	}
	if(applyTypeV==""){	alert('請選擇報名方式');  return false; }
	if(applyTypeV==1){//委請報名，要選擇人員
		var agents=document.getElementsByName("agent[]");
		var bAg=false;
		for(var i=0;i<agents.length;i++){
			if(agents[i].value!=""){	bAg=true;}
		}
		if(!bAg){alert("請選擇委請報名的人員");	 return false;}
	} */
	
	return rzt;
}
function checkInput(evt,act,elm) {
	if(evt.keyCode==13) {
		$.get('/getdbInfo.php',{'act':act,'ID':elm.value},function(data){ 
			if(data) $(elm).next().next().text(data); 
		});
		return false;
	}
}
$(function(){
	$.get('/getdbInfo.php',{'act':'dep','depID':'<?=$depID?>'},function(data){ 
		if(data) {
			var aa = data.split(',');
			var elm = document.getElementsByName('leadmen')[0];
			elm.value = aa[2];
			$(elm).next().next().text(aa[3]); 
		}
	});
});
</script>
<form id="prjform" action="apply_train/newDo.php" method="post" onsubmit="return formCheck(this)">
<table border="0" cellpadding="6" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
  <tr>
    <td width="130" align="right" class='gridtd'>申請人</td>
    <td colspan="3">
		<?=$_SESSION["empName"]?><input name="empID" type="hidden" id="empID"  value="<?=$empID?>"/>
    <input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
    </td>
  </tr>
   <tr>
    <td width="130" align="right" class='gridtd'>課程分類</td>
    <td><select name="trainClass" id="trainClass"><? foreach($trainClass as $v) echo "<option>$v</option>" ?></select></td>
    <td width="90" align="right">課程名稱</td>
    <td align="left"><input name="title" type="text" id="title" /></td>
  </tr>
   <tr>
    <td width="90" align="right">主辦單位</td>
    <td align="left"><input name="trainUnit" type="text" id="trainUnit" /></td>
    <td width="130" align="right" class='gridtd'>時數</td>
    <td><input name="hours" type="text" id="hours" /></td>
  </tr>
  <tr>
    <td align="right">受訓起日</td>
    <td><input type="text" name="bDate" id="bDate"/><script>$(function(){$('#bDate').datetimepicker()})</script></td>
    <td align="right">受訓迄日</td>
    <td align="left"><input type="text" name="eDate" id="eDate"/><script>$(function(){$('#eDate').datetimepicker()})</script></td>
  </tr>
   <tr>
    <td width="130" align="right" class='gridtd'>請假假別</td>
    <td><select name="aType" id="aType"><? foreach($absType as $v) { if($v=='公假') $selected='selected'; else $selected='';	echo "<option $selected>$v</option>";	}?></select></td>
    <td width="90" align="right">費用</td>
    <td align="left"><input name="fee" type="text" id="fee" /></td>
  </tr>
  <tr>
    <td align="right" valign="top">核准主管</td>
    <td align="left" valign="top"><input name="leadmen" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)"/><span class='formTxS'></span></td>
    <!-- 暫時停用
    <td align="right" valign="top">報名方式</td>
    <td align="left" valign="top">
    	<input type="radio" name="applyType" value="0">自行報名<br>
			<input type="radio" name="applyType" value="1">委請報名
      <input name="agent[]" type="text" class="queryID" id="empID" size="6" onkeypress="return checkInput(event,'empName',this)"/><span class="sBtn" onclick="addItem(this,'agent','empID')"> +新增其它人員</span>
    </td> -->
  </tr>
  <tr>
    <td colspan="4" align="center">
    	<input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
</form>