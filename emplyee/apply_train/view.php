<?
	include '../../config.php';
	include '../../getEmplyeeInfo.php';
	include '../../maintain/inc_vars.php';	
	$id = $_REQUEST[id];
	$sql = "select * from apply_train where id=$id";
	$rs = db_query($sql);
	$r = db_fetch_array($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/css/index.css" rel="stylesheet" type="text/css">
<title>表單列印</title>
<style>
body{
	margin:10px;
	background:#fff;
}
.pageTitle {
	font-family: "微軟正黑體", Verdana;
	font-size: 18px;
	font-weight: bold;
	padding-bottom: 12px;
	text-align:center;
}

</style>
</head>

<body>
<table width="92%" border="1" align="center" cellpadding="10" cellspacing="0" style="font:normal 15px '微軟正黑體',Verdana"> 
	<tr>
    <td colspan='4' class="pageTitle">派外訓練單</td>
  </tr>
  <tr>
    <td width="100" align="right">申請人</td>
    <td><?=$emplyeeinfo[$r[empID]]?></td>
    <td align="right">申請時間</td>
    <td><?= date('Y-m-d H:i',strtotime($r[rdate]))?></td>
  </tr>
  <tr>
    <td width="100" align="right">課程分類</td>
    <td><?=$r['trainClass']?></td>
    <td align="right">課程名稱</td>
    <td><?=$r['title']?></td>
  </tr>
  <tr>
    <td align="right">主辦單位</td>
    <td><?=$r["trainUnit"]?></td>
    <td align="right">時數</td>
    <td><?=$r["hours"]?> 小時</td>
  </tr>
  <tr>
    <td width="100" align="right">受訓起日</td>
    <td><?= date('Y-m-d H:i',strtotime($r[bdate]))?></td>
    <td align="right">受訓迄日</td>
    <td><?= date('Y-m-d H:i',strtotime($r[edate]))?></td>
  </tr>
  <tr>
    <td align="right">請假假別</td>
    <td><?=$r[aType]?></td>
    <td align="right">費用</td>
    <td><?=$r[fee]?></td>
  </tr>
  <tr>
    <td align="right">代理人</td>
    <td><?
		$agent=explode(",",$r['agent']);
		$agName=array();
		foreach($agent as $k=>$v){	array_push($agName,$emplyeeinfo[$v]);	}
		echo join(",",$agName);
		?>
    </td>
    <td align="right">主管</td>
    <td><?=$emplyeeinfo[$r[leadmen]]?></td>
  </tr>
  <tr>
    <td align="right">是否准申請</td>
    <td colspan="3"><? if($r['isOK']) echo '已准申請 '.date('Y-m-d',strtotime($r[is_leadmen_date])) ?></td>
  </tr>
	<tr>
	  <td align="right">辦理情形</td>
	  <td colspan="3"><?=$r['rspContent']?></td>
  </tr>
</table>
</body>
</html>