<?
	include '../config.php';
	include '../getEmplyeeInfo.php';
	include '../maintain/inc_vars.php';	
	$empID = $_SESSION[empID];
	$depID = $_SESSION["depID"];
	$sql = "select * from apply_train where id=$_REQUEST[id]";
	//echo $sql;
	$rs = db_query($sql);
	if($rs) $r=db_fetch_array($rs);
?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script>
Accordion1.openPanel(14);
function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
		+ "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}
function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
function formCheck(tfm) {
	var bDate = tfm.bdate.value;
	var eDate = tfm.edate.value;
	if(!bDate) { alert('請選擇受訓起日'); tfm.bdate.focus(); return false; }
	if(!eDate) { alert('請選擇受訓迄日'); tfm.edate.focus(); return false; }
	var bDateA = bDate.split('/'); 
	var eDateA = eDate.split('/');
	var bDateA = bDate.split('/'); if(bDateA.length<3) {alert('受訓起日錯誤'); tfm.bdate.focus(); return false; }
	var eDateA = eDate.split('/'); if(eDateA.length<3) {alert('受訓迄日錯誤'); tfm.edate.focus(); return false; }
	if( (bDateA[0]!=eDateA[0]) || (bDateA[1]!=eDateA[1]) ) {alert('請勿跨月請假，若有此需要請分單'); return false; }
	
	if( bDateA[2]>eDateA[2] ) {alert('受訓起迄日錯誤，請注意起迄時間'); return false; }
	if(!(tfm.hours.value>0)) { alert('時數錯誤，請填寫數字'); tfm.hours.focus(); return false; }
	if(!tfm.trainUnit.value) {alert('請填寫主辦單位'); tfm.trainUnit.focus(); return false;}
	if(isNaN(tfm.fee.value)) { alert('費用錯誤，請填寫數字'); tfm.fee.focus(); return false; }
 	var rzt = true;
	$(tfm['leadmen']).each(function(){
		if(!this.value) {alert('請選擇主管'); rzt = false;}
		if(this.value=='<?=$empID?>') {
			alert('核准主管不能為申請人'); 
			rzt = false;
		}
	});
	var applyType=document.getElementsByName("applyType");
	var applyTypeV='';
	var bSelected=true;
	for(var i=0;i<applyType.length;i++){
		if(applyType[i].checked==true){//委請報名
			applyTypeV=applyType[i].value;
		}
	}
	if(applyTypeV==""){	alert('請選擇報名方式');  return false; }
	if(applyTypeV==1){//委請報名，要選擇人員
		var agents=document.getElementsByName("agent[]");
		var bAg=false;
		for(var i=0;i<agents.length;i++){
			if(agents[i].value!=""){	bAg=true;}
		}
		if(!bAg){alert("請選擇委請報名的人員");	 return false;}
	}else{
		var agents=document.getElementsByName("agent[]");
		for(var i=0;i<agents.length;i++){	agents[i].value="";	}
	}
	return rzt;
}
$(function(){
	
	/*$.get('/getdbInfo.php',{'act':'dep','depID':'<?=$depID?>'},function(data){ 
		if(data) {
			var aa = data.split(',');
			console.log('data:'+data);
			var elm = document.getElementsByName('leadmen')[0];
			elm.value = aa[2];
			$(elm).next().next().text(aa[3]); 
		}
	});
	*/
});
</script>
<form id="prjform" action="apply_train/editDo.php" method="post" onsubmit="return formCheck(this)">
<table width="100%" class='grid'>
  <tr>
    <td align="right">申請人：</td>
    <td colspan='3'>
		<?=$_SESSION["empName"]?><input name="id" type="hidden" id="id" value="<?=$r['id']?>"/>
    <input name="depID" type="hidden" id="depID" value="<?=$depID?>"/>
    <input name="id" type="hidden" id="id" value="<?=$r[id]?>"/>
    </td>
  </tr>
   <tr>
    <td width="100" align="right" class='gridtd'>課程分類</td>
    <td><select name="trainClass" id="trainClass"><? foreach($trainClass as $v) echo "<option>$v</option>" ?></select></td>
    <td width="100" align="right">課程名稱</td>
    <td align="left"><input name="title" type="text" id="title" value="<?=$r[title]?>"/></td>
  </tr>
   <tr>
    <td width="100" align="right">主辦單位</td>
    <td align="left"><input name="trainUnit" type="text" id="trainUnit" value="<?=$r[trainUnit]?>"/></td>
    <td width="100" align="right" class='gridtd'>時數</td>
    <td><input name="hours" type="text" id="hours" value="<?=$r[hours]?>"/></td>
  </tr>
  <tr>
    <td align="right">受訓起日</td>
    <td><input type="text" name="bdate" id="bdate" value="<?=date('Y/m/d H:i',strtotime($r[bdate]))?>"/><script>$(function(){$('#bdate').datetimepicker({hour:<?=date('H',strtotime($r[bdate]))?>,minute:<?=date('i',strtotime($r[bdate]))?>})})</script></td>
    <td align="right">受訓迄日</td>
    <td><input type="text" name="edate" id="edate" value="<?=date('Y/m/d H:i',strtotime($r[edate]))?>"/><script>$(function(){$('#edate').datetimepicker({hour:<?=date('H',strtotime($r[edate]))?>,minute:<?=date('i',strtotime($r[edate]))?>})})</script></td>
  </tr>
  <tr>
    <td width="100" align="right" class='gridtd'>請假假別</td>
    <td><select name="aType" id="aType"><? foreach($absType as $v) {	$ck=($r[aType]==$v)?'selected':''; echo "<option $ck>$v</option>"; }?></select></td>
    <td width="100" align="right">費用</td>
    <td align="left"><input name="fee" type="text" id="fee" value="<?=$r[fee]?>"/></td>
  </tr>	 
	<tr>
    <td align="right">主管</td>
    <td><input name="leadmen" type="text" class="queryID" id="empID" size="6" value="<?=$r[leadmen]?>" title="<?=$emplyeeinfo[$r[leadmen]]?>"/></td>
    <td align="right" valign="top">報名方式</td>
    <td align="left" valign="top">
    	<? 
		$aa = explode(',',$r[agent]); 
		if($r['agent']==""){	$ck='checked';}else{$ck='';}
		?>
    	<input  type="radio" name="applyType" value="0" <?=$ck?>>自行報名<br>
		<?	if($r['agent']!=""){	$ck='checked';}else{$ck='';}	?>      
		<input  type="radio" name="applyType" value="1" <?=$ck?>>委請報名
            	<input name="agent[]" type="text" class="queryID" id="empID" value="<?=$aa[0]?>" size="6" title="<?=$emplyeeinfo[$aa[0]]?>" />
      			<span class="sBtn" onclick="addItem(this,'agent','empID')"> +新增其它人員</span>
          <? for($i=1; $i<count($aa); $i++) {
				echo "<div><input name='agent[]' id='empID' type='text' size='6' value='$aa[$i]'>"
			 . "<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'>".$emplyeeinfo[$aa[$i]]."</span>"
			 . "<span class='btn2' onClick='removeItem(this)'> -移除</span></div>";
		}
		?> 
    </td>
  </tr>

  <tr>
    <td colspan="4" align="center">
    	<input type="submit" name="Submit" id="Submit" value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" /></td>
  </tr>
</table>
</form>