<?
	include '../../config.php';
	include '../../getEmplyeeInfo.php';

	$empID = $_REQUEST['empID'];
	$sqc = "select count(id) from apply_train where empID='$empID'";
	$r = db_fetch_array(db_query($sqc));
	$recs   = $r[0];														//紀錄數
	$pageln = 10;																//每頁幾行
	$page   = $_REQUEST[page];									//第幾頁
	$pages  = ceil($recs / $pageln);  					//總頁數

	$sql = "select * from apply_train where empID='$empID' order by rdate Desc limit ".$page*$pageln.",$pageln";
	$rs = db_query($sql);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>派外訓練申請單</title>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<script>
function doImport(id) {
	window.opener.location.href="/maintain/emplyee_training/append.php?nwID="+id;
	window.close();
}
</script>
</head>

<body>
<table class="sTable" width="100%" cellpadding="4" cellspacing="0" border="1">
<caption style="font:bold 20px Verdana,'微軟正黑體'"><?=$emplyeeinfo[$empID]?></caption>
<tr class="hdCell">
	<td width="120">課程名稱</td><td width="80">申請日期</td><td width="80">受訓起日</td><td width="80">受訓迄日</td>
  	<!--<td width="100">主辦單位</td>-->
  	<td bgcolor="#FFEEFF" width="50">主管</td><td width="40" bgcolor="#FFEEFF">簽核</td><td width="40" bgcolor="#FFEEFF">核准</td>
  	<td bgcolor="#FFFFDF" width="80">辦理情形</td><td width="110" align="center" bgcolor="#E8E8D0">維護</td>
</tr>
<? while($rs and $r=db_fetch_array($rs)) { ?>
  <tr valign="top">
    <td class="date"><?=$r['title']?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['rdate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['bdate'])) ?></td>
    <td class="date"><?= date('Y/m/d H:i',strtotime($r['edate'])) ?></td>
    <!--<td class="cell" align="center"><?=$r['trainUnit']?></td>-->
    <td class="cell"><?=$emplyeeinfo[$r['leadmen']]?></td>
    <td class="cell"><img src="<?=$r[is_leadmen]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><img src="<?=$r[isOK]?'checked.gif':'uncheck.gif'?>"></td>
    <td class="cell"><? 
	 	  	if($r['isOK']) echo "<span class='noticeA'>已准假</span><br>";
			echo "<span class='formTx'>".$r['rspContent']."</span>";
		?></td>
    <td class="cellsmall">
     	<a href="../printform.php?apply_train/view.php?id=<?=$r[id]?>" target="_blank"><img src="../print.png" title="列印" align="absmiddle" border="0"/></a>
			<? if($r['is_leadmen']) {
					 ///if( (date('Y/m',strtotime($r['bDate']))==date('Y/m')) && $r['isOK'] ) echo "<span class='sBtn' onclick=stateitgform($r[id],'$r[leadmen]')>我要銷假</span>";
				 } else echo "<a class='sBtn' href='index.php?incfn=apply_train/edit.php&id=$r[id]'>修改</a> <a class='sBtn' onclick='doDel($r[id])' >刪除</a>";
			 if($r['isOK']){
				 echo "<input type='button' value='匯入 員工教育訓練' onclick='doImport($r[id])'/>";
			 }
			?>
    </td>    
  </tr>
<? } ?>
<? //::page row creation
if($pages>1) {
	?>
    
<tr><td colspan="10" class="topline">
<?
  if ($page>0) echo "<a href='$self&page=".($page-1)."'>前一頁</a>";
  $pg = floor($page / 10); 
  $i = 0;
  $p = $i+($pg*10);
  while ( ($p<$pages) && ($i<10) ) {
    echo " | ";
    if ($p==$page) echo ($p+1); else echo "<a href='$self&page=".$p."'>".($p+1)."</a>";
    $i++;
    $p = $i+($pg*10);
  }
  echo " | ";
  if ($page < $pages-1) echo "<a href='$self&page=".($page+1)."'>下一頁</a>";
}	
?>
</td></tr>
</table>
</body>
</html>