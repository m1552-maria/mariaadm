<?php
	@session_start();
	if(!$_SESSION[empID]) header("Location: /login.php");
	include '../inc_sys.php';
	$incfn = $_REQUEST[incfn];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="Keywords" content="網站關鍵字" />
	<title>資訊查閱</title>
	<link href="/css/index.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="/Scripts/jquery.js"></script>
  <? include 'style.php' ?>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mainTable">
  <tr>
    <td class="sideBar">&nbsp;</td>
    <td width="1000" class="main" >
    	<!-- 頁面本體 ↓-->
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td class="headBar"><? include('../inc_head.php')?></td></tr>
        <tr><td class="menu"><? include('../inc_menu2.php') ?></td></tr>        
        <tr><td class="main">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr height="20" bgcolor="#FCD9D5"><td colspan="4"></td><td width="20"></td></tr>
            <tr height="25">
            	<td width="20" bgcolor="#FCD9D5"></td>
              <td width="200" bgcolor="#FCD9D5"><span style="font:18px '微軟正黑體'"><a href="index.php?incfn=main.html" class="menuHead">資訊查閱</a></span></td>
              <td width="25" background="../images/corner.png"></td><td><td><td></td>
            </tr>
            <tr valign="top">
              <td bgcolor="#FCD9D5">&nbsp;</td>
              <td bgcolor="#FCD9D5"><? include 'menu2.php' ?></td>
              <td width="25"></td>
              <td valign="top"><? include $incfn ?></td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </td></tr>
        <tr><td class="foot" align="center"><? include('../inc_foot.php')?></td></tr>
      </table>
      <!-- 頁面本體 ↑-->
    </td>  
    <td class="sideBar">&nbsp;</td>
  </tr>
</table>
</body>
</html>
